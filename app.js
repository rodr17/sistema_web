const { initializeApp, applicationDefault, cert } = require('firebase-admin/app');
const { getFirestore, Timestamp, FieldValue, query, onSnapshot, collection, getDocs } = require('firebase-admin/firestore');
const functions = require('firebase-functions');

const firebaseConfig = {
    apiKey: "AIzaSyCmtOCzTL7RjCi2ZkoJExiBmrGTxgnxN6Q",
    authDomain: "mextemps-341000.firebaseapp.com",
    projectId: "mextemps-341000",
    storageBucket: "mextemps-341000.appspot.com",
    messagingSenderId: "198033966029",
    appId: "1:198033966029:web:12659433a00b1e05e1fa84",
    measurementId: "G-SZY88RED1D",
    credential: cert(require('./firebase.json')),
 };

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const cronJob = functions.pubsub.schedule('* * * * *').onRun((context) => {
    const queries = db.collection("chats/1/messages");
    const querySnapshot = queries.get();
    console.log(querySnapshot);
    querySnapshot.forEach(async function(doc) {
        console.log(doc);
        return await `${doc.id} y el dato es ${doc.data}`;
    });
    return null;
});

console.log(app);
// return null;
// const port = 3000;
// var http = require('http');
// var server = http.createServer(function(req, res) {
//     res.writeHead(200, {'Content-Type': 'text/plain'});
//     var message = 'It works!\n',
//         version = 'NodeJS ' + process.versions.node + '\n',
//         response = [message, version].join('\n');
//     res.end(response);
// });
// console.log(server.listen());
