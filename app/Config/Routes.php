<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/logout', 'Administrador::logout');

////////////////////////////////////////////////////////////////////////////////Administrador
////////////GET
$routes->get('/log_adm2022', 'Administrador::login');
$routes->get('/dashboard', 'Administrador::dashboard');
$routes->get('/medios-contacto', 'Administrador::mediosContacto');
$routes->get('/contactos-soporte', 'Administrador::mediosSoporte');
$routes->get('/contactos-de-boletin', 'Administrador::contactoBoletin');
$routes->get('/crear-cupon', 'Pagos::crearCupon');
$routes->get('/lista-de-cupones', 'Pagos::listaCupones');

$routes->get('/apis', 'Administrador::apis');
$routes->get('/membresias', 'Administrador::membresias');
$routes->get('/nuevo-administrador', 'Administrador::agregar');
$routes->get('/idiomas', 'Administrador::idiomas');
$routes->get('/agregar-idioma', 'Administrador::addIdioma');
$routes->get('/perfil-administrador', 'Administrador::perfilAdministrador');
$routes->get('/listado-administradores', 'Administrador::listadoAdministradores');
$routes->get('/editar-administrador', 'Administrador::editAdministrador');
$routes->get('/paginas', 'Secciones::index');
$routes->get('/editar-seccion', 'Secciones::edit');
$routes->get('/ajustes-generales', 'Secciones::ajustesGenerales');
$routes->get('/registrarme-como', 'Home::selectorRegistro');

$routes->get('/habilidades', 'Habilidades::secAdministrador');
$routes->get('/agregar-habilidad', 'Habilidades::addHabilidad');
$routes->get('/solicitudes-de-habilidades', 'Habilidades::solicitudesHabilidad');
$routes->get('/todos-los-trabajos', 'Proyectos::totalProyectos');
$routes->get('/visualizar-proyecto', 'Proyectos::verProyectoAdmin');
$routes->get('/ver-proyectos-contratista', 'Proyectos::listaProyectosContratista');
$routes->get('/listado-proyectos', 'Administrador::listadoProyectos');

$routes->get('/administrar-noticias', 'Administrador::verNoticias');
$routes->get('/nueva-noticia', 'Administrador::nuevaNoticia');
$routes->get('/editar-noticia', 'Administrador::editNoticia');

$routes->get('/lista-freelancers', 'Administrador::listadoFreelancers');
$routes->get('/lista-contratistas', 'Administrador::listadoContratistas');
$routes->get('/editar-usuario', 'Administrador::editUserFreeCont');

////SECCIONES
$routes->get('/administrar-terminos-condiciones', 'Administrador::adminTerminosCondiciones');
$routes->get('/anadir-seccion-terminos', 'Administrador::addTerminoCondiciones');
$routes->get('/edit-seccion-terminos/(:hash)', 'Administrador::editTerminoCondiciones/$1');
$routes->get('/administrar-aviso-de-privacidad', 'Administrador::AdminAvisoPrivacidad');

////PAGOS
$routes->get('/listado-de-pagos', 'Administrador::listPagos');
$routes->get('/estadisticas-generales', 'Administrador::estadisticasGenerales');
$routes->get('/solicitudes-de-facturacion', 'Administrador::solicitudesFacturacion');

////EXTRAS
$routes->get('/faq-tarifas', 'Administrador::pregFrecuentesTarifas');
$routes->get('/faq-soporte', 'Administrador::pregSoporte');
$routes->get('/editar-testimonios', 'Administrador::editTestimonios');

////////////POST
$routes->post('/sig-in-admin', 'Administrador::sigin');
$routes->post('lista-usuarios', 'Administrador::users');
$routes->post('/update-contacto', 'Administrador::updateContact');
$routes->post('/update-pays', 'Administrador::savePagosMetodos');
$routes->post('/crear-membresia', 'Administrador::createMembership');
$routes->post('/editar-membresia', 'Administrador::editMember');
$routes->post('/actualizar-membresia', 'Administrador::updateMember');
$routes->post('/enviar-correo-admins', 'Administrador::correoAdmin');
$routes->post('/filtrar-proyectos', 'Proyectos::filtrarProyectosAdmin');
$routes->post('/datos-de-freelance', 'Usuarios::datosUsuarioFreelance');
$routes->post('/aprobar-usuario', 'Proyectos::aprobarFreelance');
$routes->post('/compra_realizada', 'Pagos::compra_realizada');

////SECCIONES
$routes->post('/guardar-seccion-terminos', 'Administrador::saveTermConditions');
$routes->post('/guardar-actua-terminos', 'Administrador::actuaSecTermCondiciones');
$routes->post('/eliminar-seccion-terminos', 'Administrador::deleteTermCondiciones');
$routes->post('/guardar-aviso-privacidad', 'Administrador::saveAvisoPrivacidad');
$routes->post('/actualizar-ingresos', 'Administrador::actualizarIngreso');
$routes->post('/guardar-noticia', 'Administrador::saveNoticia');

////////////////////////////////////////////////////////////////////////////////Usuario
////////////GET

$routes->get('Llenar_perfil', 'Autenticacion::perfil_pasos_freelancer');
$routes->get('Completar_registro', 'Autenticacion::perfil_pasos_contratista');
$routes->get('/recuperar-cuenta', 'Home::recuperarCuenta');
$routes->group('', function ($routes) {
    $routes->get('', 'Home::index');
    $routes->get('insignias', 'Home::insignias');
    $routes->get('como-funciona', 'Home::comoFunciona');
    $routes->get('tarifas-freelance', 'Home::paquetes');
    $routes->get('tarifas-contratista', 'Home::paquetesContratista');
    $routes->get('soporte', 'Home::soporte');
    $routes->get('contacto', 'Home::contacto');
    $routes->get('registrarme/(:alphanum)', 'Home::registrar/$1', ['as' => 'registrarme']);
    $routes->get('recuperar', 'Autenticacion::recuperarAcceso');
    $routes->get('ingresar', 'Home::ingresar');
    $routes->get('proyecto', 'Home::proyecto', ['as' => 'proyecto']);
    $routes->get('nosotros', 'Home::nosotros');
    $routes->get('terminos-y-condiciones', 'Home::termsAndCondiciones');
    $routes->get('aviso-de-privacidad', 'Home::avisoPrivacidad');
    $routes->get('registra-tu-empresa', 'Home::registrarEmpresa');
    $routes->get('sube-tu-portafolio', 'Home::subirPortafolio');
    $routes->get('trabajos', 'Home::verTrabajos', ['as' => 'trabajos']);
    $routes->get('trabajo/(:segment)', 'Home::verTrabajoID/$1', ['as' => 'trabajoID']);
    $routes->get('postularme/(:segment)', 'Proyectos::postularse/$1');
    $routes->get('ver-trabajo/(:hash)', 'Proyectos::viewProyecto/$1');
    $routes->post('SuscripcionUsuario', 'Suscripciones::crear_suscripcion');
    $routes->post('Suscripcion_efectivo', 'Suscripciones::crear_tienda');
    $routes->post('Suscripcion_transferencia', 'Suscripciones::crear_transferencia');
    $routes->post('Cancelar_suscripcion', 'Suscripciones::cancelar_suscripcion');
    $routes->post('CambiarPago', 'Pagos::cambiarPago');
    $routes->post('Notificaciones_paypal', 'Notificaciones::eventos_paypal');
    $routes->post('Notificaciones_openpay', 'Notificaciones::eventos_openpay');
    $routes->get('ver-noticia', 'Home::verNoticia');
    $routes->get('blog', 'Home::blog');
    $routes->get('quienes-somos', 'Home::quienesSomos');
    $routes->get('como-funciona-freelance', 'Home::comoFuncionaFreelance');
    $routes->get('como-funciona-contratista', 'Home::comoFuncionaContratista');
    $routes->post('suscribirseBoletines', 'Suscripciones::boletinesSuscripciones');
});
$routes->group('Usuarios', function ($routes) {
    $routes->get('perfil/(:segment)', 'Usuarios::perfil/$1', ['as' => 'perfil']);
    $routes->get('solicitudes', 'Usuarios::solicitudes', ['as' => 'solicitudes']);
    $routes->get('solicitud', 'Usuarios::solicitud', ['as' => 'solicitud']);
    $routes->get('postulaciones', 'Usuarios::postulaciones', ['as' => 'postulaciones']);
    $routes->get('postulacion', 'Usuarios::postulacion', ['as' => 'postulacion']);
    $routes->get('progresos', 'Usuarios::progresos', ['as' => 'progresos']);
    $routes->get('progreso', 'Usuarios::progreso', ['as' => 'progreso']);
    $routes->get('finalizados', 'Usuarios::finalizados', ['as' => 'finalizados']);
    $routes->get('finalizado', 'Usuarios::finalizado', ['as' => 'finalizado']);
    $routes->get('chat/(:segment)', 'Usuarios::chat/$1', ['as' => 'chat']);
    $routes->get('prevista/(:segment)', 'Usuarios::prevista/$1', ['as' => 'prevista']);
    $routes->get('talento', 'Usuarios::talento', ['as' => 'talento']);
});
$routes->get('proyectos-postulados', 'Proyectos::proyectos_postulaciones');
$routes->post('cambio_accesos', 'Autenticacion::cambiar_contraseña');
$routes->get('logeo', 'Autenticacion::iniciar_sesionFacebook');
$routes->get('logeoGoogle', 'Autenticacion::iniciar_sesionGoogle');
$routes->get('buscar-proyecto', 'Proyectos::resultFilterProyectos');
$routes->match(['get', 'post'], 'cupon-checkout', 'Pagos::cupon');

////////////POST
$routes->post('/buscar-freelance', 'Proyectos::freeSeleccionado');
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
