<?php namespace App\Controllers;

use App\Libraries\Correos;

use CodeIgniter\I18n\Time;
use App\Libraries\NetPay;
use App\Controllers\Suscripciones;
use App\Controllers\BaseController;

class Administrador extends BaseController{
	public function __construct(){
		helper('text');
	}
	// Funcion de vista de login de administrador
	public function login(){
		if(!session('is_logged')) return view(administrador('login'));
		if(session('rol') != 'admin') return redirect()->route('');
		
		return redirect()->to('/dashboard');
	}
	// Funcion para iniciar sesion por parte del administrador
	public function sigin(){
		$correo = trim($this->request->getPost('correo'));
		if(empty($correo)) return json_encode(['tipo' => 'error', 'mensaje' => 'El campo de correo no puede estar vacío'], JSON_FORCE_OBJECT);
		
		$contra = trim($this->request->getPost('pass'));
		if(empty($contra)) return json_encode(['tipo' => 'error', 'mensaje' => 'El campo de contraseña no puede estar vacío'], JSON_FORCE_OBJECT);

		$admin = model('ModelAdministradores')->where('correo', $correo)->first();
		if(empty($admin)) return json_encode(['tipo' => 'error', 'mensaje' => 'El correo no se encuentra registrado'], JSON_FORCE_OBJECT);
		
		if(!password_verify($contra, $admin['contrasena'])) return json_encode(['tipo' => 'error', 'mensaje' => 'La contraseña es incorrecta'], JSON_FORCE_OBJECT);

		session()->set([
			'id_user' => $admin['id'],
			'usuario' => $admin['name'],
			'correo' => $admin['correo'],
			'rol' => 'admin',
			'is_logged' => true
		]);
		return json_encode(['tipo' => 'success', 'mensaje' => $admin['name']], JSON_FORCE_OBJECT);
	}
	// Funcion para terminar sesion
	public function logout(){
		session()->destroy();
		return redirect()->to('/');
	}
	// Funcion de dashboard
	public function dashboard(){
		if(!session('is_logged')) return redirect()->route('');
		if(session('rol') != 'admin') return redirect()->route('');

		return view(administrador('dashboard'));
	}
	// Funcion para agregar un administrador nuevo
	public function agregar(){
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);
		
		return view(administrador('nuevo-admin'));
	}
	// Funcion para guardar al nuevo administrador
	public function newAgreg(){
		$user = trim($this->request->getPost('usuario'));
		if(empty($user)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el nombre de usuario para el nuevo administrador.'], JSON_FORCE_OBJECT);
		
		$correo = trim($this->request->getPost('correo'));
		if(empty($correo)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el correo de registro para el nuevo administrador.'], JSON_FORCE_OBJECT);
		
		$pass = trim($this->request->getPost('password'));
		if(empty($pass)) return json_encode(['tipo' => 'error', 'mensaje' => 'Introduzca la contraseña para el nuevo usuario.'], JSON_FORCE_OBJECT);
		
		$membresias = !empty($this->request->getPost('membresias'));
		$permisos['membresias'] = ['sec' => 'membresias', 'valor' => $membresias];

		$pagos = !empty($this->request->getPost('pagos'));
		$permisos['pagos'] = ['sec' => 'pagos', 'valor' => $pagos];

		$lista_pagos = !empty($this->request->getPost('lista_pagos'));
		$permisos['lista_pagos'] = ['sec' => 'lista_pagos', 'valor' => $lista_pagos];

		$estadisticas_generales = !empty($this->request->getPost('estadisticas_generales'));
		$permisos['estadisticas_generales'] = ['sec' => 'estadisticas_generales', 'valor' => $estadisticas_generales];

		$solicitudes_facturacion = !empty($this->request->getPost('solicitudes_facturacion'));
		$permisos['solicitudes_facturacion'] = ['sec' => 'solicitudes_facturacion', 'valor' => $solicitudes_facturacion];

		$crear_cupon = !empty($this->request->getPost('crear_cupon'));
		$permisos['crear_cupon'] = ['sec' => 'crear_cupon', 'valor' => $crear_cupon];

		$lista_cupones = !empty($this->request->getPost('lista_cupones'));
		$permisos['lista_cupones'] = ['sec' => 'lista_cupones', 'valor' => $lista_cupones];

		$proyectos = !empty($this->request->getPost('proyectos'));
		$permisos['proyectos'] = ['sec' => 'proyectos', 'valor' => $proyectos];
		
		$usuarios = !empty($this->request->getPost('usuarios'));
		$permisos['usuarios'] = ['sec' => 'usuarios', 'valor' => $usuarios];

		$mi_perfil = !empty($this->request->getPost('mi_perfil'));
		$permisos['mi_perfil'] = ['sec' => 'mi_perfil', 'valor' => $mi_perfil];
		
		$listado_administradores = !empty($this->request->getPost('listado_administradores'));
		$permisos['listado_administradores'] = ['sec' => 'listado_administradores', 'valor' => $listado_administradores];

		$listado_freelancers = !empty($this->request->getPost('listado_freelancers'));
		$permisos['listado_freelancers'] = ['sec' => 'listado_freelancers', 'valor' => $listado_freelancers];

		$listado_contratantes = !empty($this->request->getPost('listado_contratantes'));
		$permisos['listado_contratantes'] = ['sec' => 'listado_contratantes', 'valor' => $listado_contratantes];

		$agregar_usuario_admin = !empty($this->request->getPost('agregar_usuario_admin'));
		$permisos['agregar_usuario_admin'] = ['sec' => 'agregar_usuario_admin', 'valor' => $agregar_usuario_admin];

		$habilidades = !empty($this->request->getPost('habilidades'));
		$permisos['habilidades'] = ['sec' => 'habilidades', 'valor' => $habilidades];

		$noticias = !empty($this->request->getPost('noticias'));
		$permisos['noticias'] = ['sec' => 'noticias', 'valor' => $noticias];

		$idiomas = !empty($this->request->getPost('idiomas'));
		$permisos['idiomas'] = ['sec' => 'idiomas', 'valor' => $idiomas];

		$contenidos = !empty($this->request->getPost('contenidos'));
		$permisos['contenidos'] = ['sec' => 'contenidos', 'valor' => $contenidos];

		$editar_portada = !empty($this->request->getPost('editar_portada'));
		$permisos['editar_portada'] = ['sec' => 'editar_portada', 'valor' => $editar_portada];

		$listado_paginas = !empty($this->request->getPost('listado_paginas'));
		$permisos['listado_paginas'] = ['sec' => 'listado_paginas', 'valor' => $listado_paginas];

		$ajustes_generales = !empty($this->request->getPost('ajustes_generales'));
		$permisos['ajustes_generales'] = ['sec' => 'ajustes_generales', 'valor' => $ajustes_generales];

		$aviso_privacidad = !empty($this->request->getPost('aviso_privacidad'));
		$permisos['aviso_privacidad'] = ['sec' => 'aviso_privacidad', 'valor' => $aviso_privacidad];

		$terminos_condiciones = !empty($this->request->getPost('terminos_condiciones'));
		$permisos['terminos_condiciones'] = ['sec' => 'terminos_condiciones', 'valor' => $terminos_condiciones];

		$faq_tarifas = !empty($this->request->getPost('faq_tarifas'));
		$permisos['faq_tarifas'] = ['sec' => 'faq_tarifas', 'valor' => $faq_tarifas];

		$faq_soporte = !empty($this->request->getPost('faq_soporte'));
		$permisos['faq_soporte'] = ['sec' => 'faq_soporte', 'valor' => $faq_soporte];
		
		$contacto = !empty($this->request->getPost('medios_contacto'));
		$permisos['medios_contacto'] = ['sec' => 'medios_contacto', 'valor' => $contacto];

		$llaves_api = !empty($this->request->getPost('llaves_api'));
		$permisos['llaves_api'] = ['sec' => 'llaves_api', 'valor' => $llaves_api];

		$contactos_soporte = !empty($this->request->getPost('contactos_soporte'));
		$permisos['contactos_soporte'] = ['sec' => 'contactos_soporte', 'valor' => $contactos_soporte];

		$contactos_boletin = !empty($this->request->getPost('contactos_boletin'));
		$permisos['contactos_boletin'] = ['sec' => 'contactos_boletin', 'valor' => $contactos_boletin];

		$data = [
			'correo' => $correo,
			'contrasena' => password_hash($pass, PASSWORD_DEFAULT),
			'tipo' => 'admin',
			'permisos' => json_encode($permisos, JSON_FORCE_OBJECT),
			'name' => $user
		];
		$model = model('ModelAdministradores');
		$admin = $model->where('correo', $correo)->first();
		
		if(!empty($admin)) return json_encode(['tipo' => 'error', 'mensaje' => 'El correo ya está registrado en el sistema'], JSON_FORCE_OBJECT);
		$model->save($data);
		
		$Correo = new Correos();
		if(!$Correo->newAdmin($correo, $pass)) return json_encode(['tipo' => 'success', 'mensaje' => 'Administrador creado correctamente'], JSON_FORCE_OBJECT);
		return json_encode(['tipo' => 'error', 'mensaje' => 'Error al enviar el correo.'], JSON_FORCE_OBJECT);
	}
	public function listadoAdministradores(){
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);

		if(!$_GET){
			$admins = model('ModelAdministradores')->findAll();
		}else{
			$busqueda = sanitizar($_GET['search']);
			$query = db_connect()->query("SELECT * FROM administradores WHERE (name LIKE '%".$busqueda."%' OR nombre LIKE '%".$busqueda."%' OR correo LIKE '%".$busqueda."%')");
			$admins = $query->getResultArray();
		}
		return view(administrador('lista-administradores'), array('administradores' => $admins));
	}
	// Funcion para editar perfil de un administrador
	public function editAdministrador(){
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);

		if(!isset($_GET['id'])) return redirect()->to('/lista-usuarios');
		
		$id = base64_decode($_GET['id']);
		$admin = model('ModelAdministradores')->where('id', $id)->first();
		return view('/backend/administrador/editar-administrador', compact('admin'));
	}
	// Funcion para guardar cambios de datos de administrador
	public function updateAdministrador(){
		$model = model('ModelAdministradores');
		$id = sanitizar($this->request->getPost('id'));
		// Se validan los datos importantes, correo y nickname
		$correo = trim($this->request->getPost('correo'));
		if(empty($correo)) return json_encode(['tipo' => 'error', 'mensaje' => 'El campo de correo no puede estar vacÍo.'], JSON_FORCE_OBJECT);

		$name = sanitizar($this->request->getPost('name'));
		if(empty($name)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un nombre de usuario.'], JSON_FORCE_OBJECT);

		// Se obtienen el resto de campos, nombre, telefono, correo de recuperacion y cambio de imagen
		$nombre = sanitizar($this->request->getPost('nombre'));
		$telefono = sanitizar($this->request->getPost('telefono'));
		$correorecuperacion = trim($this->request->getPost('correorecuperacion'));

		$membresias = !empty($this->request->getPost('membresias'));
		$permisos['membresias'] = ['sec' => 'membresias', 'valor' => $membresias];

		$pagos = !empty($this->request->getPost('pagos'));
		$permisos['pagos'] = ['sec' => 'pagos', 'valor' => $pagos];

		$lista_pagos = !empty($this->request->getPost('lista_pagos'));
		$permisos['lista_pagos'] = ['sec' => 'lista_pagos', 'valor' => $lista_pagos];

		$estadisticas_generales = !empty($this->request->getPost('estadisticas_generales'));
		$permisos['estadisticas_generales'] = ['sec' => 'estadisticas_generales', 'valor' => $estadisticas_generales];

		$solicitudes_facturacion = !empty($this->request->getPost('solicitudes_facturacion'));
		$permisos['solicitudes_facturacion'] = ['sec' => 'solicitudes_facturacion', 'valor' => $solicitudes_facturacion];

		$crear_cupon = !empty($this->request->getPost('crear_cupon'));
		$permisos['crear_cupon'] = ['sec' => 'crear_cupon', 'valor' => $crear_cupon];

		$lista_cupones = !empty($this->request->getPost('lista_cupones'));
		$permisos['lista_cupones'] = ['sec' => 'lista_cupones', 'valor' => $lista_cupones];

		$proyectos = !empty($this->request->getPost('proyectos'));
		$permisos['proyectos'] = ['sec' => 'proyectos', 'valor' => $proyectos];
		
		$usuarios = !empty($this->request->getPost('usuarios'));
		$permisos['usuarios'] = ['sec' => 'usuarios', 'valor' => $usuarios];

		$mi_perfil = !empty($this->request->getPost('mi_perfil'));
		$permisos['mi_perfil'] = ['sec' => 'mi_perfil', 'valor' => $mi_perfil];
		
		$listado_administradores = !empty($this->request->getPost('listado_administradores'));
		$permisos['listado_administradores'] = ['sec' => 'listado_administradores', 'valor' => $listado_administradores];

		$listado_freelancers = !empty($this->request->getPost('listado_freelancers'));
		$permisos['listado_freelancers'] = ['sec' => 'listado_freelancers', 'valor' => $listado_freelancers];

		$listado_contratantes = !empty($this->request->getPost('listado_contratantes'));
		$permisos['listado_contratantes'] = ['sec' => 'listado_contratantes', 'valor' => $listado_contratantes];

		$agregar_usuario_admin = !empty($this->request->getPost('agregar_usuario_admin'));
		$permisos['agregar_usuario_admin'] = ['sec' => 'agregar_usuario_admin', 'valor' => $agregar_usuario_admin];

		$habilidades = !empty($this->request->getPost('habilidades'));
		$permisos['habilidades'] = ['sec' => 'habilidades', 'valor' => $habilidades];

		$noticias = !empty($this->request->getPost('noticias'));
		$permisos['noticias'] = ['sec' => 'noticias', 'valor' => $noticias];

		$idiomas = !empty($this->request->getPost('idiomas'));
		$permisos['idiomas'] = ['sec' => 'idiomas', 'valor' => $idiomas];

		$contenidos = !empty($this->request->getPost('contenidos'));
		$permisos['contenidos'] = ['sec' => 'contenidos', 'valor' => $contenidos];

		$editar_portada = !empty($this->request->getPost('editar_portada'));
		$permisos['editar_portada'] = ['sec' => 'editar_portada', 'valor' => $editar_portada];

		$listado_paginas = !empty($this->request->getPost('listado_paginas'));
		$permisos['listado_paginas'] = ['sec' => 'listado_paginas', 'valor' => $listado_paginas];

		$ajustes_generales = !empty($this->request->getPost('ajustes_generales'));
		$permisos['ajustes_generales'] = ['sec' => 'ajustes_generales', 'valor' => $ajustes_generales];

		$aviso_privacidad = !empty($this->request->getPost('aviso_privacidad'));
		$permisos['aviso_privacidad'] = ['sec' => 'aviso_privacidad', 'valor' => $aviso_privacidad];

		$terminos_condiciones = !empty($this->request->getPost('terminos_condiciones'));
		$permisos['terminos_condiciones'] = ['sec' => 'terminos_condiciones', 'valor' => $terminos_condiciones];

		$faq_tarifas = !empty($this->request->getPost('faq_tarifas'));
		$permisos['faq_tarifas'] = ['sec' => 'faq_tarifas', 'valor' => $faq_tarifas];

		$faq_soporte = !empty($this->request->getPost('faq_soporte'));
		$permisos['faq_soporte'] = ['sec' => 'faq_soporte', 'valor' => $faq_soporte];
		
		$contacto = !empty($this->request->getPost('medios_contacto'));
		$permisos['medios_contacto'] = ['sec' => 'medios_contacto', 'valor' => $contacto];

		$llaves_api = !empty($this->request->getPost('llaves_api'));
		$permisos['llaves_api'] = ['sec' => 'llaves_api', 'valor' => $llaves_api];

		$contactos_soporte = !empty($this->request->getPost('contactos_soporte'));
		$permisos['contactos_soporte'] = ['sec' => 'contactos_soporte', 'valor' => $contactos_soporte];

		$contactos_boletin = !empty($this->request->getPost('contactos_boletin'));
		$permisos['contactos_boletin'] = ['sec' => 'contactos_boletin', 'valor' => $contactos_boletin];

		$file = $this->request->getFile('filePicker');
		$tempfile = $file->getTempName();
		$data = [
			'correo' => $correo,
			'permisos' => json_encode($permisos, JSON_FORCE_OBJECT),
			'name' => $name,
			'nombre' => $nombre,
			'telefono' => $telefono,
			'correorecuperacion' => $correorecuperacion
		];
		// se verifica si va ver cambio de imagen o no
		if(!empty($tempfile)){
			$miimagen = $model->where('id', $id)->first();
			$img = $miimagen['imagen'];

			if(!empty($img)) unlink('./'.$img);

			$nameimg = $_FILES['filePicker']['name'];//Se guarda el nombre original
			$path = $this->request->getFile('filePicker')->store('foto-perfil-admins/'.$id.'/', $nameimg);//Se guarda la imagen y se evita que se le asigne un nombre random
			$ruta = '/writable/uploads/foto-perfil-admins/'.$id.'/'.$nameimg;
			$data['imagen'] = $ruta;
		}
		
		$model->where('id', $id)->set($data)->update();
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sus datos han sido actualizados.'], JSON_FORCE_OBJECT);
	}
	// Funcion lista de usuarios
	public function users(){
		if(!session('is_logged')) return redirect()->route('');
		if(session('rol') != 'admin') return redirect()->route('');

		$admins = model('ModelAdministradores')->findAll();
		return view(administrador('lista-usuarios'), array('administradores' => $admins));
	}
	public function listadoFreelancers(){
		if(!session('is_logged')) return redirect()->route('');
		if(session('rol') != 'admin') return redirect()->route('');

		return view(administrador('lista-usuariosFreelancer'), array('freelancers' => $this->listar_usuarios('freelancer'), 'hoy' => date('Y-m-d')));
	}

	public function listar_usuarios(string $rol){
		$where = "";
		$fecha_inicio = trim($this->request->getPost('inicio'));
		$fecha_fin = trim($this->request->getPost('fin'));
		if(!empty($fecha_inicio) && !empty($fecha_fin)){
			$fechas = [
				'inicio' => $fecha_inicio.' 00:00:00',
				'fin' => $fecha_fin.' 23:59:59'
			];
			$where = "AND created_at BETWEEN CAST('".$fechas['inicio']."' AS DATETIME) AND CAST('".$fechas['fin']."' AS DATETIME)";
		}
		$search = trim($this->request->getPost('search'));
		$db     = \Config\Database::connect();
		$query = $db->query("SELECT * FROM (SELECT *, CONCAT(nombre,' ',apellidos) AS nombre_completo FROM usuarios) concatenacion
							WHERE (correo LIKE '%".$search."%' OR concatenacion.nombre_completo LIKE '%".$search."%')
								AND (rol = '$rol')
								AND (deleted_at is null)
								$where
								ORDER BY id DESC");
		$usuarios = $query->getResultArray();

		$eliminados = model('EliminadosAdmin')->findAll();
		if($eliminados != NULL){
			foreach($usuarios as $key => $c){
				foreach($eliminados as $e){
					if($c['id'] == $e['id_usuario']){
						unset($usuarios[$key]);
					}
				}
			}
		}
		
		return $usuarios;
	}
	public function listadoContratistas(){
		if(!session('is_logged')) return redirect()->route('');
		if(session('rol') != 'admin') return redirect()->route('');

		return view(administrador('lista-usuariosContratista'), array('contratistas' => $this->listar_usuarios('contratante'), 'hoy' => date('Y-m-d')));
	}

	public function filtrarUsuarios(){
		$rol = sanitizar($this->request->getPost('rol'));
		$usuarios = $this->listar_usuarios($rol);
		if(empty($usuarios)) return "<div class='alert alert-info'>Sin $rol"."s coincidentes</div>";

		$html = "";
		foreach($usuarios as $usuario){
			$html .= view_cell('App\Libraries\FuncionesAdmin::listadoUsuarios', ['usuario' => $usuario]);
		}
		return $html;
	}
	public function editUserFreeCont(){ //MODIFICAR A NETPAY PASARELA DE PAGOS
		if(!session('is_logged')) return redirect()->route('');
		if(session('rol') != 'admin') return redirect()->route('');
		if(!$_GET) return redirect()->back()->with('alerta', ['tipo' => 'warning', 'mensaje' => 'Seleccione un usuario para editar.']);
		if(!isset($_GET['id'])) return redirect()->back()->with('alerta', ['tipo' => 'warning', 'mensaje' => 'Seleccione un usuario para editar.']);
		$user = model('Autenticacion')->get_id(base64_decode($_GET['id']));
		if($user == null) return redirect()->back()->with('alerta', ['tipo' => 'warning', 'mensaje' => 'No se logró encontrar el usuario que desea editar.']);
		$con_suscripcion = Suscripciones::esta_suscrito_por_idUsuario(base64_decode($_GET['id']));
		if($con_suscripcion){
			$suscripcion_actual = model('Suscripciones')
							 ->builder()
							 ->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
							 ->where('id_usuario', base64_decode($_GET['id']))
							 ->get()->getRowArray();
		}else{
			$suscripcion_actual = null;
		}
		return view('/backend/administrador/editar-usuario', array('suscripcion'=> $suscripcion_actual, 'con_suscripcion' => $con_suscripcion, 'user' => $user, 'estados' => model('ModelEstados')->findAll()));
	}
	public function saveEditInfoUsuario(){
		// Se valida la sesion del usuario
		if(!session('is_logged')) return json_encode(['tipo' => 'warning', 'mensaje' => 'Sin permisos suficientes.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'warning', 'mensaje' => 'Sin permisos suficientes.'], JSON_FORCE_OBJECT);
		// Se validan los campos de información del usuario
		if(trim($this->request->getPost('id')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el id del usuario.'], JSON_FORCE_OBJECT);
		// if(trim($this->request->getPost('img_recortada')) == '') return json_encode([], JSON_FORCE_OBJECT);
		// if(trim($this->request->getPost('nombre')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el nombre del usuario.'], JSON_FORCE_OBJECT);
		// if(trim($this->request->getPost('apellidos')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese los apellidos del usuario.'], JSON_FORCE_OBJECT);
		// if(trim($this->request->getPost('correo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un correo del usuario.'], JSON_FORCE_OBJECT);
		// if(trim($this->request->getPost('rfc')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el RFC del usuario.'], JSON_FORCE_OBJECT);
		// if(trim($this->request->getPost('curp')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el CURP del usuario.'], JSON_FORCE_OBJECT);
		// if(trim($this->request->getPost('telefono')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el teléfono del usuario.'], JSON_FORCE_OBJECT);
		// if(trim($this->request->getPost('facebook')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el usuario de Facebook del usuario.'], JSON_FORCE_OBJECT);
		// if(trim($this->request->getPost('instagram')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el usuario de Instagram del usuario.'], JSON_FORCE_OBJECT);
		// if(trim($this->request->getPost('whatsapp')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el numero de WhatsApp del usuario.'], JSON_FORCE_OBJECT);
		// if(trim($this->request->getPost('calle')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la calle con el numero de la dirección del usuario.'], JSON_FORCE_OBJECT);
		// if(trim($this->request->getPost('colonia')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la colonia de la dirección del usuario.'], JSON_FORCE_OBJECT);
		// if(trim($this->request->getPost('cp')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el código postal de la dirección del usuario.'], JSON_FORCE_OBJECT);
		// if(trim($this->request->getPost('municipio')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el municipio de la dirección del usuario.'], JSON_FORCE_OBJECT);
		// if(trim($this->request->getPost('estado')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Seleccione el estado de la dirección del usuario.'], JSON_FORCE_OBJECT);
		// if(trim($this->request->getPost('pais')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el país de la dirección del usuario.'], JSON_FORCE_OBJECT);

		$usuario = model('Autenticacion')->get_id(trim($this->request->getPost('id')));
		if($usuario == null) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar al usuario dentro de la plataforma.'], JSON_FORCE_OBJECT);
		$contacto = [
			'telefono' => trim($this->request->getPost('telefono')),
			'facebook' => trim($this->request->getPost('facebook')),
			'instagram' => trim($this->request->getPost('instagram')),
			'whatsapp' => trim($this->request->getPost('whatsapp')),
			'whatsapp_visible' => isset($usuario['contacto']['whatsapp_visible'])? $usuario['contacto']['whatsapp_visible'] : '',
		];
		$direccion = [
			'calle' => trim($this->request->getPost('calle')),
			'colonia' => trim($this->request->getPost('colonia')),
			'municipio' => trim($this->request->getPost('municipio')),
			'ciudad' => '',
			'estado' => trim($this->request->getPost('estado')),
			'pais' => trim($this->request->getPost('pais')),
			'codigo_postal' => trim($this->request->getPost('cp')),
			'curp' => trim($this->request->getPost('curp')),
			'rfc' => trim($this->request->getPost('rfc'))
		];
		$direccion_info = array();
		array_push($direccion_info, $direccion);
		$data = [
			'nombre' => trim($this->request->getPost('nombre')),
			'apellidos' => trim($this->request->getPost('apellidos')),
			'correo' => trim($this->request->getPost('correo')),
			'imagen' => str_replace(' ', '+', trim($this->request->getPost('img_recortada'))),
			'contacto' => json_encode($contacto, JSON_FORCE_OBJECT),
			'direccion' => json_encode($direccion_info, JSON_FORCE_OBJECT)
		];
		model('Autenticacion')->where('id', $usuario['id'])->set($data)->update();
		return json_encode(['tipo' => 'success', 'mensaje' => 'La información del usuario ha sido actualizada.'], JSON_FORCE_OBJECT);
	}
	// Funcion para eliminar usuario
	public function eliminarUsuario(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Sin permisos para realizar la siguiente acción.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'Sin permisos para realizar la siguiente acción.'], JSON_FORCE_OBJECT);

		$id_usuario = trim($this->request->getPost('id'));
		if(empty($id_usuario)) return json_encode(['tipo' => 'error', 'mensaje' => 'Seleccione el usuario que desea eliminar.'], JSON_FORCE_OBJECT);

		$use = model('EliminadosAdmin')->where('id_usuario', $id_usuario)->first();
		if($use != null) return json_encode(['tipo' => 'error', 'mensaje' => 'El usuario ya se encuentra eliminado dentro de la plataforma.'], JSON_FORCE_OBJECT);
		
		$user = model('Autenticacion')->get_id($id_usuario);
		model('EliminadosAdmin')->save(['id_usuario' => $id_usuario, 'motivo' => empty($id_usuario)? NULL : $id_usuario]);

		$Correo = new Correos();
		$Correo->usuarioEliminadoAdmin($user['correo']);
		return json_encode(['tipo' => 'success', 'mensaje' => 'El usuario ha sido eliminado y se le ha notificado por correo.'], JSON_FORCE_OBJECT);
	}
	// Funcion para ver informacion de un reporte de algun proyecto por parte de un usuario
	public function verReporteSoporte(){
		if(trim($this->request->getPost('id')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el numero de reporte.'], JSON_FORCE_OBJECT);
		$reporte = model('Soporte')->where('id', trim($this->request->getPost('id')))->first();
		if($reporte == null) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el reporte seleccionado.'], JSON_FORCE_OBJECT);
		return json_encode(['tipo' => 'success', 'mensaje' => view('backend/administrador/viewcells/reporte-soporte-modal', array('reporte' => $reporte))], JSON_FORCE_OBJECT);
	}
	// Funcion para contestar reporte de soporte
	public function responderSoporte(){
		if(!session('is_logged')) return json_encode(['tipo' => 'warning', 'mensaje' => 'Sin permisos suficientes.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'warning', 'mensaje' => 'Sin permisos suficientes.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('id')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se encontro el NO. de reporte'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('mensaje')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un mensaje para el usaurio'], JSON_FORCE_OBJECT);
		$reporte = model('Soporte')->where('id', trim($this->request->getPost('id')))->first();
		// dd($reporte);
		if($reporte == null) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el reporte seleccionado.'], JSON_FORCE_OBJECT);
		$Correo = new Correos();
		if(!$Correo->respuestaSoporte($reporte, trim($this->request->getPost('mensaje')))) return json_encode(['alerta' => 'warning', 'mensaje' => 'No se pudo enviar, notificar a soporte'], JSON_FORCE_OBJECT);
		model('Soporte')->where('id', trim($this->request->getPost('id')))->delete();
		return json_encode(['tipo' => 'success', 'mensaje' => 'Se ha notificado al usuario sobre su reporte.']);
	}
	// Funcion para mostrar solicitudes de reporte
	public function mostrarReportesSoporte(){
		return view_cell('App\Libraries\FuncionesAdmin::reportesSoportes');
	}
	// Funcion para mandar correo a usuario administrador
	public function correoAdmin(){
		if(!session('is_logged')) return json_encode(['tipo' => 'warning', 'mensaje' => 'Sin permisos suficientes.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'warning', 'mensaje' => 'Sin permisos suficientes.'], JSON_FORCE_OBJECT);

		$id = trim($this->request->getPost('id'));
		$body = $this->request->getPost('body');
		$model = model('ModelAdministradores');
		if(empty($id)) return json_encode(['tipo' => 'warning', 'mensaje' => 'No se pudo identificar al destinatario.'], JSON_FORCE_OBJECT);

		$data = $model->where('id', $id)->first();
		if(empty($data)) return json_encode(['tipo' => 'warning', 'mensaje' => 'La información del usuario es incorrecta.'], JSON_FORCE_OBJECT);

		if(empty($body)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un mensaje para el usuario'], JSON_FORCE_OBJECT);

		$enviador = $model->where('id', session('id_user'))->first();
		if($enviador == null) return json_encode(['tipo' => 'warning', 'mensaje' => 'Sin permisos suficientes.'], JSON_FORCE_OBJECT);

		$Correo = new Correos();
		if(!$Correo->mensajeAdmin($data['correo'], $body, $enviador)) return json_encode(['alerta' => 'warning', 'mensaje' => 'No se pudo enviar, notificar a soporte'], JSON_FORCE_OBJECT);

		return json_encode(['tipo' => 'success', 'mensaje' => 'El correo ha sido enviado ha '.$data['name']], JSON_FORCE_OBJECT);
	}
	// Funcion para mandar contraseña nueva para el administrador
	public function newContraAdmin(){
		if(!session('is_logged')) return json_encode(['tipo' => 'warning', 'mensaje' => 'Sin permisos suficientes.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'warning', 'mensaje' => 'Sin permisos suficientes.'], JSON_FORCE_OBJECT);
		
		$contra = trim($this->request->getPost('contra'));
		$confirm = trim($this->request->getPost('confirm'));
		if(empty($contra)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese una contraseña nueva.'], JSON_FORCE_OBJECT);

		if(empty($confirm)) return json_encode(['tipo' => 'error', 'mensaje' => 'El campo de confirmar contraseña no puede estar vacío.'], JSON_FORCE_OBJECT);

		if($contra != $confirm) return json_encode(['tipo' => 'error', 'mensaje' => 'Los campos no coinciden.'], JSON_FORCE_OBJECT);

		$model = model('ModelAdministradores');
		$data = $model->where('id', session('id_user'))->first();
		if(password_verify($contra, $data['contrasena'])) return json_encode(['tipo' => 'error', 'mensaje' => 'La contraseña debe ser diferente a la existente.'], JSON_FORCE_OBJECT);

		$model->where('id', session('id_user'))->set(['contrasena' => password_hash($contra, PASSWORD_DEFAULT)])->update();
		return json_encode(['tipo' => 'success', 'mensaje' => 'Contraseña actualizada.'], JSON_FORCE_OBJECT);
	}
	// Funcion de vista de perfil de administrador
	public function perfilAdministrador(){
		if(!session('is_logged')) return redirect()->route('');
		if(session('rol') != 'admin') return redirect()->route('');
		
		return view(administrador('perfil-administrador'));
	}
	// Funcion para actualizar datos del administrador
	public function updatePerfilAdmin(){
		if(!session('is_logged')) return json_encode(['tipo' => 'warning', 'mensaje' => 'Sin permisos suficientes.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'warning', 'mensaje' => 'Sin permisos suficientes.'], JSON_FORCE_OBJECT);

		$model = model('ModelAdministradores');
		$id = session('id_user');
		// Se validan los datos importantes, correo y nickname
		$correo = trim($this->request->getPost('correo'));
		if(empty($correo)) return json_encode(['tipo' => 'error', 'mensaje' => 'El campo de correo no puede estar vacío.'], JSON_FORCE_OBJECT);

		$name = trim($this->request->getPost('name'));
		if(empty($name)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un nombre de usuario.'], JSON_FORCE_OBJECT);

		// Se obtienen el resto de campos, nombre, telefono, correo de recuperacion y cambio de imagen
		$nombre = trim($this->request->getPost('nombre'));
		$telefono = trim($this->request->getPost('telefono'));
		$correorecuperacion = trim($this->request->getPost('correorecuperacion'));
		$file = $this->request->getFile('filePicker');
		$tempfile = $file->getTempName();
		// se verifica si va ver cambio de imagen o no
		if(empty($tempfile)){
			$data = [
				'correo' => $correo,
				'name' => $name,
				'nombre' => $nombre,
				'telefono' => $telefono,
				'correorecuperacion' => $correorecuperacion
			];
			$model->where('id', $id)->set($data)->update();
			return json_encode(['tipo' => 'success', 'mensaje' => 'Sus datos han sido actualizados.'], JSON_FORCE_OBJECT);
		}else{
			$miimagen = $model->where('id', $id)->first();
			$img = $miimagen['imagen'];

			if(!empty($img)) unlink('./'.$img);

			$nameimg = $_FILES['filePicker']['name'];//Se guarda el nombre original
			$path = $this->request->getFile('filePicker')->store('foto-perfil-admins/'.$id.'/', $nameimg);//Se guarda la imagen y se evita que se le asigne un nombre random
			$ruta = '/writable/uploads/foto-perfil-admins/'.$id.'/'.$nameimg;
			$data = [
				'correo' => $correo,
				'name' => $name,
				'nombre' => $nombre,
				'telefono' => $telefono,
				'correorecuperacion' => $correorecuperacion,
				'imagen' => $ruta
			];
			$model->where('id', $id)->set($data)->update();
			return json_encode(['tipo' => 'success', 'mensaje' => 'Sus datos han sido actualizados.', 'imagen' => base_url('assets').$ruta], JSON_FORCE_OBJECT);
		}
	}
	// Funcion para vista por parte del administrador para administrar terminos y condiciones
	public function adminTerminosCondiciones(){
		if(!session('is_logged')) return redirect()->route('');
		if(session('rol') != 'admin') return redirect()->route('');
		
		$texto_terminos = model('ModelSecciones')->where('seccion', 'terminos-y-condiciones')->first();
		return view(administrador('ver-terminos-condiciones'), compact('texto_terminos'));
	}
	// Funcion para vistapor parte del administrador para administrar aviso de privacidad
	public function AdminAvisoPrivacidad(){
		if(!session('is_logged')) return redirect()->route('');
		if(session('rol') != 'admin') return redirect()->route('');
		
		$aviso = model('ModelSecciones')->where('seccion', 'aviso-privacidad')->first();
		return view(administrador('aviso-de-privacidad'), compact('aviso'));
	}
	//Funcion para guardar aviso de privacidad
	public function saveAvisoPrivacidad(){
		$body = trim($this->request->getPost('body'));
		if(empty($body)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el contenido para el aviso de privacidad.'], JSON_FORCE_OBJECT);

		$model = model('ModelSecciones');
		$aviso = $model->where('seccion', 'aviso-privacidad')->first(); 
		if(empty($aviso)){
			$data = [
				'seccion' => 'aviso-privacidad',
				'tipo' => 'seccion',
				'info' => json_encode(['body' => $body], JSON_FORCE_OBJECT)
			];
			$model->save($data);
		}
		else $model->where('seccion', 'aviso-privacidad')->set([ 'info' => json_encode(['body' => $body], JSON_FORCE_OBJECT)])->update();

		return json_encode(['tipo' => 'success', 'mensaje' => 'Aviso de privacidad actualizado'], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar actualizacion de seccion de terminos y secciones
	public function actuaSecTermCondiciones(){
		$body = $this->request->getPost('body');
		if(empty($body)) return json_encode(['tipo' => 'error','mensaje' => 'Ingrese contenido para los términos y condiciones.'], JSON_FORCE_OBJECT);
		
		$terminos = model('ModelSecciones')->select('id')->where('seccion', 'terminos-y-condiciones')->get()->getRowArray();
		if(model('ModelSecciones')->where('seccion', 'terminos-y-condiciones')->save(['id' => $terminos['id'],'info' => $body])); return json_encode(['tipo' => 'success', 'mensaje' => 'Términos y condiciones actualizado'], JSON_FORCE_OBJECT);

		return json_encode(['tipo' => 'error', 'mensaje' => 'No se pude actualizar, intente de nuevo'], JSON_FORCE_OBJECT);
	}
	// Funcion para eliminar seccion de terminos y condiciones
	public function deleteTermCondiciones(){
		$id = $this->request->getPost('id');
		model('ModelSecciones')->eliminar($id);
		return 1;
	}
	// Funcion para vista para agregar nueva seccion a terminos y condiciones
	public function addTerminoCondiciones(){
		if(!session('is_logged')) return redirect()->route('');
		if(session('rol') != 'admin') return redirect()->route('');
		
		return view(administrador('anadir-terminos-condiciones'));
	}
	// Funcion para guardar seccion de terminos y condiciones
	public function saveTermConditions(){
		$model = model('ModelSecciones');
		$body = trim($this->request->getPost('body'));
		$titulo = trim($this->request->getPost('title'));
		if(empty($titulo)) return json_encode(['tipo' => 'error','mensaje' => 'Ingrese un titulo para la sección.'], JSON_FORCE_OBJECT);

		if(empty($body)) return json_encode(['tipo' => 'error','mensaje' => 'Ingrese contenido para la sección de terminos y condiciones.'], JSON_FORCE_OBJECT);

		$termino = ['titulo' => $titulo, 'body' => $body];
		$data = [
			'seccion' => 'terminos-y-condiciones',
			'tipo' => 'seccion',
			'info' => json_encode($termino, JSON_FORCE_OBJECT)
		];
		$model->save($data);
		return json_encode(['tipo' => 'success', 'mensaje' => 'La sección se ha creado de manera correcta.'], JSON_FORCE_OBJECT);
	}
	// Funcion para vista de medios de contacto por parte del administrador
	public function mediosContacto(){
		if(!session('is_logged')) return redirect()->route('');
		if(session('rol') != 'admin') return redirect()->route('');
		
		return view(administrador('medios-de-contacto'));
	}
	// Funcion para actualizar los medios de contacto
	public function updateContact(){
		$secciones = model('ModelSecciones');
		// recibe todos los medios de contacto
		$redes_sociales_info = array();
		$redes_sociales['facebook'] = trim($this->request->getPost('facebook'));
		$redes_sociales['chat'] = trim($this->request->getPost('chat'));
		$redes_sociales['instagram'] = trim($this->request->getPost('instagram'));
		$redes_sociales['youtube'] = trim($this->request->getPost('youtube'));
		$redes_sociales['linkedin'] = trim($this->request->getPost('linkedin'));
		$redes_sociales['correo'] = trim($this->request->getPost('correo'));

		array_push($redes_sociales_info, $redes_sociales);
		$redes_socialesBD = $secciones->where(['seccion' => 'redes-sociales'])->first();

		$secciones->save(['id' => $redes_socialesBD['id'], 'seccion' => 'redes-sociales', 'info' => json_encode($redes_sociales_info, JSON_FORCE_OBJECT)]);
		return json_encode(['tipo' => 'success', 'mensaje' => 'Medios de contacto actualizados.'], JSON_FORCE_OBJECT);
	}
	// Funcion para actualzar algun valor dentro de la tabla de configuracion
	public function actualizarIngreso(){
		$configuraciones = model('ModelConfiguracion');
		$red_social = $this->request->getPost('red_social');
		$id = trim($this->request->getPost('id'));
		$secreta = trim($this->request->getPost('secreta'));
		if($red_social == 'Facebook'){
			if(empty($id)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el ID de la App por parte de Meta for Developers.'], JSON_FORCE_OBJECT);
			if(empty($secreta)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el identificador secreto de la App por parte de Meta for Developers.'], JSON_FORCE_OBJECT);
			$data = ['app_id' => $id, 'app_secret' => $secreta];
		}elseif($red_social == 'Google'){
			if(empty($id)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el Cliente ID para el logueo y registro por parte de Gmail.'], JSON_FORCE_OBJECT);
			if(empty($secreta)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el identificador Secreto de Cliente por parte de Gmail'], JSON_FORCE_OBJECT);
			$data = ['clienteID' => $id, 'clienteSecret' => $secreta];   
		}elseif($red_social == 'Recaptcha'){
			if(empty($id)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la clave de Sitio Web de reCAPTCHA.'], JSON_FORCE_OBJECT);
			if(empty($secreta)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la clave secretra de reCAPTCHA'], JSON_FORCE_OBJECT);
			$data = ['clave sitio' => $id, 'clave secreta' => $secreta];   
		}
		$red_social_existe = $configuraciones->where('clave', $red_social)->first();
		if(empty($red_social_existe)) $configuraciones->save(['clave' => $red_social, 'valor' => json_encode($data, JSON_FORCE_OBJECT)]);
		else $configuraciones->where('clave', $red_social)->set(['valor' => json_encode($data, JSON_FORCE_OBJECT)])->update();
		
		return json_encode(['tipo' => 'success', 'mensaje' => 'Los datos para el registro con '.$red_social.' están actualizados.'], JSON_FORCE_OBJECT);
	}
	// Funcion para vista de metodos de pago
	public function apis(){
		if(!session('is_logged')) return redirect()->route('');
		if(session('rol') != 'admin') return redirect()->route('');

		$netpay = get_NetPay();
		return view(administrador('metodos-de-pago'), compact('netpay'));
	}
	// Funcion para guardar y actualizar informacion de NETPAY
	public function updateNetPay(){
		$netPay['modo'] = trim($this->request->getPost('modo'));
		if(empty($netPay['modo'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Seleccione el modo de funcionamiento de NetPay.'], JSON_FORCE_OBJECT);
		
		$netPayInfo = array();
		// NETPAY LOOP
		$netPay['publica'] = trim($this->request->getPost('publica'));
		$netPay['privada'] = trim($this->request->getPost('privada'));
		$netPay['path_prueba'] = trim($this->request->getPost('path_prueba'));
		$netPay['publicaProduccion'] = trim($this->request->getPost('publica_produccion'));
		$netPay['privadaProduccion'] = trim($this->request->getPost('privada_produccion'));
		$netPay['path_produccion'] = trim($this->request->getPost('path_produccion'));
		
		// NETPAY CASH
		$netPay['publicaCash'] = trim($this->request->getPost('publicaCheckout'));
		$netPay['privadaCash'] = trim($this->request->getPost('privadaCheckout'));
		$netPay['path_pruebaCash'] = trim($this->request->getPost('path_pruebaCheckout'));
		$netPay['publicaProduccionCash'] = trim($this->request->getPost('publica_produccionCheckout'));
		$netPay['privadaProduccionCash'] = trim($this->request->getPost('privada_produccionCheckout'));
		$netPay['path_produccionCash'] = trim($this->request->getPost('path_produccionCheckout'));
		if($netPay['modo'] == 'prueba'){
			if(empty($netPay['publica'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese su llave pública de pruebas LOOP'], JSON_FORCE_OBJECT);
			if(empty($netPay['privada'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese su llave privada de pruebas LOOP'], JSON_FORCE_OBJECT);
			if(empty($netPay['path_prueba'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la URL de pruebas NetPay LOOP desde <a href="https://docs.netpay.com.mx/reference/obtener-llaves-de-api-netpay" target="_blank">aquí</a>'], JSON_FORCE_OBJECT);
			if(empty($netPay['publicaCash'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese su llave pública de pruebas para NETPAY CASH'], JSON_FORCE_OBJECT);
			if(empty($netPay['privadaCash'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese su llave privada de pruebas para NETPAY CASH'], JSON_FORCE_OBJECT);
			if(empty($netPay['path_pruebaCash'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la URL de pruebas NetPay Cash desde <a href="https://docs.netpay.com.mx/reference/obtener-llaves-de-api-netpay" target="_blank">aquí</a>'], JSON_FORCE_OBJECT);
		}elseif($netPay['modo'] == 'produccion'){
			if(empty($netPay['publicaProduccion'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese su llave pública de producción LOOP'], JSON_FORCE_OBJECT);
			if(empty($netPay['privadaProduccion'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese su llave privada de producción LOOP'], JSON_FORCE_OBJECT);
			if(empty($netPay['path_produccion'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la URL de producción NetPay LOOP desde <a href="https://docs.netpay.com.mx/reference/obtener-llaves-de-api-netpay" target="_blank">aquí</a>'], JSON_FORCE_OBJECT);
			if(empty($netPay['publicaProduccionCash'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese su llave pública de producción para NETPAY CASH.'], JSON_FORCE_OBJECT);
			if(empty($netPay['privadaProduccionCash'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese su llave privada de producción para NETPAY CASH.'], JSON_FORCE_OBJECT);
			if(empty($netPay['path_produccionCash'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la URL de producción NetPay Cash desde <a href="https://docs.netpay.com.mx/reference/obtener-llaves-de-api-netpay" target="_blank">aquí</a>'], JSON_FORCE_OBJECT);
		}
		$netPayApis = get_netPay();
		array_push($netPayInfo, $netPay);
		model('ModelConfiguracion')->save(['id' => isset($netPayApis[1]) ? $netPayApis[1] : '', 'clave' => 'netpay', 'valor' => json_encode($netPayInfo, JSON_FORCE_OBJECT)]);
		return json_encode(['tipo' => 'correcto', 'mensaje' => 'Los datos han sido actualizados.'], JSON_FORCE_OBJECT);
	}
	// Funcion para ver vista de membresias
	public function membresias(){
		if(!session('is_logged')) return redirect()->route('');
		if(session('rol') != 'admin') return redirect()->route('');
		
		$planes = model('ModelPlanes');
		$data = $planes->findAll();
		return view(administrador('membresias'), array('planes' => $data));
	}
	// Funcion para traer datos de membresia y actualizar
	public function editMember(){
		if(!session('is_logged')) return 'Sin permisos necesarios.';
		if(session('rol') != 'admin') return 'Sin permisos necesarios.';
		
		$planes = model('ModelPlanes');
		$id = trim($this->request->getPost('id'));
		$data = $planes->where('id', $id)->first();
		if($data['cuenta'] == 'freelancer') return view(administrador('viewcells/editar-membresia'), array('membresia' => $data));
		
		return view(administrador('viewcells/editar-membresia-contratista'), array('membresia' => $data));
	}
	// Funcion para actualizar membresia
	public function updateMember(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Sin permisos necesarios.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'Sin permisos necesarios.'], JSON_FORCE_OBJECT);
		// Llamamos a los modelos
		$planes = model('ModelPlanes');
		$model = model('ModelConfiguracion');
		$idplan = trim($this->request->getPost('id'));
		if(empty($idplan)) return json_encode(['tipo' => 'error', 'mensaje' => 'Se ha bloqueado el identificador de la membresía.'], JSON_FORCE_OBJECT);
		
		$name = trim($this->request->getPost('nombre'));
		if(empty($name)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un título para la membresía.'], JSON_FORCE_OBJECT);
		
		$descripcion = trim($this->request->getPost('descripcion'));
		if(empty($descripcion)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese una descripción para la membresía.'], JSON_FORCE_OBJECT);
		
		$planBD = $planes->where('id', $idplan)->first();
		
		if($planBD['monto'] != 0) {
			$monto = trim($this->request->getPost('costo'));
			if(empty($monto)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese monto mensual para la membresía.'], JSON_FORCE_OBJECT);

			$monto_anual = trim($this->request->getPost('costo_anual'));
			if(empty($monto_anual)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese monto anual para la membresía.'], JSON_FORCE_OBJECT);

			$netpay = new NetPay();
			$nombre_plan = $name.' '.ucfirst($planBD['cuenta']);
			$identificador = str_replace(' ', '-', $name).'-'.strtotime(date('d-m-Y H:i:s'));

			$datos_actualizar = [
				'amount' => $monto,
				'currency' => 'MXN',
				'frecuency' => 1,
				'interval' => 'MONTHLY',
				'name' => $nombre_plan,
				'trialDays' => 0,
				'expiryCount' => 0,
				'variableAmount' => true,
				'dayStartPayment' => 1,
				'identifier' => $identificador.'-M'
			];

			$plan_actualizadoMensual = $netpay->actualizarPlan($planBD['id_plan_mensual'], $datos_actualizar);
			if(!isset($plan_actualizadoMensual['id'])) return json_encode(['tipo' => 'error', 'mensaje' => 'No se pudo actualizar la membresía MENSUAL: '.$plan_actualizadoMensual['message']], JSON_FORCE_OBJECT);

			// SE CAMBIAN DATOS PARA PLAN ANUAL
			$datos_actualizar['name'] .= ' Anual';
			$datos_actualizar['amount'] = $monto_anual;
			$datos_actualizar['interval'] = 'YEARLY';
			$datos_actualizar['identifier'] = $identificador.'-Y';

			$plan_actualizadoAnual = $netpay->actualizarPlan($planBD['id_plan_anual'], $datos_actualizar);
			if(!isset($plan_actualizadoAnual['id'])) return json_encode(['tipo' => 'error', 'mensaje' => 'No se pudo actualizar la membresía ANUAL: '.$plan_actualizadoAnual['message']], JSON_FORCE_OBJECT);
		}

		// SE OBTIENEN CONFIGURACIONES DE FREELANCER Y CONTRATANTE
		$panel = $this->request->getPost('paneladministracion');
		(empty($panel)) ? $paneladministracion = false : $paneladministracion = true;
		
		$valoracion = $this->request->getPost('valoraciontrabajos');
		(empty($valoracion)) ? $valoraciontrabajos = false : $valoraciontrabajos = true;
		
		$historial = $this->request->getPost('historialconversaciones');
		(empty($historial)) ? $salas_chat = false : $salas_chat = true;

		if($planBD['cuenta'] == 'freelancer'){
			$cv = $this->request->getPost('crearcv');
			(empty($cv)) ? $crearcv = false : $crearcv = true;

			$oportunidades = $this->request->getPost('veroportunidades');
			(empty($oportunidades)) ? $veroportunidades = false : $veroportunidades = true;

			$postuporsemana = sanitizar($this->request->getPost('postuporsemana'));
			
			$portafolio = $this->request->getPost('anadirportafolio');
			(empty($portafolio)) ? $anadirportafolio = false : $anadirportafolio = true;
			
			$solicitud = $this->request->getPost('solicitudtrabajo');
			(empty($solicitud)) ? $solicitudtrabajo = false : $solicitudtrabajo = true;

			$freelancer_destacado = $this->request->getPost('freelancer_destacado');
			(empty($freelancer_destacado)) ? $destacado = false : $destacado = true;
			
			$actualizar = [
				'crearcv' => $crearcv,
				'veroportunidades' => $veroportunidades,
				'paneladministracion' => $paneladministracion,
				'postuporsemana' => $postuporsemana,
				'anadirportafolio' => $anadirportafolio,
				'solicitudtrabajo' => $solicitudtrabajo,
				'historialconversaciones' => $salas_chat,
				'freelancer_destacado' => $destacado
			];
		}
		else{
			$buscadorfreelancer = $this->request->getPost('buscadorfreelancer');
			if(empty($buscadorfreelancer)) return json_encode(['tipo' => 'error', 'mensaje' => 'Seleccione las preferencias del buscador para el plan'], JSON_FORCE_OBJECT);
			
			$accesofreelancernivel = $this->request->getPost('accesofreelancernivel');
			if(empty($accesofreelancernivel)) return json_encode(['tipo' => 'error', 'mensaje' => 'Seleccione los tipos de perfil al que podrá tener acceso el perfil.'], JSON_FORCE_OBJECT);
			
			$perfil = $this->request->getpost('perfilcontratante');
			(empty($perfil)) ? $perfilcontratante = false : $perfilcontratante = true;
			
			$acceso = $this->request->getPost('accesofreelancer');
			(empty($acceso)) ? $accesofreelancer = false :  $accesofreelancer = true;

			$contacto = $this->request->getPost('contactarFreelancer');
			(empty($contacto)) ? $contacto_freelancer = false :  $contacto_freelancer = true;
			
			$enviosolicitudes = sanitizar($this->request->getPost('enviosolicitudes'));
			$proyectosActivos = sanitizar($this->request->getPost('proyectosActivos'));
			$historialtrabajos = sanitizar($this->request->getPost('historialtrabajos'));
			$proyectosDestacados = sanitizar($this->request->getPost('proyectosDestacados'));
			
			$actualizar = [
				'perfilcontratante' => $perfilcontratante,
				'paneladministracion' => $paneladministracion,
				'valoraciontrabajos' => $valoraciontrabajos,
				'accesofreelancer' => $accesofreelancer,
				'buscadorfreelancer' => $buscadorfreelancer,
				'accesofreelancernivel' => $accesofreelancernivel,
				'enviosolicitudes' => $enviosolicitudes,
				'proyectosActivos' => $proyectosActivos,
				'historialtrabajos' => $historialtrabajos,
				'historialconversaciones' => $salas_chat,
				'proyectosDestacados' => $proyectosDestacados,
				'proyectosDestacados' => $contacto_freelancer
			];
		}
		
		$planes->save(['id' => $idplan, 'name' => $name, 'monto' => isset($monto)? $monto : 0, 'monto_anual' => isset($monto_anual) ? $monto_anual : 0, 'descripcion' => $descripcion]);

		$configuraciones_planes = $model->where('clave', 'plan'.$idplan)->first();

		if(empty($configuraciones_planes)) $model->insert(['clave' => 'plan'.$idplan, 'valor' => json_encode($actualizar, JSON_FORCE_OBJECT)]);
		else $model->where('clave', 'plan'.$idplan)->set(['valor' => json_encode($actualizar, JSON_FORCE_OBJECT)])->update();

		return json_encode(['tipo' => 'correcto', 'mensaje' => 'Membresía actualizada'], JSON_FORCE_OBJECT);
	}
	// Funcion para crear nueva membresia
	public function createMembership(){
		if(!session('is_logged')) return json_encode(['tipo' => 'falla', 'mensaje' => 'Sin permisos necesarios'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'falla', 'mensaje' => 'Sin permisos necesarios'], JSON_FORCE_OBJECT);

		$nombre = trim($this->request->getPost('nombre'));
		if(empty($nombre)) return json_encode(['tipo' => 'falla', 'mensaje' => 'Ingrese un nombre para la membresía'], JSON_FORCE_OBJECT);

		$tipo = sanitizar($this->request->getPost('perfil'));
		if(empty($tipo)) return json_encode(['alerta' => 'falla', 'mensaje' => 'Seleccione el tipo de usuario para la membresía'], JSON_FORCE_OBJECT);
		
		$descripcion = sanitizar($this->request->getPost('descripcion'));
		if(empty($descripcion)) return json_encode(['alerta' => 'falla', 'mensaje' => 'Ingrese una descripción para la membresía'], JSON_FORCE_OBJECT);

		$gratuito = $this->request->getPost('gratuito');
		if(!empty($gratuito)){
			$data = [
				'name' => $nombre,
				'descripcion' => $descripcion,
				'monto' => 0,
				'monto_anual' => 0,
				'trial_days' => '0',
				'currency' => 'MXN',
				'cuenta' => $tipo
			];
			model('ModelPlanes')->save($data);
			return json_encode(['alerta' => 'correcto', 'mensaje' => 'Membresía creada'], JSON_FORCE_OBJECT);
		}
		
		$costo = trim($this->request->getPost('costo'));
		if(empty($costo)) return json_encode(['alerta' => 'falla', 'mensaje' => 'Ingrese un costo mensual para la membresía.'], JSON_FORCE_OBJECT);

		$costo_anual = trim($this->request->getPost('costo_anual'));
		if(empty($costo_anual)) return json_encode(['alerta' => 'falla', 'mensaje' => 'Ingrese un costo anual para la membresía.'], JSON_FORCE_OBJECT);

		// PLAN MENSUAL
		$identificador = str_replace(' ', '-', $nombre).'-'.strtotime(date('d-m-Y H:i:s'));
		$datos_plan = [
			'amount' => $costo,
			'currency' => 'MXN',
			'frecuency' => 1,
			'interval' => 'MONTHLY',
			'name' => $nombre.' '.ucfirst($tipo),
			'trialDays' => 0,
			'expiryCount' => 0,
			'variableAmount' => true,
			'dayStartPayment' => 1,
			'selfRetries' => false,
			'selfRenewal' => false,
			'terminalView' => false,
			'identifier' => $identificador.'-M'
		];

		$netpay = new NetPay();
		$plan_mensual = $netpay->crearPlan($datos_plan);
		if(isset($plan_mensual['message'])) return json_encode(['alerta' => 'falla', 'mensaje' => $plan_mensual['message'].' Mensual'], JSON_FORCE_OBJECT);

		// SE CAMBIAN DATOS PARA PLAN ANUAL
		$datos_plan['name'] .= ' Anual';
		$datos_plan['amount'] = $costo_anual;
		$datos_plan['interval'] = 'YEARLY';
		$datos_plan['identifier'] = $identificador.'-Y';

		$plan_anual = $netpay->crearPlan($datos_plan);
		if(isset($plan_anual['message'])) return json_encode(['alerta' => 'falla', 'mensaje' => $plan_anual['message'].' Anual'], JSON_FORCE_OBJECT);
		
		//SE GUARDA EN BASE DE DATOS
		$datos = [
			'name' => $nombre,
			'descripcion' => $descripcion,
			'monto' => $costo,
			'monto_anual' => $costo_anual,
			'trial_days' => '0',
			'currency' => 'MXN',
			'id_plan_mensual' => $plan_mensual['id'],
			'id_plan_anual' => $plan_anual['id'],
			'cuenta' => $tipo
		];

		$plan_guardado = model('ModelPlanes')->save($datos);
		if($plan_guardado) return json_encode(['alerta' => 'correcto', 'mensaje' => 'Membresía creada'], JSON_FORCE_OBJECT);

		return json_encode(['alerta' => 'falla', 'mensaje' => $gratuito], JSON_FORCE_OBJECT);
	}
	// Funcion para eliminar membresia
	public function elimiMembership(){
		$idplan = $this->request->getPost('id');
		$planes = model('ModelPlanes')->where('id', $idplan)->first();

		if(!empty($planes['id_plan_mensual']) || !empty($planes['id_plan_anual'])){
			$netpay = new Netpay();
			$plan_mensual_eliminado = $netpay->cancelarPlan($planes['id_plan_mensual']);
			if(!isset($plan_mensual_eliminado['id'])) return json_encode(['alerta' => 'error', 'mensaje', 'No se pudo eliminar la membresía MENSUAL, intente más tarde o comuníquese con soporte'], JSON_FORCE_OBJECT);
			
			$plan_anual_eliminado = $netpay->cancelarPlan($planes['id_plan_anual']);
			if(!isset($plan_anual_eliminado['id'])) return json_encode(['alerta' => 'error', 'mensaje', 'No se pudo eliminar la membresía ANUAL, intente más tarde o comuníquese con soporte'], JSON_FORCE_OBJECT);
		}
		//SE ELIMINA PLAN Y CONFIGURACION DE BASE DE DATOS
		model('ModelConfiguracion')->where('clave', 'plan'.$idplan)->delete();
		$planes_eliminados = model('ModelPlanes')->delete($idplan, true);

		if($planes_eliminados) return json_encode(['alerta' => 'correcto', 'mensaje', 'Membresía eliminado correctamente'], JSON_FORCE_OBJECT);

		return json_encode(['alerta' => 'error', 'mensaje', 'No se pudo eliminar el plan de la plataforma. Planes eliminados de NETPAY'], JSON_FORCE_OBJECT);
	}
	// Funcion para enviar correo 
	public function enviarContacto(){
		$usuario = ['nombre' => trim($this->request->getPost('nombre')),'telefono' => trim($this->request->getPost('telefono'))];
		$correo = trim($this->request->getPost('correo'));
		$mensaje = trim($this->request->getPost('mensaje'));

		$Correos = new Correos();
		if($Correos->enviar_Contacto($correo, $mensaje, $usuario)) return json_encode(['alerta' => 'correcto', 'mensaje' => 'Correo enviado. Responderemos lo mas pronto posible'], JSON_FORCE_OBJECT);
	}
	// Funcion para nueva noticia
	public function nuevaNoticia(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Sin permisos necesarios.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'Sin permisos necesarios.'], JSON_FORCE_OBJECT);

		$areas = model('Habilidades')->where('superior', '0')->find();
		return view('/backend/administrador/nueva-noticia', array('areas' => $areas));
	}
	// Funcion para guardar noticia
	public function saveNoticia(){
		$noticia['titulo'] = trim($this->request->getPost('title'));
		if($noticia['titulo'] == '') return json_encode([ 'tipo' => 'error', 'mensaje' => 'Ingrese el título para la nueva sección.' ], JSON_FORCE_OBJECT);
		
		$noticia['contenido'] = trim($this->request->getPost('body'));
		if($noticia['contenido'] == '') return json_encode([ 'tipo' => 'error', 'mensaje' => 'Ingrese el contenido para la nueva sección.'], JSON_FORCE_OBJECT);

		$imagen = trim($this->request->getPost('imagen'));
		if($imagen == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese una imágen para la noticia'], JSON_FORCE_OBJECT);
		
		$noticia['imagen'] = str_replace(' ', '+', $imagen);
		$noticia['autor'] = !empty(trim($this->request->getPost('autor'))) ? trim($this->request->getPost('autor')) : null;

		model('Noticias')->save($noticia);
		return json_encode([ 'tipo' => 'success', 'mensaje' => 'La noticia creada exitosamente.' ], JSON_FORCE_OBJECT);
	}
	// Funcion para actualizar una noticia
	public function updateNoticia(){
		$model = model('Noticias');
		$id = trim($this->request->getPost('id'));
		if($id == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se seleccionó ninguna noticia.'], JSON_FORCE_OBJECT);
		
		$notice = $model->where('id', $id)->first();
		if($notice == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar la noticia seleccionada.'], JSON_FORCE_OBJECT);
		
		$noticia['titulo'] = trim($this->request->getPost('title'));
		if($noticia['titulo'] == '') return json_encode([ 'tipo' => 'error', 'mensaje' => 'Ingrese el título para la nueva sección.' ], JSON_FORCE_OBJECT);
		
		$noticia['contenido'] = trim($this->request->getPost('body'));
		if($noticia['contenido'] == '') return json_encode([ 'tipo' => 'error', 'mensaje' => 'Ingrese el contenido para la nueva sección.'], JSON_FORCE_OBJECT);
		
		$imagen = trim($this->request->getPost('imagen'));
		if($imagen == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese una imágen para la noticia'], JSON_FORCE_OBJECT);
		
		$noticia['imagen'] = str_replace(' ', '+', $imagen);
		$noticia['autor'] = !empty(trim($this->request->getPost('autor'))) ? trim($this->request->getPost('autor')) : null;

		$model->where('id', $id)->set($noticia)->update();
		return json_encode(['tipo' => 'success', 'mensaje' => 'Noticia actualizada'], JSON_FORCE_OBJECT);
	}
	// Funcion para eliminar noticia
	public function eliminarNoticia(){
		$id = trim($this->request->getPost('id'));
		if($id == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se seleccionó ninguna noticia.'], JSON_FORCE_OBJECT);
		
		$noticia = model('Noticias')->where('id', $id)->first();
		if($noticia == null) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar la noticia.'], JSON_FORCE_OBJECT);

		model('Noticias')->eliminar($id);
		return json_encode(['tipo' => 'success', 'mensaje' => 'Noticia eliminada'], JSON_FORCE_OBJECT);
	}
	// Funcion para todas las noticias
	public function verNoticias(){
		if(!$_GET){
			if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Sin permisos necesarios.'], JSON_FORCE_OBJECT);
			if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'Sin permisos necesarios.'], JSON_FORCE_OBJECT);
			$noticias = model('Noticias')->findALL();
			return view('/backend/administrador/todas-las-noticias', array('noticias' => $noticias));
		}else{
			$busqueda = trim($_GET['search']);
			$db     = \Config\Database::connect();
			$query = $db->query("SELECT * FROM noticias WHERE (titulo LIKE '%".$busqueda."%' OR contenido LIKE '%".$busqueda."%')");
			$data = $query->getResultArray();
			return view('/backend/administrador/todas-las-noticias', array('noticias' => $data));
		}
	}
	// Funcion prara declinar aprobacion de usuario
	public function declinarUsuario(){
		$mensaje = trim($this->request->getPost('motivo'));
		$id = trim($this->request->getPost('id'));
		if($mensaje == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un mensaje para enviar al usuario.'], JSON_FORCE_OBJECT);
		if($id == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se seleccionó a ningun freelance.'], JSON_FORCE_OBJECT);
		
		$user = model('Autenticacion')->where('id', $id)->first();
		if($user == null) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar al freelance seleccionado.'], JSON_FORCE_OBJECT);
		if($user['aprobado'] == true) return json_encode(['tipo' => 'error', 'mensaje' => 'El usuario seleccionado ya ha sido aprobado.'], JSON_FORCE_OBJECT);
		
		model('Autenticacion')->where('id', $id)->set(['aprobado' => false])->update();
		$Correo = new Correos();
		if($Correo->perfilDeclinado($user, $mensaje)) return json_encode(['tipo' => 'success', 'mensaje' => 'Se le ha notificado al contratante que verifíque sus datos correctamente'], JSON_FORCE_OBJECT);
		return json_encode(['tipo' => 'error', 'mensaje' => 'Error al enviar el correo.'], JSON_FORCE_OBJECT);
	}
	// Funcion para editar noticia
	public function editNoticia(){
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'warning', 'mensaje' => 'Sin permisos necesarios.']);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'warning', 'mensaje' => 'Sin permisos necesarios.']);
		if(!$_GET) return redirect()->to('/administrar-noticias');
		if(empty($_GET['notice'])) return redirect()->to('/administrar-noticias');
		if(empty($_GET['cont'])) return redirect()->to('/administrar-noticias');
		$id = base64_decode($_GET['cont']);
		$noticia = model('Noticias')->where('id', $id)->first();
		if($noticia == null) return redirect()->to('/administrar-noticias')->with('alerta', ['tipo' => 'warning', 'mensaje' => 'No se logró encontrar la noticia a editar.']);
		return view('/backend/administrador/editar-noticia', array('noticia' => $noticia));
	}
	// Funcion para ver informacion de reporte
	public function verDatosReporte(){
		$id = trim($this->request->getPost('id'));
		if($id == '') return ' ';
		$reporte = model('Calificaciones')->where('id', $id)->first();
		if($reporte == null) return ' ';
		$freelance = model('Autenticacion')->get_id($reporte['id_freelancer']);
		$proyecto = model('Proyectos')->where('id', $reporte['id_proyecto'])->first();
		return view('/viewcells/ver-reporte', array('reporte' => $reporte, 'freelance' => $freelance, 'proyecto' => $proyecto));
	}
	// Funcion para validar reporte
	public function validarReporte(){
		$id = trim($this->request->getPost('id'));
		if($id == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el id del reporte.'], JSON_FORCE_OBJECT);
		$reporte =  model('Calificaciones')->where('id', $id)->first();
		if($reporte == null) return json_encode(['tipo' => 'error', 'mensaje' => 'El reporte no existe.'], JSON_FORCE_OBJECT);
		model('Calificaciones')->where('id', $id)->set(['tipo' => 'reporte-validado'])->update();
		return json_encode(['tipo' => 'success', 'mensaje' => 'Reporte validado.'], JSON_FORCE_OBJECT);
	}
	// Funcion para mostrar idiomas disponibles
	public function idiomas(){
		$data = file_get_contents("paises.json");
		$idiomas = array();
		$datos = json_decode($data, true);
		foreach($datos as $key => $d){
			if(empty($d['languages'])) continue;
			$dat = [
				'pais' => $d['translations']['spa']['common'],
				'identificador' => $d['cca3'],
				'bandera' => $d['flags']['svg'],
			];
			array_push($idiomas, $dat);
		}
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'warning', 'mensaje' => 'Sin permisos necesarios.']);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'warning', 'mensaje' => 'Sin permisos necesarios.']);
		return view('/backend/administrador/idiomas', array('idiomas' => $idiomas, 'lenguajes' => model('Idiomas')->findAll()));
	}
	// Funcion para vista de añadir nuevo idioma
	public function addIdioma(){
		if(!session('is_logged')) return redirect()->route('');
		if(session('rol') != 'admin') return redirect()->route('');
		return view('/backend/administrador/agregar-idiomas');
	}
	public function formCrearIdioma(){
		$data = file_get_contents("paises.json");
		$idiomas = array();
		$datos = json_decode($data, true);
		foreach($datos as $key => $d){
			if(empty($d['languages'])) continue;
			$dat = [
				'pais' => $d['translations']['spa']['common'],
				'identificador' => $d['cca3'],
				'bandera' => $d['flags']['svg'],
			];
			array_push($idiomas, $dat);
		}
		$datos = model('Idiomas')->findAll();
		foreach($idiomas as $key => $i){
			foreach($datos as $d){
				if($i['identificador'] == $d['identificador']){
					unset($idiomas[$key]);
				}
			}
		}
		return view('/backend/administrador/viewcells/formCrearIdioma', array('idiomas' => $idiomas));
	}
	// Funcion para ver tabla de idiomas
	public function tablaIdiomas(){
		$idiomas = model('Idiomas')->findAll();
		return view('backend/administrador/viewcells/tabla-idiomas', array('lenguajes' => $idiomas));
	}
	// Funcion para eliminar idioma
	public function eliminarIdioma(){
		$id = trim($this->request->getPost('id'));
		if(empty($id)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un id.'], JSON_FORCE_OBJECT);
		if(model('Idiomas')->delete($id)) return json_encode(['tipo' => 'success', 'mensaje' => 'Idioma eliminado.'], JSON_FORCE_OBJECT);
		return json_encode(['tipo' => 'error', 'mensaje' => 'No se pudo eliminar el idioma seleccionado.'], JSON_FORCE_OBJECT);
	}
	// Funcion para bandera del pais seleccionado
	public function verPais(){
		$pais = trim($this->request->getPost('pais'));
		$data = file_get_contents("paises.json");
		$datos = json_decode($data, true);
		$flag = '';
		foreach($datos as $key => $d){
			if($d['cca3'] == $pais){
				$flag = $d['flags']['svg'];
			}
		}
		return $flag;
	}
	// Funcion para guardar nuevo idioma
	public function nuevoIdioma(){
		if(trim($this->request->getPost('nombre') == '')) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un nombre para el idioma.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('bandera')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Seleccione una bandera para el idioma seleccionado.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('pais')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Seleccione una bandera para el idioma seleccionado.'], JSON_FORCE_OBJECT);
		$idioma = model('Idiomas')->where('identificador', trim($this->request->getPost('pais')))->first();
		if($idioma != null) return json_encode(['tipo' => 'error', 'mensaje' => 'El idioma ya ha sido activado.']);
		$data = [
			'identificador' => trim($this->request->getPost('pais')),
			'nombre' => trim($this->request->getPost('nombre')),
			'bandera' => trim($this->request->getPost('bandera'))
		];
		model('Idiomas')->save($data);
		return json_encode(['tipo' => 'success', 'mensaje' => 'Idioma agregado.'], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar cambios de idioma guardado
	public function saveEditIdioma(){
		if(trim($this->request->getPost('id')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el ID del idioma.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('nombre')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un nombre para el idioma.'], JSON_FORCE_OBJECT);
		if(model('Idiomas')->where('id', trim($this->request->getPost('id')))->set(['nombre' => trim($this->request->getPost('nombre'))])->update()) return json_encode(['tipo' => 'success', 'mensaje' => 'Idioma actualizado.'], JSON_FORCE_OBJECT);
		return  json_encode(['tipo' => 'error', 'mensaje' => 'No se pudo actualizar el idioma.'], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar sugerencia de idioma
	public function saveSolicitudIdioma(){
		if(trim($this->request->getPost('nombre')) == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'ingrese un nombre para el idioma'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('bandera')) == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Seleccione una bandera para el idioma.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('pais')) == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Seleccione una bandera para el idioma.'], JSON_FORCE_OBJECT);
		$contenido = [
			'identificador' => trim($this->request->getPost('pais')),
			'nombre' => trim($this->request->getPost('nombre')),
			'bandera' => trim($this->request->getPost('bandera'))
		];
		if(!session('logeado')) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Su sesión a expirado, ingrese de nuevo a su cuenta para poder realizar la siguiente acción.'], JSON_FORCE_OBJECT);
		$data = [
			'tipo' => 'idioma',
			'contenido' => json_encode($contenido, JSON_FORCE_OBJECT),
			'estatus' => 'espera',
			'id_usuario' => session('id')
		];
		model('Sugerencias')->save($data);
		$sol = model('Sugerencias')->orderBy('id', 'DESC')->first();
		$Correo = new Correos();
		$Correo->solicitudSugerencia($sol['id']);
		return json_encode(['tipo' => 'correcto', 'mensaje' => 'Su solicitud ha sido enviada, le notificaremos por correo cuando su solicitud haya sido validada.'], JSON_FORCE_OBJECT);
	}
	// Funcion para ver solicitudes de idioma
	public function solicitudesIdioma(){
		$sugerencias = model('Sugerencias')->where(['tipo' => 'idioma', 'estatus' => 'espera'])->find();
		return view('backend/administrador/viewcells/solicitudes-idioma', array('solicitudes' => $sugerencias));
	}
	// Funcion para añadir idioma de solicitud de sugerencia
	public function addIdiomaSolicitud(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('id')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el id de la solicitud'], JSON_FORCE_OBJECT);
		$solicitud = model('Sugerencias')->where('id', trim($this->request->getPost('id')))->first();
		if($solicitud == null) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar la solicitud seleccionada.'], JSON_FORCE_OBJECT);
		if($solicitud['tipo'] != 'idioma') return json_encode(['tipo' => 'error', 'mensaje' => 'El tipo de solicitud no es la adecuada para la solicitud requerida.'], JSON_FORCE_OBJECT);
		$data = json_decode($solicitud['contenido'], TRUE);
		$idioma = model('Idiomas')->where('identificador', $data['identificador'])->first();
		if($idioma != null) return json_encode(['tipo' => 'error', 'mensaje' => 'El idioma ya ha sido agregado.'], JSON_FORCE_OBJECT);
		model('Idiomas')->save($data);
		model('Sugerencias')->where('id', trim($this->request->getPost('id')))->delete();
		$noti = [
			'tipo' => 'idioma-agregado',
			'id_usuario' => $solicitud['id_usuario'],
			'id_sugerencia' => trim($this->request->getPost('id')),
			'estatus' => 'espera'
		];
		model('Notificaciones')->save($noti);
		return json_encode(['tipo' => 'success', 'mensaje' => 'Idioma agregado.'], JSON_FORCE_OBJECT); 
	}
	// Funcion para guardar notificacion para usuario de que el idioma ya ha sido agregado
	public function notiSoliIdioma(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('id')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el id de la solicitud'], JSON_FORCE_OBJECT);
		$solicitud = model('Sugerencias')->where('id', trim($this->request->getPost('id')))->first();
		if($solicitud == null) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar la solicitud seleccionada.'], JSON_FORCE_OBJECT);
		if($solicitud['tipo'] != 'idioma') return json_encode(['tipo' => 'error', 'mensaje' => 'El tipo de solicitud no es la adecuada para la solicitud requerida.'], JSON_FORCE_OBJECT);
		model('Sugerencias')->where('id', trim($this->request->getPost('id')))->delete();
		$noti = [
			'tipo' => 'idioma-ya-agregado',
			'id_usuario' => $solicitud['id_usuario'],
			'id_sugerencia' => trim($this->request->getPost('id')),
			'estatus' => 'espera'
		];
		model('Notificaciones')->save($noti);
		return json_encode(['tipo' => 'success', 'mensaje' => 'Se le ha notificado al usaurio que el idioma ha sido agregado.'], JSON_FORCE_OBJECT); 
	}
	// Funcion para guardar sugerencia de area o habilidad
	public function saveSolicitudArea(){
		if(trim($this->request->getPost('nombre')) == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Ingrese un nombre para la area/habilidad.'], JSON_FORCE_OBJECT);
		if(empty($this->request->getPost('tipo'))) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Seleccione si es un area o una habilidad.'], JSON_FORCE_OBJECT);
		$contenido = [
			'nombre' => trim($this->request->getPost('nombre')),
			'tipo' => $this->request->getPost('tipo'),
			'area' => $this->request->getPost('area')
		];
		if(!session('logeado')) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Su sesión a expirado, ingrese de nuevo a su cuenta para poder realizar la siguiente acción.'], JSON_FORCE_OBJECT);
		$data = [
			'tipo' => 'habilidad',
			'contenido' => json_encode($contenido, JSON_FORCE_OBJECT),
			'estatus' => 'espera',
			'id_usuario' => session('id')
		];
		model('Sugerencias')->save($data);
		$sol = model('Sugerencias')->orderBy('id', 'DESC')->first();
		$Correo = new Correos();
		$Correo->solicitudSugerencia($sol['id']);
		return json_encode(['tipo' => 'correcto', 'mensaje' => 'Su solicitud ha sido enviada, le notificaremos por correo cuando su solicitud haya sido validada.'], JSON_FORCE_OBJECT);
	}
	// Funcion para ver solicitudes de areas o habilidades
	public function solicitudAreaHabilidad(){
		$sugerencias = model('Sugerencias')->where(['tipo' => 'habilidad', 'estatus' => 'espera'])->find();
		return view('backend/administrador/viewcells/solicitudes-habilidad', array('solicitudes' => $sugerencias));
	}
	// Funcion para editar y guardar areas o habilidades sugeridas
	public function editSoliArea(){
		$soli = model('Sugerencias')->where('id', trim($this->request->getPost('id')))->first();
		if($soli == null) return '';
		$habilidades = model('Habilidades')->where('superior', '0')->find();
		return view('backend/administrador/viewcells/edit-soli', array('solicitud' => $soli, 'habilidades' => $habilidades));
	}
	// Funcion para declinar solicitud de area o habilidad
	public function declinarSoliArea(){
		$data = model('Sugerencias')->where('id', trim($this->request->getPost('id')))->first();
		$user = model('Autenticacion')->where('id', $data['id_usuario'])->first();
		$correo = new Correos();
		if(!$correo->sugerenciaDeclinada($user['correo'], $solicitud)) return exit();
		model('Sugerencias')->where('id', trim($this->request->getPost('id')))->delete();
	}
	// Funcion para guardar area de solicitud
	public function saveAreaSugerencia(){
		if(trim($this->request->getPost('nombre')) == '') return json_encode(['tipo' => 'error', 'Ingrese un nombre para el area / habilidad.'], JSON_FORCE_OBJECT);
		if($this->request->getPost('tipo') == null || $this->request->getPost('tipo') == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Seleccione si sera un area o habilidad.'], JSON_FORCE_OBJECT);
		$solicitud = model('Sugerencias')->where('id', $this->request->getPost('id'))->first();
		if($solicitud == null) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar la solicitud'], JSON_FORCE_OBJECT);
		if($this->request->getPost('tipo') == 'area'){
			$data = [
				'area' => trim($this->request->getPost('nombre')),
				'superior' => 0
			];
		}else{
			$data = [
				'area' => trim($this->request->getPost('nombre')),
				'superior' => $this->request->getPost('area')
			];
		}
		model('Habilidades')->save($data);
		$noti = [
			'tipo' => 'idioma-agregado',
			'id_usuario' => $solicitud['id_usuario'],
			'id_sugerencia' => trim($this->request->getPost('id')),
			'estatus' => 'espera'
		];
		
		model('Notificaciones')->save($noti);
		
		$user = model('Autenticacion')->where('id', $solicitud['id_usuario'])->first();
		
		$correo = new Correos();
		if(!$correo->sugerenciaAnadida($user['correo'], $solicitud)) json_encode(['tipo' => 'error', 'mensaje' => 'Nuestros servidores de correo se encuentran en mantenimiento.'], JSON_FORCE_OBJECT);
		model('Sugerencias')->where('id', $this->request->getPost('id'))->delete();
		return json_encode(['tipo' => 'success', 'mensaje' => 'Area/habilidad añadida'], JSON_FORCE_OBJECT);
	}
	public function notifiExistenteHabilidad(){
		$solicitud = model('Sugerencias')->where('id', $this->request->getPost('id'))->first();
		if($solicitud == null) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar la solicitud'], JSON_FORCE_OBJECT);
		
		$correo = new Correos();
		$user = model('Autenticacion')->where('id', $solicitud['id_usuario'])->first();
		if(!$correo->sugerenciaYaExistente($user['correo'], $solicitud)) json_encode(['tipo' => 'error', 'mensaje' => 'Nuestros servidores de correo se encuentran en mantenimiento.'], JSON_FORCE_OBJECT);
		model('Sugerencias')->where('id', $this->request->getPost('id'))->delete();
		return json_encode(['tipo' => 'success', 'mensaje' => 'Area/habilidad añadida'], JSON_FORCE_OBJECT);
	}
	// Cantidad de sugerencias en boton
	public function cantSolicitudesAreas(){
		$data = model('Sugerencias')->where('tipo', 'habilidad')->find();
		if($data == null) return 'Solicitudes (0)';
		return 'Solicitudes ('.count($data).')';
	}
	// Funcion para ver listado de pagos
	public function listPagos(){ //MODIFICAR A NETPAY PASARELA DE PAGOS
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);
		// $pagos = model('Pagos')
		// 		->select('pagos.id AS pagosID, pagos.id_usuario, usuarios.nombre, usuarios.apellidos, pagos.monto, pagos.tipo, pagos.estatus, pagos.fecha, usuarios.imagen')
		// 		->join('usuarios', 'usuarios.id = pagos.id_usuario')
		// 		->orderBy('pagosID', 'DESC')->findAll();
		$query = db_connect()->query("SELECT pagos.id AS pagosID, pagos.id_usuario, nombre_completo, pagos.monto, pagos.tipo, pagos.estatus, pagos.fecha, pagos.id_compra, usuarios.imagen, usuarios.correo FROM (SELECT *, CONCAT(nombre,' ',apellidos) AS nombre_completo 
			FROM usuarios) usuarios
			INNER JOIN pagos ON usuarios.id = pagos.id_usuario");
		$pagosBD = $query->getResultArray();

		$pagos['total'] = 0;
		$pagos['efectivo'] = 0;
		$pagos['tarjeta'] = 0;

		foreach($pagosBD as $p){
			if($p['estatus'] == 'Activo') $pagos['total'] += $p['monto'];
			if($p['tipo'] == 'tarjeta' && ($p['estatus'] == 'Activo')) $pagos['tarjeta'] += $p['monto'];
			if($p['tipo'] == 'efectivo' && ($p['estatus'] == 'Activo')) $pagos['efectivo'] += $p['monto'];
		}
		$db = \Config\Database::connect();
		$query = $db->query("SELECT * FROM pagos INNER JOIN usuarios ON usuarios.id = pagos.id_usuario WHERE pagos.fecha = (SELECT MIN(fecha) FROM pagos)");
		$minimo = $query->getRowArray();
		$query = $db->query("SELECT * FROM pagos INNER JOIN usuarios ON usuarios.id = pagos.id_usuario WHERE pagos.fecha = (SELECT MAX(fecha) FROM pagos)");
		$maximo = $query->getRowArray();
		$hoy = new Time('now', 'America/Monterrey', 'es_MX');
		$cupones = model('Cupones')->find();
		return view('/backend/administrador/listado-pagos', array('pagos' => $pagosBD, 'cupones' => $cupones, 'montos' => $pagos, 'minimo' => $minimo, 'maximo' => $maximo, 'hoy' => $hoy, 'pagos_totales' => count($pagosBD)));
	}

	public function filtrarPagos(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);
		
		$datos = $this->request->getRawInput();
		$where = "";
		$join_cupones = "";
		$columnas = "";
		
		if(!empty($datos['buscador'])){
			$where .= "AND (usuarios.correo LIKE '%".$datos['buscador']."%' ESCAPE '!' OR nombre_completo LIKE '%".$datos['buscador']."%' ESCAPE '!')";
			unset($datos['buscador']);
		}
		
		$cupon_filtrado = $datos['cupon'] != 'Buscar cupón' && !empty($datos['cupon']);
		if($cupon_filtrado){
			$columnas .= ", usos_cupones.id_compra, usos_cupones.id_cupon";
			$join_cupones .= "INNER JOIN usos_cupones ON pagos.id_compra = usos_cupones.id_compra";
			$where .= " AND id_cupon = :cupon:";
		}
		else unset($datos['cupon']);

		$estatusPago_filtrado = $datos['estatus'] != 'Estatus de pago' && !empty($datos['estatus']);
		if($estatusPago_filtrado) $where .= " AND pagos.estatus = :estatus:";
		else unset($datos['estatus']);
		
		if(isset($datos['inicio']) && !empty($datos['inicio'])) $datos['inicio'] .= ' 00:00:00';
		if(isset($datos['fin']) && !empty($datos['fin'])) $datos['fin'] .= ' 23:59:59';
		
		$consulta = "SELECT pagos.id AS pagosID, pagos.id_usuario, nombre_completo, pagos.monto, pagos.tipo, pagos.estatus, pagos.fecha, pagos.id_compra, usuarios.imagen, usuarios.correo $columnas FROM (SELECT *, CONCAT(nombre,' ',apellidos) AS nombre_completo 
		FROM usuarios) usuarios
		INNER JOIN pagos ON usuarios.id = pagos.id_usuario $join_cupones
		WHERE pagos.fecha BETWEEN CAST(:inicio: AS DATETIME) AND CAST(:fin: AS DATETIME) $where
		ORDER BY pagosID DESC";

		$datos['pagos'] = db_connect()->query($consulta, $datos)->getResultArray();
		$datos['pagos_totales'] = count($datos['pagos']);

		return view('/backend/administrador/viewcells/card-pago', $datos);
	}
	
	public function exportarPagos(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);
		
		$datos = $this->request->getRawInput();
		$where = "";
		$join_cupones = "";
		$columnas = "";
		
		if(!empty($datos['buscador'])){
			$where .= "AND (usuarios.correo LIKE '%".$datos['buscador']."%' ESCAPE '!' OR nombre_completo LIKE '%".$datos['buscador']."%' ESCAPE '!')";
			unset($datos['buscador']);
		}
		
		$cupon_filtrado = $datos['cupon'] != 'Buscar cupón' && !empty($datos['cupon']);
		if($cupon_filtrado){
			$columnas .= ", usos_cupones.id_compra, usos_cupones.id_cupon";
			$join_cupones .= "INNER JOIN usos_cupones ON pagos.id_compra = usos_cupones.id_compra";
			$where .= " AND id_cupon = :cupon:";
		}
		else unset($datos['cupon']);

		$estatusPago_filtrado = $datos['estatus'] != 'Estatus de pago' && !empty($datos['estatus']);
		if($estatusPago_filtrado) $where .= " AND pagos.estatus = :estatus:";
		else unset($datos['estatus']);
		
		if(isset($datos['inicio']) && !empty($datos['inicio'])) $datos['inicio'] .= ' 00:00:00';
		if(isset($datos['fin']) && !empty($datos['fin'])) $datos['fin'] .= ' 23:59:59';
		
		$consulta = "SELECT pagos.id AS pagosID, pagos.id_usuario, nombre_completo, pagos.monto, pagos.tipo, pagos.estatus, pagos.fecha, pagos.id_compra, pagos.id_membresia, usuarios.imagen, usuarios.correo $columnas FROM (SELECT *, CONCAT(nombre,' ',apellidos) AS nombre_completo 
		FROM usuarios) usuarios
		INNER JOIN pagos ON usuarios.id = pagos.id_usuario $join_cupones
		WHERE pagos.fecha BETWEEN CAST(:inicio: AS DATETIME) AND CAST(:fin: AS DATETIME) $where
		ORDER BY pagosID DESC";

		$pagos = db_connect()->query($consulta, $datos)->getResultArray();

		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename='.explode(' ', $datos['inicio'])[0].' - '.explode(' ', $datos['fin'])[0].'.csv');
		echo "\xEF\xBB\xBF";
		echo "ID Orden,Fecha de compra,Usuario,Correo,Telefono,Cuenta,Plan,Frecuencia,Precio,Estado,Tipo de pago,Cupon\n";
		
		$fp = fopen('php://output', 'w');
		fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF)); // Agrega la BOM para que se reconozca como UTF-8
		foreach($pagos as $pago){
			$usuario = model('Autenticacion')->get_id($pago['id_usuario']);
			$cupon_usado = model('UsosCupones')->where('usos_cupones.id_compra', $pago['id_compra'])->join('cupones', 'usos_cupones.id_cupon = cupones.id')->first();
			fputcsv($fp, [$pago['id_compra'],
			$pago['fecha'],
			$pago['nombre_completo'],
			$usuario['correo'],
			isset($usuario['contacto']['telefono']) ? str_replace(' ', '',$usuario['contacto']['telefono']) : '',
			$usuario['rol'],
			view_cell('App\Libraries\FuncionesSistema::datoPlanAdmin', ['planid' => $pago['id_membresia'], 'dato' => 'name']),
			view_cell('App\Libraries\FuncionesSistema::datoPlanAdmin', ['planid' => $pago['id_membresia'], 'dato' => 'frecuencia']),
			'$ '.$pago['monto'],
			$pago['estatus'] == 'Activo' ? "Pagado" : "Pendiente",
			$pago['tipo'],
			isset($cupon_usado['codigo']) ? $cupon_usado['codigo'] : '']);
		}
		
		fclose($fp);
		exit();
	}

	public function filtrarFacturas(){
		$inicio = $this->request->getPost('inicio');
		$fin = $this->request->getPost('fin');
		$fechas = [
			'inicio' => $inicio.' 00:00:00',
			'fin' => $fin.' 23:59:59'
		];
		$db = \Config\Database::connect();
		$query = $db->query("SELECT * FROM solicitudes_facturas WHERE created_at BETWEEN CAST('".$fechas['inicio']."' AS DATETIME) AND CAST('".$fechas['fin']."' AS DATETIME) ORDER BY id DESC");
		$facturas = $query->getResultArray();
		
		return json_encode(['html' => view(administrador('viewcells/card_resumen_facturas'), array('facturas' => $facturas)), 'cantidad_facturas' => count($facturas)], JSON_FORCE_OBJECT);
	}
	
	// Recarga de grafica de pagos con filtrado de pagos
	public function filtradoPagosGrafica(){
		if(!$_GET){
			$pagos = model('Pagos')->orderBy('id', 'DESC')->findAll();
		}else{
			$inicio = $_GET['inicio'];
			$fin = $_GET['fin'];
			$fechas = [
				'inicio' => $inicio.' 00:00:00',
				'fin' => $fin.' 23:59:59'
			];
			$db = \Config\Database::connect();
			$query = $db->query("SELECT * FROM pagos WHERE fecha BETWEEN CAST('".$fechas['inicio']."' AS DATETIME) AND CAST('".$fechas['fin']."' AS DATETIME)");
			$pagos = $query->getResultArray();
		}
		
		$montos = [
			'total' => 0,
			'tarjetas' => 0,
			'efectivo' => 0
		];
		foreach($pagos as $p){
			if($p['estatus'] == 'Activo'){
				if($p['estatus'] == 'Activo') $montos['total'] += $p['monto'];
				if($p['tipo'] == 'tarjeta') $montos['tarjetas'] += $p['monto'];
				if($p['tipo'] == 'tienda') $montos['efectivo'] += $p['monto'];
			}
		}
		$hoy = new Time('now', 'America/Monterrey', 'es_MX');
		return view('/backend/administrador/viewcells/grafico-pagos', array('montos' => $montos, 'hoy' => $hoy));
	}
	//
	public function filtradoEstadisticasRegistro(){
		if(!$_GET){
			$usuarios = model('Autenticacion')->findAll();
		}else{
			$inicio = $_GET['inicio'];
			$fin = $_GET['fin'];
			$fechas = [
				'inicio' => $inicio.' 00:00:00',
				'fin' => $fin.' 23:59:59'
			];
			$db = \Config\Database::connect();
			$query = $db->query("SELECT * FROM usuarios WHERE created_at BETWEEN CAST('".$fechas['inicio']."' AS DATETIME) AND CAST('".$fechas['fin']."' AS DATETIME)");
			$usuarios = $query->getResultArray();
		}
		$datos = [
			'correo' => 0,
			'facebook' => 0,
			'google' => 0
		];
		foreach($usuarios as $u){
			if($u['cuenta'] == 'predeterminado') $datos['correo']++;
			if($u['cuenta'] == 'google') $datos['google']++;
			if($u['cuenta'] == 'facebook') $datos['facebook']++;
		}
		$hoy = new Time('now', 'America/Monterrey', 'es_MX');
		return view('/backend/administrador/viewcells/grafico-registro', array('montos' => $datos, 'hoy' => $hoy));
	}
	//
	public function filtradoCantUsuariosGrafica(){
		if(!$_GET){
			$usuarios = model('Autenticacion')->findAll();
		}else{
			$inicio = $_GET['inicio'];
			$fin = $_GET['fin'];
			$fechas = [
				'inicio' => $inicio.' 00:00:00',
				'fin' => $fin.' 23:59:59'
			];
			$db = \Config\Database::connect();
			$query = $db->query("SELECT * FROM usuarios WHERE created_at BETWEEN CAST('".$fechas['inicio']."' AS DATETIME) AND CAST('".$fechas['fin']."' AS DATETIME)");
			$usuarios = $query->getResultArray();
		}
		$datos['contratante'] = 0;
		$datos['freelancer'] = 0;
		$datos['correo-freelancers'] = 0;
		$datos['google-freelancers'] = 0;
		$datos['facebook-freelancers'] = 0;
		$datos['correo-contratantes'] = 0;
		$datos['google-contratantes'] = 0;
		$datos['facebook-contratantes'] = 0;
		foreach($usuarios as $u){
			if($u['cuenta'] == 'predeterminado' && ($u['rol'] == 'freelancer')) $datos['correo-freelancers']++;
			if($u['cuenta'] == 'google' && ($u['rol'] == 'freelancer')) $datos['google-freelancers']++;
			if($u['cuenta'] == 'facebook' && ($u['rol'] == 'freelancer')) $datos['facebook-freelancers']++;
			if($u['cuenta'] == 'predeterminado' && ($u['rol'] == 'contratante')) $datos['correo-contratantes']++;
			if($u['cuenta'] == 'google' && ($u['rol'] == 'contratante')) $datos['google-contratantes']++;
			if($u['cuenta'] == 'facebook' && ($u['rol'] == 'contratante')) $datos['facebook-contratantes']++;
			if($u['rol'] == 'contratante') $datos['contratante']++;
			if($u['rol'] == 'freelancer') $datos['freelancer']++;
		}
		$hoy = new Time('now', 'America/Monterrey', 'es_MX');
		return view('/backend/administrador/viewcells/grafico-tipo-usuario', array('montos' => $datos, 'hoy' => $hoy));
	}
	// Funcion para ver informacion de pago en pop up
	public function verInfoPagoUsuario(){
		$id = sanitizar($this->request->getPost('id'));
		if(empty($id)) return '<div class="text-center">Seleccione una orden de pago valida.</div>';

		$pago = model('Pagos')->where('id', $id)->first();
		if(empty($pago)) return '<div class="text-center">El pago no se logró encontrar en el sistema.</div>';

		$user = model('Autenticacion')->get_id($pago['id_usuario']);
		
		$plan = model('ModelPlanes')->where('id_plan_mensual', $pago['id_membresia'])->first();
		$tipo = 'Mensual';
		if(empty($plan)){
			$plan = model('ModelPlanes')->where('id_plan_anual', $pago['id_membresia'])->first();
			!empty($plan) ? $tipo = 'Anual' : $tipo = '';
		}
		
		($tipo == 'Anual')? $vigencia = ucfirst(strftime('%A, %e de %B de %Y', strtotime($pago['fecha']."+ 1 year"))) : $vigencia = ucfirst(strftime('%A, %e de %B de %Y', strtotime($pago['fecha']."+ 1 month")));
		$cupon_usado = model('UsosCupones')
				->where(['usos_cupones.id_compra' => $pago['id_compra']])
				->join('cupones', 'cupones.id = usos_cupones.id_cupon')
				->orderBy('usos_cupones.id', 'DESC')->first();
		return view('/backend/administrador/viewcells/informacion-pago', compact('pago', 'user', 'tipo', 'vigencia', 'cupon_usado'));
	}
	// Funcion para ver listado de solicitudes de facturacion
	public function solicitudesFacturacion(){
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);

		$data = model('Facturas')->orderBy('id', 'DESC')->findAll();
		$fecha_minima = model('Facturas')->selectMin('created_at')->orderBy('id', 'DESC')->first()['created_at'];
		$fecha_minima = new Time($fecha_minima, 'America/Monterrey', 'es_MX');
		$hoy = new Time('now', 'America/Monterrey', 'es_MX');

		return view('/backend/administrador/solicitudes-facturacion', array('solicitudes' => $data, 'fecha_minima' => $fecha_minima, 'fecha_actual' => $hoy, 'cantidad_facturas' => count($data)));
	}
	// Funcion para ver estadisticas generales
	public function estadisticasGenerales(){
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);

		$db = db_connect();
		$query = $db->query("SELECT * from pagos WHERE fecha = (SELECT MIN(fecha)FROM pagos)");
		$minimo = $query->getRowArray();
		$query = $db->query("SELECT * from pagos WHERE fecha = (SELECT MAX(fecha)FROM pagos)");
		$maximo = $query->getRowArray();
		$hoy = new Time('now', 'America/Monterrey', 'es_MX');

		return view('/backend/administrador/estadisticas-generales', array('minimo' => $minimo, 'maximo' => $maximo, 'hoy' => $hoy));
	}
	// Funcion para ver datos de solicitud de factura
	public function verDatosFactura(){
		$id = trim($this->request->getPost('id'));
		if($id == '') return '<div class="text-center">No se ha logrado encontrar el ID seleccionado.</div>';
		
		$folio = trim($this->request->getPost('folio'));
		if(empty($folio)) return '<div class="text-center">No se ha logrado encontrar el folio seleccionado.</div>';
		
		$soli = model('Facturas')->where('id', $id)->first();
		if($soli == null) return '<div class="text-center">No se ha logrado encontrar la solicitud de pago.</div>';
		
		$pago = model('Pagos')->where('id_compra', $soli['id_compra'])->first();
		if($pago == null) return '<div class="text-center">No se logró encontrar la orden de compra.</div>';
		
		$user = model('Autenticacion')->get_id($soli['id_user']);
		return view('/backend/administrador/viewcells/informacion-factura', array('solicitud' => $soli, 'pago' => $pago, 'user' => $user, 'folio' => $folio));
	}
	//
	public function actualizarSolFactura(){
		$id = trim($this->request->getPost('solicitud'));
		if($id == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el ID de la solicitud.'], JSON_FORCE_OBJECT);
		
		$folio = trim($this->request->getPost('folio'));
		if(empty($id)) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el folio de la solicitud.'], JSON_FORCE_OBJECT);
		
		$plan = trim($this->request->getPost('plan'));
		if(empty($plan)) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el plan de esa suscripción.'], JSON_FORCE_OBJECT);
		
		$facturaBD = model('Facturas')->find($id);
		$usuarioBD = model('Autenticacion')->get_usuarioID($facturaBD['id_user']);
		
		//Se verifica si se sube al menos el primer archivo con información
		$archivos = $this->request->getFiles('facturacion');
		if(empty($archivos['facturacion'][0]->getSize())) return json_encode(['tipo' => 'error', 'mensaje' => 'Adjunta al menos un archivo PDF o XML'], JSON_FORCE_OBJECT);
		
		$directorio = WRITEPATH."uploads/Facturas";
		if(!file_exists($directorio)) mkdir($directorio);
		if(!file_exists($directorio."/$folio")) mkdir($directorio."/$folio");
		
		$facturas = [];
		foreach($archivos['facturacion'] as $archivo){
			switch($archivo->getExtension()){
				case 'pdf': $archivo->store("Facturas/".$folio."/", $archivo->getName()); break;
				case 'xml': $archivo->store("Facturas/".$folio."/", $archivo->getName()); break;
				default: return json_encode(['tipo' => 'error', 'mensaje' => 'Solo se permiten archivos XML ó PDF. Cambia el archivo "'.$archivo->getName().'" por otro permitido'], JSON_FORCE_OBJECT);
				break;
			}
			array_push($facturas, $archivo->getName());
		}
		
		model('Facturas')->save(['id'=> $id, 'estatus' => 'enviado', 'facturas' => json_encode($facturas)]);
		$datos['facturas'] = [
			'folio' => $folio,
			'archivo1' => $facturas[0],
			'archivo2' => $facturas[1]
		];
		$datos['usuario'] = $usuarioBD;
		$datos['plan'] = $plan;
		$correo = new Correos();
		$correo->enviarFacturacionUsuarios($usuarioBD['correo'], $datos);
		
		return json_encode(['tipo' => 'success', 'archivos' => json_encode($facturas, true)], JSON_FORCE_OBJECT);
	}
	
	public function reenviarFacturacion(){
		$id = trim($this->request->getPost('id'));
		if($id == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el ID de la solicitud.'], JSON_FORCE_OBJECT);
		
		$folio = trim($this->request->getPost('folio'));
		if(empty($folio)) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el folio de la solicitud.'], JSON_FORCE_OBJECT);
		
		$facturasBD = model('Facturas')->find($id);
		if(empty($facturasBD)) return json_encode(['tipo' => 'error', 'mensaje' => 'No se pudo encontrar la facturación'], JSON_FORCE_OBJECT);
		$usuarioBD = model('Autenticacion')->get_usuarioID($facturasBD['id_user']);
		
		$plan = trim($this->request->getPost('plan'));
		if(empty($plan)) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el plan de esa suscripción.'], JSON_FORCE_OBJECT);
		
		$archivos = json_decode($facturasBD['facturas']);
		$datos['facturas'] = [
			'folio' => $folio,
			'archivo1' => $archivos[0],
			'archivo2' => $archivos[1]
		];
		$datos['usuario'] = $usuarioBD;
		$datos['plan'] = $plan;
		$correo = new Correos();
		$correo->enviarFacturacionUsuarios($usuarioBD['correo'], $datos);
		
		return json_encode(['tipo' => 'success', 'mensaje' => 'Se ha reenviado los archivos correctamente al usuario y se le ha notificado.'], JSON_FORCE_OBJECT);
	}
	public function recargaFacturas(){
		$data = model('Facturas')->orderBy('id', 'DESC')->findAll();
		return view('/backend/administrador/viewcells/solicitudes-facturacion', array('solicitudes' => $data));
	}
	// Funcion para ver a los registrados del boletin
	public function contactoBoletin(){
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);

		$users = model('Boletines')->findAll();
		return view('/backend/administrador/listado-boletin', compact('users'));
	}
	// Funcion para crear base de datos de boletin y descargar
	public function descargarDBboletin(){
		$users = model('Boletines')->findAll();
		if($users == null) return json_encode(['tipo' => 'error', 'mensaje' => 'Aun no hay personas contactadas para exportar.'], JSON_FORCE_OBJECT);

		$fp = fopen('usuarios-boletin.csv', 'a+');
		// d($fp);
		$datos_totales = array();
		// while (($data = fgetcsv($fp) ) !== FALSE ) {
		//     $datos_totales[] = $data;
		// }
		
		foreach ($users as $campos) {
			while ($data = fgetcsv ($fp, 1000, ",")) array_push($datos_totales, $data);
			// if($x == 0) fputcsv($fp, $campos);
		}
		if(count($datos_totales) == 0){
			foreach ($users as $campos) {
				fputcsv($fp, $campos);
			}
		}else{
			foreach($users as $u){
				$x = 0;
				foreach($datos_totales as $d){
					if($u['id'] == $d[0]){
						$x = 1;
					}
				}
				if($x == 0) fputcsv($fp, $u);
			}
		}
		
		// dd($datos_totales);
		fclose($fp);
		return json_encode(['tipo' => 'success', 'mensaje' => 'Generando archivo, en un momento se descarga el archivo.'], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar datos de medios de contacto
	public function mediosSoporte(){
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);

		return view('/backend/administrador/contacto-soporte');
	}
	// Funcion para guardar medios de soporte
	public function guardarSoporte(){
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);
		
		($this->request->getPost('activarchat') != 1)? $activochat = false : $activochat = true;
		($this->request->getPost('activarcorreo') != 1)? $activocorreo = false : $activocorreo = true;
		
		if(trim($this->request->getPost('chat')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un telefono para el chat de soporte.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('correo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un correo para el area de soporte.'], JSON_FORCE_OBJECT);
		$data = [
			'chat' => trim($this->request->getPost('chat')),
			'activochat' => $activochat,
			'correo' => trim($this->request->getPost('correo')),
			'activocorreo' => $activocorreo
		];
		$datos = model('ModelSecciones')->where(['seccion' => 'redes-soporte'])->first();
		if($datos == null){
			$update = [
				'seccion' => 'redes-soporte',
				'tipo' => '',
				'info' => json_encode($data, JSON_FORCE_OBJECT)
			];
			model('ModelSecciones')->save($update);
		}else{
			model('ModelSecciones')->where(['seccion' => 'redes-soporte'])->set(['info' => json_encode($data, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Información de soporte actualizada.'], JSON_FORCE_OBJECT);
	}
	// 
	public function listadoProyectos(){
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);

		$proyectos = $this->listar_proyectos('todos');
		return view('/backend/administrador/ver-proyectos-listado', array('proyectos' => $proyectos, 'hoy' => date('Y-m-d')));
	}

	public function filtrarProyectos(){
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);

		$proyectos = $this->listar_proyectos();
		if(empty($proyectos)) return "<div class='alert alert-info'>No se encontraron coincidencias en los proyectos</div>";

		return view(administrador('viewcells/proyectos-filtrados'), array('proyectos' => $proyectos));
	}

	public function listar_proyectos(){
		$where = "";
		$fecha_inicio = trim($this->request->getPost('inicio'));
		$fecha_fin = trim($this->request->getPost('fin'));
		if(!empty($fecha_inicio) && !empty($fecha_fin)){
			$fechas = [
				'inicio' => $fecha_inicio.' 00:00:00',
				'fin' => $fecha_fin.' 23:59:59'
			];
			$where = "AND created_at BETWEEN CAST('".$fechas['inicio']."' AS DATETIME) AND CAST('".$fechas['fin']."' AS DATETIME)";
		}
		$estatus = sanitizar($this->request->getPost('estatus'));
		$estatus_condicion = "estatus = '$estatus'";
		if($estatus == 'todos' || (empty($estatus))) {
			$estatus_condicion = str_replace("= '$estatus'", "!= ''", $estatus_condicion);
		}
		$db = \Config\Database::connect();
		$query = $db->query("SELECT * FROM proyectos WHERE $estatus_condicion $where ORDER BY id DESC");
		$proyectos = $query->getResultArray();

		return $proyectos;
	}

	public function edicion_proyecto(){
		$id = sanitizar($this->request->getPost('proyecto'));
		if(empty($id)) return redirect()->back()->with('alerta', ['tipo' => 'error', 'mensaje' => 'No se encontró el proyecto, intenté nuevamente recargando la página']);

		$proyecto = model('Proyectos')->find($id);
		if(empty($proyecto)) return redirect()->back()->with('alerta', ['tipo' => 'error', 'mensaje' => 'No se encontró el proyecto, intenté nuevamente recargando la página']);

		$habilidades = model('Habilidades')->where('superior', '0')->orderBy('area', 'asc')->findAll();
		return view(administrador('Edicion_proyecto'), compact('proyecto', 'habilidades'));
	}

	public function editarProyecto(){
		$proyecto['id'] = sanitizar($this->request->getPost('proyecto'));
		if(empty($proyecto['id'])) return json_encode(['alerta' => 'error', 'mensaje' => 'No se encontró el proyecto, intenté nuevamente recargando la página'], JSON_FORCE_OBJECT);

		$proyecto['titulo'] = sanitizar($this->request->getPost('titulo'));
		if(empty($proyecto['titulo'])) return json_encode(['alerta' => 'warning', 'mensaje' => 'Ingrese un título'], JSON_FORCE_OBJECT);

		$proyecto['categoria'] = sanitizar($this->request->getPost('categorias'));
		if(empty($proyecto['categoria']) || $proyecto['categoria'] == 'NaN') return json_encode(['alerta' => 'warning', 'mensaje' => 'Seleccione un área'], JSON_FORCE_OBJECT);

		$proyecto['subcategoria'] = sanitizar($this->request->getPost('subcategorias'));
		if(empty($proyecto['subcategoria']) || $proyecto['subcategoria'] == 'NaN') return json_encode(['alerta' => 'warning', 'mensaje' => 'Seleccione una subárea'], JSON_FORCE_OBJECT);
		
		$proyecto['presupuesto'] = sanitizar($this->request->getPost('presupuesto'));
		if($proyecto['presupuesto'] == '') return json_encode(['alerta' => 'warning', 'mensaje' => 'Ingrese el presupuesto del proyecto'], JSON_FORCE_OBJECT);

		$proyecto['descripcion'] = sanitizar($this->request->getPost('descripcion_corta'));
		if(empty($proyecto['descripcion'])) return json_encode(['alerta' => 'warning', 'mensaje' => 'Ingrese una descripción breve'], JSON_FORCE_OBJECT);

		$proyecto['descripcion_larga'] = sanitizar($this->request->getPost('descripcion_larga'));
		if(empty($proyecto['descripcion_larga'])) return json_encode(['alerta' => 'warning', 'mensaje' => 'Ingrese una descripción completa'], JSON_FORCE_OBJECT);

		$guardado = model('Proyectos')->save($proyecto);
		if($guardado) return json_encode(['alerta' => 'success', 'mensaje' => 'Información actualizada'], JSON_FORCE_OBJECT);

		return json_encode(['alerta' => 'error', 'mensaje' => 'Error al intentar actualizar los datos, inténtelo nuevamente'], JSON_FORCE_OBJECT);
	}

	public function cambiarEstatusProyecto(){
		$id = sanitizar($this->request->getPost('proyecto'));
		if(empty($id)) return json_encode(['tipo' => 'warning', 'mensaje' => 'No se encontró el proyecto, intenté nuevamente recargando la página'], JSON_FORCE_OBJECT);
		
		$accion = sanitizar($this->request->getPost('accion'));
		if(empty($accion)) return json_encode(['tipo' => 'warning', 'mensaje' => 'No se pudo realizar la operación, intenté nuevamente recargando la página'], JSON_FORCE_OBJECT);
		
		$proyecto = model('Proyectos')->find($id);
		$mensaje = 'El proyecto "<span style="font-weight: bold">'.$proyecto['titulo'].'</span>" fue reactivado exitosamente.';
		if(isset($_POST['mensaje']) && ($_POST['mensaje'] != 'NaN')){
			$mensaje = sanitizar($this->request->getPost('mensaje'));
			if(empty($mensaje)) return json_encode(['tipo' => 'warning', 'mensaje' => 'Por favor, ingrese un motivo para notificar a los usuarios del cambio del proyecto'], JSON_FORCE_OBJECT);
		}
		
		$contratista = model('Autenticacion')->get_UsuarioID($proyecto['id_contratista']);
		$correos = [$contratista['correo']];
		$freelancer = model('Autenticacion')->get_UsuarioID($proyecto['id_freelancer']);
		if(!empty($freelancer)) array_push($correos, $freelancer['correo']);
		$estatus = $accion == 'suspencion' ? true  : false;
		model('Proyectos')->save(['id' => $id, 'deleted' => $estatus]);
		$correo = new Correos();
		if($correo->notificarEstatusProyecto($correos, $mensaje, $proyecto)) return json_encode(['tipo' => 'success', 'mensaje' => 'Se ha notificado a los usuarios correctamente.'], JSON_FORCE_OBJECT);
		
		return json_encode(['tipo' => 'error', 'mensaje' => 'No se pudo cambiar la operación en el proyecto, intenténlo nuevamente.'], JSON_FORCE_OBJECT);
	}
	// Funcion para preguntas frecuentes de tarifas
	public function pregFrecuentesTarifas(){
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);

		return view('/backend/administrador/preguntas-frecuentes-tarifas');
	}
	// Funcion para actualizar pregunta de tarifas
	public function actuaPregTarifas(){
		if(trim($this->request->getPost('id')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el identificador de la pregunta.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('preg')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la pregunta.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('resp')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la respuesta de la pregutna.'], JSON_FORCE_OBJECT);
		$data = model('ModelSecciones')->where(['seccion' => 'pregunta-tarifas', 'tipo' => trim($this->request->getPost('id'))])->first();
		$a = [
			'pregunta' => trim($this->request->getPost('preg')),
			'respuesta' => trim($this->request->getPost('resp'))
		];
		if($data == null){
			$datos = [
				'seccion' => 'pregunta-tarifas',
				'tipo' => trim($this->request->getPost('id')),
				'info' => json_encode($a, JSON_FORCE_OBJECT)
			];
			model('ModelSecciones')->save($datos);
		}else{
			model('ModelSecciones')->where(['id' => $data['id']])->set(['info' => json_encode($a, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Pregunta actualizada.'], JSON_FORCE_OBJECT);
	}
	// Funcion de vista para editar preguntas de soporte
	public function pregSoporte(){
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);

		return view('/backend/administrador/preguntas-frecuentes-soporte');
	}
	// Funcion para guardar o actualizar preguntas de soporte
	public function actuaPregSoporte(){
		if(trim($this->request->getPost('id')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el identificador de la pregunta.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('seccion')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar la sección ha actualizar.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('preg')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la pregunta.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('resp')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la respuesta de la pregunta.'], JSON_FORCE_OBJECT);
		$a = [
			'pregunta' => trim($this->request->getPost('preg')),
			'respuesta' => trim($this->request->getPost('resp'))
		];
		$data = model('ModelSecciones')->where(['seccion' => trim($this->request->getPost('seccion')), 'tipo' => trim($this->request->getPost('id'))])->first();
		if($data == null){
			$datos = [
				'seccion' => trim($this->request->getPost('seccion')),
				'tipo' => trim($this->request->getPost('id')),
				'info' => json_encode($a, JSON_FORCE_OBJECT)
			];
			model('ModelSecciones')->save($datos);
		}else{
			model('ModelSecciones')->where(['id' => $data['id']])->set(['info' => json_encode($a, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Pregunta actualizada.'], JSON_FORCE_OBJECT);
	}
	// Funcion para actualizar iconos en soporte
	public function saveIconoSoporte(){
		if(trim($this->request->getPost('id')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el identificador del ícono.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('icono')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el ícono para la sección de preguntas.'], JSON_FORCE_OBJECT);
		$data = model('ModelSecciones')->where(['seccion' => 'icono-soporte', 'tipo' => trim($this->request->getPost('id'))])->first();
		$a = [
			'icono' => trim($this->request->getPost('icono'))
		];
		if($data == null){
			$datos = [
				'seccion' => 'icono-soporte',
				'tipo' => trim($this->request->getPost('id')),
				'info' => json_encode($a, JSON_FORCE_OBJECT)
			];
			model('ModelSecciones')->save($datos);
		}else{
			model('ModelSecciones')->where(['id' => $data['id']])->set(['info' => json_encode($a, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Icono actualizado.'], JSON_FORCE_OBJECT);
	}
	// Funcion de vista para editar testimoniales
	public function editTestimonios(){
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su sesión a finalizado, ingrese de nuevo'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Su cuenta no tiene permisos para la siguiente acción'], JSON_FORCE_OBJECT);
		
		return view('/backend/administrador/editar-testimonios');
	}
	// Funcion para mostrar datos de testimonio
	public function mostrarDatosTestimonio(){
		// return trim($this->request->getPost('id'));
		if(trim($this->request->getPost('id')) == ''){
			return 'Error';
		}else{
			$data = model('ModelSecciones')->where(['seccion' => 'testimonio', 'tipo' => trim($this->request->getPost('id'))])->first();
			if($data == null){
				return json_encode(['nombre' => '', 'imagen' => '', 'contenido' => ''], JSON_FORCE_OBJECT);
			}else{
				// dd($data);
				return $data['info'];
			}
		}
	}
	// Funcion para guardar testimonios
	public function saveTestimonio(){
		if(trim($this->request->getPost('id')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el identificador del icono.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('nombre')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el nombre para el tetimonio.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('profesion')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la profesion para el usuario del testimonio.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('imagen')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese una imagen para el testimonio.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('contenido')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el comentario para el testimonio.'], JSON_FORCE_OBJECT);
		$a = [
			'nombre' => trim($this->request->getPost('nombre')),
			'profesion' => trim($this->request->getPost('profesion')),
			'imagen' => str_replace(' ', '+', trim($this->request->getPost('imagen'))),
			'contenido' => trim($this->request->getPost('contenido'))
		];
		$data = model('ModelSecciones')->where(['seccion' => 'testimonio', 'tipo' => trim($this->request->getPost('id'))])->first();
		if($data == null){
			$datos = [
				'seccion' => 'testimonio',
				'tipo' => trim($this->request->getPost('id')),
				'info' => json_encode($a, JSON_FORCE_OBJECT)
			];
			model('ModelSecciones')->save($datos);
		}else{
			model('ModelSecciones')->where(['id' => $data['id']])->set(['info' => json_encode($a, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Testimonio actualizado.'], JSON_FORCE_OBJECT);
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// FUNCIONES EXTRAS
	// Tratar de descargar un archivo
	public function descargarArchivoOpenpaySpei($id){ //MODIFICAR A NETPAY PASARELA DE PAGOS
		$remote_file_url = 'https://sandbox-dashboard.openpay.mx/spei-pdf/mzxnxfp89gsah3kmswfd/'.$id;     //origen
		$local_file = 'writable/uploads/pagos/'.$id.'.pdf';                         //destino
		$copy = copy($remote_file_url, $local_file);
		if ($copy) {
			echo "Archivo copiado correctamente!";
		} else {
			echo "Error! El archivo no se copió…";
		}
	}
	public function descargarArchivoOpenpayPaynet($reference, $id){ //MODIFICAR A NETPAY PASARELA DE PAGOS
		$remote_file_url = 'https://sandbox-dashboard.openpay.mx/paynet-pdf/mzxnxfp89gsah3kmswfd/'.$reference;     //origen
		$local_file = 'writable/uploads/pagos/'.$id.'.pdf';                        //destino
		$copy = copy($remote_file_url, $local_file);
		if ($copy) {
			echo "Archivo paynet copiado correctamente!";
		} else {
			echo "Error! El archivo paynet no se copió…";
		}
	}

	public function netpay(){
		dd(model('Suscripciones')->informacion_plan('SG5x_EJm2ZjC8==ofsJyrIgS0MYro8'));
	}
}