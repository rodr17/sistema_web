<?php namespace App\Controllers;

require_once __DIR__ . '/Facebook/vendor/autoload.php';
require_once __DIR__ . '/Google/vendor/autoload.php';
use App\Libraries\Correos;
use App\Controllers\BaseController;

class Autenticacion extends BaseController{
	protected $usuariosBD;
	public function __construct(){
		helper('text');
		$this->usuariosBD = model('Autenticacion');
	}

	public function ingresar(){
		$correo = sanitizar($this->request->getPost('correo'));
		$usuarioBD = $this->usuariosBD->where('correo', $correo)->withDeleted()->first();
		
		if(empty($usuarioBD)) return json_encode(['alerta' => 'custom', 'mensaje' => 'Veo que tu correo no está registrado, intenta de nuevo o registrate.. ¡es gratis!.'], JSON_FORCE_OBJECT);
		
		$useElimi = model('EliminadosAdmin')->where('id_usuario', $usuarioBD['id'])->first();
		if($useElimi != NULL) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'OPPS... por lo que veo tu cuenta ha sido eliminada por nuestro equipo de administración.'], JSON_FORCE_OBJECT);
		if(!empty($usuarioBD['deleted_at'])){
			$correo = new Correos();
			$link = $this->linkTemporal($usuarioBD['id']);
			$enviado = $correo->enviar_recuperacionCuenta($usuarioBD, $link);
			if($enviado) return json_encode(['alerta' => 'advertencia', 'mensaje' => 'Cuenta suspendida. Hemos enviado a tu correo como poder recuperar tu cuenta'], JSON_FORCE_OBJECT);
			
			return json_encode(['alerta' => 'advertencia', 'mensaje' => 'Inténtelo nuevamente'], JSON_FORCE_OBJECT);
		}

		$contrasena = sanitizar($this->request->getPost('contraseña'));
		if(!password_verify($contrasena, $usuarioBD['contrasena'])) return json_encode(['alerta' => 'custom', 'mensaje' => 'Lo siento tu contraseña no es la correcta intenta de nuevo'], JSON_FORCE_OBJECT);

		if($usuarioBD['estatus_registro'] == 'completado'){
			session()->set([
				'id' => $usuarioBD['id'],
				'usuario' => $usuarioBD['nombre'].' '.$usuarioBD['apellidos'],
				'correo' => $usuarioBD['correo'],
				'cuenta' => $usuarioBD['cuenta'],
				'rol' => $usuarioBD['rol'],
				'imagen' => $usuarioBD['imagen'],
				'logeado' => true
			]);
			$ult_conexion = conversorZonaHoraria(date('Y-m-d H:i:s'), app_timezone(), date_default_timezone_get());
			$this->usuariosBD->save(['id' => $usuarioBD['id'], 'ult_conexion' => $ult_conexion]);
			return json_encode(['alerta' => 'correcto', 'mensaje' => 'Bienvenido, '.$usuarioBD['nombre']]);
		}else{
			session()->set([
				'id' => $usuarioBD['id'],
				'usuario' => $usuarioBD['nombre'].' '.$usuarioBD['apellidos'],
				'correo' => $usuarioBD['correo'],
				'cuenta' => $usuarioBD['cuenta'],
				'rol' => $usuarioBD['rol'],
				'imagen' => $usuarioBD['imagen'],
				'plan' => $usuarioBD['plan'],
				'logeado' => true
			]);
			
			if(session('rol') == 'freelancer'){
				return json_encode(['alerta' => 'registro', 'mensaje' => base_url('Llenar_perfil')], JSON_FORCE_OBJECT);
			}else{
				return json_encode(['alerta' => 'registro', 'mensaje' => base_url('Completar_registro')], JSON_FORCE_OBJECT);
			}
		}
	}

	public function validacion_preRegistro(){
		$correo = sanitizar($this->request->getPost('correo'));
		if(!empty($this->usuariosBD->where('correo', $correo)->first())) return json_encode(['alerta' => 'custom', 'mensaje' => 'El correo ya existe'], JSON_FORCE_OBJECT);
		
		$validation =  \Config\Services::validation();//Se llama al servicio de validaciones
		$data = [//Datos tomados del formulario
			'correo' => $correo,
		];
		if(!$validation->run($data, 'correo')){//Reglas de validacion para el formulario de login
			return json_encode(['alerta' => 'custom', 'mensaje' => 'El correo no es valido'], JSON_FORCE_OBJECT);
		}
		
		$validar = view_cell('App\Libraries\FuncionesSistema::validarEmail', ['email' => $correo]);
		if(!$validar) return json_encode(['alerta' => 'custom', 'mensaje' => 'El correo no es válido'], JSON_FORCE_OBJECT);
		
		$contrasena = sanitizar($this->request->getPost('contraseña'));
		if($contrasena != sanitizar($this->request->getPost('confirm_contra'))) return json_encode(['alerta' => 'falla', 'mensaje' => 'No coinciden contraseñas'], JSON_FORCE_OBJECT);

		return json_encode(['alerta' => 'correcto'], JSON_FORCE_OBJECT);
	}

	public function registrar(){
		$correo = sanitizar($this->request->getPost('correo'));
		$contrasena = sanitizar($this->request->getPost('contraseña'));
		$membresia = sanitizar($this->request->getPost('membresia-seleccionada'));
		$datos = [
			'nombre' => sanitizar($this->request->getPost('nombre')),
			'apellidos' => sanitizar($this->request->getPost('apellidos')),
			'correo' => $correo,
			'contrasena' => password_hash($contrasena, PASSWORD_DEFAULT),
			'cuenta' => 'predeterminado',
			'rol' => session('pre-rol'),
			'plan' => !empty($membresia)? $membresia : null
		];
		$this->usuariosBD->save($datos);
		$usuarioRegistrado = $this->usuariosBD->where('correo', $correo)->first();
		
		// Creamos directorio del perfil y se copia foto perfil DEFAULT
		mkdir(WRITEPATH.'uploads/Usuarios/'.$usuarioRegistrado['id']."_".$usuarioRegistrado['correo']);
		$origen = WRITEPATH.'uploads/Usuarios/usuario-default.webp';
		$destino = WRITEPATH.'uploads/Usuarios/'.$usuarioRegistrado['id']."_".$usuarioRegistrado['correo'].'/usuario-default.webp';
		copy($origen, $destino);
		
		session()->set([
			'id' => $usuarioRegistrado['id'],
			'usuario' => $usuarioRegistrado['nombre'].' '.$usuarioRegistrado['apellidos'],
			'correo' => $usuarioRegistrado['correo'],
			'cuenta' => $usuarioRegistrado['cuenta'],
			'imagen' => $usuarioRegistrado['imagen'],
			'rol' => $usuarioRegistrado['rol'],
			'plan' => $membresia,
			'logeado' => true
		]);
		
		$Correo = new Correos();
		$Correo->registroFreelance($usuarioRegistrado);
		$Correo->alertaNuevoUsuario($usuarioRegistrado);
		$ult_conexion = conversorZonaHoraria(date('Y-m-d H:i:s'), app_timezone(), date_default_timezone_get());
		$this->usuariosBD->save(['id' => $usuarioRegistrado['id'], 'ult_conexion' => $ult_conexion]);
		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Registrado'], JSON_FORCE_OBJECT);
	}

	public function resetar_contrasena(){
		$correo = sanitizar($this->request->getPost('correo_resetar'));
		if(empty($correo)) return json_encode(['alerta' => 'custom', 'mensaje' => 'ALTO... Se te pasó ingresar tu correo.'], JSON_FORCE_OBJECT);
		
		$usuarioBD = $this->usuariosBD->where('correo', $correo)->first();

		if(empty($usuarioBD)) return json_encode(['alerta' => 'custom', 'mensaje' => 'Veo que no está registrado tu correo, valida que esté correcto o crea una cuenta... ¡es gratis!.'], JSON_FORCE_OBJECT);
		if(!empty($usuarioBD['autenticador'])) return json_encode(['alerta' => 'custom', 'mensaje' => 'Este correo lo registraste con '.$usuarioBD['cuenta'].', solo ingresa directo a tu cuenta utilizando este método.'], JSON_FORCE_OBJECT);

		$contraResetada = random_string('alnum', 8);
		$this->usuariosBD->save(['id' => $usuarioBD['id'], 'contrasena' => password_hash($contraResetada, PASSWORD_DEFAULT)]);
		$Correo = new Correos();
		if(!$Correo->enviar($usuarioBD, $contraResetada)) return json_encode(['alerta' => 'error', 'mensaje' => 'OPPS... No puedes recuperar tu contraseña en este momento, notifica a soporte este inconveniente para poder continuar.'], JSON_FORCE_OBJECT);

		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Enviado Correctamente'], JSON_FORCE_OBJECT);
	}

	public function cambiar_contraseña(){
		$contraseña_actual = sanitizar($this->request->getPost('contrasena_actual'));
		if(empty($contraseña_actual)) return json_encode(['alerta' => 'custom', 'mensaje' => 'ALTO... para cambiar de contraseña requiero la actual, ¿puedes intentar de nuevo?'], JSON_FORCE_OBJECT);
		$contraseña_nueva = sanitizar($this->request->getPost('contrasena_nueva'));
		if(empty($contraseña_nueva)) return json_encode(['alerta' => 'custom', 'mensaje' => 'HEEY... para cambiar la contraseña ocupó la nueva, que no se te olvide.'], JSON_FORCE_OBJECT);
		$confirmar_contraseña = sanitizar($this->request->getPost('confirmar_contrasena'));
		if(empty($confirmar_contraseña)) return json_encode(['alerta' => 'custom', 'mensaje' => 'HEEY... para cambiar la contraseña ocupo que confirmes la nueva, que no se te olvide.'], JSON_FORCE_OBJECT);

		$usuarioBD = $this->usuariosBD->get_UsuarioID(session('id'));
		
		if(!password_verify($contraseña_actual, $usuarioBD['contrasena'])) return json_encode(['alerta' => 'advertencia', 'mensaje' => 'OPPS... Valida tu contraseña por que no es correcta.'], JSON_FORCE_OBJECT);
		if($contraseña_nueva != $confirmar_contraseña) return json_encode(['alerta' => 'advertencia', 'mensaje' => 'HEEY... La nueva contraseña no coincide, vuelve a intentarlo.'], JSON_FORCE_OBJECT);

		$guardado = $this->usuariosBD->save(['id' => session('id'), 'contrasena' => password_hash($contraseña_nueva, PASSWORD_DEFAULT)]);
		if($guardado) return json_encode(['alerta' => 'correcto', 'mensaje' => 'Listo... ya actualicé los datos, yo me encargo de recordar todo.'], JSON_FORCE_OBJECT);
		else return json_encode(['alerta' => 'falla', 'mensaje' => 'Error Base de Datos, inténtalo nuevamente'], JSON_FORCE_OBJECT);
	}

	public function iniciar_sesionFacebook(){
		$fb = new \Facebook\Facebook([
			'app_id' => get_Facebook('app_id'),
			'app_secret' => get_Facebook('app_secret'),
			'default_graph_version' => 'v16.0'
		]);
		
		$helper = $fb->getRedirectLoginHelper();
		if(isset($_GET['state'])){
			$helper->getPersistentDataHandler()->set('state', $_GET['state']);
			session()->set(['facebookState' => $_GET['state']]);
		}

		try{
			$token = $helper->getAccessToken();
		}
		catch(\Facebook\Exceptions\FacebookResponseException $e){
			return redirect()->back()->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Graph Error: '.$e->getMessage()]);
		}
		catch(\Facebook\Exceptions\FacebookSDKException $e){
			return redirect()->back()->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Facebook SDK Error: '.$e->getMessage()]);
		}

		if (!isset($token)) {
			if(!$helper->getError()) return redirect()->back()->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Error, favor de comunicarse con soporte']);
			if($helper->getError() == 'access_denied') return redirect()->back()->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'No aceptaste los permisos de tu perfil']);
		}

		try{
			$response = $fb->get('me?fields=email,picture.width(150).height(150),first_name,last_name,permissions{permission}', $token->getValue());
		}
		catch(\Facebook\Exceptions\FacebookResponseException $e){
			return redirect()->back()->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Graph Error: '.$e->getMessage()]);
		}
		catch(\Facebook\Exceptions\FacebookSDKException $e){
			return redirect()->back()->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Facebook SDK Error: '.$e->getMessage()]);
		}

		$usuarioFacebook = $response->getGraphUser();
		// $nacimiento = (array) $usuarioFacebook->getBirthday();
		// $ciudad = json_decode($usuarioFacebook->getLocation(), true);
		// $direccion['ciudad'] = $ciudad['name'];
		// array_push($direccion_info, $direccion);
		// if(!empty($this->usuariosBD->where('correo', $usuarioFacebook->getEmail())->first())) return redirect()->back()->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'El correo utilizado en Facebook ya existe']);
		
		$usuarioBD = $this->usuariosBD->where('autenticador', $usuarioFacebook->getId())->withDeleted()->first();
		
		if(!empty($usuarioBD)){
			if(!empty($usuarioBD['deleted_at'])){
				$correo = new Correos();
				$link = $this->linkTemporal($usuarioBD['id']);
				$enviado = $correo->enviar_recuperacionCuenta($usuarioBD, $link);
				if($enviado) return redirect()->back()->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'Cuenta suspendida. Hemos enviado a tu correo como poder recuperar tu cuenta']);
				
				return redirect()->back()->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'Inténtelo nuevamente']);
			}

			$useElimi = model('EliminadosAdmin')->where('id_usuario', $usuarioBD['id'])->first();
			if($useElimi != NULL) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'OPPS... por lo que veo tu cuenta ha sido eliminada por nuestro equipo de administración.'], JSON_FORCE_OBJECT);

			session()->set([
				'id' => $usuarioBD['id'],
				'autenticador' => $usuarioBD['autenticador'],
				'usuario' => $usuarioBD['nombre'].' '.$usuarioBD['apellidos'],
				'correo' => $usuarioBD['correo'],
				'cuenta' => $usuarioBD['cuenta'],
				'imagen' => $usuarioBD['imagen'],
				'rol' => $usuarioBD['rol'],
				'plan' => $usuarioBD['plan'],
				'logeado' => true
			]);
			
			$ult_conexion = conversorZonaHoraria(date('Y-m-d H:i:s'), app_timezone(), date_default_timezone_get());
			$this->usuariosBD->save(['id' => $usuarioBD['id'], 'ult_conexion' => $ult_conexion]);

			if($usuarioBD['estatus_registro'] != 'completado') {
				if($usuarioBD['rol'] == 'freelancer') return redirect()->to('Llenar_perfil')->with('alerta', ['tipo' => 'correcto', 'mensaje' => 'Bienvenido, '.$usuarioBD['nombre']]);
				
				return redirect()->to('Completar_registro')->with('alerta', ['tipo' => 'correcto', 'mensaje' => 'Bienvenido, '.$usuarioBD['nombre']]);
			}
			
			return redirect()->to('usuarios/perfil')->with('alerta', ['tipo' => 'correcto', 'mensaje' => 'Bienvenido, '.$usuarioBD['nombre']]);
		}

		if(empty(session('pre-rol'))) return redirect()->back()->with('alerta', ['tipo' => 'custom', 'mensaje' => 'Por favor, registrarse como Freelancer o Contratante']);
		if(empty($usuarioFacebook->getEmail())) return redirect()->back()->with('alerta', ['tipo' => 'custom', 'mensaje' => 'OPPS... por lo que veo no funciona correctamente facebook, comunícate con soporte para brinda una mejor experiencia']);
		
		$datos = [
			'autenticador' => $usuarioFacebook->getId(),
			'nombre' => $usuarioFacebook->getFirstName(),
			'apellidos' => $usuarioFacebook->getLastName(),
			'correo' => $usuarioFacebook->getEmail(),
			'cuenta' => 'facebook',
			'rol' => session('pre-rol'),
			'imagen' => $usuarioFacebook->getPicture()->getUrl()
		];
		$this->usuariosBD->insert($datos);

		$usuarioBD = $this->usuariosBD->where('autenticador', $usuarioFacebook->getId())->first();
		mkdir(WRITEPATH.'uploads/Usuarios/'.$usuarioBD['id']."_".$usuarioBD['correo']);
		
		session()->set([
			'id' => $usuarioBD['id'],
			'autenticador' => $usuarioBD['autenticador'],
			'usuario' => $usuarioBD['nombre'].' '.$usuarioBD['apellidos'],
			'correo' => $usuarioBD['correo'],
			'cuenta' => $usuarioBD['cuenta'],
			'imagen' => $usuarioBD['imagen'],
			'rol' => $usuarioBD['rol'],
			'plan' => $usuarioBD['plan'],
			'logeado' => true
		]);
		
		
		$Correo = new Correos();
		$Correo->registroFreelance($usuarioBD);
		$Correo->alertaNuevoUsuario($usuarioBD);
		$ult_conexion = conversorZonaHoraria(date('Y-m-d H:i:s'), app_timezone(), date_default_timezone_get());
		$this->usuariosBD->save(['id' => $usuarioBD['id'], 'ult_conexion' => $ult_conexion]);
		if($usuarioBD['rol'] == 'freelancer') return redirect()->to('Llenar_perfil')->with('alerta', ['tipo' => 'correcto', 'mensaje' => 'Bienvenido, '.$datos['nombre']]);
		if($usuarioBD['rol'] == 'contratante') return redirect()->to('Completar_registro')->with('alerta', ['tipo' => 'correcto', 'mensaje' => 'Bienvenido, '.$datos['nombre']]);
	}

	public function iniciar_sesionGoogle(){
		$usuarioGoogle = new \Google_Client();
		$usuarioGoogle->setClientId(get_Google('clienteID'));
		$usuarioGoogle->setClientSecret(get_Google('clienteSecret'));
		$usuarioGoogle->setRedirectUri(base_url('logeoGoogle'));
		$usuarioGoogle->addScope("email");
		$usuarioGoogle->addScope("profile");

		if (!isset($_GET['code'])) return redirect()->back()->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Error de sesión']);

		$token = $usuarioGoogle->fetchAccessTokenWithAuthCode($_GET['code']);
		if(!isset($token['access_token'])) return redirect()->back();
		$usuarioGoogle->setAccessToken($token['access_token']);

		$auth = new \Google_Service_Oauth2($usuarioGoogle);
		$infoUsuario = $auth->userinfo->get();

		// if(!empty($this->usuariosBD->where('correo', $infoUsuario->id)->first())) return redirect()->back()->with('alerta', ['tipo' => 'falla', 'advertencia' => 'El correo utilizado en Google ya existe']);
		$usuarioBD = $this->usuariosBD->where('autenticador', $infoUsuario->id)->withDeleted()->first();

		if(!empty($usuarioBD)){
			if(!empty($usuarioBD['deleted_at'])){
				$correo = new Correos();
				$link = $this->linkTemporal($usuarioBD['id']);
				$enviado = $correo->enviar_recuperacionCuenta($usuarioBD, $link);
				if($enviado) return redirect()->back()->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'Cuenta suspendida. Hemos enviado a tu correo como poder recuperar tu cuenta']);
				
				return redirect()->back()->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'Inténtelo nuevamente']);
			}

			$useElimi = model('EliminadosAdmin')->where('id_usuario', $usuarioBD['id'])->first();
			if($useElimi != NULL) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'OPPS... por lo que veo tu cuenta ha sido eliminada por nuestro equipo de administración.'], JSON_FORCE_OBJECT);

			session()->set([
				'id' => $usuarioBD['id'],
				'autenticador' => $usuarioBD['autenticador'],
				'usuario' => $usuarioBD['nombre'].' '.$usuarioBD['apellidos'],
				'correo' => $usuarioBD['correo'],
				'cuenta' => $usuarioBD['cuenta'],
				'imagen' => $usuarioBD['imagen'],
				'rol' => $usuarioBD['rol'],
				'plan' => $usuarioBD['plan'],
				'logeado' => true
			]);

			$ult_conexion = conversorZonaHoraria(date('Y-m-d H:i:s'), app_timezone(), date_default_timezone_get());
			$this->usuariosBD->save(['id' => $usuarioBD['id'], 'ult_conexion' => $ult_conexion]);
			if($usuarioBD['estatus_registro'] != 'completado') {
				if($usuarioBD['rol'] == 'freelancer') return redirect()->to('Llenar_perfil')->with('alerta', ['tipo' => 'correcto', 'mensaje' => 'Bienvenido, '.$usuarioBD['nombre']]);
				
				return redirect()->to('Completar_registro')->with('alerta', ['tipo' => 'correcto', 'mensaje' => 'Bienvenido, '.$usuarioBD['nombre']]);
			}

			return redirect()->to('usuarios/perfil')->with('alerta', ['tipo' => 'correcto', 'mensaje' => 'Bienvenido, '.$usuarioBD['nombre']]);
		}
		
		if(empty(session('pre-rol'))) return redirect()->back()->with('alerta', ['tipo' => 'custom', 'mensaje' => 'Por favor, registrate como Freelancer o Contratante']);
		$datosUsuarios = [
			'autenticador' => $infoUsuario->getId(),
			'nombre' => $infoUsuario->getGivenName(),
			'apellidos' => $infoUsuario->getFamilyName(),
			'correo' => $infoUsuario->getEmail(),
			'cuenta' => 'google',
			'rol' => session('pre-rol'),
			'imagen' => $infoUsuario->getPicture()
		];
		$this->usuariosBD->insert($datosUsuarios);
		
		$usuarioBD = $this->usuariosBD->where('autenticador', $infoUsuario->id)->first();
		mkdir(WRITEPATH.'uploads/Usuarios/'.$usuarioBD['id']."_".$usuarioBD['correo']);
		session()->set([
			'id' => $usuarioBD['id'],
			'autenticador' => $usuarioBD['autenticador'],
			'usuario' => $usuarioBD['nombre'].' '.$usuarioBD['apellidos'],
			'correo' => $usuarioBD['correo'],
			'cuenta' => $usuarioBD['cuenta'],
			'imagen' => $usuarioBD['imagen'],
			'rol' => $usuarioBD['rol'],
			'plan' => $usuarioBD['plan'],
			'logeado' => true
		]);
		
		$Correo = new Correos();
		$Correo->registroFreelance($usuarioBD);
		$Correo->alertaNuevoUsuario($usuarioBD);
		$ult_conexion = conversorZonaHoraria(date('Y-m-d H:i:s'), app_timezone(), date_default_timezone_get());
		$this->usuariosBD->save(['id' => $usuarioBD['id'], 'ult_conexion' => $ult_conexion]);
		if($usuarioBD['rol'] == 'freelancer') return redirect()->to('Llenar_perfil')->with('alerta', ['tipo' => 'correcto', 'mensaje' => 'Bienvenido, '.$datosUsuarios['nombre']]);
		if($usuarioBD['rol'] == 'contratante') return redirect()->to('Completar_registro')->with('alerta', ['tipo' => 'correcto', 'mensaje' => 'Bienvenido, '.$datosUsuarios['nombre']]);
	}
	
	public function linkTemporal($id_usuario){
		$usuarioBD = $this->usuariosBD->get_usuarioID($id_usuario);
		if(empty($usuarioBD)) return false;
		
		$cadena = $id_usuario.$usuarioBD['nombre'].$usuarioBD['apellidos'].rand(1,9999999).date('Y-m-d');
		$token = sha1($cadena);
		
		$guardado = db_connect()->table('recuperacionCuentas')->insert(['id_usuario' => $id_usuario, 'token' => $token, 'created_at' => date('Y-m-d H:i:s')]);
		if(!$guardado) return false;
		
		return base_url('recuperar')."?to=$token&us=".sha1($id_usuario);
	}
	
	public function guardarPlanPasos(){
		$id_plan = $this->request->getPost('id_plan');
		//Se valida si el plan que eligió es gratuito
		$usuario = $this->usuariosBD->get_usuarioID(session('id'));
		if($usuario['rol'] == 'contratante' && empty($id_plan)){
			$this->usuariosBD->save(['id' => session('id'), 'estatus_registro' => 'completado']);
			return json_encode(['tipo' => 'correcto', 'mensaje' => 'guardado'], JSON_FORCE_OBJECT);
		}
		
		$respuesta = $this->usuariosBD->save(['id' => session('id'), 'plan' => $id_plan]);
		if(!$respuesta) return json_encode(['tipo' => 'falla', 'mensaje' => 'No pudimos procesar la solicitud, inténtelo nuevamente'], JSON_FORCE_OBJECT);
		
		return json_encode(['tipo' => 'correcto', 'mensaje' => 'guardado'], JSON_FORCE_OBJECT);
	}
	
	public function perfilCompletado(){
		$tarjeta = $this->usuariosBD->guardarTarjetas();
		$respuesta = json_decode($tarjeta, true);
		if($respuesta['alerta'] != 'correcto') return $tarjeta;
		
		$suscripcion_creada = model('Suscripciones')->crear_suscripcion('true', $respuesta['tarjeta']);
		$respuesta = json_decode($suscripcion_creada, true);
		if($respuesta['alerta'] != 'correcto') return $suscripcion_creada;

		if(session('rol') == 'contratante') return $this->usuariosBD->save(['id' => session('id'), 'estatus_registro' => 'completado']);
		else return json_encode(['alerta' => 'correcto', 'mensaje' => 'Suscripción creada correctamente'], JSON_FORCE_OBJECT);
	}
	
	public function validarDireccion(){
		if(!session('logeado')) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Al parecer tu sesión ha finalizado, ingresa nuevamente para poder adquirir el plan que deseas.'], JSON_FORCE_OBJECT);
		$user = $this->usuariosBD->get_id(session('id'));
		if($user == null) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Lo sentimos, no pudimos encontrar su usuario en nuestros registros, inténtelo de nuevo.'], JSON_FORCE_OBJECT);
		if($user['direccion'] == null) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Opps, parece que no has ingresado tu dirección, ingresa a tu perfil para completar tus datos.'], JSON_FORCE_OBJECT);
		return json_encode(['tipo' => 'correcto', 'mensaje' => 'Correcto']);
	}
	
	public function perfil_pasos_freelancer(){
		if(autorizacion() != 'freelancer' || !session('logeado')) return redirect()->to('');
		$usuario = $this->usuariosBD->get_usuarioID(session('id'));
		if($usuario['estatus_registro'] == 'completado') return redirect()->to('usuarios/perfil');
		
		$paso = $this->usuariosBD->perfilCompleto(session('id'));
		if($paso == 0) return view(freelancer('Registro_perfil/Contacto'));
		if($paso == 1) return view(freelancer('Registro_perfil/Areas_experiencias'));
		if($paso == 2) return view(freelancer('Registro_perfil/Experiencias'));
		if($paso == 3 && !empty($usuario['plan'])) return view(freelancer('Registro_perfil/Pago'));
		else return view(freelancer('Registro_perfil/Membresias'));
	}
	public function perfil_pasos_contratista(){
		if(autorizacion() != 'contratante' || !session('logeado')) return redirect()->to('');
		$usuario = $this->usuariosBD->get_usuarioID(session('id'));
		if($usuario['estatus_registro'] == 'completado') return redirect()->to('usuarios/perfil');
		
		$paso = $this->usuariosBD->perfilCompleto(session('id'));
		if($paso == 0) return view(contratista('Registro_perfil/Contacto'));
		if($paso == 3 && !empty($usuario['plan'])) return view(contratista('Registro_perfil/Pago'));
		else return view(contratista('Registro_perfil/Membresias'));
	}
	public function guardarExperiencia(){
		return $this->usuariosBD->guardarExperiencia();
	}
	public function guardar_Perfil_1(){
		return $this->usuariosBD->guardar_Perfil_1();
	}
	public function guardar_Perfil_1_contratista(){
		$respuesta = $this->usuariosBD->guardar_Perfil_1();
		if($respuesta) return true;
		return false;
	}
    public function borrar_subarea(){
		return $this->usuariosBD->borrar_subarea();
	}
	public function zona_pago(){
		$id = $this->request->getPost('plan');
		return view('/backend/Freelancer/viewcells/zona-pago', array('id' => $id));
	}
	public function cambiarPlanRegistro(){
		$id = sanitizar($this->request->getPost('id'));
		$this->usuariosBD->save(['id' => session('id'), 'plan' => $id]);

		session()->set(['plan' => $id]);
	}
	public function guardarAreas(){
		return $this->usuariosBD->guardarAreas();
	}
	
	public function recuperarAcceso(){
		$datos['token'] = $this->request->getGet('to');
		$datos['id_usuario'] = $this->request->getGet('us');
		
		$cuentas_papelera = db_connect()->table('recuperacionCuentas')->select()->where('token', $datos['token'])->get()->getRowArray();
		if(empty($cuentas_papelera)) return redirect()->back();
		
		if(sha1($cuentas_papelera['id_usuario']) == $datos['id_usuario']) return view('front/RecuperarCuenta', $datos);
		
		return redirect()->back()->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'Tiempo expirado al recuperar la cuenta']);
	}
	
	public function restablecerCuentaContrasena(){
		$token = trim($this->request->getPost('token'));
		$id_usuario = sanitizar($this->request->getPost('usuario'));
		$contraseña1 = trim($this->request->getPost('contrasena_nueva'));
		$contraseña2 = trim($this->request->getPost('confirmar_contrasena'));
		
		if($contraseña1 != $contraseña2) return redirect()->back()->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'Las contraseñas no coinciden'], JSON_FORCE_OBJECT);
		$cuentas_papelera = db_connect()->table('recuperacionCuentas')->select()->where('token', $token)->get()->getRowArray();
		
		if(sha1($cuentas_papelera['id_usuario']) !== $id_usuario) redirect()->back()->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'Tiempo expirado al recuperar la cuenta']);
		
		$cambiada = $this->usuariosBD->save(['id' => $cuentas_papelera['id_usuario'], 'contrasena' => password_hash($contraseña1, PASSWORD_DEFAULT)]);
		if(!$cambiada) redirect()->to('ingresar')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Actualizado']);
		
		$db = db_connect();
		$token_eliminado = $db->query("DELETE FROM recuperacionCuentas WHERE token = '$token'");
		if($token_eliminado) return redirect()->to('ingresar')->with('alerta', ['tipo' => 'correcto', 'mensaje' => 'Actualizado']);
		
		return redirect()->back()->with('alerta', ['tipo' => 'falla', 'mensaje' => 'No se pudo actualizar, intentélo nuevamente']);
	}
	
	public function get_estatus_perfil_ID(){
		$id = sanitizar($this->request->getPost('id'));
		dd($id);
		return $this->usuariosBD->get_estatus_perfil_ID($id);
	}

	public function sin_experiencia_registro(){
		$completado = model('Autenticacion')->save(['id' => session('id'), 'estatus_registro' => 'completado']);
		if($completado) return 'Completado';
		return 'No completado';
	}
	
	public function buscarReactivarCuentaUsuario(){
		$correo = sanitizar($this->request->getPost('correo'));
		if(empty($correo)) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Opps, ingresa un correo para validar la recuperación de tu cuenta.'], JSON_FORCE_OBJECT);
		
		$user = $this->usuariosBD->where('correo', $correo)->first();
		if($user != null) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Al parecer tu cuenta no se encuentra eliminada aun, da <a href="'.base_url('/ingresar').'">clic aqui para ingresar</a>'], JSON_FORCE_OBJECT);
		
		$eliminado = $this->usuariosBD->withDeleted()->where('correo', $correo)->first();
		if($eliminado == null) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Lo sentimos, no pudimos encontrar el correo de tu cuenta, si no te has registrado puedes hacerlo al dar clic <a href="'.base_url('/registrarme-como').'">aqui</a>'], JSON_FORCE_OBJECT);
		if(!empty($eliminado)){
			$correo = new Correos();
			$link = $this->linkTemporal($eliminado['id']);
			$enviado = $correo->enviar_recuperacionCuenta($eliminado, $link);
			if($enviado) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Hemos enviado el enlace de recuperación a tu correo, busca en tu bandeja de entrada para reactivar tu cuenta.'], JSON_FORCE_OBJECT);
			
			return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Opps, inténtelo nuevamente'], JSON_FORCE_OBJECT);
		}
	}
}