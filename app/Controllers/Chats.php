<?php namespace App\Controllers;

use CodeIgniter\I18n\Time;
use App\Libraries\AccesosPlanes;

class Chats extends BaseController{
    protected $proyectos, $postulaciones, $salas, $usuarios, $solicitudes;
    public function __construct(){
        $this->proyectos = model('Proyectos');
        $this->postulaciones = model('Postulaciones');
        $this->salas = model('Salas');
        $this->usuarios = model('Autenticacion');
        $this->solicitudes = model('Solicitudes');
        helper('funciones');
        helper('text');
    }

    // Obtiene los archivos de la sala de chat
	public function get_archivos($id_sala){
	    $id_sala = sanitizar($id_sala);
		if(empty($id_sala)) return 'Error, no se encontró la sala de chat';

		$archivos = model('Archivos')->where('id_sala', $id_sala)->findAll();
		if(empty($archivos)) return 'Sin archivos disponibles';

		$sala = $this->salas->find($id_sala);
		$proyecto = $this->proyectos->find($sala['id_proyecto']);
		$contratista = $this->usuarios->get_usuarioID($sala['id_contratista']);

		$directorio = base_url()."/writable/uploads/Usuarios/".$contratista['id']."_".$contratista['nombre']." ".$contratista['apellidos']."/Proyectos/".$proyecto['titulo']."/Archivos/";
		foreach($archivos as $archivo){
			 echo "<a class='d-block text-center bg-secondary text-font2 text-white text-decoration-none rounded-3 h6 p-2' download href='".$directorio.$archivo['nombre']."'><i class='fas fa-download' aria-hidden='true'></i> <span class='text-font'>".$archivo['nombre']."</span></a>";
		}
	}

    public function enviar_archivosChat(){
        $usuario = $this->usuarios->get_usuarioID(session('id'));
        $permisos = new AccesosPlanes($usuario['plan']);
        if(!$permisos->getValor_permiso('compartirdocumentos')) return json_encode(['alerta' => 'advertencia', 'mensaje' => 'Adquiera una suscripción PREMIUM para enviar documentos'], JSON_FORCE_OBJECT);
        
        $id_sala = sanitizar($this->request->getPost('id'));
        if(empty($id_sala)) return json_encode(['alerta' => 'falla', 'mensaje' => 'Error, no se encontró la sala de chat. Recargue la página'], JSON_FORCE_OBJECT);

        $archivo = $this->request->getFile('archivo');
        if(empty($archivo->getTempName())) return json_encode(['alerta' => 'custom', 'mensaje' => 'Error, seleccione un archivo para compartir.'], JSON_FORCE_OBJECT);
        
        $sala = $this->salas->find($id_sala);
        $proyecto = $this->proyectos->find($sala['id_proyecto']);
		$contratista = $this->usuarios->get_usuarioID($sala['id_contratista']);
        $directorio = WRITEPATH.'uploads/Usuarios/'.$contratista['id']."_".$contratista['correo'].'/Proyectos/'.$proyecto['titulo'].'/Archivos/';
        // dd($directorio);
        
        $archivo->store(strstr($directorio, 'Usuarios/'), $archivo->getName());

        $es_guardado = model('Archivos')->save(['id_sala' => $id_sala, 'nombre' => $archivo->getName()]);
        if($es_guardado) return json_encode(['alerta' => 'correcto'], JSON_FORCE_OBJECT);
        else return json_encode(['alerta' => 'falla', 'mensaje' => 'Error, seleccione un archivo para compartir'], JSON_FORCE_OBJECT);
    }

    public function saveCantidadMensajes(){
        $tipo = session('rol');
        $data = $this->salas->where('id', $this->request->getPost('sala'))->first();
        if($data == null) return '';
        if($data['mensajes'] == null){
            if($tipo == 'freelancer'){
                $actua = [
                    'contratante' => 0,
                    'freelancer' => $this->request->getPost('cantidad')
                ];
            }else{
                $actua = [
                    'contratante' => $this->request->getPost('cantidad'),
                    'freelancer' => 0,
                ];
            }
            $this->salas->where('id', $this->request->getPost('sala'))->set(['mensajes' => json_encode($actua, JSON_FORCE_OBJECT)])->update();
            return '';
        }
        $mensajes = json_decode($data['mensajes'], true);
        if($tipo == 'freelancer'){
            $actua = [
                'contratante' => $mensajes['contratante'],
                'freelancer' => $this->request->getPost('cantidad')
            ];
        }else{
            $actua = [
                'contratante' => $this->request->getPost('cantidad'),
                'freelancer' => $mensajes['freelancer'],
            ];
        }
        $this->salas->where('id', $this->request->getPost('sala'))->set(['mensajes' => json_encode($actua, JSON_FORCE_OBJECT)])->update();
        return '';
    }

    public function totalMensajes(){
        $id = sanitizar($this->request->getPost('id'));
        $sala = sanitizar($this->request->getPost('sala'));
        $chat = $this->salas->where('id', $sala)->first();
        if($chat['mensajes'] == null){
            return 0;
        }else{
            $datos = json_decode($chat['mensajes'], true);
            (session('rol') == 'freelancer')? $total = $datos['freelancer'] : $total = $datos['contratante'];
            return $total;
        }
    }
}