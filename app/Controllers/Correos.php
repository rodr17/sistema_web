<?php namespace App\Controllers;

class Correos extends BaseController{
    protected $correo, $usuarios, $solicitudes, $proyectos, $reportes, $suscripciones;
    public function __construct(){
        $this->correo = \Config\Services::email();
        $this->correo->setFrom('correos@tresesenta.lat', 'Mextemps Freelance');
        $this->usuarios = model('Autenticacion');
        $this->solicitudes = model('Solicitudes');
        $this->proyectos = model('Proyectos');
        $this->reportes = model('Soporte');
        $this->suscripciones = model('Suscripciones');
    }
    public function index(){
        return view('Correos/index');
    }
    // Bienvenida de ambos perfiles
    public function bienvenidaFreelance(){
        return view('Correos/BienvenidoFreelance', array( 'nombre' => 'Nombre apellido'));
    }
    public function bienvenidaContratista(){
        return view('Correos/BienvenidoContratista', array( 'nombre' => 'Nombre apellido'));
    }
    // Contacto
    public function contacto(){
        return view('Correos/Contacto', array('mensaje' => 'Soy un mensaje', 'usuario' => ['nombre' => 'Nombre', 'telefono' => '8181818181'], 'correo' => 'correo@mextemps.com'));
    }
    // cuenta aprobada
    public function cuentaAprobada(){
        return view('Correos/cuenta-aprobada');
    }
    // Cuenta declinada
    public function cuentaDeclinada(){
        $mensaje = 'Soy un mensaje';
        return view('Correos/cuenta-declinada', array('mensaje' => $mensaje));
    }
    // Nuevo administrador
    public function nuevoAdministrador(){
        return view('Correos/NuevoAdmin', array('correo' => 'prueba@mextemps.com', 'contra' => 'Contraseña'));
    }
    // Proyecto no asignado
    public function proyectoNoAsignado(){
        $proyecto = $this->proyectos->orderBy('id', 'DESC')->limit(1)->first();
        return view('Correos/Proyecto_no_asignado', array('proyecto' => $proyecto));
    }
    // Proyecto finalizado
    public function proyectoFinalizado(){
        $proyecto = $this->proyectos->orderBy('id', 'DESC')->limit(1)->first();
        return view('Correos/proyecto-finalizado', array('proyecto' => $proyecto));
    }
    // Freelance reportado
    public function freelanceReportado(){
        $freelance = $this->usuarios->get_id(71);
        $contratista = $this->usuarios->get_id(57);
        $proyecto = $this->proyectos->orderBy('id', 'DESC')->limit(1)->first();
        $comentario = 'Soy un comentario.';
        return view('Correos/ReporteFreelance', array('proyecto' => $proyecto, 'freelance' => $freelance, 'contratista' => $contratista, 'comentario' => $comentario));
    }
    // Resetear contrasena
    public function resetearContrasena(){
        return view('Correos/ResetarContrasena', array('nombre' => 'Nombre Apellido', 'contrasena' => 'contrasena', 'encabezado' => 'Nueva Contraseña'));
    }
    // Solicitud aceptada
    public function solicitudAceptada(){
        $freelance = $this->usuarios->get_id(71);
        $proyecto = $this->proyectos->orderBy('id', 'DESC')->limit(1)->first();
        return view('Correos/Solicitud_aceptada', array('freelance' => $freelance, 'proyecto' => $proyecto));
    }
    // Solicitud declinada
    public function declinarSolicitud(){
        $freelancer = $this->usuarios->get_id(2);
        $proyecto = $this->proyectos->orderBy('id', 'DESC')->limit(1)->first();
        return view('Correos/Solicitud_declinada', array('mensaje' => 'Soy un mensaje', 'freelancer' => $freelancer, 'proyecto' => $proyecto));
    }
    // Solicitud de proyecto
    public function solicitudProyecto(){
        return view('Correos/Solicitudes_directas');
    }
    // Correo de soporte
    public function soporte(){
        $reporte = $this->reportes->limit(1)->first();
        return view('Correos/Soporte', array('usuario' => $reporte));
    }
    // Suscripcion cancelada
    public function cancelarSuscripcion(){
        $usuario = $this->usuarios->get_id(1);
        $suscripcion_cancelada = $this->suscripciones->builder()
                    ->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
                    ->where('id_usuario', '1')
                    ->get()->getRowArray();
        return view('Correos/SuscripcionCancelada', array('usuario' => $usuario, 'suscripcion' => $suscripcion_cancelada));
    }
    // Recordatorio de pago || Falta crear funcion
    public function recordatorioPago(){
        return view('Correos/recordatorio-pago');
    }
    // Eliminacion de cuenta || Falta crear funcion
    public function cuentaEliminada(){
        return view('Correos/cuenta-eliminada');
    }
    // Perfil incompleto || Falta crear funcion
    public function perfilIncompleto(){
        return view('Correos/perfil-incompleto');
    }
    public function respuestaSoporte(){
        $mensaje = 'Mensaje';
        $reporte = [
            'nombre' => 'Nombre Apellidos',
            'asunto' => 'Error',
            'correo' => 'correa@mextemps.com',
            'telefono' => '8181818181',
        ];
        return view('Correos/respuesta-soporte', array('mensaje' => $mensaje, 'reporte' => $reporte));
    }
    public function proyecto_aceptado(){
        $contratista = $this->usuarios->where('rol', 'contratista')->first();
        $proyecto = $this->proyectos->first();
        return view('Correos/Proyecto_aceptado', array('contratista' => $contratista, 'proyecto' => $proyecto));
    }
    public function soliFinalizarProyecto(){
        $contratista = $this->usuarios->where('rol', 'freelancer')->first();
        $proyecto = $this->proyectos->first();
        return view('Correos/solicitud_proyecto_finalizado', array('freelancer' => $contratista, 'proyecto' => $proyecto));
    }
    public function declinarSolicitudFin(){
        $proyecto = $this->proyectos->first();
        return view('Correos/solicitud_finalizar_declinada', array('proyecto' => $proyecto));
    }
    public function sugerenciaDeclinada(){
        $data = model('Sugerencias')->first();
        $datos = json_decode($data['contenido'], true);
        return view('Correos/solicitud_sugerencia_declinada', array('datos' => $datos, 'data' => $data));
    }
    public function sugerenciaAnadida(){
        $data = model('Sugerencias')->first();
        $datos = json_decode($data['contenido'], true);
        return view('Correos/solicitud_sugerencia_anadida', array('datos' => $datos, 'data' => $data));
    }
    public function sugerenciaYaExistente(){
        $data = model('Sugerencias')->first();
        $datos = json_decode($data['contenido'], true);
        return view('Correos/solicitud_sugerencia_ya_existente', array('datos' => $datos, 'data' => $data));
    }
    public function solicitudSugerencia(){
        $data = model('Sugerencias')->first();
        $datos = json_decode($data['contenido'], true);
        ($data['tipo'] == 'habilidad')? $tipo = 'habilidad' : $tipo = 'idioma';
        $user = model('Autenticacion')->where('id', $data['id_usuario'])->first();
        return view('Correos/solicitud-sugerencia', array('datos' => $datos, 'data' => $data, 'tipo' => $tipo, 'user' => $user));
    }
    public function alertaNuevoUsuario(){
        $usuario = $this->usuarios->first();
        return view('Correos/alerta-nuevo-usuario', array('usuario' => $usuario));
    }
    public function mensajesSinVer(){
        $chat = model('Salas')->orderBy('id', 'RANDOM')->first();
        // d($chat);
        $proyecto = $this->proyectos->where('id', $chat['id_proyecto'])->first();
        // d($proyecto);
        $contratista = $this->usuarios->where('id', $chat['id_contratista'])->first();
        // d($contratista);
        $freelancer = $this->usuarios->where('id', $chat['id_freelancer'])->first();
        // d($freelancer);
        if($chat['id']%2 == 0){
            // d('Numero par');
            $enviador = $contratista;
        }else{
            // d('Numero impar');
            $enviador = $freelancer;
        }
        // dd($enviador);
        return view('Correos/nuevos-mensajes', array('enviador' => $enviador, 'proyecto' => $proyecto));
    }
    public function nuevoPago(){
        $pago = model('Pagos')->orderBy('id', 'RANDOM')->first();
        $usuario = $this->usuarios->where('id', $pago['id_usuario'])->first();
        if($pago['tipo'] == 'paypal'){
            $plan = model('ModelPlanes')->where('id_paypal', $pago['id_membresia'])->first();
            if($plan == null){
                $plan = model('ModelPlanes')->where('id_paypal_anual', $pago['id_membresia'])->first();
                $tipo = 'anual';
            }else{
                $tipo = 'mensual';
            }
            $metodo = 'Paypal';
        }else{
            $plan = model('ModelPlanes')->where('idopenpay', $pago['id_membresia'])->first();
            if($plan == null){
                $plan = model('ModelPlanes')->where('idopenpay_anual', $pago['id_membresia'])->first();
                $tipo = 'anual';
            }else{
                $tipo = 'mensual';
            }
            ($pago['tipo'] == 'openpay')? $metodo = 'Pagado con tarjeta' : (($pago['tipo'] == 'tienda')? $metodo = 'Tiendas de conveniencia' : $metodo = 'Transferencia electrónica') ;
        }
        return view('Correos/alerta-nuevo-pago', array('usuario' => $usuario, 'pago' => $pago, 'tipo' => $tipo, 'plan' => $plan, 'metodo' => $metodo));
    }
    public function eliminacionCuenta(){
        $user = $this->usuarios->first();
        return view('Correos/usuario-cuenta-eliminada', array('usuario' => $user));
    }
    public function eliminadoPorAdministracion(){
        $user = model('Autenticacion')->orderBy('id', 'RANDOM')->first();
        // dd($user);
        return view('Correos/eliminado-por-administracion');
    }
    public function suscripcionVencida(){
        $suscripcion = model('Suscripciones')->orderBy('id', 'RANDOM')->first();
        $user = model('Autenticacion')->get_id($suscripcion['id_usuario']);
        $plan = model('ModelPlanes')->where('id_plan_mensual', $suscripcion['id_plan'])->orWhere('id_plan_anual', $suscripcion['id_plan'])->first();
        if($plan == NULL){
            $plan = model('ModelPlanes')->where('id_plan_mensual', $suscripcion['id_plan'])->orWhere('id_plan_anual', $suscripcion['id_plan'])->first();
        }
        if($plan == null) return 'El plan ya no existe.';
        
        return view('Correos/suscripcion-vencida', array('usuario' => $user, 'suscripcion' => $suscripcion, 'plan' => $plan));
    }
    public function renovacionFallida(){
        $suscripcion = model('Suscripciones')->orderBy('id', 'RANDOM')->first();
        $user = model('Autenticacion')->get_id($suscripcion['id_usuario']);
        $plan = model('ModelPlanes')->where('id_plan_mensual', $suscripcion['id_plan'])->orWhere('id_plan_anual', $suscripcion['id_plan'])->first();
        if($plan == NULL){
            $plan = model('ModelPlanes')->where('id_plan_mensual', $suscripcion['id_plan'])->orWhere('id_plan_anual', $suscripcion['id_plan'])->first();
        }
        if($plan == null) return 'El plan ya no existe.';
        
        return view('Correos/suscripcion-fallida', array('usuario' => $user, 'suscripcion' => $suscripcion, 'plan' => $plan, 'mensaje' => 'No cuenta con fondos suficientes.'));
    }

    public function nuevaReferenciaPago(){
        $datos_referencia_pago = [
			'amount' => 149,
			'description' => 'Compra de suscripción Intermedio José Maldonado',
			'paymentMethod' => 'cash',
			'currency' => 'MXN',
			'billing' => [
				'firstName' => 'José',
				'lastName' => 'Maldonado',
				'email' => 'jmaldonado@tresesenta.mx',
				'phone' => '8181818181',
				'merchantReferenceCode' => 'ORD-'.time().'-1'
			],
			'expiryDate' => date('Y/m/d', strtotime(date('Y-m-d').'+ 2 days')),
			'reference' => '7REFERENCIAPRUEBA55',
		];
        return view('Correos/nueva-referencia-pago', $datos_referencia_pago);
    }
}