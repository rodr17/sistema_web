<?php namespace App\Controllers;

class Habilidades extends BaseController{
	
	protected $habilidades;
	public function __construct(){
		$this->habilidades = model('Habilidades');
	}
	public function get_subareas(){
		return $this->habilidades->get_subareas();
	}
	public function secAdministrador(){
		$areas = $this->habilidades->where('superior', '0')->orderBy('area', 'ASC')->find();
		return view('/backend/administrador/habilidades', array('areas' => $areas));
	}
	public function addHabilidad(){
		if(!session('is_logged')) return redirect()->to('');
		if(session('rol') != 'admin') return redirect()->to('');
		$areas = $this->habilidades->where('superior', '0')->orderBy('area', 'ASC')->find();
		return view('/backend/administrador/agregar-habilidad', array('areas' => $areas));
	}
	public function solicitudesHabilidad(){
		if(!session('is_logged')) return redirect()->to('');
		if(session('rol') != 'admin') return redirect()->to('');
		return view('/backend/administrador/solicitudes-habilidad');
	}
	public function versubareas(){
		$buscar = sanitizar($this->request->getPost('buscar_subarea'));
		$area = trim($this->request->getPost('area'));
		
		if(empty($buscar)) $subareas = $this->habilidades->where('superior', $area)->orderBy('area', 'ASC')->find();
		else $subareas = $this->habilidades->where('superior', $area)->like('area', $buscar)->orderBy('area', 'ASC')->find();

		return view('/backend/administrador/viewcells/subareas', array('subareas' => $subareas));
	}
	public function tablaAreas(){
		$buscar = sanitizar($this->request->getPost('buscar'));
		if(empty($buscar)) $areas = $this->habilidades->where('superior', '0')->orderBy('area', 'ASC')->find();
		else $areas = $this->habilidades->where('superior', '0')->like('area', $buscar)->orderBy('area', 'ASC')->find();
		
		return view('backend/administrador/viewcells/tabla-areas', array('areas' => $areas));
	}
	public function actuaArea(){
		$area = sanitizar($this->request->getPost('area'));
		$id = sanitizar($this->request->getPost('id'));
		if($area == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el nuevo nombre para la area a editar.'], JSON_FORCE_OBJECT);
		if($id == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro encontrar el area.'], JSON_FORCE_OBJECT);
		$ar = $this->habilidades->where('id', $id)->first();
		if($ar == null) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro encontrar el area.'], JSON_FORCE_OBJECT);
		$antiguo = $ar['area'];
		$this->habilidades->where('id', $id)->set(['area' => $area])->update();
		$this->habilidades->where('superior', $antiguo)->set(['superior' => $area])->update();
		return json_encode(['tipo' => 'success', 'mensaje' => 'Area actualizada.'], JSON_FORCE_OBJECT);
	}
	public function actuaSubArea(){
		$id = sanitizar($this->request->getPost('idsub'));
		if($id == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro encontrar la subarea.'], JSON_FORCE_OBJECT);
		
		$subarea = sanitizar($this->request->getPost('subarea'));
		if($subarea == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el nuevo nombre para la subarea a editar.'], JSON_FORCE_OBJECT);
		
		$superior = sanitizar($this->request->getPost('superior'));
		if($superior == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Seleccione la area superior para la area'], JSON_FORCE_OBJECT);
		// dd($superior);
		$ar = $this->habilidades->where('id', $id)->first();
		if($ar == null) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro encontrar el area.'], JSON_FORCE_OBJECT);
		$antiguo = $ar['area'];
		$this->habilidades->where('id', $id)->set(['area' => $subarea, 'superior' => $superior])->update();
		return json_encode(['tipo' => 'success', 'mensaje' => 'Area actualizada.', 'data' => view('/backend/administrador/viewcells/subareas', array('subareas' => $this->habilidades->where('superior', $ar['superior'])->orderBy('area', 'ASC')->find()))], JSON_FORCE_OBJECT);
	}
	public function formEditSubarea(){
		$id = $this->request->getPost('id');
		$subarea = $this->habilidades->where('id', $id)->first();
		$areas = $this->habilidades->where('superior', '0')->find();
		return view('/backend/administrador/viewcells/form-edit-subarea', array('subarea' => $subarea, 'areas' => $areas));
	}
	public function subareas(){
		$area = $this->request->getPost('area');
		$areas = $this->habilidades->where('superior', $area)->find();
		if($areas == '') $areas = $this->habilidades->where('superior !=', '0')->find();
		echo '<option value="" selected>Seleccione una opción</option>';
		foreach($areas as $a){
			echo '<option value="'.$a['area'].'">'.$a['area'].'</option>';
		}
	}
	public function agregarHabilidad(){
		return view('/backend/administrador/nueva-habilidad');
	}
	public function superiores(){
		$areas = $this->habilidades->where('superior', '0')->find();
		echo '<option value="0">Seleccione una opción</option>';
		foreach($areas as $a){
			echo '<option value="'.$a['area'].'">'.$a['area'].'</option>';
		}
	}
	public function eliminar(){
		$id = sanitizar($this->request->getPost('id'));
		if(empty($id)) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro encontrar el identificador de la subarea.'], JSON_FORCE_OBJECT);
		$subarea = $this->habilidades->where('id', $id)->first();
		if($subarea == null) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro encontrar la subarea.'], JSON_FORCE_OBJECT);
		// if($subarea['superior'] == 0) return json_encode(['tipo' => 'success', 'mensaje' => 'Sin permisos para hacerlo.']);
		$superior = $subarea['superior'];
		$this->habilidades->eliminar($id);
		return json_encode(['tipo' => 'success', 'mensaje' => 'Area eliminada.', 'data' => view('/backend/administrador/viewcells/subareas', array('subareas' => $this->habilidades->where('superior', $superior)->orderBy('area', 'ASC')->find()))], JSON_FORCE_OBJECT);
	}
	public function deleteSuperior(){
		$id = sanitizar($this->request->getPost('id'));
		if($id == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro especificar el area seleccionada.'], JSON_FORCE_OBJECT);
		$area = $this->habilidades->where('id', $id)->first();
		if($area == null) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro encontrar el area seleccionada.'], JSON_FORCE_OBJECT);
		$subareas = $this->habilidades->where('superior', $area['area'])->find();
		$this->habilidades->eliminar($id);
		if(count($subareas) != 0) $this->habilidades->where('superior', $area['area'])->set(['superior' => 'Otros'])->update();
		return json_encode(['tipo' => 'success', 'mensaje' => 'Area eliminada'], JSON_FORCE_OBJECT);
	}
	public function nuevaArea(){
		$area = sanitizar($this->request->getPost('area'));
		if(empty($area)) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el nombre para el area o subarea.'], JSON_FORCE_OBJECT);
		$superior = sanitizar($this->request->getPost('superior'));
		$data = [
			'area' => $area,
			'superior' => $superior
		];
		$this->habilidades->save($data);
		return json_encode(['tipo' => 'success', 'mensaje' => 'El area/subarea a sido creada exitosamente.'], JSON_FORCE_OBJECT);
	}
}