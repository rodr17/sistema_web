<?php namespace App\Controllers;

require_once __DIR__ . '/Facebook/vendor/autoload.php';
require_once __DIR__ . '/Google/vendor/autoload.php';

use App\Libraries\Correos;
use CodeIgniter\I18n\Time;
use App\Libraries\AccesosPlanes;
use App\Controllers\Suscripciones as Suscripciones;

class Home extends BaseController{
	protected $fb, $logeoFacebook, $urlFacebook, $usuarioGoogle, $secciones, $habilidades, $proyectos, $usuarios, $noticias, $permisos;
	public function __construct() {
		helper('funciones');
		helper('text');
		
		$this->fb = new \Facebook\Facebook([
			'app_id' => get_Facebook('app_id'),
			'app_secret' => get_Facebook('app_secret'),
			'default_graph_version' => 'v16.0'
		]);
		$this->logeoFacebook = $this->fb->getRedirectLoginHelper();
		$this->permisos = ['email'];
		$this->urlFacebook = $this->logeoFacebook->getLoginUrl(base_url('logeo'), $this->permisos);
		$this->usuarioGoogle = new \Google_Client();
		$this->usuarioGoogle->setClientId(get_Google('clienteID'));
		$this->usuarioGoogle->setClientSecret(get_Google('clienteSecret'));
		$this->usuarioGoogle->setRedirectUri(base_url('logeoGoogle'));
		$this->usuarioGoogle->addScope("email");
		$this->usuarioGoogle->addScope("profile");

		$this->secciones = model('ModelSecciones');
		$this->habilidades = model('Habilidades');
		$this->proyectos = model('Proyectos');
		$this->usuarios = model('Autenticacion');
		$this->noticias = model('Noticias');
		
		if(session('logeado')){
            $userverficar = $this->usuarios->where('id', session('id'))->first();
            if($userverficar == null) return redirect()->to('');
            $misuscripcion = model('Suscripciones')->where('id_usuario', session('id'))->first();
            if(($userverficar['estatus_registro'] != 'completado') && ($userverficar['rol'] == 'freelancer') && (($userverficar['plan'] != null) || ($userverficar['plan'] != 'free')) && $misuscripcion == null) header('Location: '.base_url('Llenar_perfil'));
            elseif(($userverficar['estatus_registro'] != 'completado') && ($userverficar['rol'] == 'contratante') && (($userverficar['plan'] != null) || ($userverficar['plan'] != 'free')) && $misuscripcion == null) header('Location: '.base_url('Completar_registro'));
		}
	}
	public function index(){
		$slides = $this->secciones->where('tipo', 'slide')->find();
		$freelancers = $this->usuarios->where(['rol' => 'freelancer'])->findAll(8, 0);
		
		if(!empty($freelancers)){
			foreach($freelancers as $key => $f){
				$proy = $this->proyectos->where(['id_freelancer' => $f['id'], 'estatus' => 'finalizado'])->findAll();
				$freelancers[$key]['terminados'] = count($proy);
				$calificaciones = model('Calificaciones')->where(['id_freelancer' => $f['id']])->find();
				
				if($calificaciones == null) $cal = 0;
				else{
					$t = 0;
					foreach($calificaciones as $c){
						$t += $c['calificacion'];
					}
					$cal = $t / count($calificaciones);
				}
				$freelancers[$key]['calificacion'] = $cal;
			}
			
			$freelancer_random = isset($freelancers[7]) ? $freelancers[7] : null;
			$freelancers = array_slice($freelancers, 0, 7);
			array_push($freelancers, $freelancer_random);
		}

        $proyectos = $this->proyectos->where('estatus', 'espera')->orderBy('destacado', 'DESC')->orderBy('id', 'DESC')->limit(6)->find();
		
		return view('front/home', array('slides' => $slides, 'navbar' => 'inicio', 'freelance' => $freelancers, 'proyectos' => $proyectos, 'categorias' => $this->habilidades->where('superior', '0')->orderBy('area', 'asc')->find()));
	}
	public function insignias(){
	    return view('front/insignias');
	}
	public function ordenarFreelance(){
        $freelancers = $this->usuarios->where(['rol' => 'freelancer'])->find();
        foreach($freelancers as $key => $f){
            $proyectos = $this->proyectos->where(['id_freelancer' => $f['id'], 'estatus' => 'finalizado'])->findAll();
            $total = count($proyectos);
            $freelancers[$key]['terminados'] = $total;
            $calificaciones = model('Calificaciones')->where(['id_freelancer' => $f['id']])->find();
            if($calificaciones == null){
                $cal = 0;
            }else{
                $t = 0;
                foreach($calificaciones as $c){
                    $t = $t + $c['calificacion'];
                }
                $cal = $t / count($calificaciones);
            }
            $freelancers[$key]['calificacion'] = $cal;
        }
        $aux = array();
        foreach ($freelancers as $key => $row) {
            $aux[$key] = $row['terminados'];
        }
        array_multisort($aux, SORT_DESC, $freelancers);
        
        $aux2 = array();
        foreach ($freelancers as $i => $r){
            $aux2[$i] = $r['calificacion'];
        }
        array_multisort($aux2, SORT_DESC, $freelancers);
        
        
        $mejores = array();
        foreach ($freelancers as $key => $row) {
            if($key >= 7) unset($freelancers[$key]);
        }
        $where = "rol = 'freelancer' AND";
        foreach($freelancers as $key => $row){
            if($key != 0){
                if($key < (count($freelancers) - 1)){
                    $where .= 'id != '.$row['id'].' AND ';
                }else{
                    $where .= 'id != '.$row['id'].')';
                }
            }else{
                $where .= ' (id != '.$row['id'].' AND ';
            }
        }
        // dd($where);
        $free = $this->usuarios->where($where)->orderBy('id', 'RANDOM')->first();
        array_push($freelancers, $free);
        d($free);
        d($freelancers);
	}
	public function nosotros(){
		$noticias = $this->noticias->findALl(5, 0);
		return view('front/nosotros', array('noticias' => $noticias));
	}
	public function blog(){
        if(isset($_GET['page'])){
            if($_GET['page'] <= 1){
                $noticias = $this->noticias->orderBy('id DESC, titulo DESC')->findALl(8, 0);
                $pagina = 1;
            }else{
                $page = (($_GET['page'] - 1) * 8);
                $noticias = $this->noticias->orderBy('id DESC, titulo DESC')->findALl(8, $page);
                $pagina = $_GET['page'];
            }
        }else{
            $noticias = $this->noticias->orderBy('id DESC, titulo DESC')->findALl(8, 0);
            $pagina = 1;
        }
        $total = $this->noticias->orderBy('id DESC, titulo DESC')->findAll();
        return view('front/blog', array('noticias' => $noticias,'pagina' => $pagina, 'pages' => ceil(count($total) / 8)));
    }
	public function paquetes(){
		$con_suscripcion = session('logeado')? Suscripciones::esta_suscrito_por_idUsuario(session('id')) : false;
		$preguntas = $this->secciones->where(['seccion' => 'paquetes', 'tipo' => 'pregunta'])->limit(8)->find();
		return view('front/paquetes', array('con_suscripcion' => $con_suscripcion, 'preguntas' => $preguntas));
	}
	public function paquetesContratista(){
		$con_suscripcion = session('logeado')? Suscripciones::esta_suscrito_por_idUsuario(session('id')) : false;
		$preguntas = $this->secciones->where(['seccion' => 'paquetes', 'tipo' => 'pregunta'])->limit(8)->find();
		return view('front/paquetes-contratista', array('con_suscripcion' => $con_suscripcion, 'preguntas' => $preguntas));
	}
	public function quienesSomos(){
		return view('front/quienes-somos');
	}
	public function comoFuncionaFreelance(){
		return view('front/como-funcriona-freelance');
	}
	public function comoFuncionaContratista(){
		return view('front/como-funcriona-contratista');
	}
	public function soporte(){
		return view('front/soporte');
	}
	public function contacto(){
		return view('front/contacto');
	}
	public function registrar($pre_rol){
		if(session('logeado')) return redirect()->to('usuarios/perfil/panel');

		$roles =['freelancer', 'contratante'];
		
		if(!in_array($pre_rol, $roles)) return redirect()->to('/registrarme-como');
	    
	    if(isset($_GET['planid'])) $id_plan = sanitizar($_GET['planid']);
	    else{
	    	session()->set(['pre-rol' => $pre_rol, 'plan' => '']);
	    	return view('front/registrar', array('urlFacebook' => $this->urlFacebook, 'urlGoogle' => $this->usuarioGoogle->createAuthUrl()));
	    }

        $plan = model('ModelPlanes')->where('id_plan_mensual', $id_plan)->orWhere('id_plan_anual', $id_plan)->first();
        ($plan != null)? session()->set(['pre-rol' => $pre_rol, 'plan' => $id_plan]) : session()->set(['pre-rol' => $pre_rol, 'plan' => '']);
	    
		return view('front/registrar', array('urlFacebook' => $this->urlFacebook, 'urlGoogle' => $this->usuarioGoogle->createAuthUrl()));
		
	}
	public function ingresar(){
		if(session('logeado')) return redirect()->to('usuarios/perfil/panel');
		return view('front/ingresar', array('urlFacebook' => $this->urlFacebook, 'urlGoogle' => $this->usuarioGoogle->createAuthUrl()));
	}
	public function proyecto(){
		if(autorizacion() != 'contratante') return redirect()->back()->with('alerta', ['tipo' => 'falla', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.']);

		$usuario = $this->usuarios->get_usuarioID(session('id'));
		$permisos = new AccesosPlanes($usuario['plan']);
		$proyectos = $this->proyectos->where(['id_contratista' => session('id'), 'estatus !=' => 'finalizado'])->find();

		if($usuario['plan'] == null || $usuario['plan'] == 'free' || $permisos->getValor_permiso('proyectosActivos') == 0){
		    if(Suscripciones::esta_suscrito_por_idUsuario($usuario['id'])); //Deja seguir el flujo si tiene CUENTA PREMIUM CON CUPON
            elseif(count($proyectos) >= 1) return redirect()->back()->with('alerta', ['tipo' => 'modal', 'mensaje' => 'Solo puedes tener 1 proyecto activo. Adquiere un plan PREMIUM para tener proyectos ilimitadas']);
		}else{
            if(count($proyectos) >= $permisos->getValor_permiso('proyectosActivos')) return redirect()->back()->with('alerta', ['tipo' => 'modal', 'mensaje' => 'Solo puedes tener '.$permisos->getValor_permiso('proyectosActivos').' proyectos activos. Adquiere un plan PREMIUM para tener proyectos ilimitadas']);
		}

		$habilidades = $this->habilidades->where('superior !=', '0')->orderBy('area', 'asc')->findAll();
		// Se obtiene cantidad restante de solicitudes disponibles
		$solicitudes_disponibles = !empty($usuario['plan']) ? $permisos->getValor_permiso('enviosolicitudes') : 1;

		$total_solicitudes = !empty($usuario['plan'] || Suscripciones::esta_suscrito_por_idUsuario($usuario['id'])) ? $permisos->getValor_permiso('enviosolicitudes') : 1;
		$solicitudes_disponibles = ($total_solicitudes == 9999) ? "Ilimitadas" : $total_solicitudes; // VALIDARÁ SI ES ILIMITADA (9999) O NO 
		return view('front/Proyecto', compact('habilidades', 'solicitudes_disponibles'));
	}
	public function validarCantProyectos(){
		$id_usuario = sanitizar($this->request->getPost('id'));
        if(empty($id_usuario)) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el id del usuario.'], JSON_FORCE_OBJECT);

        $usuario = $this->usuarios->get_usuarioID($id_usuario);
        if($usuario == null) return json_encode(['tipo' => 'error', 'mensaje' => 'El usuario no logró ser encontrado.'], JSON_FORCE_OBJECT);
        
		$permisos = new AccesosPlanes($usuario['plan']);
        
		$proyectos = $this->proyectos->where(['id_contratista' => $id_usuario, 'estatus !=' => 'finalizado'])->find();
        if($usuario['plan'] == null){
            if(Suscripciones::esta_suscrito_por_idUsuario($usuario['id'])); //Deja seguir el flujo si tiene CUENTA PREMIUM CON CUPON
            elseif(count($proyectos) >= 1) return json_encode(['tipo' => 'false', 'mensaje' => 'Solo puedes tener 1 proyecto activo. Adquiere un plan PREMIUM para tener proyectos ilimitados.'], JSON_FORCE_OBJECT);
		}else{
		    if(Suscripciones::esta_suscrito_por_idUsuario($usuario['id'])); //Deja seguir el flujo si tiene CUENTA PREMIUM CON CUPON
            elseif(count($proyectos) >= $permisos->getValor_permiso('proyectosActivos')) return json_encode(['tipo' => 'false', 'mensaje' => 'Solo puedes tener '.$permisos->getValor_permiso('proyectosActivos').' proyectos activos. Adquiere un plan PREMIUM para tener proyectos ilimitados.'], JSON_FORCE_OBJECT);
		}
		return json_encode(['tipo' => 'correcto', 'mensaje' => 'Aun disponible.'], JSON_FORCE_OBJECT);
	}
	public function termsAndCondiciones(){
		$terminos = $this->secciones->where('seccion', 'terminos-y-condiciones')->first();
		return view('front/terminos-y-condiciones', compact('terminos'));
	}
	public function avisoPrivacidad(){
		$datos = $this->secciones->where('seccion', 'aviso-privacidad')->first();
		return view('front/aviso-de-privacidad', array('aviso' => $datos));
	}
	public function comoFunciona(){
		return view('front/como-funciona');
	}
	public function registrarEmpresa(){
		return view('front/registrar-empresa');
	}
	public function subirPortafolio(){
		return view('front/subir-portafolio');
	}
	// Funcion para ver trabajo y postularse por parte del freelance
	public function verTrabajoID($id){
		// $con_suscripcion = session('logeado')? Suscripciones::esta_suscrito_por_idUsuario(session('id')) : false;
// 		if(!$con_suscripcion) return  redirect()->back()->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Adquiere una suscripcion para ingresar']);
// 		$aprobado = view_cell('App\Libraries\FuncionesSistema::AprovadoONo', ['id' => session('id')]);
// 		if(!$aprobado) return redirect()->back()->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Lo sentimos, pero antes de poder postularte a un proyecto, nuestro equipo de administración debe aprobar tu perfil.']);
		if(autorizacion() != 'freelancer') return redirect()->route('')->with('alerta', ['tipo' => 'warning', 'mensaje' => 'OOPS, no puedes acceder a esta sección cambia de cuenta para poder ingresar.']);
	    
	    $usuario = $this->usuarios->get_usuarioID(session('id'));
		$permisos = new AccesosPlanes($usuario['plan']);
// 			$redirect = $permisos->validarFreelancer('veroportunidades', 'true');
// 			if(!(empty($redirect))){
// 				if($redirect->getStatusCode() == 307) return $redirect;
// 			}
		
		$proyecto = $this->proyectos->where('id', $id)->first();
		if($proyecto == null) return redirect()->to('/usuarios/perfil/panel')->with('alerta', [ 'tipo' => 'warning', 'mensaje' => 'El trabajo solicitado no pudo ser encontrado.' ]);
		
	    if($proyecto['estatus'] != 'espera' && $proyecto['id_freelancer'] != session('id')) return redirect()->back()->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'OPPS, el proyecto que deseas ver ya no se encuentra disponible.' ]);
	    $contratista = model('Autenticacion')->get_id($proyecto['id_contratista']);
        if($contratista == null) return redirect()->to('/usuarios/perfil')->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'Lo sentimos, el contratante de este proyecto ya no se encuentra en la plataforma.']);
		
		if($proyecto['updated_at'] != '0000-00-00 00:00:00'){
			setlocale(LC_TIME, "es_MX"); 
			$inicio = utf8_encode(strftime('%A, %e de %B de %Y', strtotime($proyecto['updated_at'])));
		}else{
			setlocale(LC_TIME, "es_MX");
			$fecha = new Time('now', app_timezone(), 'es_MX');
			$inicio = utf8_encode(strftime('%A, %e de %B de %Y', strtotime($fecha)));
		}
		$freelance = $this->usuarios->get_id(session('id'));
		$entrega = utf8_encode(strftime('%A, %e de %B de %Y', strtotime($proyecto['fecha_entrega'])));
		return view('front/Trabajos', array('proyecto' => $proyecto, 'creacion' => $inicio, 'entrega' => $entrega, 'freelancer' => $freelance));
	}
	public function enviarSoporte_datos(){
		$asunto = sanitizar($this->request->getPost('asunto'));
		if(empty($asunto)) return json_encode(['alerta' => 'falla', 'mensaje' => 'ALTO... Se te olvida el asunto y es muy importante, para lograr una mejor atención rápida y oportuna.'], JSON_FORCE_OBJECT);
		
		$descripcion = sanitizar($this->request->getPost('descripcion'));
		if(empty($descripcion)) return json_encode(['alerta' => 'falla', 'mensaje' => 'ALTO... Agrega la descripción detallada, para poder ayudarte, o acaso ¿eres un ROBOT?'], JSON_FORCE_OBJECT);
		
		if(session('logeado')){
			$usuario = $this->usuarios->where('id', session('id'))->first();
			$datos = [
				'nombre' => session('usuario'),
				'telefono' => !empty($usuario['telefono']) ? $usuario['telefono'] : '',
				'correo' => $usuario['correo'],
				'asunto' => $asunto,
				'descripcion' => $descripcion
			];
		}
		else{
			$nombre = sanitizar($this->request->getPost('nombre'));
			if(empty($nombre)) return json_encode(['alerta' => 'falla', 'mensaje' => 'ALTO... Me presento soy Mextemp y tu nombre ¿cuál es?'], JSON_FORCE_OBJECT);
			
			$usuario['correo'] = sanitizar($this->request->getPost('correo'));
			if(empty($usuario['correo'])) return json_encode(['alerta' => 'falla', 'mensaje' => 'ALTO... Ingresa tu correo, para responderte lo antes posible.'], JSON_FORCE_OBJECT);
			
			$telefono = sanitizar($this->request->getPost('telefono'));
            // dd($telefono);
			$datos = [
				'nombre' => $nombre,
				'telefono' => !empty($telefono) ? $telefono : '',
				'correo' => $usuario['correo'],
				'asunto' => $asunto,
				'descripcion' => $descripcion
			];
		}
		model('Soporte')->insert($datos);
		$miRegistro = model('Soporte')->where('correo', $usuario['correo'])->orderBy('id', 'DESC')->first();
		
		return $miRegistro['id'];
	}
	public function enviarSoporte_archivos(){
		$id = sanitizar($this->request->getPost('id'));
		$imagenes = $this->request->getPost('imagenes');
		!empty($imagenes) ? $imagenes = json_encode($imagenes) : $imagenes = null;
		model('Soporte')->update($id, ['archivos' => $imagenes]);

		$correo = new Correos();
		$reporte = model('Soporte')->find($id);
		if(!$correo->enviar_Reporte($reporte)) return  json_encode(['alerta' => 'falla', 'mensaje' => 'OPPS... Veo que algo salió mal, pero no te preocupes, guardé tu mensaje para que pueda ser atendido.'], JSON_FORCE_OBJECT);
		
		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Listo ya envié tu mensaje, en breve el equipo le dará un seguimiento, yo me encargo de eso.'], JSON_FORCE_OBJECT);
	}
	// Funcion para ver noticia en front
	public function verNoticia(){
		if(!$_GET) return redirect()->to('');
		if(empty($_GET['id'])) return redirect()->to('');
		if(empty($_GET['title'])) return redirect()->to('');
		$id = sanitizar(base64_decode($_GET['id']));
		$noticia = $this->noticias->where(['id' => $id])->first();
		return view('/front/ver-noticia', array('noticia' => $noticia));
	}
	//Registrarme como
	public function selectorRegistro(){
		return view('front/selector-registro');
	}
	public function recuperarCuenta(){
	    if(session('logeado')) return redirect()->to('/usuarios/perfil');
	    return view('/front/reactivar-cuenta');
	}
	public function buscar_noticias(){
	    $texto = sanitizar($this->request->getPost('buscador_blog'));
	    if(empty($texto)) return json_encode(['alerta' => 'custom', 'mensaje' => 'Ingrese el texto a buscar'], JSON_FORCE_OBJECT);
	    
	    $noticias_buscadas = $this->noticias->like('titulo', $texto)->orLike('contenido', $texto)->orderBy('id DESC, titulo DESC')->get()->getResultArray();
	    if(empty($noticias_buscadas)) return json_encode(['alerta' => 'correcto', 'html' => vacio('No se encontraron resultados coincidentes'), 'total' => 0], JSON_FORCE_OBJECT);
	    
	    $html = "";
	    foreach($noticias_buscadas as $i => $noticia){
	        $html .= view_cell('App\Libraries\FuncionesSistema::tarjetas_Blog', ['noticia' => $noticia, 'conteo' => $i]);
	    }
	    
	    return json_encode(['alerta' => 'correcto', 'html' => $html, 'total' => count($noticias_buscadas)], JSON_FORCE_OBJECT);
	}
	public function resetar_filtrado_noticias(){
	    $noticias = $this->noticias->orderBy('id DESC, titulo DESC')->findAll();
        if(empty($noticias)) return json_encode(['alerta' => 'correcto', 'html' => vacio('Aún no hay noticias que mostrar'), 'total' => 0], JSON_FORCE_OBJECT);
	    
	    $html = "";
	    foreach($noticias as $i => $noticia){
	        $html .= view_cell('App\Libraries\FuncionesSistema::tarjetas_Blog', ['noticia' => $noticia, 'conteo' => $i]);
	    }
	    
	    return json_encode(['alerta' => 'correcto', 'html' => $html, 'total' => count($noticias)], JSON_FORCE_OBJECT);
	}
	public function ordernar_noticias(){
		$orden = sanitizar($this->request->getPost('filtro'));
		$buscando = sanitizar($this->request->getPost('buscando'));
        if(empty($orden)) return json_encode(['alerta' => 'custom', 'mensaje' => 'Seleccione el filtro correctamente'], JSON_FORCE_OBJECT);

        switch ($orden) {
        	case 'titulo': $ordenamiento = 'titulo ASC'; break;
        	case 'recientes': $ordenamiento = 'created_at DESC'; break;
        	case 'antiguos': $ordenamiento = 'created_at ASC'; break;
        	default:
        		return json_encode(['alerta' => 'correcto', 'html' => vacio('Error al encontrar los resultados')], JSON_FORCE_OBJECT);
        	break;
        }

        if(empty($buscando)) $noticias_filtradas = $this->noticias->orderBy($ordenamiento)->findAll();
        else $noticias_filtradas = $this->noticias->like('titulo', $buscando)->orLike('contenido', $buscando)->orderBy($ordenamiento)->findAll();
        if(empty($noticias_filtradas)) return json_encode(['alerta' => 'correcto', 'html' => vacio('No se encontraron resultado coincidentes')], JSON_FORCE_OBJECT);

        $html = "";
	    foreach($noticias_filtradas as $i => $noticia){
	        $html .= view_cell('App\Libraries\FuncionesSistema::tarjetas_Blog', ['noticia' => $noticia, 'conteo' => $i]);
	    }
	    
	    return json_encode(['alerta' => 'correcto', 'html' => $html, 'total' => count($noticias_filtradas)], JSON_FORCE_OBJECT);
	}
}