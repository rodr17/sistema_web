<?php namespace App\Controllers;

use App\Libraries\Correos;
use App\Libraries\NetPay;
use CodeIgniter\I18n\Time;

class Notificaciones extends BaseController{

	protected $usuarios, $suscripciones, $pagos;
	public function __construct(){
		$this->usuarios = model('Autenticacion');
		$this->suscripciones = model('Suscripciones');
		$this->pagos = model('Pagos');
	}

    public function eventos(){
        log_message('notice', 'Entró al evento');
	    $json = file_get_contents("php://input");
        log_message('notice', 'Datos evento '.$json);
	    $webhook = json_decode($json, true);
	    log_message('notice', 'Evento: '.$webhook['event']);
		
		switch($webhook['event']){
			case 'cep.paid': $this->crearSuscripcion($webhook); break;
			case 'subscription.paid': $this->crearSuscripcion($webhook); break;
			case 'subscription.payment_failed': $this->enviarCorreoPago_fallido($webhook); break; 
		}
    }

	private function crearSuscripcion(array $webhook){
		$id_transaccion = $webhook['data']['transactionTokenId'] ?? $webhook['data']['transactionId'];

		$netpay = new NetPay();
		$transaccion = $netpay->getTransaccion($id_transaccion);

		// PAGO EN EFECTIVO
		if(isset($webhook['data']['reference'])){
			$pagoBD = $this->pagos->where('id_transaccion', $id_transaccion)->first();
			if(empty($pagoBD)) return log_message('notice', 'No se encontró transactionId en BD PAGOS: ID '.$id_transaccion);

			$usuario = $this->usuarios->find($pagoBD['id_usuario']);
			if(empty($usuario)) return log_message('notice', 'No se encontró el registro del CLIENTE en BD PAGOS: WEBHOOK '.$webhook);

			// SE OBTIENE FECHA MENSUAL O ANUAL BBDD
			$planBD = model('ModelPlanes')->where('id_plan_mensual', $pagoBD['id_membresia'])->first();
			$recurrencia = isset($planBD['monto']) ? '+ 1 month': '';
			
			if(empty($planBD)){
				$planBD = model('ModelPlanes')->where('id_plan_anual', $pagoBD['id_membresia'])->first();
				$recurrencia = '+ 1 year';
			}

			$hoy = date('Y-m-d H:i:s', substr($webhook['createdAt'], 0, -3));
			$fecha_inicio = conversorZonaHoraria($hoy, app_timezone(), date_default_timezone_get());
			$fecha_fin = date('Y-m-d H:i:s', strtotime($fecha_inicio.$recurrencia));

			$this->pagos->save([
				'id' => $pagoBD['id'],
				'id_compra' => $transaccion['orderId'],
				'fecha' => Time::now()->toDateTimeString(),
				'estatus' => 'Activo'
			]);

			log_message('notice', "PAGO ACTUALIZADO EFECTIVO: ID".$pagoBD['id']);

			$suscripcionBD = $this->suscripciones->where('id_usuario', $pagoBD['id_usuario'])->first();
			$this->suscripciones->save([
				'id' => !empty($suscripcionBD)? $suscripcionBD['id'] : null,
				'id_usuario' => $pagoBD['id_usuario'],
				'id_plan' => $pagoBD['id_membresia'],
				'suscripcion' => null,
				'id_referencia' => $webhook['data']['reference'],
				'id_transaccion' => $id_transaccion,
				'estatus' => 'Activo',
				'fecha_inicio' => $fecha_inicio,
				'fecha_fin' => $fecha_fin,
				'renovacion' => true,
				'fecha_cancelacion' => null,
				'tarjeta_suscripcion' => null,
				'tipo_suscripcion' => 'efectivo',
				'cambio_pago' => null
			]);
			
			log_message('notice', 'PAGO SUSCRIPCION EXITOSO: '.$suscripcionBD['id']);

			if($usuario['plan'] != $pagoBD['id_membresia']) $this->usuarios->save(['id' => $pagoBD['id_usuario'], 'plan' => $pagoBD['id_membresia']]);

			return;
		}
		
		// PAGO CON TARJETA (SUSCRIPCION RENOVACION)
		if(empty($webhook['data']['client_id'])) return log_message('notice', 'No se encontró ID CLIENTE en NETPAY PAGOS: WEBHOOK '.$webhook);

		$usuario = $this->usuarios->where('id_cliente', $webhook['data']['client_id'])->first();
		if(empty($usuario)) return log_message('notice', 'No se encontró el registro del CLIENTE en BD PAGOS: WEBHOOK '.$webhook);

		if(empty($usuario['plan'])) return log_message('notice', 'El USUARIO ID: '.$usuario['id'].' no tiene plan en BD: WEBHOOK '.$webhook);

		// SE OBTIENE FECHA MENSUAL O ANUAL BBDD
		$planBD = model('ModelPlanes')->where('id_plan_mensual', $usuario['plan'])->first();
		$recurrencia = isset($planBD['monto']) ? '+ 1 month': '';
		
		if(empty($planBD)){
			$planBD = model('ModelPlanes')->where('id_plan_anual', $usuario['plan'])->first();
			$recurrencia = '+ 1 year';
		}

		$hoy = date('Y-m-d H:i:s', substr($webhook['createdAt'], 0, -3));
		$fecha_inicio = conversorZonaHoraria($hoy, app_timezone(), date_default_timezone_get());
		$fecha_fin = date('Y-m-d H:i:s', strtotime($fecha_inicio.$recurrencia));
		
		$id_pago = $this->pagos->insert([
			'id_usuario' => $usuario['id'],
			'id_compra' => $transaccion['orderId'],
			'id_membresia' => $usuario['plan'],
			'id_transacion' => $id_transaccion,
			'monto' => $webhook['data']['amount'],
			'fecha' => Time::now()->toDateTimeString(),
			'estatus' => 'Activo',
			'tipo' => 'tarjeta'
		]);

		log_message('notice', "PAGO CREADO SUSCRIPCION: $id_pago");

		$suscripcionBD = $this->suscripciones->where('id_usuario', $usuario['id'])->first();
		$this->suscripciones->save([
			'id' => !empty($suscripcionBD)? $suscripcionBD['id'] : null,
			'id_usuario' => $usuario['id'],
			'id_plan' => $usuario['plan'],
			'estatus' => 'Activo',
			'renovacion' => true,
			'fecha_inicio' => $fecha_inicio,
			'fecha_fin' => $fecha_fin,
			'fecha_cancelacion' => null,
			'tipo_suscripcion' => 'tarjeta',
			'cambio_pago' => null
		]);

		log_message('notice', 'PAGO SUSCRIPCION EXITOSO: '.$suscripcionBD['id']);
		
        // SE VALIDA SI SE USO CUPON Y SE VINCULA CON EL USO DEL CUPON		
		// if($pagoBD['monto'] != $monto_plan){
		//     $cuponBD = model('UsosCupones')->select('*, usos_cupones.id as id_uso_cupon')
		// 		->where(['usos_cupones.id_usuario' => $pagoBD['id_usuario']])
		// 		->like('id_compra', 'compra-')
		// 		->join('cupones', 'cupones.id = usos_cupones.id_cupon')
		// 		->orderBy('usos_cupones.id', 'DESC')->first();
				
		// 	if(!empty($cuponBD) && $cuponBD['tipo'] == 'Paga'){
		// 	    $id_copmpra_token = explode('-', $cuponBD['id_compra']);
		// 	    if($id_copmpra_token[1] == $pagoBD['id_membresia'] && ($id_copmpra_token[2] == $pagoBD['monto'])){
		// 	        model('UsosCupones')->save(['id' => $cuponBD['id_uso_cupon'], 'id_compra' => $pagoBD['id_compra']]);
		// 	    }
		// 	}
		// }
		
		$correo = new Correos();
        $correo->nuevoPago($id_transaccion);
	}

	private function enviarCorreoPago_fallido(array $webhook){
		$usuario = $this->usuarios->where('id_cliente', $webhook['data']['client_id'])->first();
		$suscripcion = $this->suscripciones->select('suscripcion, fecha_final, id_plan')->where('suscripcion', $webhook['data']['subscriptionId'])->first();
		$plan = model('ModelPlanes')->where('id_plan_mensual', $suscripcion['id_plan'])->orWhere('id_plan_anual', $suscripcion['id_plan'])->first();

		$correo = new Correos();
        $correo->renovacionFallida($suscripcion['fecha_fin'], $usuario, $plan, $webhook['data']['object']);
	}
}