<?php namespace App\Controllers;
use App\Libraries\Correos;
use App\Controllers\Suscripciones;
use App\Libraries\NetPay;
use CodeIgniter\I18n\Time;

class Pagos extends BaseController{
	protected $usuarios, $cupones, $usosCupones, $suscripciones;
	public function __construct(){
		helper('text');
		$this->usuarios = model('Autenticacion');
		$this->cupones = model('Cupones');
		$this->usosCupones = model('UsosCupones');
		$this->suscripciones = model('Suscripciones');
	}
	
	public function crear(){
		$tarjeta = model('Autenticacion')->guardarTarjetas();
		$respuesta_tarjeta = json_decode($tarjeta, true);
		if($respuesta_tarjeta['alerta'] != 'correcto') return $tarjeta;
		
		return $this->pagar(session('id'), $respuesta_tarjeta['tarjeta']);
	}

	public function pagar(string $id_usuario, string $tarjeta = null){
		$id_tarjeta = empty($tarjeta) ? trim($this->request->getPost('datos')) : $tarjeta;
		if(empty($id_tarjeta)) json_encode(['alerta' => 'falla', 'mensaje' => '¡UPS! Hubo un error en la tarjeta, inténtalo nuevamente, yo sé que puedo hacerlo'], JSON_FORCE_OBJECT);
		
		$id_plan = sanitizar($this->request->getPost('plan'));
		if(empty($id_plan)) json_encode(['alerta' => 'falla', 'mensaje' => '¡UPS! Hubo un error al elegir el plan, inténtalo nuevamente, yo sé que puedo hacerlo'], JSON_FORCE_OBJECT);

		$pago_creado = model('Pagos')->crear($id_usuario, $id_tarjeta, $id_plan);

		if(isset($pago_creado['message'])) return json_encode(['alerta' => 'falla', 'mensaje' => $pago_creado['message']], JSON_FORCE_OBJECT);

		if($pago_creado['status'] == 'success') return json_encode(['alerta' => 'correcto', 'mensaje' => '¡Enhorabuena! Ya tienes tu suscripción activa', 'datos' => $pago_creado], JSON_FORCE_OBJECT);

		if($pago_creado['status'] == 'failed') return json_encode(['alerta' => 'falla', 'mensaje' => $pago_creado['error'], 'datos' => $pago_creado], JSON_FORCE_OBJECT);
		
		return json_encode(['alerta' => 'custom', 'datos' => $pago_creado], JSON_FORCE_OBJECT);
	}

	public function confirmar_pago(){
		$id_transaccion = sanitizar($this->request->getPost('transaccion'));
		if(empty($id_transaccion)) return json_encode(['alerta' => 'custom', 'mensaje' => 'ID transacción vacío'], JSON_FORCE_OBJECT);

		$id_procesador = sanitizar($this->request->getPost('procesador'));
		if(empty($id_procesador)) return json_encode(['alerta' => 'custom', 'mensaje' => 'ID procesador vacío'], JSON_FORCE_OBJECT);
		
		$id_usuario = sanitizar($this->request->getPost('usuario'));
		if(empty($id_usuario)) return json_encode(['alerta' => 'custom', 'mensaje' => 'Usuario vacío'], JSON_FORCE_OBJECT);

		$id_plan = sanitizar($this->request->getPost('plan'));
		if(empty($id_usuario)) return json_encode(['alerta' => 'custom', 'mensaje' => 'No se envió el ID del plan'], JSON_FORCE_OBJECT);
		
		$netpay = new NetPay;
		$pago = $netpay->confirmarPago_3DS($id_transaccion, $id_procesador);

		if(isset($pago['message'])) return json_encode(['alerta' => 'falla', 'mensaje' => $pago['message']], JSON_FORCE_OBJECT);
		
		if($pago['status'] != 'success') return json_encode(['alerta' => 'falla', 'mensaje' => $pago['error']], JSON_FORCE_OBJECT);
		
		$transaccion = $netpay->getTransaccion($pago['transactionTokenId']);

		$pago_guardado = model('Pagos_unicos')->save([
			'id_usuario' => $id_usuario,
			'token_transaccion' => $pago['transactionTokenId'],
			'referencia' => $pago['billing']['merchantReferenceCode']
		]);

		model('Pagos')->insert([
			'id_usuario' => $id_usuario,
			'id_compra' => $transaccion['orderId'],
			'id_membresia' => $id_plan,
			'id_transacion' => $pago['transactionTokenId'],
			'monto' => $$pago_guardado['amount'],
			'fecha' => Time::now()->toDateTimeString(),
			'estatus' => 'Activo',
			'tipo' => 'tarjeta'
		]);
		$suscripcion_creada = model('Suscripciones')->crear_suscripcion($id_usuario, $pago_guardado['client']['paymentSource']['source'], $pago_guardado['amount']);
		
		if($suscripcion_creada['alerta'] != 'correcto') return json_encode($suscripcion_creada, JSON_FORCE_OBJECT);

		if(autorizacion() == 'contratante') return $this->usuarios->save(['id' => session('id'), 'estatus_registro' => 'completado']);
		if($pago_guardado) return json_encode(['alerta' => 'correcto', 'mensaje' => '¡Enhorabuena! Ya tienes tu suscripción activa', 'datos' => $pago], JSON_FORCE_OBJECT);

		return json_encode(['alerta' => 'falla', 'mensaje' => '¡UPS! No pude guardar la información del pago, por favor comunícate con nosotros para ayudarte'], JSON_FORCE_OBJECT);
	}

	public function compra_realizada(){
		$token = sanitizar($this->request->getGet('token'));
		$pago = model('Pagos_unicos')->where('token_transaccion', $token)->first();
		if(empty($pago)) return redirect()->to('');

		return view('front/compra-realizada');
	}

	public function crearCupon(){
		if(!session('is_logged')) return redirect()->to('/');
		if(session('rol') != 'admin') return redirect()->to('/');
		
		return view('/backend/administrador/crear-cupon');
	}
	
	public function saveCupon(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Inicie sesión para realizar la siguiente operación.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'No cuenta con los permisos necesarios para la siguiente operación.'], JSON_FORCE_OBJECT);
		
		$data['codigo'] = trim($this->request->getPost('codigo'));
		if(empty($data['codigo'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el código del cupón.'], JSON_FORCE_OBJECT);

		$data['rol'] = sanitizar($this->request->getPost('rol'));
		if(empty($data['rol'])) return json_encode(['tipo' => 'error', 'mensaje' => '¡UPS! no se encuentra el tipo de usuario'], JSON_FORCE_OBJECT);

		$cupon_duplicado = $this->cupones->where(['codigo' => $data['codigo'], 'rol' => $data['rol']])->first();
		if(!empty($cupon_duplicado)) return json_encode(['tipo' => 'error', 'mensaje' => 'Elige otro cupón, ya ingresaste el mismo condigo para el mismo tipo de usuario'], JSON_FORCE_OBJECT);
		
		$limite = sanitizar($this->request->getPost('switch-limite-total'));
		$data['limite'] = $limite == 'true' ? NULL : sanitizar($this->request->getPost('limite'));
		if(!empty($data['limite']) && $data['limite'] < 1) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la cantidad de usos del cupón mayor o igual a 1.'], JSON_FORCE_OBJECT);
		
		$data['tipo'] = sanitizar($this->request->getPost('tipo_cupon'));
		if(empty($data['tipo'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Seleccione el tipo de cupón.'], JSON_FORCE_OBJECT);
		
		$feclimite = trim($this->request->getPost('switch-vigencia'));
		$data['fecha_limite'] = empty($feclimite) ? sanitizar($this->request->getPost('vigencia')) : NULL;
		if(empty($feclimite) && empty($data['fecha_limite'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Eliga una fecha de vencimiento ó eliga fecha ilimitada.'], JSON_FORCE_OBJECT);
		
		if($data['tipo'] == 'Paga'){
			$data['descuento'] = sanitizar($this->request->getPost('descuento'));
			if(empty($data['descuento'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el descuento del cupón'], JSON_FORCE_OBJECT);

			$data['tipo_plan'] = sanitizar($this->request->getPost('tipo_plan'));
			if(empty($data['tipo_plan'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Selecciona el tipo de plan'], JSON_FORCE_OBJECT);
		}
		
		$this->cupones->save($data);
		return json_encode(['tipo' => 'success', 'mensaje' => 'Se ha creado el cupón de manera correcta'], JSON_FORCE_OBJECT);
	}
	
	public function saveEditCupon(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Inicie sesión para realizar la siguiente operación.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'No cuenta con los permisos necesarios para la siguiente operación.'], JSON_FORCE_OBJECT);
		
		$data['id'] = sanitizar($this->request->getPost('id'));
		if(empty($data['id'])) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el ID del cupón.'], JSON_FORCE_OBJECT);

		// $cuponBD = $this->cupones->find($data['id']);

		$data['codigo'] = trim($this->request->getPost('codigo'));
		if(empty($data['codigo'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el codigo del cupón.'], JSON_FORCE_OBJECT);
		
		// $data['rol'] = sanitizar($this->request->getPost('rol'));
		// if(empty($data['rol'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Seleccione el tipo de usuario'], JSON_FORCE_OBJECT);
		
		// $cupon_duplicado = $this->cupones->where(['codigo' => $data['codigo'], 'rol' => $data['rol']])->first();
		// if(!empty($cupon_duplicado)) return json_encode(['tipo' => 'error', 'mensaje' => 'Elige otro cupón, ya ingresaste el mismo código para el mismo tipo de usuario'], JSON_FORCE_OBJECT);
		
		$limite = sanitizar($this->request->getPost('switch-limite-total'));
		$data['limite'] = $limite == 'true' ? NULL : sanitizar($this->request->getPost('limite'));
		if(!empty($data['limite']) && $data['limite'] < 1) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la cantidad de usos del cupón mayor o igual a 1.'], JSON_FORCE_OBJECT);
		
		$data['tipo'] = trim($this->request->getPost('tipo_cupon'));
		if(empty($data['tipo'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Seleccione el tipo de cupón.'], JSON_FORCE_OBJECT);
		
		$feclimite = sanitizar($this->request->getPost('switch-vigencia'));
		$data['fecha_limite'] = empty($feclimite) ? sanitizar($this->request->getPost('vigencia')) : NULL;
		if(empty($feclimite) && empty($data['fecha_limite'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Eliga una fecha de vencimiento ó eliga fecha ilimitada.'], JSON_FORCE_OBJECT);
		
		if($data['tipo'] == 'Paga'){
			$data['descuento'] = sanitizar($this->request->getPost('descuento'));
			if(empty($data['descuento'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el descuento del cupón'], JSON_FORCE_OBJECT);

			$data['tipo_plan'] = sanitizar($this->request->getPost('tipo_plan'));
			if(empty($data['tipo_plan'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Selecciona el tipo de plan'], JSON_FORCE_OBJECT);
		}
		
		$this->cupones->save($data);
		return json_encode(['tipo' => 'success', 'mensaje' => 'Se ha actualizado el cupón de manera correcta.'], JSON_FORCE_OBJECT);
	}
	public function informacionCupon(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Inicie sesión para realizar la siguiente operación.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'No cuenta con los permisos necesarios para la siguiente operación.'], JSON_FORCE_OBJECT);
		
		$id = sanitizar($this->request->getPost('id'));
		if(empty($id)) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró obtener el id del cupón seleccionado.'], JSON_FORCE_OBJECT);
		
		$cupon = $this->cupones->where('id', $id)->first();
		if($cupon == null) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar información del cupón seleccionado.'], JSON_FORCE_OBJECT);
		
		$usos = $this->usosCupones->where('id_cupon', $id)->find();
		if($usos == null) return json_encode(['tipo' => 'success', 'mensaje' => '<div class="alert alert-dark" role="alert">Aún no se ha utilizado el cupón.</div>'], JSON_FORCE_OBJECT);
		$cantidad_utilizados = $this->usosCupones->selectCount('id_cupon')->where('id_cupon', $id)->countAllResults();
		$cantidad_canjeados = $this->usosCupones->selectCount('id_cupon')->where(['id_cupon' => $id, 'canjeado' => true])->countAllResults();
		return json_encode(['tipo' => 'success', 'mensaje' => view('/backend/administrador/viewcells/informacion-cupon', compact('cantidad_utilizados', 'cantidad_canjeados', 'cupon'))], JSON_FORCE_OBJECT);
	}
	
	public function dasctivarCupon(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Inicie sesión para realizar la siguiente operación.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'No cuenta con los permisos necesarios para la siguiente operación.'], JSON_FORCE_OBJECT);
		
		$id = sanitizar($this->request->getPost('id'));
		if(empty($id)) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró obtener el id del cupón seleccionado.'], JSON_FORCE_OBJECT);
		
		$cupon = $this->cupones->find($id);
		if(empty($cupon)) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar información del cupón seleccionado.'], JSON_FORCE_OBJECT);
		
		$this->cupones->where('id', $id)->set(['activo' => !$cupon['activo']])->update();
		
		if(!$cupon['activo']) return json_encode(['tipo' => 'success', 'mensaje' => 'El cupon ha sido activado nuevamente.'], JSON_FORCE_OBJECT);
		
		return json_encode(['tipo' => 'success', 'mensaje' => 'El cupon ha sido desactivado.'], JSON_FORCE_OBJECT);
	}
	
	public function eliminarCupon(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Inicie sesión para realizar la siguiente operación.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'No cuenta con los permisos necesarios para la siguiente operación.'], JSON_FORCE_OBJECT);
		
		if(trim($this->request->getPost('id')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró obtener el id del cupón seleccionado.'], JSON_FORCE_OBJECT);
		
		$cupon = $this->cupones->where('id', trim($this->request->getPost('id')))->first();
		if($cupon == null) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar información del cupón seleccionado.'], JSON_FORCE_OBJECT);
		
		if($this->cupones->delete(trim($this->request->getPost('id')))) return json_encode(['tipo' => 'success', 'mensaje' => 'El cupon ha sido eliminado de manera correcta.'], JSON_FORCE_OBJECT);
		
		return json_encode(['tipo' => 'error', 'mensaje' => 'Ha surgido un error, inténtelo nuevamente.'], JSON_FORCE_OBJECT);
	}
	
	public function editarCupon(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Inicie sesión para realizar la siguiente operación.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'No cuenta con los permisos necesarios para la siguiente operación.'], JSON_FORCE_OBJECT);
		
		$id_cupon = trim($this->request->getPost('id'));
		if(empty($id_cupon)) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró obtener el id del cupón seleccionado.'], JSON_FORCE_OBJECT);
		
		$cupon = $this->cupones->find($id_cupon);
		if(empty($cupon)) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar información del cupón seleccionado.'], JSON_FORCE_OBJECT);
		
		return json_encode(['tipo' => 'success', 'mensaje' => view('/backend/administrador/viewcells/editar-cupon', compact('cupon'))], JSON_FORCE_OBJECT);
	}
	
	public function listaCupones(){
		if(!session('is_logged')) return redirect()->to('/');
		if(session('rol') != 'admin') return redirect()->to('/');
		return view('/backend/administrador/lista-cupones');
	}
	
	public function tablaCupones(){
		$data = $this->cupones->orderBy('id', 'DESC')->findAll();
		return view('/backend/administrador/viewcells/tabla-cupones', array('cupones' => $data));
	}
	
	public function guardarTarjetas(){
		return $this->usuarios->guardarTarjetas();
	}
	
	public function get_tarjeta_user(){
		$id = trim($this->request->getPost('id'));
		$tarjeta = $this->usuarios->get_tarjeta_user($id);
		if(empty($tarjeta)) return json_encode(['alerta' => 'falla', 'mensaje' => 'OH No! No pude encontrar la tarjeta, por favor recarga la página e inténtalo nuevamente']);

		$t = [
			'id' => $tarjeta['token'],
			'brand' => $tarjeta['brand'],
			'numero' => $tarjeta['lastFourDigits'],
			'bank_name' => $tarjeta['bank'],
			'type' => $tarjeta['type'],
			'expiration_month' => $tarjeta['expMonth'],
			'expiration_year' => $tarjeta['expYear']
		];
		
		return json_encode([
			'alerta' => 'correcto',
			'listado' => view('/viewcells/tarjeta-listado', compact('t')),
			'selector' => view('/viewcells/tarjeta-selector', compact('t')),
			'mis_tarjetas' => view('/viewcells/tarjeta-mis-tarjetas', compact('t')),
		], JSON_FORCE_OBJECT);
	}
	
	public function eliminarTarjeta(){
		$id_token = trim($this->request->getPost('id'));
		$id_usuario = sanitizar($this->request->getPost('usuario'));
		if($this->usuarios->eliminarTarjeta($id_usuario, $id_token)) return json_encode(['alerta' => 'correcto', 'mensaje' => 'Listo... Eliminé los datos de la tarjeta, no la usare más.'], JSON_FORCE_OBJECT);

		return json_encode(['alerta' => 'falla', 'mensaje' => 'No se pudo eliminar'], JSON_FORCE_OBJECT);
	}
	
	public function listado_pagosSuscripcionesPaypal($id, $id_usuario){ //MODIFICAR A NETPAY PASARELA DE PAGOS
		$id = sanitizar($id); $id_usuario = sanitizar($id_usuario);
		$paypal = new Paypal();
		return $paypal->get_pagos_idSuscripcion($id, $id_usuario);
	}
	
	public function cambiarPago(){
		// 		$suscripciones = new Suscripciones();
		// 		$suscripciones->cancelar_suscripcion();
		$nuevo_pago = sanitizar($this->request->getPost('cambio_metodo_pago'));
		if(empty($nuevo_pago)) return redirect()->to('usuarios/perfil')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'UPPS... No pude encontrar el nuevo método de pago, por favor inténtalo nuevamente recargando la página, sé que puedo hacerlo.']);

		$solicitud = [
			'id_usuario' => session('id'),
			'fecha' => new Time('now', 'America/Monterrey', 'es_MX')
		];
		model('SolicitudesCambio')->save($solicitud);
		
		$usuario = model('Autenticacion')->get_usuarioID(session('id'));
		$suscripcion_actual = $this->suscripciones->where('id_usuario', session('id'))->first();
		$suscripcion_cancelada = $this->suscripciones->cancelar($suscripcion_actual['suscripcion']);
		$correo = new Correos();
		$correo->cancelarSuscripcion($usuario, $suscripcion_cancelada);
		
		$guardado = model('Suscripciones')->where('id_usuario', session('id'))->set(['cambio_pago' => $nuevo_pago, 'estatus' => 'Cancelada', 'renovacion' => false, 'fecha_cancelacion' => new Time('now', 'America/Monterrey', 'es_MX')])->update();
		if($guardado) return redirect()->to('usuarios/perfil')->with('alerta', ['tipo' => 'correcto', 'mensaje' => 'Listo... He cambiado la forma de pago, para la próxima fecha haré el cobro con esta nueva forma, gracias.']);
		else return redirect()->to('usuarios/perfil')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'OH NO... no pude hacer el cambio de pago, por favor inténtalo nuevamente, sé que puedo hacerlo.']);
	}
	
	public static function tipoPago($suscripcion){
		if($suscripcion == null) return 'Sin plan';

		return $suscripcion['tipo_suscripcion'];
	}
	
	public function cancelarCambioPago(){ //MODIFICAR A NETPAY PASARELA DE PAGOS
		$suscripcion_actual = $this->suscripciones->where('id_usuario', session('id'))->first();
		if(model('Suscripciones')->cancelarCambioPago($suscripcion_actual['suscripcion'])) return 'true';
		return 'false';
	}

	public function cambioMetodosPago(){
		$suscripcion_usuarios = model('Suscripciones')->where('cambio_pago !=', null)->find();
		if(empty($suscripcion_usuarios)) return 'Sin cambios de metodos de pago vigentes';

		foreach ($suscripcion_usuarios as $key => $suscripcion) {
			$con_suscripcion = Suscripciones::esta_suscrito_por_idUsuario($suscripcion['id_usuario']);
			if($con_suscripcion){
				echo "\nEl ID usuario: ".$suscripcion['id_usuario']." aún tiene suscripcion";
				continue;
			}

			$usuario = $this->usuarios->get_usuarioID($suscripcion['id_usuario']);
			$suscripciones = new Suscripciones();
			$suscripciones->cancelar_suscripcion($suscripcion['id_usuario']);

			$correos = new Correos();
			switch ($suscripcion['cambio_pago']) {
				case 'efectivo':
					$respuesta = model('Suscripciones')->crear_tienda($usuario['plan'], $suscripcion['id_usuario']);
					if(is_string($respuesta)) $correos->enviar_Contacto($usuario['correo'], "Referencia: $respuesta", $usuario);
					break;
				default://Tarjeta
					$respuesta = model('Suscripciones')->crear_suscripcion('true', $suscripcion['cambio_pago'], $suscripcion['id_usuario']);
					$suscripcion_creada = json_decode($respuesta, true);

					$correos->enviar_Contacto($usuario['correo'], "Cambio metodo de pago y cobro exitoso: ".$suscripcion_creada['mensaje'], $usuario);
					break;
			}
		}
	}

	public function actualizar_montos_variables(){
		$netpay = new NetPay();

		$suscripciones = $netpay->getSuscripciones();
		if(isset($suscripciones['message'])) return 'Error NETPAY: '.$suscripciones['message'];

		if(empty($suscripciones)) return 'Sin suscripciones creadas';
		
		$suscripciones_sinMontos = array_filter($suscripciones, function($suscripcion){ return $suscripcion['amount'] == 0;});
		if(empty($suscripciones_sinMontos)) return 'Sin suscripciones por actualizar';

		foreach($suscripciones_sinMontos as $suscripcion_sinMonto){

			// SE VERIFICA SI SE HAY CUPONES DE PAGA
			$usuario = $this->usuarios->select(['id', 'id_cliente'])->where('id_cliente', $suscripcion_sinMonto['client']['id'])->first();
			$cuponBD = $this->usosCupones
							->where(['usos_cupones.id_usuario' => $usuario['id']])
							->join('cupones', 'cupones.id = usos_cupones.id_cupon')
							->orderBy('usos_cupones.id', 'DESC')->first();

			if(!empty($cuponBD)){
				$cupon_validado = $this->validacion_cupon($usuario['id'], $cuponBD['id_cupon']);
				if(!isset($cupon_validado['mensaje']) && ($cuponBD['tipo'] == 'Paga')){
					$suscripcion_sinMonto['plan']['amount'] = ((100-$cuponBD['descuento']) * $suscripcion_sinMonto['plan']['amount']) / 100;
				}
			}

			//ACTUALIZA EL MONTO VARIABLE DE LA SUSCRIPCIÓN
			$suscripcion_actualizada = $netpay->actualizarSuscripcion($suscripcion_sinMonto['id'], $suscripcion_sinMonto['plan']['amount']);
			
			if(!isset($suscripcion_actualizada['amount'])){
				echo "\nNo se pudo actualizar la suscripción: ".$suscripcion_sinMonto['id'];
			}

			//CUPON CANJEADO
			if(!empty($cuponBD)){
				if(!isset($cupon_validado['mensaje']) && ($cuponBD['tipo'] == 'Paga')) $this->usosCupones->save(['id' => $cupon_validado['id_cupon_uso'], 'id_usuario', $usuario['id'], 'id_cupon' => $cuponBD['id'], 'canjeado', true, 'id_compra' => 'compra-'.$suscripcion_sinMonto['plan']['id'].'-'.$suscripcion_sinMonto['plan']['amount'].'-'.$usuario['id'].'-'.$cuponBD['id']]);
			}  
		}
		echo "\n ************** SUCRIPCIONES ACTUALIZADAS **************";
	}

	public function validarSus(){
		$suscripciones = model('Suscripciones')->findAll();
		if($suscripciones == null) return 'Sin suscripciones en la plataforma.';
		$correos = new Correos();
		foreach($suscripciones as $sus){
			if($sus['renovacion']) continue;
			$hoy = new Time('now', 'America/Monterrey', 'es_MX');
			$fin = new Time($sus['fecha_fin'], 'America/Monterrey', 'es_MX');
			if(strtotime($fin) > strtotime($hoy)) continue;
			$date1 = new \DateTime($hoy);
			$date2 = new \DateTime($fin);
			$diff = $date1->diff($date2);
			if($diff->days == 1){
				$user = model('Autenticacion')->get_id($sus['id_usuario']);
				$plan = model('ModelPlanes')->where('id_plan_mensual', $sus['id_plan'])->orWhere('id_plan_anual', $sus['id_plan'])->first();
				if($plan == NULL){
					$plan = model('ModelPlanes')->where('id_plan_mensual', $sus['id_plan'])->orWhere('id_plan_anual', $sus['id_plan'])->first();
				}
				if($plan == null) return 'El plan ya no existe.';
				$correos->suscripcionVencida($sus, $user, $plan);
			}
		}
	}

	public function datosFacturacionUsuario(){
		$id = sanitizar($this->request->getPost('id'));
		if($id == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logró encontrar el ID de referencia, comuníquese con el equipo de soporte.'], JSON_FORCE_OBJECT);
		$usuario = $this->usuarios->get_id(session('id'));
		
		return json_encode(['tipo' => 'correcto', 'mensaje' => view('viewcells/form-solicitud-factura', array('usuario' => $usuario, 'id' => $id))], JSON_FORCE_OBJECT);
	}
	
	public function utilizarCupon(){
		$cuponBD = $this->validacion_cupon(session('id'), $this->request->getPost('cupon'));
		if(isset($cuponBD['mensaje'])) return json_encode(['alerta' => 'falla', 'mensaje' => $cuponBD['mensaje']], JSON_FORCE_OBJECT);

		$usuarioBD = model('Autenticacion')->get_usuarioID(session('id'));
		$con_suscripcion = Suscripciones::esta_suscrito_por_idUsuario($usuarioBD['id']);

		$id_plan = $usuarioBD['estatus_registro'] != 'completado' ? session('plan') : $usuarioBD['plan']; 
		$datos_cupon_usado = ['id_usuario' => session('id'), 'id_cupon' => $cuponBD['id'], 'canjeado' => true];
		if($cuponBD['tipo'] == 'Gratuitos' && !$con_suscripcion){
			$datos = $this->crear_cuentaPremium($usuarioBD);
		}
		elseif($cuponBD['tipo'] == 'Paga'){
			if(empty($plan)) return json_encode(['alerta' => 'falla', 'mensaje' => 'No puedes utilizar este cupón para tu tipo de plan'], JSON_FORCE_OBJECT);
			
			$datos_cupon_usado['canjeado'] = false;
			$datos = $datos_cupon_usado;
			$datos['plan'] = model('Suscripciones')->informacion_plan($id_plan);
		}
		
		$datos['tipo'] = $cuponBD['tipo'];
		$this->usosCupones->save($datos_cupon_usado);
		session()->set(['cupon' => $cuponBD['id'], 'tipo_cupon' => $cuponBD['tipo']]);
		
		return json_encode(['alerta' => 'correcto', 'tipo_cupon' => $cuponBD['tipo'], 'cuerpo' => view('backend/viewcells/Informacion_cupon', $datos)], JSON_FORCE_OBJECT);
	}

	public function cupon(){
		return view('front/viewcells/boton-cupon', $this->request->is('POST') ? ['cupon' => trim($this->request->getPost('cupon'))] : []);
	}

	public function cuponCheckout(){
		$id_plan = trim($this->request->getPost('id_plan'));
		$es_tarjeta = sanitizar($this->request->getPost('es_tarjeta')) == 'true' ? true : false;
		if(empty($id_plan)) return json_encode(['alerta' => 'falla', 'mensaje' => 'OPPS... No encuentró el plan, recarga la página para intentarlo de nuevo'], JSON_FORCE_OBJECT);
		
		$cuponBD = $this->validacion_cupon(session('id'), $this->request->getPost('cupon'));
		if(isset($cuponBD['mensaje'])) return json_encode(['alerta' => 'falla', 'mensaje' => $cuponBD['mensaje']], JSON_FORCE_OBJECT);
		
		if($cuponBD['tipo'] != 'Paga') return json_encode(['alerta' => 'falla', 'mensaje' => 'OPPS... Lo siento, no puedes aplicar este cupón para un descuento en tu compra'], JSON_FORCE_OBJECT);

		$plan = model('Suscripciones')->informacion_plan($id_plan);
		$total = ((100 - $cuponBD['descuento']) * $plan['precio']) / 100;
		
		if(($plan['frecuencia'] == 'Mensual') && $es_tarjeta){
			$hoy = new \DateTime();
            $hoy->modify('last day of this month');
            $dias_restantes = ($hoy->format('d') - date('d')) == 0 ? 1 : ($hoy->format('d') - date('d'));
			$monto_ajustado = round(($plan['precio']/30) * $dias_restantes); //Precio del plan al dia actual (granel)
			$total = ((100 - $cuponBD['descuento']) * $monto_ajustado ) / 100; //Se hace descuento sobre el ajuste (granel)
		}
		return json_encode(['alerta' => 'correcto', 'descuento' => ($monto_ajustado ?? $plan['precio']) - $total, 'precio' => $total], JSON_FORCE_OBJECT);
	}

	public function crear_cuentaPremium(array $usuarioBD){
		$monto = model('ModelPlanes')->selectMax('monto')->where('cuenta', $usuarioBD['rol'])->get()->getRowArray()['monto'];
		$id_plan = model('ModelPlanes')->select('id_plan_mensual')->where(['cuenta' => $usuarioBD['rol'], 'monto' => $monto])->get()->getRowArray()['id_plan_mensual'];
		
		$fecha_inicio = date('Y-m-d H:i:s');
		$suscripcion_gratis = [
			'id_usuario' => $usuarioBD['id'],
			'id_plan' => $id_plan,
			'estatus' => 'Activo',
			'tipo_suscripcion' => 'cupon',
			'fecha_inicio' => $fecha_inicio,
			'fecha_fin' => date('Y-m-d', strtotime($fecha_inicio.'+ 1 month'))
		];
		if($this->suscripciones->insert($suscripcion_gratis)) return $suscripcion_gratis;
		return [];
	}

	public static function validacion_cupon(string $id_usuario, string $cupon){
		$cupon = trim($cupon);
		if(empty($cupon)) return ['alerta' => 'falla', 'mensaje' => 'Ingresa un cupón'];
		
		$usuarioBD = model('Autenticacion')->get_usuarioID($id_usuario);
		$cuponBD = model('Cupones')->where(['codigo' => $cupon, 'rol' => $usuarioBD['rol']])->first();
		if(empty($cuponBD)) return ['alerta' => 'falla', 'mensaje' => '¡OPSS! El cupón que ingresaste no existe'];

		if(!$cuponBD['activo']) return ['alerta' => 'falla', 'mensaje' => '¡OPSS! El cupón que ingresaste no está disponible'];
		
		if(!empty($cuponBD['fecha_limite'])){
			$hoy_unix = strtotime(date('Y-m-d'));
			if($hoy_unix > strtotime($cuponBD['fecha_limite'])) return ['alerta' => 'falla', 'mensaje' => '¡OPSS! el cupón ha expirado'];
		}
		
		if($cuponBD['rol'] != $usuarioBD['rol']) return ['alerta' => 'falla', 'mensaje' => '¡OPSS! No puedes utilizar este cupón'];
		
		$cupon_usado = model('UsosCupones')->where(['id_usuario' => $id_usuario, 'id_cupon' => $cuponBD['id']])->first();
		if($cupon_usado != null) return ['alerta' => 'falla', 'mensaje' => '¡Espera!... Anteriormente ya has utilizado este cupón'];
		
		$uso_cupon_gratuito = model('UsosCupones')
		->where(['usos_cupones.id_usuario' => $id_usuario, 'tipo' => 'Gratuitos', 'canjeado' => true])
		->join('cupones', 'cupones.id = usos_cupones.id_cupon')
		->orderBy('usos_cupones.id', 'DESC')
		->countAllResults();
		if($uso_cupon_gratuito) return ['alerta' => 'falla', 'mensaje' => '¡Espera!.. No se puede aplicar este cupón porque ya has tenido el beneficio de cuenta gratis ilimitada'];

		$usos_limite = model('UsosCupones')
		->where(['usos_cupones.canjeado' => true, 'cupones.id' => $cuponBD['id']])
		->join('cupones', 'cupones.id = usos_cupones.id_cupon')
		->orderBy('usos_cupones.id', 'DESC')
		->countAllResults();
		
		if(!empty($cuponBD['limite']) && ($usos_limite == $cuponBD['limite'])){
			model('Cupones')->save(['id' => $cuponBD['id'], 'activo' => false]);
			return ['alerta' => 'falla', 'mensaje' => '¡OPSS! El cupón que ingresaste no está disponible. Desactivado'];
		}

		$cuponBD['id_cupon_uso'] = isset($cupon_usado['id']) ? $cupon_usado['id'] : NULL;
		return $cuponBD;
	}

	public function resumenPagoModal(){
		$plan = sanitizar($this->request->getGet('plan'));
		if(empty($plan)) $user = model('Autenticacion')->get_id(session('id'));
		else $user['plan'] = $plan;
		
		return view_cell('App\Libraries\FuncionesSistema::resumenCompraPlanModal', ['id' => $user['plan']]);
	}
}