<?php namespace App\Controllers;
use App\Libraries\NetPay;
class Pdf extends BaseController{
    protected $usuarios;

	public function __construct(){
        helper('funciones');
		helper('text');
		$this->usuarios = model('Autenticacion');
	}
	
	public function tarjeta(){
	    if(!isset($_GET['id'])) return 'Ingrese por favor el id de la compra';
	    $id_suscripcion = trim($_GET['id']);
        $pago = model('Pagos')->where('id_compra', $id_suscripcion)->first();
        if($pago == null) return 'No se encontró el pago.';
        if($pago['tipo'] == 'efectivo') return 'El apartado al que intenta acceder es solamente para los pagos a traves de tarjetas de debito/credito.';
        
        $usuarioBD = $this->usuarios->where('id', $pago['id_usuario'])->first();
        if($usuarioBD == null) return 'No se encontró el usuario.';
        
        $plan = model('ModelPlanes')->where('id_plan_mensual', $pago['id_membresia'])->first();
        if($plan == null){
            $plan = model('ModelPlanes')->where('id_plan_anual', $pago['id_membresia'])->first();
            $tipo = 'anual';
        }else{
            $tipo = 'mensual';
        }

        $netpay = new NetPay();
        $tarjeta_suscripcion = $netpay->getTarjetaCliente($usuarioBD['id_cliente'] , $id_suscripcion);
        if(isset($tarjeta_suscripcion['message'])) return vacio('¡UPS! No pude encontrar la información de tu pago, anímate a comunicarte con nosotros para ayudarte.');

        return view('pago-tarjeta', array('pago' => $pago, 'usuario' => $usuarioBD, 'plan' => $plan, 'tipo' => $tipo, 'tarjeta' => $tarjeta_suscripcion['lastFourDigits']));
	}
	
	public function efectivo(){
	    if(!$_GET) return 'Ingrese por favor el id de la compra';
	    $id_compra = $_GET['id'];
        $pago = model('Pagos')->where('id_compra', $id_compra)->first();
        if($pago == null) return 'No se encontro el pago.';
        if($pago['tipo'] != 'efectivo') return 'El apartado al que intenta acceder es solamente para los pagos a través de tarjetas de debito/credito.';
        $user = $this->usuarios->where('id', $pago['id_usuario'])->first();
        if($user == null) return 'No se encontró el usuario.';
        
        $plan = model('ModelPlanes')->where('id_paypal', $pago['id_membresia'])->first();
        if($plan == null){
            $plan = model('ModelPlanes')->where('id_paypal_anual', $pago['id_membresia'])->first();
            $tipo = 'anual';
        }else{
            $tipo = 'mensual';
        }
        return view('pago-paypal', array('pago' => $pago, 'usuario' => $user, 'plan' => $plan, 'tipo' => $tipo));
	}
	
	public function html2pdf(){
	    return redirect()->to('/');
        $html2pdf = new \Spipu\Html2Pdf\Html2Pdf();
        $html2pdf->pdf->SetAuthor('Mextemps');
        $html2pdf->pdf->SetTitle('Recibo de pago');

        $html2pdf->pdf->SetDisplayMode('fullpage');
        
        $pago = model('Pagos')->where('tipo', 'tarjeta')->first();
        if($pago == null) return 'No se encontró el pago.';
        $user = $this->usuarios->where('id', $pago['id_usuario'])->first();
        if($user == null) return 'No se encontró el usuario.';
        $html2pdf->writeHTML(view('pdf_view', array('pago' => $pago, 'usuario' => $user)));
        $html2pdf->output();
        exit();
	}
}