<?php namespace App\Controllers;

use App\Libraries\Correos;
use CodeIgniter\I18n\Time;
use App\Libraries\AccesosPlanes;
use App\Libraries\FuncionesSistema;

class Proyectos extends BaseController{
	protected $proyectos, $postulaciones, $salas, $usuarios, $solicitudes, $calificaciones, $habilidades;

	public function __construct(){
		$this->proyectos = model('Proyectos');
		$this->postulaciones = model('Postulaciones');
		$this->salas = model('Salas');
		$this->usuarios = model('Autenticacion');
		$this->solicitudes = model('Solicitudes');
		$this->calificaciones = model('Calificaciones');
		$this->habilidades = model('Habilidades');
		helper('funciones');
		helper('text');
	}
	// Funcion para crear nuevo proyecto
	public function crear(){
		$usuario = $this->usuarios->get_usuarioID(session('id'));
		$permisos = new AccesosPlanes($usuario['plan']);

		// Validación de proyectos activos
		$proyectos = $this->proyectos->where(['id_contratista' => session('id'), 'estatus' => 'espera'])->find();
		if($usuario['plan'] == null){
		    if(Suscripciones::esta_suscrito_por_idUsuario($usuario['id'])); //Deja seguir el flujo si tiene CUENTA PREMIUM CON CUPON
			elseif(count($proyectos) >= 1) return json_encode(['tipo' => 'modal', 'mensaje' => 'Solo puedes tener 1 proyecto activo. Adquiere un plan PREMIUM para postulaciones ilimitadas']);
		}else{
			if(count($proyectos) >= $permisos->getValor_permiso('proyectosActivos')) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Solo puedes tener '.$permisos->getValor_permiso('proyectosActivos').' proyectos activos. Adquiere un plan PREMIUM para postulaciones ilimitadas']);
		}
		
		$area = sanitizar($this->request->getPost('area'));
		if($area == null) return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Selecciona una categoría, de esta forma puedes mostrar tu proyecto al talento adecuado.', 'seccion' => 'nav-titulo-tab'], JSON_FORCE_OBJECT);
		$area_subarea = explode(' - ', $area); //$area_subarea[0] = habilidad $area_subarea[1] = area

		$indice = $this->request->getPost('nivel_requerido');
		if($indice == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Faltó el nivel que requieres para tu proyecto.', 'seccion' => 'nav-titulo-tab'], JSON_FORCE_OBJECT);
		
		$titulo = sanitizar($this->request->getPost('titulo'));
		if($titulo == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Requiero un Título para el proyecto.', 'seccion' => 'nav-cats-tab'], JSON_FORCE_OBJECT);
		
		$descripcion = sanitizar($this->request->getPost('descripcion'));
		if($descripcion == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Te faltó la descripción, recuerda ser lo más claro posible.', 'seccion' => 'nav-cats-tab'], JSON_FORCE_OBJECT);
		
		$corta = sanitizar($this->request->getPost('corta'));
		if(empty($corta)) return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Te faltó la descripción corta, es importante para atraer más talento a tu proyecto.', 'seccion' => 'nav-cats-tab'], JSON_FORCE_OBJECT);
		
		$fecha = sanitizar($this->request->getPost('fecha'));
		if($fecha == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Es muy importante la fecha de entrega del proyecto, ¿Puedes agregarla? Gracias.', 'seccion' => 'nav-final-tab'], JSON_FORCE_OBJECT);
		
		$arranque = sanitizar($this->request->getPost('arranque'));
		if($arranque == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Solo dime cuando vamos a arrancar el proyecto, te faltó la fecha de arranque.', 'seccion' => 'nav-final-tab'], JSON_FORCE_OBJECT);
		
		$presupuesto = sanitizar($this->request->getPost('presupuesto'));
		$toggle_presupuesto = sanitizar($this->request->getPost('toggle_presupuesto'));
		if($presupuesto == '' && empty($toggle_presupuesto)) return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Por favor ingresa un presupuesto para este proyecto, esto te ayuda bastante para atraer al talento que buscas.', 'seccion' => 'nav-final-tab'], JSON_FORCE_OBJECT);

		$nivelesProyecto = [0 => 'Básico', 1 => 'Intermedio', 2 => 'Avanzado', 3 => 'Experto', 4 => 'Platino', 5 => 'Diamante'];
		$files = $this->request->getFiles('galeria');
		$usuarioBD = $this->usuarios->get_id(session('id'));
		$usuario = $usuarioBD['id']."_".$usuarioBD['correo'];
		
		
		$directorio = WRITEPATH."uploads/Usuarios/".$usuario."/Proyectos";
		$destino = $directorio."/".$titulo;
		
		if(!file_exists(WRITEPATH.'uploads/Usuarios/'.$usuario)) mkdir(WRITEPATH.'uploads/Usuarios/'.$usuario);
		if(!file_exists($directorio)) mkdir($directorio);
		if(!file_exists($destino)) mkdir($destino);
		
		$imagenes = array();
		$a = $files['galeria']['0']->getName();
		if($a != null){
			foreach ($files['galeria'] as $key => $foto) {
				$nuevo_nombre = $foto->getRandomName();
				$extension = $foto->getExtension();
				
				$foto->store('Usuarios/'.session('id').'_'.session('correo').'/Proyectos/'.$titulo, $nuevo_nombre);
				$img_anterior = $destino."/".$nuevo_nombre;
				$imgNombreNuevo = explode('.', $nuevo_nombre);
				
				if($extension == 'webp'){ array_push($imagenes, $nuevo_nombre); continue;}
				switch ($extension){
					case 'jpeg':
						imagewebp(imagecreatefromjpeg($img_anterior), $destino."/".$imgNombreNuevo[0].'.webp');
					break;
					case 'jpg':
						$imagen = @imagecreatefromjpeg($img_anterior);
						if (!$imagen) {
							imagecreatefromstring(file_get_contents($img_anterior));
						}else imagewebp(imagecreatefromjpeg($img_anterior), $destino."/".$imgNombreNuevo[0].'.webp');
					break;	
					case 'gif':
						imagewebp(imagecreatefromgif($img_anterior), $destino."/".$imgNombreNuevo[0].'.webp');
					break;
					case 'png':
						$img = imagecreatefrompng($img_anterior);
						imagepalettetotruecolor($img);
						imagealphablending($img, true);
						imagesavealpha($img, true);
						imagewebp($img, $destino."/".$imgNombreNuevo[0].'.webp');
						imagedestroy($img);
					break;					
					case 'bmp':
						imagewebp(imagecreatefromwbmp($img_anterior), $destino."/".$imgNombreNuevo[0].'.webp');
					break;			
				}
				unlink($img_anterior);
				array_push($imagenes, $imgNombreNuevo[0].'.webp');
			}
		}
		$presupuesto = str_replace(',', '', $presupuesto);
		$save = [
			'id_contratista' => session('id'),
			'titulo' => $titulo,
			'desc_corta	' => character_limiter($corta, 200, ''),
			'descripcion' => $descripcion,
			'categoria' => $area_subarea[1],
			'subcategoria' => $area_subarea[0],
			'nivel' => $nivelesProyecto[$indice],
			'fecha_arranque_esperado' => $arranque,
			'fecha_entrega' => $fecha,
			'presupuesto' => empty($presupuesto) ? 0 : $presupuesto,
			'estatus' => 'espera',
			'imagenes' => json_encode($imagenes),
			'created_at' => new Time('now', app_timezone(), 'es_MX'),
		];
		$db = db_connect();
		$builder = $db->table('proyectos');
		$builder->insert($save);
		// $this->proyectos->save($save);
		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Listo... Tu proyecto ya lo publiqué y me aseguraré de mostrarlo.'], JSON_FORCE_OBJECT);
	}
	//Funcion para guardar cambios de edicion de proyecto
	public function saveEditProject(){
		if(session('rol') != 'contratante') return redirect()->to('ingresar')->with('alerta', ['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.']);

		$usuarioBD = $this->usuarios->get_usuarioID(session('id'));
		$permisos = new AccesosPlanes($usuarioBD['plan']);

		$frees = sanitizar($this->request->getPost('frees'));
		// if($frees == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... dime qué freelancers voy a mandar la solicitud de tu proyecto.', 'seccion' => 'modal']);
		$lista = array_filter(explode (',', $frees));
		// Validación de cantidad permitida de envio de solicitudes directas a freelancers
		$cantidad_envioSolicitudes = $permisos->getValor_permiso('enviosolicitudes');

		if(empty($usuarioBD['plan'])){
		    if(Suscripciones::esta_suscrito_por_idUsuario($usuarioBD['id'])); //Deja seguir el flujo si tiene CUENTA PREMIUM CON CUPON
			elseif($cantidad_envioSolicitudes == 0 && count($lista) > 1) return json_encode(['tipo' => 'modalsus', 'mensaje' => 'Solo se permiten como máximo 1 envío de solicitud por proyecto. Adquiere un plan PREMIUM para enviar solicitudes ilimitadas', 'seccion' => 'modal']);
		}else{
			if($cantidad_envioSolicitudes+1 < count($lista)) return json_encode(['tipo' => 'modalsus', 'mensaje' => 'Solo se permiten como máximo '.$cantidad_envioSolicitudes.' envío de solicitudes por proyecto. Adquiere un plan PREMIUM para enviar solicitudes ilimitadas', 'seccion' => 'modal']);
		}

		$area = sanitizar($this->request->getPost('area'));
		if($area == null) return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Selecciona una categoría, de esta forma puedes mostrar tu proyecto al talento adecuado.', 'seccion' => 'nav-titulo-tab'], JSON_FORCE_OBJECT);
		$area_subarea = explode(' - ', $area); //$area_subarea[0] = habilidad $area_subarea[1] = area

		$titulo = sanitizar($this->request->getPost('titulo'));
		if($titulo == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Requiero un Título para el proyecto.', 'seccion' => 'nav-cats-tab'], JSON_FORCE_OBJECT);
		
		$descripcion = sanitizar($this->request->getPost('descripcion'));
		if($descripcion == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Te faltó la descripción, recuerda ser lo más claro posible.', 'seccion' => 'nav-cats-tab'], JSON_FORCE_OBJECT);
		
		$corta = sanitizar($this->request->getPost('corta'));
		if($corta == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Te faltó la descripción corta, es importante para atraer más talento a tu proyecto.', 'seccion' => 'nav-cats-tab'], JSON_FORCE_OBJECT);
		
		$fecha = sanitizar($this->request->getPost('fecha'));
		if($fecha == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Es muy importante la fecha de entrega del proyecto, ¿Puedes agregarla? Gracias.', 'seccion' => 'nav-final-tab'], JSON_FORCE_OBJECT);
		
		$arranque = sanitizar($this->request->getPost('arranque'));
		if($arranque == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Solo dime cuando vamos a arrancar el proyecto, te faltó la fecha de arranque.', 'seccion' => 'nav-final-tab'], JSON_FORCE_OBJECT);
		
		$presupuesto = sanitizar($this->request->getPost('presupuesto'));
		if($presupuesto == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Por favor ingresa un presupuesto para este proyecto, esto te ayuda bastante para atraer al talento que buscas.', 'seccion' => 'nav-final-tab'], JSON_FORCE_OBJECT);
		
		$id = sanitizar($this->request->getPost('id'));
		$proyecto = $this->proyectos->find($id);
		
		$files = $this->request->getFiles('galeria');
		$usuarioBD = $this->usuarios->get_id(session('id')); 
		$usuario = $usuarioBD['id']."_".$usuarioBD['correo'];
		$directorio = WRITEPATH."uploads/Usuarios/".$usuario."/Proyectos";
		$destino = $directorio."/".$titulo;
		
		if(!file_exists($directorio)) mkdir($directorio);
		if(!file_exists($destino)) mkdir($destino);
		$imagenes = json_decode($proyecto['imagenes'], true);
		$ligaant = WRITEPATH."uploads/Usuarios/".$usuario."/Proyectos/".$proyecto['titulo'];
		foreach($imagenes as $imgs){
			$current = $ligaant.'/'.$imgs;
			rename($current, $destino.'/'.$imgs);
		}
		if($files['galeria'][0]->getName() != ''){
			foreach ($files['galeria'] as $key => $foto) {
				$nuevo_nombre = $foto->getRandomName();
				$extension = $foto->getExtension();
				
				$foto->store('Usuarios/'.$usuario.'/Proyectos/'.$titulo, $nuevo_nombre);
				$img_anterior = $destino."/".$nuevo_nombre;
				$imgNombreNuevo = explode('.', $nuevo_nombre);
				// dd($destino."/".$imgNombreNuevo[0].'.webp');
				if($extension == 'webp'){ array_push($imagenes, $nuevo_nombre); continue;}
				switch ($extension){
					case 'jpeg':
							imagewebp(imagecreatefromjpeg($img_anterior), $destino."/".$imgNombreNuevo[0].'.webp');
						break;
					case 'jpg':
							imagewebp(imagecreatefromjpeg($img_anterior), $destino."/".$imgNombreNuevo[0].'.webp');
						break;
					case 'gif':
							imagewebp(imagecreatefromgif($img_anterior), $destino."/".$imgNombreNuevo[0].'.webp');
						break;
					case 'png':
							$img = imagecreatefrompng($img_anterior);
							imagepalettetotruecolor($img);
							imagealphablending($img, true);
							imagesavealpha($img, true);
							imagewebp($img, $destino."/".$imgNombreNuevo[0].'.webp');
							imagedestroy($img);
						break;
					case 'bmp':
							imagewebp(imagecreatefromwbmp($img_anterior), $destino."/".$imgNombreNuevo[0].'.webp');
						break;
				}
				unlink($img_anterior);
				array_push($imagenes, $imgNombreNuevo[0].'.webp');
			}
		}

		if(!empty($lista)){
			$Correo = new Correos();
			foreach($lista as $l){
				$freelance = $this->usuarios->find($l);
				$soli = [
					'id_proyecto' => $id,
					'id_freelancer' => $l,
					'estatus' => 'espera'
				];
				$this->solicitudes->insert($soli);
				$Correo->solicitudProyecto($freelance['correo']);
			}
		}

		$save = [
		    'id' => $id,
			'titulo' => $titulo,
			'desc_corta' => character_limiter($corta, 200, ''),
			'descripcion' => $descripcion,
			'categoria' => $area_subarea[1],
			'subcategoria' => $area_subarea[0],
			'fecha_arranque_esperado' => $arranque,
			'fecha_entrega' => $fecha,
			'presupuesto' => str_replace(',', '', $presupuesto),
			'imagenes' => json_encode($imagenes),
		];
		$this->proyectos->save($save);
		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Listo... Tu proyecto ya lo actualicé y me aseguraré de mostrarlo', 'id' => $id], JSON_FORCE_OBJECT);
	}
	public function EliminarImagen(){
		$id = sanitizar($this->request->getPost('id'));
		$img = sanitizar($this->request->getPost('img'));
		$proyecto = $this->proyectos->find($id);
		$imagenes = json_decode($proyecto['imagenes'], true);
		
		$nuevas = array();
		foreach($imagenes as $im){
			if($im != $img) array_push($nuevas, $im);
		}
		
		$this->proyectos->where('id', $id)->set(['imagenes' => json_encode($nuevas, JSON_FORCE_OBJECT)])->update();
		return view_cell('App\Libraries\FuncionesSistema::carruselProyectEdit', ['id' => $id]);
	}
	//Funcion para crear solicitud de proyecto
	public function crearSolicitudes(){
		$usuarioBD = $this->usuarios->get_usuarioID(session('id'));
		$permisos = new AccesosPlanes($usuarioBD['plan']);
		
		$frees = sanitizar($this->request->getPost('frees'));
		if($frees == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Solo dime qué freelancers voy a mandar la solicitud de tu proyecto.', 'seccion' => 'modal']);
		$lista = array_filter(explode (',', $frees));
		// Validación de cantidad permitida de envio de solicitudes directas a freelancers
		$cantidad_envioSolicitudes = $permisos->getValor_permiso('enviosolicitudes');
		
		// Validación de proyectos activos
		$proyectos = $this->proyectos->where(['id_contratista' => session('id'), 'estatus !=' => 'finalizado'])->find();
		if(empty($usuarioBD['plan'])){
		    if(Suscripciones::esta_suscrito_por_idUsuario($usuarioBD['id'])); //Deja seguir el flujo si tiene CUENTA PREMIUM CON CUPON
			elseif(count($proyectos) >= 1) return json_encode(['tipo' => 'modalsus', 'mensaje' => 'Solo puedes tener 1 proyecto activo. Adquiere un plan PREMIUM para tener proyectos ilimitadas']);
			elseif($cantidad_envioSolicitudes == 0 && count($lista) > 1) return json_encode(['tipo' => 'modalsus', 'mensaje' => 'Solo se permiten como máximo 1 envío de solicitud por proyecto. Adquiere un plan PREMIUM para enviar solicitudes ilimitadas', 'seccion' => 'modal']);
		}else{
			if(count($proyectos) >= $permisos->getValor_permiso('proyectosActivos')) return json_encode(['tipo' => 'modalsus', 'mensaje' => 'Solo puedes tener '.$permisos->getValor_permiso('proyectosActivos').' proyectos activos. Adquiere un plan PREMIUM para tener proyectos ilimitadas']);
			if($cantidad_envioSolicitudes+1 < count($lista)) return json_encode(['tipo' => 'modalsus', 'mensaje' => 'Solo se permiten como máximo '.$cantidad_envioSolicitudes.' envío de solicitudes por proyecto. Adquiere un plan PREMIUM para enviar solicitudes ilimitadas', 'seccion' => 'modal']);
		}
		
		$area = sanitizar($this->request->getPost('area'));
		if($area == null) return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Selecciona una categoría, de esta forma puedes mostrar tu proyecto al talento adecuado.', 'seccion' => 'nav-titulo-tab'], JSON_FORCE_OBJECT);
		$area_subarea = explode(' - ', $area); //$area_subarea[0] = habilidad $area_subarea[1] = area
		
		$indice = sanitizar($this->request->getPost('nivel_requerido'));
		$nivelesProyecto = [0 => 'Básico', 1 => 'Intermedio', 2 => 'Avanzado', 3 => 'Experto', 4 => 'Platino', 5 => 'Diamante'];
		
		$titulo = sanitizar($this->request->getPost('titulo'));
		if($titulo == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Requiero un Título para el proyecto.', 'seccion' => 'nav-cats-tab'], JSON_FORCE_OBJECT);
		
		$descripcion = sanitizar($this->request->getPost('descripcion'));
		if($descripcion == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Te faltó la descripción, recuerda ser lo más claro posible.', 'seccion' => 'nav-cats-tab'], JSON_FORCE_OBJECT);
		
		$corta = sanitizar($this->request->getPost('corta'));
		if(empty($corta)) return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Te faltó la descripción corta, es importante para atraer más talento a tu proyecto.', 'seccion' => 'nav-cats-tab'], JSON_FORCE_OBJECT);
		
		$fecha = sanitizar($this->request->getPost('fecha'));
		if($fecha == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Es muy importante la fecha de entrega del proyecto, ¿Puedes agregarla? Gracias.', 'seccion' => 'nav-final-tab'], JSON_FORCE_OBJECT);
		
		$arranque = sanitizar($this->request->getPost('arranque'));
		if($arranque == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Solo dime cuando vamos a arrancar el proyecto, te faltó la fecha de arranque.', 'seccion' => 'nav-final-tab'], JSON_FORCE_OBJECT);
		
		$presupuesto = sanitizar($this->request->getPost('presupuesto'));
		if($presupuesto == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Por favor ingresa un presupuesto para este proyecto, esto te ayuda bastante para atraer al talento que buscas.', 'seccion' => 'nav-final-tab'], JSON_FORCE_OBJECT);

		//	if(empty($indice)) return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Faltó el nivel que requieres para tu proyecto', 'seccion' => 'nav-titulo-tab'], JSON_FORCE_OBJECT);
		//	if($indice < 0 || $indice > 4) return json_encode(['tipo' => 'custom', 'mensaje' => 'OPPS... Seleccione correctamente el nivel para su proyecto. Recargue la página si es necesario.', 'seccion' => 'nav-titulo-tab'], JSON_FORCE_OBJECT);
		
		$files = $this->request->getFiles('galeria');
		$imagenes = array();
		$a = $files['galeria']['0']->getName();
		if($a != null){
			$usuario = $usuarioBD['id']."_".$usuarioBD['correo'];
			$directorio = WRITEPATH."uploads/Usuarios/".$usuario."/Proyectos";
			$destino = $directorio."/".$titulo;

			if(!file_exists($directorio)) mkdir($directorio);
			if(!file_exists($destino)) mkdir($destino);

			foreach ($files['galeria'] as $key => $foto) {
				$nuevo_nombre = $foto->getRandomName();
				$extension = $foto->getExtension();

				$foto->store('Usuarios/'.session('id').'_'.session('correo').'/Proyectos/'.$titulo, $nuevo_nombre);
				$img_anterior = $destino."/".$nuevo_nombre;
				$imgNombreNuevo = explode('.', $nuevo_nombre);

				if($extension == 'webp'){ array_push($imagenes, $nuevo_nombre); continue;}
				switch ($extension){
					case 'jpeg':
						imagewebp(imagecreatefromjpeg($img_anterior), $destino."/".$imgNombreNuevo[0].'.webp');
						break;
					case 'jpg':
						$imagen = @imagecreatefromjpeg($img_anterior);
						if (!$imagen) {
							imagecreatefromstring(file_get_contents($img_anterior));
						}else imagewebp(imagecreatefromjpeg($img_anterior), $destino."/".$imgNombreNuevo[0].'.webp');
					break;
					case 'gif':
						imagewebp(imagecreatefromgif($img_anterior), $destino."/".$imgNombreNuevo[0].'.webp');
						break;
					case 'png':
						$img = imagecreatefrompng($img_anterior);
						imagepalettetotruecolor($img);
						imagealphablending($img, true);
						imagesavealpha($img, true);
						imagewebp($img, $destino."/".$imgNombreNuevo[0].'.webp');
						imagedestroy($img);
						break;					
					case 'bmp':
						imagewebp(imagecreatefromwbmp($img_anterior), $destino."/".$imgNombreNuevo[0].'.webp');
						break;			
				}
				unlink($img_anterior);
				array_push($imagenes, $imgNombreNuevo[0].'.webp');
			}
		}
		$save = [
			'id_contratista' => session('id'),
			'titulo' => $titulo,
			'desc_corta	' => $corta,
			'descripcion' => $descripcion,
			'categoria' => $area_subarea[1],
			'subcategoria' => $area_subarea[0],
			'nivel' => $nivelesProyecto[$indice],
			'fecha_arranque_esperado' => $arranque,
			'fecha_entrega' => $fecha,
			'presupuesto' => str_replace(',', '', $presupuesto),
			'estatus' => 'espera',
			'imagenes' => json_encode($imagenes),
			'created_at' => new Time('now', app_timezone(), 'es_MX'),
		];
		
		db_connect()->table('proyectos')->insert($save);

		$lastproyecto = $this->proyectos->where(['id_contratista' => session('id')])->orderBy('id', 'DESC')->limit(1)->first();
		$Correo = new Correos();

		foreach($lista as $l){
			$freelance = $this->usuarios->find($l);
			$soli = [
				'id_proyecto' => $lastproyecto['id'],
				'id_freelancer' => $l,
				'estatus' => 'espera'
			];
			$this->solicitudes->save($soli);
			$Correo->solicitudProyecto($freelance['correo']);
		}

		return json_encode(['tipo' => 'correcto', 'mensaje' => 'Listo... Ya publiqué tu proyecto y mandé un mensaje al los perfiles que me seleccionaste, muy pronto te contactarán, yo me encargo.'], JSON_FORCE_OBJECT);
	}
	// Funcion para compartir solicitud de proyecto
	public function shareSoliProyecto(){
		$frees = trim($this->request->getPost('frees'));
		$soli = sanitizar($this->request->getPost('soli'));
		if($soli == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Lo sentimos, no logramos encontrar la solicitud que deseas compartir.'], JSON_FORCE_OBJECT);
		$solicitud = $this->solicitudes->where('id', $soli)->first();
		if($solicitud == null) return json_encode(['tipo' => 'error', 'mensaje' => 'Lo sentimos, no logramos encontrar la solicitud que deseas compartir.'], JSON_FORCE_OBJECT);
		if($frees == '') return json_encode(['tipo' => 'error', 'mensaje' => 'OPPS... seleccione al menos a un freelance para compartir la solicitud de proyecto.'], JSON_FORCE_OBJECT);
		$lista = array_filter(explode (',', $frees));
		foreach($lista as $l){
			$freelance = $this->usuarios->where('id', $l)->first();
			$s = [
				'id_proyecto' => $solicitud['id_proyecto'],
				'id_freelancer' => $l,
				'estatus' => 'espera'
			];
			$this->solicitudes->save($s);

			$correo = new Correos();
			$correo->solicitudProyecto($freelance['correo']);
		}
		return json_encode(['tipo' => 'correcto', 'mensaje' => 'Listo... ya hemos compartido el correo a los demas freelancers.'], JSON_FORCE_OBJECT);
	}
	// Funcion para declinar solicitud de proyecto
	public function declinarSolicitud(){
		$id = sanitizar($this->request->getPost('id'));
		$mensaje = sanitizar($this->request->getPost('explicacion'));
		$idcontratista = sanitizar($this->request->getPost('contratista'));
		//Validaciones
		if($id == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar la solicitud.'], JSON_FORCE_OBJECT);
		if($mensaje == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Ingrese un mensaje para el contratante.'], JSON_FORCE_OBJECT);
		if($idcontratista == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar al contratante.'], JSON_FORCE_OBJECT);
		$contratista = $this->usuarios->where('id', $idcontratista)->first();
		if($contratista == null) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar al contratante.'], JSON_FORCE_OBJECT);

		$this->solicitudes->where('id', $id)->set(['estatus' => 'rechazada'])->update();
		$Correo = new Correos();
		if($Correo->declinarSolicitud($contratista['correo'], $mensaje, session('id'), $id) != true) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Comuníquese con el area de soporte en cuestion de correos.'], JSON_FORCE_OBJECT);
		return json_encode(['tipo' => 'correcto', 'mensaje' => 'La solicitud a sido declinada'], JSON_FORCE_OBJECT);
	}
	// Funcion para paginacion de proyectos
	public function paginacionProyectos(){
		$html = '';
		$paginacion = 10;
		$page = sanitizar($_GET['page']);
		$estatus = sanitizar($_GET['estatus']);
		$offset = ($page - 1) * $paginacion;
		
		if($estatus == 'sin') $proyectos = model('Proyectos')->where('id_contratista', session('id'))->orderBy('id', 'DESC')->findAll($paginacion, $offset);
		else $proyectos = model('Proyectos')->where(['id_contratista' => session('id'), 'estatus', $estatus])->orderBy('id', 'DESC')->findAll($paginacion, $offset);

		foreach($proyectos as $row){
			if($row['estatus'] == 'espera'){
				$html .= view('/backend/Contratista/viewcells/card-trabajo-espera', array('trabajo' => $row));
			}elseif($row['estatus'] == 'desarrollo'){
				$html .= view('/backend/Contratista/viewcells/card-trabajo-desarrollo', array('trabajo' => $row));
			}elseif($row['estatus'] == 'finalizado'){
				$html .= view('/backend/Contratista/viewcells/card-trabajo-finalizado', array('trabajo' => $row));
			}else{
				
			}
		}
		return json_encode(['contenido' => $html, 'total_proyectos' => count($proyectos)], JSON_FORCE_OBJECT);
	}
	// Funcion para ver interior de proyecto
	public function viewProyecto($id){
		if(session('rol') != 'contratante') return redirect()->to('ingresar')->with('alerta', ['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.']);
		
		$id = sanitizar($id);
		$proyecto = $this->proyectos->find($id);
		if($proyecto == null) return redirect()->to('/usuarios/perfil')->with('alerta', ['tipo' => 'error', 'mensaje' => 'El proyecto no existe']);

		$solicitudes_freelancers = model('Proyectos')
			->select(['usuarios.id', 'usuarios.nombre', 'usuarios.apellidos', 'usuarios.imagen'])
			->where(['proyectos.id' => $id])
			->join('solicitudes', 'proyectos.id = solicitudes.id_proyecto')
			->join('usuarios', 'solicitudes.id_freelancer = usuarios.id')
			->find();
		return view('backend/Contratista/interior-proyecto', compact('proyecto', 'solicitudes_freelancers'));
	}
	public function proyectos_postulaciones(){
		$proyectos = model('Proyectos')->where('id_contratista', session('id'))->orderBy('proyectos.id', 'DESC')->join('postulaciones', 'proyectos.id = postulaciones.id_proyecto')->where('estatus', 'espera')->find();
		
		return view('backend/Contratista/proyectos-postulaciones', compact('proyectos'));
	}
	// Funcion para eliminar editar proyecto
	public function eliminarProyecto(){
		if(session('rol') != 'contratante') return redirect()->to('ingresar')->with('alerta', ['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.']);

		if(!$_GET) return redirect()->back()->with('alerta', ['tipo' => 'error', 'mensaje' => 'No sabemos que proyecto eliminar, inténtelo nuevamente']);
		
		$id = sanitizar($_GET['id']);
		$this->proyectos->delete($id);
		$this->solicitudes->where('id_proyecto', $id)->delete();
		return redirect()->to('usuarios/perfil')->with('alerta', ['tipo' => 'success', 'mensaje' => 'El trabajo ha sido eliminado.']);
	}
	// Funcion para editar proyectos
	public function editProyecto($id){
		if(session('rol') != 'contratante') return redirect()->to('ingresar')->with('alerta', ['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.']);

		$usuario = $this->usuarios->get_usuarioID(session('id'));
		$permisos = new AccesosPlanes($usuario['plan']);

		$id = sanitizar($id);
		$proyecto = $this->proyectos->find($id);
		if(empty($proyecto))return redirect()->to('/usuarios/perfil')->with('alerta', ['tipo' => 'error', 'mensaje' => 'El proyecto no existe']);
		
		$habilidades = $this->habilidades->where('superior !=', '0')->orderBy('area', 'asc')->findAll();

		$contratista = model('Autenticacion')->get_id($proyecto['id_contratista']);
		$solicitudes_actuales = model('Proyectos')
			->select(['usuarios.id', 'usuarios.nombre', 'usuarios.apellidos', 'usuarios.imagen'])
			->where(['proyectos.id' => $id])
			->join('solicitudes', 'proyectos.id = solicitudes.id_proyecto')
			->join('usuarios', 'solicitudes.id_freelancer = usuarios.id');
		
		$solicitudes = $solicitudes_actuales->find();
		$solicitudes_actuales = count($solicitudes);
		$solicitudes = ",".implode(",", array_column($solicitudes, 'id'));
		
		$total_solicitudes = !empty($usuario['plan'] || Suscripciones::esta_suscrito_por_idUsuario($usuario['id'])) ? $permisos->getValor_permiso('enviosolicitudes') : 1;
		$solicitudes_restantes = ($total_solicitudes == 9999) ? 9999 : $total_solicitudes - $solicitudes_actuales; // VALIDARÁ SI ES ILIMITADA (9999) O NO 
		
		return view('/backend/Contratista/editar-proyecto', compact('proyecto', 'habilidades', 'contratista', 'solicitudes_restantes', 'solicitudes'));
	}
	//Funcion para postularse a un proyecto
	public function postularse($id){
		if(autorizacion() != 'freelancer') return redirect()->to('/usuarios/perfil')->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'OOPS, no puedes acceder a esta sección cambia de cuenta para poder ingresar.']);
		
		$id = sanitizar($id);
		$post = $this->postulaciones->where(['id_proyecto' => $id, 'id_postulante' => session('id')])->first();
		if(!empty($post)) return redirect()->back()->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'Ya se ha postulado anteriormente.']);
		
		$usuario = $this->usuarios->get_usuarioID(session('id'));
		$permisos = new AccesosPlanes($usuario['plan']);
		
		$postulaciones = model('PostulacionesUsuarios')->where('id_usuario', session('id'))->first();
		if(empty($permisos)) {
			if(!empty($postulaciones)) return redirect()->back()->with('alerta', ['tipo' => 'modal', 'mensaje' => 'Has llegado al límite de postulaciones por mes, espera al finalizar el mes para poder seguirte postulando o hazte PREMIUM']);
			
			model('PostulacionesUsuarios')->save(['id_usuario' => session('id'), 'cantidad_actual' => 1]);
		}else{
			empty($permisos->accesos) ? $postulaciones_limite = 1 : $postulaciones_limite = $permisos->accesos->postuporsemana;
		  //  dd($postulaciones);
			if(empty($postulaciones)){
				model('PostulacionesUsuarios')->save(['id_usuario' => session('id'), 'cantidad_actual' => 0]);
				$postulaciones = model('PostulacionesUsuarios')->where('id_usuario', session('id'))->first();
			}elseif($postulaciones['cantidad_actual'] == $postulaciones_limite){
				return redirect()->back()->with('alerta', ['tipo' => 'modal', 'mensaje' => 'Has llegado al límite de postulaciones por mes, espera al finalizar el mes para poder seguirte postulando o hazte PREMIUM']);
			}
			model('PostulacionesUsuarios')->where('id_usuario', session('id'))->set('cantidad_actual', $postulaciones['cantidad_actual'] + 1)->update();
		}
		
		$this->postulaciones->save(['id_proyecto' => $id, 'id_postulante' => session('id'), 'chat' => false, 'sala' => '']);
		return redirect()->to('/trabajo/'.$id)->with('alerta', ['tipo' => 'correcto', 'mensaje' => 'Grandioso... ya envíe tu interés para este proyecto, me aseguraré de que reciba toda tu información, estoy seguro que pronto recibirás noticias.']);
	}
	// Funcion para crear sala de chat
	public function verchat(){
		$post = sanitizar($this->request->getPost('postu'));
		$postulacion = $this->postulaciones->where('id', $post)->first();
		if(empty($postulacion)) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar al postulante.'], JSON_FORCE_OBJECT);
		
		$where = ['id_contratista' => session('id'), 'id_freelancer' => $postulacion['id_postulante'], 'id_proyecto' => $postulacion['id_proyecto']];
		
		$sala = $this->salas->where($where)->first();
		if(!empty($sala)) return json_encode(['tipo' => 'existente', 'mensaje' => '', 'liga' => base_url('/usuarios/chat/'.$sala['id'])], JSON_FORCE_OBJECT);
		
		$data = [
			'id_contratista' => session('id'),
			'id_freelancer' => $postulacion['id_postulante'],
			'id_proyecto' => $postulacion['id_proyecto']
		];
		$this->salas->save($data);
		$salachat = $this->salas->where($where)->orderBy('id', 'DESC')->first();
		$this->postulaciones->where('id', $post)->set(['chat' => true, 'sala' => $salachat['id']])->update();
		return json_encode(['tipo' => 'nuevo', 'mensaje' => 'Creando sala de chat, espere un momento...', 'liga' => base_url('/usuarios/chat/'.$salachat['id'])]);
	}
	// Funcion para traer informacion de proyecto a asignar con postulante
	public function preAsignarProyecto(){
		$postulacion = $this->postulaciones->where('id', sanitizar($this->request->getPost('post')))->first();
		if($postulacion == null) return 'Ocurrío un problema, contáctese con el área de soporte';
		
		$user = $this->usuarios->get_id($postulacion['id_postulante']);
		if($user == null) return 'Ocurrío un problema, contáctese con el área de soporte';
		
		$proyecto = $this->proyectos->where('id', $postulacion['id_proyecto'])->first();
		if($proyecto == null) return 'Ocurrío un problema, contáctese con el área de soporte';

		$user['es_premium'] = false;
			
		$monto = model('ModelPlanes')->selectMax('monto')->where('cuenta', 'freelancer')->get()->getRowArray()['monto'];
		$con_suscripcion_premium = (model('Suscripciones')
			->builder()
			->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
			->where(['id_usuario' => $postulacion['id_postulante'], 'estatus' => 'Activo', 'monto' => $monto])
			->get()->getRowArray());
				
		if((!empty($con_suscripcion_premium)) && Suscripciones::esta_suscrito_por_idUsuario($postulacion['id_postulante'])) $user['es_premium'] = true;

		return view('/backend/Contratista/viewcells/asignar-proyecto', array('proyecto' => $proyecto, 'freelance' => $user));
	}
	// Funcion para asignar proyecto a freelance entre postulantes
	public function asignarProyecto(){
		$pro = sanitizar($this->request->getPost('proyecto'));
		$free = sanitizar($this->request->getPost('freelance'));
		
		if(empty($pro)) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se ha seleccionado ningún proyecto.'], JSON_FORCE_OBJECT);
		if(empty($free)) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se ha seleccionado ningún freelance para el proyecto.'], JSON_FORCE_OBJECT);

		$proyecto = $this->proyectos->where('id', $pro)->first();
		if(empty($proyecto)) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se ha encontrado el proyecto solicitado.'], JSON_FORCE_OBJECT);
		if($proyecto['estatus'] != 'espera') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'El trabajo seleccionado ya se encuentra asignado a un freelance.'], JSON_FORCE_OBJECT);

		$user = $this->usuarios->get_id($free);
		if(empty($user)) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'El usuario seleccionado no pude ser encontrado.'], JSON_FORCE_OBJECT);

		$data = [
			'id_freelancer' => $free,
			'fecha_inicial' => new Time('now', app_timezone(), 'es_MX'),
			'estatus' => 'desarrollo',
			'tareas' => NULL
		];
		$this->proyectos->where('id', $pro)->set($data)->update();
		$postulacion = $this->postulaciones->where(['id_proyecto' => $pro, 'id_postulante' => $free])->first();
		$contratista = $this->usuarios->get_id($proyecto['id_contratista']);
		if($postulacion['chat'] == 0){
			$newchat = [
				'id_contratista' => $proyecto['id_contratista'],
				'id_freelancer' => $free,
				'id_proyecto' => $proyecto['id'],
			];
			$this->salas->save($newchat);
			$last = $this->salas->orderBy('id', 'DESC')->limit(1)->first();
			$this->postulaciones->where('id', $postulacion['id'])->set(['chat' => '1', 'sala' => $last['id']])->update();
		}
		$Correo = new Correos();
		if($Correo->proyecto_aceptado($user['correo'], $contratista, $proyecto)) return json_encode(['tipo' => 'correcto', 'mensaje' => 'Freelancer asignado, el proyecto a pasado a desarrollo.'], JSON_FORCE_OBJECT);
		else return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Se ha guardado exitosamente la información pero hubo un error al notificar al freelancer.'], JSON_FORCE_OBJECT);
	}
	// Funcion para buscar freelance para enviar solicitud
	public function buscadorFreelance(){
		$usuario = $this->usuarios->get_usuarioID(session('id'));
		$permisos = new AccesosPlanes($usuario['plan']);

		$search = sanitizar($this->request->getPost('search'));
		$freelancers = sanitizar($this->request->getPost('frees'));
		$freelancers_presolicitados = sanitizar($this->request->getPost('freelancers_presolicitados'));
		$lista = '\''. str_replace(',', "', '", $freelancers.$freelancers_presolicitados). '\'';
		
		$freelancer = db_connect()->query(
			"SELECT * FROM (SELECT *, CONCAT(nombre,' ',apellidos) 
			AS nombre_completo FROM usuarios) concatenacion 
			WHERE (correo LIKE '%$search%' OR concatenacion.nombre_completo LIKE '%$search%') AND (rol = 'freelancer') AND (deleted_at is null) AND id NOT IN ($lista) LIMIT 5")
		->getResultArray();
		return view('/backend/Contratista/viewcells/opciones-buscador-asignar', compact('freelancer', 'lista'));
	}
	// Funcion para mostrar toast de freelance seleccionado
	public function freeSeleccionado(){
		$id = $this->request->getPost('user');
		$freelance = $this->usuarios->get_id($id);
		echo view('/backend/Contratista/viewcells/free-select', compact('freelance'));
	}
	// Funcion para aceptar solicitud de proyecto
	public function aceptedSolicitud(){
		$id = sanitizar($this->request->getPost('id'));
		if($id == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar la solicitud de trabajo.'], JSON_FORCE_OBJECT);
		$soli = $this->solicitudes->where('id', $id)->first();
		if($soli == null) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar la solicitud de trabajo.'], JSON_FORCE_OBJECT);
		$Correo = new Correos();
		
		$proyecto = $this->proyectos->where('id', $soli['id_proyecto'])->first();
		$user = $this->usuarios->where('id', $proyecto['id_contratista'])->first();
		if($proyecto['id_freelancer'] != 0 || $proyecto['estatus'] != 'espera') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Lo sentimos, el proyecto ya ha sido asignado a otro freelancer.'], JSON_FORCE_OBJECT);
		$setproyecto = [
			'id_freelancer' => session('id'),
			'fecha_inicial' => new Time('now', app_timezone(), 'es_MX'),
			'estatus' => 'desarrollo'
		];
		$this->proyectos->where('id', $soli['id_proyecto'])->set($setproyecto)->update();
		$solicitudes = $this->solicitudes->where('id_proyecto', $soli['id_proyecto'])->find();
		$postulados = $this->postulaciones->where('id_proyecto', $soli['id_proyecto'])->find();
		foreach($solicitudes as $s){
			$freelance = $this->usuarios->get_id($s['id_freelancer']);
			if($s['id'] == $id){
				$this->solicitudes->where('id', $s['id'])->set(['estatus' => 'aceptada'])->update();
			}else{
				$Correo->proyectoNoAsignado($freelance['correo'], $proyecto);
				$this->solicitudes->where('id', $s['id'])->set(['estatus' => 'rechazada'])->update();
			}
		}
		if($postulados != null){
			foreach($postulados as $p){
				$freelance = $this->usuarios->get_id($p['id_postulante']);
				$Correo->proyectoNoAsignado($freelance['correo'], $proyecto);
				$this->postulaciones->eliminar($p['id']);
			}
		}
		$newchat = [
			'id_contratista' => $proyecto['id_contratista'],
			'id_freelancer' => session('id'),
			'id_proyecto' => $proyecto['id'],
		];
		$this->salas->save($newchat);
		$last = $this->salas->orderBy('id', 'DESC')->limit(1)->first();
		$registro = [
			'id_proyecto' => $proyecto['id'],
			'id_postulante' => session('id'),
			'chat' => true,
			'sala' => $last['id'],
		];
		$this->postulaciones->save($registro);
		$Correo->solicitudAceptada($user['correo'], $freelance, $proyecto);
		return json_encode(['tipo' => 'correcto', 'mensaje' => 'Felicidades, el proyecto se te ha asignado.', 'proyecto' => $proyecto['id']], JSON_FORCE_OBJECT);
	}
	// Funcion para crear nueva tarea para el proyecto
	public function crearTarea(){
		$id = sanitizar($this->request->getPost('id'));
		$tarea = sanitizar($this->request->getPost('tarea'));
		if($id == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar el proyecto deseado.'], JSON_FORCE_OBJECT);
		if($tarea == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Ingrese una tarea por favor.'], JSON_FORCE_OBJECT);
		$proyecto = $this->proyectos->where('id', $id)->first();
		if($proyecto == null) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar el proyecto deseado.'], JSON_FORCE_OBJECT);
		empty($proyecto['tareas']) ? $tareas = array() : $tareas = json_decode($proyecto['tareas'], true);

		$newtarea = [ 'tarea' => $tarea, 'estatus' => false];
		array_push($tareas, $newtarea);
		$this->proyectos->where('id', $id)->set(['tareas' => json_encode($tareas, JSON_FORCE_OBJECT)])->update();
		return json_encode(['tipo' => 'success', 'contenido' => view('/viewcells/tareas', array('tareas' => $tareas, 'proyecto' => $proyecto))], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar tareas ya acabadas
	public function progresoProyecto(){
		$id = sanitizar($this->request->getPost('id'));
		$tarea = sanitizar($this->request->getPost('tarea'));
		if($id == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar el proyecto.'], JSON_FORCE_OBJECT);
		if($tarea == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro enontrar la tarea.'], JSON_FORCE_OBJECT);
		$proyecto = $this->proyectos->where('id', $id)->first();
		if($proyecto == null) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar el proyecto.'], JSON_FORCE_OBJECT);
		if($proyecto['tareas'] == null) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se han creado tareas aun para el proyecto.'], JSON_FORCE_OBJECT);
		$tareas = json_decode($proyecto['tareas'], true);
		if($tareas[$tarea]['estatus'] == false){
			$tareas[$tarea]['estatus'] = true;
		}else{
			$tareas[$tarea]['estatus'] = false;
		}
		$this->proyectos->where('id', $id)->set(['tareas' => json_encode($tareas, JSON_FORCE_OBJECT)])->update();
		$total = count($tareas);
		$x = 0;
		foreach($tareas as $t){
			if($t['estatus'] == true) $x++;
		}
		$part = 100 / $total;
		$porcentaje = $part * $x;
		return json_encode(['tipó' => 'correcto', 'mensaje' => ''.round($porcentaje, 2).'%'], JSON_FORCE_OBJECT);
	}
	// Funcion para cargar barra de progreso al cargar la pagina
	public function progresoProyectoLoading(){
		$id = sanitizar($this->request->getPost('id'));
		if($id == '') return 0;
		$proyecto = $this->proyectos->where('id', $id)->first();
		if($proyecto == null) return 0;
		if($proyecto['tareas'] == null) return 0;
		$tareas = json_decode($proyecto['tareas'], true);
		$total = count($tareas);
		if($total == null) return 0;
		$x = 0;
		foreach($tareas as $t){
			if($t['estatus'] == true) $x++;
		}
		$part = 100 / $total;
		$porcentaje = $part * $x;
		return round($porcentaje, 2).'%';
	}
	// Funcion para borrar tarea de un proyecto
	public function eliminarTarea(){
		$id = sanitizar($this->request->getPost('id'));
		if($id == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar el proyecto requerido.'], JSON_FORCE_OBJECT);
		$proyecto = $this->proyectos->where('id', $id)->first();
		if($proyecto == null) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logron encontrar el proyecto requerido.'], JSON_FORCE_OBJECT);
		if($proyecto['tareas'] == null) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'OPPS... No tengo tareas que eliminar.'], JSON_FORCE_OBJECT);
		$tarea = sanitizar($this->request->getPost('tarea'));
		if($tarea == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'HEEY... Faltó seleccionar qué tarea voy a eliminar.'], JSON_FORCE_OBJECT);
		$tareas = json_decode($proyecto['tareas'], true);
		unset($tareas[$tarea]);
		$this->proyectos->where('id', $id)->set(['tareas' => json_encode($tareas, JSON_FORCE_OBJECT)])->update();
		// $trabajo = $this->proyectos->where('id', $id)->first();
		// $tar = json_decode($trabajo['tareas'], true);
		$total = count($tareas);
		$x = 0;
		foreach($tareas as $t){
			if($t['estatus'] == true) $x++;
		}
		$part = 100 / $total;
		$porcentaje = $part * $x;
		return json_encode(['tipo' => 'correcto', 'mensaje' => 'Listo... Eliminé la tarea..', 'contenido' => view('/viewcells/tareas', array('tareas' => $tareas, 'proyecto' => $proyecto)), 'porcentaje' => ''.round($porcentaje, 2).'%'], JSON_FORCE_OBJECT);
	}
	// Funcion para finalizar proyecto y guardar calificación
	public function finalizarProyecto(){
		//Variables
		$id = sanitizar($this->request->getPost('proyecto'));
		$freelance = sanitizar($this->request->getPost('freelance'));
		$conforme = sanitizar($this->request->getPost('conforme'));
		$comentario = sanitizar($this->request->getPost('comentario'));
		$calificacion = sanitizar($this->request->getPost('estrellas'));
		//Validaciones de dato
		if($id == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar el proyecto buscado.'], JSON_FORCE_OBJECT);
		if($freelance == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar al freelance.'], JSON_FORCE_OBJECT);
		if($conforme == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'Vamos a finalizar el proyecto, pero antes puedes indicar que te pareció la atención del Freelance.'], JSON_FORCE_OBJECT);
		if($comentario == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'Puedes contarnos qué te pareció el trabajo del Freelance con una pequeña reseña.'], JSON_FORCE_OBJECT);
		if($calificacion == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'HEEY... Ya estamos finalizando el proyecto pero como calificas al Freelance.'], JSON_FORCE_OBJECT);
		$cali = [
			'id_proyecto' => $id,
			'id_freelancer' => $freelance,
			'conforme' => $conforme,
			'comentario' => $comentario,
			'tipo' => 'calificacion',
			'calificacion' => $calificacion
		];
		$this->calificaciones->save($cali);
		$this->proyectos->where('id', $id)->set(['estatus' => 'finalizado'])->update();
		$data = model('Finalizar')->where('id_proyecto', $id)->set(['estatus_freelance' => 'true', 'estatus_contrante' => 'true'])->update();
		$free = $this->usuarios->get_id($freelance);
		$proyecto = $this->proyectos->where('id', $id)->first();
		$Correo = new Correos();
		if($Correo->proyectoFinalizado($free['correo'], $proyecto)) return json_encode(['tipo' => 'correcto', 'mensaje' => 'Grandioso... Hemos finalizado el proyecto, ya notifiqué al Freelance, ahora te llevo al tu panel para continuar con el próximo proyecto, Gracias.'], JSON_FORCE_OBJECT);
		return json_encode(['tipo' => 'advertencia', 'mensaje' => 'OPPS... Algo no me permite mandar la notificación, puedes ponerte en contacto con nuestro equipo de Soporte, son unos expertos y nos solucionan todo, yo sé lo que te digo.'], JSON_FORCE_OBJECT);
	}
	// Funcion de reportar freelance y republicar proyecto
	public function reportarFreelanceRepublicar(){
		$id = sanitizar($this->request->getPost('proyecto'));
		$free = sanitizar($this->request->getPost('freelance'));
		$comentario = sanitizar($this->request->getPost('comentario'));
		if($id == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar el proyecto seleccionado.'], JSON_FORCE_OBJECT);
		if($free == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar al freelance seleccionado.'], JSON_FORCE_OBJECT);
		if($comentario == '') return json_encode(['tipo' => 'custom', 'mensaje' => 'HEEY... Requiero un comentario para reportar al Freelance, te recomiendo ser lo más claro posible, yo notificare el problema para que nuestro equipo le de un seguimiento correcto.'], JSON_FORCE_OBJECT);
		$cali = [
			'id_proyecto' => $id,
			'id_freelancer' => $free,
			'conforme' => 'No',
			'comentario' => $comentario,
			'tipo' => 'reporte',
			'calificacion' => '0'
		];
		$this->calificaciones->save($cali);
		$postulacion = $this->postulaciones->where(['id_proyecto' => $id, 'id_postulante' => $free])->first();
		$this->salas->eliminar($postulacion['sala']);
		$this->postulaciones->eliminar($postulacion['id']);
		$this->proyectos->where('id', $id)->set(['id_freelancer' => '0', 'fecha_inicial' => '0000-00-00 00:00:00', 'estatus' => 'espera', 'tareas' => null])->update();
		$Correo = new Correos();
		$freelance = $this->usuarios->get_id($free);
		$contratista = $this->usuarios->get_id(session('id'));
		$project = $this->proyectos->where('id', $id)->first();
		if($Correo->ReportarFreelance('aflores@tresesenta.mx', $freelance, $contratista, $project, $comentario)) return json_encode(['tipo' => 'correcto', 'mensaje' => 'El freelance se ha sido reportado ante nuestra administración y proyecto ha sido publicado nuevamente, lamentamos lo sucedido.'], JSON_FORCE_OBJECT);
		return json_encode(['tipo' => 'advertencia', 'mensaje' => 'OPPS... No pude realizar el reporte, vamos a reportarlo directo con nuestro sorprendente equipo de soporte, ellos lo solucionan todo.'], JSON_FORCE_OBJECT);
	}
	// Funcion de reportar freelance y cancelar proyecto
	public function reportarFreelanceCancelar(){
		$id = sanitizar($this->request->getPost('proyecto'));
		$free = sanitizar($this->request->getPost('freelance'));
		$comentario = sanitizar($this->request->getPost('comentario'));
		if($id == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar el proyecto seleccionado.'], JSON_FORCE_OBJECT);
		if($free == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar al freelance seleccionado.'], JSON_FORCE_OBJECT);
		if($comentario == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'HEEY... Requiero un comentario para reportar al Freelance, te recomiendo ser lo más claro posible, yo notificare el problema para que nuestro equipo le de un seguimiento correcto.'], JSON_FORCE_OBJECT);
		$cali = [
			'id_proyecto' => $id,
			'id_freelancer' => $free,
			'conforme' => 'No',
			'comentario' => $comentario,
			'tipo' => 'reporte',
			'calificacion' => '0'
		];
		$this->calificaciones->save($cali);
		$postulacion = $this->postulaciones->where(['id_proyecto' => $id, 'id_postulante' => $free])->first();
		$this->salas->eliminar($postulacion['sala']);
		$this->postulaciones->eliminar($postulacion['id']);
		$this->proyectos->where('id', $id)->set(['fecha_inicial' => '0000-00-00 00:00:00', 'estatus' => 'cancelado', 'tareas' => null])->update();
		$Correo = new Correos();
		$freelance = $this->usuarios->get_id($free);
		$contratista = $this->usuarios->get_id(session('id'));
		$project = $this->proyectos->where('id', $id)->first();
		if($Correo->ReportarFreelance('aflores@tresesenta.mx', $freelance, $contratista, $project, $comentario)) return json_encode(['tipo' => 'correcto', 'mensaje' => 'El freelance se ha sido reportado ante nuestra administración y proyecto ha sido cancelado, lamentamos lo sucedido.'], JSON_FORCE_OBJECT);
		return json_encode(['tipo' => 'advertencia', 'mensaje' => 'OPPS... No pude realizar el reporte, vamos a reportarlo directo con nuestro sorprendente equipo de soporte, ellos lo solucionan todo.'], JSON_FORCE_OBJECT);
	}
	// Funcion para ver seccion de todos los proyectos por parte del administrador
	public function totalProyectos(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		$total = $this->proyectos->findAll();
		return view('/backend/administrador/total-proyectos', array('proyectos' => $total));
	}
	// Funcion para ver proyecto por parte del administrador
	public function verProyectoAdmin(){
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.']);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.']);
		if(!$_GET) return redirect()->back()->with('alerta', ['tipo' => 'error', 'mensaje' => 'No se ha seleccionado ningún proyecto.']);
		if(empty($_GET['id'])) return redirect()->back()->with('alerta', ['tipo' => 'error', 'mensaje' => 'No se encontro el atributo del proyecto seleccionado.']);
		$proyecto = $this->proyectos->where('id', sanitizar($_GET['id']))->first();
		if($proyecto == null) return redirect()->back()->with('alerta', ['tipo' => 'error', 'mensaje' => 'No se logro encontrar el proyecto.']);
		$contratista = $this->usuarios->get_id($proyecto['id_contratista']);
		($proyecto['id_freelancer'] != 0)? $freelancer = $this->usuarios->get_id($proyecto['id_freelancer']) : $freelancer = array();
		return view('/backend/administrador/vista-proyecto', array('proyecto' => $proyecto, 'contratista' => $contratista, 'freelancer' => $freelancer));
	}
	// Funcion para filtrar proyectos en el area de administracion
	public function filtrarProyectosAdmin(){
		$tipo = sanitizar($this->request->getPost('tipo'));
		if($tipo == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar el tipo de estatus seleccionado.'], JSON_FORCE_OBJECT);
		if($tipo == 'todos'){
			$proyectos = $this->proyectos->findAll();
			return json_encode(['tipo' => 'success', 'mensaje' => view('/backend/administrador/viewcells/proyectos-filtrados', array('proyectos' => $proyectos))], JSON_FORCE_OBJECT);
		}else{
			$proyectos = $this->proyectos->where('estatus', $tipo)->find();
			return json_encode(['tipo' => 'success', 'mensaje' => view('/backend/administrador/viewcells/proyectos-filtrados', array('proyectos' => $proyectos))], JSON_FORCE_OBJECT);
		}
	}
	// Funcion para ver lista de proyectos por contratista
	public function listaProyectosContratista(){
		if(!session('is_logged')) return redirect()->to('')->with('alerta', ['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.']);
		if(session('rol') != 'admin') return redirect()->to('')->with('alerta', ['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.']);
		if(!$_GET) return redirect()->back()->with('alerta', ['tipo' => 'error', 'mensaje' => 'No se ha seleccionado ningún contratista.']);
		if(empty($_GET['id']))return redirect()->back()->with('alerta', ['tipo' => 'error', 'mensaje' => 'No se encontro al contratista seleccionado.']);
		$contratista = $this->usuarios->get_id(sanitizar(base64_decode($_GET['id'])));
		$proyectos = $this->proyectos->where('id_contratista', sanitizar(base64_decode($_GET['id'])))->find();
		if($contratista == null) return redirect()->back()->with('alerta', ['tipo' => 'error', 'mensaje' => 'No se encontro al contratista seleccionado.']);
		return view('/backend/administrador/ver-proyectos-usuario', array('contratista' => $contratista, 'proyectos' => $proyectos));
	}
	// Funcion para aprobar a freelance
	public function aprobarFreelance(){
		$id = sanitizar($this->request->getPost('id'));
		if($id == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logró encontrar al usuario seleccionado.'], JSON_FORCE_OBJECT);
		$this->usuarios->where('id', $id)->set(['aprobado' => true])->update();
		$Correo = new Correos();
		$user = $this->usuarios->get_id($id);
		$Correo->perfilAprobado($user);
		return json_encode(['tipo' => 'success', 'mensaje' => 'Aprobado'], JSON_FORCE_OBJECT);
	}
	// Funcion para mostrar pagina de resultados de proyectos
	public function resultFilterProyectos(){
		$search = '';
		$area = '';
		if(!$_GET) return redirect()->to('');
		if(!empty($_GET['search'])) $search = sanitizar($_GET['search']);
		if(!empty($_GET['area'])) $area = sanitizar($_GET['area']);
		
		if($search == '' && $area == '' || $search == '' && $area == 'default'){
			$data = $this->proyectos->where(['estatus' => 'espera', 'deleted_at' => null])->orderBy('destacado', 'DESC')->orderBy('id', 'DESC')->find();
		}else{
			$builder = db_connect()->table('proyectos');
			$builder->where('deleted_at', null);
			$builder->like('titulo', $search);
			$builder->orLike('desc_corta', $search);
			$builder->orLike('descripcion', $search);
			$builder->orderBy('destacado', 'DESC');
			$builder->orderBy('id', 'DESC');
			$data = $builder->get()->getResultArray();
			foreach($data as $key => $d){
				if($d['deleted_at'] != null) unset($data[$key]);
			}
			
			foreach($data as $key => $d){
				if($d['estatus'] != 'espera') unset($data[$key]);
			}
			if($area != ''){
				foreach($data as $key => $d){
					if($d['categoria'] != $area) unset($data[$key]);
				}
			}
		}
		
		$FuncionesSistema = new FuncionesSistema();
		$pricemax = $FuncionesSistema->presupuestoTipo('desc');
		$pricemin = 0;
		$habilidades = $this->habilidades->where(['superior' => '0'])->orderBy('area', 'asc')->find();
		$subareas = $this->habilidades->where('superior !=', '0')->orderBy('area', 'asc')->find();
		
		return view('/front/resultado-proyectos', array('estados' => model('ModelEstados')->findAll(), 'minimo' => round($pricemin), 'maximo' => ceil($pricemax), 'proyectos' => $data, 'busqueda' => $search, 'areas' => $habilidades, 'subareas' => $subareas, 'search' => $search, 'area' => $area));
	}
	// Funcioin para filtrar proyectos 
	public function filtrarResultadosProyectos(){
		$search = sanitizar($this->request->getPost('search'));
		$area = sanitizar($this->request->getPost('area'));
		$subcategoria = sanitizar($this->request->getPost('habilidades'));
		$minimo = sanitizar($this->request->getPost('minimo'));
		$maximo = sanitizar($this->request->getPost('maximo'));
		$nivel = $this->request->getPost('nivel');
		$tiempo = $this->request->getPost('tiempo');
		$estado = trim($this->request->getPost('estado'));

		$where = "";
		$db      = db_connect();
		$builder = $db->table('proyectos');
		$builder->where(['estatus' => 'espera', 'deleted_at' => null]);
		if($area != '') $builder->where('categoria', $area); //$where .= " AND categoria = '".$area."' ";
		if($subcategoria != '') $builder->where('subcategoria', $subcategoria); //$where .= " AND subcategoria = '".$subcategoria."' ";
		$niv = '';
		if($nivel != null){
			$x = count($nivel);
			foreach($nivel as $key => $n){
				if($key == 0){
					$niv .= " AND (nivel = '".$n."' ";
				}else{
					$niv .= "OR nivel = '".$n."'";
				}
			}
			$niv .= ')';
		}
		$where .= $niv;
		
		if(!empty($where)) $builder->where($where);
		if($search != ''){
			$builder->like('titulo', $search);
			$builder->orLike('desc_corta', $search);
			$builder->orLike('descripcion', $search);
			$builder->orLike('categoria', $search);
			$builder->orLike('subcategoria', $search);
		}
		$proyectos = $builder->get()->getResultArray();
		foreach($proyectos as $key => $proyecto){
			if($proyecto['deleted_at'] != null) unset($proyectos[$key]);
		}
		
		foreach($proyectos as $key => $proyecto){
			if($proyecto['estatus'] != 'espera') unset($proyectos[$key]);
		}
		$projects = array();
		if(empty($this->request->getPost('filtro_palabra'))){
			
			if($tiempo != null){
				$times = array();
				foreach($tiempo as $time){
					switch($time){
						case 1:
							$dat = [
								'minimo' => 1,
								'maximo' => 10,
							];
							array_push($times, $dat);
							break;
						case 2:
							$dat = [
								'minimo' => 11,
								'maximo' => 30,
							];
							array_push($times, $dat);
							break;
						case 3:
							$dat = [
								'minimo' => 31,
								'maximo' => 100000,
							];
							array_push($times, $dat);
							break;
					}
				}
				foreach($proyectos as $key => $p){
					$fecha1= new \DateTime($p['fecha_arranque_esperado']);
					$fecha2= new \DateTime($p['fecha_entrega']);
					$diff = $fecha1->diff($fecha2);
					$sentencia = '';
					$tot = count($times);
					$x = 0;
					foreach($times as $tim){
						if(intval($diff->days) >= intval($tim['minimo']) && intval($diff->days) <= intval($tim['maximo'])) $x = 1;
					}
					array_push($proyectos[$key], ($diff->days));
					if($x == 0) unset($proyectos[$key]);
				}
			}
			foreach($proyectos as $p){
				$price = str_replace(",", "", $p['presupuesto']);
				if($price <= $maximo && $price >= $minimo) array_push($projects, $p);
			}
			if($projects == null) return json_encode(['html' => vacio(), 'cantidad_proyectos' => 0], JSON_FORCE_OBJECT);
			$aux = array();
			foreach ($projects as $key => $row) {
				$aux[$key] = $row['destacado'];
			}
			array_multisort($aux, SORT_DESC, $projects);
			$ids = '';
			foreach($projects as $key => $k){
				if($k['deleted_at'] != null) unset($projects[$key]);
			}
			foreach($projects as $key => $project){
				$ids .= $project['id'];
				($key < (count($projects) - 1))? $ids .= ',' : $ids .= '' ;
			}
			foreach($projects as $key => $project){
				if($project['estatus'] != 'espera' || $project['deleted_at'] != null) unset($projects[$key]);
			}
			if($estado != ''){
				foreach($projects as $key => $project){
					$user = $this->usuarios->where('id', $project['id_contratista'])->first();
					if($user == null){
						unset($projects[$key]);
					}else{
						if($user['direccion'] == null){
							unset($projects[$key]);
						}else{
							$dire = json_decode($user['direccion'], true);
							if(isset($dire[0]['estado'])){
								$pos = strpos($estado, $dire[0]['estado']);
								if($pos === false){
									unset($projects[$key]);
								}else{
									$project['estado'] = $estado;
								}
							}else{
								unset($projects[$key]);
							}
						}
					}
				}
			}
		}
		else{ 
			$projects = $proyectos;
			$ids = '';
			foreach($projects as $key => $k){
				if($k['deleted_at'] != null) unset($projects[$key]);
			}
			foreach($projects as $key => $project){
				$ids .= $project['id'];
				($key < (count($projects) - 1))? $ids .= ',' : $ids .= '' ;
			}
		}
// 		return json_encode(['html' => d($projects), 'cantidad_proyectos' => count($projects)], JSON_FORCE_OBJECT);
		foreach($projects as $key => $k){
			if($k['deleted_at'] != null) unset($projects[$key]);
		}
		if(empty($projects)) return json_encode(['html' => vacio(), 'cantidad_proyectos' => 0], JSON_FORCE_OBJECT);
		$html = '';
		foreach($projects as $pro){
			$html.= view_cell('App\Libraries\FuncionesSistema::cardProyectoDisponible', ['proyecto' => $pro]);
		}
		$html.= '<input id="ids" type="hidden" value="'.$ids.'">';
		$html.= '<input id="total" type="hidden" value="'.count($projects).'">';
		
		return json_encode(['html' => $html, 'cantidad_proyectos' => count($projects)], JSON_FORCE_OBJECT);
	}
	//Funcion para ordenar proyectos
	public function reordenarProyectos(){
		$ids = explode(",", $this->request->getPost('ids'));
		$tipo = $this->request->getPost('tipo');
		if($tipo == 'news'){
			$projects = model('Proyectos')->orderBy('id', 'DESC')->find($ids);
		}elseif($tipo == 'old'){
			$projects = model('Proyectos')->orderBy('id', 'ASC')->find($ids);
		}elseif($tipo == 'max'){
			$projects = model('Proyectos')->orderBy('presupuesto', 'DESC')->find($ids);
		}elseif($tipo == 'min'){
			$projects = model('Proyectos')->orderBy('presupuesto', 'ASC')->find($ids);
		}
		if(empty($projects)){
			echo '<input id="total" type="hidden" value="'.count($projects).'">';
			return vacio();
		}
		
		foreach($projects as $pro){
			echo view_cell('App\Libraries\FuncionesSistema::cardProyectoDisponible', ['proyecto' => $pro]);
		}
		echo '<input id="ids" type="hidden" value="'.$this->request->getPost('ids').'">';
		echo '<input id="total" type="hidden" value="'.count($projects).'">';
	}
	public function get_trabajos_finalizadosFiltro($estatus){
		$usuario = model('Autenticacion')->get_UsuarioID(session('id'));
		$permisos = new AccesosPlanes($usuario['plan']);
		
		$vista = '';
		if($estatus == 'finalizado'){
			if($permisos->getValor_permiso('historialtrabajos') == 0){
				$cant_trabajos_finalizado = model('Proyectos')->where('id_contratista', session('id'))
				->whereIn('estatus', ['finalizado', 'cancelado'])
				->countAllResults();
				$vista .= "<div class='alert alert-warning alert-dismissible mb-4' role='alert'>Cuenta con <b class='fw-bold'>".$cant_trabajos_finalizado."</b> trabajos por mostrar, adquiere una suscripción <b class='fw-bold'>PREMIUM</b> para obtener todo tu historial de trabajos ilimitado</b><button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>";
				// return json_encode(['cantidad' => 0, 'trabajos' => $vista.="<div class='alert alert-info alert-dismissible mb-4' role='alert'>Sin resultados</div>"], JSON_FORCE_OBJECT);;
			}
			$trabajos = model('Proyectos')->where(['id_contratista' => session('id'), 'estatus' => $estatus])->orderBy('id', 'DESC')->limit(intval($permisos->getValor_permiso('historialtrabajos')))->find();
			$cantidad_total_trabajos = count(model('Proyectos')->where(['id_contratista' => session('id'), 'estatus' => 'finalizado'])->orderBy('id', 'DESC')->find());
			$restante = $cantidad_total_trabajos - count($trabajos);
			if(count($trabajos) < $cantidad_total_trabajos) $vista .= "<div class='alert alert-warning alert-dismissible mb-4' role='alert'>Cuenta con <b class='fw-bold'>".$restante."</b> trabajos por mostrar, adquiere una suscripción <b class='fw-bold'>PREMIUM</b> para obtener todo tu historial de trabajos ilimitado</b><button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>";
		}else{
			if($estatus == 'con_postulaciones'){
				$trabajos = model('Proyectos')->where('id_contratista', session('id'))->orderBy('proyectos.id', 'DESC')->join('postulaciones', 'proyectos.id = postulaciones.id_proyecto')->where('estatus', 'espera')->find();
				$estatus = 'espera';
			}
			else $trabajos = model('Proyectos')->where(['id_contratista' => session('id'), 'estatus' => $estatus])->orderBy('id', 'DESC')->find();
		}

		if($estatus == 'todos'){
			return json_encode(['cantidad' => model('Proyectos')->where('id_contratista', session('id'))->countAllResults(), 'trabajos' => view_cell('App\Libraries\FuncionesSistema::misTrabajos'), JSON_FORCE_OBJECT]);    
		}
		if(empty($trabajos)) return json_encode(['cantidad' => count($trabajos), 'trabajos' => "<div class='alert alert-info alert-dismissible mb-4' role='alert'>Sin trabajos disponibles</div>"], JSON_FORCE_OBJECT);
		
		foreach($trabajos as $trabajo) $vista .=  view(contratista('viewcells/card-trabajo-'.$estatus), ['trabajo' => $trabajo]);
		
		return json_encode(['cantidad' => count($trabajos), 'trabajos' => $vista], JSON_FORCE_OBJECT);
	}
	public function mostrarProyectosEstatus(){
		$BD = db_connect();
		$estatus = sanitizar($this->request->getPost('tipo'));
		if($estatus == 'espera'){
			$builder = $BD->table('postulaciones');
			$builder->where(['postulaciones.id_postulante' => session('id'), 'proyectos.estatus' => 'espera'])->join('proyectos', 'postulaciones.id_proyecto = proyectos.id');
			$proyectos = $builder->get()->getResultArray();
			$aux = array();
			foreach ($proyectos as $key => $row) {
				$aux[$key] = $row['destacado'];
			}
			array_multisort($aux, SORT_DESC, $proyectos);
			foreach($proyectos as $p){
				echo view_cell('App\Libraries\FuncionesSistema::cardProyectoDisponible', ['proyecto' => $p]);
			}
		}elseif($estatus == 'desarrollo'){
			$proyectos = model('Proyectos')->where(['id_freelancer' => session('id'), 'estatus' => 'desarrollo'])->find();
			$aux = array();
			foreach ($proyectos as $key => $row) {
				$aux[$key] = $row['destacado'];
			}
			array_multisort($aux, SORT_DESC, $proyectos);
			foreach($proyectos as $p){
				echo view_cell('App\Libraries\FuncionesSistema::cardProyectoDisponible', ['proyecto' => $p]);
			}
		}elseif($estatus == 'finalizado'){
			$model = model('Proyectos');
			$proyectos = $model->where(['id_freelancer' => session('id'), 'estatus' => 'finalizado'])->findAll();
			$aux = array();
			foreach ($proyectos as $key => $row) {
				$aux[$key] = $row['destacado'];
			}
			array_multisort($aux, SORT_DESC, $proyectos);
			foreach($proyectos as $p){
				echo view_cell('App\Libraries\FuncionesSistema::cardProyectoDisponible', ['proyecto' => $p]);
			}
		}else{
		}
	}
	public function destacarProyecto(){
		if(autorizacion() != 'contratante' || !session('logeado')) return json_encode(['alerta' => 'falla', 'mensaje' =>'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		
		$usuario = $this->usuarios->get_usuarioID(session('id'));
		$permisos = new AccesosPlanes($usuario['plan']);
		
		// Validación de proyectos activos
		$check = $this->request->getPost('check') == 'true' ? true : false;
		$proyectos = $this->proyectos->where(['id_contratista' => session('id'), 'estatus' => 'espera'])->find();
		$proyectos_destacadosPermitidos = $permisos->getValor_permiso('proyectosDestacados');
		
		if($proyectos_destacadosPermitidos == 0) return json_encode(['alerta' => 'advertencia', 'mensaje' => 'Adquiere una suscripción PREMIUM para tener proyectos destacados ilimitados']);
		if(count($proyectos) >= $permisos->getValor_permiso('proyectosActivos')) return json_encode(['alerta' => 'advertencia', 'mensaje' => 'Solo puedes tener '.$permisos->getValor_permiso('proyectosActivos').' proyectos activos, adquiere una suscripción PREMIUM para tener tus proyectos activos ilimitados.']);
		
		// Validación de proyectos Destacados permitidos
		if($check){
			$proyectos_destacados = count($this->proyectos->where(['id_contratista' => session('id'), 'estatus' => 'espera', 'destacado' => true])->find());
			
			if($proyectos_destacados == $proyectos_destacadosPermitidos) return json_encode(['alerta' => 'advertencia', 'mensaje' => 'Ya cuenta con '.$proyectos_destacados.' proyectos destacados. Puede esperar a que se pasen a desarrollo para poder destacar nuevos proyectos o cambiar el estatus de alguno de ellos.'], JSON_FORCE_OBJECT);
		}
		
		$id_proyecto = sanitizar($this->request->getPost('id'));
		$actualizado = $this->proyectos->save(['id' => $id_proyecto, 'destacado' => $check == true ? 1 : 0]);
		
		if($actualizado) return json_encode(['alerta' => 'correcto', 'mensaje' =>'Actualizado'], JSON_FORCE_OBJECT);
		return json_encode(['alerta' => 'falla', 'mensaje' =>'Error al actualizar Base de Datos'], JSON_FORCE_OBJECT);
	}
	public function ordenar_solicitudes(){
		$ordenar = sanitizar($this->request->getPost('filtro'));
		$por = 'id';
		switch($ordenar){
			case 'recientes':
				$ordenar = 'ASC';
			break;
			case 'antiguos':
				$ordenar = 'DESC';
			break;
			case 'mayor_presupuesto':
				$por = 'presupuesto';
				$ordenar = 'ASC';
			break;
			case 'menor_presupuesto':
				$por = 'presupuesto';
				$ordenar = 'DESC';
			break;
		}
		$select = ['solicitudes.id', 'solicitudes.id_proyecto', 'proyectos.estatus', 'proyectos.id_contratista','proyectos.titulo','proyectos.desc_corta', 'proyectos.descripcion', 'proyectos.categoria', 'proyectos.subcategoria', 'proyectos.fecha_arranque_esperado', 'proyectos.fecha_entrega', 'proyectos.presupuesto', 'proyectos.imagenes'];
		$builder = $this->solicitudes->select($select)->orderBy($por, $ordenar)->where(['solicitudes.id_freelancer' => session('id'), 'solicitudes.estatus' => 'espera'])->join('proyectos', 'solicitudes.id_proyecto = proyectos.id');
		$proyectos = $builder->get()->getResultArray();
		foreach($proyectos as $proyecto){
			echo view_cell('App\Libraries\FuncionesSistema::cardSolicitud', ['proyecto' => $proyecto]);
		}
	}
	public function eliminarSolicitudFreelancer(){
		$id = sanitizar($this->request->getPost('solicitud'));
		if($this->solicitudes->delete($id, true)) return redirect()->to('usuarios/solicitudes')->with('alerta', ['tipo' => 'correcto', 'mensaje' => 'Se borró la solicitud correctamente, ya verás que tendrás mejores oportunidades.']);
		return redirect()->back()->with('alerta', ['tipo' => 'falla', 'mensaje' => 'UPPS... no se pudo eliminar, inténtalo nuevamente o comunicate con nosotros en soporte.']); 
	}
	public function soliFinalizarProyecto(){
		if(!session('logeado')) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'OPPS, al parecer tu sesión ha finalizado, ingresa nuevamente a la plataforma para poder solicitar la finalización del proyecto.'], JSON_FORCE_OBJECT);
		
		$id_proyecto = sanitizar($this->request->getPost('id'));
		if(empty($id_proyecto)) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Lo sentimos no se logro encontrar el ID del proyecto.'], JSON_FORCE_OBJECT);
		
		$freelancer = model('Autenticacion')->where('id', session('id'))->first();
		if($freelancer == null) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'OPPS, al parecer no logramos encontrar tus datos, intentelo nuevamente.'], JSON_FORCE_OBJECT);
		
		$proyecto = model('Proyectos')->where('id', $id_proyecto)->first();
		if($proyecto == null) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Lo sentimos, no logramos encontrar el proyecto solicitado.'], JSON_FORCE_OBJECT);
		
		$contratante = model('Autenticacion')->where('id', $proyecto['id_contratista'])->first();
		if($contratante == null) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'OPPS, al parecer no pudimos encontrar al contratante del proyecto, intentelo nuevamente.'], JSON_FORCE_OBJECT);
		
		$data = model('Finalizar')->where('id_proyecto', $id_proyecto)->first();
		if($data != null){
			if($data['estatus_contrante']) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'OPPS, el contratante ya ha finalizado el proyecto.'], JSON_FORCE_OBJECT);
			if($data['estatus_freelance']) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'OPPS, Ya has solicitado la finalización del proyecto, espera ha que el contratista valida cada uno de los puntos y finalice finalmente el proyecto.'], JSON_FORCE_OBJECT);
			model('Finalizar')->where('id', $data['id'])->set(['estatus_freelance' => 'true'])->update();
		}else{
			$d = [
				'id_proyecto' => $id_proyecto,
				'estatus_freelance' => 'true',
				'estatus_contrante' => null
			];
			model('Finalizar')->save($d);
		}
		$Correo = new Correos();
		if($Correo->soliFinalizarProyecto($proyecto, $freelancer, $contratante)) return json_encode(['tipo' => 'correcto', 'mensaje' => 'Se le ha notificado al contratante que ha solicitado finalizar el proyecto.'], JSON_FORCE_OBJECT);
		return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Lo sentimos, el servicio de envio de correos se encuentra en mantenimiento.'], JSON_FORCE_OBJECT);
	}
	public function declinarSoliFin(){
		$id_proyecto = sanitizar($this->request->getPost('id'));
		if(empty($id_proyecto)) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Lo sentimos no se logro encontrar el ID del proyecto.'], JSON_FORCE_OBJECT);
		
		$data = model('Finalizar')->where('id_proyecto', $id_proyecto)->first();
		if($data == null) return json_encode(['tipo' => 'success', 'mensaje' => 'Solicitud actualizada.'], JSON_FORCE_OBJECT);
		
		$proyecto = model('Proyectos')->where('id', $id_proyecto)->first();
		if($proyecto == null) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'OPPS, no logramos encontrar el proyecto seleccionado, intentelo nuevamente.'], JSON_FORCE_OBJECT);
		
		$freelancer = model('Autenticacion')->where('id', $proyecto['id_freelancer'])->first();
		if($freelancer == NULL) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'OPPS, lo sentimos, no logramos encontrar al freelancer del proyecto, intentelo nuevamente.'], JSON_FORCE_OBJECT);
		
		model('Finalizar')->where('id', $data['id'])->set(['estatus_freelance' => 'false', 'estatus_contrante' => 'false'])->update();
		$Correo = new Correos();
		if($Correo->declinarSolicitudFin($proyecto, $freelancer)) return json_encode(['tipo' => 'success', 'mensaje' => 'Solicitud actualizada.'], JSON_FORCE_OBJECT);
		return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Lo sentimos, el servicio de envio de correos se encuentra en mantenimiento.'], JSON_FORCE_OBJECT);
	}
	public function actuaSoliFin(){
		$id_proyecto = sanitizar($this->request->getPost('id'));
		if(empty($id_proyecto)) return false;
		
		$data = model('Finalizar')->where('id_proyecto', $id_proyecto)->first();
		if($data == null) return false;
		
		model('Finalizar')->where('id', $data['id'])->set(['estatus_freelance' => null, 'estatus_contrante' => null])->update();
		return true;
	}
	public function VerificarEstadoSoliFin(){
		if(!session('logeado')) return json_encode(['tipo' => 'error'], JSON_FORCE_OBJECT);

		$id_proyecto = sanitizar($this->request->getPost('id'));
		if(empty($id_proyecto)) return json_encode(['tipo' => 'error'], JSON_FORCE_OBJECT);

		$data = model('Finalizar')->where('id_proyecto', $id_proyecto)->first();
		if($data == null) return json_encode(['tipo' => 'error'], JSON_FORCE_OBJECT);
		
		if(session('rol') == 'contratista'){
			if($data['estatus_contrante'] == null && $data['estatus_freelance'] == 'true'){
				$html = '<div class="alert btn-sm alert-info text-center p-2 mb-0" role="alert">
					<label>El freelancer ha notificado que ha finalizado el proyecto.</label>
					<div class="tooltips">
						<a class="btn px-1" onclick="finProyecto()" data-id="'.$id_proyecto.'"><i class="fas fa-check text-success"></i> Confirmar</a>
						<span class="tooltiptext d-none d-md-block">Finalizar proyecto</span>
					</div>
					<div class="tooltips">
						<a class="btn px-1" onclick="declinarSoliFin('.$id_proyecto.')"><i class="fas fa-times text-danger"></i> Rechazar</a>
						<span class="tooltiptext d-none d-md-block">Declinar proyecto</span>
					</div>
				</div>';
				$a = [
					'tipo' => 'success',
					'con' => 'null',
					'free' => 'true',
					'html' => $html,
				];
			}elseif($data['estatus_contrante'] == 'true'){
				$html = '<div class="alert alert-info btn-sm text-center" role="alert">
					El proyecto ya ha finalizado.
				</div>';
				$a = [
					'tipo' => 'success',
					'con' => 'true',
					'free' => 'true',
					'html' => $html
				];
			}elseif($data['estatus_contrante'] == null && $data['estatus_freelance'] == null){
				$html = '';
				$a = [
					'tipo' => 'success',
					'con' => 'null',
					'free' => 'null',
					'html' => $html
				];
			}else{
				$html = '';
				$a = [
					'tipo' => 'success',
					'con' => 'false',
					'free' => 'false',
					'html' => $html
				];
			}
		}elseif(session('rol') == 'freelancer'){
			if($data['estatus_contrante'] == 'true'){
				$html = '<div class="alert alert-info btn-sm text-center" role="alert">
					El proyecto ya ha finalizado.
				</div>';
				$a = [
					'tipo' => 'success',
					'con' => 'true',
					'free' => 'true',
					'html' => $html
				];
			}elseif($data['estatus_contrante'] == null && $data['estatus_freelance'] == 'true'){
				$html = '<div class="alert alert-info btn-sm text-center " role="alert">
					<i class="fas fa-info-circle"></i> Se le ha notificado al contratante que ha solicitado finalizar el proyecto.
				</div>';
				$a = [
					'tipo' => 'success',
					'con' => 'true',
					'free' => 'true',
					'html' => $html
				];
			}elseif($data['estatus_contrante'] == 'false' && $data['estatus_freelance'] == 'false'){
				$html = '<div class="alert alert-danger btn-sm text-center" role="alert">
					<i class="fas fa-tasks"></i>  El contratante no ha aceptado por completo el proyecto, verifíque cada una de las tareas.
				</div>
				<a class="btn btn-primary" onclick="soliFinalizarProyecto('.$id_proyecto.')" data-id="'.$id_proyecto.'">
					Solicitar finalizar proyecto
				</a>
				<script>
					$(document).ready(function(){
						setTimeout(function(){
							resetSoli('.$id_proyecto.');
						}, 5000);
					});
				</script>';
				$a = [
					'tipo' => 'success',
					'con' => 'false',
					'free' => 'false',
					'html' => $html
				];
			}else{
				$html = '<a class="btn btn-primary" onclick="soliFinalizarProyecto('.$id_proyecto.')" data-id="'.$id_proyecto.'">
					Solicitar finalizar proyecto
				</a>';
				$a = [
					'tipo' => 'success',
					'con' => 'null',
					'free' => 'null',
					'html' => $html
				];
			}
		}
		return json_encode($a, JSON_FORCE_OBJECT);
	}
}