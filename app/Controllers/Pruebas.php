<?php namespace App\Controllers;

use CodeIgniter\I18n\Time;
use App\Controllers\BaseController;

require 'vendor/autoload.php';
use SMTPValidateEmail\Validator as SmtpEmailValidator;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
// use Kreait\Firebase\Contract\Firestore;
// use Google\Cloud\Firestore\FirestoreClient;

// require ROOTPATH.'vendor/google/cloud-firestore/FirestoreClient.php';

class Pruebas extends BaseController{
    protected $database;
    
    public function __construct(){
        helper('text');
        $firebase = (new Factory)->withServiceAccount(ROOTPATH.'firebase.json')->create();
        // $firebase = (new Factory)->withServiceAccount($acc)->create();
        $this->database = $firebase->getDatabase();
    }
    public function validarEmail(){
        $email     = 'admin@mextemps.com';
        $sender    = 'correos@tresesenta.lat';
        $validator = new SmtpEmailValidator($email, $sender);
        
        $datoscorreo = explode("@", $email);
        // If debug mode is turned on, logged data is printed as it happens:
        // $validator->debug = true;
        $results   = $validator->validate();

        $totalmxs = count($results['domains'][$datoscorreo[1]]['mxs']);
        ($totalmxs > 1)? $return = true : $return = false;
        echo $return;
        // Get log data (log data is always collected)
        $log = $validator->getLog();
        d($log);
    }
    public function validarFechas(){
        $data = model('Suscripciones')->where('id_usuario', session('id'))->first();
        // if($data == null) return false;
        // if($data['renovacion'] == true) return false;
        $hoy = new Time('now', 'America/Monterrey', 'es_MX');
        $fin = new Time($data['fecha_fin'], 'America/Monterrey', 'es_MX');
        // if($hoy > $fin) return false;
        $date1 = new \DateTime($hoy);
        $date2 = new \DateTime($fin);
        $diff = $date1->diff($date2);
        dd($diff->days);
        if($diff->days < 8) return 1;
    }
    public function mensajesFirebase(){
        $ref = $this->database->getReference('chats/1');
        dd($ref);
        dd(array_keys($ref));
        
        
        
        
        
        
        
        // $db = new FirestoreClient();
        // // $db = new FirestoreClient([
        // //     'projectId' => 'mextemps-341000',
        // // ]);
        // $docRef = $db->collection('chats')->document('1');
        // $snapshot = $docRef->snapshot();
        // if ($snapshot->exists()) {
        //     printf('Document data:' . PHP_EOL);
        //     print_r($snapshot->data());
        // } else {
        //     printf('Document %s does not exist!' . PHP_EOL, $snapshot->id());
        // }
        // $reference = $database->getReference('path/to/child/location')
        // $dbname = 'chats';
        // $userID = '1';
        // dd($this->database->getReference($dbname)->getRoot()->equalTo($userID)->startAt($userID)->getSnapshot());
        // dd($this->database->getReference($dbname));
    }
}