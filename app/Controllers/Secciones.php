<?php namespace App\Controllers;

class Secciones extends BaseController{
	protected $seccion, $habilidades;
	
	public function __construct(){
		$this->seccion = model('ModelSecciones');
		$this->habilidades = model('Habilidades');
	}
	// Funcion index que redirigira al inicio
	public function index(){
		if(!session('is_logged')) return redirect()->to('');
		if(session('rol') != 'admin') return redirect()->to('');
		return view(administrador('paginas'));
	}
	// Funcion para ver vista donde aparecen todas las paginas
	public function edit(){
		if(!session('is_logged')) return redirect()->to('');
		if(session('rol') != 'admin') return redirect()->to('');
		if(!$_GET) return redirect()->to('paginas');
		if(empty($_GET['id'])) return redirect()->to('paginas');
		if(empty($_GET['page'])) return redirect()->to('paginas');
		$seccion = $_GET['page'];
		if(view_cell('App\Libraries\FuncionesAdmin::existeONoArchivo', ['dir' => APPPATH.'/Views/backend/administrador/paginas', 'archivo_buscar' => $seccion.'.php']) == false) return redirect()->back();
		return view('/backend/administrador/paginas/'.$seccion);
	}
	// Funcion para actualizar o crear slide
	public function saveSlide(){
		$id = trim($this->request->getPost('id'));
		$titulo = trim($this->request->getPost('titulo'));
		if($titulo == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un título para el slider'], JSON_FORCE_OBJECT);
		
		$subtitulo = trim($this->request->getPost('subtitulo'));
		if($subtitulo == '') return json_encode(['tipo' => 'error', 'mensaje' => 'ingrese un subtítulo para el slider'], JSON_FORCE_OBJECT);
		
		$slide = trim($this->request->getPost('slide'));
		if($slide == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese una imágen para el slide'], JSON_FORCE_OBJECT);
		
		$buscador = trim($this->request->getPost('buscador'));
		if($buscador == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Seleccione el tipo de buscador'], JSON_FORCE_OBJECT);
		
		$tablet = trim($this->request->getPost('tablet'));
		$movil = trim($this->request->getPost('movil'));
		// if($id == '') return json_encode(['tipo' => 'error', 'mensaje' => 'sin id'], JSON_FORCE_OBJECT);
		// return json_encode(['tipo' => 'error', 'mensaje' => 'Si hay id'], JSON_FORCE_OBJECT);
		if($id == ''){
			$info = [
				'titulo' => $titulo,
				'subtitulo' => $subtitulo,
				'buscador' => $buscador,
				'slide' => str_replace(' ', '+', $slide),
				'tableta' => str_replace(' ', '+', $tablet),
				'movil' => str_replace(' ', '+', $movil)
			];
			$data = [
				'seccion' => 'inicio',
				'tipo' => 'slide',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$info = [
				'titulo' => $titulo,
				'subtitulo' => $subtitulo,
				'buscador' => $buscador,
				'slide' => str_replace(' ', '+', $slide),
				'tableta' => str_replace(' ', '+', $tablet),
				'movil' => str_replace(' ', '+', $movil)
			];
			$this->seccion->where('id', $id)->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Slide actualizado'], JSON_FORCE_OBJECT);
	}
	// Funcion para mostrar datos para actualizar slide
	public function mostrarSlide(){
		$data = $this->seccion->where('id', $this->request->getPost('id'))->first();
		return $data['info'];
	}
	// Funcion para actualizar seccion de freelances latinoamerica
	public function saveLatinoamerica(){
		$colnegro = trim($this->request->getPost('colnegro'));
		$colnaranja = trim($this->request->getPost('colnaranja'));
		$superior = trim($this->request->getPost('superior'));
		$contenido = trim($this->request->getPost('contenido'));
		if($colnegro == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un texto para la seccion en color negro para el titulo'], JSON_FORCE_OBJECT);
		if($colnaranja == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un texto para la seccion en color naranja del titulo'], JSON_FORCE_OBJECT);
		if($superior == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un texto para la parte superior del titulo'], JSON_FORCE_OBJECT);
		if($contenido == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un texto para la sección'], JSON_FORCE_OBJECT);
		$info = [
			'colnegro' => $colnegro,
			'colnaranja' => $colnaranja,
			'superior' => $superior,
			'contenido' => $contenido
		];
		$seccion = $this->seccion->where(['seccion' => 'inicio', 'tipo' => 'latinoamerica'])->first();
		if($seccion == null){
			$data = [
				'seccion' => 'inicio',
				'tipo' => 'latinoamerica',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada'], JSON_FORCE_OBJECT);
	}
	// Funcion para actualizar seccion de porque usar mextemps
	public function saveMextemps(){
		$colnegro = trim($this->request->getPost('colnegro'));
		$colnaranja = trim($this->request->getPost('colnaranja'));
		$contenido = trim($this->request->getPost('contenido'));
		if($colnegro == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un texto para la seccion en color negro para el titulo'], JSON_FORCE_OBJECT);
		if($colnaranja == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un texto para la seccion en color naranja del titulo'], JSON_FORCE_OBJECT);
		if($contenido == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un texto para la sección'], JSON_FORCE_OBJECT);
		$info = [
			'colnegro' => $colnegro,
			'colnaranja' => $colnaranja,
			'contenido' => $contenido
		];
		$seccion = $this->seccion->where(['seccion' => 'inicio', 'tipo' => 'mextemps'])->first();
		if($seccion == null){
			$data = [
				'seccion' => 'inicio',
				'tipo' => 'mextemps',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada'], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar iconos
	public function saveIconoMextemps(){
		$id = trim($this->request->getPost('id'));
		$icono = $this->seccion->where(['seccion' => 'inicio', 'tipo' => 'mextemps-icono-'.$id])->first();
		if($icono != null) return $icono['info'];
		if($id == 1) return json_encode(['titulo' => 'Acuerdos directos<br>Freelancer-Empresa', 'icono' => base_url('/assets/images/iconos/imagen-1.png')], JSON_FORCE_OBJECT);
		if($id == 2) return json_encode(['titulo' => 'Conectamos sin intermediarios', 'icono' => base_url('/assets/images/iconos/imagen-2.png')], JSON_FORCE_OBJECT);
		if($id == 3) return json_encode(['titulo' => 'Profesionales expertos en su area', 'icono' => base_url('/assets/images/iconos/imagen-3.png')], JSON_FORCE_OBJECT);
		if($id == 4) return json_encode(['titulo' => 'Efectivos en búsqueda<br>de proveedores', 'icono' => base_url('/assets/images/iconos/imagen-4.png')], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar seccion empezemos
	public function saveEmpezemos(){
		$titulo = trim($this->request->getPost('titulo'));
		$texto = trim($this->request->getPost('texto'));
		$fondo = trim($this->request->getPost('fondo'));
		$fondotablet = trim($this->request->getPost('fondo-tablet'));
		$fondomovil = trim($this->request->getPost('fondo-movil'));
		if($titulo == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un titulo para la sección.'], JSON_FORCE_OBJECT);
		if($texto == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un pequeño texto para la sección.'], JSON_FORCE_OBJECT);
		if($fondo == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Cargue una imagen de fondo para la sección.'], JSON_FORCE_OBJECT);
		if($fondotablet == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Cargue una imagen de fondo para la sección en formato tablet.'], JSON_FORCE_OBJECT);
		if($fondomovil == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Cargue una imagen de fondo para la sección para dispositivos moviles.'], JSON_FORCE_OBJECT);
		$info = [
			'titulo' => $titulo,
			'texto' => $texto,
			'fondo' => str_replace(' ', '+', $fondo),
			'fondo-tableta' => str_replace(' ', '+', $fondotablet),
			'fondo-movil' => str_replace(' ', '+', $fondomovil)
		];
		$seccion = $this->seccion->where(['seccion' => 'inicio', 'tipo' => 'empezemos'])->first();
		if($seccion == null){
			$data = [
				'seccion' => 'inicio',
				'tipo' => 'empezemos',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada.'], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar datos de seccion de trabajos en inicio
	public function saveTrabajosInicio(){
		$colnegro = trim($this->request->getPost('colnegro'));
		$colnaranja = trim($this->request->getPost('colnaranja'));
		$subtitulo = trim($this->request->getPost('subtitulo'));
		$inferior = trim($this->request->getPost('inferior'));
		if($colnegro == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un titulo para la sección.'], JSON_FORCE_OBJECT);
		if($subtitulo == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un subtitulo para la sección.'], JSON_FORCE_OBJECT);
		if($inferior == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un texto para la parte inferior de la sección a un lado del botón.'], JSON_FORCE_OBJECT);
		$info = [
			'colnegro' => $colnegro,
			'colnaranja' => $colnaranja,
			'subtitulo' => $subtitulo,
			'inferior' => $inferior
		];
		$seccion = $this->seccion->where(['seccion' => 'inicio', 'tipo' => 'trabajos'])->first();
		if($seccion == null){
			$data = [
				'seccion' => 'inicio',
				'tipo' => 'trabajos',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada.'], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar datos de seccion de freelancers en inicio
	public function saveGridFreelancers(){
		$colnegro = trim($this->request->getPost('colnegro'));
		$colnaranja = trim($this->request->getPost('colnaranja'));
		$subtitulo = trim($this->request->getPost('subtitulo'));
		if($colnegro == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un texto para la sección en color negro del titulo.'], JSON_FORCE_OBJECT);
		if($colnaranja == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un texto para la seccion en color naranja del titulo.'], JSON_FORCE_OBJECT);
		if($subtitulo == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un subtitulo para la sección.'], JSON_FORCE_OBJECT);
		$info = [
			'colnegro' => $colnegro,
			'colnaranja' => $colnaranja,
			'subtitulo' => $subtitulo,
		];
		$seccion = $this->seccion->where(['seccion' => 'inicio', 'tipo' => 'freelancers'])->first();
		if($seccion == null){
			$data = [
				'seccion' => 'inicio',
				'tipo' => 'freelancers',
				'info' => json_encode($info, JSON_FORCE_OBJECT),
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada.'], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar datoos de categorias
	public function saveSeccionCategorias(){
		$titulo = trim($this->request->getPost('titulo'));
		$boton = trim($this->request->getPost('boton'));
		if($titulo == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el titulo de la sección.'], JSON_FORCE_OBJECT);
		if($boton == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el texto para el boton de la sección'], JSON_FORCE_OBJECT);
		$info = [
			'titulo' => $titulo,
			'boton' => $boton
		];
		$seccion = $this->seccion->where(['seccion' => 'inicio', 'tipo' => 'categorias'])->first();
		if($seccion == null){
			$data = [
				'seccion' => 'inicio',
				'tipo' => 'categorias',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)]);
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada.']);
	}
	// Funcion para guardar icono
	public function guardarIconoMextemps(){
		$id = trim($this->request->getPost('id'));
		$titulo = trim($this->request->getPost('titulo'));
		$texto = trim($this->request->getPost('texto'));
		$icono = trim($this->request->getPost('icono'));
		if($id == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se ha seleccionado ningun icono.'], JSON_FORCE_OBJECT);
		if($titulo == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un titulo para el icono.'], JSON_FORCE_OBJECT);
		if($texto == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un texto para el icono.'], JSON_FORCE_OBJECT);
		if($icono == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Cargue una imagen para el icono.'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'inicio', 'tipo' => 'mextemps-icono-'.$id])->first();
		$info = [
			'titulo' => $titulo,
			'texto' => $texto,
			'icono' => str_replace(' ', '+', $icono)
		];
		if($seccion == null){
			$data = [
				'seccion' => 'inicio',
				'tipo' => 'mextemps-icono-'.$id,
				'info' => json_encode($info, JSON_FORCE_OBJECT),
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Icono actualizado'], JSON_FORCE_OBJECT);
	}
	// Funcion para ver datos de categoria en inicio
	public function verCategoriaInicio(){
		$id = $this->request->getPost('id');
		$categoria = $this->seccion->where(['seccion' => 'inicio', 'tipo' => 'categoria-inicio-'.$id])->first();
		$habilidades = $this->habilidades->where('superior', '0')->orderBy('area', 'asc')->findAll();
		return view('backend/administrador/viewcells/select-categoria', array('id' => $id, 'habilidades' => $habilidades, 'categoria' => $categoria));
	}
	// Funcion para guardar categoria de seccion de inicio
	public function saveCardCategoria(){
		$categoria = trim($this->request->getPost('categoria'));
		$idcategoria = trim($this->request->getPost('idcategoria'));
		$icocategoria = trim($this->request->getPost('icocategoria'));
		if($categoria == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Seleccione una categoria.'], JSON_FORCE_OBJECT);
		if($idcategoria == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro encontrar la categoria'], JSON_FORCE_OBJECT);
		if($icocategoria == '') return json_encode(['tipo' => 'error', 'Ingrese un icono para la sección.'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'inicio', 'tipo' => 'categoria-inicio-'.$idcategoria])->first();
		$info = [
			'categoria' => $categoria,
			'icono' => str_replace(' ', '+', $icocategoria)
		];
		if($seccion == null){
			$data = [
				'seccion' => 'inicio',
				'tipo' => 'categoria-inicio-'.$idcategoria,
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Categoria actualizada'], JSON_FORCE_OBJECT);
	}

	public function guardar_Quienes_somos(){
		$quienes_somos['titulo'] = trim($this->request->getPost('titulo'));
		if(empty($quienes_somos['titulo'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingresa el título principal'], JSON_FORCE_OBJECT);

		$quienes_somos['frase'] = trim($this->request->getPost('frase'));
		if(empty($quienes_somos['frase'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingresa la frase del título'], JSON_FORCE_OBJECT);

		$quienes_somos['parrafo'] = trim($this->request->getPost('parrafo'));
		if(empty($quienes_somos['parrafo'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingresa el texto principal'], JSON_FORCE_OBJECT);

		$quienes_somos['parrafo2'] = trim($this->request->getPost('parrafo2'));
		if(empty($quienes_somos['parrafo2'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingresa el texto secundario'], JSON_FORCE_OBJECT);

		$quienes_somos['subtitulo'] = trim($this->request->getPost('subtitulo'));
		if(empty($quienes_somos['subtitulo'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingresa el subtítulo'], JSON_FORCE_OBJECT);

		$quienes_somos['imagen'] = trim($this->request->getPost('imagen'));
		if(empty($quienes_somos['imagen'])) return json_encode(['tipo' => 'error', 'mensaje' => 'Ingresa la imagen descriptiva'], JSON_FORCE_OBJECT);

		$seccion = $this->seccion->where(['seccion' => 'inicio', 'tipo' => 'quienes-somos'])->first();
		
		$datos = ['seccion' => 'inicio', 'tipo' => 'quienes-somos'];
		$datos['id'] = isset($seccion['id']) ? $seccion['id'] : null;
		$datos['info'] = json_encode($quienes_somos, JSON_FORCE_OBJECT);

		if($this->seccion->save($datos)); return json_encode(['tipo' => 'success', 'mensaje' => 'Sección guardada correctamente'], JSON_FORCE_OBJECT);

		return json_encode(['tipo' => 'error', 'mensaje' => 'No se pudo guardar la información, inténtalo nuevamente o más tarde'], JSON_FORCE_OBJECT);
	}
	// Funcion para actualizar pagina de paquetes
	public function savePaquetes(){
		$titulo = trim($this->request->getPost('titulo'));
		$subtitulo = trim($this->request->getPost('subtitulo'));
		if($titulo == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un titulo para la sección'], JSON_FORCE_OBJECT);
		if($subtitulo == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un subtitulo para la sección.'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'paquetes', 'tipo' => 'info'])->first();
		$info = [
			'titulo' => $titulo,
			'subtitulo' => $subtitulo
		];
		if($seccion == null){
			$data = [
				'seccion' => 'paquetes',
				'tipo' => 'info',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Seccion actualizada'], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar preguntas frecuentes de paquetes
	public function savePreguntaPaquetes(){
		$id = trim($this->request->getPost('id'));
		$pregunta = trim($this->request->getPost('pregunta'));
		$respuesta = trim($this->request->getPost('respuesta'));
		if($pregunta == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese una pregunta.'], JSON_FORCE_OBJECT);
		if($respuesta == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la respuesta para su pregunta.'], JSON_FORCE_OBJECT);
		$info = [
			'pregunta' => $pregunta,
			'respuesta' => $respuesta
		];
		if($id == ''){
			$data = [
				'seccion' => 'paquetes',
				'tipo' => 'pregunta',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $id)->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Pregunta actualizada'], JSON_FORCE_OBJECT);
	}
	// Funcion para mostrar pregunta en especifico
	public function mostrarPreguntasPaquetes(){
		$id = $this->request->getPost('id');
		$pregunta = $this->seccion->where('id', $id)->first();
		if($pregunta == null) return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro encontrar la pregunta.'], JSON_FORCE_OBJECT);
		if($pregunta['tipo'] != 'pregunta') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro encontrar la pregunta.'], JSON_FORCE_OBJECT);
		$info = json_decode($pregunta['info'], true);
		$data = [
			'tipo' => 'success',
			'id' => $id,
			'pregunta' => $info['pregunta'],
			'respuesta' => $info['respuesta']
		];
		return json_encode($data, JSON_FORCE_OBJECT);
	}
	// Funcion para retornar preguntas frecuentes
	public function pregFrecuentesPaquetes(){
		return view_cell('App\Libraries\FuncionesAdmin::viewPreguntaPaquetes');
	}
	// Funcion de vista para ajustes generales
	public function ajustesGenerales(){
		if(!session('is_logged')) return 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.';
		if(session('rol') != 'admin') return 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.';
		
		return view('/backend/administrador/ajustes-generales');
	}
	// Funcion para guardar colores para iconos animados
	public function saveColores(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesion a caducado.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('principal')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un color principal para los iconos animados del sitio.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('secundario')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un color secundario para los iconos animados del sitio.'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'general', 'tipo' => 'colores'])->first();
		$info = [
			'principal' => trim($this->request->getPost('principal')),
			'secundario' => trim($this->request->getPost('secundario'))
		];
		if($seccion == null){
			$data = [
				'seccion' => 'general',
				'tipo' => 'colores',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Cambios guardados'], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar seccion de categorias mas buscadas inicio
	public function saveMasBuscadosInicio(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesion a caducado.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('titulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el titulo para la sección.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('subtitulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el subtitulo para la sección.'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'inicio', 'tipo' => 'mas-buscados'])->first();
		$info = [
			'titulo' => trim($this->request->getPost('titulo')),
			'subtitulo' => trim($this->request->getPost('subtitulo'))
		];
		if($seccion == null){
			$data = [
				'seccion' => 'inicio',
				'tipo' => 'mas-buscados',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada']);
	}
	// Funcion para ver categoria de mas buscados para editar
	public function verCatMasBuscada(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesion a caducado.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('cat')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro encontrar el id de la categoria.'], JSON_FORCE_OBJECT);
		$cat = trim($this->request->getPost('cat'));
		$seccion = $this->seccion->where(['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-'.$cat])->first();
		return json_encode(['tipo' => 'success', 'mensaje' => view('/viewcells/editar-mas-buscados', array('id' => $cat, 'categoria' => $seccion, 'categorias' => model('Habilidades')->where(['superior' => '0'])->find()))], JSON_FORCE_OBJECT);
	}
	// Funcion para actualizar categoria de seccion de mas buscados
	public function updateCategoriMasBuscados(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesion a caducado.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('id')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro especificar el id de la categoria a editar.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('categoria')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Seleccione una categoria.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('titulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un titulo para la busqueda.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('imagen')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Cargue una imagen para la categoria.'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-'.trim($this->request->getPost('id'))])->first();
		$info = [
			'categoria' => trim($this->request->getPost('categoria')),
			'titulo' => trim($this->request->getPost('titulo')),
			'imagen' => trim($this->request->getPost('imagen'))
		];
		if($seccion == null){
			$data = [
				'seccion' => 'inicio',
				'tipo' => 'categoria-mas-buscados-'.trim($this->request->getPost('id')),
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Categoria actualizada.'], JSON_FORCE_OBJECT);
	}
	/////////////////////////////////////////////////////////////////////////// Como funciona freelance
	// Funcion para guardar seccion de comentarios
	public function saveSeccComentariosComoFreelance(){
		if(trim($this->request->getPost('titulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la parte del titulo en color negro.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('destacado')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la parte del titulo en color distinto.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('inferior')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el texto inferior de la sección'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'como-funciona-freelance', 'tipo' => 'comentarios'])->first();
		$info = [
			'titulo' => trim($this->request->getPost('titulo')),
			'destacado' => trim($this->request->getPost('destacado')),
			'inferior' => trim($this->request->getPost('inferior'))
		];
		if($seccion == null){
			$data = [
				'seccion' => 'como-funciona-freelance',
				'tipo' => 'comentarios',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada.'], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar seccion de miles de oportunidades
	public function saveMilesOportunidadesFreelance(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesion a caducado.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('superior')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un texto para la parte superior de la sección.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('titulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un titulo para la sección.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('descripcion')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese una descripción para la sección.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('imagen')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Cargue una imagen para la sección'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'como-funciona-freelance', 'tipo' => 'oportunidades'])->first();
		$info = [
			'superior' => trim($this->request->getPost('superior')),
			'titulo' => trim($this->request->getPost('titulo')),
			'descripcion' => trim($this->request->getPost('descripcion')),
			'imagen' => str_replace(' ', '+', trim($this->request->getPost('imagen')))
		];
		if($seccion == null){
			$data = [
				'seccion' => 'como-funciona-freelance',
				'tipo' => 'oportunidades',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada.'], JSON_FORCE_OBJECT);
	}
	// Funcion para mostrar datos de paso
	public function verPasoFreelance(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesion a caducado.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('id')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro encontrar el id del paso a editar'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'como-funciona-freelance', 'tipo' => 'tres-pasos-'.trim($this->request->getPost('id'))])->first();
		if($seccion == null){
			$data = null;
			return json_encode(['tipo' => 'success', 'mensaje' => view('/backend/administrador/viewcells/editor-paso-freelance', array('datos' => $data, 'id' => trim($this->request->getPost('id'))))], JSON_FORCE_OBJECT);
		}else{
			$data = json_decode($seccion['info'], true);
			return json_encode(['tipo' => 'success', 'mensaje' => view('/backend/administrador/viewcells/editor-paso-freelance', array('datos' => $data, 'id' => trim($this->request->getPost('id'))))], JSON_FORCE_OBJECT);
			// return json_encode(['tipo' => 'success', 'titulo' => $data['titulo'], 'contenido' => $data['contenido'], 'imagen' => $data['imagen']], JSON_FORCE_OBJECT);
		}
	}
	// Funcion para guardar datos generales de 3 pasos de freelance
	public function saveTresPasosFreelance(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesion a caducado.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('titulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la parte de texto oscuro del titulo.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('destacado')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la parte en destacado del titulo.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('contenido')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el texto inicial de la sección.'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'como-funciona-freelance', 'tipo' => 'tres-pasos'])->first();
		$info = [
			'titulo' => trim($this->request->getPost('titulo')),
			'destacado' => trim($this->request->getPost('destacado')),
			'contenido' => trim($this->request->getPost('contenido'))
		];
		if($seccion == null){
			$data = [
				'seccion' => 'como-funciona-freelance',
				'tipo' => 'tres-pasos',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada'], JSON_FORCE_OBJECT);
	}
	// Funcion para actualizar datos de paso
	public function updatePasoFreelance(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesion a caducado.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('id')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro encontrar el id del icono.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('titulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un titulo para la sección.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('contenido')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un texto informativo para el paso.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('imagen')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un icono para e paso.'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'como-funciona-freelance', 'tipo' => 'tres-pasos-'.trim($this->request->getPost('id'))])->first();
		$info = [
			'titulo' => trim($this->request->getPost('titulo')),
			'contenido' => trim($this->request->getPost('contenido')),
			'imagen' => str_replace(' ', '+', trim($this->request->getPost('imagen')))
		];
		if($seccion == null){
			$data = [
				'seccion' => 'como-funciona-freelance',
				'tipo' => 'tres-pasos-'.trim($this->request->getPost('id')),
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada'], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar seccion de crear cuenta
	public function saveCrearCuentaFreelance(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesion a caducado.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('superior')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el texto superior al titulo.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('titulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el titulo para la sección.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('subtitulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un subtitulo para la sección.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('imagen')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la imagen para la sección.'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'como-funciona-freelance', 'tipo' => 'crea-una-cuenta'])->first();
		$info = [
			'superior' => trim($this->request->getPost('superior')),
			'titulo' => trim($this->request->getPost('titulo')),
			'subtitulo' => trim($this->request->getPost('subtitulo')),
			'imagen' => str_replace(' ', '+', trim($this->request->getPost('imagen')))
		];
		if($seccion == null){
			$data = [
				'seccion' => 'como-funciona-freelance',
				'tipo' => 'crea-una-cuenta',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada'], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar seccion principal de como funciona freelance
	public function saveHeaderComoFuncionaFreelance(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesion a caducado.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('titulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la parte incial del titulo.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('destacado')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la parte destacada del titulo.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('subtitulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el subtitulo para la sección.'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'como-funciona-freelance', 'tipo' => 'area-slides'])->first();
		$info = [
			'titulo' => trim($this->request->getPost('titulo')),
			'destacado' => trim($this->request->getPost('destacado')),
			'subtitulo' => trim($this->request->getPost('subtitulo'))
		];
		if($seccion == null){
			$data = [
				'seccion' => 'como-funciona-freelance',
				'tipo' => 'area-slides',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada.'], JSON_FORCE_OBJECT);
	}
	/////////////////////////////////////////////////////////////////////////// Como funciona contratante
	// Funcion para guardar seccion de comentarios
	public function saveSeccComentariosComoContratante(){
		if(trim($this->request->getPost('titulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la parte del titulo en color negro.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('destacado')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la parte del titulo en color distinto.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('inferior')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el texto inferior de la sección'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'como-funciona-contratante', 'tipo' => 'comentarios'])->first();
		$info = [
			'titulo' => trim($this->request->getPost('titulo')),
			'destacado' => trim($this->request->getPost('destacado')),
			'inferior' => trim($this->request->getPost('inferior'))
		];
		if($seccion == null){
			$data = [
				'seccion' => 'como-funciona-contratante',
				'tipo' => 'comentarios',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada.'], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar seccion de miles de oportunidades
	public function saveMilesOportunidadesContratante(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesion a caducado.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('superior')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un texto para la parte superior de la sección.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('titulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un titulo para la sección.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('descripcion')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese una descripción para la sección.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('imagen')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Cargue una imagen para la sección'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'como-funciona-contratante', 'tipo' => 'oportunidades'])->first();
		$info = [
			'superior' => trim($this->request->getPost('superior')),
			'titulo' => trim($this->request->getPost('titulo')),
			'descripcion' => trim($this->request->getPost('descripcion')),
			'imagen' => str_replace(' ', '+', trim($this->request->getPost('imagen')))
		];
		if($seccion == null){
			$data = [
				'seccion' => 'como-funciona-contratante',
				'tipo' => 'oportunidades',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada.'], JSON_FORCE_OBJECT);
	}
	// Funcion para mostrar datos de paso
	public function verPasoContratante(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesion a caducado.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('id')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro encontrar el id del paso a editar'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'como-funciona-contratante', 'tipo' => 'tres-pasos-'.trim($this->request->getPost('id'))])->first();
		if($seccion == null){
			$data = null;
			return json_encode(['tipo' => 'success', 'mensaje' => view('/backend/administrador/viewcells/editor-paso-contratista', array('datos' => $data, 'id' => trim($this->request->getPost('id'))))], JSON_FORCE_OBJECT);
		}else{
			$data = json_decode($seccion['info'], true);
			return json_encode(['tipo' => 'success', 'mensaje' => view('/backend/administrador/viewcells/editor-paso-contratista', array('datos' => $data, 'id' => trim($this->request->getPost('id'))))], JSON_FORCE_OBJECT);
			// return json_encode(['tipo' => 'success', 'titulo' => $data['titulo'], 'contenido' => $data['contenido'], 'imagen' => $data['imagen']], JSON_FORCE_OBJECT);
		}
	}
	// Funcion para guardar datos generales de 3 pasos de contratista
	public function saveTresPasosContratante(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesion a caducado.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('titulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la parte de texto oscuro del titulo.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('destacado')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la parte en destacado del titulo.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('contenido')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el texto inicial de la sección.'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'como-funciona-contratante', 'tipo' => 'tres-pasos'])->first();
		$info = [
			'titulo' => trim($this->request->getPost('titulo')),
			'destacado' => trim($this->request->getPost('destacado')),
			'contenido' => trim($this->request->getPost('contenido'))
		];
		if($seccion == null){
			$data = [
				'seccion' => 'como-funciona-contratante',
				'tipo' => 'tres-pasos',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada'], JSON_FORCE_OBJECT);
	}
	// Funcion para actualizar datos de paso
	public function updatePasoContratante(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesion a caducado.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('id')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'No se logro encontrar el id del icono.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('titulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un titulo para la sección.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('contenido')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un texto informativo para el paso.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('imagen')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un icono para e paso.'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'como-funciona-contratante', 'tipo' => 'tres-pasos-'.trim($this->request->getPost('id'))])->first();
		$info = [
			'titulo' => trim($this->request->getPost('titulo')),
			'contenido' => trim($this->request->getPost('contenido')),
			'imagen' => str_replace(' ', '+', trim($this->request->getPost('imagen')))
		];
		if($seccion == null){
			$data = [
				'seccion' => 'como-funciona-contratante',
				'tipo' => 'tres-pasos-'.trim($this->request->getPost('id')),
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada'], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar seccion de crear cuenta
	public function saveCrearCuentaContratante(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesion a caducado.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('superior')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el texto superior al titulo.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('titulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el titulo para la sección.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('subtitulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese un subtitulo para la sección.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('imagen')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la imagen para la sección.'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'como-funciona-contratante', 'tipo' => 'crea-una-cuenta'])->first();
		$info = [
			'superior' => trim($this->request->getPost('superior')),
			'titulo' => trim($this->request->getPost('titulo')),
			'subtitulo' => trim($this->request->getPost('subtitulo')),
			'imagen' => str_replace(' ', '+', trim($this->request->getPost('imagen')))
		];
		if($seccion == null){
			$data = [
				'seccion' => 'como-funciona-contratante',
				'tipo' => 'crea-una-cuenta',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada'], JSON_FORCE_OBJECT);
	}
	// Funcion para guardar seccion principal de como funciona contratista
	public function saveHeaderComoFuncionaContratante(){
		if(!session('is_logged')) return json_encode(['tipo' => 'error', 'mensaje' => 'Su sesion a caducado.'], JSON_FORCE_OBJECT);
		if(session('rol') != 'admin') return json_encode(['tipo' => 'error', 'mensaje' => 'OOPS, lo siento no puedo dejar que veas esta sección, ingresa a tu cuenta para acceder correctamente.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('titulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la parte incial del titulo.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('destacado')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese la parte destacada del titulo.'], JSON_FORCE_OBJECT);
		if(trim($this->request->getPost('subtitulo')) == '') return json_encode(['tipo' => 'error', 'mensaje' => 'Ingrese el subtitulo para la sección.'], JSON_FORCE_OBJECT);
		$seccion = $this->seccion->where(['seccion' => 'como-funciona-contratante', 'tipo' => 'area-slides'])->first();
		$info = [
			'titulo' => trim($this->request->getPost('titulo')),
			'destacado' => trim($this->request->getPost('destacado')),
			'subtitulo' => trim($this->request->getPost('subtitulo'))
		];
		if($seccion == null){
			$data = [
				'seccion' => 'como-funciona-contratante',
				'tipo' => 'area-slides',
				'info' => json_encode($info, JSON_FORCE_OBJECT)
			];
			$this->seccion->save($data);
		}else{
			$this->seccion->where('id', $seccion['id'])->set(['info' => json_encode($info, JSON_FORCE_OBJECT)])->update();
		}
		return json_encode(['tipo' => 'success', 'mensaje' => 'Sección actualizada.'], JSON_FORCE_OBJECT);
	}
	/////////////////////////////////////////////////////////////////////////// Ajustes generales
	public function saveCodigosCabecera(){
		if(trim($this->request->getPost('codigos')) == '') return 'No hay datos';
		// return trim($this->request->getPost('codigos'));
	}
} 