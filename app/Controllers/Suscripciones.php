<?php namespace App\Controllers;
use App\Libraries\NetPay;
use App\Libraries\Correos;

class Suscripciones extends BaseController{
	protected $suscripciones;

	public function __construct(){
		$this->suscripciones = model('Suscripciones');
		helper('text');
		helper('funciones');
	}

	public function crear_suscripcion(){
		// if($this->esta_suscrito_por_idUsuario(session('id'))) return json_encode(['alerta' => 'falla', 'mensaje' => 'Ya cuenta con una suscripción'], JSON_FORCE_OBJECT);
		
		// Cancelamos suscripcion actual, sino cuenta con suscripción la crea
		$suscripcion_cancelada_decode = $this->cancelar_suscripcion();
		$suscripcion_cancelada = json_decode($suscripcion_cancelada_decode, true);
		if($suscripcion_cancelada['alerta'] != 'correcto') return $suscripcion_cancelada_decode;
		
		return $this->suscripciones->crear_suscripcion();
	}

	public function crear_tienda(){
		// if($this->esta_suscrito_por_idUsuario(session('id'))) return json_encode(['alerta' => 'falla', 'mensaje' => 'Ya cuenta con una suscripción'], JSON_FORCE_OBJECT);
		
		// Cancelamos suscripcion actual, sino cuenta con suscripción la crea
		$suscripcion_cancelada_decode = $this->cancelar_suscripcion();
		$suscripcion_cancelada = json_decode($suscripcion_cancelada_decode, true);
		if($suscripcion_cancelada['alerta'] != 'correcto') return $suscripcion_cancelada_decode;
		
		$respuesta = $this->suscripciones->crear_tienda();
		if(!is_string($respuesta)) return json_encode(['alerta' => 'falla', 'mensaje' => 'No se pudo procesar la solicitud, inténtelo nuevamente.'], JSON_FORCE_OBJECT);
		
		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Pago en proceso', 'referencia' => $respuesta], JSON_FORCE_OBJECT);
	}

	public function actualizarTarjeta(){
		$id_tarjeta = sanitizar($this->request->getPost('id'));
		if(empty($id_tarjeta)) return json_encode(['alerta' => 'falla', 'mensaje' => 'Inténtelo nuevamente recargando la página'], JSON_FORCE_OBJECT);
		return $this->suscripciones->actualizarTarjeta($id_tarjeta);
	}

	public function cancelar_suscripcion($id = 'false'){
		($id == 'false')? $id = session('id') : $id = $id;
		if(!$this->esta_suscrito_por_idUsuario($id)) return json_encode(['alerta' => 'correcto', 'mensaje' => 'Sin suscripción'], JSON_FORCE_OBJECT);
		
		$suscripcion_actual = $this->suscripciones->where('id_usuario', $id)->first();
		if(empty($suscripcion_actual)) return json_encode(['alerta' => 'correcto', 'mensaje' => 'Sin suscripción'], JSON_FORCE_OBJECT);
		
		if($suscripcion_actual['tipo_suscripcion'] == 'tarjeta'){
			$netpay = new NetPay();

			$suscripcion_cancelada = $netpay->cancelarSuscripcion($suscripcion_actual['suscripcion']);
			if(isset($suscripcion_cancelada['message'])) return json_encode(['alerta' => 'falla', 'mensaje' => 'Oh no! No pude cancelar tu suscripción: '.$suscripcion_cancelada['message']], JSON_FORCE_OBJECT);
		}
		$this->suscripciones->cancelar($suscripcion_actual['suscripcion']);
		// $correo = new Correos();
		// $correo->cancelarSuscripcion($usuario, $suscripcion_cancelada);
		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Tu suscripción se ha cancelado correctamente'], JSON_FORCE_OBJECT);
	}

	public function cancelar_suscripcion_cronjob($id){
		if(!$this->esta_suscrito_por_idUsuario($id)) return false;
	}

	public function getPlan(){
		$id = sanitizar($this->request->getPost('id'));

		$plan = $this->suscripciones->informacion_plan($id);
		$plan['es_tarjeta'] = sanitizar($this->request->getPost('es_tarjeta')) == 'true' ? true : false;
		// Estos datos son para mostrar los precios ajustados en checkout cuando aplica un cupón de paga
		$plan['monto_descuento'] = sanitizar($this->request->getPost('descuento'));
		$plan['precio_total'] = sanitizar($this->request->getPost('precio'));
		
		$mensaje = view('/backend/viewcells/resumen-compra', $plan);
		if(!empty($plan['monto_descuento']) && !empty($plan['precio_total'])) return $mensaje;

		return json_encode(['alerta' => 'correcto', 'mensaje' => $mensaje], JSON_FORCE_OBJECT);
	}

	public static function esta_suscrito_por_idUsuario($id){
		$suscripcion = model('Suscripciones')->where('id_usuario', $id)->first();
		if(empty($suscripcion)) return false;
		$hoy = date('Y-m-d H:i:s');
		
		return (strtotime($suscripcion['fecha_fin']) >= strtotime($hoy));
	}

	public function reseteoPostulacionesUsuarios(){
		$es_reseteado = model('PostulacionesUsuarios')->truncate();
		if($es_reseteado) return 'Se ha resetado cantidad de postulaciones. Fecha: '.date('d-m-Y H:i:s');
		else return 'Se ha resetado cantidad de postulaciones. Fecha: '.date('d-m-Y H:i:s');
	}

	public function boletinesSuscripciones(){
		$correo = sanitizar($this->request->getPost('correo'));
		if(empty($correo)) return json_encode(['alerta' => 'custom', 'mensaje' => 'Ingresa tu correo'], JSON_FORCE_OBJECT);
		$validation =  \Config\Services::validation();
		$dat = [
			'correo' => $correo,
		];
		if(!$validation->run($dat, 'correo')){
			return json_encode(['alerta' => 'custom', 'mensaje' => 'Ingrese un correo válido'], JSON_FORCE_OBJECT);
		}
		
		$correosBD = model('Boletines')->where('correo', $correo)->first();
		
		if(!empty($correosBD)) return json_encode(['alerta' => 'custom', 'mensaje' => 'Ya te encuentras registrado a nuestras noticias'], JSON_FORCE_OBJECT);
		$guardado = model('Boletines')->save(['correo' => $correo]);
		
		if($guardado) return json_encode(['alerta' => 'correcto', 'mensaje' => 'Enhorabuena, ahora recibirás noticias del equipo MEXTEMPS FREELANCE'], JSON_FORCE_OBJECT);
		else return json_encode(['alerta' => 'falla', 'mensaje' => 'No se pudo guardar, inténtelo nuevamente'], JSON_FORCE_OBJECT);
	}

}