<?php namespace App\Controllers;

use App\Libraries\NetPay;
use App\Libraries\Correos;
use App\Libraries\AccesosPlanes;
use App\Controllers\Suscripciones;
use App\Controllers\Pagos;

class Usuarios extends BaseController{
	protected $usuarios, $proyectos, $modelsolicitudes, $habilidades, $calificaciones, $facturas;
	public function __construct(){
		$this->usuarios = model('Autenticacion');
		$this->proyectos = model('Proyectos');
		$this->modelsolicitudes = model('Solicitudes');
		$this->habilidades = model('Habilidades');
		$this->calificaciones = model('Calificaciones');
		$this->facturas = model('Facturas');
		helper('text');
		
		$userverficar = $this->usuarios->where('id', session('id'))->first();
		if(empty($userverficar)) return redirect()->to('');
		
		$sin_suscripcion = !Suscripciones::esta_suscrito_por_idUsuario(session('id'));
		
		if(($userverficar['estatus_registro'] != 'completado') && ($userverficar['rol'] == 'freelancer') && !empty(($userverficar['plan'])) && $sin_suscripcion) header('Location: '.base_url('Llenar_perfil'));
		elseif(($userverficar['estatus_registro'] != 'completado') && ($userverficar['rol'] == 'contratante') && !empty(($userverficar['plan'])) && $sin_suscripcion) header('Location: '.base_url('Completar_registro'));
	}
	public function verPagos(){
		if(!session('logeado')) return '<div class="text-center">Su sesión ha terminado, ingrese nuevamente para ver su historial de pagos</div>';
		$pagos = model('Pagos')->where('id_usuario', session('id'))->orderBy('id', 'DESC')->find();
		if($pagos == null) return vacio('Sin pagos realizados');

		$usuario = model('Autenticacion')->get_id(session('id'));
		return view('/backend/viewcells/pagos', array('pagos' => $pagos, 'user' => $usuario));
	}
	public function perfil($activo = 'panel'){
		if(!session('logeado')) return redirect()->route('');

		$tarjetas = $this->usuarios->get_tarjetas_usuario_ID(session('id'));
		$habilidades = $this->habilidades->where('superior !=', '0')->orderBy('area', 'asc')->findAll();
		$con_suscripcion = Suscripciones::esta_suscrito_por_idUsuario(session('id'));
		if($con_suscripcion){
			$suscripcion_actual = model('Suscripciones')
							 ->builder()
							 ->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
							 ->where('id_usuario', session('id'))
							 ->get()->getRowArray();
		}
		else $suscripcion_actual = null;
		
		$usuario = $this->usuarios->get_id(session('id'));
		if(autorizacion() == 'freelancer'){
			$proyectos = $this->proyectos->where('estatus', 'espera')->orderBy('destacado', 'DESC')->orderBy('id', 'DESC')->limit('3')->find();
			$permisos = new AccesosPlanes($usuario['plan']);
			
			if(isset($suscripcion_actual['tarjeta_suscripcion'])) {
                $tarjeta_suscripcion = model('Autenticacion')->get_tarjeta_idUsuario_suscripciones($usuario['id_cliente'], $suscripcion_actual['tarjeta_suscripcion']);
			}else {
                $tarjeta_suscripcion = null;
			}
			return view(freelancer('Perfil'), array(
				'tarjeta_suscripcion' => $tarjeta_suscripcion,
				'freelancer' => $usuario,
				'tarjetas' => $tarjetas,
				'habilidades' => $habilidades,
				'con_suscripcion' => $con_suscripcion,
				'suscripcion_actual' => $suscripcion_actual,
				'metodoPago' => Pagos::tipoPago($suscripcion_actual),
				'descripcion' => view_cell('App\Libraries\FuncionesSistema::estaadoDescripcionSuscripcion'),
				'panel_activo' => $activo,
				'proyectos' => $proyectos,
				'cargar_cv' => $permisos->getValor_permiso('crearcv'),
				'añadir_portafolio' => $permisos->getValor_permiso('anadirportafolio')));
		}

		if(autorizacion() == 'contratante'){
			// 			$pagos = $this->usuarios->get_cargos();
			$subareas = $this->habilidades->where('superior !=', '0')->find();
			$users = $this->usuarios->where('rol', 'freelancer')->findAll(16, 0);
			
			$sueldosMaximos = $this->usuarios->selectMax('pagoHora')->first();
			empty($sueldosMaximos['pagoHora']) ? $sueldo_max = 1 : $sueldo_max = $sueldosMaximos['pagoHora'];
			
			if(isset($suscripcion_actual['tarjeta_suscripcion'])) {
                $tarjeta_suscripcion = model('Autenticacion')->get_tarjeta_idUsuario_suscripciones($usuario['id_cliente'], $suscripcion_actual['tarjeta_suscripcion']);
			}else {
                $tarjeta_suscripcion = null;
			}
			
			return view(contratista('Perfil'), array(
			    'tarjeta_suscripcion' => $tarjeta_suscripcion,
				'contratista' => $this->usuarios->get_id(session('id')),
				'freelances' => $users,
				'areas' => $habilidades,
				'subareas' => $subareas,
				'total_proyectos' => $this->proyectos->where('id_contratista', session('id'))->countAllResults(),
				'sueldo_max' => $sueldo_max,
				'con_suscripcion' => $con_suscripcion,
				'suscripcion_actual' => $suscripcion_actual,
				'metodoPago' => Pagos::tipoPago($suscripcion_actual),
				'descripcion' => view_cell('App\Libraries\FuncionesSistema::estaadoDescripcionSuscripcion'),
				'panel_activo' => $activo,
				'totalfree' => count($this->get_Freelancers(1000000)),
				'tarjetas' => $tarjetas));
		}
	}
	
	public function prevista($id){
		$datos['usuario'] = $this->usuarios->get_id(base64_decode($id));
		($datos['usuario']['experiencia'] == null)? $datos['usuario']['cantidad_experiencias'] = 0 : $datos['usuario']['cantidad_experiencias'] = count($datos['usuario']['experiencia']);
		($datos['usuario']['experiencia'] == null)? $datos['usuario']['experiencia'] = null : $datos['usuario']['experiencia'] = array_slice($datos['usuario']['experiencia'], 0, 3);
		$permisos = new AccesosPlanes(session('id'));
		$datos['acceso'] = $permisos->getValor_permiso('contactarFreelancer');
		return view(freelancer('PrevistaPerfil'), $datos);
	}
	
	public function verMasExperienciasFreelancer(){
		$id = sanitizar($this->request->getGet('id'));
		$cantidad = sanitizar($this->request->getGet('cantidad'));
		$freelancer = $this->usuarios->get_id($id);
		return view(Freelancer('viewcells/Experiencias_prevista'), array('experiencias' => array_slice($freelancer['experiencia'], $cantidad - 3, $cantidad)));
	}
	
	public function solicitudes(){
		$con_suscripcion = session('logeado')? Suscripciones::esta_suscrito_por_idUsuario(session('id')) : false;
		// if(!$con_suscripcion) return  redirect()->back()->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Para poder visualizar el siguiente apartado te recomendamos actualizar tu plan a uno superior que te permita hacer uso de esta y otras herramientas.']);
		
		if(autorizacion() == 'freelancer') {
			// $user = $this->usuarios->get_id(session('id'));
		  //  if($user['aprobado'] != true) return redirect()->back()->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Opps, antes de poder ver tus solcitudes de proyectos, nuestro equipo de administración deberá aprobar tu perfil.']);
			$select = ['solicitudes.id', 'solicitudes.id_proyecto', 'proyectos.estatus', 'proyectos.id_contratista','proyectos.titulo','proyectos.desc_corta', 'proyectos.descripcion', 'proyectos.categoria', 'proyectos.subcategoria', 'proyectos.fecha_arranque_esperado', 'proyectos.fecha_entrega', 'proyectos.presupuesto', 'proyectos.imagenes', 'proyectos.nivel'];
			// $builder = db_connect()->table('solicitudes');
			$proyectos = model('Solicitudes')->select($select)->where(['solicitudes.id_freelancer' => session('id'), 'solicitudes.estatus' => 'espera'])->join('proyectos', 'solicitudes.id_proyecto = proyectos.id')->orderBy('solicitudes.id', 'DESC')->find();
			// $proyectos = $builder->get();
			return view(freelancer('Solicitudes'), array('freelancer' => $this->usuarios->get_id(session('id')), 'proyectos' => $proyectos));
		}
		if(autorizacion() == 'contratante') return redirect()->to('/usuarios/perfil');
		
		return redirect()->route('');
	}
	public function solicitud($id){
		if(!session('logeado')) return redirect()->route('');
		if(autorizacion() == 'contratista') return view(contratista('Solicitud'));
		
		$usuario = $this->usuarios->get_id(session('id'));
		if(autorizacion() == 'freelancer') {
			// $permisos = new AccesosPlanes($usuario['plan']);
			// $permisos->validarFreelancer('solicitudtrabajo');

			$solicitud = $this->modelsolicitudes->find($id);
			$proyecto = $this->proyectos->find($solicitud['id_proyecto']);
			return view(freelancer('Solicitud'), compact('proyecto', 'solicitud'));
		}
	}
	public function chat($id){
		$id = sanitizar($id);
		$sala = model('Salas')->find($id);
		if($sala == null) return redirect()->back()->with('alerta', [
			'tipo' => 'advertencia',
			'mensaje' => 'Lo sentimos, la sala de chat del proyecto ha sido finalizada.'
		]);
		$proyecto = $this->proyectos->where(['id' => $sala['id_proyecto']])->first();
		if($proyecto == null) return redirect()->back()->with('alerta', [
			'tipo' => 'advertencia',
			'mensaje' => 'Lo sentimos, el proyecto que buscas al parecer ha sido eliminado o cancelado dentro de la plataforma.'
		]);
		if($proyecto['estatus'] == 'espera'){
			$con_suscripcion = session('logeado')? Suscripciones::esta_suscrito_por_idUsuario(session('id')) : false;
			if(!$con_suscripcion) return  redirect()->back()->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Para poder visualizar el siguiente apartado te recomendamos actualizar tu plan a uno superior que te permita hacer uso de esta y otras herramientas.']);
		}
		$usuario = $this->usuarios->get_id(session('id'));
		if(autorizacion() == 'freelancer'){
			if(session('id') != $sala['id_freelancer']) return redirect()->back();
			$cantmsg = $sala['mensajes'];
			
			$permisos = new AccesosPlanes($usuario['plan']);
			if($proyecto['estatus'] == 'finalizado'){
				$redirect = $permisos->validarFreelancer('historialconversaciones', 'true');
				if(!(empty($redirect))){
					if($redirect->getStatusCode() == 307) return $redirect;
				}
			}
			return view(freelancer('Chat'), ['usuario' => $this->usuarios->get_id($sala['id_contratista']), 'id_sala' => $id, 'cantmsg' => $cantmsg, 'proyecto' => $proyecto]);
		}
		if(autorizacion() == 'contratante'){
			if(session('id') != $sala['id_contratista']) return redirect()->back();
			$cantmsg = $sala['mensajes'];
			$permisos = new AccesosPlanes($usuario['plan']);
			if($proyecto['estatus'] == 'finalizado'){
				$redirect = $permisos->validarContratista('historialconversaciones', 'true');
				if(!(empty($redirect))){
					if($redirect->getStatusCode() == 307) return $redirect;
				}
			}
			$freelancer = $this->usuarios->get_id($sala['id_freelancer']);
			$freelancer['es_premium'] = false;
			
			$monto = model('ModelPlanes')->selectMax('monto')->where('cuenta', 'freelancer')->get()->getRowArray()['monto'];
			$con_suscripcion_premium = (model('Suscripciones')
    			 ->builder()
    			 ->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
    			 ->where(['id_usuario' => $sala['id_freelancer'], 'estatus' => 'Activo', 'monto' => $monto])
    			 ->get()->getRowArray());
				 
			if((!empty($con_suscripcion_premium)) && Suscripciones::esta_suscrito_por_idUsuario($sala['id_freelancer'])) $freelancer['es_premium'] = true;

			return view(contratista('Chat'), ['usuario' => $freelancer, 'id_sala' => $id, 'cantmsg' => $cantmsg, 'proyecto' => $proyecto]);
		}
		return redirect()->route('');
	}
	public function talento(){
		$buscar = sanitizar($this->request->getGet('search'));
		$area = sanitizar($this->request->getGet('area'));

		$datos['habilidades'] = $this->habilidades->where(['superior' => '0'])->orderBy('area', 'asc')->find();
		$datos['subareas'] = $this->habilidades->where('superior !=', '0')->findAll();
		$us = $this->usuarios->selectMax('pagoHora')->first();
		$datos['sueldo_max'] = ($us['pagoHora'] == null)? 1 : $us['pagoHora'];

		$usuario = model('Autenticacion')->get_UsuarioID(session('id'));
		$permisos = new AccesosPlanes($usuario['plan'] ?? '');
		
		$buscar_freelancers = $permisos->getValor_permiso('buscadorfreelancer');
		$datos['nivel'] = ($buscar_freelancers === 0 || ($buscar_freelancers == 'restringido')) && !Suscripciones::esta_suscrito_por_idUsuario(session('id')) ? false : true;
		// dd($datos['nivel']);
		$where = "WHERE rol = 'freelancer'";

		// SE VALIDA TAMBIEN SI PUEDE BUSCAR O NO POR ILIMITADO DEPENDIENDO EL PLAN
		if(!empty($buscar) && $datos['nivel']) $where .= " AND (historial LIKE '%$buscar%' OR concatenacion.nombre_completo LIKE '%$buscar%' ESCAPE '!' OR correo LIKE '%$buscar%' ESCAPE '!' OR areas LIKE '%$buscar%')";
		if(!empty($area) && ($area != 'Área')) $where .= " AND areas LIKE '%$area%' ESCAPE '!'";
		
		$datos['freelances'] = db_connect()->query("SELECT * FROM (SELECT *, CONCAT(nombre,' ',apellidos) AS nombre_completo FROM usuarios) concatenacion ".$where." ORDER BY id DESC LIMIT 12")->getResultArray();
		$datos['totalfree'] = count($datos['freelances']);

		$monto = model('ModelPlanes')->selectMax('monto')->where('cuenta', 'freelancer')->get()->getRowArray()['monto'];
		foreach($datos['freelances'] as $key => $freelancer){
		    $datos['freelances'][$key]['es_premium'] = false;
		    $con_suscripcion_premium = (model('Suscripciones')
				->builder()
				->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
				->where(['id_usuario' => $freelancer['id'], 'estatus' => 'Activo', 'monto' => $monto])
				->get()->getRowArray());
				 
			if((!empty($con_suscripcion_premium)) && Suscripciones::esta_suscrito_por_idUsuario($freelancer['id'])) $datos['freelances'][$key]['es_premium'] = true;
			$aux[$key] = $datos['freelances'][$key]['es_premium'];
		}
		array_multisort($aux, SORT_DESC, $datos['freelances']); //SE ORDENA POR CUENTAS PREMIUM PRIMERO

		return view(contratista('Talento'), $datos);
	}
	
	public function paginacionTalentos(){
		$cantidad = sanitizar($this->request->getGet('cantidad'));
		$talentos = $this->get_Freelancers(12, $cantidad);
		if(empty($talentos)) return vacio();
		
		foreach($talentos as $talento){
			$freelancer = $this->usuarios->get_id($talento['id']);
			$freelancer['es_premium'] = $talento['es_premium'];
			echo '<div class="col-12 col-md-6 col-lg-4 mb-4">';
			echo view(Contratista('viewcells/postulante'), array('user'=>$freelancer, 'postu' => 0, 'tipo' => 'talento', 'modal' => 0));
			echo '</div>';
		}
		echo '<br>';
	}
	
	public function getHabilidadesTalento(){
		$area = sanitizar($this->request->getPost('area'));
		$subarea = $this->habilidades->where('superior', $area)->orderBy('area', 'ASC')->get()->getResultArray();
		foreach($subarea as $s){
			echo '<option value="'.$s['area'].'">';
		}
	}
	
	public function get_FreelancersReseteoFiltro($valor){
		$freelancers = $this->get_Freelancers($valor);
		if(empty($freelancers)) return json_encode(['html' => vacio(), 'cantidad_freelancers' => 0], JSON_FORCE_OBJECT);
		$html = '';
		foreach($freelancers as $freelancer){
			$talento = $this->usuarios->get_id($freelancer['id']);
			$talento['es_premium'] = $freelancer['es_premium'];
			$html.= '<div class="col-12 col-md-6 col-lg-4 mb-4">';
			$html.= view('backend/Contratista/viewcells/postulante', array('user'=> $talento, 'postu' => 0, 'tipo' => 'talento', 'modal' => 0));
			$html.= '</div>';
		}
		$html.= '<br>';
		return json_encode(['html' => $html, 'cantidad_freelancers' => count($freelancers)], JSON_FORCE_OBJECT);
	}
	
	public function get_Freelancers($indice, $offset = 0){
		$freelancers = $this->usuarios->where('rol', 'freelancer')->find();
	    $monto = model('ModelPlanes')->selectMax('monto')->where('cuenta', 'freelancer')->get()->getRowArray()['monto'];
		
		foreach($freelancers as $key => $freelancer){
		    $freelancers[$key]['es_premium'] = false;
		    $con_suscripcion_premium = (model('Suscripciones')
    			 ->builder()
    			 ->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
    			 ->where(['id_usuario' => $freelancer['id'], 'estatus' => 'Activo', 'monto' => $monto])
    			 ->get()->getRowArray());
				 
			if((!empty($con_suscripcion_premium)) && Suscripciones::esta_suscrito_por_idUsuario($freelancer['id'])) $freelancers[$key]['es_premium'] = true;
			$aux[$key] = $freelancers[$key]['es_premium'];
		}
		
		array_multisort($aux, SORT_DESC, $freelancers); //SE ORDENA POR CUENTAS PREMIUM PRIMERO
		
		return array_slice($freelancers, $offset, $indice); //SE ENTREGA FILTRADO POR LA CANTIDAD LIMITADA DE FREELANCERS
	}
	public function resultFiltroFreelance(){
		$clave = sanitizar($this->request->getPost('clave'));
		$area = sanitizar($this->request->getPost('area'));
		$habilidades = sanitizar($this->request->getPost('habilidades'));
		$ubicacion = sanitizar($this->request->getPost('ubicacion'));
		$estrellas = sanitizar($this->request->getPost('estrellas'));
		$concluidos = $this->request->getPost('concluidos');
		$sueldomin = sanitizar($this->request->getPost('sueldomin'));
		$sueldomax = sanitizar($this->request->getPost('sueldomax'));
		$idioma = sanitizar($this->request->getPost('idioma'));
		// $cantidad = sanitizar($this->request->getPost('cantidad'));
		
		$where = "WHERE rol = 'freelancer' ";
		if($clave != '') $where .= " AND (historial LIKE '%".$clave."%' OR concatenacion.nombre_completo LIKE '%".$clave."%' OR correo LIKE '%".$clave."%' OR areas LIKE '%".$clave."%')";
		if($area != '' && ($area != 'Seleccione una opción')) $where .= " AND areas LIKE '%".$area."%' ESCAPE '!'";
		if($habilidades != '' && ($habilidades != 'Seleccione una opción')) $where .= " AND areas LIKE '%".$habilidades."%' ESCAPE '!'";
		if($idioma != 'default'  && !empty($idioma)) $where .= " AND idiomas like '%".$idioma."%' ESCAPE '!'";

		$activos= db_connect()->query("SELECT * FROM (SELECT *, CONCAT(nombre,' ',apellidos) AS nombre_completo, JSON_EXTRACT(`direccion`, '$.0.estado') as estado_decode FROM usuarios) concatenacion ".$where." ORDER BY id DESC LIMIT 12")->getResultArray();
		if($activos == null) return json_encode(['html' => vacio(), 'cantidad_resultados' => 0], JSON_FORCE_OBJECT);
		
		if($estrellas != ''){
			foreach($activos as $key => $row){
				$cals = $this->calificaciones->where('id_freelancer', $row['id'])->find();
				$suma = 0;
				if($cals == null) $promedio = 0;
				else{
					foreach($cals as $c){
						$suma += $c['calificacion'];
					}
					$promedio = round($suma / count($cals));
				}
				if($promedio != $estrellas) unset($activos[$key]);
			}
		}
		if($sueldomin != '' && $sueldomax != ''){
			foreach($activos as $key => $row){
				if($row['pagoHora'] < $sueldomin || $row['pagoHora'] > $sueldomax) unset($activos[$key]);
			}
		}elseif($sueldomin == '' && $sueldomax != ''){
			foreach($activos as $key => $row){
				if($row['pagoHora'] > $sueldomax) unset($activos[$key]);
			}
		}elseif($sueldomin != '' && $sueldomax == ''){
			foreach($activos as $key => $row){
				if($row['pagoHora'] < $sueldomin) unset($activos[$key]);
			}
		}
		
		if($ubicacion != ''){
		    foreach($activos as $key => $row){
                // Decodificar el texto UNICODE a utf-8
                $texto_normal = json_decode($row['estado_decode'], JSON_UNESCAPED_UNICODE);
                
				if($texto_normal != $ubicacion) unset($activos[$key]);
			}
		}
		
		if($concluidos != NULL){
			$rangos = [];
			foreach($concluidos as $concluido){
				switch($concluido){
					case 0:
						$dat = ['minimo' => 1, 'maximo' => 15];
						break;
					case 1:
						$dat = ['minimo' => 16, 'maximo' => 30];
						break;
					case 2:
						$dat = ['minimo' => 31, 'maximo' => 45];
						break;
					case 3:
						$dat = ['minimo' => 46, 'maximo' => 60];
						break;
					case 4:
						$dat = ['minimo' => 61, 'maximo' => 99];
						break;
					case 5:
						$dat = ['minimo' => 100, 'maximo' => 10000000000];
						break;
				}
				$rangos[] = $dat;
			}
			
			foreach($activos as $key => $a){
				$cantPro = model('Proyectos')->where(['estatus' => 'finalizado', 'id_freelancer' => $a['id']])->find();
				$x = 0;
				foreach($rangos as $r){
					if(count($cantPro) >= intval($r['minimo']) && count($cantPro) <= intval($r['maximo'])) $x = 1;
				}
				if($x == 0) unset($activos[$key]);
			}
			foreach($activos as $key => $row){
				$data = $this->proyectos->where(['id_freelancer' => $row['id'], 'estatus' => 'finalizado'])->find();
				$count = count($data);
				if($concluidos == 0){
					if($count <= 25) unset($activos[$key]);
				}elseif($concluidos == 1){
					if($count <= 50) unset($activos[$key]);
				}elseif($concluidos == 2){
					if($count <= 100) unset($activos[$key]);
				}
			}
		}
		if(empty($activos)) return json_encode(['html' => vacio(), 'cantidad_resultados' => 0], JSON_FORCE_OBJECT);

		$html = '';
		$monto = model('ModelPlanes')->selectMax('monto')->where('cuenta', 'freelancer')->get()->getRowArray()['monto'];
		foreach($activos as $key => $row){
			$user = $this->usuarios->get_id($row['id']);
			$activos[$key]['es_premium'] = false;
		    $con_suscripcion_premium = (model('Suscripciones')
				->builder()
				->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
				->where(['id_usuario' => $row['id'], 'estatus' => 'Activo', 'monto' => $monto])
				->get()->getRowArray());
				 
			if((!empty($con_suscripcion_premium)) && Suscripciones::esta_suscrito_por_idUsuario($user['id'])) $activos[$key]['es_premium'] = true;
			$aux[$key] = $activos[$key]['es_premium'];
		}

		array_multisort($aux, SORT_DESC, $activos);
		
		foreach($activos as $row){
			$user = $this->usuarios->get_id($row['id']);
			$user['es_premium'] = $row['es_premium'];
			$html.= '<div class="col-12 col-md-6 col-lg-4 mb-4">';
			$html.= view('backend/Contratista/viewcells/postulante', array('user'=>$user, 'postu' => 0, 'tipo' => 'talento', 'modal' => 0));
			$html.= '</div>';
		}
		$html.= '<br>';
		return json_encode(['html' => $html, 'cantidad_resultados' => count($activos)], JSON_FORCE_OBJECT);
	}
	public function gardarDireccion(){
		return $this->usuarios->guardarDireccion();
	}
	public function editarPerfil(){
		return autorizacion() == 'freelancer' ? $this->usuarios->editarPerfilFreelancer() : $this->usuarios->editarPerfil();
	}
	public function actuaImgPerfil(){
		$resp = sanitizar($this->request->getPost('resp'));
		if($resp == '') return json_encode(['alerta' => 'advertencia', 'mensaje' => 'Operación cancelada.'], JSON_FORCE_OBJECT);
		if($this->usuarios->where('id', session('id'))->set(['imagen' => $resp])->update()){
			session()->set(['imagen' => $resp]);
			return json_encode(['alerta' => 'correcto', 'mensaje' => 'Grandioso, ya guardé tu nueva imagen de perfil, estamos listos.', 'imagen' => $resp], JSON_FORCE_OBJECT);
		}
		return json_encode(['alerta' => 'falla', 'mensaje' => 'OPPS... Algo no resultó bién, que extraño, pero no te preocupes intenta de nuevo estoy seguro que puedo hacerlo.'], JSON_FORCE_OBJECT);
	}
	public function eliminar_cuenta(){
		if($this->usuarios->eliminar_cuenta()){
			$Correo = new Correos();
			$Correo->eliminacionCuenta(session('id'));
			return json_encode(['alerta' => 'correcto', 'mensaje' => 'Nos pone muy triste que te vallas, pero siempre puedes volver y recuperar tu cuenta.'], JSON_FORCE_OBJECT);
		}
		return json_encode(['alerta' => 'falla', 'mensaje' => 'Error al eliminar, inténtelo de nuevo o comuníquese a soporte'], JSON_FORCE_OBJECT);
	}
	public function recuperar_cuenta(){
		if($this->usuarios->recuperar_cuenta()) return json_encode(['alerta' => 'correcto', 'mensaje' => 'Listo... ya recuperé tu cuenta, me alegra que regreses, estoy listo para trabajar.'], JSON_FORCE_OBJECT);
		return json_encode(['alerta' => 'falla', 'mensaje' => 'Error al restaurar cuenta, inténtelo de nuevo o comuníquese a soporte'], JSON_FORCE_OBJECT);
	}
	public function guardarHistorial(){
		return $this->usuarios->guardarHistorial();
	}
	public function guardarAreas(){
		return $this->usuarios->guardarAreas();
	}
	public function eliminarArea(){
		return $this->usuarios->eliminarArea();
	}
	public function eliminarTodasAreas(){
		if($this->usuarios->eliminarTodasAreas()) return json_encode(['alerta' => 'correcto', 'mensaje' => 'Eliminado'], JSON_FORCE_OBJECT);
		
		return json_encode(['alerta' => 'falla', 'mensaje' => 'Error al eliminar, inténtelo de nuevo o comuníquese a soporte'], JSON_FORCE_OBJECT);
	}
	public function guardarPagoHora(){
		return $this->usuarios->guardarPagoHora();
	}
	public function guardarIdiomas(){
		return $this->usuarios->guardarIdiomas();
	}
	public function editarIdioma(){
		return $this->usuarios->editarIdioma();
	}
	public function eliminarIdioma(){
		return $this->usuarios->eliminarIdioma();
	}
	public function Subareas(){
		return $this->usuarios->Subareas();
	}
	public function guardar_Perfil_1(){
		$respuesta = $this->usuarios->guardar_Perfil_1();
		if($respuesta) return true;
		return redirect()->route('Llenar_perfil')->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'No se pudo guardar, inténtelo nuevamente']);
	}
	public function guardar_Perfil_1_contratista(){
		$respuesta = $this->usuarios->guardar_Perfil_1();
		if($respuesta) return true;
		return redirect()->route('Completar_registro')->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'No se pudo guardar, inténtelo nuevamente']);
	}
	public function editarExperiencia(){
		return $this->usuarios->editarExperiencia();
	}
	public function eliminarExperiencia(){
		return $this->usuarios->eliminarExperiencia();
	}
	public function get_estatus_perfil_ID($id){
		return $this->usuarios->get_estatus_perfil_ID($id);
	}
	public function datosUsuarioFreelance(){
		$id = sanitizar($this->request->getPost('id'));
		$user = $this->usuarios->get_id($id);
		return view('/viewcells/datos-freelance', array('freelance' => $user));
	}
	public function areasUsuario($id){
		$user = $this->usuarios->get_id($id);
		return view_cell('App\Libraries\FuncionesSistema::areasPerfil', ['freelancer' => $user]);
	}
	public function experienciasUsuario($id){
		$id = sanitizar($id);
		$user = $this->usuarios->get_id($id);
		return view_cell('App\Libraries\FuncionesSistema::experienciasPerfil', ['freelancer' => $user]);
	}
	public function ocultarPerfil(){
		if(!session('logeado')) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'Su sesión a expirado, vuelva a iniciar sesión para realizar la solicitud.'], JSON_FORCE_OBJECT);
		$user = $this->usuarios->get_id(session('id'));
		if($user == null) return json_encode(['tipo' => 'advertencia', 'mensaje' => ''], JSON_FORCE_OBJECT);
		if(sanitizar($this->request->getPost('tipo')) == '') return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logro encontrar el estatus deseado.'], JSON_FORCE_OBJECT);
		if(sanitizar($this->request->getPost('tipo')) == 'true'){
			$this->usuarios->where('id', $user['id'])->set(['ocultar' => true])->update();
			return json_encode(['tipo' => 'correcto', 'mensaje' => 'Listo... No mostraré tu perfil hasta que tú me lo indiques.'], JSON_FORCE_OBJECT);
		}else{
			$this->usuarios->where('id', $user['id'])->set(['ocultar' => false])->update();
			return json_encode(['tipo' => 'correcto', 'mensaje' => 'OK... Ahora ya mostraré tu perfil, cuenta con ello.'], JSON_FORCE_OBJECT);
		}
	}
	public function crearSolicitudFactura(){
		if(!session('logeado')) return json_encode(['tipo' => 'custom', 'mensaje' => ''], JSON_FORCE_OBJECT);
		$id_compra = sanitizar($this->request->getPost('id'));
		if(empty($id_compra)) return json_encode(['tipo' => 'advertencia', 'mensaje' => 'No se logró encontrar el ID de referencia.'], JSON_FORCE_OBJECT);
		
		$facturacion['nombre'] = sanitizar($this->request->getPost('nombre'));
		if(empty($facturacion['nombre'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Se te olvida llenar el Nombre para solicitar tu factura.'], JSON_FORCE_OBJECT);
		
		$facturacion['rfc'] = sanitizar($this->request->getPost('rfc'));
		if(empty($facturacion['rfc'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Se te olvida llenar tu RFC para solicitar tu factura.'], JSON_FORCE_OBJECT);
		
		$facturacion['correo'] = sanitizar($this->request->getPost('correo'));
		if(empty($facturacion['correo'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Se te olvida llenar tu correo para solicitar tu factura.'], JSON_FORCE_OBJECT);
		
		$facturacion['telefono'] = sanitizar($this->request->getPost('telefono'));
		if(empty($facturacion['telefono'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Se te olvida llenar tu teléfono para solicitar tu factura.'], JSON_FORCE_OBJECT);
		
		$facturacion['direccion']['zip'] = sanitizar($this->request->getPost('zip'));
		if(empty($facturacion['direccion']['zip'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Se te olvida llenar tu código postal para solicitar tu factura.'], JSON_FORCE_OBJECT);
		
		$facturacion['direccion']['calle'] = sanitizar($this->request->getPost('calle'));
		if(empty($facturacion['direccion']['calle'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Se te olvida llenar la calle y el número para solicitar tu factura.'], JSON_FORCE_OBJECT);
		
		$facturacion['direccion']['colonia'] = sanitizar($this->request->getPost('colonia'));
		if(empty($facturacion['direccion']['colonia'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Se te olvida llenar la colonia para solicitar tu factura.'], JSON_FORCE_OBJECT);
		
		$facturacion['direccion']['ciudad'] = sanitizar($this->request->getPost('ciudad'));
		if(empty($facturacion['direccion']['ciudad'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Se te olvida llenar la ciudad para solicitar tu factura.'], JSON_FORCE_OBJECT);
		
		$facturacion['direccion']['estado'] = sanitizar($this->request->getPost('estado'));
		if(empty($facturacion['direccion']['estado'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Se te olvida llenar el estado para solicitar tu factura.'], JSON_FORCE_OBJECT);
		
		$facturacion['direccion']['pais'] = sanitizar($this->request->getPost('pais'));
		if(empty($facturacion['direccion']['pais'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Se te olvida llenar el país para solicitar tu factura.'], JSON_FORCE_OBJECT);
		
		$this->facturas->save([
			'id_compra' => $id_compra,
			'id_user' => session('id'),
			'datos' => json_encode($facturacion, JSON_FORCE_OBJECT),
		]);

		$correos = new \App\Libraries\Correos;
		if($correos->enviarFacturacion($facturacion)); return json_encode(['tipo' => 'correcto', 'mensaje' => 'Listo... Ya realicé la solicitud para tu factura, en breve el equipo te la hace llegar a tu correo.'], JSON_FORCE_OBJECT);
		
		return json_encode(['tipo' => 'falla', 'mensaje' => 'No se pude enviar la notificación al correo, comunicarse a soporte.'], JSON_FORCE_OBJECT);
	}
	public function HistorialPagos(){
		return view_cell('App\Libraries\FuncionesSistema::historialPago');
	}
	public function mostrararchivo($tipo){
		if(!session('logeado')) return ' ';
		$user = $this->usuarios->get_id(session('id'));
		if($user == null) return ' ';
		if($user[$tipo] == null) return ' ';
		($tipo == 'cv')? $t = 'CV' : $t = 'Portafolio';
		$liga = base_url('/'.$user[$tipo]);
		$name = explode('/', $user[$tipo]);
		
		return view('/backend/Freelancer/viewcells/descargar-archivo', array('liga' => $liga, 'tipo' => $t, 'name' => $name[5]));
	}
	
	public function verMasCalificaciones(){
		$id = sanitizar($this->request->getPost('id'));
		$cantidad = sanitizar($this->request->getPost('cantidad'));
		$calificaciones = model('Calificaciones')->where('id_freelancer', $id)->orderBy('id', 'DESC')->limit(3, $cantidad)->find();
		if(empty($calificaciones)) return '';
		return view('/backend/viewcells/card-calificacion', array('calificaciones' => $calificaciones, 'id' => $id));
	}
}
