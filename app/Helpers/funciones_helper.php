<?php

if (!function_exists('get_Facebook')) {
	function get_Facebook($clave = ''){
		$db = db_connect();
		$tabla = $db->table('configuraciones');
		$apis = $tabla->getWhere(['clave' => 'facebook'])->getRowArray();
		if(empty($apis)) return 'api_key'.$clave;
		
		$apis = json_decode($apis['valor'], true);
		return $apis[$clave];
	}
}
if (!function_exists('get_Google')) {
	function get_Google($clave = ''){
		$db = db_connect();
		$tabla = $db->table('configuraciones');
		$apis = $tabla->getWhere(['clave' => 'google'])->getRowArray();
		if(empty($apis)) return 'api_key'.$clave;
		
		$apis = json_decode($apis['valor'], true);
		return $apis[$clave];
	}
}
if (!function_exists('get_NetPay')) {
	function get_NetPay($clave = ""){
		$db = db_connect();
		$tabla = $db->table('configuraciones');
		$registro = $tabla->getWhere(['clave' => 'netpay'])->getRowArray();
		if(empty($registro)) return ' ';
		
		$apis = json_decode($registro['valor'], true);
		array_push($apis, $registro['id']);

		if(!empty($clave)) return $apis[0][$clave];
		return $apis;
	}
}
if (!function_exists('get_redSocial')) {
	function get_redSocial($redSocial){
		$db = db_connect();
		$tabla = $db->table('secciones');
		$registro = $tabla->getWhere(['seccion' => 'redes-sociales'])->getRowArray();
		if(empty($registro)) return 'red_social'.$redSocial;
		
		$apis = json_decode($registro['info'], true);
		return $apis[0][$redSocial];
	}
}
if (!function_exists('freelancer')) {
	function freelancer($view){
		if ($view) {
			$view_path = "backend/Freelancer/$view";
			return $view_path;
		}
	}
}
if (!function_exists('contratista')) {
	function contratista($view){
		if ($view) {
			$view_path = "backend/Contratista/$view";
			return $view_path;
		}
	}
}
if (!function_exists('administrador')) {
	function administrador($vista){
		if ($vista) {
			$view_path = "backend/administrador/$vista";
			return $view_path;
		}
	}
}
if (!function_exists('imagenPerfil')) {
	function imagenPerfil($nombre = '', $es_mensajeria = false){
		if($nombre == 'usuario-default.webp') return base_url()."/writable/uploads/Usuarios/".$nombre;
		if(!is_array($nombre)) return $nombre;
		if($nombre['imagen'] == 'usuario-default.webp') return base_url()."/writable/uploads/Usuarios/".$nombre['imagen'];
		if($es_mensajeria) return $nombre;
		
		return $nombre['imagen'];
	}
}
if (!function_exists('autorizacion')) {
	function autorizacion(){
		if(!session('logeado')) return redirect()->route('ingresar');
		return session('rol');
	}
}
if (!function_exists('get_recaptcha')) {
	function get_recaptcha($clave){
		$db = db_connect();
		$tabla = $db->table('configuraciones');
		$registro = $tabla->getWhere(['clave' => 'recaptcha'])->getRowArray();
		if(empty($registro)) return 'api_key'.$clave;
		
		$apis = json_decode($registro['valor'], true);
		return $apis[$clave];
	}
}
if (!function_exists('conversorZonaHoraria')) {
	function conversorZonaHoraria($fecha="", $toTz='',$fromTz='') { 
		$dia = new DateTime($fecha, new DateTimeZone($fromTz)); 
		$dia->setTimezone(new DateTimeZone($toTz)); 
		$fecha= $dia->format('Y-m-d H:i:s'); 
		return $fecha; 
	}
}
if (!function_exists('sanitizar')) {
	function sanitizar($input = ''){
		$trimmed_input = trim(preg_replace('/\s\s+/', ' ', $input));
		$type = gettype($trimmed_input);
		if ($type == "integer") {
			$sanitized_input = filter_var($trimmed_input, FILTER_SANITIZE_NUMBER_INT);
		} elseif ($type == "double") {
			$sanitized_input = filter_var($trimmed_input, FILTER_SANITIZE_NUMBER_FLOAT);
		} else { // LETS ASSUME IT IS A STRING
			// $sanitized_input = filter_var($trimmed_input, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_BACKTICK);
			$sanitized_input = htmlspecialchars($trimmed_input);
		}
		return $sanitized_input;
	}
}
if (!function_exists('vacio')) {
	function vacio($mensaje = 'false'){
		$mensaje = ($mensaje == 'false')? 'No se encontraron resultados' : $mensaje ;
		return view('/viewcells/Vacio', array('mensaje' => $mensaje));
	}
}

if (!function_exists('verificar_rangoFechas')) {
	function verificar_rangoFechas($fecha_inicio, $fecha_fin, $hoy) {
	   $fecha_inicio = strtotime($fecha_inicio);
	   $fecha_fin = strtotime($fecha_fin);
	   $hoy = strtotime($hoy);
	   
	   return ($hoy >= $fecha_inicio) && ($hoy < $fecha_fin);
	}
}