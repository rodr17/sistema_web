<?php namespace App\Libraries;

use App\Controllers\Suscripciones;
use App\Controllers\BaseController;

class AccesosPlanes extends BaseController{
	
	protected $id, $es_redireccionamiento, $tipo_acceso, $postulacionesUsuarios, $envio_solicitudes, $plan;
	public $accesos;

	public function __construct($idPlan, $id_usuario = 'false'){
		$id_usuario == 'false' ? $id_usuario = session('id') : '';
		$this->postulacionesUsuarios = model('PostulacionesUsuarios');
		$this->envio_solicitudes = model('Envio_solicitudes');
		$this->id = $idPlan;
		$this->plan = model('Suscripciones')->builder()
					->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
					->where('id_usuario', $id_usuario)
					->get()->getRowArray();
		if(empty($this->plan) || !Suscripciones::esta_suscrito_por_idUsuario($id_usuario)) $this->accesos = null;
		else $this->accesos = json_decode(model('ModelConfiguracion')->where('clave', "plan".$this->plan['id'])->first()['valor']);
	}
	
	public function validarFreelancer($tipo_acceso, $es_redireccionamiento = 'false'){
		if($tipo_acceso == 'valoraciontrabajos') return $this->accesos->$tipo_acceso;
		if($es_redireccionamiento == 'true'){
			if(empty($this->accesos) || !$this->accesos->$tipo_acceso) return redirect()->back()->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'Para poder ingresar al siguiente apartado requieres actualizar tu plan actual a uno de mayor nivel']);
		}
		else{
			if(empty($this->accesos) || !$this->accesos->$tipo_acceso) return json_encode(['alerta' => 'advertencia', 'mensaje' => 'Para poder ingresar al siguiente apartado requieres actualizar tu plan actual a uno de mayor nivel.']);
		}
	}

	public function validarContratista($tipo_acceso, $es_redireccionamiento = 'false'){
		if(empty($this->accesos)) return '';
		if($es_redireccionamiento == 'true'){
			if($tipo_acceso == 'valoraciontrabajos') return $this->accesos->$tipo_acceso;
			
			if(!$this->accesos->$tipo_acceso) return redirect()->back()->with('alerta', ['tipo' => 'advertencia', 'mensaje' => 'Para poder ingresar al siguiente apartado requieres actualizar tu plan actual a uno de mayor nivel.']);
			
			if($tipo_acceso == 'postuporsemana'){
				$postulaciones = $this->postulacionesUsuarios->where('id_usuario', session('id'))->first();
				if(empty($postulaciones)) $this->postulacionesUsuarios->save(['id_usuario' => session('id'), 'cantidad_actual' => 1]);
				else {
					if($postulaciones['cantidad_actual'] == $this->accesos->$tipo_acceso) return redirect()->back()->with('alerta', ['tipo' => 'modal', 'mensaje' => 'Has llegado al limite de postulaciones por semana, espera al finalizar la semana para poder seguirte postulando']);
					
					$this->postulacionesUsuarios->where('id_usuario', session('id'))->set('cantidad_actual', $postulaciones['cantidad_actual'] + 1)->update();
				}
			}
		}
		else{
			if($tipo_acceso == 'valoraciontrabajos') return $this->accesos->$tipo_acceso;
			
			if(!$this->accesos->$tipo_acceso) return json_encode(['alerta' => 'advertencia', 'mensaje' => 'Para poder ingresar al siguiente apartado requieres actualizar tu plan actual a uno de mayor nivel.']);

			if($tipo_acceso == 'postuporsemana'){
				$postulaciones = $this->postulacionesUsuarios->where('id_usuario', session('id'))->first();
				if(empty($postulaciones)) $this->postulacionesUsuarios->save(['id_usuario' => session('id'), 'cantidad_actual' => 1]);
				else {
					if($postulaciones['cantidad_actual'] == $this->accesos->$tipo_acceso) return json_encode(['alerta' => 'modal', 'mensaje' => 'Has llegado al limite de postulaciones por semana, espera al finalizar la semana para poder seguirte postulando'], JSON_FORCE_OBJECT);
					$this->postulacionesUsuarios->where('id_usuario', session('id'))->set('cantidad_actual', $postulaciones['cantidad_actual'] + 1)->update();
				}
			}
		}
	}

	public function getValor_permiso($permiso){
		if($this->accesos == null) return 0;
		return $this->accesos->$permiso;
	}
}