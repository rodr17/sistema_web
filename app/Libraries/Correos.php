<?php namespace App\Libraries;

class Correos{

	private $correos_admin = ['rvazquez@tresesenta.mx', 'jmaldonado@tresesenta.mx'], $correo, $solicitudes, $proyectos, $usuarios;
	
	public function __construct(){
		$this->correo = \Config\Services::email();
		$this->correo->setFrom('info@mextemps.com', 'Mextemps Freelancers');
		$this->correo->setCC('pagos@mextemps.com');
		
		$this->usuarios = model('Autenticacion');
		$this->solicitudes = model('Solicitudes');
		$this->proyectos = model('Proyectos');
	}
	
	public function enviar($usuario, $contraResetada){
		$this->correo->setTo($usuario['correo']);
		$this->correo->setSubject('Nueva Contraseña');
		$this->correo->setMessage(view('Correos/ResetarContrasena', array(
			'nombre' => $usuario['nombre'].' '.$usuario['apellidos'],
			'contrasena' => $contraResetada,
			'encabezado' => 'Nueva Contraseña'
		)));
		return $this->correo->send();
	}
	public function registroFreelance($usuario){
		$this->correo->setTo($usuario['correo']);
		$this->correo->setSubject('Bienvenido a Mextemps Freelancers');
		if($usuario['rol'] == 'freelancer'){
			$this->correo->setMessage(view('Correos/BienvenidoFreelance', array(
				'nombre' => $usuario['nombre'].' '.$usuario['apellidos']
			)));
		}else{
			$this->correo->setMessage(view('Correos/BienvenidoContratista', array(
				'nombre' => $usuario['nombre'].' '.$usuario['apellidos']
			)));
		}
		return $this->correo->send();
	}
	public function newAdmin($correo, $contra){
		$this->correo->setTo($correo);
		$this->correo->setSubject('Nuevo administrador');
		$this->correo->setMessage(view('Correos/NuevoAdmin', array(
			'correo' => $correo,
			'contra' => $contra
		)));
		$this->correo->send();
	}
	public function mensajeAdmin($correo, $mensaje, $enviador){
		$this->correo->setTo($correo);
		$this->correo->setSubject('Correo por parte de administración');
		$this->correo->setMessage(view('Correos/mensajeAdmin', array(
			'mensaje' => $mensaje,
			'enviador' => $enviador
		)));
		return $this->correo->send();
	}
	public function enviar_Contacto($correo, $mensaje, $usuario){
		$this->correo->setTo($this->correos_admin);
		$this->correo->setSubject('Correo por parte de '.$usuario['nombre']);
		$this->correo->setMessage(view('Correos/Contacto', array(
			'mensaje' => $mensaje,
			'usuario' => $usuario,
			'correo' => $correo
		)));
		return $this->correo->send();
	}
	public function solicitudProyecto($correo){
		$this->correo->setTo($correo);
		$this->correo->setSubject('Nueva solicitud de proyecto');
		$this->correo->setMessage(view('Correos/Solicitudes_directas'));
		return $this->correo->send();
	}
	public function declinarSolicitud($correo, $mensaje, $freelance, $solicitud){
		$freelancer = model('Autenticacion')->get_id($freelance);
		$soli = $this->solicitudes->where('id', $solicitud)->first();
		$proyecto = $this->proyectos->where('id', $soli['id_proyecto'])->first();
		$this->correo->setTo($correo);
		$this->correo->setSubject('Solicitud de proyecto declinada');
		$this->correo->setMessage(view('Correos/Solicitud_declinada', array('mensaje' => $mensaje, 'freelancer' => $freelancer, 'proyecto' => $proyecto)));
		return $this->correo->send();
	}
	public function solicitudAceptada($correo, $freelance, $proyecto){
		$this->correo->setTo($correo);
		$this->correo->setSubject('Solicitud de proyecto aceptada');
		$this->correo->setMessage(view('Correos/Solicitud_aceptada', array('freelance' => $freelance, 'proyecto' => $proyecto)));
		return $this->correo->send();
	}
	public function proyecto_aceptado($correo, $contratista, $proyecto){
		$this->correo->setTo($correo);
		$this->correo->setSubject('Proyecto asignado');
		$this->correo->setMessage(view('Correos/Proyecto_aceptado', array('contratista' => $contratista, 'proyecto' => $proyecto)));
		return $this->correo->send();
	}
	public function proyectoNoAsignado($correo, $proyecto){
		$this->correo->setTo($correo);
		$this->correo->setSubject('El proyecto ya ha sido asignado a otro Freelance');
		$this->correo->setMessage(view('Correos/Proyecto_no_asignado', array('proyecto' => $proyecto)));
		return $this->correo->send();
	}
	public function proyectoFinalizado($correo, $proyecto){
		$this->correo->setTo($correo);
		$this->correo->setSubject('Proyecto finalizado');
		$this->correo->setMessage(view('Correos/proyecto-finalizado', array('proyecto' => $proyecto)));
		return $this->correo->send();
	}
	public function notificarEstatusProyecto($correos, $mensaje, $proyecto){
		$this->correo->setTo($correos);
		$titulo = ($proyecto['deleted'] == 0) ? 'Proyecto temporalmente suspendido' : 'Proyecto Reactivado';
		$this->correo->setSubject($titulo);
		$this->correo->setMessage(view('Correos/Proyecto_estatus_admin', array('proyecto' => $proyecto, 'titulo' => $titulo, 'mensaje' => $mensaje)));
		return $this->correo->send();
	}
	public function ReportarFreelance($correo, $freelance, $contratista, $proyecto, $comentario){
		$this->correo->setTo($correo);
		$this->correo->setSubject('Freelancer Reportado');
		$this->correo->setMessage(view('Correos/ReporteFreelance', array('proyecto' => $proyecto, 'freelance' => $freelance, 'contratista' => $contratista, 'comentario' => $comentario)));
		return $this->correo->send();
	}
	public function enviar_Reporte($reporte){
		$this->correo->setTo($this->correos_admin);
		$this->correo->setSubject('Notificación nueva Soporte | '.$reporte['nombre']);
		
		if(empty($reporte['archivos'])) $this->correo->setMessage(view('Correos/Soporte', array('usuario' => $reporte)));
		else{
			$archivos = json_decode($reporte['archivos']);
			$this->correo->setMessage(view('Correos/Soporte', array('usuario' => $reporte, 'archivos' => $archivos)));
		}
		return $this->correo->send();
	}
	public function perfilAprobado($freelance){
		$this->correo->setTo($freelance['correo']);
		$this->correo->setSubject('Cuenta aprobada');
		$this->correo->setMessage(view('Correos/cuenta-aprobada'));
		return $this->correo->send();
	}
	public function cancelarSuscripcion($usuario, $suscripcion_cancelada){
		$this->correo->setTo($usuario['correo']);
		$this->correo->setSubject('Suscripción Cancelada '.$suscripcion_cancelada['suscripcion']);
		$this->correo->setMessage(view('Correos/SuscripcionCancelada', array(
			'usuario' => $usuario,
			'suscripcion' => $suscripcion_cancelada
		)));
	}
	public function perfilDeclinado($user, $mensaje){
		$this->correo->setTo($user['correo']);
		$this->correo->setSubject('Cuenta declinada');
		$this->correo->setMessage(view('Correos/cuenta-declinada', array('mensaje' => $mensaje)));
		return $this->correo->send();
	}
	public function respuestaSoporte($reporte, $mensaje){
		$this->correo->setTo($reporte['correo']);
		$this->correo->setSubject('Respuesta a solicitud de reporte');
		$this->correo->setMessage(view('Correos/respuesta-soporte', array('mensaje' => $mensaje, 'reporte' => $reporte)));
		return $this->correo->send();
	}
	//Correo llega a administrador
	public function enviarFacturacion($datos){
		$this->correo->setTo($this->correos_admin);
		$this->correo->setSubject('Solicitud de facturación');
		$this->correo->setMessage(view('Correos/solicitud_Facturacion', $datos));
		return $this->correo->send();
	}
	public function enviarFacturacionUsuarios($correo, $datos){
		$this->correo->setTo($correo);
		$this->correo->setSubject('Documentos facturación');
		$this->correo->attach(WRITEPATH."/uploads/Facturas/".$datos['facturas']['folio']."/".$datos['facturas']['archivo1']);
		$this->correo->attach(WRITEPATH."/uploads/Facturas/".$datos['facturas']['folio']."/".$datos['facturas']['archivo2']);
		$this->correo->setMessage(view('Correos/DocumentosFacturacion', $datos));
		return $this->correo->send();
	}
	public function enviar_recuperacionCuenta($usuario, $link){
		$this->correo->setTo($usuario['correo']);
		$usuario['link'] = $link;
		$this->correo->setSubject('Recuperacion Cuenta MEXTEMPS FREELANCE');
		$this->correo->setMessage(view('Correos/RecuperarCuenta', $usuario));
		return $this->correo->send();
	}
	public function soliFinalizarProyecto($proyecto, $freelancer, $contratante){
		$this->correo->setTo($contratante['correo']);
		$this->correo->setSubject('Solcitud para finalizar proyecto');
		$this->correo->setMessage(view('Correos/solicitud_proyecto_finalizado', array('freelancer' => $freelancer, 'proyecto' => $proyecto)));
		return $this->correo->send();
	}
	public function declinarSolicitudFin($proyecto, $freelancer){
		$this->correo->setTo($freelancer['correo']);
		$this->correo->setSubject('Su solicitud para finalizar el proyecto ha sido declinada');
		$this->correo->setMessage(view('Correos/solicitud_finalizar_declinada', array('proyecto' => $proyecto)));
		return $this->correo->send();
	}
	public function sugerenciaDeclinada($correo, $datos){
		$this->correo->setTo($correo);
		$this->correo->setSubject('Su solicitud de sugerencia ha sido declinada');
		$this->correo->setMessage(view('Correos/solicitud_sugerencia_declinada', array('datos' => json_decode($datos['contenido'], true), 'data' => $datos)));
		return $this->correo->send();
	}
	public function sugerenciaAnadida($correo, $datos){
		$this->correo->setTo($correo);
		$this->correo->setSubject('Su solicitud de sugerencia ha sido añadida');
		$this->correo->setMessage(view('Correos/solicitud_sugerencia_anadida', array('datos' => json_decode($datos['contenido'], true), 'data' => $datos)));
		return $this->correo->send();
	}
	public function sugerenciaYaExistente($correo, $datos){
		$this->correo->setTo($correo);
		$this->correo->setSubject('Su solicitud de sugerencia ha sido añadida');
		$this->correo->setMessage(view('Correos/solicitud_sugerencia_ya_existente', array('datos' => json_decode($datos['contenido'], true), 'data' => $datos)));
		return $this->correo->send();
	}
	public function solicitudSugerencia($id){
		$data = model('Sugerencias')->where('id', $id)->first();
		$datos = json_decode($data['contenido'], true);
		($data['tipo'] == 'habilidad')? $tipo = 'habilidad' : $tipo = 'idioma';
		$user = model('Autenticacion')->where('id', $data['id_usuario'])->first();
		
		$this->correo->setTo($this->correos_admin);
		$this->correo->setSubject('Nueva solicitud de sugerencia de '.$tipo);
		$this->correo->setMessage(view('Correos/solicitud-sugerencia', array('datos' => $datos, 'data' => $data, 'tipo' => $tipo, 'user' => $user)));
		return $this->correo->send();
	}
	public function alertaNuevoUsuario($usuario){
		$this->correo->setTo($this->correos_admin);
		$this->correo->setSubject('Nuevo usuario registrado');
		$this->correo->setMessage(view('Correos/alerta-nuevo-usuario', array('usuario' => $usuario)));
		return $this->correo->send();
	}
	public function nuevoPago($id){
		$pago = model('Pagos')->where('id_compra', $id)->first();
		$usuario = $this->usuarios->where('id', $pago['id_usuario'])->first();
		
		// SE OBTIENE RECURRENCIA MENSUAL O ANUAL BBDD
		$planBD = model('ModelPlanes')->where('id_plan_mensual', $pago['id_membresia'])->first();
		$recurrencia = isset($planBD['monto']) ? 'mensual': '';
		
		if(empty($planBD)){
			$planBD = model('ModelPlanes')->where('id_plan_anual', $pago['id_membresia'])->first();
			$recurrencia = 'anual';
		}
		
		$this->correo->setTo($this->correos_admin);
		$this->correo->setSubject('Nuevo pago realizado');
		$this->correo->setMessage(view('Correos/alerta-nuevo-pago', array('usuario' => $usuario, 'pago' => $pago, 'tipo' => $recurrencia, 'plan' => $planBD, 'metodo' => $pago['tipo'])));
		return $this->correo->send();
	}
	public function eliminacionCuenta($id){
		$user = $this->usuarios->get_id($id);
		$this->correo->setTo($this->correos_admin);
		$this->correo->setSubject('Un usuario ha solicitado eliminar su cuenta');
		$this->correo->setMessage(view('Correos/usuario-cuenta-eliminada', array('usuario' => $user)));
		return $this->correo->send();
	}
	public function usuarioEliminadoAdmin($correo){
		$this->correo->setTo($correo);
		$this->correo->setSubject('Su cuenta ha sido eliminada por el area de administración.');
		$this->correo->setMessage(view('Correos/eliminado-por-administracion'));
		return $this->correo->send();
	}
	public function suscripcionVencida(string $suscripcion, array $user, string $plan){
		$this->correo->setTo($user['correo']);
		$this->correo->setSubject('Su suscripción ha vencido.');
		$this->correo->setMessage(view('Correos/suscripcion-vencida', array('usuario' => $user, 'suscripcion' => $suscripcion, 'plan' => $plan)));
		return $this->correo->send();
	}
    public function renovacionFallida(string $fecha, array $user, array $plan, string $mensaje){
		$this->correo->setTo($user['correo']);
		$this->correo->setSubject('Renovación fallida.');
		$this->correo->setMessage(view('Correos/suscripcion-fallida', array('usuario' => $user, 'fecha_pago' => $fecha, 'plan' => $plan, 'mensaje' => $mensaje)));
		return $this->correo->send();
	}

	public function nuevaReferenciaPago(array $datos_a_pagar){
		$this->correo->setTo($datos_a_pagar['billing']['email']);
		$this->correo->setSubject('Referencia de pago en efectivo.');
		$this->correo->setMessage(view('Correos/nueva-referencia-pago', $datos_a_pagar));
		return $this->correo->send();
	}
}