<?php namespace App\Libraries;

use App\Controllers\Suscripciones;
class FuncionesAdmin {
    private $model, $usuarios, $secciones, $habilidades, $calificaciones, $sugerencias, $idiomas, $soporte;
    
    public function __construct(){
        $this->model = model('ModelAdministradores');
        $this->usuarios = model('Autenticacion');
        $this->secciones = model('ModelSecciones');
        $this->habilidades = model('Habilidades');
        $this->calificaciones = model('Calificaciones');
        $this->sugerencias = model('Sugerencias');
        $this->idiomas = model('Idiomas');
        $this->soporte = model('Soporte');
        helper('funciones');
        helper('text');
    }
    // Funcion para mostrar total de usuario
    public function totalUsers(){
        $data = $this->usuarios->findAll();
        if($data == null) return 0;
        return count($data);
    }
    // Funcion para mostrar total de freelancers
    public function totalFreelancers(){
        $data = $this->usuarios->where(['rol' => 'freelancer'])->find();
        if($data == null) return 0;
        return count($data);
    }
    // Funcion para mostrar total de contratistas
    public function totalContratista(){
        $data = $this->usuarios->where(['rol' => 'contratista'])->find();
        if($data == null) return 0;
        return count($data);
    }
    // Funcion para mostrar cuantos usuarios se han registrado de diferente tipo
    public function tipoCuentaUsers($cuenta){
        $data = $this->usuarios->where(['cuenta' => $cuenta])->find();
        if($data == null) return 0;
        return count($data);
    }
    public function dataAdmin($id, $campo){
        $data = $this->model->where('id', $id)->first();
        if($data == null) return ' ';
        return $data[$campo];
    }
    // Funcion para mostrar foto de usuario
    public function imagenAdmin($id){
        $data = $this->model->where('id', $id)->first();
        if($data == null) return base_url('/assets/images/Backend/user-default.jpg');
        if($data['imagen'] == '') return base_url('/assets/images/Backend/user-default.jpg');
        return base_url($data['imagen']);
    }
    // Funcion para humanizar fechas
    public function humanizarPieza($fecha){
        if(empty($fecha)) return '';
        setlocale(LC_TIME, 'es_ES');
        return ucfirst(utf8_encode(strftime('%A, %e de %B de %Y', strtotime($fecha))));
    }
    // Funcion para humanizar fechas
    public function humanizarFechaHora($fecha){
        if(empty($fecha)) return '';
        setlocale(LC_TIME, 'es_ES');
        return ucfirst(utf8_encode(strftime('%A, %e de %B de %Y', strtotime($fecha)))).', '.date('h:i A', strtotime($fecha));
    }
    // Funcion para mostrar card de trabajo en administrador
    public function cardTrabajoAdministracion($proyecto, $contratista){
        return view('/backend/administrador/viewcells/card-trabajo-administracion', array('p' => $proyecto, 'contratista' => $contratista));
    }
    // Funcion para mostrar buscador o no en side administrador
    public function verBuscadorSide($id){
        $user = $this->model->where('id', $id)->first();
        if($user['tipo'] != 'superadmin') return false;
        return true;
    }
    // Funcion para mostrar seccion de side de admin
    public function verSeccionSide($id, $seccion){
        $user = $this->model->where('id', $id)->first();
        if($user['tipo'] == 'superadmin') return true;

        $permisos = json_decode($user['permisos'], true);
        if(empty($permisos)) return false;

        foreach($permisos as $p){
            if($p['sec'] == $seccion) return $p['valor'];
        }
    }
    // Funcion para mostrar permisos por tipo de administrador
    public function verPermisos($id){
        $admin = $this->model->where('id', $id)->first();
        if($admin == null) return '';
        return view('/backend/administrador/viewcells/permisos-admin', array('id' => $id));
    }
    // Funcion para ver solicitudes de aprovacion
    public function verSolicitudesAprobacion(){
        $solicitudes = $this->usuarios->where('aprobado', false)->orWhere('aprobado', null)->orderBy('id', 'DESC')->find();
        return view('/backend/administrador/viewcells/card-freelance-backend', array('freelances' => $solicitudes));
    }
    // Funcion para mostrar reportes de soporte
    public function reportesSoportes(){
        $reportes = $this->soporte->orderBy('id', 'DESC')->find();
        if($reportes == null) return '<div class="text-center fw-bold">Sin reportes aun.</div>';
        return view('backend/administrador/viewcells/reportes-soporte', array('reportes' => $reportes));
    }
    // Funcion para mostrar slides ya creados
    public function mostrarSlides(){
        $slides = $this->secciones->where('tipo', 'slide')->find();
        if($slides == null) return '<div class="text-center">Aun no ha creado ningun slider</div>';
        return view('backend/administrador/viewcells/sliders', array('slides' => $slides));
    }
    // Funcion para mostrar datos de secciones
    public function mostrarValorSeccion($seccion, $tipo, $valor){
        $data = $this->secciones->where(['seccion' => $seccion, 'tipo' => $tipo])->first();
        if($data == null) return ' ';
        $info = json_decode($data['info'], true);
        if(empty($info[$valor])) return ' ';
        return $info[$valor];
    }
    // Funcion para ver preguntas frecuentes de paquetes en administrador para editar
    public function viewPreguntaPaquetes(){
        $preguntas = model('ModelSecciones')->where(['seccion' => 'paquetes', 'tipo' => 'pregunta'])->find();
        if($preguntas == null) return '<div class="text-center">Aun no se han creado preguntas</div>';
        return view('viewcells/preguntas-paquestes', array('preguntas' => $preguntas));
    }
    // Funcion para ver la cantidad de sub areas
    public function cantidadSubAreas($area){
        $data = $this->habilidades->where('superior', $area)->find();
        if($data == null) return 0;
        return count($data);
    }
    // Funcion para ver reportes de freelances
    public function verReportesFreelance(){
        $reportes = $this->calificaciones->where('tipo', 'reporte')->orderBy('id', 'DESC')->find();
        if($reportes == null) return '<div class="text-center fw-bold">Sin reportes aun.</div>';
        return view('/backend/administrador/viewcells/reportes-freelance', array('reportes' => $reportes));
    }
    // Funcion para ver cantidad de solicitudes de idiomas
    public function cantSoliIdiomas(){
        $cant = $this->sugerencias->where(['tipo' => 'idioma', 'estatus' => 'espera'])->find();
        if($cant == null) return 0;
        return count($cant);
    }
    // Funcion para ver cantidad de solicitudes de areas o habilidades
    public function cantSoliHabilidades(){
        $cant = $this->sugerencias->where(['tipo' => 'habilidad', 'estatus' => 'espera'])->find();
        if($cant == null) return 0;
        return count($cant);
    }
    // Funcion para validar idioma de solicitud
    public function validaridioma($idioma){
        $data = $this->idiomas->where('identificador', $idioma)->first();
        if($data != null) return false;
        return true;
    }
    // Funcion para validar si existe o no archivos para vistas
    public function existeONoArchivo($dir,&$archivo_buscar){   // Funcion Recursiva
        if ( is_dir($dir) ){
            // Recorremos Directorio
            $d=opendir($dir); 
            while( $archivo = readdir($d) ){
                if ( $archivo!="." AND $archivo!=".."  ){
                    if ( is_file($dir.'/'.$archivo) ){
                        // Es Archivo
                        if ( $archivo == $archivo_buscar  ){
                            return ($dir.'/'.$archivo);
                        }
                    }
                    if ( is_dir($dir.'/'.$archivo) ){
                        // Es Directorio
                        // Volvemos a llamar
                        $r = $this->existeONoArchivo($dir.'/'.$archivo,$archivo_buscar);
                        if ( basename($r) == $archivo_buscar ){
                            return $r;
                        } 
                    }
                }
            }
        }
        return FALSE;
    }
    // Funcion para ver dato de base de datos
    public function datoSeccionDBAdmin($seccion, $tipo, $valor){
        $data = model('ModelSecciones')->where(['seccion' => $seccion, 'tipo' => $tipo])->first();
        if($data == null) return ' ';
        $datos = json_decode($data['info'], true);
        isset($datos[$valor])? $a = $datos[$valor] : $a = ' ';
        return $a;
    }
    // Funcion para ver tarjeta con la que se pago una compra
    public function verTarjetaCompra($tipo, $id, $user){
        return '';
        // if($tipo != 'tarjeta') return ' ';
        // $info = model('Autenticacion')->get_cargoID($id, $user);
        // // dd($info);
        // return '<label>Tarjeta con terminacion: <strong>'.$info['card']['lastFourDigits'].' ('.$info['card']['bank_name'].') </strong></label>';
    }

    public function listadoUsuarios(array $usuario){
        $con_suscripcion = Suscripciones::esta_suscrito_por_idUsuario($usuario['id']);
        $suscripcion_activa = model('Suscripciones')->where(['id_usuario' =>  $usuario['id'], 'estatus' => 'Activo'])->first();
        
		return view(administrador('viewcells/card-listado_usuarios'), ['usuario' => $usuario, 'suscripcion_activa' => $con_suscripcion && !empty($suscripcion_activa), 'suscripcion' => $suscripcion_activa]);
	}
}