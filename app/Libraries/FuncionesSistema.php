<?php namespace App\Libraries;

use App\Libraries\AccesosPlanes;
use CodeIgniter\I18n\Time;
use Coduo\PHPHumanizer\DateTimeHumanizer;
use App\Controllers\Suscripciones;

require COMPOSER_PATH;
use SMTPValidateEmail\Validator as SmtpEmailValidator;

class FuncionesSistema {
	// Funcion para retornar el valor de un medio de contacto seleccionado
	public function medioContacto(string $medio){
		$model = model('ModelSecciones');
		$data = $model->where(['seccion' => 'medio-contacto', 'tipo' => $medio])->first();
		if($data == null){
			return ' ';
		}else{
			$info = json_decode($data['info'], true);
			return $info['liga'];
		}
	}
	// Funcion para retornar el valor necesario para la configuracion de un registro de configuracion
	public function configValor(string $clave, string $valor){
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', $clave)->first();
		if($data == null){
			return ' ';
		}else{
			$datos = json_decode($data['valor'], true);
			return $datos[$valor];
		}
	}
	// Funcion para validar modo de openpay
	public function verModoOpenpay(string $valor){
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', 'modoopen')->first();
		if($data == null){
			if($valor == 0){
				return 'checked';
			}else{
				return ' ';
			}
		}else{
			$val = json_decode($data['valor'], true);
			if($val['modo'] == $valor){
				return 'checked';
			}else{
				return ' ';
			}
		}
	} 
	// Funcion para checked de parte de membresias
	public function verificarPreferencia(string $plan, string $valor){
		$clave = 'plan'.$plan;
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', $clave)->first();
		if($data == null){
			return ' ';
		}else{
			$val = json_decode($data['valor'], true);
			if($val[$valor] == true){
				return 'checked';
			}else{
				return ' ';
			}
		}
	}
	// Funcion para mostrar valor ya en tabla
	public function verValorTabla(string $plan, string $valor){
		$clave = 'plan'.$plan;
		$data = model('ModelConfiguracion')->where('clave', $clave)->first();
		if($data == null) return '<span class="tabla-tarifas-danger"><i class="fa fa-times-circle" aria-hidden="true"></i></span>';
		
		$val = json_decode($data['valor'], true);
		if($val[$valor] == false) return '<span class="tabla-tarifas-danger"><i class="fa fa-times-circle" aria-hidden="true"></i></span>';
		return '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>';
	}
	// Funcion para ver cantidad de postulaciones por semana
	public function postuSemana(string $plan){
		$clave = 'plan'.$plan;
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', $clave)->first();
		if($data == null){
			return '5';
		}else{
			$val = json_decode($data['valor'], true);
			if(empty($val['postuporsemana'])) return 0;
			return $val['postuporsemana'];
		}
	}
	// Funcion para ver cantidad de envio de solicitudes por mes
	public function enviosolicitudes(string $plan){
		$clave = 'plan'.$plan;
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', $clave)->first();
		if($data == null){
			return '5';
		}else{
			$val = json_decode($data['valor'], true);
			return $val['enviosolicitudes'];
		}
	}
	// Funcion para cantidad de poryectos activos
	public function proyectosActivos($plan){
		$clave = 'plan'.$plan;
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', $clave)->first();
		if($data == null){
			return '5';
		}else{
			$val = json_decode($data['valor'], true);
			return $val['proyectosActivos'];
		}
	}
	// Funcion para cantidad de historial de conversaciones
	public function historialconversaciones($plan){
		$clave = 'plan'.$plan;
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', $clave)->first();
		if($data == null){
			return '5';
		}else{
			$val = json_decode($data['valor'], true);
			return $val['historialconversaciones'];
		}
	}
	// Funcion para cantidad de proyectos destacados
	public function proyectosDestacados($plan){
		$clave = 'plan'.$plan;
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', $clave)->first();
		if($data == null){
			return '5';
		}else{
			$val = json_decode($data['valor'], true);
			return $val['proyectosDestacados'];
		}
	}
	//
	public function accesoNivelFreelancer($plan, $valor){
		$clave = 'plan'.$plan;
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', $clave)->first();
		if(empty($data)) return ' ';
		
		$valores = json_decode($data['valor'], true);
		$accesosFreelance = $valores['accesofreelancernivel'];
		if(empty($accesosFreelance)) return ' ';
		
		$x = 0;
		foreach($accesosFreelance as $d){
			if($d == $valor) $x = 1;
		}
		if($x == 0) return ' ';
		return 'checked';
	}
	//
	public function accesoNivelFreelancerVista($plan){
		$clave = 'plan'.$plan;
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', $clave)->first();
		if(empty($data)) return ' ';
		
		$valores = json_decode($data['valor'], true);
		$accesosFreelance = json_decode($valores['accesofreelancernivel'], true);
		if(empty($accesosFreelance)) return ' ';
		
		$x = '';
		if($accesosFreelance == null) return ' ';
		foreach($accesosFreelance as $key => $d){
			if($key == 0){
				$x .= $d;
			}else{
				$x .= '<hr>'.$d;
			}
		}
		return $x;
	}
	// Funcion para ver cantidad optunidades para promocionar trabajos durante el mes
	// public function promocionartrabajos(string $plan){
	//     $clave = 'plan'.$plan;
	//     $model = model('ModelConfiguracion');
	//     $data = $model->where('clave', $clave)->first();
	//     if($data == null){
	//         return '2';
	//     }else{
	//         $val = json_decode($data['valor'], true);
	//         return $val['promocionartrabajos'];
	//     }
	// }
	// Funcion para ver la cantidad de trabajos para el historial
	public function historialtrabajos(string $plan){
		$clave = 'plan'.$plan;
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', $clave)->first();
		if($data == null){
			return '2';
		}else{
			$val = json_decode($data['valor'], true);
			return $val['historialtrabajos'];
		}
	}
	// Funcion para ver preferencia del buscador del tipo de plan
	public function buscadorfreelancer(string $plan, string $valor){
		$clave = 'plan'.$plan;
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', $clave)->first();
		if($data == null){
			return ' ';
		}else{
			$val = json_decode($data['valor'], true);
			if($val['buscadorfreelancer'] == $valor){
				return 'checked';
			}else{
				return ' ';
			}
		}
	}
	public function nivelsoporteVista(string $plan){
		$clave = 'plan'.$plan;
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', $clave)->first();
		if($data == null){
			return ' ';
		}else{
			$val = json_decode($data['valor'], true);
			if(!empty($val['nivelsoporte']) ) return $val['nivelsoporte'];
			return '+ 48h';
		}
	}
	public function nivelsoporte(string $plan, string $valor){
		$clave = 'plan'.$plan;
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', $clave)->first();
		if($data == null){
			return ' ';
		}else{
			$val = json_decode($data['valor'], true);
			if($val['nivelsoporte'] == $valor){
				return 'checked';
			}else{
				return ' ';
			}
		}
	}
	// Funcion para ver cantidad de publicaciones de trabajos al mes 
	// public function publicartrabajos(string $plan){
	//     $clave = 'plan'.$plan;
	//     $model = model('ModelConfiguracion');
	//     $data = $model->where('clave', $clave)->first();
	//     if($data == null){
	//         return '5';
	//     }else{
	//         $val = json_decode($data['valor'], true);
	//         return $val['proyectosActivos'];
	//     }
	// }
	// Funcion para ver cantidad de alguna seccion del plan en tablas
	public function valorNumPlan(string $plan, string $valor){
		$clave = 'plan'.$plan;
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', $clave)->first();
		if($data == null) return '0';
		$val = json_decode($data['valor'], true);
		if(empty($val[$valor])) return '0';
		return $val[$valor];
	}
	// Funcion para iconos en seccion de paquetes
	public function checkedPaquetes(string $plan, string $valor){
		$clave = 'plan'.$plan;
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', $clave)->first();
		if($data == null) return 'unchecked';
		
		$val = json_decode($data['valor'], true);
		if(empty($val[$valor])) return 'unchecked';
		if($val[$valor] == true) return 'checked';
		
		return 'unchecked';
	}
	public function valorBneficio(string $plan, string $valor){
		$clave = 'plan'.$plan;
		$model = model('ModelConfiguracion');
		$data = $model->where('clave', $clave)->first();
		if($data == null){
			return ' ';
		}else{
			$val = json_decode($data['valor'], true);
			if($val[$valor] == null || $val[$valor] == ''){
				return ' ';
			}else{
				return $val[$valor];
			}
		}
	}
	public function carruselmembresias(){
		if(session('logeado') != true){
			return '';
		}else{
			$model = model('ModelPlanes');
			$mem = $model->where('cuenta', session('rol'))->find();
			if(session('rol') == 'contratante') return view('/backend/administrador/viewcells/membresias', array('membresias' => $mem));
			if(session('rol') == 'freelancer') return view('/backend/administrador/viewcells/membresias-freelancer', array('membresias' => $mem));
		}
	}
	// Funcion para mostrar todos mis trabajos
	public function misTrabajos(){
		$paginacion = 10;
		$usuario = model('Autenticacion')->get_UsuarioID(session('id'));
		$permisos = new AccesosPlanes($usuario['plan']);
		$proyectos = model('Proyectos')->where(['id_contratista' => session('id')])->orderBy('id', 'DESC')->countAllResults();
		if($proyectos == 0){
			$vista = '<div class="alert alert-info alert-dismissible mb-4" role="alert">Aún no has creado ningún proyecto, permítenos guiarte para crear tu primer proyecto.</div>';
			$vista .= '<div class="text-center"><a class="btn btn-default" href="'.base_url('/proyecto').'">Agregar proyecto</a></div>';
			return $vista;
		}

		if($permisos->getValor_permiso('historialtrabajos') == 0){
			$cant_trabajos_finalizado = model('Proyectos')->where('id_contratista', session('id'))
			->whereIn('estatus', ['finalizado', 'cancelado'])
			->countAllResults();
			$vista = "<div class='alert alert-warning alert-dismissible mb-4' role='alert'>Cuenta con <b class='fw-bold'>".$cant_trabajos_finalizado."</b> trabajos por mostrar, adquiere una suscripción <b class='fw-bold'>PREMIUM</b> para obtener todo tu historial de trabajos ilimitado</b><button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>";
			// return $vista.="<div class='alert alert-info alert-dismissible mb-4' role='alert'>Sin resultados</div><script>$('#cantidad_proyectos').text(0);</script>";
		}
		
		$cantidad_trabajos = model('Proyectos')->where(['id_contratista' => session('id')])->countAllResults();
		$trabajos = model('Proyectos')->where(['id_contratista' => session('id')])->orderby('id', 'DESC')->limit($paginacion)->find();
		if($cantidad_trabajos == 0) return "<div class='alert alert-info alert-dismissible mb-4' role='alert'>Sin trabajos publicados</div>";
		
		$pages = ceil($cantidad_trabajos / $paginacion);
		return view('/backend/Contratista/viewcells/trabajos', array('trabajos' => $trabajos, 'pages' => $pages));
	}
	public function cardTrabajo(array $trabajo){
		if($trabajo['estatus'] == 'espera'){
			return view('/backend/Contratista/viewcells/card-trabajo-espera', array('trabajo' => $trabajo));
		}elseif($trabajo['estatus'] == 'desarrollo'){
			return view('/backend/Contratista/viewcells/card-trabajo-desarrollo', array('trabajo' => $trabajo));
		}elseif($trabajo['estatus'] == 'finalizado'){
			return view('/backend/Contratista/viewcells/card-trabajo-finalizado', array('trabajo' => $trabajo));
		}elseif($trabajo['estatus'] == 'cancelado'){
			return view('/backend/Contratista/viewcells/card-trabajo-cancelado', array('trabajo' => $trabajo));
		}
	}
	//Funcion para ver comentario de calificacion o reporte
	public function verReporte($id){
		$model = model('Proyectos');
		$calificaciones = model('Calificaciones');
		$proyecto = $model->where('id', $id)->first();
		$reporte = $calificaciones->where(['id_freelancer' => $proyecto['id_freelancer'], 'id_proyecto' => $id])->first();
		if($reporte == null) return '';
		return $reporte['comentario'];
	}
	//Funcion para ver areas
	public function verAreas(){
		$db      = \Config\Database::connect();
		$builder = $db->query('SELECT * FROM habilidades GROUP BY area ORDER BY id ASC');
		$areas = $builder->getResultArray();
		return view('/backend/Contratista/viewcells/areas', array('areas' => $areas));
	}
	// Funcion para llamar el nombre del usuario
	public function nombreUser($id){
		$model = model('Autenticacion');
		$user = $model->where('id', $id)->first();
		if($user == null){
			return 'Verifique sus datos';
		}else{
			return $user['nombre'].' '.$user['apellidos'];
		}
	}
	// Funcion para validar si ya se postulo un freelance a un proyecto
	public function validarPostulacion($idproyecto, $idusuario){
		$model = model('Postulaciones');
		$data = $model->where(['id_proyecto' => $idproyecto, 'id_postulante' => $idusuario])->first();
		return view('/viewcells/postular', array('data' => $data, 'id_proyecto' => $idproyecto, 'titulo' => model('Proyectos')->find($idproyecto)['titulo']));
	}
	// Funcion para ver chat
	public function mostrarChat($idproyecto, $id){
		$model = model('Postulaciones');
		$data = $model->where(['id_proyecto' => $idproyecto, 'id_postulante' => $id])->first();
		return view('/viewcells/chat', array('data' => $data));
	}
	// Funcion para ver chat en card de proyecto
	public function mostrarChatCard($idproyecto, $id){
		$model = model('Postulaciones');
		$data = $model->where(['id_proyecto' => $idproyecto, 'id_postulante' => $id])->first();
		return view('/viewcells/chat-card-trabajo', array('data' => $data));
	}
	// Funcion para ver cantidad de postulantes a proyecto
	public function verPostulantes($id){
		$model = model('Postulaciones');
		$name = 'modal'.$id.'post';
		$data = $model->where('id_proyecto', $id)->find();

		$monto = model('ModelPlanes')->selectMax('monto')->where('cuenta', 'freelancer')->get()->getRowArray()['monto'];
		foreach($data as $key => $freelancer){
		    $data[$key]['premium'] = false;
		    $con_suscripcion_premium = (model('Suscripciones')
				->builder()
				->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
				->where(['id_usuario' => $freelancer['id'], 'estatus' => 'Activo', 'monto' => $monto])
				->get()->getRowArray());
				 
			 if(!empty($con_suscripcion_premium)) $data[$key]['premium'] = true;
			 $aux[$key] = $data[$key]['premium'];
		}
		if(!empty($data)) array_multisort($aux, SORT_DESC, $data);
		return view('/backend/Contratista/viewcells/cantidad-postulantes', array('data' => $data, 'modal' => $name));
	}
	// Funcion para cambiar entre chats de postulantes
	public function varChatsPostulantes($proyecto, $actual){
		$data = model('Postulaciones')->where(['id_proyecto' => $proyecto, 'id_postulante !=' => $actual])->find();
		// $data = model('Postulaciones')->where('id_proyecto', $proyecto)->find();
		return view('/backend/Contratista/viewcells/chats-postulantes', array('data' => $data));
	}
	// Funcion de vista de card de postulante
	public function cardPostulantes($id, $postu, $tipo, $modal, $es_premium){
		$user =  model('Autenticacion')->get_id($id);
		$user['es_premium'] = $es_premium;
		if(!empty($user)) return view('/backend/Contratista/viewcells/postulante', array('tipo' => $tipo, 'user' => $user, 'post' => $postu, 'modal' => $modal));
		
		return 'Freelance no encontrado';        
	}
	// Funcion para mostrar chat de proyecto
	public function chatProyecto($proyecto, $freelance){
		$postu = model('Postulaciones')->where(['id_proyecto' => $proyecto, 'id_postulante' => $freelance])->first();
		if(empty($postu)) return ' ';
		return view('/backend/viewcells/boton-chat', array('postu' => $postu));
	}
	// Funcion para mostrar chat de proyecto dentro del proyecto
	public function chatProyectoInterior($proyecto, $freelance){
		$postulaciones = model('Postulaciones');
		$model = model('Salas');
		$postu = $postulaciones->where(['id_proyecto' => $proyecto, 'id_postulante' => $freelance])->first();
		if(empty($postu)) return ' ';
		return view('/backend/viewcells/boton-chat-interior', array('postu' => $postu));
	}
	// Funcion para validar si existe sala de chat o no
	public function validarChat($postulacion){
		$postulaciones = model('Postulaciones');
		$model = model('Salas');
		$postu = $postulaciones->where('id', $postulacion)->first();
		if($postu == null){
			return ' ';
		}else{
			$sala = $model->where(['id_contratista' => session('id'), 'id_freelancer' => $postu['id_postulante'], 'id_proyecto' => $postu['id_proyecto']])->first();
			if($sala == null){
				return view('/backend/Contratista/viewcells/boton-chat', array('tipo' => 0, 'post' => $postulacion));
			}else{
				return view('/backend/Contratista/viewcells/boton-chat', array('tipo' => 1, 'post' => $postulacion));
			}
		}
	}
	// Funcion para ver cards de acceso rapido a postulaciones
	public function misPostulaciones($id){
		$select = [];
		$builder = db_connect()->table('postulaciones');
		$proyectos = $builder->select($select)->where(['postulaciones.id_postulante' => $id, 'proyectos.estatus' => 'espera'])->join('proyectos', 'postulaciones.id_proyecto = proyectos.id')->get();
		return view('/backend/Freelancer/viewcells/mis-postulaciones', array('postulaciones' => $proyectos->getResultArray()));
	}
	// Funcion para ver cantidad de postulaciones activas
	public function cantPostulaciones($id, $rol){
		if($rol == 'freelancer'){
			$BD = db_connect();
			$builder = $BD->table('postulaciones');
			$builder->where(['postulaciones.id_postulante' => $id, 'proyectos.estatus' => 'espera'])->join('proyectos', 'postulaciones.id_proyecto = proyectos.id');
			$proyectos = $builder->get()->getResultArray();
			if($proyectos == null) return 0;
			return count($proyectos);
		}else{
			$data = model('Proyectos')->where(['id_contratista' => $id, 'estatus' => 'espera'])->find();
			if($data == null) return 0;
			return count($data);
		}
	}
	// Funcion para ver cards de acceso rapido a proyectos en desarrollo
	public function misProyectosDesarrollo($id){
		$select = [];
		$BD = db_connect();
		$builder = $BD->table('postulaciones');
		$builder->where(['postulaciones.id_postulante' => $id, 'proyectos.estatus' => 'desarrollo'])->join('proyectos', 'postulaciones.id_proyecto = proyectos.id');
		$proyectos = $builder->get();
		$data = $proyectos->getResultArray();
		return view('/backend/Freelancer/viewcells/mi-desarrollos', array('desarrollos' => $data));
	}
	// Funcion para ver cantidad de proyectos en desarrollo
	public function cantProyectosDesarrollo($id, $rol){
		if($rol == 'freelancer'){
			// $BD = db_connect();
			// $builder = $BD->table('postulaciones');
			// $builder->where(['postulaciones.id_postulante' => $id, 'proyectos.estatus' => 'desarrollo'])->join('proyectos', 'postulaciones.id_proyecto = proyectos.id')->limit(6);
			// $proyectos = $builder->get()->getResultArray();
			$proyectos = model('Proyectos')->where(['id_freelancer' => session('id'), 'estatus' => 'desarrollo'])->limit(6)->find();
			if($proyectos == null) return 0;
			return count($proyectos);
		}else{
			$data = model('Proyectos')->where(['id_contratista' => $id, 'estatus' => 'desarrollo'])->find();
			if($data == null) return 0;
			return count($data);
		}
	}
	// Funcion para mostrar proyectos finalizados por freelance
	public function misProyectosFinalizados($id){
		$model = model('Proyectos');
		$data = $model->where(['id_freelancer' => $id, 'estatus' => 'finalizado'])->findAll(6, 0);
		return view('/backend/Freelancer/viewcells/proyectos-finalizados', array('proyectos' => $data));
	}
	// Funcion para ver cantidad de proyectos finalizados
	public function cantProyectosFinalizados($id, $rol){
		if($rol == 'freelancer'){
			$data = model('Proyectos')->where(['id_freelancer' => $id, 'estatus' => 'finalizado'])->findAll();
			if($data == null) return 0;
			return count($data);
		}else{
			$data = model('Proyectos')->where(['id_contratista' => $id, 'estatus' => 'finalizado'])->find();
			if($data == null) return 0;
			return count($data);
		}
	}
	
	public function proyectosRestantesInsignia($id_usuario){
		$cantidad = $this->cantProyectosFinalizados($id_usuario, 'freelancer');
		if($cantidad == 0) $restante = '1 proyecto';
		
		if($cantidad >= 1 && $cantidad <= 15) $restante = 16 - $cantidad.' proyectos';
		elseif($cantidad >= 16 && $cantidad <= 30) $restante = 31 - $cantidad.' proyectos';
		elseif($cantidad >= 31 && $cantidad <= 45) $restante = 46 - $cantidad.' proyectos';
		elseif($cantidad >= 46 && $cantidad <= 60) $restante = 61 - $cantidad.' proyectos';
		elseif($cantidad >= 61 && $cantidad <= 99) $restante = 100 - $cantidad.' proyectos';
		elseif($cantidad >= 100) return '<label class="text-font">Nivel máximo</label>';
		
		return "<label class='text-font'>Te faltan <b class='fw-bold'>$restante</b> para subir de nivel</label>";
	}
	// Funcion de carrusel interior de cada proyecto
	public function carruselProyecto($id){
		$model = model('Proyectos');
		$usuarios = model('Autenticacion');
		$proyecto = $model->where('id', $id)->first();
		if($proyecto == null){
			return ' ';
		}else{
			$imagenes = json_decode($proyecto['imagenes'], true);
			
			if($imagenes == null && $imagenes == ''){
				return view('/backend/Contratista/viewcells/carrusel-defecto');
			}else{
				$user = $usuarios->get_id($proyecto['id_contratista']);
				if($user == null) return view('/backend/Contratista/viewcells/carrusel-defecto');
				$usuario = $user['id'].'_'.$user['nombre'].' '.$user['apellidos'];
				$ruta = "/writable/uploads/Usuarios/".$usuario."/Proyectos/".$proyecto['titulo'];
				// $ruta = $directorio."\\".$proyecto['titulo'];
				return view('/backend/Contratista/viewcells/carrusel', array('imagenes' => $imagenes, 'ruta' => $ruta));
			}
		}
	}
	public function carruselProyectEdit($id){
		$model = model('Proyectos');
		$usuarios = model('Autenticacion');
		$proyecto = $model->where('id', $id)->first();
		if($proyecto == null){
			return ' ';
		}else{
			$imagenes = json_decode($proyecto['imagenes'], true);
			
			if($imagenes == null && $imagenes == ''){
				return view('/backend/Contratista/viewcells/carrusel-defecto');
			}else{
				$user = $usuarios->get_id($proyecto['id_contratista']);
				$usuario = $user['id'].'_'.$user['nombre'].' '.$user['apellidos'];
				$ruta = "/writable/uploads/Usuarios/".$usuario."/Proyectos/".$proyecto['titulo'];
				// $ruta = $directorio."\\".$proyecto['titulo'];
				return view('/backend/Contratista/viewcells/carrusel-edit', array('imagenes' => $imagenes, 'ruta' => $ruta, 'id' => $id));
			}
		}
	}
	// Funcion de card freelance en Inicio
	public function cardTopPostulante($id){
		$user =  model('Autenticacion')->get_id($id);
		if(!empty($user)) return view('/front/viewcells/freelance-top', array('freelance' => $user, 'nombre' => $user['nombre'].' '.$user['apellidos']));
		return ' ';
	}
	// Funcion para ver tareas de proyecto al cargar
	public function tareasProyecto($id){
		$model = model('Proyectos');
		$data = $model->where('id', $id)->first();
		if($data['tareas'] == null) return '';
		$tareas = json_decode($data['tareas'], true);
		return view('/viewcells/tareas', array('tareas' => $tareas));
	}
	public function tareasProyectoContra($id){
		$model = model('Proyectos');
		$data = $model->where('id', $id)->first();
		if($data['tareas'] == null) return '';
		$tareas = json_decode($data['tareas'], true);
		return view('/viewcells/tareas-contratista', array('tareas' => $tareas));
	}
	// Funcion para ver barra de progreso de trabajo
	public function barraProgreso($id){
		$model = model('Proyectos');
		$proyecto = $model->where('id', $id)->first();
		if($proyecto == null) return '';
		if($proyecto['tareas'] == null) return '';
		$tareas = json_decode($proyecto['tareas'], true);
		$total = count($tareas);
		$x = 0;
		foreach($tareas as $t){
			if($t['estatus'] == true) $x++;
		}
		if($total == 0) return 0;
		$part = 100 / $total;
		$porcentaje = $part * $x;
		return view('/viewcells/progreso', array('progreso' => round($porcentaje, 2)));
	}
	// Funcion para ver calificacion de usuario
	public function verCalificación($id){
		$usuario = model('Autenticacion')->get_id($id);
		if(empty($usuario)) return '';
		// $permisos = new AccesosPlanes($usuario['plan'], $id);
		// if(!$permisos->validarFreelancer('valoraciontrabajos')) return view('viewcells/estrellas', array('promedio' => 0));
		
		$model = model('Calificaciones');
		$cals = $model->where('id_freelancer', $id)->find();
		$suma = 0;
		if($cals == null) return view('viewcells/estrellas', array('promedio' => 0));
		foreach($cals as $c){
			$suma = $suma + $c['calificacion'];
		}
		$promedio = round($suma / count($cals));
		return view('viewcells/estrellas', array('promedio' => $promedio));
	}
	// Funcion para mostrar cantidad de proyectos finalizados con promedio
	public function cantProyectos($id){
		$trabajos = model('Proyectos')->where(['id_freelancer' => $id, 'estatus' => 'finalizado'])->find();
		$total = '';
		($trabajos == null)? $total = 0 : $total = count($trabajos);
		$model = model('Calificaciones');
		// $cals = $model->where('id_freelancer', $id)->find();
		// $suma = 0;
		// foreach($cals as $c){
		//     $suma = $suma + $c['calificacion'];
		// }
		// (count($cals) == 0 )? $promedio = 0 : $promedio = round($suma / count($cals), 1);
		return $total == 1 ? '<i class="fi fi-rr-briefcase me-1"></i> <span>'.$total.' proyecto</span>' :'<i class="fi fi-rr-briefcase me-1"></i> <span>'.$total.' proyectos</span>';
	}
	// Funcion para mostrar card de trabajo disponible
	public function cardProyectoDisponible($proyecto){
		return view('/viewcells/card-trabajo', array('pro' => $proyecto));
	}
	// Funcion para mostrar el presupuesto mas bajo o mas alto
	public function presupuestoTipo($tipo){
		$model = model('Proyectos');
		$proyectos = $model->where('estatus', 'espera')->find();
		if($proyectos == null) return 0;
		if(count($proyectos) < 2 ) return str_replace(",", "", $proyectos[0]['presupuesto']);
		$precios = array();
		foreach($proyectos as $p){
			array_push($precios, str_replace(",", "", $p['presupuesto']));
		}
		if($tipo == 'desc'){
			rsort($precios);
		}else{
			sort($precios);
		}
		return $precios[0];
	}
	// Funcion para obtener habilidad de usuario
	public function obtenerHabilidad($id){
		$user = model('Autenticacion')->get_id($id);
		if(empty($user)) return ' ';
		
		if($user['areas'] == null) return ' ';
		
		return array_values($user['areas'])[0]['area'];
	}
	// Funcion para obtener hablidades principales de cada freelance
	public function habilidadesFreelance($id){
		$user = model('Autenticacion')->get_id($id);
		if(empty($user)) return ' ';
		
		if($user['areas'] == null) return ' ';
		return view('viewcells/areas-freelance', array('areas' => $user['areas']));
	}
	// Funcion para obtener ubicación de contratista
	public function direccionContratista($id){
		if(empty($id)) return 'México';
		
		$user = model('Autenticacion')->get_id($id);
		if(empty($user)) return ' ';
		
		if(empty($user['direccion'])) return ' ';
		
		(trim($user['direccion']['pais']) == '')? $pais = 'México' : $pais = $user['direccion']['pais'];
		(trim($user['direccion']['estado']) == '')? $estado = 'Nuevo Leon' : $estado = $user['direccion']['estado'];
		(trim($user['direccion']['municipio']) == '')? $municipio = 'Monterrey' : $municipio = $user['direccion']['municipio'];
		return $municipio.', '.$estado.', '.$pais;
	}
	// Funcion para mostrar proyectos relacionados 
	public function proyectosRelacionados($id, $pro){
		$user = model('Autenticacion')->get_id($id);
		if($user == null){
			$proyectos = model('Proyectos')->where(['id !=' => $pro, 'estatus' => 'espera'])->orderBy('id', 'desc')->limit(4)->find();
			return view('viewcells/proyectos-relacionados', array('proyectos' => $proyectos));
		}else{
			if($user['areas'] == null){
				$proyectos = model('Proyectos')->where(['id !=' => $pro, 'estatus' => 'espera'])->orderBy('id', 'desc')->limit(4)->find();
			}else{
				$proyectos = model('Proyectos')->where(['id !=' => $pro, 'categoria' => array_values($user['areas'])[0]['area'], 'estatus' => 'espera'])->orderBy('id', 'desc')->limit(4)->find();
			}
			if($proyectos == null) $proyectos = model('Proyectos')->where(['id !=' => $pro, 'estatus' => 'espera'])->orderBy('id', 'desc')->limit(4)->find();
			return view('viewcells/proyectos-relacionados', array('proyectos' => $proyectos));
		}
	}
	// Funcion para mostrar slide de mas noticias diferentes a la que esta
	public function masNoticias($id){
		$noticias = model('Noticias')->where('id !=', $id)->orderBy('id', 'desc')->findALl(6, 0);
		return view('viewcells/noticias-relacionadas', array('noticias' => $noticias));
	}
	// Funcion para mostar sueldo mas bajo
	public function sueldoMinimo(){
		$data = model('Autenticacion')->orderby('pagoHora', 'ASC')->limit(1)->first();
		if($data == null) return 0;
		if($data['pagoHora'] == NULL) return 0;
		return $data['pagoHora'];
	}
	// Funcion para mostrar sueldo mas alto
	public function sueldoMaximo(){
		$data = model('Autenticacion')->orderby('pagoHora', 'DESC')->limit(1)->first();
		if($data == null) return 0;
		if($data['pagoHora'] == NULL) return 0;
		return $data['pagoHora'];
	}
	// Funcion de alerta de no aprobado
	public function verAprovadoONo($id){
		$data = model('Autenticacion')->where('id', $id)->first();
		if(empty($data)){
			$usuarioEliminado = model('Autenticacion')->where('id', $id)->withDeleted()->first();
			if($usuarioEliminado['eliminado'] != 0) return false;
			
			$fecha = conversorZonaHoraria($usuarioEliminado['deleted_at'], app_timezone(), date_default_timezone_get());
			$fecha_bajaCuenta = strtotime($fecha."+ 60 days");
			$fecha_actual = strtotime(date("d-m-Y H:i:s", time()));
			$diferencia = intval(($fecha_bajaCuenta - $fecha_actual) / 86400);
			if($fecha_actual <= $fecha_bajaCuenta) return "<div class='alert alertas shadow alert-warning' role='alert'>Su cuenta será eliminada. <b class='fw-bold'>". $diferencia." días restantes</b></div>";
			else{
				// model('Autenticacion')->where('id', $id)->delete();
				// return redirect()->route('')->with('alerta', ['tipo' => 'correcto', 'mensaje' => 'Usuario no existe']);
			}
		}
		if($data['aprobado'] == 0) return '<div class="alert alertas shadow alert-warning" role="alert">Su cuenta no ha sido aprobada aún, el proceso de aprobación puede durar de 3 a 5 días.</div>';
		return '';
	}
	// Funcion para validar estatus
	public function AprovadoONo($id){
		$data = model('Autenticacion')->where('id', $id)->first();
		if(empty($data)){
			$usuarioEliminado = model('Autenticacion')->where('id', $id)->withDeleted()->first();
			if($usuarioEliminado['eliminado'] != 0) return false;
			
			$fecha = conversorZonaHoraria($usuarioEliminado['deleted_at'], app_timezone(), date_default_timezone_get());
			$fecha_bajaCuenta = strtotime($fecha."+ 60 days");
			$fecha_actual = strtotime(date("d-m-Y H:i:s", time()));
			if($fecha_actual <= $fecha_bajaCuenta) return 'cuenta suspendida';
			else{
				// model('Autenticacion')->purgar_cuenta($id);
				// session()->destroy();
				return redirect()->route('')->with('alerta', ['tipo' => 'falla', 'mensaje' => 'Usuario no existe']);
			}
		}
		if($data['rol'] == 'contratante') return '';
		if(empty($data['aprobado'])) return false;
		return $data['aprobado'];
	}
	// Funcion para que plan ver
	public function planesRol($rol){
		if($rol == 'contratante'){
			return view('viewcells/tabla-planes-contratista');
		}elseif($rol == 'freelancer'){
			return view('viewcells/tabla-planes-freelance');
		}

		return vacio("¡UPS! No pude encontrar los planes: $rol. Comunícate con nosotros para ayudarte.");
		
	}
	// Funcion para ver cantidad de calificaciones
	public function cantidadCalificaciones($id){
		$calificaciones = model('Calificaciones')->where('id_freelancer', $id)->find();
		if($calificaciones == null) return '0 reviews';
		return count($calificaciones).' reviews';
	}
	// Funcion para ver card de solicitud
	public function cardSolicitud($proyecto){
		return view('backend/Freelancer/viewcells/card-solicitud', array('proyecto' => $proyecto));
	}
	// Funcion para ver el nombre del contratista
	public function nameContratista($id){
		$user = model('Autenticacion')->get_id($id);
		if($user == null) return 'Name';
		return $user['nombre'].' '.$user['apellidos'];
	}
	// Funcion para ver diferencia de fechas con la fecha actual
	public function diferenciaFechas($fecha){
		$DateTimeHumanizer = new DateTimeHumanizer();
		$data = $DateTimeHumanizer::difference(new \DateTime(), new \DateTime($fecha), service('request')->getLocale()); // just now
		return $data;
	}
	// Funcion para ver diferencia de tiempo entre dos fechas
	public function diferenciaCantidadTiempo($inicio, $fin){
		$firstDate  = new \DateTime($inicio);
		$secondDate = new \DateTime($fin);
		$intvl = $firstDate->diff($secondDate);
		$diferencia = '';
		if($intvl->y != 0) $diferencia .= $intvl->y . " año, ";
		if($intvl->m != 0 && $intvl->d != 0) $diferencia .= $intvl->m." meses, ".$intvl->d." días";
		if($intvl->m == 0 && $intvl->d != 0){
			if($intvl->d == 1) $diferencia .= $intvl->d.' día';
			else $diferencia .= $intvl->d.' días';
		} 
		if($intvl->m != 0 && $intvl->d == 0) $diferencia .= $intvl->m.' meses';
		return $diferencia;
	}
	public function verCalificacionPerfil($id){
		$model = model('Calificaciones');
		$cals = $model->where('id_freelancer', $id)->find();
		$suma = 0;
		if($cals == null) return view('viewcells/estrellasPerfil', array('promedio' => 0, 'total' => 0));
		foreach($cals as $c){
			$suma = $suma + $c['calificacion'];
		}
		$promedio = intval($suma / count($cals));
		$tot = round($suma / count($cals), 1);
		return view('viewcells/estrellasPerfil', array('promedio' => $promedio, 'total' => $tot));
	}
	public function areasPerfil($freelancer){
		return view('backend/Freelancer/viewcells/areas-perfil', array('freelancer' => $freelancer));
	}
	public function experienciasPerfil($freelancer){
		return view('backend/Freelancer/viewcells/experiencias-perfil', array('freelancer' => $freelancer));
	}
	// Funcion para mostrar idiomas guardados en vase de datos
	public function idiomasActivos($atributo){
		return view('viewcells/idiomas-activos', array('idiomas' => model('Idiomas')->findAll(), 'atributo' => $atributo));
	}
	// Funcion para mostrar tabla de idiomas
	public function tablaIdiomas(){
		$data = file_get_contents("paises.json");
		$idiomas = array();
		$datos = json_decode($data, true);
		foreach($datos as $key => $d){
			if(empty($d['languages'])) continue;
			$dat = [
				'pais' => $d['translations']['spa']['common'],
				'identificador' => $d['cca3'],
				'bandera' => $d['flags']['svg'],
			];
			array_push($idiomas, $dat);
		}
		return view('backend/Freelancer/viewcells/idiomas-lista', array('idiomas' => $idiomas));
	}
	// Funcion para mostrar slides del home
	public function formBuscador(string $buscador){
		$datos['buscador'] = $buscador;
		$datos['areas'] = model('Habilidades')->where(['superior' => '0'])->find();

		$usuario = model('Autenticacion')->get_UsuarioID(session('id'));
		$permisos = new AccesosPlanes($usuario['plan'] ?? '');
		
		$buscar_freelancers = $permisos->getValor_permiso('buscadorfreelancer');
		$datos['buscar_freelancers'] = ($buscar_freelancers === 0 || ($buscar_freelancers == 'restringido')) && !Suscripciones::esta_suscrito_por_idUsuario(session('id')) ? false : true;
		
		return view('/viewcells/buscador-slide', $datos);
	}
	// Funcion para mostrar selector de categorias
	public function categoriasSelector(){
		$data = model('Habilidades')->where(['superior' => '0'])->find();
		return view('/viewcells/selector-categoria', array('categorias' => $data));
	}
	// Funcion para traer datos de planes
	public function datoPlan($planid, $dato){
		if($planid != 'free' && $planid != null && $planid != ''){
			$plan = model('ModelPlanes')->where('id_plan_mensual', $planid)->first();
			if($plan != null){
				if($dato == 'monto') return 'Total a pagar $'.$plan[$dato].'.00 <small>MXN</small>';
				if($dato == 'name') return $plan[$dato].' Mensual';
				return $plan[$dato];
			}else{
				$plan = model('ModelPlanes')->where('id_plan_anual', $planid)->first();
				if($plan != null){
					if($dato == 'monto') return 'Total a pagar: $'.$plan['monto_anual'].'.00 <small>MXN</small> anualmente.<br>Precio mensual: $'.($plan['monto_anual'] / 12).'.00 <small>MXN</small>';
					if($dato == 'name') return $plan[$dato].' Anual';
					return $plan[$dato];
				}else{
					if($dato == 'name') return 'Gratuito';
					if($dato == 'monto') return ' ';
					if($dato == 'descripcion') return ' ';
				}
			}
		}else{
			if($dato == 'name') return 'Gratuito';
			if($dato == 'monto') return ' ';
			if($dato == 'descripcion') return ' ';
		}
	}
	// Funcion para ver informacion de pago
	public function datoPlanAdmin($planid, $dato){
		$plan = model('Suscripciones')->informacion_plan($planid);
		// dd($plan);
		if(empty($plan)) return ' ';
		if($dato == 'monto') return '$'.$plan['precio'].'.00';
		if($dato == 'name') return $plan['suscripcion'];
		
		return $plan[$dato];
	}
	//Funcion para validar si ya existe una suscripcion o no
	// public function validarSuscripcion($id){
	// 	$data = model('Suscripciones')->where('id_usuario', $id)->first();
	// 	($data == null)? $a = 'false' : $a = 'true';
	// 	return $a;
	// }
	//Funcion para mostrar alerta de completar perfil
	public function completarPerfil(){
		$x = ' ';
		$user = model('Autenticacion')->find(session('id'));
		if($user == null) return $x;
		if(session('rol') == 'freelancer'){
			if($user['direccion'] == null) return '<div class="alert alert-warning alertas shadow" role="alert">Perfiles más completos tienen mayor posibilidades de conseguir proyectos.</div>';
			if($user['areas'] == null) return '<div class="alert alert-warning alertas shadow" role="alert">Perfiles más completos tienen mayor posibilidades de conseguir proyectos. Te invitamos a completar tu perfil.</div>';
			if($user['experiencia'] == null) return '<div class="alert alert-warning alertas shadow" role="alert">Perfiles más completos tienen mayor posibilidades de conseguir proyectos. Te invitamos a completar tu perfil.</div>';
		}else{
			if($user['direccion'] == null) return '<div class="alert alert-warning alertas shadow" role="alert">Complete la información de su perfil</div>';
		}
		return $x;
	}
	//Funcion para mostrar alertas en header (cantidad)
	public function cantAlertas($id){
		$cantalert = 0;
		if(!session('logeado')) return $cantalert;

		$perfil = $this->completarPerfil();
		if($perfil != ' ') $cantalert++;
		
		$dias = $this->diasPreviosFinalizarSuscripcion($id);
		if($dias != ' ') $cantalert++;

		if(autorizacion() == 'freelancer'){
			$solicitudes = model('Solicitudes')
						->where(['solicitudes.id_freelancer' => session('id'), 'solicitudes.estatus' => 'espera'])
						->join('proyectos', 'solicitudes.id_proyecto = proyectos.id')
						->countAllResults();
			if($solicitudes > 0) $cantalert++;
		}
		return $cantalert;
	}
	public function cantidadProyectosNivel($nivel){
		$where = 'nivel = "'.$nivel.'" AND deleted_at IS NULL';
		$data = model('Proyectos')->where($where)->findAll();
		foreach($data as $key => $d){
			if($d['estatus'] != 'espera') unset($data[$key]);
		}
		return count($data);
	}
	//Funcion para badge de cantidad de dias
	public function cantidadProyectosTiempo($minimo, $maximo){
		$proyectos = model('Proyectos')->where('estatus', 'espera')->findAll();
		$proy = array();
		foreach($proyectos as $key => $p){
			$fecha1= new \DateTime($p['fecha_arranque_esperado']);
			$fecha2= new \DateTime($p['fecha_entrega']);
			$diff = $fecha1->diff($fecha2);
			if($diff->days >= $minimo && $diff->days <= $maximo){
				array_push($proy, $p);
			}
		}
		return count($proy);
	}
	//Funcion para mostrar si hay suscripcion actual o no
	public function suscripcionActual(){
		return model('Suscripciones')->builder()
									->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
									->where('id_usuario', session('id')
									)->get()->getRowArray();
	}
	// Funcion para ver que tantos proyectos disponibles hay
	public function cantDisponiblesProyectos(){
		$data = model('Proyectos')->where('estatus', 'espera')->find();
		if($data == null) return '0';
		return number_format(count($data));
	}
	// Funcion para ver historial de pago
	public function historialPago(){
		if(!session('logeado')) return '<div class="text-center">Su sesión ha terminado, ingrese nuevamente para ver su historial de pagos</div>';
		$pagos = model('Pagos')->where('id_usuario', session('id'))->orderBy('id', 'DESC')->find();
		if($pagos == null) return vacio();
		
		$usuario = model('Autenticacion')->get_id(session('id'));
		return view('/backend/viewcells/pagos', array('pagos' => $pagos, 'user' => $usuario));
	}
	// Funcion para mostrar archivo de pago de transferencia o de pago en tienda
	public function mostrarArchivoPago($id){ //MODIFICAR A NETPAY PASARELA DE PAGOS
		$pago = model('Pagos')->where('id', $id)->first();
		return '';
		// $api[0]['modo'] == 'prueba' ? $url = $api[0]['path_prueba'] . 'paynet-pdf/' . $api[0]['ID'] . '/' : $url = $api[0]['path_produccion'] . 'paynet-pdf/' . $api[0]['ProduccionID'] . '/';
		// $openpay = model('Autenticacion')->objectOpenpay();
		// $usuario = model('Autenticacion')->get_id(session('id'));
		// $customer = $openpay->customers->get($usuario['id_openpay']);
		// $charge = $customer->charges->get($pago['id_compra']);
		// return view('/backend/viewcells/liga-archivo-pago', array('charge' => $charge, 'url' => $url));
	}
	// Funcion para mostrar estatus de pago
	public function EstatusPago($pago){
		if($pago['estatus'] == 'Activo') return 'Completado';
		$hoy = new Time('now', app_timezone(), 'es_MX');
		$firstDate  = new \DateTime($pago['created_at']);
		$secondDate = new \DateTime($hoy);
		$diff = $firstDate->diff($secondDate);
		if(intval($diff->days) > 2) return 'Cancelado';
		return 'En proceso';
	}
	// Funcion para mostrar datos de cargo
	public function verDatosCargo($user, $compra){
		$openpay = model('Autenticacion')->objectOpenpay();
		$customer = $openpay->customers->get($user);
		$charge = $customer->charges->getList(['creation[lte]' => date('Y-m-d')]);
		return d($charge);
	}
	// FUncion para ver testimonios
	public function testimonios(){
		return view('/front/viewcells/testimonios');
	}
	// Funciuon de preguntas en tarifas
	public function tarifasFAQ(){
		return view('/front/viewcells/preguntas-tarifas');
	}
	// Funcion para descripcion de estado de membresia
	public function estaadoDescripcionSuscripcion(){
		$ultpago = model('Pagos')->where('id_usuario', session('id'))->orderBy('id', 'DESC')->first();
		$suscripcion = model('Suscripciones')->where('id_usuario', session('id'))->first();
		$hoy = conversorZonaHoraria(date('Y-m-d H:i:s'), app_timezone(), date_default_timezone_get());
		$descripcion = '';
		if($suscripcion == null) return "Plan actual: <b class='fw-bold'>Gratuito</b>";
		if($suscripcion['estatus'] == 'Activo' || $suscripcion['estatus'] == 'Confirmado'){
			if(strtotime($suscripcion['fecha_fin']) >= strtotime($hoy) || !empty($suscripcion['fecha_cancelacion'])){
				$suscripcion_actual = model('Suscripciones')
										->builder()
										->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
										->where('id_usuario', session('id'))
										->get()->getRowArray();
				$descripcion = "Plan actual: <b class='fw-bold'>".$suscripcion_actual['name'].'</b>';
			}else{
				(session('rol') == 'freelancer')? $descripcion = "Plan actual: <b class='fw-bold'>Gratuito</b>" : $descripcion = "Plan actual: <b class='fw-bold'>Gratuito</b>";
			}
			return $descripcion;
		}else{
			if($ultpago['estatus'] == 'En proceso'){
				$plan = model('ModelPlanes')->where('id_plan_mensual', $ultpago['id_membresia'])->orWhere('id_plan_anual', $ultpago['id_membresia'])->first();
				if($plan != null) return "Plan actual: <b class='fw-bold'>".$plan['name'].'</b> (En proceso de pago)';
				$plananual = model('ModelPlanes')->where('id_plan_mensual', $ultpago['id_membresia'])->orWhere('id_plan_anual', $ultpago['id_membresia'])->first();
				if($plananual != null ) $descripcion = "Plan actual: <b class='fw-bold'>".$plananual['name'].' Anual</b> (En proceso de pago)';
			}else{
				if(strtotime($suscripcion['fecha_fin']) <= strtotime($hoy)) return "Plan actual: <b class='fw-bold'>Gratuito</b>";
				$plan = model('ModelPlanes')->where('id_plan_mensual', $ultpago['id_membresia'])->orWhere('id_plan_anual', $ultpago['id_membresia'])->first();
				if($plan != null) return "Plan actual: <b class='fw-bold'>".$plan['name'].'</b>';
				$plananual = model('ModelPlanes')->where('id_plan_mensual', $ultpago['id_membresia'])->orWhere('id_plan_anual', $ultpago['id_membresia'])->first();
				if($plananual != null ) $descripcion = "Plan actual: <b class='fw-bold'>".$plananual['name'].'</b>';
			}
			return $descripcion;
		}
	}
	// Funcion para consultar mensajes en firebase
	public function consultarMensajesFirebase(){
		$url = 'mextemps-341000.firebaseapp.com/';
	}
	// Funcion para validar si correo existe o no
	public function validarEmail($email){
		$sender    = 'correos@tresesenta.lat';
		$validator = new SmtpEmailValidator($email, $sender);
		
		$datoscorreo = explode("@", $email);
		$results   = $validator->validate();

		$totalmxs = count($results['domains'][$datoscorreo[1]]['mxs']);
		($totalmxs > 1)? $return = true : $return = false;
		return $return;
	}
	// Funcion para validar cantidad de proyectos en rango de filtro de talentos
	public function cantRangoProyectosConcluidos($rango){
		$freelancers = model('Autenticacion')->where('rol', 'freelancer')->find();
		$total = array();
		switch ($rango) {
			case 0:
				$x = 0;
				foreach($freelancers as $f){
					$proyectos = model('Proyectos')->where(['estatus' => 'finalizado', 'id_freelancer' => $f['id']])->find();
					if($proyectos != null){
						if(count($proyectos) >= 1 && count($proyectos) <= 15) $x++;
					}
				}
				return $x;
				break;
			case 1:
				$x = 0;
				foreach($freelancers as $f){
					$proyectos = model('Proyectos')->where(['estatus' => 'finalizado', 'id_freelancer' => $f['id']])->find();
					if($proyectos != null){
						if(count($proyectos) >= 16 && count($proyectos) <= 30) $x++;
					}
				}
				return $x;
				break;
			case 2:
				$x = 0;
				foreach($freelancers as $f){
					$proyectos = model('Proyectos')->where(['estatus' => 'finalizado', 'id_freelancer' => $f['id']])->find();
					if($proyectos != null){
						if(count($proyectos) >= 31 && count($proyectos) <= 45) $x++;
					}
				}
				return $x;
				break;
			case 3:
				$x = 0;
				foreach($freelancers as $f){
					$proyectos = model('Proyectos')->where(['estatus' => 'finalizado', 'id_freelancer' => $f['id']])->find();
					if($proyectos != null){
						if(count($proyectos) >= 46 && count($proyectos) <= 60) $x++;
					}
				}
				return $x;
				break;
			case 4:
				$x = 0;
				foreach($freelancers as $f){
					$proyectos = model('Proyectos')->where(['estatus' => 'finalizado', 'id_freelancer' => $f['id']])->find();
					if($proyectos != null){
						if(count($proyectos) >= 61 && count($proyectos) <= 99) $x++;
					}
				}
				return $x;
				break;
			case 5:
				$x = 0;
				foreach($freelancers as $f){
					$proyectos = model('Proyectos')->where(['estatus' => 'finalizado', 'id_freelancer' => $f['id']])->find();
					if($proyectos != null){
						if(count($proyectos) >= 100) $x++;
					}
				}
				return $x;
				break;
			default:
				return '+99';
				break;
		}
	}
	public function limitar_cadena($cadena, $limite, $sufijo){
		// Si la longitud es mayor que el límite...
		if(strlen($cadena) > $limite){
			// Entonces corta la cadena y ponle el sufijo
			return substr($cadena, 0, $limite) . $sufijo;
		}
		// Si no, entonces devuelve la cadena normal
		return $cadena;
	}
	// Funcion para mostrar cantidad de registros en una tabla
	public function cantidadRegistros($tabla, $id, $campo){
		$db      = \Config\Database::connect();
		$builder = $db->table($tabla);
		$builder->where($campo, $id);
		$data = $builder->get()->getResultArray();
		return count($data);
	}
	// Funcion para mostrar calificaciones del usuario
	public function calificacionesUsuario($id){
		$calificaciones = model('Calificaciones')->where('id_freelancer', $id)->orderBy('id', 'DESC')->limit(3)->find();
		if($calificaciones == null) return vacio('Sin calificaciones aún');
		return view('/backend/viewcells/card-calificacion', array('calificaciones' => $calificaciones, 'id' => $id));
	}
	public function nivelInsignia($cantidad){
		if($cantidad >= 1 && $cantidad <= 15) return 'Básico';
		elseif($cantidad >= 16 && $cantidad <= 30) return 'Intermedio';
		elseif($cantidad >= 31 && $cantidad <= 45) return 'Avanzado';
		elseif($cantidad >= 46 && $cantidad <= 60) return 'Experto';
		elseif($cantidad >= 61 && $cantidad <= 99) return 'Platino';
		elseif($cantidad >= 100) return 'Diamante';
		
		return 'Ninguno';
	}
	public function misInsignias($cantidad){
		$nivel = 0;
		if($cantidad >= 1 && $cantidad <= 15) $nivel = 1;
		elseif($cantidad >= 16 && $cantidad <= 30) $nivel = 2;
		elseif($cantidad >= 31 && $cantidad <= 45) $nivel = 3;
		elseif($cantidad >= 46 && $cantidad <= 60) $nivel = 4;
		elseif($cantidad >= 61 && $cantidad <= 99) $nivel = 5;
		elseif($cantidad >= 100) $nivel = 6;
		
		return view('/viewcells/nivel-insignia', array('nivel' => $nivel));
	}
	public function miNivelInsigniaCard($cantidad){
		$nivel = 0;
		if($cantidad >= 1 && $cantidad <= 15) $nivel = 1;
		elseif($cantidad >= 16 && $cantidad <= 30) $nivel = 2;
		elseif($cantidad >= 31 && $cantidad <= 45) $nivel = 3;
		elseif($cantidad >= 46 && $cantidad <= 60) $nivel = 4;
		elseif($cantidad >= 61 && $cantidad <= 99) $nivel = 5;
		elseif($cantidad >= 100) $nivel = 6;
		
		if($nivel == 0) return '<img src="'.base_url('/assets/images/insignias/no-obtenidas/insignia-1-g.svg').'" style="max-width: 25px; display: inline-block; vertical-align: middle;" alt="Mextemps">';
		return '<img src="'.base_url('/assets/images/insignias/obtenidas/insignia-'.$nivel.'.svg').'" style="max-width: 25px; display: inline-block; vertical-align: middle;" alt="Mextemps">';
	}
	public function miNivelInsignia($cantidad, $tipo){
		$nivel = 0;
		if($cantidad >= 1 && $cantidad <= 15) $nivel = 1;
		elseif($cantidad >= 16 && $cantidad <= 30) $nivel = 2;
		elseif($cantidad >= 31 && $cantidad <= 45) $nivel = 3;
		elseif($cantidad >= 46 && $cantidad <= 60) $nivel = 4;
		elseif($cantidad >= 61 && $cantidad <= 99) $nivel = 5;
		elseif($cantidad >= 100) $nivel = 6;
		
		if($tipo == 'icono' && $nivel == 0) return '<img src="'.base_url('/assets/images/insignias/no-obtenidas/insignia-1-g.svg').'" alt="Mextemps">';
		if($tipo == 'icono') return '<img src="'.base_url('/assets/images/insignias/obtenidas/insignia-'.$nivel.'.svg').'" alt="Mextemps">';
		switch($nivel){
			case 0:
				if($tipo == 'nivel') return 'Ninguno';
				return 'Actualmente no tienes ninguna insignia asignada, ya que eres nuevo en nuestra plataforma. Te invitamos a seguir postulándote a diferentes proyectos, así como a ganarlos y concluirlos, para que puedas ser acreedor de estas insignias. Entre más proyectos acumules subirás de ranking y tendrás la insignia que vayas ganando.';
				break;
			case 1:
				if($tipo == 'nivel') return 'Básico';
				return 'Un Freelancer con insignia básica está empezando a dar a conocer su experiencia, todos empiezan por aquí, pero ya cuentan con un talento reconocido, por lo que puedes apoyarlo para impulsarlo a crecer.';
				break;
			case 2:
				if($tipo == 'nivel') return 'Intermedio';
				return 'Un Freelancer con insignia intermedia ya está teniendo una buena reputación con su trabajo realizado en proyectos, por lo que su experiencia está creciendo y ha sido bien evaluado.';
				break;
			case 3:
				if($tipo == 'nivel') return 'Avanzado';
				return 'Un Freelancer con nivel avanzado es bueno para ti, si quieres tener a alguien con una gran experiencia y que domine las habilidades que necesitas para que realice tu proyecto, puedes confiar en él, ya que ha cumplido y ya tiene una gran trayectoria.';
				break;
			case 4:
				if($tipo == 'nivel') return 'Experto';
				return 'Un Freelancer con nivel experto, Wow! Esto es genial, ya que tendrás un gran aliado para que tu proyecto sea realizado con éxito. Puedes tener la confianza y recomendación de docenas de proyectos concluidos.';
				break;
			case 5:
				if($tipo == 'nivel') return 'Platino';
				return 'Un Freelancer nivel platinó, es una persona que sabe lo que hace y tiene un sinfín de proyectos realizados, donde podrás ver su nivel de experiencia y talento para que ese gran compromiso sea reflejado en las tareas que le proporciones.';
				break;
			case 6:
				if($tipo == 'nivel') return 'Diamante';
				return 'WOW un freelancer nivel diamanté es aquel que ya pasó por todos los niveles y ha finalizado cada uno de ellos con éxito. Por lo que puedes confiar en el talento desarrollado y actitudes para apoyarte a crear tu proyecto en tiempo y forma. Y ha demostrado que es el aliado perfecto para crear y trabajar de la mano contigo en todo momento. ';
				break;
			default:
				if($tipo == 'nivel') return 'Básico';
				return 'Un Freelancer con insignia básica está empezando a dar a conocer su experiencia, todos empiezan por aquí, pero ya cuentan con un talento reconocido, por lo que puedes apoyarlo para impulsarlo a crecer.';
				break;
		}
	}
	// Funcion para mostrar nivel del freelance
	public function nivelInsigniaProyecto($nivel){
		$t = 0;
		if($nivel == 'Básico') $t = 1;
		elseif($nivel == 'Intermedio') $t = 2;
		elseif($nivel == 'Avanzado') $t = 3;
		elseif($nivel == 'Experto') $t = 4;
		elseif($nivel == 'Platino') $t = 5;
		elseif($nivel == 'Diamante') $t = 6;
		
		if($t != 0) return '<span class="card-job-top--post-time text-sm d-inline"><img src="'.base_url('/assets/images/insignias/obtenidas/insignia-'.$t.'.svg').'" class="insig-nivel-proyecto" alt="Mextemps">  '.$nivel.'</span>';
		return '<span class="card-job-top--post-time text-sm"><i class="fas fa-medal"></i>  '.$nivel.'</span>';
	}
	// Funcion para mostrar nivel de proyecto para solicitud de freelancers
	public function nivelInsigniaSolicitud($nivel){
		$t = 0;
		if($nivel == 'Básico') $t = 1;
		elseif($nivel == 'Intermedio') $t = 2;
		elseif($nivel == 'Avanzado') $t = 3;
		elseif($nivel == 'Experto') $t = 4;
		elseif($nivel == 'Platino') $t = 5;
		elseif($nivel == 'Diamante') $t = 6;
		
		if($t != 0) return '<img src="'.base_url('/assets/images/insignias/obtenidas/insignia-'.$t.'.svg').'" class="insig-nivel-proyecto" alt="Mextemps">  '.$nivel;
		return '<i class="fas fa-medal"></i>  '.$nivel;
	}
	// Funcion mostrar cantidad de clientes insatisfechos por un freelance
	public function clientesInsatisfechos($id){
		$calificaciones = model('Calificaciones')->where(['id_freelancer' => $id, 'conforme' => 'No'])->find();
		if($calificaciones == null) return 0;
		return count($calificaciones);
	}
	// Funcion para mostrar al freelance seleccionado del proyecto
	public function freelanceInteriorProyecto($id){
		$user = model('Autenticacion')->get_id($id);
		if($user == null) return ' ';
		return view('/backend/viewcells/freelance-interior-proyecto', array('usuario' => $user));
	}
	// Funcion para mostrar el archivo de resumen de compra con pago tarjeta
	public function mostrarArchivoPagoTarjeta($id, $tipo){
		return view('/backend/viewcells/liga-archivo-pago-ext', array('id' => $id, 'tipo' => $tipo));
	}
	// Funcion para humanizar fecha de inicio de suscripcion de plan gratis
	public function fechaInicioVencida(){
		$con_suscripcion = Suscripciones::esta_suscrito_por_idUsuario(session('id'));

		if($con_suscripcion){
			$suscripcion = model('Suscripciones')->where('id_usuario', session('id'))->first();
			return strftime('%d/%B/%Y', strtotime($suscripcion['fecha_fin']));
		}
		
		$user = model('Autenticacion')->where('id', session('id'))->first();
		return strftime('%d/%B/%Y', strtotime($user['created_at']));
	}
	// Funcion para humanizar fecha de finalizar de fin de suscripcion plan gratis
	public function fechaFinVencida(){
		$con_suscripcion = Suscripciones::esta_suscrito_por_idUsuario(session('id'));

		if($con_suscripcion){
			$suscripcion = model('Suscripciones')->where('id_usuario', session('id'))->first();
			return strftime('%d/%B/%Y', strtotime($suscripcion['fecha_fin']."+ 1 year"));
		}

		$user = model('Autenticacion')->where('id', session('id'))->first();
		return strftime('%d/%B/%Y', strtotime($user['created_at']."+ 1 year"));
	}
	
	// Funcion para mostrar resumen de compras en diferentes secciones del sitio
	public function resumenCompraPlan($id, $vista = 'resumen-compra'){
		$plan = model('Suscripciones')->informacion_plan($id);
		return view("/backend/viewcells/$vista", $plan);
	}
	// // Funcion para mostrar resumen de compras en registro
	// public function resumenCompraPlanRegistro($id){
	// 	$plan = model('Suscripciones')->informacion_plan($id);
	// 	return view('/backend/viewcells/resumen-compra-registro', $plan);
	// }
	// public function resumenCompraRegistroCupon($iduser, $cupon){
	// 	$cupon = model('Cupones')->where('codigo', $cupon)->first();
	// 	$hoy = new Time('now', app_timezone(), 'es_MX');
	// 	$usuario = model('Autenticacion')->get_UsuarioID($iduser);

	// 	$plan = model('Suscripciones')->informacion_plan($usuario['plan']);
	// 	return view('/backend/viewcells/resumen-compra-registro-cupon', array('cupon' => $cupon, 'plan' => $plan, 'hoy' => $hoy));
	// }
	// Funcion para mostrar resumen de compra en modal de registro
	//TODO: Cambiar a valor no default en caso de que se quiera cambiar la modalidad de el resumen de compra con tarjeta
	public function resumenCompraPlanModal($id, $es_tarjeta){
		$plan = model('Suscripciones')->informacion_plan($id);
		$plan['es_tarjeta'] = $es_tarjeta;
		
		if(session('cupon') == '') return view('/backend/viewcells/resumen-compra-modal-tarjeta', $plan);

		$cupon = model('Cupones')->where('codigo', session('cupon'))->first();
		$hoy = new Time('now', app_timezone(), 'es_MX');

		return view('/backend/viewcells/resumen-compra-registro-cupon-modal', array('cupon' => session('cupon'), 'plan' => $plan, 'hoy' => $hoy));
	}
	// Funcion para crear folio de compra
	public function folioCompra($id){
		$pago = model('Pagos')->where('id_compra', $id)->first();
		if($pago == null) return 'CO-000000-NA';
		
		return 'CO-'.strftime('%d%m%y', strtotime(explode(' ', $pago['fecha'])[0])).'-'.$pago['id'];
	}
	// Alerta dias faltantes de para finalizar membresia
	public function diasPreviosFinalizarSuscripcion($id){
		$x = ' ';
		$data = model('Suscripciones')->where('id_usuario', $id)->first();
		$con_suscripcion = Suscripciones::esta_suscrito_por_idUsuario(session('id'));
		if(!$con_suscripcion) return $x;
		
		$hoy = new Time('now', app_timezone(), 'es_MX');
		$fin = new Time($data['fecha_fin'], 'America/Monterrey', 'es_MX');
		if(strtotime($fin) < strtotime($hoy)) return $x;
		// if($hoy > $fin) return $x;
		$date1 = new \DateTime($hoy);
		$date2 = new \DateTime($fin);
		$diff = $date1->diff($date2);
		($diff->days > 1)? $days = 'días' : $days = 'un día' ;
		if($diff->days < 12) return '<div class="alert alert-warning alertas shadow" role="alert">Tu suscripción está a punto de finalizar en '.$diff->days.' '.$days.', te recomendamos adquirir una nueva membresía para no perder los beneficios dentro de la plataforma.</div>';
		return $x;
	}
	
	public function tarjetas_Blog($noticia, $conteo){
		return view('viewcells/tarjetas_Blog', compact('noticia', 'conteo'));
	}
}