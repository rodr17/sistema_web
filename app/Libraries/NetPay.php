<?php namespace App\Libraries;

require_once(COMPOSER_PATH);

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException as ErrorServidor;

class NetPay{
	protected $cliente, $llave_secreta, $modo, $llave_publica, $base_url;

	public function __construct(){
		if (get_netPay('modo') === 'prueba') {
			$this->modo = true;
			$this->llave_secreta = get_netPay('privada');
			$this->llave_publica = get_netPay('publica');
			$this->base_url = get_netPay('path_prueba');
		} else {
			$this->modo = false;
			$this->llave_secreta = get_netPay('privadaProduccion');
			$this->llave_publica = get_netPay('publicaProduccion');
			$this->base_url = get_netPay('path_produccion');
		}

		$this->cliente = new GuzzleClient(['base_uri' => $this->base_url]);
	}

	/**
	* @return array
	*/
	protected function getEncabezados(){
		return [
			'Authorization' => $this->llave_secreta,
			'Content-Type' => 'application/json',
			'Accept' => 'application/json'
		];
	}

	/**
	* @return string
	*/
	public function getLlave(){
		return $this->llave_publica;
	}

	/**
	* @return boolean
	*/
	public function getModo(){
		return $this->modo;
	}

	/**
	* @return array
	*/
	public function getTransaccion(string $id){
		try{
			$response = $this->cliente->request('GET', "v3/transactions/$id", ['headers' => $this->getEncabezados()]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return ['message' => $error->getResponse()->getReasonPhrase()];
		}
	}

	/**
	* @return array
	*/
	public function crearCliente(array $cuerpo_parametros){
		$datos_cliente = json_encode($cuerpo_parametros);
		try{
			$response = $this->cliente->request('POST', 'v3/clients', [
				'headers' => $this->getEncabezados(),
				'body' => $datos_cliente
			]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return ['message' => $error->getResponse()->getReasonPhrase()];
		}
	}

	/**
	* @return array
	*/
	public function getClienteId(string $idCliente){
		try{
			$response = $this->cliente->request('GET', "v4/clients/$idCliente", ['headers' => $this->getEncabezados()]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return ['message' => $error->getResponse()->getReasonPhrase()];
		}
	}

	/**
	* @return array
	*/
	public function getClientes(){
		try{
			$response = $this->cliente->request('GET', 'v4/clients?active=true', ['headers' => $this->getEncabezados()]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return json_encode(['message' => $error->getResponse()->getReasonPhrase()]);
		}
	}

	/**
	* @return array
	*/
	public function editarCliente(string $idCliente, array $datos){
		try{
			$response = $this->cliente->request('PUT', "v3/clients/$idCliente", [
				'headers' => $this->getEncabezados(),
				'body' => json_encode($datos)
			]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return json_encode(['message' => $error->getResponse()->getReasonPhrase()]);
		}
	}

	/**
	* @return array
	*/
	public function getTarjetasCliente(string $idCliente){
		$cliente = $this->getClienteId($idCliente);
		if(isset($cliente['message'])) return $cliente;
		
		return $cliente['paymentSources'];
	}

	/**
	* @return array
	*/
	public function getTarjetaClienteSuscripcion(string $idCliente, string $token){
		return $cliente = $this->getClienteId($idCliente);
		$clave = array_search($token, array_column($cliente['paymentSources'], 'source'));
		return $cliente['paymentSources'][$clave]['card'];
	}

	/**
	* @return array
	*/
	public function getTarjetaCliente(string $idCliente, string $token){
		$cliente = $this->getClienteId($idCliente);
        if(isset($cliente['message'])) return $cliente;
		
		$clave = array_search($token, array_column($cliente['paymentSources'], 'source'));
		return $cliente['paymentSources'][$clave]['card'];
	}

	/**
	* @return array
	*/
	public function crearTarjetaCliente(string $idCliente, array $parametros){
		$datos_cliente = json_encode($parametros);
		try{
			$response = $this->cliente->request('PUT', "v3/clients/$idCliente/token", [
				'headers' => $this->getEncabezados(),
				'body' => $datos_cliente
			]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return json_encode(['message' => $error->getResponse()->getReasonPhrase()]);
		}
	}
	
	/**
	* @return array
	*/
	public function eliminarTarjetaCliente(string $idCliente, string $token){
		try{
			$response = $this->cliente->request('DELETE', "v3/clients/$idCliente/token/$token", ['headers' => $this->getEncabezados()]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return json_encode(['message' => $error->getResponse()->getReasonPhrase()]);
		}
	}

	/**
	* @return array
	*/
	public function crearPlan(array $cuerpo_parametros){
		$datos_plan = json_encode($cuerpo_parametros);
		try{
			$response = $this->cliente->request('POST', "v3/plans", [
				'headers' => $this->getEncabezados(),
				'body' => $datos_plan
			]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error) {
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return json_encode(['message' => $error->getResponse()->getReasonPhrase()]);
		}
	}

	/**
	* @return array
	*/
	public function crearCargo(array $cuerpo_parametros){
		try{
			$response = $this->cliente->request('POST', "v3.5/charges", [
				'headers' => $this->getEncabezados(),
				'body' => json_encode($cuerpo_parametros)
			]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return ['message' => $error->getResponse()->getReasonPhrase()];
		}
	}

	/**
	* @return array
	*/
	public function confirmarPago_3DS(string $id_transaccion, string $id_procesador){
		try{
			$response = $this->cliente->request('POST', "v3.5/charges/$id_transaccion/confirm?processorTransactionId=$id_procesador", [
				'headers' => $this->getEncabezados(),
			]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return ['message' => $error->getResponse()->getReasonPhrase()];
		}
	}


	/**
	* @return array
	*/
	public function getPlan(string $id){
		try{
			$response = $this->cliente->request('GET', "v3/plans/$id", ['headers' => $this->getEncabezados()]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return json_encode(['message' => $error->getResponse()->getReasonPhrase()]);
		}
	}
	
	/**
	* @return array
	*/
	public function getPlanes(){
		try{
			$response = $this->cliente->request('GET', 'v4/plans?active=true', ['headers' => $this->getEncabezados()]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return json_encode(['message' => $error->getResponse()->getReasonPhrase()]);
		}
	}

	/**
	* @return array
	*/
	public function actualizarPlan(string $id, array $cuerpo_parametros){
		$datos_plan = json_encode($cuerpo_parametros);
		try{
			$response = $this->cliente->request('PUT', "v3/plans/$id", [
				'headers' => $this->getEncabezados(),
				'body' => $datos_plan
			]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return json_encode(['message' => $error->getResponse()->getReasonPhrase()]);
		}
	}

	/**
	* @return array
	*/
	public function cancelarPlan(string $id){
		try{
			$response = $this->cliente->request('PUT', "v3/plans/$id/cancel", ['headers' => $this->getEncabezados()]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return json_encode(['message' => $error->getResponse()->getReasonPhrase()]);
		}
	}

	/**
	* @return array
	*/
	public function crearSuscripcion(string $id_plan, array $parametros){
		$datos_plan = json_encode($parametros);
		try{
			$response = $this->cliente->request('POST', "v3/plans/$id_plan/subscriptions", [
				'headers' => $this->getEncabezados(),
				'body' => $datos_plan
			]);

			$suscripcion = json_decode($response->getBody(), true);
			$suscripcion['status'] = $this->setEstatusSuscripcion($suscripcion['status']);
			return $suscripcion;

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return ['message' => $error->getResponse()->getReasonPhrase()];
		}
	}

	/**
	* @return array
	*/
	public function getSuscripciones(){
		try{
			$response = $this->cliente->request('GET', "v4/subscriptions?active=true", ['headers' => $this->getEncabezados()]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return ['message' => $error->getResponse()->getReasonPhrase()];
		}
	}

	/**
	* @return array
	*/
	public function getSuscripcionCliente(string $idSuscripcion){
		try{
			$response = $this->cliente->request('GET', "v4/subscriptions/$idSuscripcion", ['headers' => $this->getEncabezados()]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return ['message' => $error->getResponse()->getReasonPhrase()];
		}
	}

	/**
	 * Devuelve el estatus de una suscripci贸n m谩s descriptivo
	* @return string
	*/
	protected function setEstatusSuscripcion(string $estatus){
		switch($estatus){
			case 'A': return 'Activo'; break;
			case 'P': return 'Confirmado'; break;
			case 'W': return 'Esperando'; break;
			case 'C': return 'Cancelado'; break;
			case 'N': return 'No_pagado'; break;
			case 'F': return 'Fallado'; break;
			case 'R': return 'Reintentar'; break;
		}
	}

	/**
	* @return array
	*/
	public function actualizarSuscripcion(string $id_suscripcion, int $monto){
		try{
			$response = $this->cliente->request('PUT', "v3/subscriptions/$id_suscripcion", [
				'headers' => $this->getEncabezados(),
				'body' => json_encode(['amount' => $monto])
			]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return ['message' => $error->getResponse()->getReasonPhrase()];
		}
	}

	/**
	* @return array
	*/
	public function actualizarTarjetaSucripcion(string $idCliente, string $token_tarjeta){
		$datos['paymentSource'] = ['source' => $token_tarjeta, 'type' => 'card'];
		try{
			$response = $this->cliente->request('PUT', "v3/clients/$idCliente", [
				'headers' => $this->getEncabezados(),
				'body' => json_encode($datos)
			]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return ['message' => $error->getResponse()->getReasonPhrase()];
		}
	}

	/**
	* @return array
	*/
	public function cancelarSuscripcion(string $idSuscripcion){
		try{
			$response = $this->cliente->request('PUT', "v3/subscriptions/$idSuscripcion/cancel", ['headers' => $this->getEncabezados()]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return ['message' => $error->getResponse()->getReasonPhrase()];
		}
	}

	/**
	* @return array
	*/
	public function webhook(string $metodoRequest, string $url = ''){
		if(!empty($url)) $datos['body'] = json_encode(['webhook' => $url]);

		$datos['headers'] = $this->getEncabezados();
		try{
			$response = $this->cliente->request($metodoRequest, 'v3/webhooks/', $datos);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return json_encode(['message' => $error->getResponse()->getReasonPhrase()]);
		}
	}
}
