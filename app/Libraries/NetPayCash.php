<?php namespace App\Libraries;
require_once('vendor/autoload.php');

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException as ErrorServidor;

class NetPayCash extends NetPay{

	public function __construct(){
		if (get_netPay('modo') === 'prueba') {
			$this->modo = true;
			$this->llave_secreta = get_netPay('privadaCash');
			$this->llave_publica = get_netPay('publicaCash');
			$this->base_url = get_netPay('path_pruebaCash');
		} else {
			$this->modo = false;
			$this->llave_secreta = get_netPay('privadaProduccionCash');
			$this->llave_publica = get_netPay('publicaProduccionCash');
			$this->base_url = get_netPay('path_produccionCash');
		}

		$this->cliente = new GuzzleClient(['base_uri' => $this->base_url]);
	}
	
	public function estaActivoCash(){
		try{
			$response = $this->cliente->request('GET', "v3/stores", ['headers' => $this->getEncabezados()]);

			$activo = json_decode($response->getBody(), true);
			return $activo['cashPaymentEnable'];

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return ['message' => $error->getResponse()->getReasonPhrase()];
		}
	}

	public function crearReferencia(array $parametros){
		$datos_pago = json_encode($parametros);
		try{
			$response = $this->cliente->request('POST', "v3/charges", [
				'headers' => $this->getEncabezados(),
				'body' => $datos_pago
			]);
			return json_decode($response->getBody(), true);

		}catch(ClientException $error){
			$respuesta = json_decode($error->getResponse()->getBody()->getContents(), true);
			return $respuesta;

		}catch(ErrorServidor $error){
			return ['message' => $error->getResponse()->getReasonPhrase()];
		}
	}
}