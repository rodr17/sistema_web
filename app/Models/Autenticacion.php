<?php namespace App\Models;

use CodeIgniter\Model;
use App\Libraries\NetPay;
use App\Libraries\NetPayCash;

class Autenticacion extends Model {
	protected $table      = 'usuarios';
	protected $primaryKey = 'id';

	protected $useAutoIncrement = true;

	protected $returnType     = 'array';
	protected $useSoftDeletes = true;

	protected $allowedFields = ['autenticador', 
								'id_cliente',
								'nombre',
								'apellidos',
								'correo',
								'contrasena',
								'cuenta',
								'rol',
								'estatus_registro',
								'imagen',
								'nacimiento',
								'contacto',
								'direccion',
								'historial',
								'experiencia',
								'idiomas',
								'areas',
								'plan',
								'pagoHora',
								'aprobado',
								'ult_conexion',
								'cv',
								'portafolio',
								'ocultar',
								'eliminado',
								'deleted_at'];

	protected $useTimestamps = true;
	protected $createdField  = 'created_at';
	protected $updatedField  = 'updated_at';
	protected $deletedField  = 'deleted_at';
	
	protected $validationRules    = [];
	protected $validationMessages = [];
	protected $skipValidation     = false;

	public function get_usuarioID($id){
	    $id = sanitizar($id);
		return db_connect()->table('usuarios')->where('id', $id)->get()->getRowArray();
	}

	public function get_id($id){
	    $id = sanitizar($id);
		$usuario = $this->get_usuarioID($id);

		if(!empty($usuario['direccion']) || 
			!empty($usuario['contacto']) || 
			!empty($usuario['experiencia']) || 
			!empty($usuario['idiomas']) ||
			!empty($usuario['historial'])){

			$direccion_decode = json_decode($usuario['direccion'], true);
			$direccion_decode != null ? $direccion = ['direccion' => $direccion_decode[0]] : $direccion = [];
			$usuario = array_replace($usuario, $direccion);
			
			$contacto_decode = json_decode($usuario['contacto'], true);
			$contacto_decode != null ? $contacto = ['contacto' => $contacto_decode] : $contacto = [];
			$usuario = array_replace($usuario, $contacto);

			$experiencia_decode = json_decode($usuario['experiencia'], true);
			$experiencia_decode != null ? $experiencia = ['experiencia' => $experiencia_decode] : $experiencia = [];
			$usuario = array_replace($usuario, $experiencia);

			$idiomas_decode = json_decode($usuario['idiomas'], true);
			$idiomas_decode != null ? $idiomas = ['idiomas' => $idiomas_decode] : $idiomas = [];
			$usuario = array_replace($usuario, $idiomas);

			$areas_decode = json_decode($usuario['areas'], true);
			$areas_decode != null ? $areas = ['areas' => $areas_decode] : $areas = [];
			$usuario = array_replace($usuario, $areas);
		}
		return $usuario;
	}

	public function crearClienteNetpay(array $usuario){
		$cliente = [
			'firstName' => $usuario['nombre_netpay'],
			'lastName' => $usuario['apellidos_netpay'],
			'phone' => str_replace(' ', '', $usuario['contacto']['telefono']),
			'email' => $usuario['correo'],
			'identifier' => 'cliente-'.strtotime(date('d-m-Y H:i:s')).'-'.$usuario['id'],
			'paymentSource' => [
				'source' => $usuario['token'],
				'type' => 'card'
			]
		];
		$netpay = new NetPay();
		$cliente_creado = $netpay->crearCliente($cliente);
		if(!isset($cliente_creado['id'])) return json_encode(['alerta' => 'falla', 'mensaje' => 'No se pudo crear cliente en NetPay: '.$cliente_creado['message']], JSON_FORCE_OBJECT);

		$this->save(['id' => session('id'), 'id_cliente' => $cliente_creado['id']]);
		return json_encode(['alerta' => 'correcto', 'id_cliente' => $cliente_creado['id']], JSON_FORCE_OBJECT);
	}

	public function eliminar_cuenta(){
		return $this->delete(session('id'));
	}

	public function purgar_cuenta($id){
		$db = db_connect();
		return $db->query("DELETE FROM $this->table WHERE id = $id");
	}

	public function recuperar_cuenta(){
		return $this->save(['id' => session('id'), 'deleted_at' => null]);
	}

	public function guardarDireccion(){
		$request = \Config\Services::request();
		$direccion['calle'] = sanitizar($request->getPost('calle'));
		$direccion['colonia'] = sanitizar($request->getPost('colonia'));
		$direccion['municipio'] = sanitizar($request->getPost('municipio'));
		$direccion['ciudad'] = sanitizar($request->getPost('ciudad'));
		$direccion['estado'] = sanitizar($request->getPost('estado'));
		$direccion['pais'] = 'México'; // sanitizar($request->getPost('pais'));
		$direccion['codigo_postal'] = sanitizar($request->getPost('codigo_postal'));
		$direccion['curp'] = sanitizar($request->getPost('curp'));
		$direccion['rfc'] = sanitizar($request->getPost('rfc'));
		
		if(empty($direccion['calle'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Veo que estás editando tus datos pero falta tu calle.'], JSON_FORCE_OBJECT);
		if(empty($direccion['colonia'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Veo que estás editando tus datos pero falta tu colonia'], JSON_FORCE_OBJECT);
		if(empty($direccion['municipio'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Veo que estás editando tus datos pero falta tu municipio'], JSON_FORCE_OBJECT);
// 		if(empty($direccion['ciudad'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'Ingresa tu ciudad'], JSON_FORCE_OBJECT);
		if(empty($direccion['estado'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Veo que estás editando tus datos pero falta tu estado.'], JSON_FORCE_OBJECT);
// 		if(empty($direccion['pais'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Veo que estas editando tus datos pero falta tu país.'], JSON_FORCE_OBJECT);
		if(empty($direccion['codigo_postal'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Veo que estás editando tus datos pero falta tu código postal.'], JSON_FORCE_OBJECT);
// 		if(empty($direccion['curp'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'ALTO... Veo que estás editando tus datos pero falta tu CURP.'], JSON_FORCE_OBJECT);
// 		if(empty($direccion['rfc'])) return json_encode(['tipo' => 'custom', 'mensaje' => 'Ingresa tu RFC'], JSON_FORCE_OBJECT);
		
		$direccion_info = array();
		array_push($direccion_info, $direccion);

		return $this->save(['id' => session('id'), 'direccion' => json_encode($direccion_info, JSON_FORCE_OBJECT)]);
	}

	public function editarPerfil(){
		$request = \Config\Services::request();
		$this->guardarDireccion();
		$contacto = [
			'telefono' => sanitizar($request->getPost('telefono')),
			'facebook' => sanitizar($request->getPost('facebook')),
			'instagram' => sanitizar($request->getPost('instagram')),
			'whatsapp' => sanitizar($request->getPost('whatsapp')),
		];
		$nombre = sanitizar($request->getPost('nombre'));
		$apellidos = sanitizar($request->getPost('apellidos'));
		if(empty($nombre) || empty($apellidos)) return json_encode(['alerta' => 'custom', 'mensaje' => 'ALTO... Veo que estás editando tus datos pero falta tu nombre completo.'], JSON_FORCE_OBJECT);

		$usuario = $this->select('id, id_cliente, correo')->find(session('id'));
		if(!empty($usuario['id_cliente'])){
			// CAMBIAMOS DATOS EN NETPAY
			$netpay = new NetPay();
			$cliente_guardado = $netpay->editarCliente($usuario['id_cliente'], [
				'firstName' => $nombre,
				'lastName' => $apellidos,
				'phone' => empty($contacto['telefono']) ? '0000000000' : str_replace(' ', '', $contacto['telefono']),
				'email' => $usuario['correo']
			]);
			if(!isset($cliente_guardado['id'])) return json_encode(['alerta' => 'falla', 'mensaje' => 'UPS! no pude actualizar tu información: '.$cliente_guardado['message']], JSON_FORCE_OBJECT);
		}

		$perfil = [
			'id' => session('id'),
			'nombre' => $nombre,
			'apellidos' => $apellidos,
			// 'imagen' => $imagen,
			'nacimiento' => !empty(sanitizar($request->getPost('nacimiento'))) ? sanitizar($request->getPost('nacimiento')) : null,
			'contacto' => json_encode($contacto),
		];
		$this->save($perfil);
		session()->remove(['usuario']);
		session()->set(['usuario' => $nombre.' '.$apellidos]);
		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Listo... actualicé tu perfil correctamente.'], JSON_FORCE_OBJECT);
	}
	public function editarPerfilFreelancer(){
		$request = \Config\Services::request();
		$this->guardarDireccion();
		$contacto = [
			'telefono' => sanitizar($request->getPost('telefono')),
			'facebook' => sanitizar($request->getPost('facebook')),
			'instagram' => sanitizar($request->getPost('instagram')),
			'whatsapp' => sanitizar($request->getPost('whatsapp')),
			'correo' => sanitizar($request->getPost('correo')),
			'whatsapp_visible' => sanitizar($request->getPost('check_whatsapp'))
		];
		$nombre = sanitizar($request->getPost('nombre'));
		$apellidos = sanitizar($request->getPost('apellidos'));
		if(empty($nombre) || empty($apellidos)) return json_encode(['alerta' => 'custom', 'mensaje' => 'ALTO... Veo que estás editando tus datos pero falta tu nombre completo.'], JSON_FORCE_OBJECT);

		$cv = $request->getFile('curriculum');
		if(!empty($cv)){
		    $tempfile = $cv->getTempName();
    		// se verifica si va ver cambio de imagen o no
    		if(empty($tempfile)){
    			$micv = $this->get_id(session('id'));
    			$img = $micv['cv'];
    			if(!empty($img)){
    				$cv = $img;
    			}else{
    				$cv = NULL;
    			}
    		}else{
    			$micv = $this->get_id(session('id'));
    			$img = $micv['cv'];
    			if(!empty($img)) unlink('./'.$img);
    			$nameimg = $_FILES['curriculum']['name'];//Se guarda el nombre original
    			$path = $request->getFile('curriculum')->store('cvs/'.session('id').'/', $nameimg);//Se guarda la imagen y se evita que se le asigne un nombre random
    			$ruta = '/writable/uploads/cvs/'.session('id').'/'.$nameimg;
    			$cv = $ruta;
    		}
		}
		$porta = $request->getFile('portafolio');
		if(!empty($porta)){
		    $tempport = $porta->getTempName();
    		$portafolio = '';
    		if(empty($tempport)){
    			$miport = $this->get_id(session('id'));
    			if(!empty($pt)){
    				$portafolio = $pt;
    			}else{
    				$portafolio == NULL;
    			}
    		}else{
    			$miport = $this->get_id(session('id'));
    			$pt = $miport['portafolio'];
    			if(!empty($pt)) unlink('./'.$pt);
    			$nameport = $_FILES['portafolio']['name'];
    			$path = $request->getFile('portafolio')->store('portafolio/'.session('id').'/', $nameport);
    			$rut = '/writable/uploads/portafolio/'.session('id').'/'.$nameport;
    			$portafolio = $rut;
    		}
		}

		$usuario = $this->select('id, id_cliente, correo')->find(session('id'));
		if(!empty($usuario['id_cliente'])){
			// CAMBIAMOS DATOS EN NETPAY
			$netpay = new NetPay();
			$cliente_guardado = $netpay->editarCliente($usuario['id_cliente'], [
				'firstName' => $nombre,
				'lastName' => $apellidos,
				'phone' => empty($contacto['telefono']) ? '0000000000' : str_replace(' ', '', $contacto['telefono']),
				'email' => $usuario['correo']
			]);
			if(!isset($cliente_guardado['id'])) return json_encode(['alerta' => 'falla', 'mensaje' => 'UPS! no pude actualizar tu información: '.$cliente_guardado['message']], JSON_FORCE_OBJECT);
		}
		
		$data = [
			'id' => session('id'),
			'nombre' => $nombre,
			'apellidos' => $apellidos,
			'nacimiento' => !empty(sanitizar($request->getPost('nacimiento'))) ? sanitizar($request->getPost('nacimiento')) : null,
			'contacto' => json_encode($contacto),
			'cv' => empty($cv) ? null : $cv,
			'portafolio' => empty($porta) ? null : $portafolio
		];
		$this->save($data);
		session()->remove('usuario');
		session()->set('usuario', $nombre.' '.$apellidos);
		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Listo... actualicé tu perfil correctamente.'], JSON_FORCE_OBJECT);
	}

	public function eliminarCliente(){
		$openpay = $this->objectOpenpay();
		$usuario = $this->get_usuarioID(session('id'));
		$cliente = $openpay->customers->get($usuario['id_openpay']);
		return $cliente->delete();
	}

	public function get_cargoID(string $id_transaccion){
		$netpay = new NetpayCash();
		return $netpay->getTransaccion($id_transaccion);
	}

	public function get_tarjeta_idUsuario_suscripciones($id_usuario, $token){
		$netpay = new NetPay();
        return $netpay->getTarjetaCliente($id_usuario, $token);
	}

	public function get_tarjetas_usuario_ID($id_usuario){
		$id_usuario = sanitizar($id_usuario);
		$usuarioBD = $this->get_usuarioID($id_usuario);
        if(empty($usuarioBD['id_cliente'])) return null;
        
		$netpay = new NetPay();
        $tarjetas = $netpay->getTarjetasCliente($usuarioBD['id_cliente']);
        if(isset($tarjetas['message'])) return null;

		return $tarjetas;
	}
	
	public function get_tarjeta_user($token_tarjeta){
		$usuario = $this->get_usuarioID(session('id'));
		if(empty($usuario['id_cliente'])) return null;

		$netpay = new Netpay();
		return $netpay->getTarjetaCliente($usuario['id_cliente'], $token_tarjeta);
	}

	public function guardarTarjetas(){
		$usuarioBD = $this->get_id(session('id'));
		if(empty($usuarioBD['direccion'])) return json_encode(['alerta' => 'custom', 'mensaje' => 'Registra todos los datos de dirección para facturación'], JSON_FORCE_OBJECT);
		unset($usuarioBD['direccion']['rfc']);
		unset($usuarioBD['direccion']['ciudad']);
		unset($usuarioBD['direccion']['curp']);
		$direccion_vacia = array_diff_key($usuarioBD['direccion'], array_filter($usuarioBD['direccion']));
		if(count($direccion_vacia) > 0){
			$mensaje = '';
			
			for($i = 1; $i <= count($direccion_vacia); $i++)
				($i == count($direccion_vacia)) ? $mensaje .= ucfirst(array_keys($direccion_vacia)[$i-1]).'.' : $mensaje .= ucfirst(array_keys($direccion_vacia)[$i-1]).', ';

			return json_encode(['alerta' => 'custom', 'mensaje' => 'Registra los datos de dirección faltantes para continuar: '.$mensaje], JSON_FORCE_OBJECT);
		}

		$netpay = new NetPay();
		$request = \Config\Services::request();

		$nombre = explode(' ', sanitizar($request->getPost('nombre')));
		
		$usuarioBD['nombre_netpay'] = $nombre[0];
		$usuarioBD['apellidos_netpay'] = $nombre[1];
		$usuarioBD['token'] = sanitizar($request->getPost('token_id'));
		
		if(empty($usuarioBD['id_cliente'])) {
		    $cliente_guardado = $this->crearClienteNetpay($usuarioBD);
		    $respuesta = json_decode($cliente_guardado, true);
		    if($respuesta['alerta'] != 'correcto') return $cliente_guardado;
		    // $usuarioBD['id_cliente'] = $respuesta['id_cliente'];
		}else{
			$netpay->crearTarjetaCliente($usuarioBD['id_cliente'], [
				'token' => $usuarioBD['token'],
				'preAuth' => false,
				'cvv2' => sanitizar($request->getPost('cvv'))
			]);
		}
		// dd($usuarioBD['token']);
		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Listo... Ya tengo registrado otro método de pago para tu cuenta.', 'tarjeta' => $usuarioBD['token']], JSON_FORCE_OBJECT);
	}

	public function eliminarTarjeta(string $id_usuario, string $token_card){
		$usuarioBD = $this->get_usuarioID($id_usuario);

		$netpay = new NetPay();
		$tarjeta_eliminada = $netpay->eliminarTarjetaCliente($usuarioBD['id_cliente'], $token_card);
		return isset($tarjeta_eliminada['responseMsg']);
	}

	public function urlImgBanco($brand){
		if($brand == 'visa') return base_url('assets/images/iconos/visa.ico');
		if($brand == 'visa_electron') return base_url('assets/images/visa_electron.ico');
		if($brand == 'mastercard') return base_url('assets/images/iconos/mastercard.ico');
		if($brand == 'amex') return base_url('assets/images/iconos/amex.ico');
		if($brand == 'diners_cb') return base_url('assets/images/iconos/diners_cb.ico');
		if($brand == 'diners_int') return base_url('assets/images/iconos/diners_int.ico');
		if($brand == 'jcb') return base_url('assets/images/iconos/jcb.ico');
		if($brand == 'discover') return base_url('assets/images/iconos/discover.ico');
		if($brand == 'carnet') return base_url('assets/images/iconos/Carnet.png');
	}

	public function guardarExperiencia(){
		$request = \Config\Services::request();
		$usuario = $this->get_usuarioID(session('id'));
		$experiencia_info = array();
		$titulo = sanitizar($request->getPost('titulo'));
		if($titulo == '') return json_encode(['alerta' => 'custom', 'mensaje' => 'Heey... No olvides el título, recuerda que es muy importante ya que es lo primero que ven tus futuros clientes.'], JSON_FORCE_OBJECT);
		if(sanitizar($request->getPost('fechaInicio')) == '') return json_encode(['alerta' => 'custom', 'mensaje' => 'HEEY... Es importante saber cuando inició este proyecto, ingresa la fecha de inicio.'], JSON_FORCE_OBJECT);
		if(sanitizar($request->getPost('fechaFin')) == '') return json_encode(['alerta' => 'custom', 'mensaje' => 'HEEY... Es importante saber cuando inició este proyecto, ingresa la fecha de inicio.'], JSON_FORCE_OBJECT);
		if(sanitizar($request->getPost('descripcion')) == '') return json_encode(['alerta' => 'custom', 'mensaje' => 'HEEY... Se te olvida lo más importante, el campo de descripción es donde puedes presumir tu experiencia en el proyecto.'], JSON_FORCE_OBJECT);
		$experiencia = [
			'titulo' => $titulo,
			'inicio' => sanitizar($request->getPost('fechaInicio')),
			'fin' => sanitizar($request->getPost('fechaFin')),
			'descripcion' => sanitizar($request->getPost('descripcion'))
		];
		array_push($experiencia_info, $experiencia);
		
		$experiencias_guardadas = json_decode($usuario['experiencia'], true);
		
		if(empty($experiencias_guardadas)) $experiencias_guardadas = $experiencia_info;
		else{
			foreach ($experiencias_guardadas as $key => $exp) {
				if($exp['titulo'] == $titulo) return json_encode(['alerta' => 'advertencia', 'mensaje' => 'OPPS... Creo que ya tienes una experiencia con este título, asegurate de no repetir.'], JSON_FORCE_OBJECT);
			}
			array_push($experiencias_guardadas, $experiencia_info[0]);
		} 

		count($experiencias_guardadas) == 1? $movimientoCarrusel = 'active' : $movimientoCarrusel = '';
		$datos = ['id' => session('id'), 'experiencia' => json_encode($experiencias_guardadas, JSON_FORCE_OBJECT)];
		
		if(!empty($request->getPost('registrando'))) $datos['estatus_registro'] = 'completado';
		$this->save($datos);
		
		$usuario = $this->get_usuarioID(session('id'));
		$experiencias = json_decode($usuario['experiencia'], true);
		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Listo... Esta experiencia ya la agregé a tu perfil, me aseguraré de mostrarlo a tus próximos clientes.', 'datos' => array_pop($experiencias_guardadas), 'movimiento' => $movimientoCarrusel, 'posicion' => array_key_last($experiencias)], JSON_FORCE_OBJECT);
	}

	public function editarExperiencia(){
		$request = \Config\Services::request();
		$usuario = $this->get_usuarioID(session('id'));

		$experiencias_guardadas = json_decode($usuario['experiencia']);
		$posicion = sanitizar($request->getPost('experiencia'));
		
		$experiencias_guardadas->$posicion->descripcion = sanitizar($request->getPost('descripcion'));

		$this->save(['id' => session('id'), 'experiencia' => json_encode($experiencias_guardadas, JSON_FORCE_OBJECT)]);
		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Listo... Actualicé toda la información, ahora se la presentare a tus clientes.', 'datos' => $experiencias_guardadas->$posicion], JSON_FORCE_OBJECT);
	}

	public function eliminarExperiencia(){
		$request = \Config\Services::request();
		$usuario = $this->get_usuarioID(session('id'));

		$experienciasBD = json_decode($usuario['experiencia']);
		$posicion = sanitizar($request->getPost('id'));

		unset($experienciasBD->$posicion);

		if(count((array)$experienciasBD) == 0) $this->save(['id' => session('id'), 'experiencia' => null]);
		else $this->save(['id' => session('id'), 'experiencia' => json_encode($experienciasBD, JSON_FORCE_OBJECT)]);

		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Listo... Eliminé la experiencia, te recomiendo agregar una nueva, por qué tus clientes es algo que buscan.'], JSON_FORCE_OBJECT);
	}

	public function guardarPagoHora(){
		$request = \Config\Services::request();

		$this->save(['id' => session('id'), 'pagoHora' => sanitizar($request->getPost('pagoHora'))]);
		return json_encode(['alerta' => 'correcto','mensaje' => 'Listo... Recordaré tu nuevo costo por hora.'], JSON_FORCE_OBJECT);
	}

	public function guardarIdiomas(){
		$request = \Config\Services::request();

		$idioma = sanitizar($request->getPost('idioma'));
		if($idioma == 'default') return json_encode(['alerta' => 'custom', 'mensaje' => 'HEEY... Elige un idioma.'], JSON_FORCE_OBJECT);

		$usuario = $this->get_usuarioID(session('id'));
		$idiomas_info = array();
		$idiomas = [
			'idioma' => $idioma,
			'rango' => sanitizar($request->getPost('rango')),
			'nivel' => sanitizar($request->getPost('nivel')),
			'imagen' => $this->get_bandera($idioma)
		];
		array_push($idiomas_info, $idiomas);

		$idiomasBD = json_decode($usuario['idiomas'], true);
		if(!empty($idiomasBD)){
			foreach ($idiomasBD as $key => $idiomaBD) {
				if($idiomaBD['idioma'] == $idioma) return json_encode(['alerta' => 'custom', 'mensaje' => 'OPPS... Este idioma ya lo habías agregado.'], JSON_FORCE_OBJECT);
			}
			array_push($idiomasBD, $idiomas_info[0]);
		} 
		else $idiomasBD = $idiomas_info;

		$this->save(['id' => session('id'), 'idiomas' => json_encode($idiomasBD, JSON_FORCE_OBJECT)]);
		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Guardado exitosamente', 'datos' => $idiomas, 'id' => $idiomasBD], JSON_FORCE_OBJECT);
	}

	public function editarIdioma(){
		$request = \Config\Services::request();
// 		$idioma = sanitizar($request->getPost('modal_idioma'));
// 		if($idioma  == 'default') return json_encode(['alerta' => 'custom', 'mensaje' => 'HEEY... Elige un idioma.'], JSON_FORCE_OBJECT);

		$usuario = $this->get_usuarioID(session('id'));
		$idiomasBD = json_decode($usuario['idiomas']);

		$posicion = $request->getPost('id');
// 		$idiomasBD->$posicion->idioma = $idioma;
		$idiomasBD->$posicion->rango = sanitizar($request->getPost('rango'));
		$idiomasBD->$posicion->nivel = sanitizar($request->getPost('nivel'));
// 		$idiomasBD->$posicion->imagen = $this->get_bandera($idioma);

		$this->save(['id' => session('id'), 'idiomas' => json_encode($idiomasBD, JSON_FORCE_OBJECT)]);
		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Actualizado', 'datos' => $idiomasBD->$posicion, 'nivel' => $idiomasBD->$posicion->nivel, 'rango' => $idiomasBD->$posicion->rango], JSON_FORCE_OBJECT);
	}

	public function eliminarIdioma(){
		$request = \Config\Services::request();
		$usuario = $this->get_usuarioID(session('id'));
		
		$posicion = $request->getGet('id');
		$idiomasBD = json_decode($usuario['idiomas']);
		unset($idiomasBD->$posicion);

		if(count((array)$idiomasBD) == 0) $this->save(['id' => session('id'), 'idiomas' => null]);
		else $this->save(['id' => session('id'), 'idiomas' => json_encode($idiomasBD, JSON_FORCE_OBJECT)]);

		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Listo... Ya eliminé el idioma.'], JSON_FORCE_OBJECT);
	}

	public function guardarHistorial(){
		$request = \Config\Services::request();
		$historial = sanitizar($request->getPost('historial'));
		if(empty($historial)) return json_encode(['alerta' => 'custom', 'mensaje' => 'HEEY... Se te olvida llenar el campo de historial, pero no te preocupes estoy aquí para ayudarte'], JSON_FORCE_OBJECT);
		
		$this->save(['id' => session('id'), 'historial' => $historial]);
		return json_encode(['alerta' => 'correcto', 'mensaje' => 'Listo... Actualicé toda la información, ahora se la presentar a tus clientes.'], JSON_FORCE_OBJECT);
	}

	public function get_tarjetaId($id_cliente, $id_tarjeta){
		$openpay = $this->objectOpenpay();
		$cliente = $openpay->customers->get($id_cliente);
		return $cliente->cards->get($id_tarjeta);
	}

	public function guardarAreas(){
		$request = \Config\Services::request();

		$area = sanitizar($request->getPost('area'));
		if($area == '') return json_encode(['alerta' => 'custom', 'mensaje' => 'ALTO... Requiero saber cual es tu especialidad, agrega una habilidad, te aseguro que será más fácil que tus clientes te encuentren.', 'datos' => $area], JSON_FORCE_OBJECT);
		
		$area_subarea = explode(' - ', $area); //$area_subarea[0] = habilidad $area_subarea[1] = area
		$usuario = $this->get_usuarioID(session('id'));
		$areasBD = json_decode($usuario['areas']);

		$areas_info = array();
		$subareas_info = array();

		$area_encontrada = false;
		array_push($subareas_info, $area_subarea[0]);
		if(empty($areasBD)){
			array_push($areas_info, ['area' => $area_subarea[1], 'subarea' => json_encode($subareas_info, JSON_UNESCAPED_UNICODE)]);
			$areasBD = $areas_info;
		}
		else{
			foreach ($areasBD as $key => $areaBD) {
				if($areaBD->area == $area_subarea[1]){
					$subarea_decode = json_decode($areaBD->subarea, true);
					if(in_array($area_subarea[0], $subarea_decode)) return json_encode(['alerta' => 'custom', 'mensaje' => 'ESPERA... la habilidad "'.$area_subarea[0].'" la agregaste anteriormente, intenta con otra'], JSON_FORCE_OBJECT);
					
					array_push($subarea_decode, $area_subarea[0]);
					
					$areaBD->subarea = json_encode($subarea_decode, JSON_UNESCAPED_UNICODE);
					$area_encontrada = true;
					break;
				}
			}
			if(!$area_encontrada){
				$areasBD = (array) $areasBD;
				$sub = str_replace('\/', '/', stripslashes(json_encode($subareas_info, JSON_UNESCAPED_UNICODE)));
				array_push($areasBD, ['area' => $area_subarea[1], 'subarea' => $sub]);
			}
		}
		$datos = ['id' => session('id'), 'areas' => json_encode($areasBD, JSON_UNESCAPED_UNICODE)];
		if(!empty($request->getPost('registrando'))) $datos['estatus_registro'] = 2;

		$this->save($datos);
		return json_encode(['alerta' => 'correcto','mensaje' => 'Listo... Ya dí de alta esta área, con esto puedo encontrar más clientes para tí.', 'datos' => $areasBD], JSON_FORCE_OBJECT);
	}

	public function Subareas(){
		$request = \Config\Services::request();

		$area = sanitizar($request->getPost('area'));
		$usuario = $this->get_usuarioID(session('id'));
		$areas_decode = json_decode($usuario['areas'], true);
		foreach ($areas_decode as $key => $areas) {
			if($areas['area'] == $area) return $areas['subarea'];
		}
		return [];
	}

	public function eliminarArea(){
		$request = \Config\Services::request();
		$usuario = $this->get_usuarioID(session('id'));
		$areasBD = json_decode($usuario['areas']);

		$area = sanitizar($request->getPost('area'));
		foreach ($areasBD as $key => $areaBD) {
			if($areaBD->area == $area){
				unset($areasBD[$key]);
				break;
			}
		}
		$this->save(['id' => session('id'), 'areas' => (!(array)$areasBD)? null : json_encode($areasBD, JSON_UNESCAPED_UNICODE)]);
		return json_encode(['alerta' => 'correcto','mensaje' => 'Listo... ya eliminé esta área.'], JSON_FORCE_OBJECT);
	}

	public function eliminarTodasAreas(){
		$usuario = $this->get_usuarioID(session('id'));
		return $this->where('id', session('id'))->set(['areas' => null])->update();
	}

	public function borrar_subarea(){
		$request = \Config\Services::request();
		$usuario = $this->get_usuarioID(session('id'));
		$areasBD = json_decode($usuario['areas']);

		$area = sanitizar($request->getPost('area'));
		$subarea = sanitizar($request->getPost('subarea'));
		$area_eliminada = false;
		foreach ($areasBD as $key => $areaBD) {
			if($areaBD->area == $area){
				$subarea_decode = json_decode($areaBD->subarea, true);
				
				$indice = array_search($subarea, $subarea_decode);
				if(!$indice && $indice != 0) return json_encode(['alerta' => 'falla', 'mensaje' => 'No es encontró la subarea, intente de nuevo'], JSON_UNESCAPED_UNICODE);
				
				// Se elimina la subarea
				unset($subarea_decode[$indice]);
				
				// Se elimina toda la area si la subarea es el ultimo elemento en el area
				if(empty($subarea_decode)){
					if(is_array($areasBD)) unset($areasBD[$key]);
					else unset($areasBD->{$key});
					$area_eliminada = true;
				}
				else $areaBD->subarea = json_encode($subarea_decode, JSON_UNESCAPED_UNICODE);
				break;
			}
		}
		$borrado = $this->save(['id' => session('id'), 'areas' => (!(array)$areasBD)? null : json_encode($areasBD, JSON_UNESCAPED_UNICODE)]);
		if($borrado) return json_encode(['alerta' => 'correcto','mensaje' => 'Eliminado', 'eliminar_area' => $area_eliminada], JSON_FORCE_OBJECT);

		return json_encode(['alerta' => 'falla','mensaje' => 'No se pudo eliminar', 'eliminar_area' => $area_eliminada], JSON_FORCE_OBJECT);
	}

	public function guardar_Perfil_1(){
		$request = \Config\Services::request();
		$telefono = sanitizar($request->getPost('telefono'));
		if(empty($telefono)) return json_encode(['tipo' => 'falla', 'mensaje' => 'Ingresa tu teléfono'], JSON_FORCE_OBJECT);
		$contacto = ['telefono' => sanitizar($request->getPost('telefono'))];

		$direccion_guardada = $this->guardarDireccion();
		$usuario = $this->get_usuarioID(session('id'));
		($usuario['rol'] == 'freelancer')? $estatus = 1 : $estatus = 'completado';
		$cambioPaso = $this->save(['id' => session('id'), 'estatus_registro' => $estatus, 'contacto' => json_encode($contacto)]);
		return $direccion_guardada && $cambioPaso;
	}

	public function get_bandera($idioma){
		$paises = json_decode(file_get_contents('paises.json'), true);
		foreach($paises as $key => $pais){
			if($pais['cca3'] == $idioma) return $pais['flags']['svg']; 
		}
	}

	public function get_plan(){
		$usuario = $this->get_usuarioID(session('id'));
		return $usuario['plan'];
	}

	public function get_estatus_perfil_ID($id){
	    // dd($id);
		$usuario = $this->get_usuarioID($id);
		// dd($usuario);
		return $usuario['estatus_registro'];
	}

	public function perfilCompleto($id){
		$usuario = $this->get_usuarioID($id);
		if($usuario['estatus_registro'] == 'completado') return route_to('perfil', 'panel');
		return $usuario['estatus_registro'];
	}
}