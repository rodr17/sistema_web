<?php namespace App\Models;

use CodeIgniter\Model;

class Cupones extends Model {
    protected $table      = 'cupones';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['codigo', 'limite', 'fecha_limite', 'tipo', 'descuento', 'tipo_plan', 'rol', 'activo'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}