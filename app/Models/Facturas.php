<?php namespace App\Models;

use CodeIgniter\Model;

class Facturas extends Model {
    protected $table      = 'solicitudes_facturas';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['id_compra', 'id_user', 'estatus', 'datos', 'facturas'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
    
    public function eliminar($id){
        $db = \Config\Database::connect();
        $query = $db->query('DELETE FROM solicitudes_facturas WHERE id = '.$id);
    }
}