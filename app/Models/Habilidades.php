<?php namespace App\Models;

use CodeIgniter\Model;

class Habilidades extends Model {
    protected $table      = 'habilidades';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['area', 'superior'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function get_subareas(){
        $request = \Config\Services::request();
        $area = $request->getPost('area');
        $habilidades = $this->builder();
        $area = $habilidades->where('superior', $area)->orderBy('area', 'ASC')->get()->getResultArray();
        return json_encode($area, JSON_FORCE_OBJECT);
    }
    public function eliminar($id){
        $db = \Config\Database::connect();
        $query = $db->query('DELETE FROM habilidades WHERE id = '.$id);
    }

    public function get_categoriasFiltro($cantidad){
        $cantidad = sanitizar($cantidad);
        $categorias = $this->where('superior', '0')->orderBy('area', 'RANDOM')->limit(4, $cantidad)->find();// opcion de filtrado por cada 4 categorias
        if(empty($categorias)) return '';

        $html = "";
        $html.= "<div class='row'>";
        foreach($categorias as $a => $categoria){
            dd($categoria);
            $html.= "<div class='col-3'>";
            $html.= "<div class='contenedor-buscado'>
                <a href='".base_url('/buscar-proyecto?search=&area='.view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-'.$a, 'valor' => 'categoria']))."' class='text-decoration-none'>
                    <img class='img-carrusel rounded-3 h-100' src='".view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-'.$a, 'valor' => 'imagen'])."'>
                    <p class='text-white position-relative text-truncate bottom-0 ms-3' style='margin-top: -30px; font-size: 16px;'><i class='fas fa-laptop-code d-none' aria-hidden='true' ></i> ".view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-'.$a, 'valor' => 'titulo'])."</p>
                </a>
            </div>";
            $html.= "</div>";
        }
        $html.= "</div>";
        dd($html);
        echo $html;
    }
}