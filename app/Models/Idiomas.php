<?php namespace App\Models;

use CodeIgniter\Model;

class Idiomas extends Model {
    protected $table      = 'idiomas';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['identificador', 'nombre', 'bandera'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
    
    public function eliminar($id){
        $db = \Config\Database::connect();
        $query = $db->query('DELETE FROM idiomas WHERE id = '.$id);
    }
}