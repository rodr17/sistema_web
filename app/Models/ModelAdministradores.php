<?php namespace App\Models;

use CodeIgniter\Model;

class ModelAdministradores extends Model {
    protected $table      = 'administradores';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['correo','contrasena','tipo','permisos','name','nombre','telefono','correorecuperacion','imagen'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
    
}