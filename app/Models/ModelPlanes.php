<?php namespace App\Models;

use CodeIgniter\Model;

class ModelPlanes extends Model {
    protected $table      = 'planes';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['name',
                                'descripcion',
                                'monto',
                                'monto_anual',
                                'trial_days',
                                'currency',
                                'id_plan_mensual',
                                'id_plan_anual',
                                'cuenta'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}