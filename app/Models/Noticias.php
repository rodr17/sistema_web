<?php namespace App\Models;

use CodeIgniter\Model;

class Noticias extends Model {
    protected $table      = 'noticias';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['titulo','autor','contenido','imagen'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
    
    public function eliminar($id){
        $db = \Config\Database::connect();
        $query = $db->query('DELETE FROM noticias WHERE id = '.$id);
    }
}