<?php namespace App\Models;

use DateTime;
use CodeIgniter\Model;
use CodeIgniter\I18n\Time;
use App\Libraries\NetPay;
use App\Controllers\Pagos as PagoController;

class Pagos extends Model {
    protected $table      = 'pagos';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['id_usuario','id_compra','id_membresia','monto','fecha','estatus','tipo'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function crear(string $id_usuario, string $id_tarjeta, string $id_plan = ''){
        $request = \Config\Services::request();

        $deviceFingerPrint = sanitizar($request->getPost('deviceFingerPrint'));
        if(empty($deviceFingerPrint)) return ['message' => 'DeviceFingerPrint vacío'];
        
        $cvv = sanitizar($request->getPost('cvv'));
        if(empty($cvv)) return ['message' => 'Código seguridad de tarjeta vacío'];

        $id_referencia = sanitizar($request->getPost('referencia_cargo'));
        if(empty($id_referencia)) return ['message' => 'Referencia vacía'];

        $usuario = model('Autenticacion')->get_id($id_usuario);
        $plan_id = empty($id_plan) ? $usuario['plan'] : $id_plan;
        
		// SE VERIFICA PLAN MENSUAL O ANUAL BBDD
		$planBD = model('ModelPlanes')->where('id_plan_mensual', $plan_id)->first();
		$datos_plan['monto'] = isset($planBD['monto']) ? $planBD['monto'] : '';
		$datos_plan['id_plan'] = isset($planBD['id_plan_mensual']) ? $planBD['id_plan_mensual'] : '';
		
		if(empty($planBD)){
			$planBD = model('ModelPlanes')->where('id_plan_anual', $plan_id)->first();
			$datos_plan['monto'] = $planBD['monto_anual'];
			$datos_plan['id_plan'] = $planBD['id_plan_anual'];
		}

		// SE GENERA AJUSTE DIFERENCIAL DE MONTO AL DÍA DE FACTURACION (01) MENSUAL
		if(isset($planBD['monto'])){
			$hoy = new DateTime();
            $hoy->modify('last day of this month');
            $dias_restantes = ($hoy->format('d') - date('d')) == 0 ? 1 : ($hoy->format('d') - date('d'));
			$datos_plan['monto'] = round(($planBD['monto']/30) * $dias_restantes);
		}

		// VALIDACION PARA CUPONES DE PAGA EN CHECKOUT
		$cupon_utilizado = false;
		$cupon_checkout = trim($request->getPost('cupon'));
		if(!empty($cupon_checkout)){
			$pagos = new PagoController();
			$cuponBD = $pagos->validacion_cupon($usuario['id'], $cupon_checkout);
			if(isset($cuponBD['mensaje'])) return ['message' => $cuponBD['mensaje']];

			if($cuponBD['tipo'] != 'Paga') return ['message' => 'OPPS... Lo siento, no puedes aplicar este cupón para un descuento en tu compra'];
			
			$datos_plan['monto'] = ((100 - $cuponBD['descuento']) * $datos_plan['monto']) / 100;
			$cupon_utilizado = true;
			$datos_cupon = ['id_usuario' => $usuario['id'], 'id_cupon' => $cuponBD['id'], 'canjeado' => false];
			$id_uso_cupon = model('UsosCupones')->insert($datos_cupon);
		}

		$datos_cobro = [
			'transactionType' => 'Auth',
			'amount' => $datos_plan['monto'],
			'source' => $id_tarjeta,
			'description' => 'Primer cobro del usuario para suscripción',
			'paymentMethod' => 'card',
			'sessionId' => $deviceFingerPrint,
			'deviceFingerPrint' => $deviceFingerPrint,
			'currency' => 'MXN',
			'redirect3dsUri' => $usuario['estatus_registro'] != 'completado' ? base_url('Llenar_perfil') : base_url('usuarios/perfil'),
			'client' => ['id' => $usuario['id_cliente']],
			'cvv' => $cvv,
			'saveCard' => true,
			'referenceID' => $id_referencia,
		];

		$datos_cobro['billing'] = [
			'firstName' => $usuario['nombre'],
			'lastName' => $usuario['apellidos'],
			'email' => $usuario['correo'],
			'phone' => $usuario['contacto']['telefono']
		];

		$datos_cobro['billing']['address'] = [
			'city' => $usuario['direccion']['municipio'],
			'country' => 'MX',
			'postalCode' => $usuario['direccion']['codigo_postal'],
			'state' => $usuario['direccion']['estado'],
			'street1' => $usuario['direccion']['calle']
		];
		
		$netpay = new Netpay();
		$pago = $netpay->crearCargo($datos_cobro);
		if(isset($pago['message']) || ($pago['status'] != 'success')) return $pago;
        
		model('Pagos_unicos')->save([
			'id_usuario' => $usuario['id'],
			'token_transaccion' => $pago['transactionTokenId'],
			'referencia' => $pago['billing']['merchantReferenceCode']
		]);

		$transaccion = $netpay->getTransaccion($pago['transactionTokenId']);

		// SE ACTUALIZA REFERENCIA DE PAGO CON EL CUPON DE PAGA EN CHECKOUT
		if($cupon_utilizado) model('UsosCupones')->save(['id' => $id_uso_cupon, 'id_compra' => $transaccion['orderId']]);
		
		model('Autenticacion')->save(['id' => session('id'), 'plan' => $datos_plan['id_plan']]);

		$this->insert([
			'id_usuario' => $usuario['id'],
			'id_compra' => $transaccion['orderId'],
			'id_membresia' => $datos_plan['id_plan'],
			'id_transacion' => $pago['transactionTokenId'],
			'monto' => $datos_plan['monto'],
			'fecha' => Time::now()->toDateTimeString(),
			'estatus' => 'Activo',
			'tipo' => 'tarjeta'
		]);

		$suscripcion_creada = model('Suscripciones')->crear_suscripcion($usuario['id'], $id_tarjeta, $datos_plan['id_plan']);
		
		if($suscripcion_creada['alerta'] != 'correcto') return ['message' => $suscripcion_creada['mensaje']];
		
		if(autorizacion() == 'contratante') model('Autenticacion')->save(['id' => session('id'), 'estatus_registro' => 'completado']);
    }
}