<?php namespace App\Models;

use CodeIgniter\Model;

class Postulaciones extends Model {
    protected $table      = 'postulaciones';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['id_proyecto',
                                'id_postulante',
                                'chat',
                                'sala'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    // Funcion para eliminar
    public function eliminar($id){
        $db = \Config\Database::connect();
        $query = $db->query('DELETE FROM postulaciones WHERE id = '.$id);
    }
}