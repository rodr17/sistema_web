<?php namespace App\Models;

use CodeIgniter\Model;

class Proyectos extends Model {
    protected $table      = 'proyectos';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['id_contratista',
                                'titulo',
                                'desc_corta',
                                'descripcion',
                                'categoria',
                                'subcategoria',
                                'nivel',
                                'destacado',
                                'fecha_arranque_esperado',
                                'fecha_entrega',
                                'presupuesto',
                                'id_freelancer',
                                'fecha_inicial',
                                'estatus',
                                'imagenes',
                                'tareas',
                                'deleted'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    // Funcion para eliminar
    public function eliminar($id){
        $db = \Config\Database::connect();
        $query = $db->query('DELETE FROM proyectos WHERE id = '.$id);
    }
}