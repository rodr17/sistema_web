<?php namespace App\Models;
use App\Controllers\Pagos as PagosController;
use App\Controllers\Suscripciones as _Suscripciones;
use App\Libraries\Correos;
use App\Libraries\NetPay;
use App\Libraries\NetPayCash;
use CodeIgniter\I18n\Time;
use CodeIgniter\Model;

class Suscripciones extends Model {
	protected $table      = 'suscripciones';
	protected $primaryKey = 'id';

	protected $useAutoIncrement = true;

	protected $returnType     = 'array';
	protected $useSoftDeletes = true;

	protected $allowedFields = ['id_usuario',
								'id_plan',
								'suscripcion',
								'id_referencia',
								'id_transaccion',
								'fecha_inicio',
								'fecha_fin',
								'estatus',
								'tipo_suscripcion',
								'renovacion',
								'fecha_cancelacion',
								'tarjeta_suscripcion',
								'cambio_pago'];

	protected $useTimestamps = true;
	protected $createdField  = 'created_at';
	protected $updatedField  = 'updated_at';
	protected $deletedField  = 'deleted_at';
	
	protected $validationRules    = [];
	protected $validationMessages = [];
	protected $skipValidation     = false;

	public function crear_suscripcion(string $id_usuario, string $id_tarjeta, string $id_plan, $cron = 'false'){
		if(empty($id_usuario)) return ['alerta' => 'falla', 'mensaje' => 'Proporciona el ID del usuario'];

		if(empty($id_usuario)) return ['alerta' => 'falla', 'mensaje' => 'Proporciona el ID de la tarjeta'];

		if(empty($id_plan)) return ['alerta' => 'falla', 'mensaje' => 'Proporciona el ID del plan'];
		

		$id_usuario == 'false' ?  $usuario = model('Autenticacion')->get_id(session('id')) : $usuario = model('Autenticacion')->get_id($id_usuario);
		if($cron == 'false'){
			if(_Suscripciones::esta_suscrito_por_idUsuario($id_usuario)){
				$suscripcion =  $this->where('id_usuario', $usuario['id'])->first();
				$planes = model('ModelPlanes')->where('cuenta', session('rol'))->find();
				
				$planactual = model('ModelPlanes')->where('id_plan_mensual', $suscripcion['id_plan'])->orWhere('id_plan_mensual', $suscripcion['id_plan'])->first();
				if($planactual == null) $planactual = model('ModelPlanes')->where('id_plan_anual', $suscripcion['id_plan'])->orWhere('id_plan_anual', $suscripcion['id_plan'])->first();
				
				$planselect = model('ModelPlanes')->where('id_plan_mensual', $id_plan)->orWhere('id_plan_mensual', $id_plan)->first();
				if($planselect == null) $planselect = model('ModelPlanes')->where('id_plan_anual', $id_plan)->orWhere('id_plan_anual', $id_plan)->first();
				$aux = array();
				foreach ($planes as $key => $row){
					$aux[$key] = $row['monto'];
				}
				array_multisort($aux, SORT_ASC, $planes);
				foreach($planes as $key => $p){
					if($p['id'] == $planactual['id']){
						$actual = $key;
					}
					if($p['id'] == $planselect['id']){
						$sel = $key;
					}
				}
				// dd(intval($actual).', '.intval($sel));
				if($actual > $sel){
					$datacambio = [
						'id_usuario' => session('id'),
						'id_plan' => $id_plan,
						'metodo' => 'tarjeta',
						'fecha_cambio' => date('Y-m-d', strtotime($suscripcion['fecha_fin'])),
						'identificador' => $id_tarjeta
					];
					model('CambioMembresia')->save($datacambio);
					return ['alerta' => 'correcto', 'mensaje' => 'Su suscripción cambiará al plan '.$planselect['name'].' el '.view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $suscripcion['fecha_fin']]).', hasta entonces contará con los beneficios de su membresía '.$planactual['name'].'.'];
				}
			}
		}
		
		$netpay = new NetPay();
		// SE VERIFICA PLAN MENSUAL O ANUAL BBDD
		$planBD = model('ModelPlanes')->where('id_plan_mensual', $id_plan)->first();
		$recurrencia = isset($planBD['monto']) ? '+ 1 month' : '';
		
		if(empty($planBD)){
			$planBD = model('ModelPlanes')->where('id_plan_anual', $id_plan)->first();
			$recurrencia = '+ 1 year';
		}
		$hoy = date('Y-m-d');
		$datosSuscripcion['billingStart'] = date('Y-m-d', strtotime($hoy.$recurrencia));
		
		$datosSuscripcion['client'] = [
			'id' => $usuario['id_cliente'],
			'paymentSource' => ['source' => $id_tarjeta]
		];

		$suscripcion = $netpay->crearSuscripcion($id_plan, $datosSuscripcion);
		if(!isset($suscripcion['id'])) return ['alerta' => 'falla', 'mensaje' => 'No se pudo crear la suscripción: '.$suscripcion['message']];
		
		// SE ACTUALIZA MONTO VARIABLE
		$monto_actualizado = $netpay->actualizarSuscripcion($suscripcion['id'], $suscripcion['plan']['amount']);
		if(!isset($monto_actualizado['amount'])) return ['alerta' => 'falla', 'mensaje' => 'No se pudo actualizar el monto: '.$monto_actualizado['message']];

		//Se verifica cambio de membresia
		if(($usuario['plan'] ?? '') != $id_plan) model('Autenticacion')->save(['id' => $usuario['id'], 'plan' => $id_plan]);
		
		$suscripcion_anterior = $this->where('id_usuario', $usuario['id'])->first();
		$datosBD = [
			'id' => !empty($suscripcion_anterior)? $suscripcion_anterior['id'] : null,
			'id_usuario' => $usuario['id'],
			'id_plan' => $suscripcion['plan']['id'],
			'suscripcion' => $suscripcion['id'],
			'id_referencia' => null,
			'estatus' => $suscripcion['status'],
			'renovacion' => true,
			'fecha_inicio' => conversorZonaHoraria($suscripcion['createdAt'], app_timezone(), date_default_timezone_get()),
			'fecha_fin' => $suscripcion['billingStart'],
			'fecha_cancelacion' => null,
			'tarjeta_suscripcion' => $suscripcion['cardToken']['token'],
			'tipo_suscripcion' => 'tarjeta',
			'cambio_pago' => null
		];

		if($this->save($datosBD)) return ['alerta' => 'correcto', 'mensaje' => '¡Enhorabuena! Ya tienes tu suscripción activa'];
		
		return ['alerta' => 'falla', 'mensaje' => 'No se pudo procesar la solicitud, inténtelo nuevamente.'];
	}

	public function crear_tienda($id = 'false', $id_usuario = 'false', $cron = 'false'){
		$request = \Config\Services::request();
		$id == 'false' ? $id_plan = $request->getPost('id_plan') : $id_plan = $id;
		$id_usuario == 'false' ?  $usuario = model('Autenticacion')->get_id(session('id')) : $usuario = model('Autenticacion')->get_id($id_usuario);
		
		if($cron == 'false'){
			if(_Suscripciones::esta_suscrito_por_idUsuario($id_usuario)){
				$suscripcion =  $this->where('id_usuario', $usuario['id'])->first();
				$planes = model('ModelPlanes')->where('cuenta', session('rol'))->find();
				
				$planactual = model('ModelPlanes')->where('id_plan_mensual', $suscripcion['id_plan'])->orWhere('id_plan_mensual', $suscripcion['id_plan'])->first();
				if($planactual == null) $planactual = model('ModelPlanes')->where('id_plan_anual', $suscripcion['id_plan'])->orWhere('id_plan_anual', $suscripcion['id_plan'])->first();
				
				$planselect = model('ModelPlanes')->where('id_plan_mensual', $id_plan)->orWhere('id_plan_mensual', $id_plan)->first();
				if($planselect == null) $planselect = model('ModelPlanes')->where('id_plan_anual', $id_plan)->orWhere('id_plan_anual', $id_plan)->first();
				$aux = array();
				foreach ($planes as $key => $row){
					$aux[$key] = $row['monto'];
				}
				array_multisort($aux, SORT_ASC, $planes);
				foreach($planes as $key => $p){
					if($p['id'] == $planactual['id']){
						$actual = $key;
					}
					if($p['id'] == $planselect['id']){
						$sel = $key;
					}
				}
				// dd(intval($actual).', '.intval($sel));
				if($actual > $sel){
					$datacambio = [
						'id_usuario' => $usuario['id'],
						'id_plan' => $id_plan,
						'metodo' => 'efectivo',
						'fecha_cambio' => d('Y-m-d', strtotime($suscripcion['fecha_fin'])),
						'identificador' => NULL
					];
					model('CambioMembresia')->save($datacambio);
					return json_encode(['alerta' => 'correcto', 'mensaje' => 'Su suscripción cambiará al plan '.$planselect['name'].' el '.view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $suscripcion['fecha_fin']]).', hasta entonces contará con los beneficios de su membresía '.$planactual['name'].'.'], JSON_FORCE_OBJECT);
				}
			}
		}

		// SE OBTIENE MONTO MENSUAL O ANUAL BBDD
		$planBD = model('ModelPlanes')->where('id_plan_mensual', $id_plan)->first();
		$monto_plan = isset($planBD['monto']) ? $planBD['monto'] : '';
		
		if(empty($planBD)){
			$planBD = model('ModelPlanes')->where('id_plan_anual', $id_plan)->first();
			$monto_plan = $planBD['monto_anual'];
		}
		
		// VALIDACION PARA CUPONES DE PAGA EN CHECKOUT
		$cupon_utilizado = false;
		$cupon_checkout = trim($request->getPost('cupon'));
		if(!empty($cupon_checkout)){
			$pagos = new PagosController();
			$cuponBD = $pagos->validacion_cupon($usuario['id'], $cupon_checkout);
			if(isset($cuponBD['mensaje'])) return json_encode(['alerta' => 'falla', 'mensaje' => $cuponBD['mensaje']], JSON_FORCE_OBJECT);

			if($cuponBD['tipo'] != 'Paga') return json_encode(['alerta' => 'falla', 'mensaje' => 'OPPS... Lo siento, no puedes aplicar este cupón para un descuento en tu compra'], JSON_FORCE_OBJECT);
			$monto_plan = ((100 - $cuponBD['descuento']) * $monto_plan) / 100;
			$cupon_utilizado = true;
			$id_uso_cupon = model('UsosCupones')->insert(['id_usuario' => $usuario['id'], 'id_cupon' => $cuponBD['id'], 'canjeado' => false]);
		}
		
		$datos_referencia_pago = [
			'amount' => $monto_plan,
			'description' => 'Compra de suscripción '.$planBD['name'].' '.$usuario['nombre'].' '.$usuario['apellidos'],
			'paymentMethod' => 'cash',
			'currency' => 'MXN',
			'billing' => [
				'firstName' => explode(' ', $usuario['nombre'])[0],
				'lastName' => explode(' ', $usuario['apellidos'])[0],
				'email' => $usuario['correo'],
				'phone' => str_replace(' ', '', $usuario['contacto']['telefono']),
				'merchantReferenceCode' => 'ORD-'.time().'-'.$usuario['id']
			],
			'expiryDate' => date('Y/m/d', strtotime(date('Y-m-d').'+ 2 days'))
		];
		$netpay = new NetPayCash();
		$pago_referencia = $netpay->crearReferencia($datos_referencia_pago);
		
		if(isset($pago_referencia['message'])) return json_encode(['alerta' => 'falla', 'mensaje' => 'No se pudo crear la suscripción: '.$pago_referencia['message']], JSON_FORCE_OBJECT);

		// SE ACTUALIZA REFERENCIA DE PAGO CON EL CUPON DE PAGA EN CHECKOUT
		if($cupon_utilizado) model('UsosCupones')->save(['id' => $id_uso_cupon, 'id_compra' => $pago_referencia['paymentSource']['cashDto']['reference']]);
		
		// SE ACTUALIZA EL USO DE CUPONES SI ESTÁ UTILIZANDOLO EN LA TRANSACCIÓN (SOLO PAGOS NO DIRECTOS)
		if(session('cupon') != ''){
			$cupon = model('Cupones')->where('codigo', session('cupon'))->first();
			model('UsosCupones')->where(['id_usuario' => $usuario['id'], 'id_cupon' => $cupon['id']])->set(['id_compra' => $pago_referencia['paymentSource']['cashDto']['reference']])->update();
		}
		
		// ID_COMPRA SE AGREGARÁ CUANDO SE HAYA PAGADO (WEBHOOK CEP.PAID)
		$datosPago = [
			'id_usuario' => $usuario['id'],
			'id_membresia' => $id_plan,
			'id_transacion' => $pago_referencia['transactionTokenId'],
			'monto' => $monto_plan,
			'fecha' => Time::now()->toDateTimeString(),
			'estatus' => 'En proceso',
			'tipo' => 'efectivo'
		];

		$correo = new Correos();
		$datos_referencia_pago['reference'] = $pago_referencia['paymentSource']['cashDto']['reference'];
		$correo->nuevaReferenciaPago($datos_referencia_pago);
		
		if(model('Pagos')->save($datosPago)) return $pago_referencia['paymentSource']['cashDto']['reference'];

		return false;
	}
	public function cancelar($id_suscripcion){
		$suscripcion_cancelar = $this->where('suscripcion', $id_suscripcion)->orWhere('id_transaccion', $id_suscripcion)->first();
		$fecha_actual = conversorZonaHoraria(date('Y-m-d H:i:s'), app_timezone(), date_default_timezone_get());
		$this->save(['id' => $suscripcion_cancelar['id'], 'estatus' => 'Cancelado', 'renovacion' => false, 'fecha_cancelacion' => $fecha_actual]);
		return $this->builder()
					->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
					->where('id_usuario', session('id'))
					->get()->getRowArray();
	}

	public function cancelarCambioPago($id_suscripcion){
		$suscripcion_cancelar = $this->where('suscripcion', $id_suscripcion)->orWhere('id_transaccion', $id_suscripcion)->first();
		return $this->save(['id' => $suscripcion_cancelar['id'], 'estatus' => 'Activo', 'renovacion' => true, 'fecha_cancelacion' => NULL, 'cambio_pago' => NULL]);
	}

	public function informacion_plan(string $id_plan){
		// SE OBTIENE PLAN MENSUAL O ANUAL
		$planBD = model('ModelPlanes')->where('id_plan_mensual', $id_plan)->first();
		$datos = [
			'monto' => isset($planBD['monto']) ? $planBD['monto'] : '',
			'frecuencia' => 'Mensual'
		];

		if(empty($planBD)){
			$planBD = model('ModelPlanes')->where('id_plan_anual', $id_plan)->first();
			$datos['monto'] = $planBD['monto_anual'];
			$datos['frecuencia'] = 'Anual';
		}
		
		return [
			'id_plan' => $id_plan,
			'suscripcion' => $planBD['name'],
			'precio' => $datos['monto'],
			'frecuencia' => $datos['frecuencia'],
			'descripcion' => $planBD['descripcion']
		];
	}
}