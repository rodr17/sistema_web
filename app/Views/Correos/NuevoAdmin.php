<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <!--[if (gte mso 9)|(IE)]>
        <xml>
        <o:OfficeDocumentSettings>
        <o:AllowPNG/>
        <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
        <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
        <meta name="format-detection" content="date=no"> <!-- disable auto date linking in iOS -->
        <meta name="format-detection" content="address=no"> <!-- disable auto address linking in iOS -->
        <meta name="format-detection" content="email=no"> <!-- disable auto email linking in iOS -->
        <title>Nuevo administrador</title>
        <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <style type="text/css">
            /*Basics*/
            body {
                margin: 0px !important;
                padding: 0px !important;
                display: block !important;
                min-width: 100% !important;
                width: 100% !important;
                -webkit-text-size-adjust: none;
            }
            table {
                border-spacing: 0;
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
            }
            table td {
                border-collapse: collapse;
                mso-line-height-rule: exactly;
            }
            td img {
                -ms-interpolation-mode: bicubic;
                width: auto;
                max-width: auto;
                height: auto;
                margin: auto;
                display: block !important;
                border: 0px;
            }
            td p {
                margin: 0;
                padding: 0;
            }
            td div {
                margin: 0;
                padding: 0;
            }
            td a {
                text-decoration: none;
                color: inherit;
            }
            /*Outlook*/
            .ExternalClass {
                width: 100%;
            }
            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: inherit;
            }
            .ReadMsgBody {
                width: 100%;
                background-color: #ffffff;
            }
            /* iOS BLUE LINKS */
            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
                font-size: inherit !important;
                font-family: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
            }
            /*Gmail blue links*/
            u+#body a {
                color: inherit;
                text-decoration: none;
                font-size: inherit;
                font-family: inherit;
                font-weight: inherit;
                line-height: inherit;
            }
            /*Buttons fix*/
            .undoreset a,
            .undoreset a:hover {
                text-decoration: none !important;
            }
            .yshortcuts a {
                border-bottom: none !important;
            }
            .ios-footer a {
                color: #aaaaaa !important;
                text-decoration: none;
            }
            /*Responsive*/
            @media screen and (max-width: 799px) {
                table.row {
                    width: 100% !important;
                    max-width: 100% !important;
                }
                td.row {
                    width: 100% !important;
                    max-width: 100% !important;
                }
                .img-responsive img {
                    width: 100% !important;
                    max-width: 100% !important;
                    height: auto !important;
                    margin: auto;
                }
                .center-float {
                    float: none !important;
                    margin: auto !important;
                }
                .center-text {
                    text-align: center !important;
                }
                .container-padding {
                    width: 100% !important;
                    padding-left: 15px !important;
                    padding-right: 15px !important;
                }
                .container-padding10 {
                    width: 100% !important;
                    padding-left: 10px !important;
                    padding-right: 10px !important;
                }
                .hide-mobile {
                    display: none !important;
                }
                .menu-container {
                    text-align: center !important;
                }
                .autoheight {
                    height: auto !important;
                }
                .m-padding-10 {
                    margin: 10px 0 !important;
                }
                .m-padding-15 {
                    margin: 15px 0 !important;
                }
                .m-padding-20 {
                    margin: 20px 0 !important;
                }
                .m-padding-30 {
                    margin: 30px 0 !important;
                }
                .m-padding-40 {
                    margin: 40px 0 !important;
                }
                .m-padding-50 {
                    margin: 50px 0 !important;
                }
                .m-padding-60 {
                    margin: 60px 0 !important;
                }
                .m-padding-top10 {
                    margin: 30px 0 0 0 !important;
                }
                .m-padding-top15 {
                    margin: 15px 0 0 0 !important;
                }
                .m-padding-top20 {
                    margin: 20px 0 0 0 !important;
                }
                .m-padding-top30 {
                    margin: 30px 0 0 0 !important;
                }
                .m-padding-top40 {
                    margin: 40px 0 0 0 !important;
                }
                .m-padding-top50 {
                    margin: 50px 0 0 0 !important;
                }
                .m-padding-top60 {
                    margin: 60px 0 0 0 !important;
                }
                .m-height10 {
                    font-size: 10px !important;
                    line-height: 10px !important;
                    height: 10px !important;
                }
                .m-height15 {
                    font-size: 15px !important;
                    line-height: 15px !important;
                    height: 15px !important;
                }
                .m-height20 {
                    font-size: 20px !important;
                    line-height: 20px !important;
                    height: 20px !important;
                }
                .m-height25 {
                    font-size: 25px !important;
                    line-height: 25px !important;
                    height: 25px !important;
                }
                .m-height30 {
                    font-size: 30px !important;
                    line-height: 30px !important;
                    height: 30px !important;
                }
                .rwd-on-mobile {
                    display: inline-block !important;
                    padding: 5px;
                }
                .center-on-mobile {
                    text-align: center !important;
                }
            }
        </style>
    </head>
    <body style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" bgcolor="#f0f0f0">
        <span class="preheader-text" style="color: transparent; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; visibility: hidden; width: 0; display: none; mso-hide: all;"></span>
        <div style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;"></div>
        <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="width:100%;max-width:100%;">
            <tr>
                <!-- Outer Table -->
                <td align="center" bgcolor="#f0f0f0" data-composer>
                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                        <!-- lotus-header-1 -->
                        <tr>
                            <td align="center" bgcolor="#FE5000" class="container-padding">
                                <!-- Content -->
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="580" style="width:580px;max-width:580px;">
                                    <tr>
                                        <td height="40" style="font-size:40px;line-height:40px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <!-- Logo & Webview -->
                                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                                <tr>
                                                    <td align="center" class="container-padding">
                                                        <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0" dir="rtl"><tr><td><![endif]-->
                                                        <!-- column -->
                                                        <table border="0" align="right" cellpadding="0" cellspacing="0" role="presentation" class="row" width="280" style="width:280px;max-width:280px;">
                                                            <tr>
                                                                <td class="center-text" align="right" height="22" style="height: 22px;">
                                                                    <a href="#" style="font-family:'Poppins',Arial,Helvetica,sans-serif;font-size:16px;display: block; margin-top: 30px;line-height:20px;font-style:italic;font-weight:400;color:#ffffff;text-decoration:none;letter-spacing:0px;">Comunidad de<br>Freelance en Latinoamerica</a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!-- column -->
                                                        <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                                        <!-- gap -->
                                                        <table border="0" align="right" cellpadding="0" cellspacing="0" role="presentation" class="row" width="20" style="width:20px;max-width:20px;">
                                                            <tr>
                                                                <td height="20" style="font-size:20px;line-height:20px;">&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                        <!-- gap -->
                                                        <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                                        <!-- column -->
                                                        <table border="0" align="right" cellpadding="0" cellspacing="0" role="presentation" class="row" width="280" style="width:280px;max-width:280px;">
                                                            <tr>
                                                                <td align="left" class="center-text">
                                                                    <img style="width:152px;border:0px;display: inline!important;" src="<?=base_url('assets/images/emails/mextemps-blanco.png')?>" width="152" border="0" alt="logo Mextemps">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!-- column -->
                                                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- Logo & Webview -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="40" style="font-size:40px;line-height:40px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="center-text">
                                            <img style="width:190px;border:0px;display: inline!important;" src="<?=base_url('assets/images/emails/users.png')?>" width="190" border="0" alt="intro">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="40" style="font-size:40px;line-height:40px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="center-text" align="center" style="font-family:'Montserrat', sans-serif;font-size:42px;line-height:52px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;">
                                            <div>
                                                Nuevo Administrador
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20" style="font-size:20px;line-height:20px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="center-text" align="center" style="font-family:'Poppins',Arial,Helvetica,sans-serif;font-size:16px;line-height:26px;font-weight:300;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;">
                                            <div>
                                                Sé le ha invitado a colaborar como administrador en nuestra plataforma, para ingresar utilice los siguientes accesos:
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="40" style="font-size:40px;line-height:40px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <!-- Header Button -->
                                            <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                                <tr>
                                                    <td align="center" bgcolor="#003c71" style="border-radius: 6px;">
                                                        <!--[if (gte mso 9)|(IE)]>
                                                        <table border="0" cellpadding="0" cellspacing="0" align="center">
                                                        <tr>
                                                        <td align="center" width="50"></td>
                                                        <td align="center" height="50" style="height:50px;">
                                                        <![endif]-->
                                                        <a href="<?=base_url('/log_adm2022')?>" target="_blank" onMouseOver="this.style.backgroundColor='#1f2938',this.style.borderRadius='6px'" onMouseOut="this.style.backgroundColor='#003c71'" style="font-family:'Montserrat', sans-serif;font-size:16px;line-height:19px;font-weight:700;font-style:normal;color:#ffffff;text-decoration:none;letter-spacing:0px;padding: 20px 50px 20px 50px;display: inline-block;"><span>IR A PANEL</span></a>
                                                        <!--[if (gte mso 9)|(IE)]>
                                                        </td>
                                                        <td align="center" width="50"></td>
                                                        </tr>
                                                        </table>
                                                        <![endif]-->
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- Header Button -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="40" style="font-size:40px;line-height:40px;">&nbsp;</td>
                                    </tr>
                                </table>
                                <!-- Content -->
                            </td>
                        </tr>
                    </table>
                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                        <!-- lotus-arrow-divider -->
                        <tr>
                            <td align="center" bgcolor="#FFFFFF">
                                <img style="width:50px;border:0px;display: inline!important;" src="<?=base_url('assets/images/emails/Arrow.png')?>" width="50" border="0" alt="arrow">
                            </td>
                        </tr>
                    </table>
                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                        <!-- lotus-content-1-1 -->
                        <tr>
                            <td align="center" bgcolor="#FFFFFF" class="container-padding">
                                <!-- Content -->
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="580" style="width:580px;max-width:580px;">
                                    <tr>
                                        <td height="50" style="font-size:50px;line-height:50px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="center-text" align="center" style="font-family:'Montserrat', sans-serif;font-size:32px;line-height:42px;font-weight:400;font-style:normal;color:#282828;text-decoration:none;letter-spacing:0px;">
                                            <div>
                                                Datos de acceso
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="30" style="font-size:30px;line-height:30px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <!-- 2-columns -->
                                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%; ">
                                                <tr>
                                                    <td align="center">
                                                        <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                                        <!-- column -->
                                                        <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="100%" style="">
                                                            <tr>
                                                                <td class="center-text" align="left" style="font-family:'Montserrat', sans-serif;font-size:20px;line-height:28px;font-weight:400;font-style:normal;color:#003c71;text-decoration:none;letter-spacing:0px;text-align: center;">
                                                                    <div>
                                                                        Email:
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="5" style="font-size:5px;line-height:5px;">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="center-text container-padding" align="left" style="font-family:'Poppins',Arial,Helvetica,sans-serif;font-size:14px;line-height:24px;font-weight:400;font-style:normal;color:#6e6e6e;text-decoration:none;letter-spacing:0px;text-align: center;">
                                                                    <div>
                                                                        <?=$correo?>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!-- column -->
                                                        <!-- column -->
                                                        <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="100%" style="margin-top: 30px;">
                                                            <tr>
                                                                <td class="center-text" align="left" style="font-family:'Montserrat', sans-serif;font-size:20px;line-height:28px;font-weight:400;font-style:normal;color:#003c71;text-decoration:none;letter-spacing:0px;text-align: center;">
                                                                    <div>
                                                                        Contraseña:
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="5" style="font-size:5px;line-height:5px;">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="center-text container-padding" align="left" style="font-family:'Poppins',Arial,Helvetica,sans-serif;font-size:14px;line-height:24px;font-weight:400;font-style:normal;color:#6e6e6e;text-decoration:none;letter-spacing:0px;text-align: center;">
                                                                    <div>
                                                                        <?=$contra?>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!-- column -->
                                                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- 2-columns -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="45" style="font-size:15px;line-height:45px;">&nbsp;</td>
                                    </tr>
                                </table>
                                <!-- Content -->
                            </td>
                        </tr>
                    </table>
                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                        <!-- lotus-footer-2 -->
                        <tr>
                            <td align="center" bgcolor="#f0f0f0" class="container-padding">
                                <!-- Content -->
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="580" style="width:580px;max-width:580px;">
                                    <tr>
                                        <td height="50" style="font-size:50px;line-height:50px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <!-- Social Icons -->
                                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                                <tr>
                                                    <td align="center">
                                                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation">
                                                            <tr>
                                                                <td class="rwd-on-mobile" align="center" valign="middle" height="36" style="height: 36px;">
																	<table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation">
																		<tr>
																			<td width="10"></td>
																			<td align="center">
																			    <a href="<?= get_redSocial('facebook') ?>" target="_blank">
																				    <img style="width:36px;border:0px;display: inline!important;" src="<?=base_url('assets/images/emails/facebook.png')?>" width="36" border="0" alt="icon">
																				</a>
																			</td>
																			<td width="10"></td>
																		</tr>
																	</table>
																</td>
																<!-- <td class="rwd-on-mobile" align="center" valign="middle" height="36" style="height: 36px;">
																	<table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation">
																		<tr>
																			<td width="10"></td>
																			<td align="center">
																				<img style="width:36px;border:0px;display: inline!important;" src="images/twitter.png" width="36" border="0" alt="icon">
																			</td>
																			<td width="10"></td>
																		</tr>
																	</table>
																</td> -->
																<td class="rwd-on-mobile" align="center" valign="middle" height="36" style="height: 36px;">
																	<table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation">
																		<tr>
																			<td width="10"></td>
																			<td align="center">
																			    <a href="<?= get_redSocial('linkedin') ?>" target="_blank">
																				    <img style="width:36px;border:0px;display: inline!important;" src="<?=base_url('assets/images/emails/linkedin.png')?>" width="36" border="0" alt="icon">
																				</a>
																			</td>
																			<td width="10"></td>
																		</tr>
																	</table>
																</td>
																<td class="rwd-on-mobile" align="center" valign="middle" height="36" style="height: 36px; display: none;">
																	<table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation">
																		<tr>
																			<td width="10"></td>
																			<td align="center">
																			    <a href="<?= get_redSocial('youtube') ?>" target="_blank">
																				    <img style="width:36px;border:0px;display: inline!important;" src="<?=base_url('assets/images/emails/youtube.png')?>" width="36" border="0" alt="icon">
																			    </a>
																			</td>
																			<td width="10"></td>
																		</tr>
																	</table>
																</td>
																<td class="rwd-on-mobile" align="center" valign="middle" height="36" style="height: 36px;">
																	<table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation">
																		<tr>
																			<td width="10"></td>
																			<td align="center">
																			    <a href="<?= get_redSocial('instagram') ?>" target="_blank">
																				    <img style="width:36px;border:0px;display: inline!important;" src="<?=base_url('assets/images/emails/instagram.png')?>" width="36" border="0" alt="icon">
																				</a>
																			</td>
																			<td width="10"></td>
																		</tr>
																	</table>
																</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- Social Icons -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="30" style="font-size:30px;line-height:30px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="center-text" align="center" style="font-family:'Poppins',Arial,Helvetica,sans-serif;font-size:14px;line-height:24px;font-weight:400;font-style:normal;color:#6e6e6e;text-decoration:none;letter-spacing:0px;">
                                            <div>
                                                <img style="width:100px;border:0px;display: block !important; margin: 0 auto 20px;" src="<?=base_url('assets/images/emails/mextemps-color.png')?>" width="100" border="0" alt="logo Mextemps">
                                                C.P. 66220, Nuevo León, México,
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="center-text" align="center" style="font-family:'Poppins',Arial,Helvetica,sans-serif;font-size:14px;line-height:24px;font-weight:400;font-style:normal;color:#6e6e6e;text-decoration:none;letter-spacing:0px;">
                                            <a href="mailto:<?= get_redSocial('correo') ?>" style="color:#6e6e6e;"><span><?= get_redSocial('correo') ?></span></a> - <a href="https://wwww.mextemps.com" style="color:#6e6e6e;"><span>www.mextemps.com</span></a>
                                            <br>Todos los derecho reservados por MexTemps © 2022
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="50" style="font-size:50px;line-height:50px;">&nbsp;</td>
                                    </tr>
                                </table>
                                <!-- Content -->
                            </td>
                        </tr>
                    </table>
                </td>
            </tr><!-- Outer-Table -->
        </table>
    </body>
</html>