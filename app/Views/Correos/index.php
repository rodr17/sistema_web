<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <title>Lista de correos</title>
    </head>
    <body>
        <div class="container">
            <h1>Lista de correos</h1>
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="text-center">
                        Usuarios
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="text-center">
                        Administrador
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="list-group py-5">
                        <a href="<?=base_url('Correos/bienvenidaFreelance')?>" class="list-group-item list-group-item-action">
                            Bienvenida a Freelance
                        </a>
                        <a href="<?=base_url('Correos/bienvenidaContratista')?>" class="list-group-item list-group-item-action">
                            Bienvenida a Contratante
                        </a>
                        <a href="<?=base_url('Correos/cuentaAprobada')?>" class="list-group-item list-group-item-action">
                            Cuenta aprobada
                        </a>
                        <a href="<?=base_url('Correos/cuentaDeclinada')?>" class="list-group-item list-group-item-action">
                            Cuenta declinada
                        </a>
                        <a href="<?=base_url('Correos/proyectoNoAsignado')?>" class="list-group-item list-group-item-action">
                            Proyecto no asignado
                        </a>
                        <a href="<?=base_url('Correos/proyectoFinalizado')?>" class="list-group-item list-group-item-action">
                            Proyecto finalizado
                        </a>
                        <a href="<?=base_url('Correos/freelanceReportado')?>" class="list-group-item list-group-item-action">
                            Freelance reportado
                        </a>
                        <a href="<?=base_url('Correos/resetearContrasena')?>" class="list-group-item list-group-item-action">
                            Resetear contraseña
                        </a>
                        <a href="<?=base_url('Correos/solicitudAceptada')?>" class="list-group-item list-group-item-action">
                            Solicitud aceptada
                        </a>
                        <a href="<?=base_url('Correos/declinarSolicitud')?>" class="list-group-item list-group-item-action">
                            Solicitud declinada
                        </a>
                        <a href="<?=base_url('Correos/solicitudProyecto')?>" class="list-group-item list-group-item-action">
                            Solicitud de proyecto
                        </a>
                        <a href="<?=base_url('Correos/cancelarSuscripcion')?>" class="list-group-item list-group-item-action">
                            Suscripción cancelada
                        </a>
                        <a href="<?=base_url('Correos/recordatorioPago')?>" class="list-group-item list-group-item-action">
                            Recordatorio de pago
                        </a>
                        <a href="<?=base_url('Correos/nuevaReferenciaPago')?>" class="list-group-item list-group-item-action">
                            Nueva referencia pago
                        </a>
                        <a href="<?=base_url('Correos/cuentaEliminada')?>" class="list-group-item list-group-item-action">
                            Cuenta eliminada
                        </a>
                        <a href="<?=base_url('Correos/perfilIncompleto')?>" class="list-group-item list-group-item-action">
                            Perfil incompleto
                        </a>
                        <a href="<?=base_url('Correos/proyecto_aceptado')?>" class="list-group-item list-group-item-action">
                            Seleccionado para realizar proyecto
                        </a>
                        <a href="<?=base_url('Correos/soliFinalizarProyecto')?>" class="list-group-item list-group-item-action">
                            Solicitud para finalizar proyecto
                        </a>
                        <a href="<?=base_url('Correos/declinarSolicitudFin')?>" class="list-group-item list-group-item-action">
                            Solicitud para declinar proyecto declinada
                        </a>
                        <a href="<?=base_url('Correos/mensajesSinVer')?>" class="list-group-item list-group-item-action">
                            Nuevos mensajes sin ver
                        </a>
                        <a href="<?=base_url('Correos/eliminadoPorAdministracion')?>" class="list-group-item list-group-item-action">
                            Cuenta eliminada por area de administración
                        </a>
                        <a href="<?=base_url('Correos/suscripcionVencida')?>" class="list-group-item list-group-item-action">
                            Suscripción vencida
                        </a>
                        <a href="<?=base_url('Correos/renovacionFallida')?>" class="list-group-item list-group-item-action">
                            Suscripción fallida
                        </a>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="list-group py-5">
                        <a href="<?=base_url('Correos/contacto')?>" class="list-group-item list-group-item-action">
                            Contacto
                        </a>
                        <a href="<?=base_url('Correos/nuevoAdministrador')?>" class="list-group-item list-group-item-action">
                            Nuevo administrador
                        </a>
                        <a href="<?=base_url('Correos/soporte')?>" class="list-group-item list-group-item-action">
                            Soporte
                        </a>
                        <a href="<?=base_url('Correos/respuestaSoporte')?>" class="list-group-item list-group-item-action">
                            Respuesta a solicitud de soporte
                        </a>
                        <a href="<?=base_url('Correos/solicitudSugerencia')?>" class="list-group-item list-group-item-action">
                            Solicitud de sugerencia
                        </a>
                        <a href="<?=base_url('Correos/alertaNuevoUsuario')?>" class="list-group-item list-group-item-action">
                            Nuevo usuario
                        </a>
                        <a href="<?=base_url('Correos/nuevoPago')?>" class="list-group-item list-group-item-action">
                            Nuevo pago dentro de la plataforma
                        </a>
                        <a href="<?=base_url('Correos/eliminacionCuenta')?>" class="list-group-item list-group-item-action">
                            Usuario ha eliminado su cuenta
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Optional JavaScript; choose one of the two! -->
        <!-- Option 1: Bootstrap Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <!-- Option 2: Separate Popper and Bootstrap JS -->
        <!--
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
        -->
    </body>
</html>