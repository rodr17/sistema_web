<?= $this->extend('front/main') ?>
<?= $this->section('title') ?>
Chat
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<style>
    .barra{position: relative;}
    .barra::after{
        content: '';
        position: absolute;
        width: 9px;
        height: 9px;
        background: #ffffff;
        top: 0px;
        right: 0px;
        border-radius: 0px 0px 2px 2px;
    }
    .barra::before {
        content: '';
        position: absolute;
        width: 9px;
        height: 9px;
        background: #ffffff;
        top: 92%;
        right: 0px;
        border-radius: 2px 2px 0px 0px;
    }
	.inputfile {
		width: 0.1px;
		height: 0.1px;
		opacity: 0;
		overflow: hidden;
		position: absolute;
		z-index: -1;
	}

	.inputfile+label, .inputfile + .tooltips label {
		width: 100%;
		white-space: nowrap;
		cursor: pointer;
		display: inline-block;
		padding: 0.625rem 1.25rem;
		text-align: center;
	}

	.inputfile+label svg, .inputfile + .tooltips label svg {
		height: 1em;
		vertical-align: middle;
		fill: currentColor;
		margin-top: -0.25em;
		margin-right: 0.25em;
	}

	.inputfile-1+label, .inputfile-1 + .tooltips label {
		color: #fff;
		background-color: var(--color-azul);
	}

	.inputfile-1:focus+label,
	.inputfile-1.has-focus+label,
	.inputfile-1+label:hover {
		background-color: var(--color-azulHover);
	}

	.form-wrapper {
		border-radius: 7px;
	}

	.form-wrapper label {
		font-weight: bold;
	}

	.errors li {
		list-style: none;
		width: 100%;
		text-align: center;
	}

	.errors ul {
		padding-left: 0;
		margin-bottom: 0;
	}

	.message-holder {
		border: 1px solid #ccc;
		border-radius: 4px;
		overflow-x: hidden;
		overflow-y: auto;
		height: 400px;
		margin-bottom: 20px;
		padding-top: 10px;
		padding-bottom: 10px;
		padding-right: 25px;
		padding-left: 25px;
	}

	.msg-item {
		overflow: hidden;
		clear: both;
		margin-bottom: 12px;
		padding-top: 5px;
		border-radius: 4px;
		padding-bottom: 5px;
	}

	.msg-text {
		padding-left: 10px;
		padding-top: 5px;
		line-height: 19px;
		padding-right: 20px;
	}

	.msg-img {
		width: 60px;
		margin: 0 10px;
	}

	.msg-item .time {
		font-size: 10px;
	}

	.msg-item.left-msg {
		background: #003c71;
		color: #fff;
	}

	.msg-item.right-msg {
		background: #444;
		color: #fff;
	}

	.time {
		color: #fff;
		opacity: 0.8;
	}

	.left-msg .msg-img {
		float: left;
	}

	.right-msg {
		text-align: right;
	}

	.right-msg .msg-img {
		float: right;
	}
	.chat-mextemps .card.shadow {
		border-radius: 30px;
	}
	.chat-mextemps .card {
		border: 0px;
	}
	.chat-mextemps .card-header, .chat-mextemps .card-footer {
		border: 0px;
	}
	.chat-mextemps #form-archivos input {
	  height: 40px;
	  padding-left: 10px;
	  font-size: 12px;
	  padding-top: 5px;
	  margin-bottom: 10px;
	}
	.chat-mextemps .message-holder {
		/*box-shadow: inset 0.4em 0px 0px 0 rgb(254 80 0);*/
		border-radius: 30px;
		/*border: 1px solid rgb(254 80 0) !important;*/
	}

	.chat-mextemps .info-de-proyecto {
		border-radius: 30px;
		padding: 20px;
		border: 1px solid #ccc;
	}

	@media (max-width: 768px) {
		.form-wrapper .text-right {
			text-align: center !important;
		}

		.form-wrapper .btn-primary {
			display: block;
			margin: 0 auto;
		}
	}
</style>
<!-- Contratante -->
<main class="main sala-de-chat">
	<section class="container pb-5 chat-mextemps">
		<div class="row justify-content-center pb-5">
			<div class="col-lg-12">
				<h1 class="h3 text-center">Sala de chat del proyecto</h1>
				<div class="card shadow p-30 mtb-20">
					<div class="row">
						<div class="col-lg-8 col-sm-12 col-md-12 sin_padding_suscripcion ventana-de-proyecto mb-2">
							<div class="card barra">
								<div class="message-holder">
									<div id="contenedor-mensajes" class="row"></div>
								</div>
							</div>
							<form id="enviar_mensaje" class="input-group mt-0">
                                <?php if ($proyecto['estatus'] != 'finalizado') { ?>
                                    <input class="form-control text-font" placeholder="Mensaje" id="mensaje">
                                    <button class="btn btn-outline-danger rounded-end text-font" type="submit" ><i class="fas fa-paper-plane"></i></button>
								<?php } ?>
								<audio id="audio" src="<?= base_url('assets/nota_iphone.mp3') ?>"></audio>
							</form>
						</div>
						<div class="col-lg-4 col-sm-12 col-md-12 info-de-proyecto">
							<h2 class="h6">Proyecto <span class="text-naranja"><?= $proyecto['titulo'] ?></span></h2>
							<ul class="list-group">
								<li class="list-group-item border-0 px-0">
									<a class="btn px-0 text-font" target="_blank" href="<?= base_url('Usuarios/prevista/' . base64_encode($usuario['id'])) ?>">
									    <div class="position-relative">
	                                        <img alt="mextemps" src="<?= imagenPerfil($usuario) ?>" class="align-middle img-perfil-icon"/>
	                                        <span class="sidebar-company"><?=$usuario['nombre'].' '.$usuario['apellidos']?></span>
	                                    </div>
									</a>
								</li>
								<li class="list-group-item border-0 px-0">
                                    <div class="d-grid gap-2">
                                        <a class="btn btn-naranja text-font" href="<?=base_url('Proyectos/viewProyecto/'.$proyecto['id'])?>" target="_blank">
                                            Ver proyecto <i class="fas fa-share-square"></i>
                                        </a>
                                        <?php if($proyecto['estatus'] == 'espera'){ ?>
                                            <?= view_cell('App\Libraries\FuncionesSistema::varChatsPostulantes', ['proyecto' => $proyecto['id'], 'actual' => $usuario['id']]) ?>
                                            <a class="btn btn-naranja" data-bs-toggle="modal" data-bs-target="#modalasignar">
                                                Asignar
                                            </a>
                                        <?php }elseif($proyecto['estatus'] == 'desarrollo'){ ?>
                                            <?php $finsol = model('Finalizar')->where('id_proyecto', $proyecto['id'])->first(); ?>
                                            <div class="d-grid gap-2 mt-2 card-job-bottom" id="sec-boton-fin">
                                                <?php if($finsol != null){ ?>
                                                    <?php if($finsol['estatus_contrante'] == null && $finsol['estatus_freelance'] == 'true'){ ?>
                                                        <div class="alert btn-sm alert-info text-center d-flex justify-content-around p-2 mb-0" role="alert">
                                                            <div class="tooltips">
                                                                <a class="btn px-1" onclick="finProyecto()" data-id="<?= $proyecto['id'] ?>"><i class="fas fa-check text-success"></i> Confirmar</a>
                                                                <span class="tooltiptext d-none d-md-block">Finalizar proyecto</span>
                                                            </div>
                                                            <div class="tooltips">
                                                                <a class="btn px-1" onclick="declinarSoliFin(<?= $proyecto['id'] ?>)" data-id="<?= $proyecto['id'] ?>"><i class="fas fa-times text-danger"></i> Rechazar</a>
                                                                <span class="tooltiptext d-none d-md-block">Declinar proyecto</span>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                            <input type="hidden" value="<?= ($finsol != null)? $finsol['estatus_freelance'] : null ?>" id="est-free">
                                            <input type="hidden" value="<?= ($finsol != null)? $finsol['estatus_contrante'] : null ?>" id="est-cont">
                                            <script>
                                                setInterval(function(){
                                                    verificarEstSoli(<?=$proyecto['id']?>);
                                                }, 3500);
                                            </script>
                                        <?php } ?>
                                    </div>
								</li>
								<li class="list-group-item border-0 px-0">
									<label class="text-font">Estatus: <span class="text-naranja"><?= ($proyecto['estatus'] == 'desarrollo')? 'En desarrollo' : $proyecto['estatus'] ?></span></label>
								</li>
							</ul>
							<hr>
							<div class="row overflow-auto mb-3" id="sec-tareas" style="max-height: 220px">
								<?php if(!empty($proyecto['tareas'])) : ?>
									<?=view_cell('App\Libraries\FuncionesSistema::tareasProyecto', ['id' => $proyecto['id']])?>
								<?php else : ?>
									<label class="text-font mb-0">Indica las tareas ha realizar para informar al contratante.</label>
								<?php endif ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<div class="modal fade" id="modalasignar" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="exampleModalToggleLabel" tabindex="-1" aria-modal="true" role="dialog">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header border-0 pb-0">
				<div class="text-center w-100">
					<h3 class="h3">Asignar Proyecto</h3>
				</div>
				<a class="btn cerrar-modal px-3 py-2 desplegar-modal" data-show="" data-close="modalasignar"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body pt-0">
				<div class="text-center w-100">
					<p class="text-font ">
						Estas a punto de asignar un proyecto a un Freelancer, te recomendamos establecer bien los puntos a acordar, al finalizar el proyecto, platícanos tu experiencia y califca al Freelancer que te atendió, esto nos ayuda a todos.
					</p>
				</div>
				<div class="p-3" id="asignar-proyecto" ></div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="finalizar-proyecto" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="fin-proyect" aria-modal="true" role="dialog">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0 pt-4">
				<h5 class="modal-title fw-bold h4" id="fin-proyect">Finalizar proyecto</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body">
				<form id="form-finalizar">
					<div class="row">
						<div class="col-12 col-lg-4">
							<div class="">
								<?= view_cell('App\Libraries\FuncionesSistema::cardPostulantes', ['id' => $proyecto['id_freelancer'], 'postu' => 0, 'tipo' => 'talento', 'modal' => 0, 'es_premium' => $usuario['es_premium']]) ?>
							</div>
						</div>
						<div class="col-12 col-lg-8">
							<label><span class="fw-bold">Título del proyecto: </span><?=$proyecto['titulo']?></label>
							<div class="fw-bold my-3">Estás a punto de finalizar el proyecto.</div>
							<div class="d-flex justify-content-around gap-2 mb-3">
								<label class="small w-100 mb-0" style="line-height: 2rem;">¿Estás conforme con el resultado? :</label>
								<select class="form-select" name="conforme" aria-label="Default select example">
									<option value="si">Si</option>
									<option value="no">No</option>
								</select>
							</div>
							<div class="mb-3">
								<small class="label-modal">Cuéntanos un poco de tu experiencia en este proyecto.</small>
								<br>
								<textarea class="form-control" placeholder="" id="comentario-finalizado" name="comentario" style="height: 100px"></textarea>
								<input type="hidden" name="proyecto" value="<?=$proyecto['id']?>">
								<input type="hidden" name="freelance" value="<?=$proyecto['id_freelancer']?>">
							</div>
							<small class="label-modal">Califica al Freelancer</small>
							<div class="text-center" id="calificar">
								<p class="clasificacion">
									<input id="radio1" type="radio" name="estrellas" value="5">
									<label for="radio1">
										<i class="fas fa-star"></i>
									</label>
									<input id="radio2" type="radio" name="estrellas" value="4">
									<label for="radio2">
										<i class="fas fa-star"></i>
									</label>
									<input id="radio3" type="radio" name="estrellas" value="3">
									<label for="radio3">
										<i class="fas fa-star"></i>
									</label>
									<input id="radio4" type="radio" name="estrellas" value="2">
									<label for="radio4">
										<i class="fas fa-star"></i>
									</label>
									<input id="radio5" type="radio" name="estrellas" value="1">
									<label for="radio5">
										<i class="fas fa-star"></i>
									</label>
								</p>
							</div>
							<div class="p-3 w-100 text-center">
                                <a class="btn btn-naranja" id="btn-fin-proyecto">Finalizar</a>
                            </div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
    <?php
        $post = model('Postulaciones')->where(['id_proyecto' => $proyecto['id'], 'id_postulante' => $usuario['id']])->first();
    ?>
    <?php if($post != null){ ?>
        $(document).ready(function(){
            $.ajax({
                type: 'POST',
                url: '/Proyectos/preAsignarProyecto',
                data:{post:<?=$post['id']?>},
                success: function(data){
                    $('#asignar-proyecto').html(data);
                }, error: function(data){
                    alertify.notify('Contacte con el área de soporte.', 'falla', 10);
                }
            });
        });
    <?php } ?>
    function finProyecto(){
        $('#finalizar-proyecto').modal('show');
    }
    function verificarEstSoli(id){
        let fre = $('#est-free').val();
        let contra = $('#est-cont').val();
        $.ajax({
            type: 'POST',
            url: '<?= base_url('/Proyectos/VerificarEstadoSoliFin') ?>',
            data:{id:id, fre:fre, contra:contra},
            success:function(data){
                let cont = JSON.parse(data);
                if(cont.tipo != 'error'){
                    if(cont.con == 'null' || cont.free == 'true'){
                        $('#sec-boton-fin').html(cont.html);
                    }else if(cont.con == 'true'){
                        $('#sec-boton-fin').html(cont.html);
                    }else{
                        $('#sec-boton-fin').html(cont.html);
                    }
                }
            }, error: function(data){
                console.log('Error en la conexion con el servidor.');
            }
        });
    }
    function declinarSoliFin(id){
        $.ajax({
            type: 'POST',
            url: '<?= base_url('/Proyectos/declinarSoliFin') ?>',
            data:{id:id},
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, 'advertencia');
                $('#sec-boton-fin').empty();
                return alertify.notify('Se le ha notificado al freelance que verifique todas las tareas.', 'correcto');
            }, error: function(data){
                alertify.notify('Inténtelo mas tarde, nuestros servidores se encuentran en mantenimiento.', 'advertencia');
            }
        });
    }
	$(function () {
		$('#btn-fin-proyecto').on('click', function(){
			alertify.dialog('confirm').set({transition:'zoom'}).show();
			alertify.dialog('confirm').set('labels', {ok:'Aceptar', cancel:'Cancelar'}); 
			alertify.confirm('ATENCIÓN', 'Está a punto de finalizar el proyecto, valíde que todo los puntos esten de acuerdo con el freelancer.', function(){
				$.ajax({
					type: 'POST',
					url: '<?=base_url('/Proyectos/finalizarProyecto')?>',
					data:$('#form-finalizar').serialize(),
					success: function(data){
						let cont = JSON.parse(data);
						if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, 'advertencia');
						alertify.notify(cont.mensaje, 'correcto');
						setTimeout(function(){ location.href = '<?=base_url('/usuarios/perfil/panel')?>' }, 3000);
					}, error: function(data){
						alertify.error('Error, comuníquese con el area de soporte.', 10);
					}
				});
			}, function(){});
		});
	});
</script>
<script type="module">
	// Import the functions you need from the SDKs you need
	import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
	import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-analytics.js";
	import { getFirestore } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";
	// TODO: Add SDKs for Firebase products that you want to use
	// https://firebase.google.com/docs/web/setup#available-libraries
	
	// Your web app's Firebase configuration
	// For Firebase JS SDK v7.20.0 and later, measurementId is optional
	const firebaseConfig = {
        apiKey: "AIzaSyCmtOCzTL7RjCi2ZkoJExiBmrGTxgnxN6Q",
        authDomain: "mextemps-341000.firebaseapp.com",
        projectId: "mextemps-341000",
        storageBucket: "mextemps-341000.appspot.com",
        messagingSenderId: "198033966029",
        appId: "1:198033966029:web:12659433a00b1e05e1fa84",
        measurementId: "G-SZY88RED1D"
     };

	// Initialize Firebase
	const app = initializeApp(firebaseConfig);
	const analytics = getAnalytics(app);
	const db = getFirestore(app);
	import { collection, addDoc, getDocs, query, onSnapshot, orderBy, serverTimestamp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";

	$(document).ready(function () {
		let vid = document.getElementById("audio");
		vid.volume = 0.4;
		$('#enviar_archivo').addClass('disabled');
		$(function() { scrollMsgBottom(); })

		function scrollMsgBottom() {
			$('.message-holder').scrollTop($('.message-holder').prop("scrollHeight"));
		}

		$(function() {
			const q = query(collection(db, "chats/<?= $id_sala ?>/messages"), orderBy("timestamp"));
			const unsubscribe = onSnapshot(q, (querySnapshot) => {
				//const cities = [];
				$('#contenedor-mensajes').empty();
				querySnapshot.forEach((doc) => {
					newMessage(doc.data())
				});
				actuaCantMessages($('.msg-item').length, <?= $id_sala ?>);
			});
			$('#enviar_mensaje').on('click', async function(evento) {
				evento.preventDefault();
				let msg = $('#mensaje').val();
				msg = msg.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
				$('#mensaje').val('');
				if (msg.trim() == '') return false;
				
				<?php $nombre = explode(' ', session('usuario'))?>
				let fecha = new Date;
				fecha.toLocaleDateString("es-MX", { month: 'long', day: 'numeric', hour:'numeric',minutes:'2-digit' })
				let minutes = fecha.getMinutes();
				let hour = fecha.getHours();
				let time = hour + ':' + minutes;
				try {
					const docRef = await addDoc(collection(db, "chats/<?= $id_sala ?>/messages"), {
						author: "<?= $nombre[0] ?>",
						authorId: "<?= session('id') ?>",
						imagen: "<?=imagenPerfil(session('imagen'))?>",
						tiempo: time,
						message: msg,
						timestamp: serverTimestamp()
					});
					console.log("Document written with ID: ", docRef.id);
				} catch (e) {
					console.error("Error adding document: ", e);
				}
				$('#mensaje').val('');
			})
		})
		
		$("#archivo_chat").on('change', function(evento) {
		    let archivos = evento.target.files;
            for (let i = 0; i < archivos.length; i++) {
                let archivo = archivos[i];
                if (archivo.size > 4194304) {
                    $(this).val("");
                    return alertify.notify("OPPS... La imagen '"+archivo.name+"' es muy pesada, solo puedes enviar como máximo 4MB.", 'advertencia', 10);
                }
            }
		});

		function newMessage(msg) {
			let typeClass = "left-msg";
			if (msg.authorId == "<?= session('id') ?>") typeClass = "right-msg offset-sm-4 offset-2";
			if (msg.authorId != "<?= session('id') ?>") document.getElementById('audio').play();
			let html = `<div class="col-10 col-sm-8 msg-item ` + typeClass + `">
					<div class="msg-img">
						<img class="img-thumbnail rounded-circle" src="` + msg.imagen + `">
					</div>
					<div class="msg-text">
						<span>` + msg.author + `</span> <span class="time">` + msg.tiempo + `</span><br>
						<p class="text-sm m-0">` + msg.message + `</p>
					</div>
					</div>`;
			$('#contenedor-mensajes').append(html);
			scrollMsgBottom();
			document.getElementById('audio').play();
		}

		$('#enviar_archivo').click(function() {
		    let form = new FormData($('#form-archivos')[0]);
		    $.ajax({
				url: '<?=base_url('Chats/enviar_archivosChat')?>',
				type: 'POST',
				contentType: false,
			    processData: false,
				data: form,
				success: function (data) {
				    let respuesta = JSON.parse(data);
					if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					
					let input = document.querySelector('.inputfile');
    				
					$('#enviar_archivo').addClass('disabled');
    				document.querySelector('.inputfile').nextElementSibling.querySelector('span').innerHTML = 'Seleccionar archivo';
    				$('#form-archivos')[0].reset();
    				$('#contenido_archivos').load('<?=base_url('chats/get_archivos/'.$id_sala)?>').scrollTop($('#contenido_archivos').prop("scrollHeight"));
    				return document.getElementById('audio').play();
				},
				error: function(data){
					return alertify.notify(data.responseText, 'falla', 10);
				}
			});
		});
	});
    $('#fin-proyecto').click(function(){
        let id = $(this).attr('data-id');
    });
    // function declinarSoli(id){
    //     $.ajax({
    //         type: 'POST',
    //         url: '<?= base_url('/Proyectos/declinarSoliFin') ?>',
    //         data:{id:id},
    //         success: function(data){
    //             let cont = JSON.parse(data);
    //             if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, 'advertencia');
    //             $('#sec-boton-fin').empty();
    //             return alertify.notify('Sele a notificado al freelance que verifique todas las tareas.', 'correcto');
    //         }, error: function(data){
    //             alertify.notify('Intentelo mas tarde, nuestros servidores se encuentran en mantenimiento.', 'advertencia');
    //         }
    //     });
    // }
    
	function actuaCantMessages(cantidad, sala){
        console.log('Cantidad: '+cantidad);
        console.log('Sala: '+sala);
        $.ajax({
            type: 'POST',
            url: '<?= base_url('/Chats/saveCantidadMensajes') ?>',
            data:{cantidad:cantidad, sala:sala},
            success: function(data){
                console.log('Mensaajes actualizados');
            }, error: function(data){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.')
            }
        });
	}
</script>
<?= $this->endSection() ?> 