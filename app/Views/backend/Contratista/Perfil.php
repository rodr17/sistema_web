<?php use App\Libraries\NetPay; use App\Libraries\AccesosPlanes; $permisos = new AccesosPlanes($contratista['plan'])?>
<?=$this->extend('front/main')?>
<?=$this->section('estilos')?>
	<link href="https://code.jquery.com/ui/1.12.1/themes/ui-darkness/jquery-ui.css" rel="stylesheet"/>
<?=$this->endSection()?>
<?=$this->section('title')?>
	Perfil
<?=$this->endSection()?>

<?=$this->section('content')?>
<!-- SCRIPT EXTRAS PARA CONTENIDO -->
<script src="<?=base_url('assets/js/croppie/croppie.min.js')?>"></script>
<link href="<?=base_url('assets/css/croppie/croppie.min.css')?>" rel="stylesheet" type="text/css">
<script src="https://code.iconify.design/3/3.0.0/iconify.min.js"></script>
<script src="<?=base_url('assets/themes/js/noUISlider.js')?>"></script>
<!-- SCRIPT EXTRAS PARA CONTENIDO -->
<main class="main contratante-perfil-detalle perfil-freelancer-page">
	<!-- HEADER PERFIL -->
	<section class="section-box">
		<div class="box-head-single box-head-single-candidate">
			<div class="container" id="sec-sup-perfil">
				<?=view_cell('App\Libraries\FuncionesSistema::completarPerfil')?>
				<?=view_cell('App\Libraries\FuncionesSistema::diasPreviosFinalizarSuscripcion', ['id' => session('id')])?>
				<div class="heading-image-rd online">
					<form>
						<label class="cabinet center-block">
							<figure class="m-0 user-select-none">
								<img src="<?= imagenPerfil($contratista) ?>" class="gambar img-responsive img-thumbnail img-perfil shadow border border-3 m-auto" id="imagen-perfil">
							</figure>
							<input type="file" class="item-img file center-block" name="imagenPerfil" id="file_imagen_perfil" hidden>
							<input class="inputs-form text-font2" id="img_recortada" name="img_recortada" hidden value="<?= imagenPerfil($contratista['imagen']) ?>">
						</label>
					</form>
				</div>
				<div class="heading-main-info">
					<h4><span id="nombre_freelancer"><?=$contratista['nombre']?> <?=$contratista['apellidos']?></span></h4>
					<div class="col-12 d-block d-lg-none mas-info-contra">
						<?php if($contratista['cuenta'] == 'predeterminado') :?>
							<span class="d-block text-naranja opciones-ingresar actualizar-contrasena" data-bs-toggle="modal" data-bs-target="#actualizar_contraseña_modal">Actualizar contraseña</span>
						<?php endif ;?>
					</div>
					<div class="head-info-profile">
						<?= isset($contratista['direccion']['estado'])? '<span class="text-small mr-20"><i class="fi-rr-marker text-mutted"></i> '.$contratista['direccion']['estado'].', ' : ''?><?= isset($contratista['direccion']['pais'])? $contratista['direccion']['pais'].'</span>' : ''?>
						<span class="text-small"><i class="fi-rr-clock text-mutted"></i> Usuario desde <?= strftime('%B, %Y', strtotime($contratista['created_at'])) ?></span>
						<div class="col-12 d-block d-lg-none mas-info-contra">
							<span class="d-block sidebar-website-text"><?=$contratista['correo']?></span>
						</div>
					</div>
					<div class="row align-items-end">
						<div class="col-lg-12">
							<a class="btn btn-tags-sm mb-10 btn-listados mr-5" href="#mis_proyectos" style="padding: 10px 18px; background: rgba(81, 146, 255, 0.12) !important; border-radius: 50px; font-size: 14px; line-height: 12px; color: #727272;" data-tipo="espera"><?=view_cell('App\Libraries\FuncionesSistema::cantPostulaciones', ['id' => $contratista['id'], 'rol' => session('rol')])?> proyectos Por Asignar</a>
							<a class="btn btn-tags-sm mb-10 btn-listados mr-5" href="#mis_proyectos" style="padding: 10px 18px; background: rgba(81, 146, 255, 0.12) !important; border-radius: 50px; font-size: 14px; line-height: 12px; color: #727272;" data-tipo="desarrollo"><?=view_cell('App\Libraries\FuncionesSistema::cantProyectosDesarrollo', ['id' => $contratista['id'], 'rol' => session('rol')])?> proyectos en Progreso</a>
							<?php if($permisos->getValor_permiso('historialtrabajos') > 0) :?>
								<a class="btn btn-tags-sm mb-10 btn-listados mr-5" href="#mis_proyectos" style="padding: 10px 18px; background: rgba(81, 146, 255, 0.12) !important; border-radius: 50px; font-size: 14px; line-height: 12px; color: #727272;" data-tipo="finalizado"><?=view_cell('App\Libraries\FuncionesSistema::cantProyectosFinalizados', ['id' => $contratista['id'], 'rol' => session('rol')])?> proyectos Terminados</a>
							<?php endif ;?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section-box mt-50">
		<div class="container">
			<div class="row">
				<div class="col-12 d-block d-lg-none mt-1">
					<div class="d-flex justify-content-center gap-2 mb-3">
						<?php if(!empty($contratista['deleted_at'])) :?>
							<button class="btn btn-naranja btn-border" data-bs-toggle="modal" data-bs-target="#recuperar_cuenta_modal">Recuperar cuenta</button>
						<?php else :?>
							<button class="btn btn-naranja btn-border" data-bs-toggle="modal" data-bs-target="#eliminar_cuenta_modal">Eliminar cuenta</button>
						<?php endif ;?>
					</div>
				</div>
				<div class="col-lg-8 col-md-12 col-sm-12 col-12">
					<div class="content-single">
						<div class="tabs-generales">
							<div class="tab d-sm-flex ">
								<button class="tablinks" onclick="openCity(event, 'MiPerfil')" id="defaultOpen"><i class="far fa-user" aria-hidden="true"></i> Editar Perfil</button>
								<!--<button class="tablinks d-none" onclick="openCity(event, 'BuscadorTalento')"><i class="far fa-folder-open" aria-hidden="true"></i> Buscar Talento</button>-->
								<button class="tablinks" onclick="openCity(event, 'MetodosPago')"><i class="far fa-credit-card" aria-hidden="true"></i> Mis Tarjetas</button>
								<button class="tablinks" onclick="openCity(event, 'HistorialPagos')"><i class="far fa-file-alt" aria-hidden="true"></i> Mis Pagos</button>
								<button class="tablinks" onclick="openCity(event, 'MiSuscripcion')"><i class="far fa-address-card" aria-hidden="true"></i> Mi Suscripción</button>
							</div>
							<div id="MiPerfil" class="tabcontent">
								<form class="contact-form-style mt-10 form-perfil needs-validation" id="form-editarPerfil" enctype="multipart/form-data" novalidate style="padding: 30px;">
									<div class="row wow animate__ animate__fadeInUp animated" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
										<div class="sidebar-shadow-md detalles-dir-uno">
											<div class="row">
												<h4>Datos Personales</h4>
												<div class="col-lg-4 col-md-4">
													<div class="input-style">
														<input class="text-font" id="nombre" name="nombre" placeholder="Nombre" value="<?= $contratista['nombre'] ?>">
														<input class="text-font" id="telefono" name="telefono" placeholder="Telefono" value="<?= isset($contratista['contacto']['telefono']) ? $contratista['contacto']['telefono'] : '' ?>">
													</div>
												</div>
												<div class="col-lg-4 col-md-4">
													<div class="input-style">
														<input class="text-font" id="apellidos" name="apellidos" placeholder="Apellidos" value="<?= $contratista['apellidos'] ?>">
													</div>
												</div>
												<div class="col-lg-4 col-md-4">
													<div class="input-style">
														<input class="inputs-form text-font" type="date" id="nacimiento" name="nacimiento" max="<?=date('Y-m-d', strtotime(date('Y-m-d')."- 18 year"))?>" placeholder="Fecha Nacimiento" value="<?= $contratista['nacimiento'] ?>">
													</div>
												</div>
											</div>
										</div>
										<div class="sidebar-shadow-md detalles-dir-dos">
											<div class="row">
												<h4>Dirección Fiscal</h4>
												<div class="col-lg-4 col-md-4">
													<div class="input-style">
														<input class="text-font" id="calle" name="calle" placeholder="Calle y Número" value="<?= isset($contratista['direccion']['calle']) ? $contratista['direccion']['calle'] : '' ?>" required>
														<!--<input class="text-font" id="ciudad" name="ciudad" placeholder="Ciudad" value="<?= isset($contratista['direccion']['ciudad']) ? $contratista['direccion']['ciudad'] : '' ?>" required>-->
														<input class="text-font" id="codigo_postal" name="codigo_postal" placeholder="Código Postal" value="<?= isset($contratista['direccion']['codigo_postal']) ? $contratista['direccion']['codigo_postal'] : '' ?>" required>
														<input class="inputs-form text-font px-3" id="curp" name="curp" placeholder="CURP" oninput="validarCurp(this)" value="<?= isset($contratista['direccion']['curp']) ? $contratista['direccion']['curp'] : '' ?>" required>
													</div>
												</div>
												<div class="col-lg-4 col-md-4">
													<div class="input-style">
														<input class="text-font" id="colonia" name="colonia" placeholder="Colonia" value="<?= isset($contratista['direccion']['colonia']) ? $contratista['direccion']['colonia'] : '' ?>" required>
														<input class="text-font" id="estado" name="estado" placeholder="Estado" value="<?= isset($contratista['direccion']['estado']) ? $contratista['direccion']['estado'] : '' ?>" required>
														<input class="text-font" id="rfc" name="rfc" placeholder="RFC" value="<?= isset($contratista['direccion']['rfc']) ? $contratista['direccion']['rfc'] : '' ?>">
													</div>
												</div>
												<div class="col-lg-4 col-md-4">
													<div class="input-style">
														<input class="text-font" id="municipio" name="municipio" placeholder="Municipio o Localidad" value="<?= isset($contratista['direccion']['municipio']) ? $contratista['direccion']['municipio'] : '' ?>" required>
														<input class="text-font" id="pais" name="pais" placeholder="País" value="<?= isset($contratista['direccion']['pais']) ? $contratista['direccion']['pais'] : '' ?>" required readonly>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-12 col-md-12 text-center">
											<button class="btn btn-default text-font" id="actualizar-perfil" type="submit">Actualizar</button>
										</div>
									</div>
								</form>
							</div>
							<div id="MetodosPago" class="tabcontent">
								<!-- <h3>Métodos de Pago</h3> -->
								<div class="row p-20" >
									<div class="d-flex justify-content-between align-items-center flex-column flex-sm-row gap-2 mt-0 mb-20">
										<h4 style="font-size: 20px;" class=" vigencia-info-cuenta">Membresia vigente hasta: <?= !empty($suscripcion_actual['fecha_fin']) ? strftime('%d/%B/%Y', strtotime(explode(' ', $suscripcion_actual['fecha_fin'])[0])) : view_cell('App\Libraries\FuncionesSistema::fechaFinVencida') ?></h4>
										<a class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#agregar-pago">Agregar</a>
									</div>
									<div class="row align-items-center" id="card-mis-tarjetas">
										<?php if(!empty($tarjetas)) :?>
											<?php foreach ($tarjetas as $tarjeta) : ?>
												<div class="col-12 col-sm-6 contenedor-tarjeta mb-3 mb-sm-0" tarjeta="<?= $tarjeta['card']['token'] ?>" data="<?= $tarjeta['card']['brand'] ?>">
													<div class="card-grid position-relative hover-up wow animate__ animate__fadeInUp animated actual-metodo" style="visibility: visible; animation-name: fadeInUp;">
														<?php if(isset($tarjeta_suscripcion) && ($tarjeta_suscripcion['token'] == $tarjeta['card']['token'])) :?>
															<label class="etiqueta-principal">Principal</label>
														<?php endif ;?>
														<div class="text-center">
															<a>
																<figure><img class="ms-3" src="<?= model('autenticacion')->urlImgBanco($tarjeta['card']['brand']) ?>" alt="Procesador de pago bancario" width="60"></figure>
															</a>
														</div>
														<h5 class="text-center mt-0 mb-0 ">
															<a class="num-tarjeta" style="font-size: 20px;">**** <?= $tarjeta['card']['lastFourDigits'] ?> | Expira <?= $tarjeta['card']['expMonth'] . '/' . $tarjeta['card']['expYear'] ?></a>
														</h5>
														<!-- <p class="text-center" style="margin: 0;"><= $tarjeta->holder_name?></p> -->
														<?php if(!empty($tarjeta['card']['type']) && $tarjeta['card']['type'] != 'unknown') :?>
															<span class="metodo-agregado"> Tarjeta de <?=str_replace('e', 'é', $tarjeta['card']['type'])?>o</span>
														<?php endif ;?>
														<div class="text-center mt-10">
															<a class="btn btn-default eliminar-tarjeta" datos="<?= $tarjeta['card']['token'] ?>" tarjeta="<?= $tarjeta['card']['lastFourDigits'] ?>">Eliminar</a>
														</div>
													</div>
												</div>
											<?php endforeach; ?>
										<?php else :?>
											<?= vacio('No tienes tarjetas disponibles.')?>
										<?php endif ;?>
									</div>
								</div>
							</div>
							<div id="HistorialPagos" class="tabcontent">
								<div class="card-body border-0">
									<div class="row justify-content-center" id="listado-historial-pago" style="max-height: 400px; overflow-y: auto;">
										<?= view_cell('App\Libraries\FuncionesSistema::historialPago') ?>
									</div>
								</div>
							</div>
							<div id="MiSuscripcion" class="tabcontent">
								<!-- <h3>Mi Suscripción</h3> -->
								<!-- tarifas -->
								<section class="container p-10 planes-perfil">
									<div class="row mb-0 listado-paquetes">
										<article class="col-12 text-start mb-5">
											<div class="card-header bg-transparent border-0 sin_padding_suscripcion"><h2 class="h3 fw-bold mb-0">Mi Suscripción</h2></div>
											<div class="card-body sin_padding_suscripcion">
												<div class="row justify-content-center">
													<div class="col-md-6">
														<label class="text-font mb-3"><?=$descripcion?></label>
														<label class="d-block text-font">Fecha de Inicio: <strong><?= isset($suscripcion_actual['fecha_inicio']) ? strftime('%d/%B/%Y', strtotime(explode(' ', $suscripcion_actual['fecha_inicio'])[0])) : view_cell('App\Libraries\FuncionesSistema::fechaInicioVencida') ?></strong></label>
														<?php if(isset($suscripcion_actual['renovacion'])) :?>
															<?php if($suscripcion_actual['renovacion']) :?>
																<label class="d-block text-font fw-bold">Próxima fecha de facturacion: <?= !empty($suscripcion_actual['fecha_fin']) ? strftime('%d/%B/%Y', strtotime(explode(' ', $suscripcion_actual['fecha_fin'])[0])) :'Vigencia de 365 dias' ?></label>
																<?php else :?>
																	<label class="d-block text-font">Membresia vigente hasta: <strong><?= !empty($suscripcion_actual['fecha_fin']) ? strftime('%d/%B/%Y', strtotime(explode(' ', $suscripcion_actual['fecha_fin'])[0])) : view_cell('App\Libraries\FuncionesSistema::fechaFinVencida') ?></strong></label>
															<?php endif ;?>
														<?php endif ;?>
														<?php if($metodoPago != 'Sin plan' && $metodoPago != 'cupon'){ ?>
															<label class="d-inline-flex align-items-center text-font">Pagado con:
																<?php if($metodoPago == 'efectivo') :?>
																	<label class="card p-1 contenedor_checks active m-0 ms-2" for="pagado_con">
																		<input class="invisible" type="radio" name="pagado_con" hidden="" id="pagado_con">
																		<div class="d-flex align-items-center">
																			<div class="flex-shrink-0">
																				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 20 20" style="color: #3483FA;">
																					<path fill="currentColor" fill-rule="nonzero" d="M19.002 4.004v11.981h-18V4.004h18zm-1.2 1.2h-15.6v9.581h15.6V5.204zm-7.8 2.354a2.436 2.436 0 110 4.873 2.436 2.436 0 110-4.873zm0 1.2a1.236 1.236 0 10-.001 2.473 1.236 1.236 0 00.001-2.473zm4.806.03a1.206 1.206 0 11-.002 2.41 1.206 1.206 0 01.002-2.409zm-9.614 0a1.206 1.206 0 110 2.41 1.206 1.206 0 010-2.409z"></path>
																				</svg>
																			</div>
																			<div class="flex-grow-1 ms-2">
																				<label class="d-flex text-font fw-bold m-0">Efectivo</label>
																			</div>
																		</div>
																	</label>
																<?php elseif($metodoPago == 'tarjeta') :?>
																	<div class="card p-0 contenedor_checks active m-0 ms-2">
																		<div class="d-flex align-items-center">
																			<div class="d-flex flex-shrink-0">
																				<img class="ms-3" src="<?= model('autenticacion')->urlImgBanco($tarjeta_suscripcion['brand']) ?>" alt="Procesador de pago bancario">
																			</div>
																			<div class="flex-grow-1 mx-3">
																				<label class="d-flex text-font fw-bold mb-0">Terminación <?= $tarjeta_suscripcion['lastFourDigits'] ?></label>
																				<label class="d-flex text-font2 mb-0">Vencimiento: <?= $tarjeta_suscripcion['expMonth'] . '/' . $tarjeta_suscripcion['expYear'] ?></label>
																			</div>
																		</div>
																	</div>
																<?php elseif($metodoPago == 'Sin plan'):?>
																<!--&nbsp;<label class="mb-0"><b><?= empty($suscripcion_actual) ? 'Vigencia de 365 dias' : 'Cobro automático cancelado' ?></b></label>-->
																<?php endif ;?>
															</label>
														<?php } ?>
														<?php if(!empty($suscripcion_actual['cambio_pago'])) :?>
															<div class="row">
																<label class="d-inline-flex align-items-center text-font fw-bold">Próxima renovación con:
																	<?php if($suscripcion_actual['cambio_pago'] == 'efectivo') :?>
																		<label class="card p-1 contenedor_checks active m-0 ms-2" for="cambio_pago">
																			<input class="invisible" type="radio" name="pagado_con" hidden="" id="cambio_pago">
																			<div class="d-flex align-items-center">
																				<div class="flex-shrink-0">
																					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 20 20" style="color: #3483FA;">
																						<path fill="currentColor" fill-rule="nonzero" d="M19.002 4.004v11.981h-18V4.004h18zm-1.2 1.2h-15.6v9.581h15.6V5.204zm-7.8 2.354a2.436 2.436 0 110 4.873 2.436 2.436 0 110-4.873zm0 1.2a1.236 1.236 0 10-.001 2.473 1.236 1.236 0 00.001-2.473zm4.806.03a1.206 1.206 0 11-.002 2.41 1.206 1.206 0 01.002-2.409zm-9.614 0a1.206 1.206 0 110 2.41 1.206 1.206 0 010-2.409z"></path>
																					</svg>
																					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="color: #3483FA;">
																						<path fill="currentColor" fill-rule="evenodd" d="M11.999.632l9.751 5.67v3.447l-2.244.001v7.52h1.24l2.24 5.265H1.164l2.071-5.265h1.267V9.766l-2.253.004V6.302l9.749-5.67zm7.755 18.138H4.258l-.891 2.265h17.35l-.963-2.265zm-1.748-1.5V9.751l-5.257.006v7.511h5.257zm-6.757-7.51l-5.246.006v7.503h5.246V9.76zm.75-7.392L3.75 7.165v1.104l16.5-.018V7.165l-8.251-4.797z"></path>
																					</svg>
																				</div>
																				<div class="flex-grow-1 ms-2">
																					<label class="d-flex text-font fw-bold m-0">Efectivo</label>
																				</div>
																			</div>
																		</label>
																	<?php else :?>
																		<?php $tarjeta_a_cambiar = model('Autenticacion')->get_tarjeta_user($suscripcion_actual['cambio_pago'])?>
																		<div class="card p-0 contenedor_checks active m-0 ms-2">
																			<div class="d-flex align-items-center">
																				<div class="d-flex flex-shrink-0">
																					<img class="ms-3" src="<?= model('autenticacion')->urlImgBanco($tarjeta_a_cambiar['card']['brand']) ?>" alt="Procesador de pago bancario">
																				</div>
																				<div class="flex-grow-1 mx-3">
																					<label class="d-flex text-font fw-bold mb-0">Terminación <?= $tarjeta_a_cambiar['card']['lastFourDigits'] ?></label>
																					<label class="d-flex text-font2 mb-0">Vencimiento: <?= $tarjeta_a_cambiar['card']['expMonth'] . '/' . $tarjeta_a_cambiar['card']['expYear'] ?></label>
																				</div>
																			</div>
																		</div>
																	<?php endif ;?>
																</label>
															</div>
														<?php endif ;?>
													</div>
													<div class="col-md-6">
														<div class="card shadow-sm" style="border-radius: 30px;">
															<div class="card-body">
																<?php if($metodoPago != 'cupon') :?>
																	<div id="seccion_info_cupon">
																		<?php $cupon_gratuito = model('UsosCupones')
																							->where(['usos_cupones.id_usuario' => $contratista['id'], 'tipo' => 'Gratuitos', 'canjeado' => true])
																							->join('cupones', 'cupones.id = usos_cupones.id_cupon')
																							->orderBy('usos_cupones.id', 'DESC')
																							->countAllResults();
																		if($cupon_gratuito == 0 && !$con_suscripcion) :?>
																			<h6 class="h6">Cupon de cuenta premium gratis</h6>
																			<small class="d-block fst-italic small mb-3">* Funciona solamente 1 vez por cuenta y si no has adquirido un plan.</small>
																		<?php else :?>
																			<h6 class="h6">Cupon de descuento</h6>
																			<small class="d-block fst-italic small mb-3">* Aplica para próximo pago de mensualidad o anualidad.</small>
																		<?php endif ;?>
																		<div class="d-flex justify-content-start">
																			<div class="d-inline-flex">
																				<input class="border border-secondary text-font" onkeyup="mayus(this)" name="cupon" type="text" placeholder="Ingrese su cupón">
																				<a class="btn btn-default mx-1 align-self-center validar-cupon">Validar</a>
																			</div>
																		</div>
																	</div>
																<?php endif ;?>
																<?php if($con_suscripcion) :?>
																	<?php $cupon_valido = model('UsosCupones')
																					->where(['usos_cupones.id_usuario' => $contratista['id']])
																					->join('cupones', 'cupones.id = usos_cupones.id_cupon')
																					->orderBy('usos_cupones.id', 'DESC')
																					->first();?>
																	<?php if(!empty($cupon_valido)) :?>
																		<?php $cuenta_gratis = model('Suscripciones')->where(['id_usuario' =>  session('id'), 'tipo_suscripcion' => 'cupon'])->first();
																			if(empty($cuenta_gratis)) {
																				$plan = model('Suscripciones')->informacion_plan($contratista['plan']);
																				echo view('backend/viewcells/Informacion_cupon', ['id_usuario' => session('id'), 'tipo' => $cupon_valido['tipo'], 'plan' => $plan]);
																			}
																			else{
																				$cuenta_gratis['tipo'] = $cupon_valido['tipo'];
																			} echo view('backend/viewcells/Informacion_cupon', $cuenta_gratis);?>
																	<?php endif ;?>
																<?php endif ;?>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="card-footer sin_padding_suscripcion border-0 d-flex gap-2 justify-content-center bg-transparent">
												<?php if(isset($suscripcion_actual['renovacion'])) :?>
													<?php if(!$con_suscripcion) :?>
														<label class="h6 text-naranja">¡Obtén una membresía!</label>
													<?php endif ;?>
													<?php if($suscripcion_actual['renovacion']) :?>
														<?php if($metodoPago == 'tarjeta') :?>
															<button class="btn btn-naranja btn-border text-font" data-bs-toggle="modal" data-bs-target="#actualizar_tarjeta">Actualizar tarjeta</button>
															<div class="modal fade" id="actualizar_tarjeta" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-hidden="true">
																<div class="modal-dialog modal-dialog-centered">
																	<div class="modal-content">
																		<div class="modal-header justify-content-center border-bottom-0">
																			<h5 class="modal-title fw-bold">Seleccione tarjeta</h5>
																			<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
																		</div>
																		<div class="modal-body pt-0">
																			<div class="container">
																				<div class="row align-items-center text-center">
																					<div class="d-flex col-12 justify-content-end">
																						<!-- <a class="text-font text-naranja opciones-ingresar" data-bs-toggle="modal" data-bs-target="#agregar-pago">Agregar tarjeta</a> -->
																						<label class="card p-2 add-tarjeta opciones-ingresar">
																							<div class="d-flex align-item-center">
																								<div class="flex-shrink-0 align-self-center">
																									<img src="<?=base_url('/assets/images/iconos/visa.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
																									<img src="<?=base_url('/assets/images/iconos/amex.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
																									<img src="<?=base_url('/assets/images/iconos/mastercard.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
																								</div>
																								<div class="flex-grow-1 ms-3">
																									<a class="nueva-card" data-bs-toggle="modal" data-bs-target="#agregar-pago">Agregar tarjeta</a>
																								</div>
																							</div>
																						</label>
																					</div>
																					<?php if($metodoPago != 'Sin plan'): ?>
																						<?php foreach ($tarjetas as $key => $tarjeta) : ?>
																							<?php if(!empty($tarjeta_suscripcion)):?>
																								<?php if($tarjeta['card']['token'] == $tarjeta_suscripcion['token']): continue; endif;?>
																							<?php endif ;?>
																							<div class="col-lg-12 mt-3" tarjeta="<?= $tarjeta['card']['token'] ?>">
																								<label class="card p-1 contenedor_checks" for="<?= $tarjeta['card']['token'] ?>">
																									<input class=" actualizar_tarjeta text-font" type="radio" name="tarjetas_usuarios_actualizar" hidden id="<?= $tarjeta['card']['token'] ?>">
																									<div class="d-flex align-items-center">
																										<div class="d-flex flex-shrink-0">
																											<img class="ms-3" src="<?= model('Autenticacion')->urlImgBanco($tarjeta['card']['brand'])?>" alt="Procesador de pago bancario">
																										</div>
																										<div class="flex-grow-1 ms-3">
																											<p class="d-flex text-font fw-bold mb-0">Terminación <?= $tarjeta['card']['lastFourDigits'] ?></p>
																											<p class="d-flex text-font mb-0"><?= $tarjeta['card']['bank'] ?></p>
																											<p class="d-flex text-font mb-0">Vencimiento: <?= $tarjeta['card']['expMonth'] . '/' . $tarjeta['card']['expYear'] ?></p>
																										</div>
																									</div>
																								</label>
																							</div>
																						<?php endforeach; ?>
																					<?php endif;?>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														<?php endif ;?>
														<form class="d-inline-block" id="form-metodo-pago" action="<?= base_url('CambiarPago')?>" method="POST">
															<input class="invisible text-font" name="cambio_metodo_pago" id="nuevo_metodo_pago" readonly hidden value="<?=$suscripcion_actual['tipo_suscripcion']?>">
															<button class="btn btn-naranja btn-border text-font" type="button" data-bs-toggle="modal" data-bs-target="#cambiar_metodo_pago">Cambiar método pago</button>
														</form>
													<?php else :?>
														<?php if(!empty($suscripcion_actual['cambio_pago'])) :?>
															<button class="btn btn-primary" id="cancelar-cambio-pago">
																Cancelar cambio de metodo de pago
															</button>
															<script>
																$('#cancelar-cambio-pago').click(function(){
																	if(alertify.confirm(iconoAlertify+' ¡Atención!', '¿Deseas cancelar el cambio método de pago?', function(){
																		$.ajax({
																			type: 'POST',
																			url: '<?=base_url('/Pagos/cancelarCambioPago')?>',
																			data:{},
																			success: function(data){
																				if(data == 'false') return alertify.notify('Ha surgido un error, inténtelo nuevamente.', 'advertencia');
																				alertify.notify('La solicitud de cambio de pago ha sido declinada.', 'correcto');
																				return setTimeout(function(){location.reload()}, 3000);
																			}, error: function(data){
																				alertify.notify('Error: '+data, 'error');
																			}
																		});
																	}, function(){}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})){}
																});
															</script>
														<?php endif; ?>
													<?php endif; ?>
												<?php endif ;?>
											</div>
										</article>
										<article class="col-12">
											<hr>
											<div class="row mb-5">
												<div class="d-flex justify-content-between justify-content-md-center align-items-center gap-2 mt-15">
													<span class="text-md text-billed">Pago Mensual</span>
													<label class="switch mb-0">
														<input type="checkbox" id="cb_billed_type" name="billed_type" onchange="checkBilled()">
														<span class="slider round"></span>
													</label>
													<span class="text-md text-billed">Pago Anual</span>
												</div>
												<?php $planes = model('ModelPlanes')->where('cuenta', 'contratante')->orderBy('id', 'ASC')->find()?>
												<div class="block-pricing mt-100 mt-md-50" style="background: #ff510014;">
													<div class="row">
														<?php foreach($planes as $key => $plan){ ?>
															<?php if($plan['monto'] == 0) continue;?>
															<div class="col-12 col-lg-6 wow animate__animated animate__fadeInUp animated tarifas-cuadros-perfil" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
																<div class="box-pricing-item <?= ($plan['name'] == 'Intermedio')? 'most-popular' : ''?> <?=(($suscripcion_actual != null) && ($plan['id_plan_mensual'] == $suscripcion_actual['id_plan_mensual'] || ($plan['id_plan_anual'] == $suscripcion_actual['id_plan_anual'])))? 'm-0' : ''?>">
																	<?php if(!empty($suscripcion_actual)) :?>
																		<?php if($plan['id_plan_mensual'] == $suscripcion_actual['id_plan_mensual'] || ($plan['id_plan_anual'] == $suscripcion_actual['id_plan_anual'])) :?>
																			<div class="text-end mb-10 d-none">
																				<label class="btn btn-white-sm">Plan Actual</label>
																			</div>
																		<?php endif ;?>
																	<?php endif ;?>
																	<h4 class="mb-15">Plan <?=$plan['name']?></h4>
																	<div class="box-info-price">
																		<span class="text-price for-month display-month"><?= ($plan['monto'] == '0')? 'Gratis' : '$'.number_format($plan['monto'], 2, '.', ',').' MXN' ?></span>
																		<span class="text-price for-year">
																			<?php if($plan['monto'] != 0) :?>
																				<span class="mes-tachado"><?= '$'.number_format($plan['monto'], 2, '.', ',').' MXN' ?></span>
																			<?php else :?>
																				<span>Gratis</span>
																			<?php endif ;?>
																			<?= ($plan['monto'] != '0')? '$'.number_format(($plan['monto_anual'] / 12), 2, '.', ',').' MXN'  : '' ?>
																		</span>
																		<span class="text-month for-month display-month" style="margin-top: 1px;"><?= ($plan['monto'] == '0') ? 'Vigencia 1 año' : 'Pago mensual'?></span>
																		<span class="text-month for-year" style="margin-top: 1px;"><?= ($plan['monto'] != '0') ? 'Precio mensual' : 'Vigencia 1 año'?></span>
																	</div>
																	<div>
																		<p class="text-desc-package mb-30">
																			<?=$plan['descripcion']?>
																			<br><br class="for-month">
																			<span class="text-facturados for-year">$<?=number_format($plan['monto_anual'], 2, '.', ',')?> MXN facturados anualmente</span>
																		</p>
																	</div>
																	<?php if(!empty($suscripcion_actual)) :?>
																		<?php if($plan['id_plan_mensual'] != $suscripcion_actual['id_plan_mensual'] || ($plan['id_plan_anual'] != $suscripcion_actual['id_plan_anual'])) :?>
																			<div class="botones">
																				<a class="btn btn-border for-month display-month" plan="<?=$plan['id_plan_mensual']?>" name="<?=$plan['id_plan_mensual']?>">Seleccionar</a>
																				<a class="btn btn-border for-year anual" plan="<?=$plan['id_plan_anual']?>" name="<?=$plan['id_plan_anual']?>">Seleccionar</a>
																				<input class='invisible text-font inputs-form plan_seleccionado' hidden>
																			</div>
																		<?php else :?>
																		<label class="btn btn-white-sm">Plan Actual</label>
																	<?php endif ;?>
																	<?php else :?>
																		<div class="botones">
																			<a class="btn btn-border for-month display-month" plan="<?=$plan['id_plan_mensual']?>" name="<?=$plan['id_plan_mensual']?>">Seleccionar</a>
																			<a class="btn btn-border for-year anual" plan="<?=$plan['id_plan_anual']?>" name="<?=$plan['id_plan_anual']?>">Seleccionar</a>
																			<input class='invisible text-font inputs-form plan_seleccionado' hidden>
																		</div>
																	<?php endif ;?>
																	<div class="d-block d-lg-none acordion-plan-perfil">
																		<div class="accordion accordion-flush" id="accordionFlush<?=$key?>">
																			<div class="accordion-item border-0">
																				<h2 class="accordion-header" id="flush-heading-card-<?=$key?>">
																					<button class="accordion-button collapsed contenedor-arrow-v d-block text-center" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-card-<?=$key?>" aria-expanded="false" aria-controls="flush-collapse-card-<?=$key?>">
																						Ver características <i class="arrow-v down-v"></i>
																					</button>
																				</h2>
																				<div id="flush-collapse-card-<?=$key?>" class="accordion-collapse collapse" aria-labelledby="flush-heading-card-<?=$key?>" data-bs-parent="#accordionFlush<?=$key?>">
																					<div class="accordion-body p-2 listado-caracteristicas">
																						<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'perfilcontratante']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								Crear perfil de Contratante
																							</div>
																						<?php } ?>
																						<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'paneladministracion']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								Panel de administración
																							</div>
																						<?php } ?>
																						<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'valoraciontrabajos']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								Valoracion de trabajos
																							</div>
																						<?php } ?>
																						<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'historialconversaciones']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								Salas de chat
																							</div>
																						<?php } ?>
																						<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'contactarFreelancer']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								Contactar Freelancer
																							</div>
																						<?php } ?>
																						<div class="text-center my-3 border-bottom caract-list">
																							Buscador de freelancer
																						</div>
																						<?php $solicitudes = intval(view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'enviosolicitudes']))?>
																						<?php if($solicitudes > 0){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								<?= $solicitudes == 9999 ? 'Envio de solicitudes por proyecto ilimitadas' : $solicitudes.' envio(s) de solicitudes por proyecto' ?>
																							</div>
																						<?php } ?>
																						<?php $proyectos_activos = intval(view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'proyectosActivos']))?>
																						<?php if($proyectos_activos > 0){?>
																							<div class="text-center my-3 border-bottom caract-list">
																								<?= $proyectos_activos == 9999 ? 'Proyectos activos ilimitados' : $proyectos_activos.' proyecto(s) activos'?>
																							</div>
																						<?php } ?>
																						<?php $historial_trabajos = intval(view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'historialtrabajos'])) ?>
																						<?php if($historial_trabajos > 0){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								<?= $historial_trabajos == 9999 ? 'Historial de proyectos ilimitado' : 'Historial de '.$historial_trabajos.' proyectos'?>
																							</div>
																						<?php } ?>
																						<?php $destacados = intval(view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'proyectosDestacados'])); ?>
																						<?php if($destacados > 0){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								<?= $destacados > 9998 ? 'Proyectos destacados ilimitados' : $destacados.' proyecto(s) destacado(s)'?>
																							</div>
																						<?php } ?>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														<?php } ?>
													</div>
												</div>
												<div class="col-12 mt-4 text-center">
													<a href="<?= base_url('tarifas-contratista')?>" class="btn btn-border" target="_blank">Ver detalle de planes</a>
												</div>
											</div>
										</article>
									</div>
								</section>
							</div>
						</div>
						<script>
							function openCity(evt, cityName) {
								var i, tabcontent, tablinks;
								tabcontent = document.getElementsByClassName("tabcontent");
								for (i = 0; i < tabcontent.length; i++) {
								tabcontent[i].style.display = "none";
								}
								tablinks = document.getElementsByClassName("tablinks");
								for (i = 0; i < tablinks.length; i++) {
								tablinks[i].className = tablinks[i].className.replace(" active", "");
								}
								document.getElementById(cityName).style.display = "block";
								evt.currentTarget.className += " active";
							}
							// Get the element with id="defaultOpen" and click on it
							document.getElementById("defaultOpen").click();
						</script>
						<article class="row" id="mis_proyectos">
							<div class="col-12">
								<h4 class="heading-border"><span>Mis Proyectos</span></h4>
								<a class="btn btn-naranja my-2" href="<?= base_url('proyecto') ?>">Publicar nuevo proyecto</a>
							</div>
							<div class="col-lg-7 align-self-center">
								<span class="text-small">Mostrando <strong id="cantidad_proyectos"><?= $total_proyectos?></strong> de <strong><?= model('Proyectos')->where('id_contratista', session('id'))->countAllResults()?></strong> proyectos</span>
							</div>
							<div class="col-lg-5 text-lg-end mt-sm-15">
								<div class="d-flex" style="justify-content: space-between;">
									<?php if($permisos->getValor_permiso('historialtrabajos') == 0) :?>
										<div class="tooltips"><i style="color: #003B71; font-size:17px;" class="fas fa-info-circle info_proyecto" aria-hidden="true"></i>
											<span class="tooltiptext" style="top: -20px; left: 25px;">Adquiere un plan mayor ó hazte <a href="<?=base_url('tarifas-freelance')?>" target="_blank">PRÉMIUM</a> para ver tu <b class="text-naranja">historial de trabajos</b> finalizados</span>
										</div>
									<?php endif ;?>
									<span class="text-sortby" for="filtro-trabajos">Filtrar por estatus:</span>
									<div class="d-flex align-items-center gap-2">
										<select class="" type="button" name="filtro-trabajos" id="filtro-trabajos" >
											<option selected hidden value="sin">Seleccionar</option>
											<option value="todos">Todos</option>
											<option value="espera">Asignar</option>
											<option value="con_postulaciones">Con postulaciones</option>
											<option value="desarrollo">En progreso</option>
											<?php if($permisos->getValor_permiso('historialtrabajos') == 0) :?>
												<option disabled>Finalizado</option>
												<option disabled>Cancelado</option>
											<?php else :?>
												<option value="finalizado">Finalizado</option>
												<option value="cancelado">Cancelado</option>
											<?php endif ;?>
										</select>
									</div>
								</div>
							</div>
							<div class="list-recent-jobs" id="contenido_trabajos">
								<?=view_cell('App\Libraries\FuncionesSistema::misTrabajos')?>
							</div>
						</article>
					</div>
				</div>
				<!-- COLUMNA DERECHA -->
				<div class="col-lg-4 col-md-12 col-sm-12 col-12 pl-40 pl-lg-15 mt-lg-30 columna-der d-none d-lg-block">
					<div class="sidebar-shadow">
						<!-- <h5 class="font-bold">Overview</h5> -->
						<div class="sidebar-heading">
							<div class="avatar-sidebar">
								<figure><img class="img-perfil-icon" alt="mextemps" src="<?=imagenPerfil($contratista)?>" /></figure>
								<div class="sidebar-info">
									<span class="d-block titulo-name" id="nombre_freelancer_aside">
										<?=$contratista['nombre']?> <?=$contratista['apellidos']?>
									</span>
									<span class="d-block sidebar-website-text"><?=$contratista['correo']?></span>
									<?php if($contratista['cuenta'] == 'predeterminado') :?>
										<span class="d-block text-naranja opciones-ingresar actualizar-contrasena" data-bs-toggle="modal" data-bs-target="#actualizar_contraseña_modal">Actualizar contraseña</span>
									<?php endif ;?>
								</div>
							</div>
						</div>
						<!-- <div class="text-start mt-20">
							<a class="btn btn-primary mr-10">Cambiar mi contraseña</a>
						</div> -->
						<div class="sidebar-list-job pt-20">
							<h6 class="small-heading mb-20">Mi Suscripción</h6>
							<div class="sidebar-list-member sidebar-list-follower">
								<label class="text-font d-flex justify-content-between"><?=$descripcion?></label>
								<label class="d-flex justify-content-between text-font">Fecha de Inicio: <strong><?= isset($suscripcion_actual['fecha_inicio']) ? strftime('%d/%B/%Y', strtotime(explode(' ', $suscripcion_actual['fecha_inicio'])[0])) : view_cell('App\Libraries\FuncionesSistema::fechaInicioVencida') ?></strong></label>
								<label class="d-flex justify-content-between text-font">Vigente hasta: <strong><?= !empty($suscripcion_actual['fecha_fin']) ? strftime('%d/%B/%Y', strtotime(explode(' ', $suscripcion_actual['fecha_fin'])[0].'- 1 days')) : view_cell('App\Libraries\FuncionesSistema::fechaFinVencida') ?></strong></label>
								<?php if($con_suscripcion){ ?>
									<?php if($metodoPago != 'Sin plan'):?>
										<label class="d-flex justify-content-between align-items-center text-font mb-20">
											Pagado con: &nbsp; <span class="mb-0 <?= $metodoPago == 'tarjeta'? 'text-end' : '' ?>">
												<?php if($metodoPago == 'efectivo') :?>
													<label class="card p-1 contenedor_checks active m-0 ms-2" for="pagado_con">
														<input class="invisible" type="radio" name="pagado_con" hidden="" id="pagado_con">
														<div class="d-flex align-items-center">
															<div class="flex-shrink-0">
																<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 20 20" style="color: #3483FA;">
																	<path fill="currentColor" fill-rule="nonzero" d="M19.002 4.004v11.981h-18V4.004h18zm-1.2 1.2h-15.6v9.581h15.6V5.204zm-7.8 2.354a2.436 2.436 0 110 4.873 2.436 2.436 0 110-4.873zm0 1.2a1.236 1.236 0 10-.001 2.473 1.236 1.236 0 00.001-2.473zm4.806.03a1.206 1.206 0 11-.002 2.41 1.206 1.206 0 01.002-2.409zm-9.614 0a1.206 1.206 0 110 2.41 1.206 1.206 0 010-2.409z"></path>
																</svg>
																<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="color: #3483FA;">
																	<path fill="currentColor" fill-rule="evenodd" d="M11.999.632l9.751 5.67v3.447l-2.244.001v7.52h1.24l2.24 5.265H1.164l2.071-5.265h1.267V9.766l-2.253.004V6.302l9.749-5.67zm7.755 18.138H4.258l-.891 2.265h17.35l-.963-2.265zm-1.748-1.5V9.751l-5.257.006v7.511h5.257zm-6.757-7.51l-5.246.006v7.503h5.246V9.76zm.75-7.392L3.75 7.165v1.104l16.5-.018V7.165l-8.251-4.797z"></path>
																</svg>
															</div>
															<div class="flex-grow-1 ms-2">
																<label class="d-flex text-font m-0" style="font-size: 12px !important;">Efectivo</label>
															</div>
														</div>
													</label>
												<?php elseif($metodoPago == 'tarjeta') :?>
													Tarjeta "<?=$tarjeta_suscripcion['brand']?>"
													<span class="text-font fw-bold">Terminación <?= $tarjeta_suscripcion['lastFourDigits'] ?></span>
												<?php endif ;?>
											</span>
										</label>
									<?php endif;?>
								<?php } ?>
							</div>
						</div>
						<?php if(!empty($contratista['deleted_at'])) :?>
							<button class="btn btn-naranja btn-border text-font ms-2" data-bs-toggle="modal" data-bs-target="#recuperar_cuenta_modal">Recuperar cuenta</button>
						<?php else :?>
							<div class="d-grid gap-2">
								<button class="btn btn-naranja btn-border text-font ms-2" data-bs-toggle="modal" data-bs-target="#eliminar_cuenta_modal">Eliminar cuenta</button>
							</div>
						<?php endif ;?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- ************************** MODALES ************************** -->
	<div class="modal fade" id="cropImagePop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
					<h5 class="modal-title h5" id="myModalLabel">Ajustar imagen</h5>
				</div>
				<div class="modal-body">
					<div id="upload-demo" class="center-block"></div>
				</div>
				<div class="modal-footer">
					<button type="button" id="cropImageBtnPerfil" class="btn btn-naranja btn-border">Aceptar</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalasignar" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="exampleModalToggleLabel" tabindex="-1" aria-modal="true" role="dialog">
		<div class="modal-dialog modal-dialog-centered modal-lg">
			<div class="modal-content">
				<div class="modal-header border-0">
					<div class="text-center w-100">
						<h3 class="titulo-asigna-free">Asignar Proyecto</h3>
					</div>
					<a class="btn cerrar-modal px-3 py-2 desplegar-modal" data-show="" data-close="modalasignar"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body">
					<div class="text-center w-100">
						<p class="text-font ">
							Estas a punto de asignar un proyecto a un freelancer, te recomendamos establecer bien los puntos a acordar, al finalizar el proyecto, platícanos tu experiencia y califca al Freelancer que te atendió, esto nos ayuda a todos.
						</p>
					</div>
					<div class="p-3" id="asignar-proyecto">
					</div>
				</div>
				<!-- <div class="modal-footer border-0">
					<button type="button" class="btn btn-secondary " >Cerrar</button>
				</div> -->
			</div>
		</div>
	</div>
	<?php if(!empty($contratista['deleted_at'])) :?>
		<div class="modal fade" id="recuperar_cuenta_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content" style="background-color: transparent;">
					<div class="modal-header justify-content-center flex-column border-bottom-0">
						<h5 class="modal-title fw-bold mb-2">Restaurar cuenta</h5>
						<h6 class="fw-bold">¿Estás seguro?</h6>
						<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
					</div>
					<div class="modal-body pt-0">
						<div class="container">
							<div class="row align-items-center text-center">
								<p class="mb-0 text-font">Tu cuenta volverá a seguir en funcionamiento y accediendo a los beneficios disponibles de acorde a tu tipo de plan adquirido.</p>
								<div class="d-flex col-12 justify-content-end">
									<a class="btn btn-naranja" id="recuperar_cuenta">Recuperar cuenta</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php else :?>
		<div class="modal fade" id="eliminar_cuenta_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header justify-content-center flex-column border-bottom-0">
						<h5 class="modal-title fw-bold mb-2">Eliminar cuenta permanentemente</h5>
						<h6 class="fw-bold">¿Estás seguro?</h6>
						<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
					</div>
					<div class="modal-body pt-0">
						<div class="container">
							<div class="row align-items-center text-center">
								<p class="mb-0 text-font">Tu cuenta será suspendida durante un perdiodo de <b>60 días</b> si usted desea recuperarla, posteriormente de procederá a dar eliminar permanentemente su cuenta con todos sus datos.<br><br>Para formar parte del equipo de <span class="text-naranja">MEXTEMPS FREELANCE</span> tendrá que registrase nuevamente.</p>
								<div class="d-flex col-12 justify-content-end">
									<a class="btn btn-naranja text-font" id="baja_cuenta">Eliminar cuenta</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif ;?>
	<div class="modal fade" id="agregar-pago" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" style="max-width: 620px;">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0">
					<h5 class="modal-title fw-bold" id="titulo">Agregar método de pago</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body">
					<div class="container">
						<form class="row justify-content-center needs-validation" id="netpay-form" novalidate>
							<input type="hidden" name="token_id" id="token_id">
							<input type="hidden" name="deviceFingerPrint" id="deviceFingerPrint">
							<input type="hidden" name="referencia_cargo" id="referencia_cargo">
							<input type="hidden" name="token_transaccion" id="token_transaccion">
							<div class="text-center">
								<p class="fw-bold h6 text-start mt-3" style="display: inline-block; padding-right: 20px;">Tarjeta Débito/Crédito</p>
								<div class="flex-shrink-0" style="display: inline-block; vertical-align: bottom;">
									<img src="<?= base_url('assets/images/iconos/visa.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
									<img src="<?= base_url('assets/images/iconos/amex.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
									<img src="<?= base_url('assets/images/iconos/mastercard.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
								</div>
							</div>
							<div class="row justify-content-around mt-3">
								<div class="col-12">
									<div class="form-floating mb-3 ">
										<input class="form-control text-font" name="nombre" id="nombrePay" required>
										<label class="text-font" for="nombrePay">Nombre Titular</label>
									</div>
								</div>
								<div class="col-12">
									<div class="form-floating mb-3 ">
										<input class="form-control text-font binking__number-field numerico" autocomplete="cc-number" inputmode="numeric" pattern="[0-9 ]*" name="tarjeta" id="tarjetaPay" maxlength="16" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
										<label class="text-font" for="tarjetaPay">Número Tarjeta</label>
									</div>
								</div>
								<div class="col-12 mes-an-cvv">
									<dv class="row">
										<div class="col-12 col-md-4 filas-t1">
											<div class="form-floating ">
												<input class="form-control text-font binking__month-field numerico" name="mes" id="mesPay" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
												<label class="text-font" for="mesPay">Mes</label>
											</div>
										</div>
										<div class="col-12 col-md-4 filas-t2">
											<div class="form-floating ">
												<input class="form-control text-font binking__year-field numerico" name="año" id="añoPay" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
												<label class="text-font" for="añoPay">Año</label>
											</div>
										</div>
										<div class="col-12 col-md-4 filas-t3">
											<div class="form-floating ">
												<input class="form-control text-font binking__code-field numerico" type="password" name="cvv" id="cvvPay" maxlength="4" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
												<label class="text-font" for="cvvPay">CVV</label>
												<img class="referencia-card" src="<?=base_url('/assets/images/cvv.png')?>" style="position: relative; top: -44px; right: -100px;">
											</div>
										</div>
									</dv>
								</div>
							</div>
							<div class="col-12 my-3">
								<img src="" alt="">
								<button class="btn btn-naranja btn-border text-font" id="btn-agregarTarjeta" type="submit">Agregar </button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="obtener_membresia" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header border-bottom-0">
					<h5 class="modal-title fw-bold" id="titulo">Formas de pago</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row align-items-center text-center" id="contenido-tarjetas">
							<div id="selector">
                            <?php if(empty($tarjetas)) :?>
                                    <?= vacio('No tienes tarjetas disponibles')?>
                                <?php else :?>
                                    <?php foreach ($tarjetas as $key => $tarjeta) : ?>
                                        <?php if ($key == count($tarjetas) - 1) : ?>
                                            <div class="col-lg-12 mt-3 mb-1" id="contenedor-tarjetaObtenerSubs-<?= $tarjeta['card']['token'] ?>">
                                                <div class="card p-1">
                                                    <div class="d-flex align-items-center">
                                                        <div class="d-flex flex-shrink-0">
                                                            <img class="ms-3" id="imagen_modal" src="<?= model('autenticacion')->urlImgBanco($tarjeta['card']['brand']) ?>" alt="Procesador de pago bancario">
                                                        </div>
                                                        <div class="flex-grow-1 ms-3">
                                                            <label class="d-flex text-font fw-bold" id="terminacion_modal">Terminación <?= $tarjeta['card']['lastFourDigits'] ?></label>
															<?php if(!empty($tarjeta['card']['type']) && $tarjeta['card']['type'] != 'unknown') :?>
                                                            	<label class="d-flex text-font2" id="banco_modal"><?= $tarjeta['card']['bank'] ?></label>
															<?php endif ;?>
                                                            <label class="d-flex text-font2" id="vencimiento_modal">Vencimiento: <?= $tarjeta['card']['expMonth'] . '/' . $tarjeta['card']['expYear'] ?></label>
                                                        </div>
                                                        <div class="flex-grow-1 ms-3">
                                                            <a class="text-font justify-content-center user-select-none text-decoration-none text-naranja opciones-ingresar" id="tarj_modal" datos="<?= $tarjeta['card']['token'] ?>">Proceder al pago <i class="fas fa-chevron-right"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="d-flex col-12 justify-content-end">
                                                <a class="text-font2 opciones-ingresar" data-bs-toggle="modal" data-bs-target="#cambiar_tarjeta">Cambiar tarjeta</a>
                                            </div>
                                        <?php endif; ?>
									<?php endforeach; ?>
                                <?php endif ;?>
							</div>
							<div class="modal-header border-bottom-0 py-0">
								<h5 class="modal-title fw-bold" id="titulo">Opciones de pago</h5>
							</div>
							<div class="col-lg-12 my-2">
								<label class="card p-2 add-tarjeta opciones-ingresar" id="flujo_PagoTarjeta">
									<div class="d-flex align-item-center">
										<div class="flex-shrink-0 align-self-center">
											<img src="<?=base_url('/assets/images/iconos/visa.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
											<img src="<?=base_url('/assets/images/iconos/amex.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
											<img src="<?=base_url('/assets/images/iconos/mastercard.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
										</div>
										<div class="flex-grow-1 ms-3">
											<p class="d-flex text-font fw-bold m-0">Agregar tarjeta</p>
										</div>
									</div>
								</label>
							</div>
							<div class="col-lg-12 my-2" id="columna_tienda">
								<label class="card p-2 contenedor_checks" for="metodo_tienda">
									<input class="check-otrosPagos" type="radio" name="otros_pagos" hidden id="metodo_tienda" value="efectivo">
									<div class="d-flex align-items-center">
										<div class="flex-shrink-0">
											<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 20 20" style="color: #3483FA;">
												<path fill="currentColor" fill-rule="nonzero" d="M19.002 4.004v11.981h-18V4.004h18zm-1.2 1.2h-15.6v9.581h15.6V5.204zm-7.8 2.354a2.436 2.436 0 110 4.873 2.436 2.436 0 110-4.873zm0 1.2a1.236 1.236 0 10-.001 2.473 1.236 1.236 0 00.001-2.473zm4.806.03a1.206 1.206 0 11-.002 2.41 1.206 1.206 0 01.002-2.409zm-9.614 0a1.206 1.206 0 110 2.41 1.206 1.206 0 010-2.409z"></path>
											</svg>
											<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="color: #3483FA;">
												<path fill="currentColor" fill-rule="evenodd" d="M11.999.632l9.751 5.67v3.447l-2.244.001v7.52h1.24l2.24 5.265H1.164l2.071-5.265h1.267V9.766l-2.253.004V6.302l9.749-5.67zm7.755 18.138H4.258l-.891 2.265h17.35l-.963-2.265zm-1.748-1.5V9.751l-5.257.006v7.511h5.257zm-6.757-7.51l-5.246.006v7.503h5.246V9.76zm.75-7.392L3.75 7.165v1.104l16.5-.018V7.165l-8.251-4.797z"></path>
											</svg>
										</div>
										<div class="flex-grow-1 ms-3">
											<p class="d-flex text-font fw-bold m-0">Efectivo</p>
										</div>
									</div>
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if($con_suscripcion && $metodoPago != 'cupon') :?>
		<div class="modal fade" id="cambiar_metodo_pago" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="tituloCambiopago" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header justify-content-center border-bottom-0">
						<h5 class="modal-title fw-bold" id="tituloCambiopago">Cambio de pago</h5>
						<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
					</div>
					<div class="modal-body">
						<div class="container">
							<div class="row align-items-center text-center" id="contenido-tarjetas">
								<?php if($metodoPago != 'tarjeta') :?>
									<div class="d-flex col-12 justify-content-start">
										<a class="text-font text-naranja opciones-ingresar" data-bs-toggle="modal" data-bs-target="#agregar-pago">Agregar tarjeta</a>
									</div>
									<?php foreach ($tarjetas as $key => $tarjeta) : ?>
										<?php if(!empty($tarjeta_suscripcion)):?>
											<?php if($tarjeta['card']['token'] == $tarjeta_suscripcion['token']): continue; endif;?>
										<?php endif ;?>
										<?php if ($key == count($tarjetas) - 1) : ?>
											<div class="col-lg-12 mt-3 mb-1" id="contenedor-tarjeta-<?= $tarjeta['card']['token'] ?>">
												<div class="card p-1">
													<div class="d-flex align-items-center">
														<div class="d-flex flex-shrink-0">
															<img class="ms-3" id="imagen_modalCambiopago" src="<?= model('autenticacion')->urlImgBanco($tarjeta['card']['brand']) ?>" alt="Procesador de pago bancario">
														</div>
														<div class="flex-grow-1 ms-3">
															<label class="d-flex text-font fw-bold" id="terminacion_modalCambiopago">Terminación <?= $tarjeta['card']['lastFourDigits'] ?></label>
															<label class="d-flex text-font2" id="banco_modalCambiopago"><?= $tarjeta['card']['bank'] ?></label>
															<label class="d-flex text-font2" id="vencimiento_modalCambiopago">Vencimiento: <?= $tarjeta['card']['expMonth'] . '/' . $tarjeta['card']['expYear'] ?></label>
														</div>
														<div class="flex-grow-1 ms-3">
															<a class="text-font justify-content-center user-select-none text-decoration-none text-naranja opciones-ingresar" id="tarj_modalCambiopago" datos="<?= $tarjeta['card']['token'] ?>">Continuar <i class="fas fa-chevron-right"></i></a>
														</div>
													</div>
												</div>
											</div>
											<div class="d-flex col-12 justify-content-end">
												<a class="text-font2 opciones-ingresar" data-bs-toggle="modal" data-bs-target="#cambiar_tarjetaCambiopago">Cambiar tarjeta</a>
											</div>
										<?php endif; ?>
									<?php endforeach; ?>
								<?php endif; ?>
								<?php if($metodoPago != 'efectivo') :?>
									<div class="col-lg-12 my-2">
										<label class="card p-2 contenedor_checks" for="metodo_tiendaCambiopago">
											<input class="check-otrosPagosCambiopago" type="radio" name="Cambiopago" hidden id="metodo_tiendaCambiopago" value="efectivo">
											<div class="d-flex align-items-center">
												<div class="flex-shrink-0">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 20 20" style="color: #3483FA;">
														<path fill="currentColor" fill-rule="nonzero" d="M19.002 4.004v11.981h-18V4.004h18zm-1.2 1.2h-15.6v9.581h15.6V5.204zm-7.8 2.354a2.436 2.436 0 110 4.873 2.436 2.436 0 110-4.873zm0 1.2a1.236 1.236 0 10-.001 2.473 1.236 1.236 0 00.001-2.473zm4.806.03a1.206 1.206 0 11-.002 2.41 1.206 1.206 0 01.002-2.409zm-9.614 0a1.206 1.206 0 110 2.41 1.206 1.206 0 010-2.409z"></path>
													</svg>
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="color: #3483FA;">
														<path fill="currentColor" fill-rule="evenodd" d="M11.999.632l9.751 5.67v3.447l-2.244.001v7.52h1.24l2.24 5.265H1.164l2.071-5.265h1.267V9.766l-2.253.004V6.302l9.749-5.67zm7.755 18.138H4.258l-.891 2.265h17.35l-.963-2.265zm-1.748-1.5V9.751l-5.257.006v7.511h5.257zm-6.757-7.51l-5.246.006v7.503h5.246V9.76zm.75-7.392L3.75 7.165v1.104l16.5-.018V7.165l-8.251-4.797z"></path>
													</svg>
												</div>
												<div class="flex-grow-1 ms-3">
													<p class="d-flex text-font fw-bold m-0">Efectivo</p>
												</div>
											</div>
										</label>
									</div>
								<?php endif ;?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="cambiar_tarjetaCambiopago" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="tituloCambioPago_tarjeta" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header justify-content-center border-bottom-0">
						<h5 class="modal-title fw-bold" id="tituloCambioPago_tarjeta">Seleccione tarjeta</h5>
						<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#cambiar_metodo_pago"><i class="fas fa-times" aria-hidden="true"></i></a>
					</div>
					<div class="modal-body">
						<div class="container">
							<div class="row align-items-center text-center">
                                <?php if(empty($tarjetas)) :?>
                                    <?= vacio('No tienes tarjetas disponibles')?>
                                <?php else :?>
                                    <?php foreach ($tarjetas as $key => $tarjeta) : ?>
                                        <div class="col-lg-12 my-2" tarjeta="<?= $tarjeta['card']['token'] ?>" id="contenedor-tarjetaCambiopago-<?= $tarjeta['card']['token'] ?>">
                                            <label class="card p-1 contenedor_checks" for="<?= $tarjeta['card']['token'] ?>">
                                                <input class="check-tarjetasCambiopago" type="radio" name="tarjetas_usuarios_radioCambiopago" hidden id="<?= $tarjeta['card']['token'] ?>">
                                                <div class="d-flex align-items-center">
                                                    <div class="d-flex flex-shrink-0">
                                                        <img class="ms-3" src="<?= model('autenticacion')->urlImgBanco($tarjeta['card']['brand']) ?>" alt="Procesador de pago bancario">
                                                    </div>
                                                    <div class="flex-grow-1 ms-3">
                                                        <p class="d-flex text-font fw-bold mb-1">Terminación <?= $tarjeta['card']['lastFourDigits'] ?></p>
                                                        <p class="d-flex text-font2 mb-1"><?= $tarjeta['card']['bank'] ?></p>
                                                        <p class="d-flex text-font2 mb-1">Vencimiento: <?= $tarjeta['card']['expMonth'] . '/' . $tarjeta['card']['expYear'] ?></p>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif ;?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif ;?>
	<div class="modal fade" id="cambiar_tarjeta" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0">
					<h5 class="modal-title fw-bold" id="titulo">Seleccione tarjeta</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#obtener_membresia"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body" >
					<div class="container">
						<div class="row align-items-center text-center" id="listado-tarjetas" style="max-height: 450px; overflow: auto;">
                            <?php if(empty($tarjetas)) :?>
                                <?= vacio('No tienes tarjetas disponibles')?>
                            <?php else :?>
                                <?php foreach ($tarjetas as $key => $tarjeta) : ?>
                                    <div class="col-lg-12 my-3 sel-tar" tarjeta="<?= $tarjeta['card']['token'] ?>">
                                        <label class="card p-1 contenedor_checks" for="<?= $tarjeta['card']['token'] ?>">
                                            <input class="invisible check-tarjetas" type="radio" name="tarjetas_usuarios_radio" hidden id="<?= $tarjeta['card']['token'] ?>-mem">
                                            <div class="d-flex align-items-center">
                                                <div class="d-flex flex-shrink-0">
                                                    <img class="ms-3" src="<?= model('autenticacion')->urlImgBanco($tarjeta['card']['brand']) ?>" alt="Procesador de pago bancario">
                                                </div>
                                                <div class="flex-grow-1 ms-3">
                                                    <p class="d-flex text-font fw-bold">Terminación <?= $tarjeta['card']['lastFourDigits'] ?></p>
                                                    <p class="d-flex text-font2"><?= $tarjeta['card']['bank'] ?></p>
                                                    <p class="d-flex text-font2">Vencimiento: <?= $tarjeta['card']['expMonth'] . '/' . $tarjeta['card']['expYear'] ?></p>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif ;?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="tarjetaResumen_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0">
					<h5 class="modal-title fw-bold">Resumen de compra con tarjeta</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#obtener_membresia"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body pt-0">
					<div class="container">
						<div class="row align-items-center">
							<div class="col-12 mt-3" id="suscripcion_seleccionada_tarjeta"></div>
							<div class="seccion-cupon my-2">
								<?= view('front/viewcells/boton-cupon') ?>
							</div>
							<div class="d-flex justify-content-end gap-2">
								<div class="contenedor_cvv">
									<input class="border border-secondary text-font numerico" id="cvv2" name="cvv2" type="password" placeholder="cvv" maxlength="4" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
									<img src="<?=base_url('assets/images/cvv.png')?>" alt="codigo de seguridad tarjeta">
								</div>
							</div>
							<div class="col-12 text-end mt-3">
								<a class="btn btn-naranja" id="btn_confirm_tarjeta">Confirmar compra</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade pagos" id="efectivo_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo_efectivo" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0 pb-0">
					<h5 class="modal-title fw-bold" id="titulo_efectivo">Resumen de compra en efectivo</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#obtener_membresia"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body pt-0">
					<div class="container">
						<div class="row align-items-center">
							<div class="col-12 mt-3" id="suscripcion_seleccionada_efectivo"></div>
							<div class="seccion-cupon">
								<?= view('front/viewcells/boton-cupon') ?>
							</div>
							<div class="col-12 text-end mt-3">
								<a class="btn btn-naranja" id="btn_confirm_efectivo">Confirmar compra</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="respuesta_pago" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0">
					<h5 class="modal-title fw-bold" id="titulo">¡Un paso más!</h5>
					<a class="btn cerrar-modal px-3 py-2" script="location.reload()"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row align-items-center text-center">
							<div class="col-12">
							<p class="h6 fw-normal mb-2">Número de referencia: <span class="fw-bold" id="referencia"></span>.</p>
							<label class="text-font mb-2">Te hemos enviado un correo con los pasos a seguir para poder proceder con la suscripción a tu cuenta. Guarda este número de referencia, lo necesitarás para pagar tu plan.</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <?php if($contratista['cuenta'] == 'predeterminado') :?>
        <div class="modal fade" id="actualizar_contraseña_modal" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="actualizar_contra" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header justify-content-center border-bottom-0">
                        <h5 class="modal-title fw-bold" id="actualizar_contra">Actualizar contraseña</h5>
                        <a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
                    </div>
                    <form class="modal-body needs-validation" novalidate encytpe="multipart/form-data" id="form_actualizar_contrasena">
                        <div class="form-floating mb-2">
                            <input type="password" class="form-control" id="contrasena_actual" name="contrasena_actual" required>
                            <label for="contrasena_actual">Contraseña actual</label>
                            <div class="invalid-tooltip text-font2">Requerido</div>
                        </div>
                        <div class="form-floating mb-2">
                            <input type="password" class="form-control" id="contrasena_nueva" name="contrasena_nueva" required>
                            <label for="contrasena_nueva">Nueva contraseña</label>
                            <div class="invalid-tooltip text-font2">Requerido</div>
                        </div>
                        <div class="form-floating mb-2">
                            <input type="password" class="form-control" id="confirmar_contrasena" name="confirmar_contrasena" required>
                            <label for="confirmar_contrasena">Confirmar contraseña</label>
                            <div class="invalid-tooltip text-font2">Requerido</div>
                        </div>
                        <button class="btn btn-naranja btn-border mt-3" type="button" id="btn_guardar_contrasena">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
    <?php endif ;?>
	<!-- Solicitar factura -->
	<div class="modal fade" id="solicitar-factura" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo-modal-factura" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0">
					<h5 class="modal-title fw-bold" id="titulo-modal-factura">Solicitud de factura</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body">
					<form id="form-solicitud-factura">
					</form>
				</div>
			</div>
		</div>
	</div>
	<script>
		let _this = this;
		const respuesta_3ds = function(_this, referenceId) {
			console.log('referenceId: ' + referenceId);
			charges(referenceId);
		}

		const charges = function(referenceId) {
			console.log('charges: ' + referenceId);
			$('#referencia_cargo').val(referenceId);
		}
		
		const confirm = function(processorTransactionId) {
			console.log('confirm: ' + processorTransactionId);
			$.ajax({
				url: '<?= base_url('Pagos/confirmar_pago') ?>',
				type: 'POST',
				data: {transaccion: $('#token_transaccion').val(), procesador: processorTransactionId, usuario: <?=session('id')?>},
				success: function(data) {
					let respuesta = JSON.parse(data);
					$('#tarjetaResumen_modal').modal('hide');
					$('#btn_confirm_tarjeta').removeClass('disabled').children('span').remove();
					$('input[name="otros_pagos"]').prop('checked', false).parent().removeClass('active');
					$('#obtener_membresia .cerrar-modal').click();
					$('.modal').modal('hide');
					$('#listado-historial-pago').load('/usuarios/HistorialPagos');

					if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta);

					alertify.notify(respuesta.mensaje, respuesta.alerta);
					return setTimeout((e) => location.href = "/compra_realizada?token="+respuesta.datos.transactionTokenId, 3000);
				},
				error: function(data) {
					let respuesta = JSON.parse(data.responseText);
					$('#btn_confirm_tarjeta').removeAttr('disabled').children('span').remove();
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
				}
			});
		}

		const callbackProceed = function(_this, processorTransactionId) {
			alertify.notify('processorTransactionId: ' + processorTransactionId, 'custom');
			confirm(processorTransactionId);
		}

		//La generación del device fingerprint debe de estar al momento en que se visualiza el formulario de pago
		function generateDevice(callback) {
			deviceFingerPrint = NetPay.form.generateDeviceFingerPrint();
			callback();
		}
		$('#filtrar-freelance').click(function(){
			$('#area-freelancers').fadeOut();
			$('#icono-loading').fadeIn();
			$('#area-freelancers').siblings().removeClass('d-none');
			fetch('<?=base_url('usuarios/resultFiltroFreelance')?>', {
				method : 'POST',
				body : $('#filtro-freelance').serialize(),//+"&cantidad="+$('.page-link[paginacion]').attr('paginacion'),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
			.then(respuesta => respuesta.text())
			.then(datos => {
				$('#icono-loading').fadeOut();
				$('#area-freelancers').fadeIn();
				$('#area-freelancers').html(datos);
			})
			.catch(error => alertify.notify('Error: '.error, 'falla', 10));
		});
		$('#limpiar-filtro').on('click', function(){
			$(this).addClass('disabled');
			$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);
			$('#palabra-clave').val('');
			$("#area").val('').trigger('change');
			$('#habilidad').val('').trigger('change');
			$('#ubicacion').val('');
			$('#inputidioma').val('').trigger('change');
			$('input[name="estrellas"]').prop('checked', false);
			$('input[name="concluidos"]').val(0);
			$('#smin').val('<?=view_cell('App\Libraries\FuncionesSistema::sueldoMinimo')?>');
			$('#smax').val('<?=view_cell('App\Libraries\FuncionesSistema::sueldoMaximo')?>');
			$(this).removeClass('disabled').children('span').remove();
		});
		$(document).ready(function () {
			let deviceFingerPrint;
			<?php $netpay = new NetPay();?>
			<?= $netpay->getModo() ? 'NetPay.setSandboxMode(true); netpay3ds.setSandboxMode(true);' : 'NetPay.setSandboxMode(false); netpay3ds.setSandboxMode(false);' ?> // Esta línea se debe de llamar antes de la generación del device fingerprint

			// SE OBTIENE REFERENCEID POR 3DS
			netpay3ds.init(() => netpay3ds.config(_this, 100, respuesta_3ds));
			
			$(".noUi-handle").on("click", function () {
				$(this).width(50);
			});
			var rangeSlider = document.getElementById("slider-range");
			var moneyFormat = wNumb({
				decimals: 0,
				thousand: ",",
				prefix: "$"
			});
			noUiSlider.create(rangeSlider, {
				start: [0, <?= $sueldo_max?>],
				step: 1,
				range: {
					min: [0],
					max: [<?= $sueldo_max?>]
				},
				format: moneyFormat,
				connect: true
			});
	
			// Set visual min and max values and also update value hidden form inputs
			rangeSlider.noUiSlider.on("update", function (values, handle) {
				$(".min-value-money").val(values[0]);
				$(".max-value-money").val(values[1]);
				$(".min-value").val(moneyFormat.from(values[0]));
				$(".max-value").val(moneyFormat.from(values[1]));
			});
			
			let searchInput = 'ubicacion';
			$('#icono-loading').hide();
			let autocomplete;
			autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
				types: ['geocode'],
					componentRestrictions: {
					country: "MEX",
				}
			});
			google.maps.event.addListener(autocomplete, 'place_changed', function () {
				var near_place = autocomplete.getPlace();
				var address = near_place.address_components;
				address.forEach( function(valor, indice, array) {
					if(address[indice]['types'][0] == 'locality'){
						$('#ubicacion').val(address[indice]['long_name'])
					}
				});
			});
		});
		$('.solicitar-factura').click(function(){
		let id = $(this).attr('data-id');
			$.ajax({
				type:'POST',
				url: '<?=base_url('/Pagos/datosFacturacionUsuario')?>',
				data:{id:id},
				success: function(data){
					let cont = JSON.parse(data);
					if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, 'advertencia');
					$('#form-solicitud-factura').html(cont.mensaje);
					return $('#solicitar-factura').modal('show');
				}, error: function(data){
					let respuesta = JSON.parse(data.responseText);
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
				}
			});
		});
		$('#area').on('change', function(){
		let area = $(this).val();
			$.ajax({
				type: 'POST',
				url: '<?=base_url('/Habilidades/subareas')?>',
				data:{area:area},
				success: function(data){
					$('#habilidad').html(data);
				}, error: function(data){
				let respuesta = JSON.parse(data.responseText);
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
				}
			});
		})

		$('#btn-agregarTarjeta').click(function(evento) {
			if(evento.currentTarget.form.nombrePay.value.length == 0 || evento.currentTarget.form.tarjetaPay.value.length == 0 ||
				evento.currentTarget.form.mesPay.value.length == 0 || evento.currentTarget.form.cvvPay.value.length == 0) return true;
	
			evento.preventDefault();
			$(this).attr('disabled', true);
			$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);

			generateDevice(function() { 
				let tarjeta = {
					cardNumber: evento.currentTarget.form.tarjetaPay.value,
					expMonth: evento.currentTarget.form.mesPay.value,
					expYear: evento.currentTarget.form.añoPay.value,
					cvv2: evento.currentTarget.form.cvvPay.value,
					// cardHolderName: evento.currentTarget.form.nombrePay.value
					vault: true,
					simpleUse:false,
					deviceFingerPrint : deviceFingerPrint
				};
				NetPay.setApiKey('<?= $netpay->getLlave() ?>');
				var validateNumberLength = NetPay.card.validateNumberLength(tarjeta.cardNumber);
				var validateNumber = NetPay.card.validateNumber(tarjeta.cardNumber);
				var validateExpiry = NetPay.card.validateExpiry(tarjeta.expMonth, tarjeta.expYear);
				var validateCVV = NetPay.card.validateCVV(tarjeta.cvv2, tarjeta.cardNumber);

				if (!validateNumberLength || !validateNumber){
					$('#btn-agregarTarjeta').removeAttr('disabled').children('span').remove();
					return alertify.notify('Número de tarjeta incorrecto', 'advertencia', 10);
				} 
				if (!validateExpiry){
					('#btn-agregarTarjeta').removeAttr('disabled').children('span').remove();
					return alertify.notify('Expiración de tarjeta incorrecto', 'advertencia', 10);
				}
				if (!validateCVV){
					$('#btn-agregarTarjeta').removeAttr('disabled').children('span').remove();
					return alertify.notify('Código de seguridad incorrecto', 'advertencia', 10);
				}

				NetPay.token.create(tarjeta, correcto, falla);

				function correcto(respuesta) {
					console.log("Token creado correctamente");
					let datos_token = JSON.parse(respuesta.message.data);
					console.log(datos_token);
					$('#token_id').val(datos_token.token);
					$.ajax({
						url: '<?= base_url('Pagos/guardarTarjetas')?>',
						type: 'POST',
						data: $('#netpay-form').serialize(),
						success: function (data) {
							let respuesta = JSON.parse(data);
							$('#btn-agregarTarjeta').removeAttr('disabled').children('span').remove();
			
							if(respuesta.alerta == 'custom') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
							$('#agregar-pago').modal('hide');
							$('#netpay-form')[0].reset();
							if($('#flujo_PagoTarjeta').is('[agregar]')){
								añadirTarjeta(respuesta.tarjeta);
								$('#obtener_membresia').modal('show');
								$('#flujo_PagoTarjeta').removeAttr('agregar');
							}else setTimeout(function(){ location.reload() }, 2000);
							return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
						},
						error: function(data){
							let respuesta = JSON.parse(data.responseText);
							alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
						}
					});
				}

				function falla(error) {
					console.trace(error);
					let mensaje = error.message == 'Empty or invalid card number' ? 'Número de tarjeta inválido' : error.message;
					alertify.notify("ERROR [" + mensaje + "]", 'falla', 10);
					$('#btn-agregarTarjeta').removeAttr('disabled').children('span').remove();
				}
			});
		});
		
		$('#btn_confirm_tarjeta').click(function(){
			let cvv2 = $('#cvv2').val().trim()
			if(cvv2.length == 0) return alertify.notify('Ingrese el código de seguridad de su tarjeta', 'custom');

			$this = $(this);
			generateDevice(function() {
				let datos = {deviceFingerPrint : deviceFingerPrint}
				$('#deviceFingerPrint').val(datos.deviceFingerPrint);
			});

			$(this).addClass('disabled');
			$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);
			if (alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro de realizar tu compra?', function() {
				$.ajax({
					url: '/Pagos/pagar/<?=session('id')?>',
					type: 'POST',
					data: {
						cvv : cvv2,
						deviceFingerPrint : $('#deviceFingerPrint').val(),
						referencia_cargo : $('#referencia_cargo').val(),
						plan : $('.plan_seleccionado').val(),
						datos : $this.attr('datos'),
						cupon: $('#tarjetaResumen_modal .cupon_ingresado').val()
					},
					success: function(data) {
						let respuesta = JSON.parse(data);
						$('#tarjetaResumen_modal').modal('hide');
						$($this).removeClass('disabled').children('span').remove();
						$('input[name="otros_pagos"]').prop('checked', false).parent().removeClass('active');
						$('#obtener_membresia .cerrar-modal').click();
						$('.modal').modal('hide');
						$('#listado-historial-pago').load('/usuarios/HistorialPagos');

						if (respuesta.alerta == 'custom'){
							$('#token_transaccion').val(respuesta.datos.transactionTokenId);
							let canProceed = netpay3ds.canProceed(respuesta.datos.status, respuesta.datos.threeDSecureResponse.responseCode, respuesta.datos.threeDSecureResponse.acsUrl);
							console.log("canProceed es: " + canProceed);
							
							if (!canProceed) return alertify.notify('Transacción rechadaza', 'falla');
							
							let proceed = netpay3ds.proceed(_this, respuesta.datos.threeDSecureResponse.acsUrl, respuesta.datos.threeDSecureResponse.paReq, respuesta.datos.threeDSecureResponse.authenticationTransactionID, callbackProceed);
						}
						
						alertify.notify(respuesta.mensaje, respuesta.alerta);
						return setTimeout((e) => location.href = "/compra_realizada?token="+respuesta.datos.transactionTokenId, 3000);
					},
					error: function(data) {
						let respuesta = JSON.parse(data.responseText);
						alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
					}
				});
			}, function() { 
				$($this).removeClass('disabled').children('span').remove();
			}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
		});
		$('#card-mis-tarjetas').delegate('.eliminar-tarjeta', 'click', function() {
			let dato = $(this).attr('datos');
			let tarjeta = $(this).attr('tarjeta');
			let btnEliminar = $(this);
			if (alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro de eliminar tarjeta con terminación <b class="fw-bold">' + tarjeta + '</b>?', function() {
				$.ajax({
					url: '<?= base_url('Pagos/eliminarTarjeta') ?>',
					type: 'POST',
					data: { id: btnEliminar.attr('datos'), usuario : <?=session('id')?> },
					success: function(data) {
						let respuesta = JSON.parse(data);
						//Eliminamos tarjetas en Obtener Membresia, Cambiar Pago y al elegir
						btnEliminar.parents('.contenedor-tarjeta').remove();
						$('.sel-tar[tarjeta="'+dato+'"]').remove();
						$('#contenedor-tarjeta-'+btnEliminar.attr('datos')).remove();
						$('#contenedor-tarjetaCambiopago-'+btnEliminar.attr('datos')).remove();
						$('#contenedor-tarjetaObtenerSubs-'+btnEliminar.attr('datos')).remove();
						return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					},
					error: function(data) {
						let respuesta = JSON.parse(data.responseText);
						if(respuesta.message == 'Can not delete, It is used by Subscription'){
							respuesta.message = 'No se puede eliminar, se está usando en una suscripcion activa.\nPuede cambiar de tarjeta en la suscripción'
							return alertify.notify('Error: ' + respuesta.message, 'advertencia', 10);
						}
						alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
					}
				});
			}, function(){}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
		});
		
		$('#baja_cuenta').click(function(evento){
			$this = $(this);
			evento.preventDefault();
			$($this).addClass('disabled');
			$($this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo($this);
			$.ajax({
				url: '<?=base_url('usuarios/eliminar_cuenta')?>',
				type: 'POST',
				success: function (data) {
					let respuesta = JSON.parse(data);
					$($this).removeClass('disabled').children('span').remove();
					if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					$('#eliminar_cuenta_modal').modal('hide');
					alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					setTimeout(function(){ location.reload() }, 2000);
				},
				error: function(data){
					let respuesta = JSON.parse(data.responseText);
					$($this).removeClass('disabled').children('span').remove();
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
				}
			});
		});
		
		$('#recuperar_cuenta').click(function(evento){
			$this = $(this);
			evento.preventDefault();
			$($this).addClass('disabled');
			$($this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo($this);
			$.ajax({
				url: '<?=base_url('usuarios/recuperar_cuenta')?>',
				type: 'POST',
				contentType: false,
				processData: false,
				data: {},
				success: function (data) {
					let respuesta = JSON.parse(data);
					$($this).removeClass('disabled').children('span').remove();
					if(respuesta.alerta == 'falla') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					$('#recuperar_cuenta_modal').modal('hide');
					alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					setTimeout(function(){ location.reload() }, 2000);
				},
				error: function(data){
					let respuesta = JSON.parse(data.responseText);
					$($this).removeClass('disabled').children('span').remove();
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
				}
			});
		});
		
		$('.check-otrosPagos').change(function(e) {
			$this = $(this);
			$($this).parent().addClass('active');
			$('.check-otrosPagos').not(':checked').parent().removeClass('active');

			$.ajax({
				url: '<?= base_url('Suscripciones/getPlan')?>',
				type: 'POST',
				data: {id : $('.plan_seleccionado').val(), es_tarjeta: false},
				success: function (data) {
					let respuesta = JSON.parse(data);
					if(respuesta.alerta == 'falla') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					$('#suscripcion_seleccionada_'+$($this).val()).html(respuesta.mensaje);
					
					$('#obtener_membresia').modal('hide');
					return $('#' + $($this).val() + '_modal').modal('show');
				},
				error: function(data){
					alertify.notify('Network', 'falla');
				}
			});
			$('#obtener_membresia').modal('hide');
			$('#' + $(this).val() + '_modal').modal('show');
		});

		$('#actualizar-perfil').click(function(e) {
			if(e.target.form.calle.value.length == 0 || e.target.form.colonia.value.length == 0 ||
			e.target.form.municipio.value.length == 0 || e.target.form.estado.value.length == 0 ||
			e.target.form.codigo_postal.value.length == 0 || e.target.form.curp.value.length == 0){ 
				return true;
			}

			e.preventDefault();
			let form = new FormData(document.getElementById("form-editarPerfil"));
			$.ajax({
				url: '<?= base_url('usuarios/editarPerfil')?>',
				type: 'POST',
				contentType: false,
				processData: false,
				data: form,
				success: function (data) {
					let respuesta = JSON.parse(data);
					if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					
					$('#form-editarPerfil').removeClass('was-validated');
					$('#nombre_freelancer, #nombre_freelancer_aside').text(e.target.form.nombre.value+' '+e.target.form.apellidos.value);
					return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				},
				error: function(data){
					let respuesta = JSON.parse(data.responseText);
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
				}
			});
		});
		
		$('#btn_guardar_contrasena').click(function(e){
			if(e.target.form.contrasena_actual.value.length == 0 || e.target.form.contrasena_nueva.value.length == 0 || e.target.form.confirmar_contrasena.value.length == 0) return true;
			
			e.preventDefault();
			let p = document.getElementById('contrasena_nueva').value,
				errors = [];
			if (p.length < 8) {
				errors.push("La contraseña debe tener un mínimo de 8 caracteres.");
			}
			if (p.search(/[a-z]/i) < 0) {
				errors.push("La contraseña debe tener al menos una letra."); 
			}
			if(p.search(/[A-Z]/) < 0){
				errors.push("La contraseña debe tener al menos una letra mayúscula.");
			}
			if(p.search(/[!@#$%^&*.,?]/) < 0){
				errors.push("La contraseña debe tener al menos un caracter especial. como: ( *, &, #, ?, !)");
			}
			if (p.search(/[0-9]/) < 0) {
				errors.push("La contraseña debe tener al menos un número.");
			}
			if(errors.length <= 0){
				console.log('Contraseña válida');
			}else{
				let cadena = '';
				errors.forEach(element => cadena = cadena + element + '<br>');
				return alertify.notify(cadena, 'advertencia', 10);
			}
			if(e.target.form.contrasena_nueva.value != e.target.form.confirmar_contrasena.value) return alertify.notify('Contraseña a cambiar no coinciden', 'advertencia', 10);

			$this = $(this);
			$this.addClass('disabled');
			$this.add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo($this);
			$.ajax({
				type: 'POST',
				url: '<?=base_url('cambio_accesos')?>',
				data: $('#form_actualizar_contrasena').serialize(),
				success: function(data){
					let datos = JSON.parse(data);
					$this.removeClass('disabled').children('span').remove();
					if(datos.alerta != 'correcto') return alertify.notify(datos.mensaje, datos.alerta);
					
					$('#form_actualizar_contrasena')[0].reset();
					$('#actualizar_contraseña_modal').modal('hide');
					return alertify.notify(datos.mensaje, datos.alerta);
				}, error: function(error){
					let respuesta = JSON.parse(error.responseText);
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
				}
			});
		});
		
		$('#filtro-trabajos').change(function(){ filtrarMisTrabajos($('#filtro-trabajos').val()); });
		
		$('.btn-listados').click(function(){
			let estatus = $(this).attr('data-tipo');
			filtrarMisTrabajos(estatus);
			$('#filtro-trabajos').children().attr('selected', false).parent().children('option[value="'+estatus+'"]').attr('selected', true);
		});
		
		$('#flujo_PagoTarjeta').click(function(){
			$(this).attr('agregar', true);
			$('#obtener_membresia').modal('hide');
			$('#agregar-pago').modal('show');
		});
		$('#agregar-tarjeta').click(function(){
			$this = $(this);
			$.ajax({
				type: 'POST',
				url: '<?= base_url('/Autenticacion/validarDireccion') ?>',
				data:{d:1},
				success: function(data){
					let cont = JSON.parse(data);
					if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, cont.tipo);
					
					$($this).attr('agregar', true);
					$('#obtener_membresia').modal('hide');
					$('#agregar-pago').modal('show');
				}, error: function(){
					let respuesta = JSON.parse(data.responseText);
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
				}
			});
		});
		
		function filtrarMisTrabajos(estatus){
			$.ajax({
				url: '<?=base_url('Proyectos/get_trabajos_finalizadosFiltro')?>/'+estatus,
				type: 'POST',
				success: function (data) {
					let respuesta = JSON.parse(data);
					$('#cantidad_proyectos').text(respuesta.cantidad);
					$('#contenido_trabajos').html(respuesta.trabajos);
				},
				error: function(data){
					let respuesta = JSON.parse(data.responseText);
					$($this).removeClass('disabled').children('span').remove();
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
				}
			});
		}
		function añadirTarjeta(id, tipo){
			$.ajax({
				type: 'POST',
				url: '<?=base_url('/Pagos/get_tarjeta_user')?>',
				data:{id:id, tipo:tipo},
				success: function(data){
					let respuesta = JSON.parse(data);
					if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta);

					$('#selector').html(respuesta.selector);
					$('#listado-tarjetas').empty().append(respuesta.listado);
					
					if($('#card-mis-tarjetas').children('.empty-img').length > 0){ //SI NO HAY TARJETAS
						$('#card-mis-tarjetas').children('.empty-img').remove();
						$('#card-mis-tarjetas div').remove();
					} 
					return $('#card-mis-tarjetas').append(respuesta.mis_tarjetas);
				}, error: function(data){
					alertify.error('Ha surgido un error', 10);
				}
			});
		}
		function verificarEstSoli(id){
			let fre = $('#est-free').val();
			let contra = $('#est-cont').val();
			$.ajax({
				type: 'POST',
				url: '<?= base_url('/Proyectos/VerificarEstadoSoliFin') ?>',
				data:{id:id, fre:fre, contra:contra},
				success:function(data){
					let cont = JSON.parse(data);
					if(cont.tipo != 'error'){
						if(cont.con == 'null' || cont.free == 'true'){
							$('#sec-boton-fin').html(cont.html);
						}else if(cont.con == 'true'){
							$('#sec-boton-fin').html(cont.html);
						}else{
							$('#sec-boton-fin').html(cont.html);
						}
					}
				}, error: function(data){
					console.log('Error en la conexion con el servidor.');
				}
			});
		}
		function declinarSoliFin(id){
			$.ajax({
				type: 'POST',
				url: '<?= base_url('/Proyectos/declinarSoliFin') ?>',
				data:{id:id},
				success: function(data){
					let cont = JSON.parse(data);
					if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, 'advertencia');
					$('#sec-boton-fin').empty();
					return alertify.notify('Se le a notificado al freelance que verifique todas las tareas.', 'correcto');
				}, error: function(data){
					alertify.notify('Intentelo mas tarde, nuestros servidores se encuentran en mantenimiento.', 'advertencia');
				}
			});
		}
		function finProyecto(id){
			$('#'+id).modal('show');
		}
		function finProyectoCalificacion(id){
			alertify.dialog('confirm').set({transition:'zoom'}).show();
			alertify.dialog('confirm').set('labels', {ok:'Aceptar', cancel:'Cancelar'}); 
			alertify.confirm('ATENCIÓN', 'Está apunto de finalizar el proyecto, valíde que todo los puntos esten de acuerdo con el freelancer.', function(){
				$.ajax({
					type: 'POST',
					url: '<?=base_url('/Proyectos/finalizarProyecto')?>',
					data:$('#'+id).serialize(),
					success: function(data){
						let cont = JSON.parse(data);
						if(cont.tipo == 'advertencia'){
							alertify.notify(cont.mensaje, 'advertencia', 10);
						}else{
							alertify.notify(cont.mensaje, 'correcto', 10);
							setTimeout(function(){ location.href = '<?=base_url('/usuarios/perfil/panel')?>' }, 3000);
						}
					}, error: function(data){
						alertify.error('Error, comuníquese con el area de soporte.', 10);
					}
				});
			}, function(){});
		}
	</script>
</main>
<?=$this->endSection()?>