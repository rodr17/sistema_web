<?= $this->extend('front/main') ?>
<?= $this->section('title') ?>
	Contacto
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<?php
	use App\Libraries\NetPay;
	use App\Controllers\Suscripciones;
	$user = model('Autenticacion')->get_id(session('id'));
	$estados = model('ModelEstados')->findAll();
?>
<style>
.block-pricing .most-popular {
	padding: 20px;
}

.block-pricing .box-pricing-item {
	padding: 20px;
}
.mes-tachado {
	text-decoration: line-through; 
	color: #88929b; 
	font-size: 20px; 
	line-height: 20px;
	display: inline-block;
}
.mes-tachado.abajo, .text-month.abajo {
	display: block;
	font-size: 65% !important;
}
.box-pricing-item.most-popular .mes-tachado {
	color: #fff;
}
.accordion-flush .accordion-button::after{
	background-image: none;
}
.accordion-flush .accordion-button:not(.collapsed)::after{
	background-image: none;
}
</style>
<main class="main registro-freelancer">
	<section class="container mt-5">
		<div class="row align-items-start">
			<form class="col-12 col-lg-6 needs-validation" id="form-paso1" enctype="multipart/form-data" novalidate>
				<h1 class="h4 fw-bold">Para que te contacten</h1>
				<label class="text-font2 text-secundario mb-4 fuente-titulos-registro">Mantén actualizados tus datos y permite que puedan contactarte</label>
				<div class="d-flex justify-content-around elformualrio">
					<div class="form-floating mb-3 w-50 px-2 fila-de-datos">
						<input class="form-control text-font" id="callenew" name="calle" required>
						<label class="text-font" for="callenew">Calle y número</label>
					</div>
					<div class="form-floating mb-3 w-50 px-2 fila-de-datos">
						<input class="form-control text-font" id="colonia" name="colonia" required>
						<label class="text-font" for="colonia">Colonia</label>
					</div>
				</div>
				<div class="d-flex justify-content-around elformualrio">
					<div class="form-floating mb-3 w-50 px-2 fila-de-datos">
						<input class="form-control text-font" id="municipio" name="municipio" required>
						<label class="text-font" for="municipio">Municipio</label>
					</div>
					<div class="form-floating mb-3 w-50 px-2 fila-de-datos">
						<select class="form-select text-font" id="estado" name="estado" required aria-label="Floating label select example">
							<?php foreach($estados as $e){ ?>
								<option value="<?=$e['nombre']?>"><?=$e['nombre']?></option>
							<?php } ?>
						</select>
						<label for="floatingSelect">Seleccione un estado</label>
					</div>
				</div>
				<div class="d-flex justify-content-around elformualrio">
					<div class="w-50 px-2 align-self-center fila-de-datos">
						<label>País: <span class="fw-bold">México</span></label>
					</div>
					<div class="form-floating mb-3 w-50 px-2 fila-de-datos">
						<input class="form-control text-font" id="codigo_postal" name="codigo_postal" required>
						<label class="text-font" for="codigo_postal">Código Postal</label>
					</div>
				</div>
				<div class="d-flex justify-content-around elformualrio">
					<div class="form-floating mb-3 w-100 px-2 fila-de-datos">
						<input class="form-control text-font input-telefono" name="telefono" id="telefono" required>
						<label class="text-font" for="telefono">Teléfono</label>
					</div>
					<div class="form-floating mb-3 w-50 px-2 d-none fila-de-datos">
						<input class="form-control text-font" id="rfc" name="rfc">
						<label class="text-font" for="rfc">Rfc</label>
					</div>
				</div>
				<div class="d-none justify-content-around elformualrio">
					<div class="form-floating mb-3 w-100 px-2 fila-de-datos elcurp">
						<input class="form-control text-font" id="curp" name="curp" oninput="validarCurp2(this)" required>
						<div class="invalid-tooltip text-font2 end-0">Requerido</div>
						<label class="text-font" for="curp">Curp</label>
					</div>
				</div>
				<button class="btn btn-naranja mx-2 <?= (($user['plan'] != 'free') && ($user['plan'] != null) && ($user['plan'] != ''))? 'd-none' : '' ?>" id="btn-perfil-paso1">Guardar y continuar</button>
			</form>
			<div class="col-12 col-lg-6 resumen-de-compras">
				<div class="card mb-3" style="border-radius: 30px;">
					<div class="card-body">
					<label>Más información sobre tu cuenta</label>
						<hr>
						<?php if(!empty($user['plan'])){ ?>
							<div id="sec-info-pago-card">
								<?= view_cell('App\Libraries\FuncionesSistema::resumenCompraPlan', ['id' => $user['plan'], 'vista' => 'resumen-compra-registro']) ?>
							</div>
						<?php }else{ ?>
							<div id="seccion_info_cupon">
								<?php if(empty(session('cupon'))) :?>
									<div class="d-flex justify-content-start mb-3">
										<div class="d-inline-flex">
											<input class="border border-secondary text-font" onkeyup="mayus(this)" name="cupon" type="text" placeholder="Ingrese su cupón">
											<a class="btn btn-default mx-1 align-self-center validar-cupon">Validar</a>
										</div>
									</div>
								<?php else :?>
									<?php $cuenta_gratis = model('Suscripciones')->where(['id_usuario' =>  $user['id'], 'tipo_suscripcion' => 'cupon'])->first();
										echo view('backend/viewcells/Informacion_cupon', $cuenta_gratis);?>
								<?php endif ;?>
							</div>
							<hr>
							<h6 class="fw-bold text-start" id="suscripcion_seleccionada_transferencia">
								Tipo de cuenta: <strong>Plan <?= view_cell('App\Libraries\FuncionesSistema::datoPlan', ['planid' => session('plan'), 'dato' => 'name']) ?></strong>
							</h6>
							<label class="text-font d-block text-start" id="costo_seleccionada_transferencia">
								<?= view_cell('App\Libraries\FuncionesSistema::datoPlan', ['planid' => session('plan'), 'dato' => 'monto']) ?>
							</label>
							<label class="text-font2 d-block text-start" id="descripcion_seleccionada_transferencia">
								<?= view_cell('App\Libraries\FuncionesSistema::datoPlan', ['planid' => session('plan'), 'dato' => 'descripcion']) ?>
							</label>
						<?php } ?>
					</div>
				</div>
				<?php if(!empty($user['plan'])){ ?>
					<?php $contratante = model("Autenticacion")->where("id", session("id"))->first();
					$plan = model("ModelPlanes")->where("id_plan_mensual", $contratante["plan"])->orWhere("id_plan_anual", $contratante["plan"])->first() ?>
					<div class="row align-items-center text-center" id="contenido-tarjetas">
						<div class="col-lg-12 my-2" id="columna_tarjeta">
							<label class="card p-2 contenedor_checks cont-mt-pagos" for="metodo_tarjeta">
								<input class="check-otrosPagos" type="radio" name="otros_pagos" hidden="" id="metodo_tarjeta" value="tarjeta">
								<div class="d-flex align-items-center">
									<div class="mr-20">
										<p class="d-flex text-font fw-bold m-0">Tarjeta de crédito / débito</p>
									</div>
									<div class="flex-shrink-0">
										<img src="<?=base_url('/assets/images/iconos/visa.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
										<img src="<?=base_url('/assets/images/iconos/amex.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
										<img src="<?=base_url('/assets/images/iconos/mastercard.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
									</div>
								</div>
							</label>
							<div class="modal fade" id="resumenTarjeta" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" style="max-width: 800px;">
									<div class="modal-content">
										<div class="modal-header justify-content-center border-bottom-0 pb-0">
											<h5 class="modal-title fw-bold" id="titulo">Resumen de compra</h5>
											<a class="btn cerrar-modal px-3 py-2" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
										</div>
										<div class="modal-body">
											<div class="container">
												<form class="row justify-content-start needs-validation binking__form" id="netpay-form" enctype="'multipart/form-data" novalidate>
													<div class="col-12 col-lg-6">
														<div class="row">
															<input type="hidden" name="token_id" id="token_id">
															<input type="hidden" name="deviceFingerPrint" id="deviceFingerPrint">
															<input type="hidden" name="referencia_cargo" id="referencia_cargo">
															<input type="hidden" name="token_transaccion" id="token_transaccion">
															<div class="">
																<p class="fw-bold h6 text-start mt-3" style="display: inline-block; padding-right: 20px;">Tarjeta Débito/Crédito</p>
																<!-- <img class="binking__form-brand-logo"> -->
																<div class="flex-shrink-0" style="display: inline-block; vertical-align: bottom;">
																	<img src="<?=base_url('/assets/images/iconos/visa.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
																	<img src="<?=base_url('/assets/images/iconos/amex.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
																	<img src="<?=base_url('/assets/images/iconos/mastercard.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
																</div>
															</div>
															<div class="row justify-content-around mt-3">
																<div class="col-12">
																	<div class="form-floating mb-3 ">
																		<input class="form-control text-font" name="nombre" id="nombrePay" required>
																		<label class="text-font" for="nombrePay">Nombre Titular</label>
																	</div>
																</div>
																<div class="col-12">
																	<div class="form-floating mb-3 ">
																		<input class="form-control text-font binking__number-field numerico" autocomplete="cc-number" inputmode="numeric" pattern="[0-9 ]*" name="tarjeta" id="tarjetaPay" maxlength="16" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
																		<label class="text-font" for="tarjetaPay">Número Tarjeta</label>
																	</div>
																</div>
																<div class="col-12 ">
																	<dv class="row">
																		<div class="col-12 col-md-4">
																			<div class="form-floating ">
																				<input class="form-control text-font binking__month-field numerico" name="mes" id="mesPay" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
																				<label class="text-font" for="mesPay">Mes</label>
																			</div>
																		</div>
																		<div class="col-12 col-md-4">
																			<div class="form-floating ">
																				<input class="form-control text-font binking__year-field numerico" name="año" id="añoPay" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
																				<label class="text-font" for="añoPay">Año</label>
																			</div>
																		</div>
																		<div class="col-12 col-lg-4">
																			<div class="form-floating ">
																				<input class="form-control text-font binking__code-field numerico" type="password" name="cvv" id="cvvPay" maxlength="4" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
																				<label class="text-font" for="cvvPay">CVV</label>
																				<img src="<?=base_url('/assets/images/cvv.png')?>" style="position: relative; top: -44px; right: -35px;">
																			</div>
																		</div>
																	</dv>
																</div>
															</div>
															<div class="col-12">
																<input class='invisible text-font inputs-form plan_seleccionado' value="<?= $contratante['plan'] ?>" hidden> 
															</div>
														</div>
													</div>
													<div class="col-12 col-lg-6 info-detalles-compra">
														<?= view_cell('App\Libraries\FuncionesSistema::resumenCompraPlanModal', ['id' => $user['plan']]) ?>
													</div>
													<div class="col-12 text-center">
														<button class="btn btn-naranja btn-border mx-2" id="btn-membresia">Continuar registro</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-12 my-2" id="columna_tienda">
							<label class="card p-2 contenedor_checks cont-mt-pagos" for="metodo_tienda">
								<input class="check-otrosPagos" type="radio" name="otros_pagos" hidden="" id="metodo_tienda" value="efectivo">
								<div class="d-flex align-items-center">
									<div class="mr-20">
										<p class="d-flex text-font fw-bold m-0">Efectivo</p>
									</div>
									<div class="flex-shrink-0">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 20 20" style="color: #3483FA;">
											<path fill="currentColor" fill-rule="nonzero" d="M19.002 4.004v11.981h-18V4.004h18zm-1.2 1.2h-15.6v9.581h15.6V5.204zm-7.8 2.354a2.436 2.436 0 110 4.873 2.436 2.436 0 110-4.873zm0 1.2a1.236 1.236 0 10-.001 2.473 1.236 1.236 0 00.001-2.473zm4.806.03a1.206 1.206 0 11-.002 2.41 1.206 1.206 0 01.002-2.409zm-9.614 0a1.206 1.206 0 110 2.41 1.206 1.206 0 010-2.409z"></path>
										</svg>
									</div>
								</div>
							</label>
							<div class="modal fade" id="resumenEfectivo" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered">
									<div class="modal-content">
										<div class="modal-header justify-content-center border-bottom-0 pb-0">
											<h5 class="modal-title fw-bold" id="titulo">Resumen de compra</h5>
											<a class="btn cerrar-modal px-3 py-2" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
										</div>
										<div class="modal-body">
											<div class="container">
												<form class="row justify-content-start needs-validation" id="form-editarPerfil-efectivo" enctype="multipart/form-data" novalidate>
													<div class="col-12">
														<div class="row align-items-center text-center">
															<div class="col-12 info-detalles-compra">
																<!--<h6 class="fw-bold">Suscripcion: <label class="fw-bold"><?= view_cell('App\Libraries\FuncionesSistema::datoPlan', ['planid' => session('plan'), 'dato' => 'name']) ?></label></h6>-->
																<!--<label class="text-font">Costo <span class="fw-bold"><?= view_cell('App\Libraries\FuncionesSistema::datoPlan', ['planid' => session('plan'), 'dato' => 'monto']) ?></span></label>-->
																<!--<label class="text-font2"><?= view_cell('App\Libraries\FuncionesSistema::datoPlan', ['planid' => session('plan'), 'dato' => 'descripcion']) ?></label>-->
																<?= view_cell('App\Libraries\FuncionesSistema::resumenCompraPlanModal', ['id' => $user['plan']]) ?>
															</div>
															<div class="col-12 text-end mt-3">
																<button class="btn btn-naranja" type="submit" form="form-editarPerfil-efectivo" id="btn_confirm_efectivo_cont">Confirmar compra</button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php }elseif(!Suscripciones::esta_suscrito_por_idUsuario(session('id')) && empty((session('plan')))){ ?>
					<div class="mb-3 listado-de-planes listado-paquetes d-md-block">
						<div class="w-100">
							<div class="text-center mt-20">
								<span class="text-lg text-billed">Pago Mensual</span>
									<label class="switch ml-20 mr-20">
										<input type="checkbox" id="cb_billed_type" name="billed_type" onchange="checkBilled()"/>
										<span class="slider round"></span>
									</label>
								<span class="text-lg text-billed">Pago anual</span>
							</div>
						</div>
						<div class="block-pricing carrito-info mt-75 mt-md-50" style="background: #ff510014;">
							<?php $planes = model('ModelPlanes')->where(['cuenta' => 'contratante', 'monto !=' => '0'])->orderBy('id', 'ASC')->find()?>
							<div class="row">
								<?php foreach($planes as $key => $plan){ ?>
									<div class="col-12 col-md-6 wow destacado-comprar animate__animated animate__fadeInUp" data-wow-delay=".1s">
										<div class="box-pricing-item <?= ($plan['name'] == 'Intermedio')? 'most-popular' : ''?>">
											<?php if($plan['name'] == 'Intermedio') :?>
												<div class="text-end mb-10">
													<a class="btn btn-white-sm">Más popular</a>
												</div>
											<?php endif ;?>
											<h4 class="mb-15"><?=$plan['name']?></h4>
											<div class="box-info-price">
												<span class="text-price for-month display-month"><?= ($plan['monto'] == '0')? 'Gratis' : '$'.$plan['monto'] ?></span>
												<span class="text-price for-year">
													<?php if($plan['monto'] != 0) :?>
														<span class="mes-tachado d-block"><?= '$'.$plan['monto']?></span>
													<?php else :?>
														<span>Gratis</span>
													<?php endif ;?>
													<?= ($plan['monto'] != '0')? '$'.$plan['monto_anual'] / 12 : '' ?>
												</span>
												<span class="text-month for-month display-month" style="margin-top: 1px;"><?= ($plan['monto'] == '0') ? 'Vigencia 1 año' : 'Pago mensual'?></span>
												<span class="text-month for-year" style="margin-top: 1px;"><?= ($plan['monto'] != '0') ? 'Precio mensual' : 'Vigencia 1 año'?></span>
											</div>
											<div>
												<p class="text-desc-package mb-3 min-h-desc-paquetes">
													<span class="min-h-desc-paquetes">
														<?=$plan['descripcion']?>
													</span>
													<?php if($plan['monto'] != 0){ ?>
														<br><br class="for-month">
														<span class="text-facturados for-year">$<?=number_format($plan['monto_anual'], 2, '.', ',')?> MXN facturados anualmente</span>
													<?php } else { ?>
														<br><br class="for-month">
														<span class="text-facturados for-year">365 días de vigencia</span>
													<?php } ?>
												</p>
											</div>
											<?php if(session('logeado')) :?>
												<div class="seleccion_planes_registro">
													<a class="btn btn-border for-month display-month" plan="<?=$plan['id_plan_mensual']?>" name="<?=$plan['id_plan_mensual']?>">Seleccionar</a>
													<a class="btn btn-border for-year" plan="<?=$plan['id_plan_anual']?>" name="<?=$plan['id_plan_anual']?>">Seleccionar</a>
													<input class='invisible text-font inputs-form plan_seleccionado' hidden>
												</div>
											<?php else :?>
												<a class="btn btn-border for-month display-month" href="<?=base_url('registrarme/contratante?planid='.$plan['id_plan_mensual'])?>">Seleccionar</a>
												<a class="btn btn-border for-year" href="<?=base_url('registrarme/contratante?planid='.$plan['id_plan_anual'])?>">Seleccionar</a>
											<?php endif ;?>
											<div class="d-block d-lg-none">
												<div class="accordion accordion-flush" id="accordionFlush<?=$key?>">
													<div class="accordion-item border-0">
														<h2 class="accordion-header" id="flush-heading-card-<?=$key?>">
															<button class="accordion-button collapsed contenedor-arrow-v fw-normal" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-card-<?=$key?>" aria-expanded="false" aria-controls="flush-collapse-card-<?=$key?>">
																Ver características <i class="arrow-v down-v"></i>
															</button>
														</h2>
														<div id="flush-collapse-card-<?=$key?>" class="accordion-collapse collapse" aria-labelledby="flush-heading-card-<?=$key?>" data-bs-parent="#accordionFlush<?=$key?>">
															<div class="accordion-body p-2 listado-caracteristicas">
																<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'perfilcontratante']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		Crear perfil de Contratante
																	</div>
																<?php } ?>
																<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'paneladministracion']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		Panel de administración
																	</div>
																<?php } ?>
																<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'valoraciontrabajos']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		Valoracion de trabajos
																	</div>
																<?php } ?>
																<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'historialconversaciones']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		Salas de chat
																	</div>
																<?php } ?>
																<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'contactarFreelancer']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		Contactar Freelancer
																	</div>
																<?php } ?>
																<div class="text-center my-3 border-bottom caract-list">
																	Buscador de freelancer
																</div>
																<?php $solicitudes = intval(view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'enviosolicitudes']))?>
																<?php if($solicitudes > 0){ ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		<?= $solicitudes == 9999 ? 'Envio de solicitudes por proyecto ilimitadas' : $solicitudes.' envio(s) de solicitudes por proyecto' ?>
																	</div>
																<?php } ?>
																<?php $proyectos_activos = intval(view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'proyectosActivos']))?>
																<?php if($proyectos_activos > 0){?>
																	<div class="text-center my-3 border-bottom caract-list">
																		<?= $proyectos_activos == 9999 ? 'Proyectos activos ilimitados' : $proyectos_activos.' proyecto(s) activos'?>
																	</div>
																<?php } ?>
																<?php $historial_trabajos = intval(view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'historialtrabajos'])) ?>
																<?php if($historial_trabajos > 0){ ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		<?= $historial_trabajos == 9999 ? 'Historial de proyectos ilimitado' : 'Historial de '.$historial_trabajos.' proyectos'?>
																	</div>
																<?php } ?>
																<?php $destacados = intval(view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'proyectosDestacados'])); ?>
																<?php if($destacados > 0){ ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		<?= $destacados > 9998 ? 'Proyectos destacados ilimitados' : $destacados.' proyecto(s) destacado(s)'?>
																	</div>
																<?php } ?>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<script>
						$('.seleccion_planes_registro a').click(function(){
							let id = $(this).attr('plan');
							$.ajax({
								type: 'POST',
								url: '<?= base_url('/Autenticacion/cambiarPlanRegistro')?>',
								data:{id:id},
								success: (data) => location.reload(),
								error: (data) =>console.log('error')
							});
						});
					</script>
				<?php } ?>
			</div>
		</div>
	</section>
	<div class="modal fade" id="respuesta_pago" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0">
					<h5 class="modal-title fw-bold" id="titulo">¡Un paso más!</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#respuesta_pago" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row align-items-center text-center">
							<div class="col-12">
								<p class="h6 fw-normal mb-2">Número de referencia: <span class="fw-bold" id="referencia"></span>.</p>
								<label class="text-font mb-2">Te hemos enviado un correo con los pasos a seguir para poder proceder con la suscripción a tu cuenta. Guarda este número de referencia, lo necesitarás para pagar tu plan.</label>
							</div>
							<div class="col-12">
								<a href="<?= base_url('usuarios/perfil/panel')?>" class="btn btn-naranja">Ir a panel</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<script>
	let _this = this;
	const respuesta_3ds = function(_this, referenceId) {
		console.log('referenceId: ' + referenceId);
		charges(referenceId);
	}

	const charges = function(referenceId) {
		console.log('charges: ' + referenceId);
		$('#referencia_cargo').val(referenceId);
	}
	
	const confirm = function(processorTransactionId) {
		console.log('confirm: ' + processorTransactionId);
		$.ajax({
			url: '<?= base_url('Pagos/confirmar_pago') ?>',
			type: 'POST',
			data: {transaccion: $('#token_transaccion').val(), procesador: processorTransactionId, usuario: <?=session('id')?>},
			success: function(data) {
				let respuesta = JSON.parse(data);
				$('#btn-membresia').removeAttr('disabled').children('span').remove();

				if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta);

				$('#resumenTarjeta').modal('hide');
				alertify.notify(respuesta.mensaje, respuesta.alerta);
				return setTimeout((e) => location.href = "/compra_realizada?token="+respuesta.datos.transactionTokenId, 2000);
			},
			error: function(data) {
				let respuesta = JSON.parse(data.responseText);
				$('#btn-membresia').removeAttr('disabled').children('span').remove();
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
			}
		});
	}

	const callbackProceed = function(_this, processorTransactionId) {
		alertify.notify('processorTransactionId: ' + processorTransactionId, 'custom');
		confirm(processorTransactionId);
	}

	//La generación del device fingerprint debe de estar al momento en que se visualiza el formulario de pago
	function generateDevice(callback) {
		deviceFingerPrint = NetPay.form.generateDeviceFingerPrint();
		callback();
	}
	
	$(document).ready(function() {
		let deviceFingerPrint;
		<?php $netpay = new NetPay()?>
		<?= $netpay->getModo() ? 'NetPay.setSandboxMode(true); netpay3ds.setSandboxMode(true);' : 'NetPay.setSandboxMode(false); netpay3ds.setSandboxMode(false);' ?> // Esta línea se debe de llamar antes de la generación del device fingerprint

		// SE OBTIENE REFERENCEID POR 3DS
		netpay3ds.init(() => netpay3ds.config(_this, 100, respuesta_3ds));
	});
	
	$('input[name="otros_pagos"]').change(function(){
		$('#btn-perfil-paso1').click();
		let id = $(this).attr('id');
		$('label[for="'+id+'"]').addClass('active');
		if($(this).val() == 'efectivo'){
			$('#resumenEfectivo').modal('show');
		}else if($(this).val() == 'tarjeta'){
			$('#resumenTarjeta').modal('show');
		}
	})
	
	$('.cerrar-modal').click(function(){
		$('.cont-mt-pagos').removeClass('active');
		$('.modal').modal('hide');
		$('input[name="otros_pagos"]').prop('checked', false);
	});
	
	$('#btn-membresia').click(function(evento) {
		$this = $(this);
		if (evento.currentTarget.form.nombrePay.value.length == 0 || evento.currentTarget.form.tarjetaPay.value.length == 0 ||
			evento.currentTarget.form.mesPay.value.length == 0 || evento.currentTarget.form.cvvPay.value.length == 0) return true;

		evento.preventDefault();
		$($this).attr('disabled', true);
		$($this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo($this);

		generateDevice(function() { 
			let tarjeta = {
				cardNumber: evento.currentTarget.form.tarjetaPay.value,
				expMonth: evento.currentTarget.form.mesPay.value,
				expYear: evento.currentTarget.form.añoPay.value,
				cvv2: evento.currentTarget.form.cvvPay.value,
				vault: true,
				simpleUse:false,
				deviceFingerPrint : deviceFingerPrint
			};
			NetPay.setApiKey('<?= $netpay->getLlave() ?>');
			var validateNumberLength = NetPay.card.validateNumberLength(tarjeta.cardNumber);
			var validateNumber = NetPay.card.validateNumber(tarjeta.cardNumber);
			var validateExpiry = NetPay.card.validateExpiry(tarjeta.expMonth, tarjeta.expYear);
			var validateCVV = NetPay.card.validateCVV(tarjeta.cvv2, tarjeta.cardNumber);

			if (!validateNumberLength || !validateNumber){
				$('#btn-membresia').removeAttr('disabled').children('span').remove();
				return alertify.notify('Número de tarjeta incorrecto', 'advertencia', 10);
			} 
			if (!validateExpiry){
				$('#btn-membresia').removeAttr('disabled').children('span').remove();
				return alertify.notify('Expiración de tarjeta incorrecto', 'advertencia', 10);
			}
			if (!validateCVV){
				$('#btn-membresia').removeAttr('disabled').children('span').remove();
				return alertify.notify('Código de seguridad incorrecto', 'advertencia', 10);
			}

			NetPay.token.create(tarjeta, correcto, falla);
			function correcto(respuesta) {
				console.log("Token creado correctamente");
				let datos_token = JSON.parse(respuesta.message.data);
				console.log(datos_token);
				$('#token_id').val(datos_token.token);
				$('#deviceFingerPrint').val(tarjeta.deviceFingerPrint);

				$.ajax({
					url: '<?= base_url('Pagos/crear') ?>',
					type: 'POST',
					data: $('#netpay-form').serialize(),
					success: function(data) {
						let respuesta = JSON.parse(data);
						$('#btn-membresia').removeAttr('disabled').children('span').remove();

						if (respuesta.alerta == 'custom'){
							$('#token_transaccion').val(respuesta.datos.transactionTokenId);
							let canProceed = netpay3ds.canProceed(respuesta.datos.status, respuesta.datos.threeDSecureResponse.responseCode, respuesta.datos.threeDSecureResponse.acsUrl);
							console.log("canProceed es: " + canProceed);
							
							if (!canProceed) return alertify.notify('Transacción rechadaza', 'falla');
							
							let proceed = netpay3ds.proceed(_this, respuesta.datos.threeDSecureResponse.acsUrl, respuesta.datos.threeDSecureResponse.paReq, respuesta.datos.threeDSecureResponse.authenticationTransactionID, callbackProceed);

						}
						$('#resumenTarjeta').modal('hide');
						alertify.notify(respuesta.mensaje, respuesta.alerta);
						return setTimeout((e) => location.href = "/compra_realizada?token="+respuesta.datos.transactionTokenId, 2000);
					},
					error: function(data) {
						let respuesta = JSON.parse(data.responseText);
						$('#btn-membresia').removeAttr('disabled').children('span').remove();
						alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
					}
				});
			}

			function falla(error) {
				console.trace(error);
				let mensaje = error.message == 'Empty or invalid card number' ? 'Número de tarjeta inválido' : error.message;
				alertify.notify("ERROR [" + mensaje + "] inténtalo nuevamente recargando la página", 'falla', 10);
				$('#btn-membresia').removeAttr('disabled').children('span').remove();
			}
		});
	});
	$('#btn-perfil-paso1').click(function(evento) {
		if (evento.target.form.callenew.value.length == 0 || evento.target.form.colonia.value.length == 0 ||
			evento.target.form.municipio.value.length == 0 || evento.target.form.estado.value.length == 0 ||
			evento.target.form.codigo_postal.value.length == 0 || evento.target.form.telefono.value.length == 0) return setTimeout(function(){ $('.cerrar-modal').click(); alertify.notify('Por favor complete los campos de registro.', 'advertencia'); }, 600);

		evento.preventDefault();
		$(this).addClass('disabled');
		$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);

		let form = new FormData(document.getElementById("form-paso1"));
		$.ajax({
			url: 'Autenticacion/guardar_Perfil_1_contratista',
			contentType: false,
			processData: false,
			type: 'POST',
			data: form,
			success: function(data) {
				<?php if(empty($user['plan'])){ ?>
					location.reload();
				<?php } ?>
			},
			error: function(data) {
				alertify.notify(data.responseText, 'falla', 10);
			}
		});
	});
	$('#btn_confirm_efectivo_cont').click(function(evento) {
		$this = $(this);
		$('#btn-perfil-paso1').click();
		evento.preventDefault();
		$(this).addClass('disabled');
		$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);
		if (alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro de realizar tu compra?', function() {
			$.ajax({
				url: '/Suscripcion_efectivo',
				type: 'POST',
				data: {
					id_plan: $('.plan_seleccionado').val()
				},
				success: function(data) {
					let respuesta = JSON.parse(data);
					$('#efectivo_modal').modal('hide');
					$($this).removeClass('disabled').children('span').remove();
					$('input[name="otros_pagos"]').prop('checked', false).parent().removeClass('active');
					$('#obtener_membresia .cerrar-modal').click();
					$('.modal').modal('hide');
					if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					$('#referencia').text(respuesta.referencia);
					$('#respuesta_pago').modal('show');

					alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				},
				error: function(data) {
					let respuesta = JSON.parse(data.responseText);
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
				}
			});
		}, function() {
			$($this).removeClass('disabled').children('span').remove()
		}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
	});
</script>
<?= $this->endSection() ?>