<?= $this->extend('front/main') ?>
<?= $this->section('title') ?>
Beneficios
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<main class="main">
    <section class="container">
        <div class="row align-items-start">
            <div class="col-12 col-md-8">
                <h1 class="h4 fw-bold">Obtén tu membresía</h1>
                <label class="text-font2 text-secundario mb-4">Para poder tener grandes beneficios y mejores proyectos</label>
                <?php $contratista = model("Autenticacion")->where("id", session("id"))->first();
                $plan = model("ModelPlanes")->where("id_paypal", $contratista["plan"])->first() ?>
                <div class="row mb-5">
                    <?php $planes = model('ModelPlanes')->where('cuenta', 'contratista')->orderBy('id', 'ASC')->find() ?>
                    <div class="col-12 col-lg-6 wow animate__animated animate__fadeInUp mb-3">
                        <div class="border-1 shadow p-3 table-responsive">
                            <table class="table table-borderless">
                                <thead class="border-bottom">
                                    <tr>
                                        <th scope="col"></th>
                                        <?php foreach ($planes as $plan) { ?>
                                            <th scope="col" class="text-center"><?= $plan['name'] ?></th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">Crear perfil de Contratante</th>
                                        <?php foreach ($planes as $plan) { ?>
                                            <td class="text-center"><?= view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'perfilcontratante']) ?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <th scope="row">Panel de administración</th>
                                        <?php foreach ($planes as $plan) { ?>
                                            <td class="text-center"><?= view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'paneladministracion']) ?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <th scope="row">Valoracion de trabajos</th>
                                        <?php foreach ($planes as $plan) { ?>
                                            <td class="text-center"><?= view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'valoraciontrabajos']) ?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <th scope="row">Acceso a freelancer</th>
                                        <?php foreach ($planes as $plan) { ?>
                                            <td class="text-center"><?= view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'accesofreelancer']) ?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <th scope="row">Compartir documentos</th>
                                        <?php foreach ($planes as $plan) { ?>
                                            <td class="text-center"><?= view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'compartirdocumentos']) ?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <th scope="row">Buscador de freelancer</th>
                                        <?php foreach ($planes as $plan) { ?>
                                            <td class="text-center"><?= view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'buscadorfreelancer']) ?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <th scope="row">Acceso a Freelancer por nivel</th>
                                        <td class="text-center">
                                            Basicos
                                        </td>
                                        <td class="text-center">
                                            Intermedios
                                        </td>
                                        <td class="text-center">
                                            Avanzados
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Envio de solicitudes por proyecto</th>
                                        <?php foreach ($planes as $plan) { ?>
                                            <td class="text-center"><?= view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'enviosolicitudes']) ?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <th scope="row">Proyectos activos</th>
                                        <?php foreach ($planes as $plan) { ?>
                                            <td class="text-center"><?= view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'proyectosActivos']) ?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <th scope="row">Historial de trabajos</th>
                                        <?php foreach ($planes as $plan) { ?>
                                            <td class="text-center"><?= view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'historialtrabajos']) ?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <th scope="row">Historial de conversaciones</th>
                                        <?php foreach ($planes as $plan) { ?>
                                            <td class="text-center"><?= view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'historialconversaciones']) ?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <th scope="row">Proyectos Destacados</th>
                                        <?php foreach ($planes as $plan) { ?>
                                            <td class="text-center"><?= view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'proyectosDestacados']) ?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <th scope="row"></th>
                                        <?php foreach ($planes as $plan) { ?>
                                            <td class="text-center"><?= $plan['monto'] != 0 ? '$ ' . $plan['monto'] : 'Gratis' ?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr class="botones">
                                        <th scope="row"></th>
                                        <?php foreach ($planes as $plan) { ?>
                                            <td class="text-center">
                                                <a class='btn btn-naranja' plan="<?= $plan['id_plan_mensual'] ?>" name="<?= $plan['id_plan_mensual'] ?>">Obtener</a>
                                                <input class='invisible text-font inputs-form plan_seleccionado' hidden>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 wow animate__animated animate__fadeInUp">
                        <?php foreach ($planes as $plan) { ?>
                            <div class="card border-1 p-4 mb-3 shadow">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <span class="fws-bold">Plan <?= $plan['name'] ?></span>
                                        <span class="fws-bold">12 meses</span>
                                        <label>
                                            <span class="fws-bold fs-4 text-success">
                                                <?= $plan['monto_anual'] == 0 ? 'Gratuito' : '$ ' . ($plan['monto_anual'] / 12) . '.00 MX*' ?>
                                            </span>
                                            <?php if ($plan['monto_anual'] != 0) : ?>
                                                <span class="fs-6">/mes</span>
                                            <?php endif; ?>
                                        </label>
                                        <br><span class="fs-6"><?= $plan['monto_anual'] != 0 ? 'Monto anual <b>$ ' . $plan['monto_anual'] . '.00</b>' : '' ?></span>
                                    </div>
                                    <div class="col-12 col-md-6 text-end align-self-center botones">
                                        <a class='btn btn-primary anual' plan="<?= $plan['id_plan_anual'] ?>" name="<?= $plan['id_plan_anual'] ?>">Obtener</a>
                                        <input class='invisible text-font inputs-form plan_seleccionado' hidden>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-4 d-none d-md-block vh-100">
                <ul class="lista">
                    <li class="circulo text-font active">Contacto</li>
                    <li class="circulo text-font">Ultimó paso</li>
                </ul>
            </div>
        </div>
        <div class="modal fade" id="respuesta_pago" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header justify-content-center border-bottom-0">
                        <h5 class="modal-title fw-bold" id="titulo">¡Un paso más!</h5>
                        <a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#respuesta_pago" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row align-items-center text-center">
                                <div class="col-12">
                                    <label class="text-font mb-2">Te hemos enviado un correo con los pasos a seguir para poder proceder con la suscripción a tu cuenta. Ó</label>
                                    <label class="text-font">descarga <a class="text-decoration-none text-naranja" target="_blank" id="referencia">aquí</a> tu comprobante de pago.</label>
                                </div>
                                <div class="col-12">
                                    <a href="<?= base_url('perfil') ?>" class="btn btn-naranja">Ir a perfil</a>
                                </div>
                                <div class="mextemps-hr position-relative my-3">
                                    <img class="logo-hr" src="<?= base_url('assets/images/logos/Logo-hr.webp') ?>" alt="Logo MEXTEMPS escala grises">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<script>
    $('.botonesPerfil').click('a', function(e) {
        e.preventDefault();
        if (e.target.name != undefined) {
            $.ajax({
                url: 'Autenticacion/guardarPlanPasos',
                type: 'POST',
                data: {
                    id_plan: e.target.name
                },
                success: function(data) {
                    let respuesta = JSON.parse(data);
                    if (respuesta.alerta == 'falla') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);

                    location.reload();
                },
                error: function(data) {
                    alertify.notify('Network', 'falla', 10);
                }
            });
        }
    });
    $('.botonesAnual_Perfil').click('a', function(e) {
        e.preventDefault();
        if (e.target.name != undefined) {
            $.ajax({
                url: 'Autenticacion/guardarPlanPasos',
                type: 'POST',
                data: {
                    id_plan: e.target.name
                },
                success: function(data) {
                    let respuesta = JSON.parse(data);
                    if (respuesta.alerta == 'falla') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);

                    location.reload();
                },
                error: function(data) {
                    alertify.notify('Network', 'falla', 10);
                }
            });
        }
    });
</script>
<?= $this->endSection() ?>