<?= $this->extend('front/main') ?>
<?= $this->section('title') ?>
Beneficios
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="container mt-5">
    <div class="row align-items-start">
        <div class="col-12 col-md-8">
            <h1 class="h4 fw-bold">Obtén tu membresía</h1>
            <label class="text-font2 text-secundario mb-4">Para poder tener grandes beneficios y mejores proyectos</label>
            <?php $contratista = model("Autenticacion")->where("id", session("id"))->first();
            $plan = model("ModelPlanes")->where("id_paypal", $contratista["plan"])->orWhere("id_paypal_anual", $contratista["plan"])->first() ?>
            <input class='invisible text-font inputs-form plan_seleccionado' value="<?= $contratista['plan'] ?>" hidden>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <button class="nav-link text-font active" id="nav-formulario" data-bs-toggle="tab" data-bs-target="#tab-formulario" type="button" role="tab" aria-controls="tab-formulario" aria-selected="true">Tarjeta</button>
                    <button class="nav-link text-font" id="nav-efectivo" data-bs-toggle="tab" data-bs-target="#tab-efectivo" type="button" role="tab" aria-controls="tab-efectivo" aria-selected="false">Efectivo</button>
                    <button class="nav-link text-font" id="nav-transferencia" data-bs-toggle="tab" data-bs-target="#tab-transferencia" type="button" role="tab" aria-controls="tab-transferencia" aria-selected="false">Transferencia electrónica</button>
                    <button class="nav-link text-font" id="nav-paypal" data-bs-toggle="tab" data-bs-target="#tab-paypal" type="button" role="tab" aria-controls="tab-paypal" aria-selected="false">Paypal</button>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade active show" id="tab-formulario" role="tabpanel" aria-labelledby="nav-formulario">
                    <form class="row justify-content-start needs-validation" id="form-editarPerfil" enctype="'multipart/form-data" novalidate>
                        <input type="hidden" name="token_id" id="token_id">
                        <p class="fw-bold h6 text-start mt-3">Tarjeta Débito/Crédito</p>
                        <label class="text-font text-secundario mb-4"><em class="fst-italic">Al momento de guardarse la tarjeta se validará haciendo un autorización por <b>$10.00</b> los cuales son devueltos en el momento.</em></label>
                        <div class="d-flex justify-content-around">
                            <div class="form-floating mb-3 w-50 px-2">
                                <input class="form-control text-font" name="nombre" id="nombrePay" data-openpay-card="holder_name" required>
                                <label class="text-font" for="nombrePay">Nombre Titular</label>
                            </div>
                            <div class="form-floating mb-3 w-25 px-2">
                                <input class="form-control text-font" name="tarjeta" id="tarjetaPay" maxlength="16" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" data-openpay-card="card_number" required>
                                <label class="text-font" for="tarjetaPay">Número Tarjeta</label>
                            </div>
                            <div class="form-floating mb-3 w-25 px-2">
                                <input class="form-control text-font" name="mes" id="mesPay" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" data-openpay-card="expiration_month" required>
                                <label class="text-font" for="mesPay">Mes</label>
                            </div>
                            <div class="form-floating mb-3 w-25 px-2">
                                <input class="form-control text-font" name="año" id="añoPay" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" data-openpay-card="expiration_year" required>
                                <label class="text-font" for="añoPay">Año</label>
                            </div>
                        </div>
                        <div class="d-flex justify-content-start">
                            <div class="form-floating mb-3 w-25 px-2">
                                <input class="form-control text-font" type="password" name="cvv" id="cvvPay" maxlength="4" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" data-openpay-card="cvv2" required>
                                <label class="text-font" for="cvvPay">CVV</label>
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-naranja btn-border mx-2" id="btn-membresia">Completar registro</button>
                            <!-- <input class='invisible text-font inputs-form plan_seleccionado' value="<?= $contratista['plan'] ?>" hidden> -->
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="tab-efectivo" role="tabpanel" aria-labelledby="nav-efectivo">
                    <div class="container">
                        <form class="row justify-content-start needs-validation" id="form-editarPerfil-efectivo" enctype="multipart/form-data" novalidate>
                            <div class="col-12">
                                <div class="row align-items-center text-center mt-4">
                                    <h6 class="text-start">Resumen de compra</h6>
                                    <div class="col-12">
                                        <h6 class="fw-bold">Suscripcion: <label class="fw-bold"><?= $plan['name'] ?></label></h6>
                                        <label class="text-font">Costo <span class="fw-bold">$<?= $plan['monto'] ?></span></label>
                                        <label class="text-font2">Lorem ipsum dolor, sit amet consectetur adipisicing elit. At amet, a voluptates ex est sint sunt consequuntur eos voluptatum natus</label>
                                    </div>
                                    <div class="col-12 text-end mt-3">
                                        <button class="btn btn-naranja" type="submit" form="form-editarPerfil-efectivo" id="btn_confirm_efectivo">Confirmar compra</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-transferencia" role="tabpanel" aria-labelledby="nav-transferencia">
                    <div class="container">
                        <form class="row justify-content-start needs-validation" id="form-editarPerfil-transferencia" enctype="multipart/form-data" novalidate>
                            <div class="col-12">
                                <div class="row align-items-center text-center mt-4">
                                    <h6 class="text-start">Resumen de compra</h6>
                                    <div class="col-12">
                                        <h6 class="fw-bold">Suscripcion: <label class="fw-bold"><?= $plan['name'] ?></label></h6>
                                        <label class="text-font">Costo <span class="fw-bold">$<?= $plan['monto'] ?></span></label>
                                        <label class="text-font2">Lorem ipsum dolor, sit amet consectetur adipisicing elit. At amet, a voluptates ex est sint sunt consequuntur eos voluptatum natus</label>
                                    </div>
                                    <div class="col-12 text-end mt-3">
                                        <button class="btn btn-naranja" id="btn_confirm_transferencia">Confirmar compra</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-paypal" role="tabpanel" aria-labelledby="nav-paypal">
                    <div class="container">
                        <div class="row justify-content-center align-items-center text-center mt-4">
                            <div class="col-6">
                                <div id="paypal-button-container"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4 d-none d-md-block vh-100">
            <ul class="lista">
                <li class="circulo text-font active">Contacto</li>
                <li class="circulo text-font">Ultimó paso</li>
            </ul>
        </div>
    </div>
    <div class="modal fade" id="respuesta_pago" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header justify-content-center border-bottom-0">
                    <h5 class="modal-title fw-bold" id="titulo">¡Un paso más!</h5>
                    <a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#respuesta_pago" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row align-items-center text-center">
                            <div class="col-12">
                                <label class="text-font mb-2">Te hemos enviado un correo con los pasos a seguir para poder proceder con la suscripcion a tu cuenta. Ó</label>
                                <label class="text-font">descarga <a class="text-decoration-none text-naranja" target="_blank" id="referencia">aquí</a> tu comprobante de pago.</label>
                            </div>
                            <div class="col-12">
                                <a href="<?= base_url('usuarios/perfil/panel')?>" class="btn btn-naranja">Ir a panel</a>
                            </div>
                            <div class="mextemps-hr position-relative my-3">
                                <img class="logo-hr" src="<?= base_url('assets/images/logos/Logo-hr.webp') ?>" alt="Logo MEXTEMPS escala grises">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        let deviceSessionId = OpenPay.deviceData.setup("form-editarPerfil", "deviceIdHiddenFieldName");
    });
    $('#btn-membresia').click(function(evento) {
        $this = $(this);
        if (evento.currentTarget.form.nombrePay.value.length == 0 || evento.currentTarget.form.tarjetaPay.value.length == 0 ||
            evento.currentTarget.form.mesPay.value.length == 0 || evento.currentTarget.form.cvvPay.value.length == 0) return true;

        evento.preventDefault();
        $($this).attr('disabled', true);
        $($this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo($this);

        if (!OpenPay.card.validateCardNumber($('#tarjetaPay').val())) {
            $($this).removeAttr('disabled').children('span').remove();
            return alertify.notify('Verifique el número de tarjeta o inténtelo con una diferente', 'advertencia', 10);
        }
        if (!OpenPay.card.validateExpiry($('#mesPay').val(), $('#añoPay').val())) {
            $($this).removeAttr('disabled').children('span').remove();
            return alertify.notify('Expiración de tarjeta incorrecto', 'advertencia', 10);
        }
        if (!OpenPay.card.validateCVC($('#cvvPay').val(), $('#tarjetaPay').val())) {
            $($this).removeAttr('disabled').children('span').remove();
            return alertify.notify('Código de seguridad incorrecto', 'advertencia', 10);
        }
        OpenPay.token.extractFormAndCreate('form-editarPerfil', success_callbak, error_callbak);
    });
    let success_callbak = function(response) {
        $('#token_id').val(response.data.id);
        $.ajax({
            url: '<?= base_url('Autenticacion/perfilCompletado') ?>',
            type: 'POST',
            data: $('#form-editarPerfil').serialize(),
            success: function(data) {
                $('#btn-membresia').removeAttr('disabled').children('span').remove();
                location.href ='usuarios/perfil/panel';
            },
            error: function(data) {
                let respuesta = JSON.parse(data.responseText);
                $('#btn-membresia').removeAttr('disabled').children('span').remove();
                alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
            }
        });
    };
    let error_callbak = function(response) {
        let desc = response.data.description != undefined ? response.data.description : response.message;
        alertify.notify("ERROR [" + response.status + "] " + desc, 'falla', 10);
        $('#btn-membresia').removeAttr('disabled').children('span').remove();
    };
</script>
<?= $this->endSection() ?>