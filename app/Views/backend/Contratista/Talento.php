<?=$this->extend('front/main')?>
<?= $this->section('librerias')?>
<style>
	.form-group input:focus { background: #ffffff !important; }
	.pagination .page-link, .page-item.disabled .page-link {
		background: #FE5000;
		border-radius: 50px !important;
		width: 40px;
		height: 40px;
		line-height: 20px !important;
		padding: 0 14px;
		font-size: 24px !important;
		border: 0px;
		color: #fff !important;
		cursor: pointer;
	}
</style>
<?= $this->endSection()?>
<?=$this->section('title')?>
	Talento
<?=$this->endSection()?>

<?=$this->section('content')?>
<script src="<?=base_url('assets/themes/js/noUISlider.js')?>"></script>
<main class="main listado-talentos">
	<section class="section-box-2">
		<div class="box-head-single none-bg">
			<div class="container">
				<h4>Contamos con una base de datos, <br>con los mejores talentos de México para tu proyecto</h4>
				<div class="row mt-15 mb-40">
					<div class="col-lg-12 col-md-12">
						<span class="text-mutted">Encuentra a un profesional.</span>
					</div>
				</div>
				<div class="box-shadow-bdrd-15 box-filters">
					<form id="filtro-freelance-sup">
						<div class="row">
							<div class="col-lg-5">
								<?php if(strval($nivel) == 'avanzado'){ ?>
									<div class="box-search-job" style="width: 100%;">
										<input type="text" name="clave" id="clave-busqueda" class="input-search-job" placeholder="Buscar..." style="padding: 12px 10px 10px 45px; background: url(<?=base_url('assets/themes/imgs/theme/icons/icon-search.svg')?>) no-repeat 10px center;height: 50px;border: 1px solid #efefef;"/>
									</div>
								<?php }else{ ?>
									<div class="tooltips w-100">
										<div class="box-search-job" style="width: 100%;">
											<input type="text" class="input-search-job" placeholder="Buscar..." disabled style="padding: 12px 10px 10px 45px; background: url(<?=base_url('assets/themes/imgs/theme/icons/icon-search.svg')?>) no-repeat 10px center;height: 50px;border: 1px solid #efefef;"/>
										</div>
										<span class="tooltiptext" style="top: 0;left: 100%;">Hazte PRÉMIUM para poder utilizar el filtro de busqueda. Adquirir <a href="<?=base_url('tarifas-contratista')?>" target="_blank">aquí</a></span>
									</div>
								<?php } ?>
							</div>
							<div class="col-lg-7">
								<div class="d-flex job-fillter flex-column flex-lg-row justify-content-center">
									<div class="flex-column flex-md-row justify-content-lg-center align-items-lg-center d-none d-lg-flex">
										<div class="dropdown">
											<div class="form-group select-style select-style-icon mb-0">
												<select class="form-control form-icons select-active select-hidden-accessible" id="habilidadsup" name="habilidades" data-select-id="1" tabindex="-1" aria-hidden="true">
													<option value="" selected>Categoría</option>
													<?php foreach($subareas as $s){ ?>
														<option value="<?=$s['area']?>"><?=$s['area']?></option>
													<?php } ?>
												</select>
												<i class="fi-rr-shield-check"></i>
											</div>
										</div>
										<div class="dropdown">
											<?php if(strval($nivel) == 'restringido' || strval($nivel) == '0'){ ?>
												<div class="tooltips w-100">
													<div class="form-group select-style select-style-icon mb-0">
														<select class="form-control form-icons select-active select3-hidden-accessible">
															<option value="default" hidden selected>Idiomas</option>
														</select>
														<i class="fi-rr-globe"></i>
													</div>
													<span class="tooltiptext" style="top: 0;left: 100%;">Hazte PRÉMIUM para poder utilizar el filtro de idiomas. Adquirir <a href="<?=base_url('tarifas-contratista')?>" target="_blank">aquí</a></span>
												</div>
											<?php }else{ ?>
												<?=view_cell('App\Libraries\FuncionesSistema::idiomasActivos', ['atributo' => 'idioma'])?>
											<?php } ?>
										</div>
									</div>
									<div class="d-flex align-items-center justify-content-around justify-content-lg-end mt-2 mt-lg-0">
										<button class="btn btn-default float-right" id="filtrar-freelance-sup" type="submit">Buscar</button>
										<button class="btn btn-default d-lg-none" type="button" data-bs-toggle="offcanvas" data-bs-target="#contenido_canvas" aria-controls="contenido_canvas">Filtro avanzado</button>
										<div class="offcanvas offcanvas-bottom d-lg-none p-2" tabindex="-1" id="contenido_canvas" aria-labelledby="titulo_canvas" style="height: 75vh;">
											<div class="offcanvas-header">
												<h5 class="offcanvas-title" id="titulo_canvas">Filtrar Freelancers</h5>
												<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
											</div>
											<div class="offcanvas-body" id="contenido_filtro_canvas">
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<div class="container py-4">
		<section class="row flex-row-reverse">
			<div class="col-lg-9 col-md-12 col-sm-12 col-12 float-right">
				<div class="row pb-3">
					<div class="col-12" id="contenido_resultados_filtro">
						<span class="text-small">Mostrando <strong id="total-strong"><?= $totalfree ?></strong> freelancers.</span>
					</div>
					<div class="col-lg-5 text-lg-end mt-sm-15">
						<div class="d-none" style="justify-content: space-between;">
							<span class="text-sortby" for="filtro-trabajos">Ordenar por:</span>
							<div class="dropdown dropdown-sort">
								<select class="btn dropdown-toggle" type="button" name="filtro-trabajos" id="filtro-cantidad">
									<option value="3">3 en 3</option>
									<option selected value="12">12 en 12</option>
									<option value="24">24 en 24</option>
									<option value="48">48 en 48</option>
								</select>
							</div>
						</div>
						<div class="display-flex2 d-none">
							<span class="text-sortby">Ordenar por:</span>
							<div class="dropdown dropdown-sort">
								<button class="btn dropdown-toggle" type="button" id="dropdownSort" data-bs-toggle="dropdown" aria-expanded="false" data-bs-display="static"><span>Más nuevos</span> <i class="fi-rr-angle-small-down"></i></button>
								<ul class="dropdown-menu dropdown-menu-light" aria-labelledby="dropdownSort">
									<li><a class="dropdown-item order-cards active" data-ord="news">12 en 12</a></li>
									<li><a class="dropdown-item order-cards" data-ord="old">24 en 24</a></li>
									<li><a class="dropdown-item order-cards" data-ord="max">48 en 48</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="row" id="area-freelancers">
					<?php foreach($freelances as $user){ ?>
						<div class="col-12 col-md-6 col-lg-4 mb-1">
							<?= view_cell('App\Libraries\FuncionesSistema::cardPostulantes', ['id' => $user['id'], 'postu' => 0, 'tipo' => 'talento', 'modal' => 0, 'es_premium' => $user['es_premium']]) ?>
						</div>
					<?php } ?>
				</div>
				<div class="row">
					<div class="listado-blog">
						<div class="pagination paginations text-center wow animate__animated animate__fadeIn">
							<ul class="pager">
								<li class="page-item">
									<a class="page-link" data-page="0" aria-label="Previous">
										<span aria-hidden="true">&laquo;</span>
									</a>
								</li>
								<li class="sec-pages" data-value="1" data-max="<?= ceil($totalfree / 12) ?>" data-min="1">
									1 de <?=ceil($totalfree / 12)?>
								</li>
								<li class="page-item">
									<a class="page-link" data-page="2" paginacion="0" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="text-center" id="icono-loading">
					<script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
					<lottie-player src="<?=base_url('assets/animaciones/loading.json')?>"  background="transparent"  speed="1"  style="width: 250px; height: 250px; margin: auto;"  loop autoplay></lottie-player>
				</div>
			</div>
			<div class="col-lg-3 col-md-12 col-sm-12 col-12 d-none d-lg-block">
				<div class="sidebar-shadow none-shadow mb-30">
					<div class="sidebar-filters filtro_abajo">
						<form id="filtro-freelance">
							<div class="filter-block mb-30">
    							<h5 class="medium-heading mb-15">Categoría</h5>
								<div class="form-group select-style select-style-icon">
									<select class="form-control form-icons select-active select-hidden-accessible" id="habilidad" name="habilidades" data-select-id="1" tabindex="-1" aria-hidden="true">
										<option value="" selected>Seleccione una opción</option>
										<?php foreach($subareas as $s){ ?>
											<option value="<?=$s['area']?>"><?=$s['area']?></option>
										<?php } ?>
									</select>
									<i class="fi-rr-shield-check"></i>
								</div>
							</div>
							<?php $estados = model('ModelEstados')->findAll(); ?>
							<div class="filter-block mb-30">
								<h5 class="medium-heading mb-15">Estado</h5>
								<?php if(strval($nivel) == 'restringido' || strval($nivel) == '0'){ ?>
									<div class="tooltips w-100">
										<div class="form-group select-style select-style-icon">
											<select class="form-control form-icons select-active select-hidden-accessible" data-select-id="1" tabindex="-1" aria-hidden="true">
												<option value="" selected>Seleccione una opción</option>
											</select>
											<i class="fi-rr-marker"></i>
										</div>
										<span class="tooltiptext" style="top: 0;left: 100%;">Hazte PRÉMIUM para poder utilizar el filtro de ubicación. Adquirir <a href="<?=base_url('tarifas-contratista')?>" target="_blank">aquí</a></span>
									</div>
								<?php }else{ ?>
									<div class="form-group select-style select-style-icon">
										<select class="form-control form-icons select-active select-hidden-accessible" id="ubicacion" name="ubicacion" data-select-id="1" tabindex="-1" aria-hidden="true">
											<option value="" selected>Seleccione una opción</option>
											<?php foreach($estados as $e){ ?>
												<option value="<?=$e['nombre']?>"><?=$e['nombre']?></option>
											<?php } ?>
										</select>
										<i class="fi-rr-marker"></i>
									</div>
								<?php } ?>
							</div>
							<div class="filter-block mb-30">
								<h5 class="medium-heading mb-15">Idioma</h5>
								<?php if(strval($nivel) == 'restringido' || strval($nivel) == '0'){ ?>
									<div class="tooltips w-100">
										<div class="form-group select-style select-style-icon mb-0">
											<select class="form-control form-icons select-active select3-hidden-accessible">
												<option value="default" hidden selected>Idiomas</option>
											</select>
											<i class="fi-rr-globe"></i>
										</div>
										<span class="tooltiptext" style="top: 0;left: 100%;">Hazte PRÉMIUM para poder utilizar el filtro de idiomas. Adquirir <a href="<?=base_url('tarifas-contratista')?>" target="_blank">aquí</a></span>
									</div>
								<?php }else{ ?>
									<?=view_cell('App\Libraries\FuncionesSistema::idiomasActivos', ['atributo' => 'idioma'])?>
								<?php } ?>
							</div>
							<div class="filter-block mb-30">
								<h5 class="medium-heading mb-10">Proyectos concluidos</h5>
								<div class="form-group">
									<?php if(strval($nivel) == 'avanzado'){ ?>
										<ul class="list-checkbox">
											<li>
												<label class="cb-container">
													<input type="checkbox" name="concluidos[]" value="0"> <span class="text-small">1 - 15</span>
													<span class="checkmark"></span>
												</label>
												<span class="number-item">
													<?= view_cell('App\Libraries\FuncionesSistema::cantRangoProyectosConcluidos', ['rango' => 0]) ?>
												</span>
											</li>
											<li>
												<label class="cb-container">
													<input type="checkbox" name="concluidos[]" value="1"> <span class="text-small">16 - 30</span>
													<span class="checkmark"></span>
												</label>
												<span class="number-item">
													<?= view_cell('App\Libraries\FuncionesSistema::cantRangoProyectosConcluidos', ['rango' => 1]) ?>
												</span>
											</li>
											<li>
												<label class="cb-container">
													<input type="checkbox" name="concluidos[]" value="2"> <span class="text-small">31 - 45</span>
													<span class="checkmark"></span>
												</label>
												<span class="number-item">
													<?= view_cell('App\Libraries\FuncionesSistema::cantRangoProyectosConcluidos', ['rango' => 2]) ?>
												</span>
											</li>
											<li>
												<label class="cb-container">
													<input type="checkbox" name="concluidos[]" value="3"> <span class="text-small">46 - 60</span>
													<span class="checkmark"></span>
												</label>
												<span class="number-item">
													<?= view_cell('App\Libraries\FuncionesSistema::cantRangoProyectosConcluidos', ['rango' => 3]) ?>
												</span>
											</li>
											<li>
												<label class="cb-container">
													<input type="checkbox" name="concluidos[]" value="4"> <span class="text-small">61 - 99</span>
													<span class="checkmark"></span>
												</label>
												<span class="number-item">
													<?= view_cell('App\Libraries\FuncionesSistema::cantRangoProyectosConcluidos', ['rango' => 4]) ?>
												</span>
											</li>
											<li>
												<label class="cb-container">
													<input type="checkbox" name="concluidos[]" value="5"> <span class="text-small">+ 100</span>
													<span class="checkmark"></span>
												</label>
												<span class="number-item">
													<?= view_cell('App\Libraries\FuncionesSistema::cantRangoProyectosConcluidos', ['rango' => 5]) ?>
												</span>
											</li>
										</ul>
									<?php }else{ ?>
										<ul class="list-checkbox">
											<li class="p-0">
												<div class="tooltips w-100">
													<label class="cb-container">
														<input type="checkbox" disabled> <span class="text-small">1 - 15</span>
														<span class="checkmark"></span>
													</label>
													<span class="number-item">
														<?= view_cell('App\Libraries\FuncionesSistema::cantRangoProyectosConcluidos', ['rango' => 0]) ?>
													</span>
													<span class="tooltiptext" style="top: 0;left: 100%;">Hazte PRÉMIUM para poder utilizar el filtro de proyectos concluidos. Adquirir <a href="<?=base_url('tarifas-contratista')?>" target="_blank">aquí</a></span>
												</div>
											</li>
											<li class="p-0">
												<div class="tooltips w-100">
													<label class="cb-container">
														<input type="checkbox" disabled> <span class="text-small">16 - 30</span>
														<span class="checkmark"></span>
													</label>
													<span class="number-item">
														<?= view_cell('App\Libraries\FuncionesSistema::cantRangoProyectosConcluidos', ['rango' => 1]) ?>
													</span>
													<span class="tooltiptext" style="top: 0;left: 100%;">Hazte PRÉMIUM para poder utilizar el filtro de proyectos concluidos. Adquirir <a href="<?=base_url('tarifas-contratista')?>" target="_blank">aquí</a></span>
												</div>
											</li>
											<li class="p-0">
												<div class="tooltips w-100">
													<label class="cb-container">
														<input type="checkbox" disabled> <span class="text-small">31 - 45</span>
														<span class="checkmark"></span>
													</label>
													<span class="number-item">
														<?= view_cell('App\Libraries\FuncionesSistema::cantRangoProyectosConcluidos', ['rango' => 2]) ?>
													</span>
													<span class="tooltiptext" style="top: 0;left: 100%;">Hazte PRÉMIUM para poder utilizar el filtro de proyectos concluidos. Adquirir <a href="<?=base_url('tarifas-contratista')?>" target="_blank">aquí</a></span>
												</div>
											</li>
											<li class="p-0">
												<div class="tooltips w-100">
													<label class="cb-container">
														<input type="checkbox" disabled> <span class="text-small">46 - 60</span>
														<span class="checkmark"></span>
													</label>
													<span class="number-item">
														<?= view_cell('App\Libraries\FuncionesSistema::cantRangoProyectosConcluidos', ['rango' => 3]) ?>
													</span>
													<span class="tooltiptext" style="top: 0;left: 100%;">Hazte PRÉMIUM para poder utilizar el filtro de proyectos concluidos. Adquirir <a href="<?=base_url('tarifas-contratista')?>" target="_blank">aquí</a></span>
												</div>
											</li>
											<li class="p-0">
												<div class="tooltips w-100">
													<label class="cb-container">
														<input type="checkbox" disabled> <span class="text-small">61 - 99</span>
														<span class="checkmark"></span>
													</label>
													<span class="number-item">
														<?= view_cell('App\Libraries\FuncionesSistema::cantRangoProyectosConcluidos', ['rango' => 4]) ?>
													</span>
													<span class="tooltiptext" style="top: 0;left: 100%;">Hazte PRÉMIUM para poder utilizar el filtro de proyectos concluidos. Adquirir <a href="<?=base_url('tarifas-contratista')?>" target="_blank">aquí</a></span>
												</div>
											</li>
											<li class="p-0">
												<div class="tooltips w-100">
													<label class="cb-container">
														<input type="checkbox" disabled> <span class="text-small">+ 100</span>
														<span class="checkmark"></span>
													</label>
													<span class="number-item">
														<?= view_cell('App\Libraries\FuncionesSistema::cantRangoProyectosConcluidos', ['rango' => 5]) ?>
													</span>
													<span class="tooltiptext" style="top: 0;left: 100%;">Hazte PRÉMIUM para poder utilizar el filtro de proyectos concluidos. Adquirir <a href="<?=base_url('tarifas-contratista')?>" target="_blank">aquí</a></span>
												</div>
											</li>
										</ul>
									<?php } ?>
								</div>
							</div>
							<div class="filter-block mb-40">
								<h5 class="medium-heading mb-25">Calificación</h5>
								<?php if(strval($nivel) == 'avanzado'){ ?>
									<div class="text-center" id="calificar">
										<p class="clasificacion">
											<input id="radio1" type="radio" name="estrellas" value="5">
											<label for="radio1">
												<i class="fas fa-star"></i>
											</label>
											<input id="radio2" type="radio" name="estrellas" value="4">
											<label for="radio2">
												<i class="fas fa-star"></i>
											</label>
											<input id="radio3" type="radio" name="estrellas" value="3">
											<label for="radio3">
												<i class="fas fa-star"></i>
											</label>
											<input id="radio4" type="radio" name="estrellas" value="2">
											<label for="radio4">
												<i class="fas fa-star"></i>
											</label>
											<input id="radio5" type="radio" name="estrellas" value="1">
											<label for="radio5">
												<i class="fas fa-star"></i>
											</label>
										</p>
									</div>
								<?php }else{ ?>
									<div class="tooltips w-100">
										<div class="text-center" id="calificar">
											<p class="clasificacion">
												<input id="radio1" type="radio" disabled>
												<label for="radio1">
													<i class="fas fa-star"></i>
												</label>
												<input id="radio2" type="radio" disabled>
												<label for="radio2">
													<i class="fas fa-star"></i>
												</label>
												<input id="radio3" type="radio" disabled>
												<label for="radio3">
													<i class="fas fa-star"></i>
												</label>
												<input id="radio4" type="radio" disabled>
												<label for="radio4">
													<i class="fas fa-star"></i>
												</label>
												<input id="radio5" type="radio" disabled>
												<label for="radio5">
													<i class="fas fa-star"></i>
												</label>
											</p>
										</div>
										<span class="tooltiptext" style="top: 0;left: 100%;">Hazte PRÉMIUM para poder utilizar el filtro de calificación. Adquirir <a href="<?=base_url('tarifas-contratista')?>" target="_blank">aquí</a></span>
									</div>
								<?php } ?>
							</div>
							<div class="filter-block mb-40">
								<h5 class="medium-heading mb-25">Sueldo por hora</h5>
								<div class="">
									<div class="row mb-20 <?= (strval($nivel) == 'avanzado')? '' : 'd-none' ?>">
										<div class="col-sm-12">
											<div id="slider-range"></div>
										</div>
									</div>
									<?php if(strval($nivel) == 'avanzado'){ ?>
										<div class="row">
											<div class="col-lg-6">
												<label class="lb-slider">Desde</label>
												<div class="form-group minus-input">
													<input type="text" name="min-value-money" class="input-disabled form-control min-value-money" disabled="disabled" value="" />
													<input type="hidden" name="sueldomin" class="form-control min-value" value="" />
												</div>
											</div>
											<div class="col-lg-6">
												<label class="lb-slider">Hasta</label>
												<div class="form-group">
													<input type="text" name="max-value-money" class="input-disabled form-control max-value-money" disabled="disabled" value="" />
													<input type="hidden" name="sueldomax" class="form-control max-value" value="" />
												</div>
											</div>
										</div>
									<?php }else{ ?>
										<div class="tooltips w-100">
											<div class="row">
												<div class="col-lg-6">
													<label class="lb-slider">Desde</label>
													<div class="form-group minus-input">
														<input type="text" class="input-disabled form-control min-value-money" disabled="disabled" value="" />
														<input type="hidden"  class="form-control min-value" value="" />
													</div>
												</div>
												<div class="col-lg-6">
													<label class="lb-slider">Hasta</label>
													<div class="form-group">
														<input type="text"  class="input-disabled form-control max-value-money" disabled="disabled" value="" />
														<input type="hidden"  class="form-control max-value" value="" />
													</div>
												</div>
											</div>
											<span class="tooltiptext" style="top: 0;left: 100%;">Hazte PRÉMIUM para poder utilizar el filtro de sueldo por hora. Adquirir <a href="<?=base_url('tarifas-contratista')?>" target="_blank">aquí</a></span>
										</div>
									<?php } ?>
								</div>
							</div>
							<div class="buttons-filter d-flex justify-content-center mx-auto gap-2 botones_filtro">
								<a class="btn btn-default" id="filtrar-freelance">Aplicar filtro</a>
								<a class="btn btn-default" id="limpiar-filtro">Reiniciar filtro</a>
							</div>
						</form>
					</div>
				</div>
				<div class="sidebar-with-bg background-naranja bg-sidebar pb-80">
					<h5 class="medium-heading text-white mb-20 mt-20">¿Reclutamiento?</h5>
					<p class="text-body-999 text-white mb-30">Anuncie sus trabajos a miles de usuarios mensuales y busque 14,000 CV en nuestra base de datos.</p>
					<a href="<?= base_url('proyecto')?>" class="text-decoration-none btn-border icon-chevron-right btn-white-sm">Publicar proyecto</a>
				</div>
				<div class="sidebar-azulmt-bg">
					<h5 class="font-semibold mb-10">Suscríbete ahora</h5>
					<p class="text-body-999">Recibe aviso de los últimos proyectos publicados.</p>
					<div class="box-email-reminder">
						<form class="needs-validation form-newsletter" id="boletines_suscripcion" novalidation encytpe="multipart/form-data">
							<div class="form-group mt-15">
								<input type="email" class="form-control input-bg-white form-icons" name="correo" id="correo" placeholder="usuario@gmail.com" required>
								<i class="fi-rr-envelope"></i>
							</div>
							<div class="form-group mt-25 mb-5">
								<button class="btn btn-default btn-md" type="submit">Suscribirme</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>
</main>
<script defer>
	// Inicia paginacion
	$('.page-link').click(function(){
		$this = $(this);
		let p = $this.attr('data-page');
		let tipo = $this.attr('aria-label');
		let cantidad_paginacion;
		if(tipo == 'Previous'){
			$('.page-link').removeClass('disabled');
			if(p == '0'){
				$this.attr('paginacion', '0').addClass('disabled');
				$('.sec-pages').html('1 de '+$('.sec-pages').attr('data-max'));
			}else{
				$('.sec-pages').html((parseInt($('.sec-pages').val()) - 1)+' de '+$('.sec-pages').attr('data-max'));
			}
			$this.attr('paginacion') > 0 ? cantidad_paginacion = parseInt($this.attr('paginacion')) - 12 : cantidad_paginacion = 0;
		}else{
			$('.page-link').removeClass('disabled');
			if(p == $('.sec-pages').attr('data-max')){
				$this.attr('paginacion', p * 12).addClass('disabled');
				$('.sec-pages').html($('.sec-pages').attr('data-max')+' de '+$('.sec-pages').attr('data-max'));
			}else{
				$('.sec-pages').html((parseInt($('.sec-pages').val()) + 1)+' de '+$('.sec-pages').attr('data-max'));
			}
			p != $('.sec-pages').attr('data-max') ? cantidad_paginacion = parseInt($this.attr('paginacion')) + 12 : (p == 2 ? cantidad_paginacion =12 : cantidad_paginacion = p*12);
		}
		$('#area-freelancers').fadeOut();
		$('#icono-loading').fadeIn();
		$.ajax({
			type: "GET",
			url: "<?=base_url('Usuarios/paginacionTalentos')?>",
			data: {cantidad : cantidad_paginacion},
			success: function(data) {
				$('#icono-loading').fadeOut();
				$('#area-freelancers').fadeIn().html(data);
				$this.attr('paginacion', cantidad_paginacion);
			}
		});
	});
	// Termina paginacion
	// Requires jQuery
	// Initialize slider:
	$(document).ready(function () {
		$(".noUi-handle").on("click", function () {
			$(this).width(50);
		});
		var moneyFormat = wNumb({
			decimals: 0,
			thousand: ",",
			prefix: "$"
		});
		var rangeSlider = $('.filtro_abajo #slider-range')[0];
		var rangeSlider_canvas = $('#contenido_filtro_canvas #slider-range')[0];
		noUiSlider.create(rangeSlider, {
			start: [0, <?= $sueldo_max?>],
			step: 1,
			range: {
				min: [0],
				max: [<?= $sueldo_max?>]
			},
			format: moneyFormat,
			connect: true
		});
		noUiSlider.create(rangeSlider_canvas, {
			start: [0, <?= $sueldo_max?>],
			step: 1,
			range: {
				min: [0],
				max: [<?= $sueldo_max?>]
			},
			format: moneyFormat,
			connect: true
		});

		// Set visual min and max values and also update value hidden form inputs
		rangeSlider.noUiSlider.on("update", function (values, handle) {
			$(".min-value-money").val(values[0]);
			$(".max-value-money").val(values[1]);
			$(".min-value").val(moneyFormat.from(values[0]));
			$(".max-value").val(moneyFormat.from(values[1]));
		});
		rangeSlider_canvas.noUiSlider.on("update", function (values, handle) {
			$(".min-value-money").val(values[0]);
			$(".max-value-money").val(values[1]);
			$(".min-value").val(moneyFormat.from(values[0]));
			$(".max-value").val(moneyFormat.from(values[1]));
		});
	});

	$('#smin').on('change keyup paste click', function(){
		let val = parseFloat($(this).val());
		let max = parseFloat($('#smax').val().trim());
		if(max != ''){
			if(val >= max){
				if($("#filtrar-freelance").hasClass("disabled") == false){
					$('#filtrar-freelance').addClass('disabled');
				}else{
					$('#filtrar-freelance').removeClass('disabled');
				}
				alertify.notify('Ingrese un valor menor al sueldo maximo', 'advertencia');
			}
			if($("#filtrar-freelance").hasClass("disabled") == false){
				$('#filtrar-freelance').removeClass('disabled');
			}
		}
		if($("#filtrar-freelance").hasClass("disabled") == false){
			$('#filtrar-freelance').removeClass('disabled');
		}
	});
	$('#filtro-freelance-sup').submit(function(e){
		e.preventDefault();
		$('#area-freelancers').fadeOut();
		$('#icono-loading').fadeIn();
		$.ajax({
			type: 'POST',
			url: '<?=base_url('usuarios/resultFiltroFreelance')?>',
			data:$('#filtro-freelance-sup').serialize(),
			success: function(data){
				let respuesta = JSON.parse(data);
				$('#area-freelancers').html(respuesta.html);
				$('#icono-loading').fadeOut();
				$('#area-freelancers').fadeIn();
			}, error: function(data){
				alertify.error('Ha surgido un error, comuníquese con el área de soporte.', 10);
			}
		});
	});
	$(document).ready(function () {
		$('#icono-loading').hide();
	});
	function obtenerSubareas(area, selectOuput){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Habilidades/subareas')?>',
			data:{area:area},
			success: function(data){
				selectOuput.html(data);
			}, error: function(data){
				console.log('ERROR');
			}
		});
	}
	function limpiar_filtro(){
		$this = $('#limpiar-filtro');
		$this.addClass('disabled');
		$this.add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo($this);
		$("#area").val('').trigger('change');
		$("#areasup").val('').trigger('change');
		$('#palabra-clave').val('');
		$('select[name="habilidades"]').val('').trigger('change');
		$('select[name="idioma"]').val('default').trigger('change');
		$('select[name="ubicacion"]').val('').trigger('change');
		$('input[name="estrellas"]').prop('checked', false);
		$('input[name="concluidos"]').val(0);
		$('#smin').val('<?=view_cell('App\Libraries\FuncionesSistema::sueldoMinimo')?>');
		$('#smax').val('<?=view_cell('App\Libraries\FuncionesSistema::sueldoMaximo')?>');
		$this.removeClass('disabled').children('span').remove();
		$.ajax({
			type: 'POST',
			dataType : 'json',
			url: '<?= base_url('Usuarios/get_FreelancersReseteoFiltro')?>/'+$('#filtro-cantidad').val(),
			success: function(respuesta){
				$('#total-strong').text(respuesta.cantidad_freelancers);
				$('#area-freelancers').html(respuesta.html);
			}, error: function(){
				alertify.notify('Error al reiniciar filtro');
			}
		});
	}
	$('#area').on('change', function(){
		if($(this).val().length == 0) return false;
		obtenerSubareas($(this).val(), $('#habilidad'));
	});
	$('#areasup').on('change', function(){
		if($(this).val().length == 0) return false;
		obtenerSubareas($(this).val(), $('#habilidadsup'));
	});
	$('#filtro-freelance').clone().prependTo('#contenido_filtro_canvas');
	
	$('.botones_filtro, #contenido_filtro_canvas .botones_filtro').delegate('#filtrar-freelance, #limpiar-filtro','click', function(e){
		// Se limpia filtro
		if(e.target.id == 'limpiar-filtro') return limpiar_filtro();
		
		// Se ejecuta filtrado de freelancers
		$('#area-freelancers').fadeOut();
		$('#icono-loading').fadeIn();
		let formulario;
		if(e.delegateTarget.parentElement.parentElement.attributes.id == undefined) formulario = '.filtro_abajo #filtro-freelance';
		else formulario = '#contenido_filtro_canvas #filtro-freelance';
		
		console.log($(formulario).serialize(), formulario);
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/usuarios/resultFiltroFreelance')?>',
			data: $(formulario).serialize(),
			success: function(data){
				let respuesta = JSON.parse(data);
				$('#contenido_canvas').offcanvas('hide');
				$('html').scrollTop(0);
				$('#area-freelancers').html(respuesta.html);
				$('#icono-loading').fadeOut();
				$('#area-freelancers').fadeIn();
				$('#total-strong').html(respuesta.cantidad_resultados)
			}, error: function(data){
				console.log(data.responteText);
				$('#icono-loading').fadeOut();
				$('#area-freelancers').fadeIn();
			}
		});
	});
</script>
<?=$this->endSection()?>