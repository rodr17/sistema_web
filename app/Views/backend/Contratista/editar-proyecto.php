<?=$this->extend('front/main')?>
<?=$this->section('title')?>
	Editar proyecto: <?=$proyecto['titulo']?>
<?=$this->endSection()?>
 
<?=$this->section('content')?>
<style>
	.thumb-image {
		float: left;
		width: 100px;
		position: relative;
		padding: 5px;
	}
</style> 
<main class="main editar-proyectos">
	<form id="form-edit-proy" enctype="'multipart/form-data" novalidate>
		<section class="section-box">
			<div class="box-head-single">
				<div class="container">
					<div class="input-group-lg mb-3">
						<label class="label">Titulo del proyecto</label>
						<input type="text" name="titulo" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" value="<?=$proyecto['titulo']?>">
					</div>
					<input name="id" type="hidden" value="<?=$proyecto['id']?>">
					<!-- <h3><span data-bs-toggle="tooltip" data-bs-placement="top" title="Titulo del proyecto"><?=$proyecto['titulo']?></span></h3> -->
				</div>
			</div>
		</section>
		<section class="section-box mt-50">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-12 col-sm-12 col-12"> 
						<div class="single-image-feature" id="galeria-edit">
							<?=view_cell('App\Libraries\FuncionesSistema::carruselProyectEdit', ['id' => $proyecto['id']])?>
						</div>
						<div class="content-single">
							<h5>Descripción</h5>
							<div class="form-floating mb-3">
								<textarea class="form-control" name="corta" placeholder="Leave a comment here" id="desc-corta" style="height: 100px"><?=$proyecto['desc_corta']?></textarea>
								<span class="d-block text-end" id="contador">0/200</span>
								<label for="desc-corta">Descripción corta</label>
							</div>
							<div class="form-floating">
								<textarea class="form-control" name="descripcion" placeholder="Leave a comment here" id="desc" style="height: 200px"><?=$proyecto['descripcion']?></textarea>
								<label for="desc">Descripción</label>
							</div>
							<h5>Galería de fotos</h5>
							<div id="wrapper" style="margin-top: 20px;">
								<input style="height: 40px;" id="fileUpload" name="galeria[]" multiple="multiple" accept=".jpg, .jpeg, .png, .webp, .bmp" type="file"/> 
								<div class="d-flex flex-wrap" id="image-holder"></div>
							</div>
							<label class="fst-italic text-start d-block small mb-0">* Extensiones de archivo permitidas: jpg, jpeg, png, webp, bmp.</label>
							<label class="fst-italic text-start d-block small mb-0">* Tamaño aximo de archivos: 4MB.</label>
							<label class="fst-italic text-start d-block small mb-0">* Hasta 5 archivos máximo.</label>
						</div>
						<div class="author-single w-100">
							<!-- <span>AliThemes</span> -->
							<?php if($proyecto['estatus'] == 'desarrollo'){ ?>
								<?=view_cell('App\Libraries\FuncionesSistema::barraProgreso', ['id' => $proyecto['id']])?>
								<br><br>
							<?php } ?>
						</div>
					</div>
					<div class="col-lg-4 col-md-12 col-sm-12 col-12 pl-40 pl-lg-15 mt-lg-30">
						<div class="sidebar-shadow">
							<div class="sidebar-heading">
								<div class="avatar-sidebar">
									<figure><img alt="jobhub" src="<?=imagenPerfil($contratista)?>" /></figure>
									<div class="sidebar-info">
										<span class="sidebar-company"><?= $contratista['nombre'].' '.$contratista['apellidos']?></span>
									</div>
								</div>
							</div>
							<div class="text-center mt-20" id="sec-chat">
								<a class="btn btn-naranja btn-border" data-bs-toggle="modal" data-bs-target="#modalpublicar">Guardar cambios</a>
								<a href="/usuarios/perfil" class="btn btn-naranja btn-border">Mi perfil</a>
							</div>
							<div class="sidebar-list-job">
								<ul>
									<li>
										<div class="form-group select-style select-style-icon">
											<div class="form-group select-style select-style-icon position-relative">
												<select class="form-control form-icons select-active select-hidden-accessible" id="datalist_area" name="area" tabindex="-1" aria-hidden="true" required>
													<option selected>Seleccione un área</option>
													<?php foreach ($habilidades as $key => $a) : ?>
														<option value="<?= $a['area'].' - '.$a['superior'] ?>" <?=$proyecto['subcategoria'].' - '.$proyecto['categoria'] == $a['area'].' - '.$a['superior'] ? 'selected' : ''?> ><?= $a['area'].' - '.$a['superior'] ?></option>
													<?php endforeach; ?>
												</select>
												<i class="fi fi-rr-list"></i>
												<div class="invalid-tooltip text-font2" id="inv-area">Requerido</div>
											</div>
										</div>
									</li>
									<li>
										<div class="sidebar-icon-item"><i class="fi-rr-marker"></i></div>
										<div class="sidebar-text-info">
											<span class="text-description">Locación</span>
											<strong class="small-heading"><?=view_cell('App\Libraries\FuncionesSistema::direccionContratista', ['id' => $proyecto['id_contratista']])?></strong>
										</div>
									</li>
									<li>
										<div class="sidebar-icon-item"><i class="fi-rr-dollar"></i></div>
										<div class="sidebar-text-info">
											<span class="text-description">Presupuesto</span>
											<div class="mb-3">
												<input type="text" class="form-control numerico" id="presupuesto" name="presupuesto" value="<?=number_format($proyecto['presupuesto'], '2', '.', ',')?>" placeholder="name@example.com">
											</div>
										</div>
									</li>
									<li>
										<div class="sidebar-icon-item"><i class="fi-rr-clock"></i></div>
										<div class="sidebar-text-info">
											<span class="text-description">Fecha estimada de inicio</span>
											<div class="position-relative mb-3">
												<label for="arranque" class="form-label">Fecha estimada de inicio del proyecto</label>
												<input type="date" id="startDate" name="arranque" value="<?=$proyecto['fecha_arranque_esperado']?>" min="<?=date('Y-m-d')?>">
											</div>
										</div>
									</li>
									<li>
										<div class="sidebar-icon-item"><i class="fi-rr-time-fast"></i></div>
										<div class="sidebar-text-info">
											<span class="text-description">Fecha de entrega esperado</span>
											<div class="position-relative mb-3">
												<label for="fecha" class="form-label">Fecha de entrega estimada</label>
												<input type="date" id="endDate" name="fecha" value="<?=$proyecto['fecha_entrega']?>" min="<?=date('Y-m-d')?>">
												<div class="invalid-tooltip text-font">Requerido</div>
											</div>
										</div>
									</li>
								</ul>
							</div>
							<!---->
							<?php if($proyecto['estatus'] == 'desarrollo' || $proyecto['estatus'] == 'finalizado'){ ?>
								<div class="sidebar-team-member pt-40">
									<h6 class="small-heading">Tareas</h6>
									<div class="w-100 border" style="min-height: 20px;">
										<div class="row p-2" id="sec-tareas">
											<?=view_cell('App\Libraries\FuncionesSistema::tareasProyectoContra', ['id' => $proyecto['id']])?>
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	</form>
</main>
<!-- **************************************************** COMO PUBLICAR PROYECTO ****************************************** -->
<div class="modal fade" id="modalpublicar" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="como-publicar" aria-modal="true" role="dialog">
	<div class="modal-dialog modal-dialog-centered" style="max-width: 650px; min-height: min-content;">
		<div class="modal-content" style="background-color: #00000000; border: 0;">
			<div class="modal-header justify-content-center border-bottom-0 pt-4">
				<h5 class="modal-title fw-bold h4 text-white" id="como-publicar">¿Cómo desea publicar su proyecto?</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body burbuja">
				<div class="row" style="min-height: 200px;">
					<div class="col-12 col-sm-12 col-md-6 mt-15">
						<div class="text-center donde-imagen" style="border: 1px solid #dddddd;border-radius: 30px;padding: 20px;min-height: 450px;background: #fff;">
							<span class="medida-imagenes" style="border-radius: 100px;width: 200px;height: 200px;display: flex;overflow: hidden;margin: 0 auto;"><img style="object-fit: cover;" src="https://img.freepik.com/foto-gratis/atractiva-joven-trabajadora-independiente-enfocada-que-navega-sitios-web-contratacion-trabajo-publica-su-curriculum-busca-nuevos-proyectos-clientes_343059-426.jpg?w=1380&amp;t=st=1663708301~exp=1663708901~hmac=53e8dc66000ec9848a840684c8dd873d19164510aeef23fc20acc61ca457cf8c" width="300"></span>
							<p style="padding-top: 20px;line-height: 20px;min-height: 140px;">Publica rápidamente tu proyecto para que cualquier freelancer pueda postularse a el, después de publicar te llevaremos a tu panel de usuario.</p>
							<div class="tooltips" style="margin: 20px 0 0 0;">
								<a id="actua-proyecto" class="btn btn-naranja btn-border">Solo Publicar</a>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-6 mt-15">
						<div class="text-center donde-imagen" style="border-radius: 30px;border: 1px solid #dddddd;padding: 20px;min-height: 450px;background: #fff;">
							<span class="medida-imagenes" style="border-radius: 100px;width: 200px;height: 200px;display: flex;overflow: hidden;margin: 0 auto;"><img style="object-fit: cover;" src="https://img.freepik.com/foto-gratis/vista-superior-equipo-companeros-trabajo-trabajando-oficina_329181-12055.jpg?w=1380&amp;t=st=1663708228~exp=1663708828~hmac=d5c9a721a76485a967197575efa99db716f03474414a92a7a8d8e6a10f44794f" width="300"></span>
							<p style="line-height: 20px;padding-top: 20px;min-height: 140px;">Enviar solicitudes a freelancees para aceptar desarrollar su proyecto, de igual manera se publicará para que otros freelancers puedan postularse al proyecto.</p>
							<div class="tooltips" style="margin: 20px 0 0 0;">
								<button class="btn btn-naranja btn-border" data-bs-toggle="modal" data-bs-target="#solicitudmodal">Solicitar y publicar</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- **************************************************** SOLICITUD Y PUBLICAR ****************************************** -->
<div class="modal fade" id="solicitudmodal" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="mandar-solicitud" aria-modal="true" role="dialog">
	<div class="modal-dialog modal-dialog-centered modal-lg" style="min-height: min-content;">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0 pt-4">
				<h5 class="modal-title fw-bold h4" id="mandar-solicitud">Enviar solicitud a Freelancers</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#modalpublicar"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body">
				<div class="p-3">
					<div class="row justify-content-center">
						<div class="col-12 col-sm-12 col-lg-10">
							<label for="search-freelance">Solicitudes disponibles: <b class="fw-bold" id="cont_solicitudes_diponibles"><?=($solicitudes_restantes == 9999)? 'Ilimitadas' : $solicitudes_restantes ?></b></label>
							<input class="form-control" id="search-freelance" placeholder="Buscar por: Nombre o correo">
						</div>
						<div class="col-12 col-sm-12 col-lg-8">
							<div class="dropdown w-100">
								<ul class="dropdown-menu p-0 w-100" id="lista-freelance" >
								</ul>
							</div>
						</div>
						<div class="p-3"></div>
						<div class="col-12 col-sm-12 col-lg-10">
							<div id="free-seleccionados" class="row">
							</div>
							<input type="hidden" id="free-select">
							<input type="hidden" id="free-preseleccionados" value="<?= $solicitudes ?>">
						</div>
						<div class="col-12 p-3 text-center">
							<a class="btn btn-naranja" id="solicitudes-publicar">Publicar y enviar solicitudes</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	function validacion_campos(){
		if($('#datalist_area').val() == 'Seleccione un área'){
			$('#inv-area').show()
			return false;
		} 
		else {
			$('#inv-area').hide();
			return true;
		}
	}

	$(document).ready(function(){
		var longitudMax = 200;
		$('#contador').html(`${$('#desc-corta').val().length}/${longitudMax}`);
		
		$('#desc-corta').on('input', function(e) {
			const longitudAct = $(this).val().length;
			
			$(this).attr('maxlength', longitudMax);
			$('#contador').html(`${longitudAct}/${longitudMax}`);
		});
		
		$('#startDate').change(function(e){
			if($('#endDate').val() < $(this).val()) $('#endDate').val($(this).val());
			$('#endDate').attr('min', $(this).val());
		});
		
		$('#presupuesto').mask('#,##0.00', {reverse: true});
	});
	
	
	$('#actua-proyecto').click(function(){
		if(!validacion_campos()){
			$('#modalpublicar').modal('hide');
			return alertify.notify('Llena los campos requeridos', 'advertencia');
		}
		
		$(this).attr('disabled', true);
		$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);
		var form = $('#form-edit-proy')[0];
		var formData = new FormData(form);
		$.ajax({
			type: "POST",
			url: "<?=base_url('/Proyectos/saveEditProject')?>",
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function(data) {
				let respuesta = JSON.parse(data);
				$('#actua-proyecto').removeAttr('disabled').children('span').remove();
				$('#modalpublicar').modal('hide');
				if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.tipo);
				
				alertify.notify(respuesta.mensaje, respuesta.alerta);
				setTimeout(() => { location.href = `/Proyectos/viewProyecto/${respuesta.id}` }, 1500);
			}, error: function(data) {
				alertify.notify('Error: '+data.responseText, 'falla');
			}
		});
	});
	
	$('#search-freelance').keyup(function(){
		let search = $(this).val();
		let frees = $('#free-select').val();
		let freelancers_preseleccionados = $('#free-preseleccionados').val();
		$('#lista-freelance').addClass('d-block');
		if(search.length == 0) return $('#lista-freelance').removeClass('d-block');
		
		$.ajax({
			type: 'POST',
			url: '<?= base_url('/Proyectos/buscadorFreelance') ?>',
			data:{search:search, frees:frees, freelancers_presolicitados: freelancers_preseleccionados},
			success: function(data){
				$('#lista-freelance').html(data);
			}, error: function(data){
				alertify.notify('Contáctese con el área de soporte técnico.', 'falla');
			}
		});
	});
	
	$('#solicitudes-publicar').click(function(){
		if(!validacion_campos()){
			$('#solicitudes-publicar').modal('hide');
			return alertify.notify('Llena los campos requeridos', 'advertencia');
		}

		$(this).addClass('disabled');
		$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);
		var form = $('#form-edit-proy')[0];
		var formData = new FormData(form);
		formData.append('frees', $('#free-select').val());

		$.ajax({
			type: "POST",
			url: "<?=base_url('/Proyectos/saveEditProject')?>",
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function(data) {
				let respuesta = JSON.parse(data);
				$('#solicitudes-publicar').removeClass('disabled').children('span').remove();
				$('#solicitudmodal').modal('hide');
				if(respuesta.tipo != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta);
				
				$('#free-seleccionados').html('');
				alertify.notify(respuesta.mensaje, respuesta.alerta);
				setTimeout(() => { location.href = `/Proyectos/viewProyecto/${respuesta.id}` }, 1500);
			}, error: function(data) {
				alertify.notify('Error: '+data.responseJSON.message, 'falla');
			}
		});
	});
	
	$("#fileUpload").on('change', function() {
		if($(this)[0].files.length > 5){
			alertify.notify('La cantidad maxima de imagenes a cargar es de 5.', 'advertencia');
			$("#fileUpload").val(null);
			$('#image-holder').html('');
		}else{
			//Get count of selected files
			var countFiles = $(this)[0].files.length;
			var imgPath = $(this)[0].value;
			var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
			var image_holder = $("#image-holder");
			image_holder.empty();
			if (extn == "png" || extn == "jpg" || extn == "jpeg" || extn == "webp") {
				if (typeof(FileReader) != "undefined") {
					//loop for each file selected for uploaded.
					for (var i = 0; i < countFiles; i++) {
						var reader = new FileReader();
						reader.onload = function(e) {
							$("<img />", {
								"src": e.target.result,
								"class": "thumb-image"
							}).appendTo(image_holder);
						}
						image_holder.show();
						reader.readAsDataURL($(this)[0].files[i]);
					}
				} else {
					alertify.notify("Su navegador no soporta la funcion de vista previa.", 'advertencia');
					$("#fileUpload").val(null);
					$('#image-holder').html('');
				}
			} else {
				alertify.notify("Seleccione solamente imagenes.", "advertencia");
				$("#fileUpload").val(null);
				$('#image-holder').html('');
			}
		}
	});
</script>
<?=$this->endSection()?>