<?=$this->extend('front/main')?>
<?=$this->section('title')?>
	<?=$proyecto['titulo']?>
<?=$this->endSection()?>

<?=$this->section('content')?>
<style>
    .btn{
        font-size: 14px !important;
    }
    .info_proyecto {
        transition: all .8s;
        position: relative;
    }
    .info_proyecto:after{
        content: '';
        border: 1em solid #fe500096;
        border-radius: 50%;
        width: 17px;
        height: 17px;
        position: absolute;
        top: -22px;
        left: -9px;
        animation: animacion 2s linear infinite;
    }
    .info_proyecto:before{
        position: absolute;
        top: -14px;
    }
    @keyframes animacion{
        0%{
            transform: scale(0);
            opacity: 0;
        }
        50%{
            opacity: 1;
        }
        100%{
            transform: scale(1);
            opacity: 0;
        }
    }
</style>
<main class="main proyectos-detalle">
	<section class="section-box">
		<div class="box-head-single">
			<div class="container">
				<h3 class="title-proyecto"><span><?=$proyecto['titulo']?></span></h3>
				<ul class="breadcrumbs">
					<li><a>
						<?php if($proyecto['estatus'] == 'espera'){ ?>
							Trabajo en postulación
						<?php }elseif($proyecto['estatus'] == 'desarrollo'){ ?>
							Trabajo en progreso
						<?php }elseif($proyecto['estatus'] == 'finalizado'){ ?>
							Trabajo finalizado
						<?php }else{?>
							Trabajo
						<?php } ?>
					</a></li>
				</ul>
				<button class="btn btn-regresar btn-border" onclick="history.back()">Regresar</button>
			</div>
		</div>
	</section>
	<section class="section-box mt-10">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12 col-sm-12 col-12">
					<div class="single-image-feature-proyecto">
						<?=view_cell('App\Libraries\FuncionesSistema::carruselProyecto', ['id' => $proyecto['id']])?>
					</div>
					< class="content-single">
						<?php if(!empty($proyecto['desc_corta'])) :?>
    						<p>
    						    <h6>Resumen</h6>
    						    <?=$proyecto['desc_corta']?>
    					    </p>
					    <?php endif ;?>
					    <?php if(!empty($proyecto['descripcion'])) :?>
    					    <p>
    					        <h6>Descripción</h6>
    					        <?=$proyecto['descripcion']?>
    				        </p>
				        <?php endif ;?>
						<?php if(!empty($solicitudes_freelancers)) :?>
    					    <p><h6>Freelancers Asignados</h6></p>
							<?php foreach($solicitudes_freelancers as $freelancer) :?>
								<a class="d-flex align-items-center contenedor_checks rounded-3 gap-2 p-2" href="<?= base_url("usuarios/prevista/".base64_encode($freelancer['id'])) ?>" target="_blank">
									<img class="img-perfil-icon" src="<?= imagenPerfil($freelancer['imagen']) ?>" alt="Foto perfil de">
									<?= $freelancer['nombre'].' '.$freelancer['apellidos'] ?>
								</a>
							<?php endforeach ;?>
				        <?php endif ;?>
					</div>
					<div class="author-single w-100 barra-de-progreso">
						<?php if($proyecto['estatus'] == 'desarrollo'){ ?>
							<?=view_cell('App\Libraries\FuncionesSistema::barraProgreso', ['id' => $proyecto['id']])?>
							<br><br>
						<?php } ?>
					</div>
				</div>
				<div class="col-lg-4 col-md-12 col-sm-12 col-12 pl-40 pl-lg-15 mt-lg-30">
					<div class="sidebar-shadow">
						<div class="sidebar-heading">
							<div class="avatar-sidebar">
								<figure><img alt="mextemps" src="<?= imagenPerfil(session('imagen')) ?>" /></figure>
								<div class="sidebar-info">
									<span class="sidebar-company"><?=session('usuario')?></span>
								</div>
							</div>
						</div>
						<div class="text-center">
							<?php if($proyecto['estatus'] == 'espera'){ ?>
                                <div class="row pt-3">
                                    <div class="col-3 col-lg-3 text-center align-self-center" id="sec-chat">
                                        <?= view_cell('App\Libraries\FuncionesSistema::verPostulantes', ['id' => $proyecto['id']]) ?>
                                    </div>
                                    <div class="col-9 col-lg-9 text-center">
                                        <div class="row">
                                            <div class="col-6 col-md-12 d-grid gap-1 mb-1">
                                                <a class="btn btn-default" href="<?=base_url('/Proyectos/editProyecto/'.$proyecto['id'])?>" title="Editar">
                                                    Editar proyecto
                                                </a>    
                                            </div>
                                            <div class="col-6 col-md-12 d-grid gap-1 mb-1">
                                                <a class="btn btn-naranja eliminar-proyecto" data-id="<?=$proyecto['id']?>" title="Eliminar">
                                                    Eliminar proyecto
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							<?php }elseif($proyecto['estatus'] == 'desarrollo'){ ?>
                                <?= view_cell('App\Libraries\FuncionesSistema::freelanceInteriorProyecto', ['id' => $proyecto['id_freelancer']]) ?>
                                <div class="d-flex justify-content-around">
                                    <div class="tooltips" id="reportar-proyecto">
                                        <i class="fas fa-user-times" style="font-size: x-large;border-radius: 50px;border-bottom: groove;padding: 8px;"></i>
                                        <span class="tooltiptext">Reportar Freelance</span>
                                    </div>
                                    <?= view_cell('App\Libraries\FuncionesSistema::chatProyectoInterior', ['proyecto' => $proyecto['id'], 'freelance' => $proyecto['id_freelancer']]) ?>
                                </div>
                                <?php $finsol = model('Finalizar')->where('id_proyecto', $proyecto['id'])->first(); ?>
                                <div class="d-grid gap-2 my-2" id="sec-boton-fin">
                                    <?php if($finsol != null){ ?>
                                        <?php if($finsol['estatus_contrante'] == null && $finsol['estatus_freelance'] == 'true'){ ?>
                                            <div class="alert btn-sm alert-info text-center p-2 mb-0" role="alert">
                                                <label class="tooltiptext">El freelancer ha notificado que ha finalizado el proyecto.</label>
                                                <div class="tooltips">
                                                    <a class="btn px-1" onclick="finProyecto()" data-id="<?= $proyecto['id'] ?>"><i class="fas fa-check text-success"></i> Confirmar</a>
                                                    <span class="tooltiptext">Finalizar proyecto</span>
                                                </div>
                                                <div class="tooltips">
                                                    <a class="btn px-1" onclick="declinarSoliFin(<?= $proyecto['id'] ?>)" data-id="<?= $proyecto['id'] ?>"><i class="fas fa-times text-danger"></i> Rechazar</a>
                                                    <span class="tooltiptext">Declinar proyecto</span>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <script>
                                    setInterval(function(){
                                        verificarEstSoli(<?=$proyecto['id']?>);
                                    }, 3500);
                                </script>
                            <?php }elseif($proyecto['estatus'] == 'finalizado'){ ?>
                                <div class="d-grid">
                                    <?= view_cell('App\Libraries\FuncionesSistema::chatProyectoInterior', ['proyecto' => $proyecto['id'], 'freelance' => $proyecto['id_freelancer']]) ?>
                                </div>
							<?php }elseif($proyecto['estatus'] == 'cancelado'){ ?>
								<span class="text-danger fs-3 fw-bold">Proyecto Reportado y Cancelado</span>
								<a class="btn" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Ver reporte" id="ver-reporte"><i class="fas fa-id-card-alt"></i></a>
							<?php }?>
						</div>
						<!-- TAREAS DEL PROYECTO -->
						<!---->
						<?php if($proyecto['estatus'] == 'desarrollo' || $proyecto['estatus'] == 'finalizado'){ ?>
							<div class="sidebar-team-member pt-40">
								<h6 class="small-heading">Tareas del proyecto</h6>
								<div class="w-100 border-0" style="min-height: 20px;">
									<div class="row p-2" id="sec-tareas">
                                        <?php if(!empty($proyecto['tareas'])){ ?>
                                            <?=view_cell('App\Libraries\FuncionesSistema::tareasProyectoContra', ['id' => $proyecto['id']])?>
										<?php }else{ ?>
                                            <div class="text-left">El freelancer aún no ha creado tareas para el proyecto.</div>
										<?php } ?>
									</div>
								</div>
							</div>
						<?php } ?>
						<!-- TIPO DE PROYECTO -->
						<div class="sidebar-list-job">
							<ul>
								<li>
									<div class="sidebar-icon-item"><i class="fi-rr-briefcase"></i></div>
									<div class="sidebar-text-info">
										<span class="text-description">Tipo de proyecto</span>
										<strong class="small-heading"><?=$proyecto['categoria']?> / <?=$proyecto['subcategoria']?></strong>
									</div>
								</li>
								<li>
									<div class="sidebar-icon-item"><i class="fi-rr-marker"></i></div>
									<div class="sidebar-text-info">
										<span class="text-description">Locación</span>
										<strong class="small-heading"><?=view_cell('App\Libraries\FuncionesSistema::direccionContratista', ['id' => $proyecto['id_contratista']])?></strong>
									</div>
								</li>
								<li>
									<div class="sidebar-icon-item"><i class="fi-rr-dollar"></i></div>
									<div class="sidebar-text-info">
										<span class="text-description">Presupuesto</span>
										<?php if(!empty($proyecto['presupuesto'])) :?>
											<strong class="small-heading">$ <?= number_format($proyecto['presupuesto'], 2, '.', ',') ?> MXN</strong>
										<?php else :?>
											<strong class="small-heading">Confidencial</strong>
										<?php endif ;?>
									</div>
								</li>
								<li>
									<div class="sidebar-icon-item"><i class="fi-rr-clock"></i></div>
									<div class="sidebar-text-info">
										<span class="text-description">Fecha de creación</span>
										<strong class="small-heading"><?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $proyecto['created_at']])?></strong>
									</div>
								</li>
								<li>
									<div class="sidebar-icon-item"><i class="fi-rr-time-fast"></i></div>
									<div class="sidebar-text-info">
										<span class="text-description">Fecha de entrega esperado</span>
										<strong class="small-heading"><?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $proyecto['fecha_entrega']])?></strong>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<!--MODALS-->
<div class="modal fade" id="reportarModal" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="como-publicar" aria-modal="true" role="dialog">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0 pt-4">
				<h5 class="modal-title fw-bold h4" id="como-publicar">Reportar Freelancer</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-12 col-lg-5 p-2 text-center align-self-center">
						<div>
							<img src="<?= base_url('/assets/images/iconos/sad.png')?>" class="w-50">
						</div>
						<small class="label-modal">
							En forma automática recibe el Freelancer una califcación negativa
						</small>
					</div>
					<div class="col-12 col-lg-7 p-2 align-self-center">
						<form id="form-reporte">
							<div class="mb-3">
								<small class="label-modal">Lamentamos mucho que tengas problemas, pero estamos aqui para apoyarte.</small>
								<br>
								<div class="form-floating">
									<textarea class="form-control" placeholder="Leave a comment here" name="comentario" id="comentarios-reporte" style="height: 100px"></textarea>
									<label class="label-modal" for="comentarios-reporte">Platícanos que pasa y cual es el motivo de reportar al Freelancer.</label>
								</div>
								<input type="hidden" id="id-proyecto" name="proyecto" value="<?=$proyecto['id']?>">
								<input type="hidden" id="id-freelancer" name="freelance" value="<?=$proyecto['id_freelancer']?>">
							</div>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-sm-12 col-lg-6 p-2 align-self-center">
						<div class="text-center">
							<a id="reportar-publicar" class="btn btn-naranja">Reportar y buscar otro Freelancer</a>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-lg-6 p-2 align-self-center">
						<div class="text-center">
							<a id="reportar-cancelar" class="btn btn-naranja">Reportar y cancelar proyecto</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if(!empty($proyecto['id_freelancer'])) :?>
	<div class="modal fade" id="finalizar-proyecto" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="fin-proyect" aria-modal="true" role="dialog">
		<div class="modal-dialog modal-dialog-centered modal-lg">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0">
					<h5 class="modal-title fw-bold h4" id="fin-proyect">Finalizar proyecto</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body pt-0">
					<form id="form-finalizar">
						<div class="row">
							<div class="col-12 col-lg-4">
								<div class="">
								<?php $monto = model('ModelPlanes')->selectMax('monto')->where('cuenta', 'freelancer')->get()->getRowArray()['monto'];
									$con_suscripcion_premium = (model('Suscripciones')
									->builder()
									->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
									->where(['id_usuario' => $proyecto['id_freelancer'], 'estatus' => 'Activo', 'monto' => $monto])
									->get()->getRowArray());
								?>
								<?= view_cell('App\Libraries\FuncionesSistema::cardPostulantes', ['id' => $proyecto['id_freelancer'], 'postu' => 0, 'tipo' => 'talento', 'modal' => 0, 'es_premium' => !empty($con_suscripcion_premium) ? true : false]) ?>
								</div>
							</div>
							<div class="col-12 col-lg-8">
								<div class="fw-bold mb-3">Estás apunto de finalizar el proyecto.</div>
								<span class="fst-italic" data-bs-toggle="tooltip" data-bs-placement="right" title="Titulo del proyecto"><?=$proyecto['titulo']?></span>
								<div class="row">
									<div class="col-12 col-lg-7 pb-1">Estás conforme con el resultado:</div>
									<div class="col-12 col-lg-auto pb-1">
										<select class="form-select" name="conforme" aria-label="Default select example">
											<option value="si">Si</option>
											<option value="no">No</option>
										</select>
									</div>
								</div>
								<div class="mb-3">
									<small class="label-modal">Cuéntanos un poco de tu experiencia en este proyecto.</small>
									<br>
									<textarea class="form-control" placeholder="" id="comentario-finalizado" name="comentario" style="height: 100px"></textarea>
									<input type="hidden" name="proyecto" value="<?=$proyecto['id']?>">
									<input type="hidden" name="freelance" value="<?=$proyecto['id_freelancer']?>">
								</div>
								<small class="label-modal">Califica al Freelancer</small>
								<div class="text-center" id="calificar">
									<p class="clasificacion">
										<input id="radio1" type="radio" name="estrellas" value="5">
										<label for="radio1">
											<i class="fas fa-star"></i>
										</label>
										<input id="radio2" type="radio" name="estrellas" value="4">
										<label for="radio2">
											<i class="fas fa-star"></i>
										</label>
										<input id="radio3" type="radio" name="estrellas" value="3">
										<label for="radio3">
											<i class="fas fa-star"></i>
										</label>
										<input id="radio4" type="radio" name="estrellas" value="2">
										<label for="radio4">
											<i class="fas fa-star"></i>
										</label>
										<input id="radio5" type="radio" name="estrellas" value="1">
										<label for="radio5">
											<i class="fas fa-star"></i>
										</label>
									</p>
								</div>
								<div class="p-3 w-100 text-center">
									<a class="btn btn-naranja" id="btn-fin-proyecto">Finalizar</a>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="ver-reporte-modal" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="ver-reporte-freelance" aria-modal="ture" role="dialog">
<?php endif ;?>
<script>
    function finProyecto(){
        $('#finalizar-proyecto').modal('show');
    }
    function verificarEstSoli(id){
        let fre = $('#est-free').val();
        let contra = $('#est-cont').val();
        $.ajax({
            type: 'POST',
            url: '<?= base_url('/Proyectos/VerificarEstadoSoliFin') ?>',
            data:{id:id, fre:fre, contra:contra},
            success:function(data){
                let cont = JSON.parse(data);
                if(cont.tipo != 'error'){
                    if(cont.con == 'null' || cont.free == 'true'){
                        $('#sec-boton-fin').html(cont.html);
                    }else if(cont.con == 'true'){
                        $('#sec-boton-fin').html(cont.html);
                    }else{
                        $('#sec-boton-fin').html(cont.html);
                    }
                }
            }, error: function(data){
                console.log('Error en la conexion con el servidor.');
            }
        });
    }
    function declinarSoliFin(id){
        $.ajax({
            type: 'POST',
            url: '<?= base_url('/Proyectos/declinarSoliFin') ?>',
            data:{id:id},
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, 'advertencia');
                $('#sec-boton-fin').empty();
                return alertify.notify('Se le ha notificado al freelancer que verifíque todas las tareas.', 'correcto');
            }, error: function(data){
                alertify.notify('Intentelo mas tarde, nuestros servidores se encuentran en mantenimiento.', 'advertencia');
            }
        });
    }
	$('#ver-reporte').click(function(){
		$('#ver-reporte-modal').modal('show');
	});
	$('#reportar-publicar').on('click', function(){
		alertify.dialog('confirm').set({transition:'zoom'}).show();
		alertify.dialog('confirm').set('labels', {ok:'Aceptar', cancel:'Cancelar'});
		alertify.confirm('ATENCIÓN', 'Está a punto de reportar al freelancer, ¿Está seguro de esto?, El área de administración verificará el reporte.', function(){
			$.ajax({
				type: 'POST',
				url: '<?=base_url('/Proyectos/reportarFreelanceRepublicar')?>',
				data:$('#form-reporte').serialize(),
				success: function(data){
					let cont = JSON.parse(data);
					if(cont.tipo == 'advertencia'){
						alertify.notify(cont.mensaje, 'advertencia', 10);
					}else{
						alertify.notify(cont.mensaje, 'correcto', 10);
						setTimeout(function(){
							location.href = '<?=base_url('usuarios/perfil/panel')?>';
						}, 3000);
					}
				}, error: function(data){
					alertify.error('Error, comuníquese con el equipo de soporte.', 10);
				}
			});
		}, function(){});
	});
	$('#reportar-cancelar').on('click', function(){
		alertify.dialog('confirm').set({transition:'zoom'}).show();
		alertify.dialog('confirm').set('labels', {ok:'Aceptar', cancel:'Cancelar'});
		alertify.confirm('ATENCIÓN', 'Está a punto de reportar al freelancer, ¿Está seguro de esto?, El área de administración verificará el reporte.', function(){
			$.ajax({
				type: 'POST',
				url: '<?=base_url('/Proyectos/reportarFreelanceCancelar')?>',
				data:$('#form-reporte').serialize(),
				success: function(data){
					let cont = JSON.parse(data);
					if(cont.tipo == 'advertencia'){
						alertify.notify(cont.mensaje, 'advertencia', 10);
					}else{
						alertify.notify(cont.mensaje, 'correcto', 10);
						setTimeout(function(){
							location.href = '<?=base_url('usuarios/perfil/panel')?>';
						}, 3000);
					}
				}, error: function(data){
					alertify.error('Error, comuníquese con el equipo de soporte.', 10);
				}
			});
		}, function(){});
	});
	$('#btn-fin-proyecto').on('click', function(){
		alertify.dialog('confirm').set({transition:'zoom'}).show();
		alertify.dialog('confirm').set('labels', {ok:'Aceptar', cancel:'Cancelar'}); 
		alertify.confirm('ATENCIÓN', 'Está apunto de finalizar el proyecto, valíde que todo los puntos esten de acuerdo con el freelancer.', function(){
			$.ajax({
				type: 'POST',
				url: '<?=base_url('/Proyectos/finalizarProyecto')?>',
				data:$('#form-finalizar').serialize(),
				success: function(data){
					let cont = JSON.parse(data);
					if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, 'advertencia', 10);
					
					alertify.notify(cont.mensaje, 'correcto', 10);
					setTimeout(function(){ location.href = '<?=base_url('/usuarios/perfil/panel')?>' }, 3000);
				}, error: function(data){
					alertify.error('Error, comuníquese con el area de soporte.', 10);
				}
			});
		}, function(){});
	});
	$('#fin-proyecto').on('click', function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Proyectos/progresoProyectoLoading')?>',
			data:{id:<?=$proyecto['id']?>},
			success: function(data){
				if(data == '100%') return $('#finalizar-proyecto').modal('show');
				
				alertify.notify('El proyecto aún no se encuentra al 100% para ser finalizado.', 'advertencia', 10);
			}, error: function(data){
				alertify.error('Error, comuníquese con el equipo de soporte.', 10);
			}
		});
	});
	$('#reportar-proyecto').on('click', function(){
		$('#reportarModal').modal('show');
	});
	$('.eliminar-proyecto').on('click', function(){
		let id = $(this).attr('data-id');
		alertify.confirm('¡Cuidado!', 'Estás a punto de eliminar el siguiente proyecto, ¿estás seguro de esta decisión?', function(){
			window.location.href = '<?=base_url('/Proyectos/eliminarProyecto')?>?id='+id;
		}, function(){ });
	});
</script>
<?=$this->endSection()?>