<div class="container asignar-freelancer-proyecto">
    <div class="row flex-row-reverse">
        <div class="col-12 col-lg-5 align-self-center">
            <?= view_cell('App\Libraries\FuncionesSistema::cardPostulantes', ['id' => $freelance['id'], 'postu' => 0, 'tipo' => 'asignar', 'modal' => 0, 'es_premium' => $freelance['es_premium']]) ?>
        </div>
        <div class="col-12 col-lg-7 align-self-center">
            <div class="row">
                <div class="col-12 col-lg-7 align-self-center">
                    <?php 
                        setlocale(LC_TIME, "es_MX");
                        $inicio = utf8_encode(strftime('%A, %e de %B de %Y', strtotime($proyecto['fecha_arranque_esperado'])));
                        $entrega = utf8_encode(strftime('%A, %e de %B de %Y', strtotime($proyecto['fecha_entrega'])))
                    ?>
                    Titulo del proyecto: <b><?= $proyecto['titulo'] ?></b><br>
                    Fecha de inicio: <span class="fw-bold"><?=$inicio?></span><br>
                    Fecha de entrega estimada: <span class="fw-bold"><?=$entrega?></span>
                    <input type="hidden" value="<?=$proyecto['id']?>" id="proyecto-a-asignar">
                    <input type="hidden" value="<?=$freelance['id']?>" id="freelance-esc">
                </div>
                <div class="col-12 col-lg-5 align-self-center">
                    <div class="tooltips text-center">
                        <button class="btn btn-naranja btn-border disabled" onclick="asignar()" id="btn-asignar-proyecto">Asignar</button>
                        <span class="tooltiptext" id="tooltipTerminos" style="top: 0;left: 100%;">OPPS... Acepta primero términos y condiciones</span>
                    </div>
                </div>
                <div class="col-12 mt-50">
                    <hr>
                    <div class="mb-3 form-check">
                        <input type="checkbox" class="form-check-input" id="aceptar-asignar">
                        <label class="form-check-label text-font opciones-ingresar" for="aceptar-asignar">
                            Aceptas los <a href="<?=base_url('/terminos-y-condiciones')?>" target="_blank">Términos y Condiciones</a> al asignar este proyecto
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#aceptar-asignar').on('change', function(){
        if($(this).prop('checked')){
            $('#btn-asignar-proyecto').removeClass('disabled');
            $('#tooltipTerminos').addClass('d-none')
        }else{
            $('#btn-asignar-proyecto').addClass('disabled');
            $('#tooltipTerminos').removeClass('d-none')
        }
    });
</script>