<?php if($tipo == 0){ ?>
    <a class="crear-chat" style="cursor: pointer;" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Crear nueva sala de chat." data-post="<?=$post?>">
        <i class="fas fa-comments"></i>
    </a>
<?php }else{ ?>
    <a class="ver-chat" style="cursor: pointer;" data-post="<?=$post?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Ingresar a sala de chat">
        <i class="fas fa-comments"></i>
    </a>
<?php } ?>