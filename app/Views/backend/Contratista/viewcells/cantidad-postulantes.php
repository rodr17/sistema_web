<?php if(count($data) > 0){ ?>
    <div class="tooltips">
        <a type="button" class="btn position-relative" data-bs-target="#<?=$modal?>" data-bs-toggle="modal">
            <i class="fas fa-user">
                <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger"><?php echo count($data); ?></span>
            </i>
        </a>
        <span class="tooltiptext">Ver postulantes</span>
    </div>
    <div class="modal fade cantidad-de-postulantes" id="<?=$modal?>" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="exampleModalToggleLabel" tabindex="-1" aria-modal="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-fullscreen">
            <div class="modal-content bg-transparent">
                <div class="modal-header border-0">
                    <div class="text-center w-100">
                    </div>
                    <a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#<?=$modal?>" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
                </div>
                <div class="modal-body info-detalles">
                    <div class="text-center w-100">
                        <h6 class="text-white titulo">Freelancer postulados a tu proyecto</h6>
                        <p class="text-font text-white descripcion">Aquí puedes ver los postulantes, para participar en tu proyecto</p>
                        <div class="my-3 p-3">
                            <legend class="text-start text-font m-0" name="Programación y Tecnología">
                                <!--Postulantes-->
                            </legend>
                            <div class="row justify-content-center listado-talentos">
                                <?php foreach ($data as $post) { ?>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-3">
                                        <?= view_cell('App\Libraries\FuncionesSistema::cardPostulantes', ['id' => $post['id_postulante'], 'postu' => $post['id'], 'tipo' => 'postulante', 'modal' => $modal, 'es_premium' => $post['premium']]) ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div> 
    </div>
<?php }else{ ?>
    <div class="tooltips">
        <a type="button" class="btn position-relative" title="Sin postulantes">
            <i class="fas fa-user"></i>
        </a>
        <span class="tooltiptext">Sin postulantes</span>
    </div>
<?php } ?>