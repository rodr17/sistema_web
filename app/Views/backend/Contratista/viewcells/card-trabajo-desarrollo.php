<div class="card-job hover-up wow animate__  animate__fadeIn animated <?= $trabajo['destacado'] ? 'proyecto_destacado' : ''?>" style="visibility: visible; animation-name: fadeIn;">
	<div class="card-job-top">
		<div class="card-job-top--info">
			<h6 class="card-job-top--info-heading"><a href="<?=base_url('/Proyectos/viewProyecto/'.$trabajo['id'])?>" class="text-decoration-none"><?=$trabajo['titulo']?></a></h6>
			<div class="row">
				<div class="col-lg-7">
					<?= view_cell('App\Libraries\FuncionesSistema::nivelInsigniaProyecto', ['nivel' => $trabajo['nivel']]) ?>
					<span class="card-job-top--type-job text-sm"><i class="fi-rr-briefcase"></i> <?=view_cell('App\Libraries\FuncionesSistema::diferenciaCantidadTiempo', ['inicio' => $trabajo['fecha_arranque_esperado'], 'fin' => $trabajo['fecha_entrega']])?></span>
					<span class="card-job-top--post-time text-sm text-capitalize"><i class="fi-rr-clock"></i> <?=view_cell('App\Libraries\FuncionesSistema::diferenciaFechas', ['fecha' => $trabajo['created_at']])?></span>
				</div>
				<div class="col-lg-5 text-lg-end">
					<span class="card-job-top--price">
						<?php if($trabajo['presupuesto'] == 0) :?>
							<label class="text-md">Presupuesto condifencial</label>
						<?php else :?>
							$<?= number_format($trabajo['presupuesto'], 2, '.', ',') ?> <small>MXN</small><span>/Presupuesto</span>
						<?php endif;?>
					</span>
					<!-- <span class="card-job-top--price"><= '$'.number_format($trabajo['presupuesto'], 2, '.', ',').' <small>MXN</small>'?><span>/Presupuesto</span></span> -->
					<p class="text-font" style="text-align: right;">Fecha de entrega sugerida: <span class="fw-bold text-azul"><?=$trabajo['fecha_entrega']?></span></p>
				</div>
			</div>
		</div>
	</div>
	<div class="card-job-description">
		<?= word_limiter($trabajo['descripcion'], 30, '...') ?>
	</div>
	<div class="card-job-bottom mt-25">
		<div class="row flex-wrap align-items-center">
			<div class="col-12 col-lg-4">
				<?= view_cell('App\Libraries\FuncionesSistema::chatProyecto', ['proyecto' => $trabajo['id'], 'freelance' => $trabajo['id_freelancer']]) ?> <a href="<?=base_url('/usuarios/prevista/'.base64_encode($trabajo['id_freelancer']))?>" target="_blank" class="btn position-relative botones-proyectos-iconos" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Ver perfil de freelance"><i class="fas fa-user" aria-hidden="true"></i></a>
				Estatus: <span class="text-azul">Progreso</span>
				<!-- <a href="#" class="btn btn-small background-15 mr-5" style="background-color: #ffcfc5;"><?=$trabajo['categoria']?></a>
				<a href="#" class="btn btn-small background-15 mr-5" style="background-color: #ffcfc5;"><?=$trabajo['subcategoria']?></a> -->
			</div>
			<div class="col-12 col-lg-3">
				<?php $html = view_cell('App\Libraries\FuncionesSistema::barraProgreso', ['id' => $trabajo['id']]);
				if(!empty($html)) :?>
				
					<?=$html?>
				<?php endif;?>
			</div>
			<div class="col-12 col-lg-4 mt-10 offset-lg-1">
				<div class="d-grid">
					<?php $finsol = model('Finalizar')->where('id_proyecto', $trabajo['id'])->first(); ?>
					<a class="btn btn-default" style="color: #ffffff;" href="<?=base_url('/Proyectos/viewProyecto/'.$trabajo['id'])?>">Ver proyecto</a>
					<?php if($finsol != null){ ?>
						<div class="d-grid gap-2 my-2" id="sec-boton-fin-<?=$trabajo['id']?>">
							<?php if($finsol['estatus_contrante'] == null && $finsol['estatus_freelance'] == 'true'){ ?>
								<div class="alert btn-sm alert-info text-center p-2 mb-0" role="alert">
									<label>El freelancer ha notificado que ha finalizado el proyecto.</label>
									<div class="tooltips">
										<a class="btn px-1" onclick="finProyecto('finalizar-proyecto-<?=$trabajo['id']?>')" data-id="<?= $trabajo['id'] ?>"><i class="fas fa-check text-success"></i> Confirmar</a>
										<span class="tooltiptext d-none d-md-block">Finalizar proyecto</span>
									</div>
									<div class="tooltips">
										<a class="btn px-1" onclick="declinarSoliFin(<?= $trabajo['id'] ?>)" data-id="<?= $trabajo['id'] ?>"><i class="fas fa-times text-danger"></i> Rechazar</a>
										<span class="tooltiptext d-none d-md-block">Declinar proyecto</span>
									</div>
								</div>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="finalizar-proyecto-<?=$trabajo['id']?>" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="fin-proyect" aria-modal="true" role="dialog">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0">
				<h5 class="modal-title fw-bold h4" id="fin-proyect">Finalizar proyecto</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body pt-0">
				<form id="form-finalizar-<?=$trabajo['id']?>">
					<div class="row">
						<div class="col-12 col-lg-4">
							<div class="">
								<?= view_cell('App\Libraries\FuncionesSistema::cardPostulantes', ['id' => $trabajo['id_freelancer'], 'postu' => 0, 'tipo' => 'talento', 'modal' => 0, 'es_premium' => '']) ?>
							</div>
						</div>
						<div class="col-12 col-lg-8">
							<label><span class="fw-bold">Título del proyecto: </span><?=$trabajo['titulo']?></label>
							<div class="fw-bold my-3">Estás a punto de finalizar el proyecto.</div>
							<div class="d-flex justify-content-around gap-2 mb-3">
								<label class="small w-100 mb-0" style="line-height: 2rem;">¿Estás conforme con el resultado? :</label>
								<select class="form-select" name="conforme" aria-label="Default select example">
									<option value="si">Si</option>
									<option value="no">No</option>
								</select>
							</div>
							<div class="mb-3">
								<small class="label-modal">Cuéntanos un poco de tu experiencia en este proyecto.</small>
								<br>
								<textarea class="form-control" placeholder="" id="comentario-finalizado" name="comentario" style="height: 100px"></textarea>
								<input type="hidden" name="proyecto" value="<?=$trabajo['id']?>">
								<input type="hidden" name="freelance" value="<?=$trabajo['id_freelancer']?>">
							</div>
							<small class="label-modal">Califica al Freelancer</small>
							<div class="text-center" id="calificar">
								<p class="clasificacion">
									<input id="radio1-<?=$trabajo['id']?>" type="radio" name="estrellas" value="5">
									<label for="radio1-<?=$trabajo['id']?>">
										<i class="fas fa-star"></i>
									</label>
									<input id="radio2-<?=$trabajo['id']?>" type="radio" name="estrellas" value="4">
									<label for="radio2-<?=$trabajo['id']?>">
										<i class="fas fa-star"></i>
									</label>
									<input id="radio3-<?=$trabajo['id']?>" type="radio" name="estrellas" value="3">
									<label for="radio3-<?=$trabajo['id']?>">
										<i class="fas fa-star"></i>
									</label>
									<input id="radio4-<?=$trabajo['id']?>" type="radio" name="estrellas" value="2">
									<label for="radio4-<?=$trabajo['id']?>">
										<i class="fas fa-star"></i>
									</label>
									<input id="radio5-<?=$trabajo['id']?>" type="radio" name="estrellas" value="1">
									<label for="radio5-<?=$trabajo['id']?>">
										<i class="fas fa-star"></i>
									</label>
								</p>
							</div>
							<div class="p-3 w-100 text-center">
								<a class="btn btn-naranja btn-fin-proyecto" onclick="finProyectoCalificacion('form-finalizar-<?=$trabajo['id']?>')">Finalizar</a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>