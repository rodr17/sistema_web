<?php $id_proyecto = $trabajo['id_proyecto'] ?? $trabajo['id'] ?>
<div class="card-job wow animate__  animate__fadeIn animated <?= $trabajo['destacado'] == 1 ? 'proyecto_destacado' : ''?>" id="proyecto_<?= $id_proyecto ?>" style="padding: 26px 23px 38px;">
	<div class="card-job-top">
		<div class="card-job-top--info">
			<div class="d-flex justify-content-between">
				<h6 class="card-job-top--info-heading"><a href="<?= base_url("Proyectos/viewProyecto/$id_proyecto") ?>" class="text-decoration-none"><?=$trabajo['titulo']?></a></h6>
				<?php if(session('id') == $trabajo['id_contratista']):?>
					<div class="form-check form-switch">
						<input class="form-check-input" type="checkbox" role="switch" id="check_destacado_<?= $id_proyecto ?>" <?= $trabajo['destacado'] == 1 ? 'checked' : ''?>>
						<label class="form-check-label" for="check_destacado_<?= $id_proyecto ?>">Destacado</label>
					</div>
				<?php endif ;?>
			</div>
			<div class="row">
				<div class="col-lg-7">
					<?= view_cell('App\Libraries\FuncionesSistema::nivelInsigniaProyecto', ['nivel' => $trabajo['nivel']]) ?>
					<span class="card-job-top--type-job text-sm"><i class="fi-rr-briefcase"></i> <?=view_cell('App\Libraries\FuncionesSistema::diferenciaCantidadTiempo', ['inicio' => $trabajo['fecha_arranque_esperado'], 'fin' => $trabajo['fecha_entrega']])?></span>
					<span class="card-job-top--post-time text-sm text-capitalize"><i class="fi-rr-clock"></i> <?=view_cell('App\Libraries\FuncionesSistema::diferenciaFechas', ['fecha' => $trabajo['created_at']])?></span>
				</div>
				<div class="col-lg-5 text-lg-end">
					<span class="card-job-top--price">
						<?php if($trabajo['presupuesto'] == 0) :?>
							<label class="text-md">Presupuesto condifencial</label>
						<?php else :?>
							$<?= number_format($trabajo['presupuesto'], 2, '.', ',') ?> <small>MXN</small><span>/Presupuesto</span>
						<?php endif;?>
					</span>
					<!-- <span class="card-job-top--price"><= '$'.number_format($trabajo['presupuesto'], 2, '.', ',').' <small>MXN</small>'?><span>/Presupuesto</span></span> -->
					<p class="text-font" style="text-align: right;">Fecha de entrega sugerida: <span class="fw-bold text-azul"><?=$trabajo['fecha_entrega']?></span></p>
				</div>
			</div>
		</div>
	</div>
	<div class="card-job-description">
		<?= character_limiter($trabajo['descripcion'], 200, '...') ?>
	</div>
	<div class="card-job-bottom mt-25">
		<div class="row flex-wrap">
			<div class="col-12 col-sm-8">
				<?= view_cell('App\Libraries\FuncionesSistema::verPostulantes', ['id' =>  $id_proyecto]) ?>
				<label class="mb-0 ms-3">Estatus: <span class="text-naranja">Por Asignar</span></label>
			</div>
			<div class="col-12 col-sm-4">
				<div class="d-grid gap-2">
					<a class="btn btn-default" style="color: #ffffff;" href="<?=base_url("Proyectos/viewProyecto/$id_proyecto")?>">Ver proyecto</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if(session('id') == $trabajo['id_contratista']) :?>
	<script type="text/javascript">
		$('#check_destacado_<?= $id_proyecto ?>').change(function(e){
			e.preventDefault();
			$btn = $(this);
			let valor = $btn.is(':checked');
			$.ajax({
				url: '<?=base_url('Proyectos/destacarProyecto')?>',
				type: 'POST',
				data: {check : valor, id : <?= $id_proyecto ?>},
				success: function (data) {
					let respuesta = JSON.parse(data);
					if(respuesta.alerta != 'correcto'){
						$btn.prop('checked', false);
						$('#body-alerta').html(respuesta.mensaje);
						return $('#aumentar-membresia').modal('show');
					}
					
					if(valor) $('#proyecto_<?= $id_proyecto ?>').addClass('proyecto_destacado');
					else $('#proyecto_<?= $id_proyecto ?>').removeClass('proyecto_destacado');
					
					$btn.attr('checked', valor);
					return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				},
				error: function (data) {
					let respuesta = JSON.parse(data.responseText);
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
				}
			});
		});
	</script>
<?php endif ;?>