<div class="card-job hover-up wow animate__  animate__fadeIn animated <?= $trabajo['destacado'] ? 'proyecto_destacado' : ''?>" style="visibility: visible; animation-name: fadeIn;">
	<div class="card-job-top">
		<div class="card-job-top--info">
			<h6 class="card-job-top--info-heading"><a href="<?=base_url('/Proyectos/viewProyecto/'.$trabajo['id'])?>" class="text-decoration-none"><?=$trabajo['titulo']?></a></h6>
			<div class="row">
				<div class="col-lg-7">
					<?= view_cell('App\Libraries\FuncionesSistema::nivelInsigniaProyecto', ['nivel' => $trabajo['nivel']]) ?>
					<span class="card-job-top--type-job text-sm"><i class="fi-rr-briefcase"></i> <?=view_cell('App\Libraries\FuncionesSistema::diferenciaCantidadTiempo', ['inicio' => $trabajo['fecha_arranque_esperado'], 'fin' => $trabajo['fecha_entrega']])?></span>
					<span class="card-job-top--post-time text-sm text-capitalize"><i class="fi-rr-clock"></i> <?=view_cell('App\Libraries\FuncionesSistema::diferenciaFechas', ['fecha' => $trabajo['created_at']])?></span>
				</div>
				<div class="col-lg-5 text-lg-end">
					<span class="card-job-top--price">
						<?php if($trabajo['presupuesto'] == 0) :?>
							<label class="text-md">Presupuesto condifencial</label>
						<?php else :?>
							$<?= number_format($trabajo['presupuesto'], 2, '.', ',') ?> <small>MXN</small><span>/Presupuesto</span>
						<?php endif;?>
					</span>
					<!-- <span class="card-job-top--price"><= '$'.number_format($trabajo['presupuesto'], 2, '.', ',').' <small>MXN</small>'?><span>/Presupuesto</span></span> -->
					<p class="text-font" style="text-align: right;">Fecha de entrega sugerida: <span class="fw-bold text-azul"><?=$trabajo['fecha_entrega']?></span></p>
				</div>
			</div>
		</div>
	</div>
	<div class="card-job-description">
		<?= word_limiter($trabajo['descripcion'], 30, '...') ?>
	</div>
	<div class="card-job-bottom mt-25">
		<div class="row flex-wrap align-items-center">
			<div class="col-12 col-lg-4">
				<?= view_cell('App\Libraries\FuncionesSistema::chatProyecto', ['proyecto' => $trabajo['id'], 'freelance' => $trabajo['id_freelancer']]) ?> <a href="<?=base_url('/usuarios/prevista/'.base64_encode($trabajo['id_freelancer']))?>" target="_blank" class="btn position-relative" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Ver perfil de freelance"><i class="fas fa-user" aria-hidden="true"></i></a>
				Estatus: <span class="text-success">Finalizado</span>
				<!-- <a href="#" class="btn btn-small background-15 mr-5" style="background-color: #ffcfc5;"><?=$trabajo['categoria']?></a>
				<a href="#" class="btn btn-small background-15 mr-5" style="background-color: #ffcfc5;"><?=$trabajo['subcategoria']?></a> -->
			</div>
			<div class="col-12 col-lg-3">
				<?=view_cell('App\Libraries\FuncionesSistema::barraProgreso', ['id' => $trabajo['id']])?>
			</div>
			<div class="col-12 col-lg-4 mt-10 offset-lg-1">
				<div class="d-grid gap-2">
					<a class="btn btn-default" style="color: #ffffff;" href="<?=base_url('/Proyectos/viewProyecto/'.$trabajo['id'])?>">Ver proyecto</a>
				</div>
			</div>
		</div>
	</div>
</div>