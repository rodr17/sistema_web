<div class="row justify-content-center carousel slide" data-bs-ride="carousel" id="carrusel-solicitud">
    <div class="carousel-inner text-center">
        <div class="carousel-item">
            <img class="w-50" src="https://mextemps.tresesenta.lat/assets/images/Buscador1.webp" alt="">
        </div>
        <div class="carousel-item">
            <img class="w-50" src="https://mextemps.tresesenta.lat/assets/images/Buscador2.webp" alt="">
        </div>
        <div class="carousel-item active">
            <img class="w-50" src="https://mextemps.tresesenta.lat/assets/images/Buscador3.webp" alt="">
        </div>
    </div>
    <button class="carousel-control-prev margenIzq" type="button" data-bs-target="#carrusel-solicitud" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next margenDer" type="button" data-bs-target="#carrusel-solicitud" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>