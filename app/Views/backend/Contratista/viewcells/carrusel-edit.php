<?php if(count($imagenes) > 1){ ?>
	<div class="row justify-content-center carousel slide galeria-proyecto-editar" data-bs-ride="carousel" id="carrusel-solicitud">
		<div class="carousel-inner text-center overflow-auto">
			<?php for($i = 0; $i < count($imagenes); $i++){ ?>
				<div class="carousel-item <?= $i == 0 ? 'active' : '' ?>">
					<div class="position-relative">
						<img class="imagen_carrusel_proyectos" src="<?=base_url($ruta.'/'.$imagenes[$i])?>" alt="Imágen del proyecto">
						<button class="elim-img" data-img="<?=$imagenes[$i]?>"><i class="fas fa-times"></i></button>
					</div>
				</div>
			<?php } ?>
		</div>
		<button class="carousel-control-prev margenIzq" type="button" data-bs-target="#carrusel-solicitud" data-bs-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="visually-hidden">Previous</span>
		</button>
		<button class="carousel-control-next margenDer" type="button" data-bs-target="#carrusel-solicitud" data-bs-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="visually-hidden">Next</span>
		</button>
	</div>
<?php }else{ ?>
	<div class="row justify-content-center carousel slide imagen-editar-proyecto" data-bs-ride="carousel" id="carrusel-solicitud">
		<div class="carousel-inner text-center overflow-auto">
			<?php for($i = 0; $i < count($imagenes); $i++){ ?>
				<div class="carousel-item active">
					<div class="position-relative">
						<img class="imagen_carrusel_proyectos" src="<?=base_url($ruta.'/'.$imagenes[$i])?>" alt="Imágen del proyecto">
						<button class="elim-img" data-img="<?=$imagenes[$i]?>"><i class="fas fa-times"></i></button>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
<?php } ?>
<script>
	$('.elim-img').click(function(e){
		e.preventDefault();
		let img = $(this).attr('data-img');
		alertify.confirm('ATENCION','¿Esta seguro de eliminar la siguiente imagen? Se eliminara de manera permanente de la plataforma.', function(){
			$.ajax({
				type: 'POST',
				url: '<?=base_url('/Proyectos/EliminarImagen')?>',
				data:{img:img, id:<?=$id?>},
				success: function(data){
					$('#galeria-edit').html(data);
				}, error: function(){
					alertify.error('Comuniquese con el area de soporte', 10);
				}
			});           
		}, function(){});
	});
</script>