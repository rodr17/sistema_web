<?php if(count($imagenes) > 1){ ?>
	<div class="row justify-content-center carousel slide" data-bs-ride="carousel" id="carrusel-solicitud">
		<div class="carousel-inner text-center imagen-proyecto-detalles overflow-auto pe-0">
			<?php for($i = 0; $i < count($imagenes); $i++){ ?>
				<div class="carousel-item <?= $i == 0 ? 'active' : '' ?>">
					<img class="imagen_carrusel_proyectos" src="<?=base_url($ruta.'/'.$imagenes[$i])?>" alt="Imágen de proyecto">
				</div>
			<?php } ?>
		</div>
		<button class="carousel-control-prev margenIzq" type="button" data-bs-target="#carrusel-solicitud" data-bs-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="visually-hidden">Previous</span>
		</button>
		<button class="carousel-control-next margenDer" type="button" data-bs-target="#carrusel-solicitud" data-bs-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="visually-hidden">Next</span>
		</button>
	</div>
<?php }else{ ?>
	<div class="row justify-content-center carousel slide" data-bs-ride="carousel" id="carrusel-solicitud">
		<div class="carousel-inner text-center imagen-proyecto-info overflow-auto">
			<?php for($i = 0; $i < count($imagenes); $i++){ ?>
				<div class="carousel-item active">
					<img class="imagen_carrusel_proyectos" src="<?=base_url($ruta.'/'.$imagenes[$i])?>" alt="Imágen de proyecto">
				</div>
			<?php } ?>
		</div>
	</div>
<?php } ?>