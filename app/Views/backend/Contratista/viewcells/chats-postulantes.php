<?php if(!empty($data)){ ?>
    <a class="btn btn-primary" data-bs-toggle="collapse" href="#postulantes-collapse" role="button" aria-expanded="false" aria-controls="postulantes-collapse">
        Postulantes
    </a>
    <div class="collapse" id="postulantes-collapse">
        <div class="card card-body" style="max-height: 200px; overflow-y: auto;">
            <div class="d-grid gap-2">
                <?php foreach($data as $d){ ?>
                    <?php $user = model('Autenticacion')->get_id($d['id_postulante']); ?>
                    <?php if($user != null){ ?>
                        <a class="btn btn-naranja <?=($d)? 'ver-chat' : 'crear-chat'?>" <?=($d)? 'data-post="'.$d['id'].'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Ingresar a sala de chat"': 'data-bs-toggle="tooltip" data-bs-placement="bottom" title="Crear nueva sala de chat." data-post="'.$d['id'].'"'?>>
                            <div class="position-relative">
                                <img alt="mextemps" src="<?= imagenPerfil($user) ?>" class="align-middle img-perfil-icon"/>
                                <span class="sidebar-company"><?=$user['nombre'].' '.$user['apellidos']?></span>
                            </div>
                        </a>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
    <script>
        $('.crear-chat').click(function(){
            let postu = $(this).attr('data-post');
            alertify.confirm('Cuidado', 'Esta a punto de generar un nuevo chat de negociación, ¿Esta seguro de esto?', function(){
                $.ajax({
                    type: 'POST',
                    url: '<?=base_url('/Proyectos/verchat')?>',
                    data:{postu:postu},
                    success: function(data){
                        let contenido = JSON.parse(data);
                        if(contenido.tipo == 'error'){
                            alert(contenido.mensaje);
                            // alertify.error(contenido.mensaje, 10);
                        }else if(contenido.tipo == 'nuevo'){
                            alertify.success(contenido.mensaje);
                            setTimeout(function(){
                                window.location.href = contenido.liga;
                            }, 3000);
                        }else if(contenido.tipo == 'existente'){
                            window.location.href = contenido.liga;
                        }else{
                            alertify.error('A surguido un error, comuniquese con el area de soporte.', 10);
                        }
                    }, error: function(data){
                        alertify.error('A surguido un error, comuniquese con el area de soporte.', 10);
                    }
                });
            }, function(){
                alertify.notifiy('Operacion cancelada', 10);
            });
        });
        $('.ver-chat').click(function(){
            let postu = $(this).attr('data-post');
            $.ajax({
                type: 'POST',
                url: '<?=base_url('/Proyectos/verchat')?>',
                data: {postu:postu},
                success: function(data){
                    let contenido = JSON.parse(data);
                    if(contenido.tipo != 'error'){
                        window.location.href = contenido.liga;
                    }else{
                        // alertify.warning(contenido.mensaje);
                        alert(contenido.mensaje);
                    }
                }, error: function(data){
                    alertify.error('A surgido un error, comuniquese con el area de soporte.', 10);
                }
            });
        });
    </script>
<?php } ?>