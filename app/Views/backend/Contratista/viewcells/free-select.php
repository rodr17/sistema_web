<div class="col-12 col-sm-12 col-lg-6 p-2" id="toast<?=$freelance['id']?>option">
    <div class="toast d-block w-100" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-header">
            <img src="<?= imagenPerfil($freelance) ?>" class="rounded-circle me-2" alt="..." style="width: 25px;">
            <strong class="me-auto"></strong>
            <small class="text-truncate"><?=$freelance['correo']?></small>
            <button type="button" class="btn-close cerrar-toast" onclick="removeFreelance('toast<?=$freelance['id']?>option', '<?=$freelance['id']?>')" aria-label="Close"></button>
        </div>
        <div class="toast-body">
            <strong class="me-auto" data-bs-toggle="tooltip" data-bs-placement="top" title="Ver perfil de freelance."><a class="text-decoration-none" target="_blank" href="<?=base_url('/Usuarios/prevista/'.base64_encode($freelance['id'])) ?>"><?=$freelance['nombre']?> <?=$freelance['apellidos']?></a></strong>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $label = $('#cont_solicitudes_diponibles');
        if($label.text() == 'Ilimitadas') return false;
        let cantidad = parseInt($label.text())-1;
        // console.log(cantidad);
        if(cantidad == 0) $('#search-freelance').attr('disabled', true);
        else $('#search-freelance').attr('disabled', false);
        $label.text(cantidad);
    });
</script>