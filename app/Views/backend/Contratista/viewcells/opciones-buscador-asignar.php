<?php if(count($freelancer) == 0){ ?>
    <?=vacio('Sin freelancers encontrados')?>
<?php }else{ ?>
    <?php foreach($freelancer as $f){ ?>
        <a data-user="<?=$f['id']?>" href="#" class="sel-freelance text-decoration-none">
            <div class="d-flex p-2">
                <div class="flex-shrink-0">
                    <img class="rounded-circle" src="<?= imagenPerfil($f) ?>" style="max-width: 50px;">
                </div>
                <div class="flex-shrink-1 ms-3">
                    <label class="text-font"><?=$f['nombre_completo']?> <br><small class="small">(<?=$f['correo']?>)</small></label>
                </div>
            </div>
        </a>
    <?php } ?>
    <script>
        $(document).change(function(){
            $('.cerrar-toast').on('click', function(){
                let tost = $(this).attr('data-eliminar');
                $('#'+tost).remove();
            });
        });
        $('.sel-freelance').click(function(){
            let user = $(this).attr('data-user');
            let cont = $('#free-select').val().trim();
            $('#search-freelance').val('');
            $.ajax({
                type: 'POST',
                url: '<?=base_url('/Proyectos/freeSeleccionado')?>',
                data:{user:user},
                success: function(data){
                    $('#free-seleccionados').append(data);
                    $('#lista-freelance').removeClass('d-block');
                }, error: function(data){
                    alertify.notify('Comuniquese con el area de soporte', 'falla', 10);
                }
            });
            if(cont == ''){
                $('#free-select').val(user);
            }else{
                $('#free-select').val(cont+','+user);
            }
        });
    </script>
<?php } ?>