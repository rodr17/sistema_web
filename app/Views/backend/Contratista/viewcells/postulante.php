<div class="card-grid-2 ">
    <div class="card-grid-2-link">
        <?php if ($tipo == 'postulante') { ?>
            <?= view_cell('App\Libraries\FuncionesSistema::validarChat', ['postulacion' => $post]) ?>
        <?php } ?>
    </div>
    <div class="text-center card-grid-2-image-rd online fondo-uno position-relative <?= $user['es_premium'] ? 'freelancer_destacado' : '' ?>">
        <?php if($user['es_premium']) :?>
			<span class="position-absolute badge etiq-destacado bg-white" style="top: 10px;right: 10px;color: var(--color-naranja);font-size: .7rem;"><i class="fas fa-user"></i> Destacado</span>
		<?php endif ;?>
        <a href="<?= base_url('Usuarios/prevista') . '/' . base64_encode($user['id']) ?>" target="_blank">
            <figure><img class="imagen-freelancer" alt="Mextemps" src="<?= imagenPerfil($user) ?>"></figure>
        </a>
        <div class="card-profile area-uno">
            <h5 class="py-0 el-titulo" style="min-height: 42px;">
                <a href="<?= base_url('Usuarios/prevista') . '/' . base64_encode($user['id']) ?>" target="_blank">
                    <strong><?= $user['nombre'] ?> <?= $user['apellidos'] ?></strong>
                </a>
            </h5>
            <p class="descripcion-freelancer"><?php echo substr_replace($user['historial'], "...", 100); ?></p>
            <div class="ubicacion-usuario mt-15">
                <div class="col-sm-12 text-left align-items-left">
                    <i class="fi-rr-marker mr-5"></i> <?= isset($user['direccion']['estado']) ? $user['direccion']['estado'] . ', ' : '' ?><?= isset($user['direccion']['pais']) ? $user['direccion']['pais'] : '' ?>
                </div>
            </div>
        </div>
    </div>
    <div class="card-block-info pt-0 como-te-ven fondo-dos">
        <div class="card-profile area-dos">
            <div class="fila-rate-nivel">
                <div class="columna-zquierda">
                    <?= view_cell('App\Libraries\FuncionesSistema::verCalificación', ['id' => $user['id']]) ?>
                    <div class="mis-habilidades text-left">
                        <span class="text-sm"><?= view_cell('App\Libraries\FuncionesSistema::obtenerHabilidad', ['id' => $user['id']]) ?></span>
                    </div>
                    <span class="insignias-lsitado d-block">
                        <span class="text-md"><?= view_cell('App\Libraries\FuncionesSistema::nivelInsignia', ['cantidad' => view_cell('App\Libraries\FuncionesSistema::cantProyectosFinalizados', ['id' => $user['id'], 'rol' => $user['rol']])]) ?></span>
                        <?= view_cell('App\Libraries\FuncionesSistema::miNivelInsigniaCard', ['cantidad' => view_cell('App\Libraries\FuncionesSistema::cantProyectosFinalizados', ['id' => $user['id'], 'rol' => $user['rol']])]) ?>
                    </span>
                </div>
                <div class="text-right mt-25 mb-5 botones-free">
                    <a href="<?= base_url('Usuarios/prevista') . '/' . base64_encode($user['id']) ?>" target="_blank" class="btn btn-naranja">Ver perfil</a>
                    <?php if ($tipo == 'postulante') { ?>
                        <a class="btn btn-naranja desplegar-modal asignar" onclick="asignarProyecto(<?= $post ?>, <?= $modal ?>)" data-postu="<?= $post ?>" data-show="modalasignar" data-close="<?= $modal ?>">
                            Asignar
                        </a>
                        <script>
                            $('.desplegar-modal').on('click', function() {
                                let show = $(this).attr('data-show');
                                let hide = $(this).attr('data-close');
                                $('#' + hide).modal('hide');
                                $('#' + show).modal('show');
                                $('#' + show + ' .desplegar-modal').attr('data-show', hide);
                            });
                            $('.asignar').on('click', function() {
                                let post = $(this).attr('data-postu');
                                $.ajax({
                                    type: 'POST',
                                    url: '/Proyectos/preAsignarProyecto',
                                    data: {
                                        post: post
                                    },
                                    success: function(data) {
                                        $('#asignar-proyecto').html(data);
                                    },
                                    error: function(data) {
                                        alertify.notify('Contácte con el área de soporte.', 'falla', 10);
                                    }
                                });
                            });
                        </script>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="card-block-info fondo-tres">
        <div class="card-2-bottom card-2-bottom-candidate area-tres">
            <div class="cuantos-proyectos d-flex justify-content-left align-items-left">
                <span class="text-muted text-small d-flex align-items-center"><?= view_cell('App\Libraries\FuncionesSistema::cantProyectos', ['id' => $user['id']]) ?></span>
            </div>
            <div class="hora-free">
                <span class="col-sm-12 costo-por-hora">
                    <i class="fa fa-usd" aria-hidden="true"></i> <?= isset($user['pagoHora']) ? $user['pagoHora'] : '' ?><small>/hr</small>
                </span>
            </div>
        </div>
    </div>
</div>
<script>
    $('.crear-chat').click(function() {
        let postu = $(this).attr('data-post');
        alertify.confirm('Cuidado', 'Esta a punto de generar un nuevo chat de negociación, ¿Esta seguro de esto?', function() {
            $.ajax({
                type: 'POST',
                url: '<?= base_url('/Proyectos/verchat') ?>',
                data: {
                    postu: postu
                },
                success: function(data) {
                    let contenido = JSON.parse(data);
                    if (contenido.tipo == 'error') {
                        alert(contenido.mensaje);
                        // alertify.error(contenido.mensaje, 10);
                    } else if (contenido.tipo == 'nuevo') {
                        alertify.success(contenido.mensaje);
                        setTimeout(function() {
                            window.location.href = contenido.liga;
                        }, 3000);
                    } else if (contenido.tipo == 'existente') {
                        window.location.href = contenido.liga;
                    } else {
                        alertify.error('A surguido un error, comuniquese con el area de soporte.', 10);
                    }
                },
                error: function(data) {
                    alertify.error('A surguido un error, comuniquese con el area de soporte.', 10);
                }
            });
        }, function() {
            alertify.warning('Operacion cancelada');
        });
    });
    $('.ver-chat').click(function() {
        let postu = $(this).attr('data-post');
        $.ajax({
            type: 'POST',
            url: '<?= base_url('/Proyectos/verchat') ?>',
            data: {
                postu: postu
            },
            success: function(data) {
                let contenido = JSON.parse(data);
                if (contenido.tipo != 'error') {
                    window.location.href = contenido.liga;
                } else {
                    // alertify.warning(contenido.mensaje);
                    alert(contenido.mensaje);
                }
            },
            error: function(data) {
                alertify.error('Ha surgido un error, comuniquese con el area de soporte.', 10);
            }
        });
    });
</script>