<div id="proyectos">
    <?php foreach($trabajos as $t){ ?>
        <?=view_cell('App\Libraries\FuncionesSistema::cardTrabajo', ['trabajo' => $t])?> 
    <?php } ?>
</div> 
<?php if($pages > 1){ ?>
    <div class="row">
        <div class="col-12 p-3">
            <nav aria-label="Page navigation">
                <ul class="pagination justify-content-end">
                    <li class="page-item disabled" id="sec-prev">
                        <a class="page-link" href="#" aria-label="Previous" id="prev" data-pag="0">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li class="pages">
                        <span id="indicador-page">1</span> de <?=$pages?>
                        <input type="hidden" id="page-input" value="1">
                        <input type="hidden" id="limit" value="<?=$pages?>">
                    </li>
                    <li class="page-item" id="sec-next">
                        <a class="page-link" href="#" aria-label="Next" id="next" data-pag="2">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <script> 
        // Paginacion de montacargas
        $('.page-link').on('click', function(){
            var tip = $(this).attr('id');
            var ind = $('#page-input').val();
            var limit = $('#limit').val();
            var pag = $(this).attr('data-pag');
            if(tip == 'next'){
                if(ind == 1){
                    $('#sec-prev').removeClass('disabled');
                }
                if(pag <= limit){
                    var prew = parseFloat(pag) - 1;
                    var next = parseFloat(pag) + 1;
                    $('#page-input').val(pag);
                    $('#indicador-page').html(pag);
                    
                    $('#next').attr('data-pag', next);
                    $('#prev').attr('data-pag', prew);
                    if($('#next').attr('data-pag') > limit){
                        $('#sec-next').addClass('disabled');
                    }
                    let estatus = $('#filtro-trabajos').val();
                    // $('#proyectos').html('<div class="text-center" id="icono-loading"><lottie-player src="https://mextemps.tresesenta.lat/assets/animaciones/loading.json" background="transparent" speed="1" style="width: 250px; height: 250px; margin: auto;" loop="" autoplay=""></lottie-player></div>');
                    // $('#proyectos').html('<tr><td></td><td></td><td><div class="loading" style="text-align: -webkit-center;"><img src="https://jundo.tresesenta.lat/assets/images/loading.gif" width="200px" height="200px"/><br/>Un momento por favor...</div></td><td></td><td></td></tr>');
                    var dataString = 'page='+pag+'&estatus='+estatus;
                    $.ajax({
                        type: "GET",
                        url: "<?=base_url('/proyectos/paginacionProyectos')?>",
                        data: dataString,
                        success: function(data) {
                            respuesta = JSON.parse(data);
                            $('#proyectos').fadeIn().html(respuesta.contenido);
                            $('#cantidad_proyectos').fadeIn().html(respuesta.total_proyectos);
                        }
                    });
                    return false;
                }
            }else if(tip == 'prev'){
                if(pag == 1){
                    $('#sec-prev').addClass('disabled');
                    
                    $('#page-input').val(1);
                    $('#indicador-page').html(1);
                    
                    $('#next').attr('data-pag', 2);
                    $('#prev').attr('data-pag', 1);
                    
                    $('#sec-next').removeClass('disabled');
                }else{
                    $('#sec-prev').removeClass('disabled');
                    
                    var prew = parseFloat(pag) - 1;
                    var next = parseFloat(pag) + 1;
                    
                    $('#page-input').val(pag);
                    $('#indicador-page').html(pag);
                    
                    $('#next').attr('data-pag', next);
                    $('#prev').attr('data-pag', prew);
                    $('#sec-next').removeClass('disabled');
                }
                let estatus = $('#filtro-trabajos').val();
                // $('#proyectos').html('<div class="text-center" id="icono-loading"><lottie-player src="https://mextemps.tresesenta.lat/assets/animaciones/loading.json" background="transparent" speed="1" style="width: 250px; height: 250px; margin: auto;" loop="" autoplay=""></lottie-player></div>');
                // $('#proyectos').html('<tr><td></td><td></td><td><div class="loading" style="text-align: -webkit-center;"><img src="https://jundo.tresesenta.lat/assets/images/loading.gif" width="200px" height="200px"/><br/>Un momento por favor...</div></td><td></td><td></td></tr>');
                var dataString = 'page='+pag+'&estatus='+estatus;
                $.ajax({
                    type: "GET",
                    url: "<?=base_url('/proyectos/paginacionProyectos')?>",
                    data: dataString,
                    success: function(data) {
                        respuesta = JSON.parse(data);
                        $('#proyectos').fadeIn().html(respuesta.contenido);
                        $('#cantidad_proyectos').fadeIn().html(respuesta.total_proyectos);
                    }
                });
                return false;
            } 
        });
        $('.crear-chat').click(function(){
            let postu = $(this).attr('data-post');
            alertify.confirm('Cuidado', 'Está a punto de generar un nuevo chat de negociación, ¿Está seguro de esto?', function(){
                $.ajax({
                    type: 'POST',
                    url: '<?=base_url('/Proyectos/verchat')?>',
                    data:{postu:postu},
                    success: function(data){
                        let contenido = JSON.parse(data);
                        if(contenido.tipo == 'error'){
                            alert(contenido.mensaje);
                            // alertify.error(contenido.mensaje, 10);
                        }else if(contenido.tipo == 'nuevo'){
                            alertify.success(contenido.mensaje);
                            setTimeout(function(){
                                window.location.href = contenido.liga;
                            }, 3000);
                        }else if(contenido.tipo == 'existente'){
                            window.location.href = contenido.liga;
                        }else{
                            alertify.error('Ha surguido un error, comuniquese con el area de soporte.', 10);
                        }
                    }, error: function(data){
                        alertify.error('Ha surguido un error, comuniquese con el area de soporte.', 10);
                    }
                });
            }, function(){
                alertify.warning('Operacion cancelada', 10);
            });
        });
        $('.ver-chat').click(function(){
            let postu = $(this).attr('data-post');
            $.ajax({
                type: 'POST',
                url: '<?=base_url('/Proyectos/verchat')?>',
                data: {postu:postu},
                success: function(data){
                    let contenido = JSON.parse(data);
                    if(contenido.tipo != 'error'){
                        window.location.href = contenido.liga;
                    }else{
                        // alertify.warning(contenido.mensaje);
                        alert(contenido.mensaje);
                    }
                }, error: function(data){
                    alertify.error('Ha surgido un error, comuniquese con el area de soporte.', 10);
                }
            });
        });
        $('#cantidad_proyectos').text(<?=count($trabajos)?>);
    </script>
<?php } ?>