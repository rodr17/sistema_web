<?= $this->extend('front/main') ?>
<?= $this->section('title') ?>
Chat
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<style>
    .info_proyecto {
        transition: all .8s;
        position: relative;
    }
    .info_proyecto:after{
        content: '';
        border: 1em solid #fe500096;
        border-radius: 50%;
        width: 17px;
        height: 17px;
        position: absolute;
        top: -22px;
        left: -9px;
        animation: animacion 2s linear infinite;
    }
    .info_proyecto:before{
        position: absolute;
        top: -14px;
    }
    @keyframes animacion{
        0%{
            transform: scale(0);
            opacity: 0;
        }
        50%{opacity: 1;}
        100%{
            transform: scale(1);
            opacity: 0;
        }
    }
    .barra{position: relative;}
    .barra::after{
        content: '';
        position: absolute;
        width: 9px;
        height: 9px;
        background: #ffffff;
        top: 0px;
        right: 0px;
        border-radius: 0px 0px 2px 2px;
    }
    .barra::before {
        content: '';
        position: absolute;
        width: 9px;
        height: 9px;
        background: #ffffff;
        top: 92%;
        right: 0px;
        border-radius: 2px 2px 0px 0px;
    }
	.inputfile {
		width: 0.1px;
		height: 0.1px;
		opacity: 0;
		overflow: hidden;
		position: absolute;
		z-index: -1;
	}

	.inputfile+label, .inputfile + .tooltips label {
		width: 100%;
		white-space: nowrap;
		cursor: pointer;
		display: inline-block;
		padding: 0.625rem 1.25rem;
		text-align: center;
	}

	.inputfile+label svg, .inputfile + .tooltips label svg {
		height: 1em;
		vertical-align: middle;
		fill: currentColor;
		margin-top: -0.25em;
		margin-right: 0.25em;
	}

	.inputfile-1+label, .inputfile-1 + .tooltips label {
		color: #fff;
		background-color: var(--color-azul);
	}

	.inputfile-1:text()focus+label,
	.inputfile-1.text()has-focus+label,
	.inputfile-1+text()label:hover {
		background-color: var(--color-azulHover);
	}

	.form-wrapper {
		border-radius: 7px;
	}

	.form-wrapper label {
		font-weight: bold;
	}

	.errors li {
		list-style: none;
		width: 100%;
		text-align: center;
	}

	.errors ul {
		padding-left: 0;
		margin-bottom: 0;
	}

	.message-holder {
		border: 1px solid #ccc;
		border-radius: 4px;
		overflow-x: hidden;
		overflow-y: auto;
		height: 400px;
		margin-bottom: 20px;
		padding-top: 10px;
		padding-bottom: 10px;
		padding-right: 25px;
		padding-left: 25px;
	}

	.msg-item {
		overflow: hidden;
		clear: both;
		margin-bottom: 12px;
		padding-top: 5px;
		border-radius: 4px;
		padding-bottom: 5px;
	}

	.msg-text {
		padding-left: 10px;
		padding-top: 5px;
		line-height: 19px;
		padding-right: 20px;
	}

	.msg-img {
		width: 60px;
		margin: 0 10px;
	}

	.msg-item .time {
		font-size: 10px;
	}

	.msg-item.left-msg {
		background: #003c71;
		color: #fff;
	}

	.msg-item.right-msg {
		background: #444;
		color: #fff;
	}

	.time {
		color: #fff;
		opacity: 0.8;
	}

	.left-msg .msg-img {
		float: left;
	}

	.right-msg {
		text-align: right;
	}

	.right-msg .msg-img {
		float: right;
	}
	.chat-mextemps .card.shadow {
		border-radius: 30px;
	}
	.chat-mextemps .card {
		border: 0px;
	}
	.chat-mextemps .card-header, .chat-mextemps .card-footer {
		border: 0px;
	}
	.chat-mextemps #form-archivos input {
	  height: 40px;
	  padding-left: 10px;
	  font-size: 12px;
	  padding-top: 5px;
	  margin-bottom: 10px;
	}
	.chat-mextemps .message-holder {
		/*box-shadow: inset 0.4em 0px 0px 0 rgb(254 80 0);*/
		border-radius: 30px;
		/*border: 1px solid rgb(254 80 0) !important;*/
	}

	.chat-mextemps .info-de-proyecto {
		border-radius: 30px;
		padding: 20px;
		border: 1px solid #ccc;
	}
	.agregar-tarea{
	    right: 3px;
        top: 3px;
	}
	@media (max-width: 768px) {
		.form-wrapper .text-right {
			text-align: center !important;
		}

		.form-wrapper .btn-primary {
			display: block;
			margin: 0 auto;
		}
	}
</style>
<!-- Freelancer -->
<main class="main sala-de-chat">
	<section class="container pb-5 chat-mextemps">
		<div class="row justify-content-center pb-5">
			<div class="col-lg-12">
				<h1 class="h3 text-center">Sala de chat del proyecto</h1>
				<div class="card shadow p-30 mtb-20">
					<div class="row">
						<div class="col-lg-8 col-sm-12 col-md-12 sin_padding_suscripcion ventana-de-proyecto mb-2">
							<div class="card barra">
								<div class="message-holder">
									<div id="contenedor-mensajes" class="row"></div>
								</div>
							</div>
							<form id="enviar_mensaje" class="input-group mt-0">
                                <?php if ($proyecto['estatus'] != 'finalizado') { ?>
                                    <input class="form-control text-font" placeholder="Mensaje" id="mensaje">
                                    <button class="btn btn-outline-danger rounded-end text-font" type="submit"><i class="fas fa-paper-plane"></i></button>
                                <?php } ?>
								<audio id="audio" src="<?= base_url('assets/nota_iphone.mp3') ?>"></audio>
							</form>
						</div>
						<div class="col-lg-4 col-sm-12 col-md-12 info-de-proyecto">
							<h2 class="h6">Proyecto <span class="text-naranja"><?= $proyecto['titulo'] ?></span></h2>
							<ul class="list-group">
								<li class="list-group-item border-0 px-0">
                                    <div class="position-relative">
                                        <img alt="mextemps" src="<?= imagenPerfil($usuario) ?>" class="align-middle img-perfil-icon"/>
                                        <span class="sidebar-company"><?=$usuario['nombre'].' '.$usuario['apellidos']?></span>
                                    </div>
                                </li>
								<li class="list-group-item border-0 px-0">
                                    <div class="d-grid gap-2">
                                        <a class="btn btn-naranja text-font" target="_blank" href="<?= base_url('trabajo/' . $proyecto['id']) ?>">
                                            Ver proyecto <i class="fas fa-share-square"></i>
                                        </a>
                                    </div>
                                    <?php if($proyecto['estatus'] == 'desarrollo'){ ?>
                                        <div class="d-grid gap-2 mt-2 card-job-bottom" id="sec-boton-fin">
                                            <?php $finsol = model('Finalizar')->where('id_proyecto', $proyecto['id'])->first(); ?>
                                            <?php if($finsol == null){ ?>
                                                <a class="btn btn-primary" onclick="soliFinalizarProyecto(<?=$proyecto['id']?>)" data-id="<?=$proyecto['id']?>">
                                                    Solicitar finalizar proyecto
                                                </a>
                                            <?php }else{ ?>
                                                <?php if($finsol['estatus_contrante'] == 'true'){ ?>
                                                    <div class="alert alert-info btn-sm text-center" role="alert">
                                                        El proyecto ya ha finalizado.
                                                    </div>
                                                <?php }elseif($finsol['estatus_contrante'] == null && $finsol['estatus_freelance'] == 'true'){ ?>
                                                    <div class="alert alert-info btn-sm text-center" role="alert">
                                                        Se le ha notificado al contratante que ha solicitado finalizar el proyecto.
                                                    </div>
                                                <?php }elseif($finsol['estatus_contrante'] == 'false' && $finsol['estatus_freelance'] == 'false'){ ?>
                                                    <div class="alert alert-info btn-sm text-center" role="alert">
                                                        El contratante nos ha notificado que aun no se han completado el proyecto, verifique cada una de las tareas.
                                                    </div>
                                                    <a class="btn btn-primary" onclick="soliFinalizarProyecto(<?=$proyecto['id']?>)" data-id="<?=$proyecto['id']?>">
                                                        Solicitar finalizar proyecto
                                                    </a>
                                                    <script>
                                                        $(document).ready(function(){
                                                            setTimeout(function(){
                                                                resetSoli(<?=$proyecto['id']?>);
                                                            }, 5000);
                                                        });
                                                    </script>
                                                <?php }elseif($finsol['estatus_contrante'] == null && $finsol['estatus_freelance'] == null){ ?>
                                                    <a class="btn btn-primary" onclick="soliFinalizarProyecto(<?=$proyecto['id']?>)" data-id="<?=$proyecto['id']?>">
                                                        Solicitar finalizar proyecto
                                                    </a>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <input type="hidden" value="<?= ($finsol != null)? $finsol['estatus_freelance'] : null ?>" id="est-free">
                                        <input type="hidden" value="<?= ($finsol != null)? $finsol['estatus_contrante'] : null ?>" id="est-cont">
                                        <script>
                                            setInterval(function(){
                                                verificarEstSoli(<?=$proyecto['id']?>);
                                            }, 3500);
                                        </script>
                                    <?php } ?>
								</li>
								<li class="list-group-item border-0 px-0">
									<label class="text-font">Estatus: <span class="text-naranja"><?= ($proyecto['deleted']? 'Suspendido' : (($proyecto['estatus'] == 'desarrollo')? 'En desarrollo' : $proyecto['estatus'])) ?></span></label>
								</li>
							</ul>
							<hr>
							<label class="h6 fw-bold">Tareas a realizar</label>
							<form class="row mb-1" id="tareas">
								<div class="col-12 align-self-center position-relative">
									<div class="form-floating mb-3">
										<input type="text" class="form-control" <?= $proyecto['deleted'] ? 'disabled' : ''?> id="tarea_input" name="tarea" placeholder="...">
										<label for="tarea">Agregar una nueva tarea</label>
										<?php if(!$proyecto['deleted']) :?>
											<button class="btn position-absolute agregar-tarea rounded-circle p-0" title="Agregar tarea" type="submit">
												<i class="fas fa-pencil-alt text-naranja border border-2 rounded-circle p-1"></i>
											</button>
										<?php else :?>
											<div clasS="position-absolute top-0" style="right: 30px">
												<div class="tooltips"><i style="color: #003B71; font-size:17px;" class="fas fa-info-circle info_proyecto" aria-hidden="true"></i>
													<span class="tooltiptext d-none d-md-block">Proyecto temporalmente suspendido.</span>
												</div>
											</div>
										<?php endif ;?>
									</div>
									<input type="hidden" id="id-proyecto" value="<?=$proyecto['id']?>" name="id">
								</div>
							</form>
							<label class="h6 fw-bold">Tareas a realizar</label>
							<div class="row overflow-auto mb-3" id="sec-tareas" style="max-height: 220px">
								<?php if(!empty($proyecto['tareas'])) : ?>
									<?=view_cell('App\Libraries\FuncionesSistema::tareasProyecto', ['id' => $proyecto['id']])?>
								<?php else : ?>
									<label class="text-font mb-0">Indica las tareas ha realizar para informar al contratante.</label>
								<?php endif ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<script>
    function resetSoli(id){
        $.ajax({
            type: 'POST',
            url: '<?= base_url('/Proyectos/actuaSoliFin') ?>',
            data:{id:id},
            success: function(data){
                console.log(data);
            }, error: function(data){
                console.log('Error al actualizar el estatus de la solicitud');
            }
        });
    }
    function verificarEstSoli(id){
        let fre = $('#est-free').val();
        let contra = $('#est-cont').val();
        $.ajax({
            type: 'POST',
            url: '<?= base_url('/Proyectos/VerificarEstadoSoliFin') ?>',
            data:{id:id, fre:fre, contra:contra},
            success:function(data){
                let cont = JSON.parse(data);
                if(cont.tipo != 'error'){
                    $('#sec-boton-fin').html(cont.html);
                    if(cont.con == 'true' && cont.free == 'true'){
                        $('#enviar_mensaje').addClass('d-none');
                    }else{
                        $('#enviar_mensaje').removeClass('d-none');
                    }
                }
            }, error: function(data){
                console.log('Error en la conexion con el servidor.');
            }
        });
    }
    function soliFinalizarProyecto(id){
        $.ajax({
            type: 'POST',
            url: '<?= base_url('/Proyectos/soliFinalizarProyecto') ?>',
            data:{id:id},
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, 'advertencia');
                $('#sec-boton-fin').html('<div class="alert alert-info btn-sm text-center" role="alert">Se le ha notificado al contratante que ha solicitado finalizar el proyecto.</div>');
                return alertify.notify(cont.mensaje, 'advertencia');
            }, error: function(data){
                alertify.notify('Intentelo más tarde, nuestros servidores se encuentran en mantenimiento.', 'advertencia');
            }
        });
    }
</script>
<script type="module">
	'use strict';
	// Import the functions you need from the SDKs you need
	import {
		initializeApp
	} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
	import {
		getAnalytics
	} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-analytics.js";
	import {
		getFirestore
	} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";
	// TODO: Add SDKs for Firebase products that you want to use
	// https://firebase.google.com/docs/web/setup#available-libraries

	// Your web app's Firebase configuration
	// For Firebase JS SDK v7.20.0 and later, measurementId is optional
	const firebaseConfig = {
        apiKey: "AIzaSyCmtOCzTL7RjCi2ZkoJExiBmrGTxgnxN6Q",
        authDomain: "mextemps-341000.firebaseapp.com",
        projectId: "mextemps-341000",
        storageBucket: "mextemps-341000.appspot.com",
        messagingSenderId: "198033966029",
        appId: "1:198033966029:web:12659433a00b1e05e1fa84",
        measurementId: "G-SZY88RED1D"
     };

	// Initialize Firebase
	const app = initializeApp(firebaseConfig);
	const analytics = getAnalytics(app);
	const db = getFirestore(app);
	import {
		collection,
		addDoc,
		getDocs,
		query,
		onSnapshot,
		orderBy,
		serverTimestamp
	} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";
    
	$(document).ready(function() {
		let vid = document.getElementById("audio");
		vid.volume = 0.4;
		$(function() { scrollMsgBottom(); })

		function scrollMsgBottom() {
			$('.message-holder').scrollTop($('.message-holder').prop("scrollHeight"));
		}
		
		
		$(function() {
			const q = query(collection(db, "chats/<?= $id_sala ?>/messages"), orderBy("timestamp"));
			const unsubscribe = onSnapshot(q, (querySnapshot) => {
				//const cities = [];
				$('#contenedor-mensajes').empty();
				// console.log(doc.data());
				querySnapshot.forEach((doc) => {
					newMessage(doc.data());
				});
				actuaCantMessages($('.msg-item').length, <?= $id_sala ?>);
			});
			$('#enviar_mensaje').on('submit', async function(evento) {
				evento.preventDefault();
				let msg = $('#mensaje').val();
				msg = msg.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
				$('#mensaje').val('');
				if (msg.trim() == '') return false;

				<?php $nombre = explode(' ', session('usuario')) ?>
				let fecha = new Date;
				fecha.toLocaleDateString("es-MX", {
					month: 'long',
					day: 'numeric',
					hour: 'numeric',
					minutes: '2-digit'
				})
				let minutes = fecha.getMinutes();
				let hour = fecha.getHours();
				let time = hour + ':' + minutes;
				try {
					const docRef = await addDoc(collection(db, "chats/<?= $id_sala ?>/messages"), {
						author: "<?= $nombre[0] ?>",
						authorId: "<?= session('id') ?>",
						imagen: "<?= imagenPerfil(session('imagen')) ?>",
						tiempo: time,
						message: msg,
						timestamp: serverTimestamp()
					});
					console.log("Document written with ID: ", docRef.id);
				} catch (e) {
					console.error("Error adding document: ", e);
				}
				$('#mensaje').val('');
			})
		})

		function newMessage(msg) {
			let typeClass = "left-msg";
			if (msg.authorId == "<?= session('id') ?>") typeClass = "right-msg offset-sm-4 offset-2";
			if (msg.authorId != "<?= session('id') ?>") document.getElementById('audio').play();
			let html = `<div class="col-10 col-sm-8 msg-item ` + typeClass + `">
					<div class="msg-img">
						<img class="img-thumbnail rounded-circle" src="` + msg.imagen + `">
					</div>
					<div class="msg-text">
						<span>` + msg.author + `</span> <span class="time">` + msg.tiempo + `</span><br>
						<p class="text-sm m-0">` + msg.message + `</p>
					</div>
					</div>`;
			$('#contenedor-mensajes').append(html);
			scrollMsgBottom();
			document.getElementById('audio').play();
		}
		function actuaCantMessages(cantidad, sala){
            console.log('Cantidad: '+cantidad);
            console.log('Sala: '+sala);
            $.ajax({
                type: 'POST',
                url: '<?= base_url('/Chats/saveCantidadMensajes') ?>',
                data:{cantidad:cantidad, sala:sala},
                success: function(data){
                    console.log('Mensaajes actualizados');
                }, error: function(data){
                    alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.')
                }
            });
		}
	});
	<?php if(!$proyecto['deleted']) :?>
    	$('#tareas').submit(function(e){
			e.preventDefault();
            console.log($('#tareas').serialize());
            $.ajax({
                type: 'POST',
                url: '<?=base_url('/Proyectos/crearTarea')?>',
                data:$('#tareas').serialize(),
                success: function(data){
                    let cont = JSON.parse(data);
                    if(cont.tipo != 'success') return alertify.notify(cont.mensaje, 'advertencia', 10);
					
					$('#tarea_input').val('');
					$('#sec-tareas').html(cont.contenido);
					// progreso();
                }, error: function(data){
                    alertify.npotify('A surgido un error, comuniquese con el area de soporte.', 'falla', 10);
                }
            });
        });
    <?php endif ;?>
</script>
<?= $this->endSection() ?>