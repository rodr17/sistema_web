<?php use App\Libraries\NetPay; helper('funciones'); ?>
<?= $this->extend('front/main') ?>

<?=$this->section('estilos')?>
	<link href="https://code.jquery.com/ui/1.12.1/themes/ui-darkness/jquery-ui.css" rel="stylesheet"/>
	<style>
		@media only screen and (max-width: 992px) {
			.heading-main-info {
				display: block;
				padding-left: 0;
				text-align: center;
			}
			#sec-sup-perfil{
				display: grid;
				place-items: center;
			}
		}
	</style>
<?=$this->endSection()?>

<?= $this->section('title') ?>Perfil<?= $this->endSection() ?>

<?= $this->section('content') ?>
<!-- SCRIPT EXTRAS PARA CONTENIDO -->
<script src="<?=base_url('assets/js/croppie/croppie.min.js')?>"></script>
<link href="<?=base_url('assets/css/croppie/croppie.min.css')?>" rel="stylesheet" type="text/css">
<script src="https://unpkg.com/qrious@4.0.2/dist/qrious.js"></script>
<script src="https://code.iconify.design/3/3.0.0/iconify.min.js"></script>
<!-- SCRIPT EXTRAS PARA CONTENIDO -->
<main class="main perfil-freelancer-page">
	<!-- HEADER PERFIL -->
	<section class="section-box">
		<div class="box-head-single box-head-single-candidate">
			<div class="container" id="sec-sup-perfil">
				<?=view_cell('App\Libraries\FuncionesSistema::completarPerfil')?>
				<?=view_cell('App\Libraries\FuncionesSistema::diasPreviosFinalizarSuscripcion', ['id' => session('id')])?>
				<div class="heading-image-rd online">
					<form>
						<label class="cabinet center-block">
							<figure class="m-0 user-select-none">
								<img src="<?= imagenPerfil($freelancer) ?>" class="gambar img-responsive img-thumbnail img-perfil shadow border border-3 m-auto" id="imagen-perfil">
							</figure>
							<input type="file" class="item-img file center-block" name="imagenPerfil" id="file_imagen_perfil" hidden>
							<input class="inputs-form text-font2" id="img_recortada" name="img_recortada" hidden value="<?= imagenPerfil($freelancer['imagen']) ?>">
						</label>
					</form>
				</div>
				<div class="heading-main-info ">
					<div class="d-flex d-md-inline-flex d-lg-flex">
						<h1 class="h3 fw-bold py-1">
							<a class="text-dark" href="<?= route_to('prevista', base64_encode(session('id'))) ?>" target="_blank" id="nombre_freelancer"><?=$freelancer['nombre']?> <?=$freelancer['apellidos']?></a>
						</h1>
						<div class="d-inline-block perfil-usuarios-freelancer">
							<span class="insignias-lsitado">
								<?= view_cell('App\Libraries\FuncionesSistema::miNivelInsignia', ['cantidad' => view_cell('App\Libraries\FuncionesSistema::cantProyectosFinalizados', ['id' => $freelancer['id'], 'rol' => $freelancer['rol']]), 'tipo' => 'icono']) ?>
							</span>
						</div>
					</div>
					<div class="col-12 d-block d-lg-none mas-info-contra">
						<?php if($freelancer['cuenta'] == 'predeterminado') :?>
							<span class="d-block text-naranja opciones-ingresar actualizar-contrasena" data-bs-toggle="modal" data-bs-target="#actualizar_contraseña_modal">Actualizar contraseña</span>
						<?php endif ;?>
					</div>
					<div class="head-info-profile mb-0 mb-md-2">
						<span class="text-small mr-20"><i class="fi-rr-marker text-mutted"></i> <?= isset($freelancer['direccion']['estado'])? $freelancer['direccion']['estado'].', ' : ''?><?= isset($freelancer['direccion']['pais'])? $freelancer['direccion']['pais'] : ''?></span>
						<span class="text-small mr-20"><i class="fi-rr-briefcase text-mutted"></i> <?=view_cell('App\Libraries\FuncionesSistema::obtenerHabilidad', ['id' => $freelancer['id']])?></span>
						<span class="text-small"><i class="fi-rr-clock text-mutted"></i> Usuario desde <?= strftime('%B, %Y', strtotime($freelancer['created_at'])) ?></span>
						<?=view_cell('App\Libraries\FuncionesSistema::verCalificacionPerfil', ['id' => session('id')])?>
						<?php $contactos = array_filter($freelancer['contacto'], function($elemento) {return $elemento != '';}); unset($contactos['telefono'])?>
						<?php if(!empty($contactos)) :?>
							<span class="mt-2 mt-xxl-0" id="contenedor_redes_sociales">
								<?php if(isset($freelancer['contacto']['facebook']) && !empty($freelancer['contacto']['facebook'])) :?>
									<span style="padding-left: 10px;" class="lh-sm social-info"><a class="text-azul" href="https://www.facebook.com/<?=$freelancer['contacto']['facebook']?>" target="_blank"><i class="fab fa-facebook-f" aria-hidden="true" style="float:none;"></i> <?= $freelancer['contacto']['facebook']?></a></span>
								<?php endif ;?>
								<?php if(isset($freelancer['contacto']['instagram']) && !empty($freelancer['contacto']['instagram'])) :?>
									<span style="padding-left: 10px;" class="lh-1 social-info"><a style="color: #df0041;" href="https://www.instagram.com/<?=$freelancer['contacto']['instagram']?>" target="_blank"><i class="fab fa-instagram" aria-hidden="true" style="float:none;"></i> <?= $freelancer['contacto']['instagram']?></a></span>
								<?php endif ;?>
								<?php if(isset($freelancer['contacto']['whatsapp']) && !empty($freelancer['contacto']['whatsapp'])) :?>
									<span style="padding-left: 10px;" class="text-small social-info"><a class="text-success" href="https://wa.me/<?= $freelancer['contacto']['whatsapp']?>" target="_blank"><i class="fab fa-whatsapp" aria-hidden="true" style="float:none;"></i> <?= $freelancer['contacto']['whatsapp']?></a></span>
								<?php endif ;?>
								<?php if(isset($freelancer['contacto']['correo']) && !empty($freelancer['contacto']['correo'])) :?>
									<span style="padding-left: 10px;" class="text-small social-info"><a class="text-primary" href="mailto:<?= $freelancer['contacto']['correo']?>" target="_blank"><i class="fas fa-at" aria-hidden="true" style="float:none;"></i> <?= $freelancer['contacto']['correo']?></a></span>
								<?php endif ;?>
							</span>
						<?php endif ;?>
					</div>
					<div class="col-12 d-block d-md-none mas-info-contra">
						<span class="d-block sidebar-website-text"><?=$freelancer['correo']?></span>
					</div>
					<div class="row align-items-end">
						<div class="col-lg-12">
							<a class="btn btn-tags-sm mb-10 btn-listados mr-5" style="padding: 10px 18px; background: rgba(81, 146, 255, 0.12) !important; border-radius: 50px; font-size: 14px; line-height: 12px; color: #727272;" onclick="openCity(event, 'Proyectos'); $(this).click(function(e){ $('.btn-listados').removeClass('inactivo'); $('#tab_proyectos').addClass('active'); $('span .btn-listados')[1].classList.add('inactivo'); $('span .btn-listados')[2].classList.add('inactivo');})" data-tipo="espera"><?=view_cell('App\Libraries\FuncionesSistema::cantPostulaciones', ['id' => $freelancer['id'], 'rol' => session('rol')])?> Proyectos Postulados</a>
							<a class="btn btn-tags-sm mb-10 btn-listados mr-5" style="padding: 10px 18px; background: rgba(81, 146, 255, 0.12) !important; border-radius: 50px; font-size: 14px; line-height: 12px; color: #727272;" onclick="openCity(event, 'Proyectos'); $(this).click(function(e){ $('.btn-listados').removeClass('inactivo'); $('#tab_proyectos').addClass('active'); $('span .btn-listados')[2].classList.add('inactivo'); $('span .btn-listados')[0].classList.add('inactivo');})" data-tipo="desarrollo"><?=view_cell('App\Libraries\FuncionesSistema::cantProyectosDesarrollo', ['id' => $freelancer['id'], 'rol' => session('rol')])?> Proyectos en Progreso</a>
							<a class="btn btn-tags-sm mb-10 btn-listados mr-5" style="padding: 10px 18px; background: rgba(81, 146, 255, 0.12) !important; border-radius: 50px; font-size: 14px; line-height: 12px; color: #727272;" onclick="openCity(event, 'Proyectos'); $(this).click(function(e){ $('.btn-listados').removeClass('inactivo'); $('#tab_proyectos').addClass('active'); $('span .btn-listados')[0].classList.add('inactivo'); $('span .btn-listados')[1].classList.add('inactivo');})" data-tipo="finalizado"><?=view_cell('App\Libraries\FuncionesSistema::cantProyectosFinalizados', ['id' => $freelancer['id'], 'rol' => session('rol')])?> Proyectos Terminados</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section-box mt-50">
		<div class="container">
			<div class="row">
				<form class="needs-validation d-lg-none" enctype="multipart/form-data" id="form-pagoHora_movil" novalidate>
					<div class="sidebar-list-job my-2">
						<h6 class="small-heading mb-2">Costo por hora de tu trabajo</h6>
						<div class="d-flex justify-content-around gap-1">
							<div class="input-group">
								<div class="input-group-prepend">
									<div class="input-group-text" style="border-radius: .25rem 0 0 .25rem; height: 50px;">$</div>
								</div>
								<input name="pagoHora" id="pagoHora_movil" type="number" value="<?= $freelancer['pagoHora'] ?>" required class="form-control numerico" min="1" maxlength="9" placeholder="Costo por hora" aria-label="monto" aria-describedby="basic-addon1">
								<div class="invalid-tooltip text-font2">Requerido</div>
							</div>
							<button id="btn-pagoHora_movil" type="submit" class="btn btn-primary">Actualizar costo</button>
						</div>
					</div>
				</form>
				<div class="col-12 d-block d-lg-none mas-info-contra mt-1">
					<div class="d-flex justify-content-center gap-2">
						<?php if(!empty($freelancer['deleted_at'])) :?>
							<button class="btn btn-naranja btn-border" data-bs-toggle="modal" data-bs-target="#recuperar_cuenta_modal">Recuperar cuenta</button>
						<?php else :?>
							<button class="btn btn-naranja btn-border" data-bs-toggle="modal" data-bs-target="#eliminar_cuenta_modal">Eliminar cuenta</button>
						<?php endif ;?>
						<a class="btn btn-primary" data-estatus="<?= (empty($freelancer['ocultar']) || !empty($freelancer['ocultar']) == false)? 'ocultar' : 'mostrar' ?>" id="ocultar-cuenta">
							<?= (empty($freelancer['ocultar']) || !empty($freelancer['ocultar']) == false)? 'Ocultar cuenta' : 'Mostrar cuenta' ?>
						</a>
					</div>
					<div class="sidebar-list-job pt-20 misuscripcion-columna movil-vista-qr ">
						<h6 class="small-heading mb-20">Compartir mi perfil</h6>
						<div class="text-center row">
							<div class="col-12 col-md-6" id="contenedor-qr-movil">
							</div>
							<div class="col-12 col-md-6 text-md-start align-self-center">
								<a href="<?= route_to('prevista', base64_encode(session('id'))) ?>" class="btn btn-naranja btn-border mb-3" target="_blank">Vista de perfil</a>
								<div class="row align-items-center flex-clomun gap-2 mb-3">
									<label class="fw-bold me-auto mb-0" style="display: block;">Compartir en:</label>    
									<div class="d-flex justify-content-center gap-2">
										<a class="rounded-circle iconos-compartir" href="https://www.facebook.com/sharer/sharer.php?u=<?=base_url('Usuarios/prevista/'.base64_encode(session('id')))?>" target="_blank">
											<span class="iconify" data-icon="akar-icons:facebook-fill" style="color: #0b5ed7; padding: 0px; border: 0px;" data-width="25" data-height="25"></span>
										</a>
										<a class="rounded-circle mx-2 iconos-compartir" href="https://wa.me/send?text=<?=base_url('Usuarios/prevista/'.base64_encode(session('id')))?>" target="_blank">
											<span class="iconify" data-icon="akar-icons:whatsapp-fill" style="color: #0b5ed7; padding: 0px; border: 0px;" data-width="25" data-height="25"></span>
										</a>
										<a class="rounded-circle iconos-compartir" href="https://www.linkedin.com/sharing/share-offsite/?url=<?=base_url('Usuarios/prevista/'.base64_encode(session('id')))?>" target="_blank">
											<span class="iconify" data-icon="akar-icons:linkedin-box-fill" style="color: #0b5ed7; padding: 0px; border: 0px;" data-width="25" data-height="25"></span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-8 col-md-12 col-sm-12 col-12">
					<div class="content-single">
						<div class="tabs-generales">
							<div class="tab d-sm-flex">
								<button class="tablinks" onclick="openCity(event, 'MiPerfil')" id="defaultOpen"><i class="far fa-user" aria-hidden="true"></i> Editar Perfil</button>
								<button class="tablinks" id="tab_proyectos" onclick="openCity(event, 'Proyectos')"><i class="far fa-folder-open" aria-hidden="true"></i> Mis Proyectos</button>
								<button class="tablinks" onclick="openCity(event, 'MetodosPago')"><i class="far fa-credit-card" aria-hidden="true"></i> Mis Tarjetas</button>
								<button class="tablinks" onclick="openCity(event, 'HistorialPagos')"><i class="far fa-file-alt" aria-hidden="true"></i> Mis Pagos</button>
								<button class="tablinks" onclick="openCity(event, 'MiSuscripcion')"><i class="far fa-address-card" aria-hidden="true"></i> Mi Suscripción</button>
							</div>
							<div id="MiPerfil" class="tabcontent">
								<form class="contact-form-style mt-10 form-perfil needs-validation" id="form-editarPerfil" enctype="multipart/form-data" novalidate style="padding: 25px;">
									<div class="row wow animate__ animate__fadeInUp animated" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
										<div class="sidebar-shadow-md detalles-dir-uno">
											<div class="row">
												<h4>Datos Personales</h4>
												<div class="col-lg-4 col-md-4">
													<input class="text-font" id="nombre" name="nombre" placeholder="Nombre" value="<?= $freelancer['nombre'] ?>">
												</div>
												<div class="col-lg-4 col-md-4">
													<input class="text-font" id="apellidos" name="apellidos" placeholder="Apellidos" value="<?= $freelancer['apellidos'] ?>">
												</div>
												<div class="col-lg-4 col-md-4">
													<div class="position-relative">
														<label for="nacimiento" class="position-absolute bg-white rounded-pill m-0 px-1" style="top: -12px; right: 6px;">Fecha de nacimiento</label>
														<input class="inputs-form text-font" type="date" id="nacimiento" name="nacimiento" max="<?=date('Y-m-d', strtotime(date('Y-m-d')."- 18 year"))?>" placeholder="Fecha de nacimiento" value="<?= $freelancer['nacimiento'] ?>">
													</div>
												</div>
												<div class="col-lg-4 col-md-4">
													<input class="text-font" id="telefono" name="telefono" placeholder="Teléfono" value="<?= isset($freelancer['contacto']['telefono']) ? $freelancer['contacto']['telefono'] : '' ?>">
												</div>
											</div>
										</div>
										<div class="sidebar-shadow-md detalles-dir-dos">
											<div class="row">
												<h4 class="mt-3">Dirección Fiscal</h4>
												<div class="col-lg-4 col-md-4">
													<div class="input-style mb-20">
														<input class="text-font" id="calle" name="calle" placeholder="Calle y Número" value="<?= isset($freelancer['direccion']['calle']) ? $freelancer['direccion']['calle'] : '' ?>" required>
														<!--<input class="text-font" id="ciudad" name="ciudad" placeholder="Ciudad" value="<= isset($freelancer['direccion']['ciudad']) ? $freelancer['direccion']['ciudad'] : '' ?>" required>-->
														<input class="text-font" id="codigo_postal" name="codigo_postal" placeholder="Código Postal" value="<?= isset($freelancer['direccion']['codigo_postal']) ? $freelancer['direccion']['codigo_postal'] : '' ?>" required>
														<input class="inputs-form text-font px-3" id="curp" name="curp" placeholder="CURP" oninput="validarCurp(this)" value="<?= isset($freelancer['direccion']['curp']) ? $freelancer['direccion']['curp'] : '' ?>" required>
													</div>
												</div>
												<div class="col-lg-4 col-md-4">
													<div class="input-style mb-20">
														<input class="text-font" id="colonia" name="colonia" placeholder="Colonia" value="<?= isset($freelancer['direccion']['colonia']) ? $freelancer['direccion']['colonia'] : '' ?>" required>
														<input class="text-font" id="estado" name="estado" placeholder="Estado" value="<?= isset($freelancer['direccion']['estado']) ? $freelancer['direccion']['estado'] : '' ?>" required>
														<input class="text-font" id="rfc" name="rfc" placeholder="RFC" value="<?= isset($freelancer['direccion']['rfc']) ? $freelancer['direccion']['rfc'] : '' ?>">
													</div>
												</div>
												<div class="col-lg-4 col-md-4">
													<div class="input-style mb-20">
														<input class="text-font" id="municipio" name="municipio" placeholder="Municipio o Localidad" value="<?= isset($freelancer['direccion']['municipio']) ? $freelancer['direccion']['municipio'] : '' ?>" required>
														<input class="text-font" id="pais" name="pais" placeholder="País" value="<?= isset($freelancer['direccion']['pais']) ? $freelancer['direccion']['pais'] : '' ?>" readonly="" required>
														
													</div>
												</div>
											</div>
										</div>
										<div class="sidebar-shadow-md detalles-dir-tres">
											<div class="row">
												<h4>Documentos</h4>
												<div class="col-lg-6 col-md-6 text-center">
													<label style="text-align: left; display: block; font-size: 16px; font-weight: bold;">Adjunta tu CV</label>
													<input type="file" id="curriculum" name="curriculum" class="inputfile inputfile-1 d-none" data-multiple-caption="{count} archivos seleccionados">
													<label for="curriculum">
														<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" width="20" height="20" x="0" y="0" viewBox="0 0 477.873 477.873" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g>
																<g>
																	<g>
																		<path d="M392.533,238.937c-9.426,0-17.067,7.641-17.067,17.067V426.67c0,9.426-7.641,17.067-17.067,17.067H51.2
																			c-9.426,0-17.067-7.641-17.067-17.067V85.337c0-9.426,7.641-17.067,17.067-17.067H256c9.426,0,17.067-7.641,17.067-17.067
																			S265.426,34.137,256,34.137H51.2C22.923,34.137,0,57.06,0,85.337V426.67c0,28.277,22.923,51.2,51.2,51.2h307.2
																			c28.277,0,51.2-22.923,51.2-51.2V256.003C409.6,246.578,401.959,238.937,392.533,238.937z" fill="#FFF" data-original="#FFF" class=""></path>
																	</g>
																</g>
																<g>
																	<g>
																		<path d="M458.742,19.142c-12.254-12.256-28.875-19.14-46.206-19.138c-17.341-0.05-33.979,6.846-46.199,19.149L141.534,243.937
																			c-1.865,1.879-3.272,4.163-4.113,6.673l-34.133,102.4c-2.979,8.943,1.856,18.607,10.799,21.585
																			c1.735,0.578,3.552,0.873,5.38,0.875c1.832-0.003,3.653-0.297,5.393-0.87l102.4-34.133c2.515-0.84,4.8-2.254,6.673-4.13
																			l224.802-224.802C484.25,86.023,484.253,44.657,458.742,19.142z M434.603,87.419L212.736,309.286l-66.287,22.135l22.067-66.202
																			L390.468,43.353c12.202-12.178,31.967-12.158,44.145,0.044c5.817,5.829,9.095,13.72,9.12,21.955
																			C443.754,73.631,440.467,81.575,434.603,87.419z" fill="#FFF" data-original="#FFF" class=""></path>
																	</g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																</g></svg>
														<!--<svg xmlns="http://www.w3.org/2000/svg" class="fw-normal" width="20" height="17" viewBox="0 0 20 17">-->
														<!--    <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>-->
														<!--</svg>-->
														<span>Seleccionar archivo</span>
													</label>
													<!--<input style="padding: 10px;" type="file" class="form-control-file text-font d-none d-sm-block" id="curriculum" name="curriculum">-->
													<div id="sec-cv">
													</div>
												</div>
												<?php if($añadir_portafolio) :?>
													<div class="col-lg-6 col-md-6 text-center">
														<label style="text-align: left; display: block; font-size: 16px; font-weight: bold;">Adjunta tu Portafolio</label>
														<input type="file" id="portafolio" name="portafolio" class="inputfile inputfile-2 d-none" data-multiple-caption="{count} archivos seleccionados">
														<label for="portafolio">
															<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" width="20" height="20" x="0" y="0" viewBox="0 0 477.873 477.873" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g>
																<g>
																	<g>
																		<path d="M392.533,238.937c-9.426,0-17.067,7.641-17.067,17.067V426.67c0,9.426-7.641,17.067-17.067,17.067H51.2
																			c-9.426,0-17.067-7.641-17.067-17.067V85.337c0-9.426,7.641-17.067,17.067-17.067H256c9.426,0,17.067-7.641,17.067-17.067
																			S265.426,34.137,256,34.137H51.2C22.923,34.137,0,57.06,0,85.337V426.67c0,28.277,22.923,51.2,51.2,51.2h307.2
																			c28.277,0,51.2-22.923,51.2-51.2V256.003C409.6,246.578,401.959,238.937,392.533,238.937z" fill="#FFF" data-original="#FFF" class=""></path>
																	</g>
																</g>
																<g>
																	<g>
																		<path d="M458.742,19.142c-12.254-12.256-28.875-19.14-46.206-19.138c-17.341-0.05-33.979,6.846-46.199,19.149L141.534,243.937
																			c-1.865,1.879-3.272,4.163-4.113,6.673l-34.133,102.4c-2.979,8.943,1.856,18.607,10.799,21.585
																			c1.735,0.578,3.552,0.873,5.38,0.875c1.832-0.003,3.653-0.297,5.393-0.87l102.4-34.133c2.515-0.84,4.8-2.254,6.673-4.13
																			l224.802-224.802C484.25,86.023,484.253,44.657,458.742,19.142z M434.603,87.419L212.736,309.286l-66.287,22.135l22.067-66.202
																			L390.468,43.353c12.202-12.178,31.967-12.158,44.145,0.044c5.817,5.829,9.095,13.72,9.12,21.955
																			C443.754,73.631,440.467,81.575,434.603,87.419z" fill="#FFF" data-original="#FFF" class=""></path>
																	</g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																</g></svg>
															<!--<svg xmlns="http://www.w3.org/2000/svg" class="fw-normal" width="20" height="17" viewBox="0 0 20 17">-->
															<!--    <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>-->
															<!--</svg>-->
															<span>Seleccionar archivo</span>
														</label>
														<!--<input style="padding: 10px;" type="file" class="form-control-file text-font" id="portafolio" name="portafolio">-->
														<div id="sec-portafolio"></div>
													</div>
												<?php else :?>
													<div class="col-lg-6 col-md-6 text-center">
														<div class="tooltips w-100">
															<label style="text-align: left; display: block; font-size: 16px; font-weight: bold;">Adjunta tu Portafolio</label>
															<input type="file" id="portafolio" name="portafolio" class="inputfile inputfile-2 d-none disabled" disabled readonly data-multiple-caption="{count} archivos seleccionados">
															<label for="portafolio" class="opacity-75">
																<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" width="20" height="20" x="0" y="0" viewBox="0 0 477.873 477.873" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g>
																<g>
																	<g>
																		<path d="M392.533,238.937c-9.426,0-17.067,7.641-17.067,17.067V426.67c0,9.426-7.641,17.067-17.067,17.067H51.2
																			c-9.426,0-17.067-7.641-17.067-17.067V85.337c0-9.426,7.641-17.067,17.067-17.067H256c9.426,0,17.067-7.641,17.067-17.067
																			S265.426,34.137,256,34.137H51.2C22.923,34.137,0,57.06,0,85.337V426.67c0,28.277,22.923,51.2,51.2,51.2h307.2
																			c28.277,0,51.2-22.923,51.2-51.2V256.003C409.6,246.578,401.959,238.937,392.533,238.937z" fill="#FFF" data-original="#FFF" class=""></path>
																	</g>
																</g>
																<g>
																	<g>
																		<path d="M458.742,19.142c-12.254-12.256-28.875-19.14-46.206-19.138c-17.341-0.05-33.979,6.846-46.199,19.149L141.534,243.937
																			c-1.865,1.879-3.272,4.163-4.113,6.673l-34.133,102.4c-2.979,8.943,1.856,18.607,10.799,21.585
																			c1.735,0.578,3.552,0.873,5.38,0.875c1.832-0.003,3.653-0.297,5.393-0.87l102.4-34.133c2.515-0.84,4.8-2.254,6.673-4.13
																			l224.802-224.802C484.25,86.023,484.253,44.657,458.742,19.142z M434.603,87.419L212.736,309.286l-66.287,22.135l22.067-66.202
																			L390.468,43.353c12.202-12.178,31.967-12.158,44.145,0.044c5.817,5.829,9.095,13.72,9.12,21.955
																			C443.754,73.631,440.467,81.575,434.603,87.419z" fill="#FFF" data-original="#FFF" class=""></path>
																	</g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																<g>
																</g>
																</g></svg>
																<!--<svg xmlns="http://www.w3.org/2000/svg" class="fw-normal" width="20" height="17" viewBox="0 0 20 17">-->
																<!--    <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>-->
																<!--</svg>-->
																<span>Seleccionar archivo</span>
															</label>
															<span class="tooltiptext">Hazte PRÉMIUM para poder cargar Portafolio. Adquirir <a href="<?=base_url('tarifas-freelance')?>" target="_blank">aquí</a></span>
														</div>
													</div>
												<?php endif ;?>
											</div>
										</div>
										<div class="sidebar-shadow-md area-social-free">
											<div class="row">
												<h4 class="mt-3">¿Cómo desesas ser contactado?</h4>
												<div class="d-flex flex-wrap flex-sm-nowrap gap-2">
													<div class="flex-fill d-inline-flex align-items-center gap-2">
														<i class="fab fa-facebook-f" aria-hidden="true"></i>
														<input class="mb-0" name="facebook" id="facebook" placeholder="Usuario Facebook:" value="<?= isset($freelancer['contacto']['facebook']) ? $freelancer['contacto']['facebook'] : '' ?>">
													</div>
													<div class="flex-fill d-inline-flex align-items-center gap-2">
														<i class="fab fa-instagram" aria-hidden="true"></i>
														<input class="mb-0" name="instagram" id="instagram" placeholder="Usuario Instagram:" value="<?= isset($freelancer['contacto']['instagram']) ? $freelancer['contacto']['instagram'] : '' ?>">
													</div>
													<div class="flex-fill d-inline-flex align-items-center gap-2">
														<i class="fab fa-whatsapp" aria-hidden="true"></i>
														<input class="numerico mb-0" name="whatsapp" id="whatsapp" placeholder="Cod. país + número:" value="<?= isset($freelancer['contacto']['whatsapp']) ? $freelancer['contacto']['whatsapp'] : '' ?>">
													</div>
													<div class="flex-fill d-inline-flex align-items-center gap-2">
														<i class="fas fa-at" aria-hidden="true"></i>
														<input class="mb-0" type="email" name="correo" id="correo" placeholder="Correo" value="<?= isset($freelancer['contacto']['correo']) ? $freelancer['contacto']['correo'] : '' ?>">
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-12 col-md-12 text-center mt-3">
											<button class="btn btn-default" id="actualizar-perfil" type="submit">Actualizar</button>
										</div>
									</div>
								</form>
								<div class="divider"></div>
								<form class="contact-form-style mt-10 needs-validation" id="form-historial" enctype="multipart/form-data" novalidate style="padding: 25px;">
									<h2 class="h4 fw-bold my-3">Platícanos un poco sobre ti</h2>
									<textarea class="inputs-form text-font my-3" id="historial" name="historial" placeholder="Puedes poner aquí tu historial laboral" required><?= $freelancer['historial'] ?></textarea>
									<div class="text-center">
										<button class="btn btn-default mt-20" id="btn-historial" type="submit" form="form-historial">Actualizar</button>
									</div>
								</form>
								<div class="divider"></div>
								<form class="contact-form-style mt-10 form-perfil needs-validation" id="form-experiencia" enctype="multipart/form-data" novalidate style="padding: 25px;">
									<div class="row wow animate__ animate__fadeInUp animated" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
										<h4 class="mb-20">Cuéntanos de tu experiencia</h4>
										<div class="col-lg-4 col-md-4">
											<div class="input-style mb-0 position-relative">
												<input class="text-font" placeholder="Nombre del proyecto" type="text" name="titulo" id="titulo" required>
												<div class="invalid-tooltip text-font">Requerido</div>
											</div>
										</div>
										<div class="col-6 col-md-4">
											<div class="input-style position-relative">
												<label for="fechaInicio" class="position-absolute bg-white rounded-pill m-0 px-1" style="top: -12px; right: 6px;">Fecha inicio</label>
												<input class="inputs-form text-font" name="fechaInicio" id="fechaInicio" type="date" required>
												<div class="invalid-tooltip text-font">Requerido</div>
											</div>
										</div>
										<div class="col-6 col-md-4">
											<div class="input-style position-relative">
											<label for="fechaFin" class="position-absolute bg-white rounded-pill m-0 px-1" style="top: -12px; right: 6px;">Fecha fin</label>
												<input class="inputs-form text-font" name="fechaFin" id="fechaFin" type="date" required>
												<div class="invalid-tooltip text-font">Requerido</div>
											</div>
										</div>
										<div class="col-lg-12 col-md-12 text-center">
											<div class="position-relative">
												<textarea class="text-font" name="descripcion" id="descripcion" placeholder="Descripción del proyecto" required></textarea>
												<div class="invalid-tooltip text-font">Requerido</div>
											</div>
											<button class="btn btn-default mt-20" type="submit" id="btn-experiencia">Guardar</button>
										</div>
									</div>
								</form>
								<section class="section-box experiencia-carrusel-info ">
									<div class="container">
										<div class="row" id="sec-experiencias">
											<?= view_cell('App\Libraries\FuncionesSistema::experienciasPerfil', ['freelancer' => $freelancer]) ?>
										</div>
									</div>
								</section>
							</div>
							<div id="Proyectos" class="tabcontent">
								<!-- <h3>Panel de Proyectos</h3> -->
								<div class="single-recent-jobs">
									<h4 class="heading-border">
										<span class="d-inline-flex flex-wrap flex-md-nowrap gap-2 p-0">
											<a class="btn-listados text-check ms-0 p-0 px-md-2" data-tipo="espera">Postulado <b>(<?=view_cell('App\Libraries\FuncionesSistema::cantPostulaciones', ['id' => $freelancer['id'], 'rol' => session('rol')])?>)</b></a> 
											<a class="btn-listados inactivo text-check ms-0 p-0 px-md-2" data-tipo="desarrollo">En Progreso <b>(<?=view_cell('App\Libraries\FuncionesSistema::cantProyectosDesarrollo', ['id' => $freelancer['id'], 'rol' => session('rol')])?>)</b></a> 
											<a class="btn-listados inactivo text-check ms-0 p-0 px-md-2" data-tipo="finalizado">Terminados <b>(<?=view_cell('App\Libraries\FuncionesSistema::cantProyectosFinalizados', ['id' => $freelancer['id'], 'rol' => session('rol')])?>)</b></a>
										</span>
									</h4>
									<div class="list-recent-jobs" id="mis-proyectos"></div>
								</div>
							</div>
							<div id="MetodosPago" class="tabcontent">
								<!-- <h3>Métodos de Pago</h3> -->
								<div class="row justify-content-center mt-20">
									<div class="d-flex justify-content-between align-items-center flex-column flex-sm-row gap-2 mt-0 mb-20">
										<h4 style="font-size: 20px;" class="vigencia-info-cuenta">Membresía vigente hasta: <?= !empty($suscripcion_actual['fecha_fin']) ? strftime('%d/%B/%Y', strtotime(explode(' ', $suscripcion_actual['fecha_fin'])[0])) :view_cell('App\Libraries\FuncionesSistema::fechaFinVencida') ?></h4>
										<a class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#agregar-pago">Agregar</a>
									</div>
									<div class="row align-items-center" id="card-mis-tarjetas">
										<?php if(!empty($tarjetas)) :?>
											<?php foreach ($tarjetas as $tarjeta) : ?>
												<div class="col-12 col-sm-6 contenedor-tarjeta mb-3 mb-sm-0" tarjeta="<?= $tarjeta['card']['token'] ?>" data="<?= $tarjeta['card']['brand'] ?>">
													<div class="card-grid position-relative hover-up wow animate__ animate__fadeInUp animated actual-metodo" style="visibility: visible; animation-name: fadeInUp;">
														<?php if(isset($tarjeta_suscripcion) && ($tarjeta_suscripcion['token'] == $tarjeta['card']['token'])) :?>
															<label class="etiqueta-principal">Principal</label>
														<?php endif ;?>
														<div class="text-center">
															<a>
																<figure><img class="ms-3" src="<?= model('autenticacion')->urlImgBanco($tarjeta['card']['brand']) ?>" alt="Procesador de pago bancario" width="60"></figure>
															</a>
														</div>
														<h5 class="text-center mt-0 mb-0 ">
															<a class="num-tarjeta" style="font-size: 20px;">**** <?= $tarjeta['card']['lastFourDigits'] ?> | Expira <?= $tarjeta['card']['expMonth'] . '/' . $tarjeta['card']['expYear'] ?></a>
														</h5>
														<!-- <p class="text-center" style="margin: 0;"><= $tarjeta->holder_name?></p> -->
														<?php if(!empty($tarjeta['card']['type']) && $tarjeta['card']['type'] != 'unknown') :?>
															<span class="metodo-agregado"> Tarjeta de <?=str_replace('e', 'é', $tarjeta['card']['type'])?>o</span>
														<?php endif ;?>
														<div class="text-center mt-10">
															<a class="btn btn-default eliminar-tarjeta" datos="<?= $tarjeta['card']['token'] ?>" tarjeta="<?= $tarjeta['card']['lastFourDigits'] ?>">Eliminar</a>
														</div>
													</div>
												</div>
											<?php endforeach; ?>
										<?php else :?>
											<?= vacio('No tienes tarjetas disponibles.')?>
										<?php endif ;?>
									</div>
								</div>
							</div>
							<div id="HistorialPagos" class="tabcontent">
								<div class="card-body border-0">
									<div class="row justify-content-center" id="listado-historial-pago" style="max-height: 800px; overflow-y: auto;">
										<?= view_cell('App\Libraries\FuncionesSistema::historialPago') ?>
									</div>
								</div>
							</div>
							<div id="MiSuscripcion" class="tabcontent">
								<!-- tarifas -->
								<section class="container p-10 planes-perfil">
									<div class="row mb-0 listado-paquetes">
										<article class="col-12 text-start mb-10">
											<div class="card-header bg-transparent border-0 sin_padding_suscripcion"><h2 class="h3 fw-bold mb-0">Mi Suscripción</h2></div>
											<div class="card-body sin_padding_suscripcion">
												<div class="row justify-content-center">
													<div class="col-md-6">
														<label class="text-font mb-3"><?=$descripcion?></label>
														<label class="d-block text-font">Fecha de Inicio: <strong><?= isset($suscripcion_actual['fecha_inicio']) ? strftime('%d/%B/%Y', strtotime(explode(' ', $suscripcion_actual['fecha_inicio'])[0])) : view_cell('App\Libraries\FuncionesSistema::fechaInicioVencida') ?></strong></label>
														<?php if(isset($suscripcion_actual['renovacion'])) :?>
															<?php if($suscripcion_actual['renovacion']) :?>
																<label class="d-block text-font fw-bold">Próxima fecha de facturación: <?= !empty($suscripcion_actual['fecha_fin']) ? strftime('%d/%B/%Y', strtotime(explode(' ', $suscripcion_actual['fecha_fin'])[0])) :'Vigencia de 365 dias' ?></label>
															<?php else :?>
																<label class="d-block text-font">Membresia vigente hasta: <strong><?= !empty($suscripcion_actual['fecha_fin']) ? strftime('%d/%B/%Y', strtotime(explode(' ', $suscripcion_actual['fecha_fin'])[0])) :view_cell('App\Libraries\FuncionesSistema::fechaFinVencida') ?></strong></label>
															<?php endif ;?>
														<?php endif ;?>
														<?php if($con_suscripcion) :?>
															<?php if($metodoPago != 'Sin plan' && $metodoPago != 'cupon'){ ?>
																<label class="d-inline-flex align-items-center text-font">Pagado con:
																	<?php if($metodoPago == 'efectivo') :?>
																		<label class="card p-1 contenedor_checks active m-0 ms-2" for="pagado_con">
																			<input class="invisible" type="radio" name="pagado_con" hidden="" id="pagado_con">
																			<div class="d-flex align-items-center">
																				<div class="flex-shrink-0">
																					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 20 20" style="color: #3483FA;">
																						<path fill="currentColor" fill-rule="nonzero" d="M19.002 4.004v11.981h-18V4.004h18zm-1.2 1.2h-15.6v9.581h15.6V5.204zm-7.8 2.354a2.436 2.436 0 110 4.873 2.436 2.436 0 110-4.873zm0 1.2a1.236 1.236 0 10-.001 2.473 1.236 1.236 0 00.001-2.473zm4.806.03a1.206 1.206 0 11-.002 2.41 1.206 1.206 0 01.002-2.409zm-9.614 0a1.206 1.206 0 110 2.41 1.206 1.206 0 010-2.409z"></path>
																					</svg>
																					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="color: #3483FA;">
																						<path fill="currentColor" fill-rule="evenodd" d="M11.999.632l9.751 5.67v3.447l-2.244.001v7.52h1.24l2.24 5.265H1.164l2.071-5.265h1.267V9.766l-2.253.004V6.302l9.749-5.67zm7.755 18.138H4.258l-.891 2.265h17.35l-.963-2.265zm-1.748-1.5V9.751l-5.257.006v7.511h5.257zm-6.757-7.51l-5.246.006v7.503h5.246V9.76zm.75-7.392L3.75 7.165v1.104l16.5-.018V7.165l-8.251-4.797z"></path>
																					</svg>
																				</div>
																				<div class="flex-grow-1 ms-2">
																					<label class="d-flex text-font fw-bold m-0">Efectivo</label>
																				</div>
																			</div>
																		</label>
																	<?php elseif($metodoPago == 'tarjeta') :?>
																		<div class="card p-0 contenedor_checks active m-0 ms-2">
																			<div class="d-flex align-items-center">
																				<div class="d-flex flex-shrink-0">
																					<img class="ms-3" src="<?= model('autenticacion')->urlImgBanco($tarjeta_suscripcion['brand']) ?>" alt="Procesador de pago bancario">
																				</div>
																				<div class="flex-grow-1 mx-3">
																					<label class="d-flex text-font fw-bold mb-0">Terminación <?= $tarjeta_suscripcion['lastFourDigits'] ?></label>
																					<label class="d-flex text-font2 mb-0">Vencimiento: <?= $tarjeta_suscripcion['expMonth'] . '/' . $tarjeta_suscripcion['expYear'] ?></label>
																				</div>
																			</div>
																		</div>
																	<?php endif ;?>
																</label>
															<?php } ?>
															<?php if(!empty($suscripcion_actual['cambio_pago'])) :?>
																<div class="row">
																	<label class="d-inline-flex align-items-center text-font fw-bold">Próxima renovación con:
																	<?php if($suscripcion_actual['cambio_pago'] == 'efectivo') :?>
																		<label class="card p-1 contenedor_checks active m-0 ms-2" for="cambio_pago">
																			<input class="invisible" type="radio" name="pagado_con" hidden="" id="cambio_pago">
																			<div class="d-flex align-items-center">
																				<div class="flex-shrink-0">
																					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 20 20" style="color: #3483FA;">
																						<path fill="currentColor" fill-rule="nonzero" d="M19.002 4.004v11.981h-18V4.004h18zm-1.2 1.2h-15.6v9.581h15.6V5.204zm-7.8 2.354a2.436 2.436 0 110 4.873 2.436 2.436 0 110-4.873zm0 1.2a1.236 1.236 0 10-.001 2.473 1.236 1.236 0 00.001-2.473zm4.806.03a1.206 1.206 0 11-.002 2.41 1.206 1.206 0 01.002-2.409zm-9.614 0a1.206 1.206 0 110 2.41 1.206 1.206 0 010-2.409z"></path>
																					</svg>
																					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="color: #3483FA;">
																						<path fill="currentColor" fill-rule="evenodd" d="M11.999.632l9.751 5.67v3.447l-2.244.001v7.52h1.24l2.24 5.265H1.164l2.071-5.265h1.267V9.766l-2.253.004V6.302l9.749-5.67zm7.755 18.138H4.258l-.891 2.265h17.35l-.963-2.265zm-1.748-1.5V9.751l-5.257.006v7.511h5.257zm-6.757-7.51l-5.246.006v7.503h5.246V9.76zm.75-7.392L3.75 7.165v1.104l16.5-.018V7.165l-8.251-4.797z"></path>
																					</svg>
																				</div>
																				<div class="flex-grow-1 ms-2">
																					<label class="d-flex text-font fw-bold m-0">Efectivo</label>
																				</div>
																			</div>
																		</label>
																	<?php else :?>
																		<?php $tarjeta_a_cambiar = model('Autenticacion')->get_tarjeta_user($suscripcion_actual['cambio_pago']); ?>
																		<div class="card p-0 contenedor_checks active m-0 ms-2">
																			<div class="d-flex align-items-center">
																				<div class="d-flex flex-shrink-0">
																					<img class="ms-3" src="<?= model('autenticacion')->urlImgBanco($tarjeta_a_cambiar['card']['brand']) ?>" alt="Procesador de pago bancario">
																				</div>
																				<div class="flex-grow-1 mx-3">
																					<label class="d-flex text-font fw-bold mb-0">Terminación <?= $tarjeta_a_cambiar['card']['lastFourDigits'] ?></label>
																					<label class="d-flex text-font2 mb-0">Vencimiento: <?= $tarjeta_a_cambiar['card']['expMonth'] . '/' . $tarjeta_a_cambiar['card']['expYear'] ?></label>
																				</div>
																			</div>
																		</div>
																	<?php endif ;?>
																	</label>
																</div>
															<?php endif ;?>
															<?php $cambio_plan = model('CambioMembresia')->where('id', session('id'))->first();
																if(!empty($cambio_plan)) :?>
															<?php endif ;?>
														<?php endif ;?>
													</div>
													<div class="col-md-6">
														<div class="card shadow-sm" style="border-radius: 30px;">
															<div class="card-body">
																<?php if($metodoPago != 'cupon') :?>
																	<div id="seccion_info_cupon">
																		<?php $cupon_gratuito = model('UsosCupones')
																							->where(['usos_cupones.id_usuario' => $freelancer['id'], 'tipo' => 'Gratuitos', 'canjeado' => true])
																							->join('cupones', 'cupones.id = usos_cupones.id_cupon')
																							->orderBy('usos_cupones.id', 'DESC')
																							->countAllResults();
																		if($cupon_gratuito == 0 && !$con_suscripcion) :?>
																			<h6 class="h6">Cupón de cuenta premium gratis</h6>
																			<small class="d-block fst-italic small mb-3">* Funciona solamente 1 vez por cuenta y si no has adquirido un plan.</small>
																		<?php else :?>
																			<h6 class="h6">Cupón de descuento</h6>
																			<small class="d-block fst-italic small mb-3">* Aplica para próximo pago de mensualidad o anualidad.</small>
																		<?php endif ;?>
																		<div class="d-flex justify-content-start">
																			<div class="d-inline-flex">
																				<input class="border border-secondary text-font" onkeyup="mayus(this)" name="cupon" type="text" placeholder="Ingrese su cupón">
																				<a class="btn btn-default mx-1 align-self-center validar-cupon">Validar</a>
																			</div>
																		</div>
																	</div>
																<?php endif ;?>
																<?php if($con_suscripcion) :?>
																	<?php $cupon_valido = model('UsosCupones')
																					->where(['usos_cupones.id_usuario' => $freelancer['id']])
																					->join('cupones', 'cupones.id = usos_cupones.id_cupon')
																					->orderBy('usos_cupones.id', 'DESC')
																					->first();?>
																	<?php if(!empty($cupon_valido)) :?>
																		<?php $cuenta_gratis = model('Suscripciones')->where(['id_usuario' =>  session('id'), 'tipo_suscripcion' => 'cupon'])->first();
																			if(empty($cuenta_gratis)) {
																				$plan = model('Suscripciones')->informacion_plan($freelancer['plan']);
																				echo view('backend/viewcells/Informacion_cupon', ['id_usuario' => session('id'), 'tipo' => $cupon_valido['tipo'], 'plan' => $plan]);
																			}
																			else{
																				$cuenta_gratis['tipo'] = $cupon_valido['tipo'];
																			} echo view('backend/viewcells/Informacion_cupon', $cuenta_gratis);?>
																	<?php endif ;?>
																<?php endif ;?>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="card-footer border-0 sin_padding_suscripcion d-flex gap-2 justify-content-center bg-transparent">
												<div class="d-flex justify-content-around gap-2 apartado-de-pagos">
													<?php if(isset($suscripcion_actual['renovacion'])) :?>
														<?php if(!$con_suscripcion) :?>
															<label class="h6 text-naranja">¡Obtén una membresía!</label>
														<?php endif ;?>
														<?php if($suscripcion_actual['renovacion']) :?>
															<?php if($metodoPago == 'tarjeta') :?>
																<button class="btn btn-naranja btn-border text-font" data-bs-toggle="modal" data-bs-target="#actualizar_tarjeta">Actualizar tarjeta</button>
																<div class="modal fade" id="actualizar_tarjeta" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-hidden="true">
																	<div class="modal-dialog modal-dialog-centered">
																		<div class="modal-content">
																			<div class="modal-header justify-content-center border-bottom-0">
																				<h5 class="modal-title fw-bold">Seleccione tarjeta</h5>
																				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
																			</div>
																			<div class="modal-body pt-0">
																				<div class="container">
																					<div class="row align-items-center text-center">
																						<div class="d-flex col-12 justify-content-end">
																							<label class="card p-2 add-tarjeta opciones-ingresar">
																								<div class="d-flex align-item-center">
																									<div class="flex-shrink-0 align-self-center">
																										<img src="<?=base_url('/assets/images/iconos/visa.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
																										<img src="<?=base_url('/assets/images/iconos/amex.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
																										<img src="<?=base_url('/assets/images/iconos/mastercard.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
																									</div>
																									<div class="flex-grow-1 ms-3">
																										<a class="nueva-card" data-bs-toggle="modal" data-bs-target="#agregar-pago">Agregar tarjeta</a>
																									</div>
																								</div>
																							</label>
																						</div>
																						<?php if($metodoPago != 'Sin plan'): ?>
																							<?php foreach ($tarjetas as $key => $tarjeta) : ?>
																								<?php if(!empty($tarjeta_suscripcion)):?>
																									<?php if($tarjeta['card']['token'] == $tarjeta_suscripcion['token']): continue; endif;?>
																								<?php endif ;?>
																								<?php if ($key == count($tarjetas) - 1) : ?>
																									<div class="col-lg-12 my-3" tarjeta="<?= $tarjeta['card']['token'] ?>">
																										<label class="card p-1 contenedor_checks" for="<?= $tarjeta['card']['token'] ?>">
																											<input class=" actualizar_tarjeta text-font" type="radio" name="tarjetas_usuarios_actualizar" hidden id="<?= $tarjeta['card']['token'] ?>">
																											<div class="d-flex align-items-center">
																												<div class="d-flex flex-shrink-0">
																													<img class="ms-3" src="<?= model('autenticacion')->urlImgBanco($tarjeta['card']['brand']) ?>" alt="Procesador de pago bancario">
																												</div>
																												<div class="flex-grow-1 ms-3">
																													<p class="d-flex text-font fw-bold mb-1">Terminación <?= $tarjeta['card']['lastFourDigits'] ?></p>
																													<p class="d-flex text-font2 mb-1"> Tarjeta de <?=str_replace('e', 'é', $tarjeta['card']['type'])?>o</p>
																													<p class="d-flex text-font2 mb-1">Vencimiento: <?= $tarjeta['card']['expMonth'] . '/' . $tarjeta['card']['expYear'] ?></p>
																												</div>
																											</div>
																										</label>
																									</div>
																								<?php endif; ?>
																							<?php endforeach; ?>
																						<?php endif;?>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															<?php endif ;?>
															<form class="d-inline-block" id="form-metodo-pago" action="<?= base_url('CambiarPago')?>" method="POST">
																<input class="invisible text-font" name="cambio_metodo_pago" id="nuevo_metodo_pago" hidden readonly value="<?=$suscripcion_actual['tipo_suscripcion']?>">
																<button class="btn btn-naranja btn-border text-font" type="button" data-bs-toggle="modal" data-bs-target="#cambiar_metodo_pago">Cambiar método pago</button>
															</form>
														<?php else :?>
															<?php if(!empty($suscripcion_actual['cambio_pago'])) :?>
																<button class="btn btn-primary" id="cancelar-cambio-pago">
																	Cancelar cambio de metodo de pago
																</button>
																<script>
																	$('#cancelar-cambio-pago').click(function(){
																		$.ajax({
																			type: 'POST',
																			url: '<?=base_url('/Pagos/cancelarCambioPago')?>',
																			data:{},
																			success: function(data){
																				if(data == 'false') return alertify.notify('Ha surgido un error, inténtelo nuevamente.', 'advertencia');
																				alertify.notify('La solicitud de cambio de pago ha sido declinada.', 'correcto');
																				return setTimeout(function(){location.reload()}, 3000);
																			}, error: function(data){
																				alertify.notify('Error: '+data, 'error');
																			}
																		});
																	});
																</script>
															<?php endif; ?>
														<?php endif; ?>
													<?php endif ;?>
													<?php if(!empty($freelancer['deleted_at'])) :?>
														<button class="btn btn-naranja btn-border text-font" data-bs-toggle="modal" data-bs-target="#recuperar_cuenta_modal">Recuperar cuenta</button>
													<?php else :?>
														<!--<button class="btn btn-naranja btn-border text-font ms-2" data-bs-toggle="modal" data-bs-target="#eliminar_cuenta_modal">Eliminar cuenta</button>-->
													<?php endif ;?>
												</div>
											</div>
										</article>
										<article class="col-12 tarifas-contratantes">
											<div class="row mb-5 listado-paquetes ">
												<div class="d-flex justify-content-between justify-content-md-center align-items-center gap-2 mt-15">
													<span class="text-md text-billed">Pago Mensual</span>
													<label class="switch mb-0">
														<input type="checkbox" id="cb_billed_type" name="billed_type" onchange="checkBilled()">
														<span class="slider round"></span>
													</label>
													<span class="text-md text-billed">Pago Anual</span>
												</div>
												<?php $planes = model('ModelPlanes')->where('cuenta', 'freelancer')->orderBy('id', 'ASC')->find()?>
												<div class="block-pricing mt-100 mt-md-50" style="background: #ff510014;">
													<div class="row">
														<?php foreach($planes as $key => $plan){ ?>
															<?php if($plan['monto'] == 0) continue;?>
															<div class="col-xl-4 col-12 wow animate__animated animate__fadeInUp animated tarifas-cuadros-perfil <?= ($plan['name'] == 'Intermedio')? 'order-md-1 order-lg-2' : 'order-2'?>" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
																<div class="box-pricing-item <?= ($plan['name'] == 'Intermedio')? 'most-popular' : ''?> <?= (($suscripcion_actual != null) && ($plan['id_plan_mensual'] == $suscripcion_actual['id_plan_mensual'] || ($plan['id_plan_anual'] == $suscripcion_actual['id_plan_anual'])))? '' : '' ?>">
																	<?php if(!empty($suscripcion_actual)) :?>
																		<?php if($plan['id_plan_mensual'] == $suscripcion_actual['id_plan_mensual'] || ($plan['id_plan_anual'] == $suscripcion_actual['id_plan_anual'])) :?>
																			<div class="text-center mb-10 d-none">
																				<label class="btn btn-white-sm">Plan Actual</label>
																			</div>
																		<?php endif ;?>
																	<?php endif ;?>
																	<h4 class="mb-15">Plan <?=$plan['name']?></h4>
																	<div class="box-info-price">
																		<span class="text-price for-month display-month"><?= ($plan['monto'] == '0')? 'Gratis' : '$'.number_format($plan['monto'], 2, '.', ',').' <small>MXN</small>' ?></span>
																		<span class="text-price for-year">
																			<?php if($plan['monto'] != 0) :?>
																				<span class="mes-tachado"><?= '$'.number_format($plan['monto'], 2, '.', ',').' <small>MXN</small>'?></span>
																			<?php else :?>
																				<span>Gratis</span>
																			<?php endif ;?>
																			<?= ($plan['monto'] != '0')? '$'.number_format(($plan['monto_anual'] / 12), 2, '.', ',').' <small>MXN</small>' : '' ?>
																		</span>
																		<span class="text-month for-month display-month" style="margin-top: 1px;"><?= ($plan['monto'] == '0') ? 'Vigencia 1 año' : 'Pago mensual'?></span>
																		<span class="text-month for-year" style="margin-top: 1px;"><?= ($plan['monto'] != '0') ? 'Precio anual' : 'Vigencia 1 año'?></span>
																	</div>
																	<div>
																		<p class="text-desc-package mb-30 min-h-desc-paquetes">
																			<span class="min-h-desc-paquetes">
																				<?=$plan['descripcion']?>
																			</span>
																			<br><br class="for-month">
																			<span class="text-facturados for-year">$<?=number_format($plan['monto_anual'], 2, '.', ',').' <small>MXN</small>'?> facturados anualmente</span>
																		</p>
																	</div>
																	<?php if(!empty($suscripcion_actual)) :?>
																		<?php if($plan['id_plan_mensual'] == $suscripcion_actual['id_plan_mensual'] || ($plan['id_plan_anual'] == $suscripcion_actual['id_plan_anual'])) :?>
																			<label class="btn btn-white-sm">Plan Actual</label>
																		<?php else :?>
																			<div class="botones">
																				<a class="btn btn-border for-month display-month" plan="<?=$plan['id_plan_mensual']?>" name="<?=$plan['id_plan_mensual']?>">Seleccionar</a>
																				<a class="btn btn-border for-year anual" plan="<?=$plan['id_plan_anual']?>" name="<?=$plan['id_plan_anual']?>">Seleccionar</a>
																				<input class='invisible text-font inputs-form plan_seleccionado' hidden>
																			</div>
																		<?php endif ;?>
																	<?php else :?>
																		<div class="botones">
																			<a class="btn btn-border for-month display-month" plan="<?=$plan['id_plan_mensual']?>" name="<?=$plan['id_plan_mensual']?>">Seleccionar</a>
																			<a class="btn btn-border for-year anual" plan="<?=$plan['id_plan_anual']?>" name="<?=$plan['id_plan_anual']?>">Seleccionar</a>
																			<input class='invisible text-font inputs-form plan_seleccionado' hidden>
																		</div>
																	<?php endif ;?>
																	<div class="linea-separacion">
																		<span class="separador-tarjetas"></span>
																	</div>
																	<div class="d-block d-lg-none acordion-plan-perfil">
																		<div class="accordion accordion-flush" id="accordionFlush<?=$key?>">
																			<div class="accordion-item border-0">
																				<h2 class="accordion-header" id="flush-heading-card-<?=$key?>">
																					<button class="accordion-button collapsed contenedor-arrow-v justify-content-center" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-card-<?=$key?>" aria-expanded="false" aria-controls="flush-collapse-card-<?=$key?>">
																						Ver características <i class="arrow-v down-v"></i>
																					</button>
																				</h2>
																				<div id="flush-collapse-card-<?=$key?>" class="accordion-collapse collapse" aria-labelledby="flush-heading-card-<?=$key?>" data-bs-parent="#accordionFlush<?=$key?>">
																					<div class="accordion-body p-2 listado-caracteristicas">
																						<div class="text-center my-3 border-bottom caract-list">
																							Crear Perfil
																						</div>
																						<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'paneladministracion']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								Panel de administración
																							</div>
																						<?php } ?>
																						<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'solicitudtrabajo']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								Solicitudes de trabajo
																							</div>
																						<?php } ?>
																						<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'crearcv']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								Cargar CV
																							</div>
																						<?php } ?>
																						<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'veroportunidades']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								Visualización de oportunidades
																							</div>
																						<?php } ?>
																						<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'historialconversaciones']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								Salas de chat
																							</div>
																						<?php } ?>
																						<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'freelancer_destacado']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								Freelancer destacado
																							</div>
																						<?php } ?>
																						<!-- <php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'ganarinsignias']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								Acceso a insignias
																							</div>
																						<php } ?> -->
																						<!-- <php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'valoraciontrabajos']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								Valoración de trabajos
																							</div>
																						<php } ?> -->
																						<!-- <php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'recomendacionesclientes']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								Testimonios de tus clientes
																							</div>
																						<php } ?> -->
																						<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'anadirportafolio']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								Cargar portafolio
																							</div>
																						<?php } ?>
																						<?php $porSemana = view_cell('App\Libraries\FuncionesSistema::postuSemana', ['plan' => $plan['id']])?>
																						<?php if($porSemana > 0){ ?>
																							<div class="text-center my-3 border-bottom caract-list">
																								<?= $porSemana == 9999 ? 'Postulaciones al mes ilimitadas' : $porSemana.' postulaciones al mes'?>
																							</div>
																						<?php } ?>
																						<table class="w-100 tabla-desplegada d-none">
																							<tbody>
																								<tr class="border-bottom">
																									<td scope="row">Crear Perfil 
																										<div class="tooltips d-none"><i style="color: #000000;" class="fas fa-info-circle"></i>
																											<span class="tooltiptext">Descripción del contenido de este apartado pendiente...</span>
																										</div>
																									</td>
																									<td class="text-center col-uno">
																										<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
																									</td>
																								</tr>
																								<tr class="border-bottom">
																									<td scope="row">Panel de administración
																										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																											<span class="tooltiptext">Es donde podrás tener el historial de tu perfil, proyectos, pagos y  método de pago.</span>
																										</div>
																									</td>
																									<td class="text-center">
																										<?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'paneladministracion'])?>
																									</td>
																								</tr>
																								<tr class="border-bottom">
																									<td scope="row">Solicitudes de trabajo 
																										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																											<span class="tooltiptext">Los contratantes pueden mandarte solicitudes de propuestas, y las podrás ver en esta sección.</span>
																										</div>
																									</td>
																									<td class="text-center">
																										<?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'solicitudtrabajo'])?>
																									</td>
																								</tr>
																								<tr class="border-bottom">
																									<td scope="row">Cargar CV 
																										<div class="tooltips d-none"><i style="color: #000000;" class="fas fa-info-circle"></i>
																											<span class="tooltiptext">Cargar CV.</span>
																										</div>
																									</td>
																									<td class="text-center">
																										<?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'crearcv'])?>
																									</td>
																								</tr>
																								<tr class="border-bottom">
																									<td scope="row">Visualización de oportunidades 
																										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																											<span class="tooltiptext">Podrás ver las diferentes oportunidades de proyectos y la información del contratante para que lo puedas contactar y postularte.</span>
																										</div>
																									</td>
																									<td class="text-center">
																										<?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'veroportunidades'])?>
																									</td>
																								</tr>
																								<tr class="border-bottom">
																									<td scope="row">Salas de chat 
																										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																											<span class="tooltiptext">Acceso donde podrás chatear y consultar todas las conversaciones que hayas tenido previamente en algún proyecto.</span>
																										</div>
																									</td>
																									<td class="text-center">
																										<?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'historialconversaciones'])?>
																									</td>
																								</tr>
																								<tr class="border-bottom">
																									<td scope="row">Freelancer Destacado
																										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																											<span class="tooltiptext">Sé de los primeros freelancers en ser buscados por contratante</span>
																										</div>
																									</td>
																									<td class="text-center">
																										<?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'freelancer_destacado'])?>
																									</td>
																								</tr>
																								<!-- <tr class="border-bottom">
																									<td scope="row">Acceso a insignias
																										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																											<span class="tooltiptext">Tendrás acceso de que tu trabajo sea valorado y convertirte en un freelancer distinguido.</span>
																										</div>
																									</td>
																									<td class="text-center">
																										<=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'ganarinsignias'])?>
																									</td>
																								</tr> -->
																								<!-- <tr class="border-bottom">
																									<td scope="row">Valoración de trabajos
																										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																											<span class="tooltiptext">Tendrás la oportunidad de que el contratante  pueda valorarte y que tu trabajo sea reconocido en nuestra red.</span>
																										</div>
																									</td>
																									<td class="text-center">
																										<=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'valoraciontrabajos'])?>
																									</td>
																								</tr> -->
																								<!-- <tr class="border-bottom">
																									<td scope="row">Testimonios de tus clientes 
																										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																											<span class="tooltiptext">Podrás tener comentarios de clientes acerca de los trabajos realizados, esto te ayudará a que tengas recomendaciones de ellos.</span>
																										</div>
																									</td>
																									<td class="text-center"><=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'recomendacionesclientes'])?></td>
																								</tr> -->
																								<tr class="border-bottom">
																									<td scope="row">Cargar portafolio 
																										<div class="tooltips d-none"><i style="color: #000000;" class="fas fa-info-circle"></i>
																											<span class="tooltiptext">Cargar portafolio.</span>
																										</div>
																									</td>
																									<td class="text-center"><?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'anadirportafolio'])?></td>
																								</tr>
																								<tr class="border-bottom">
																									<td scope="row">Postulaciones al mes
																										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																											<span class="tooltiptext">Es el número de propuestas a las que te puedes postular por mes.</span>
																										</div>
																									</td>
																									<?php $porSemana = view_cell('App\Libraries\FuncionesSistema::postuSemana', ['plan' => $plan['id']])?>
																									<td class="text-center"><?= $porSemana == 9999 ? 'Ilimitado' : $porSemana?></td>
																								</tr>
																							</tbody>
																						</table>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														<?php } ?>
													</div>
												</div>
											</div>
											<div class="col-12 mt-4 text-center">
												<a href="<?= base_url('tarifas-freelance')?>" class="btn btn-border" target="_blank">Ver detalle de planes</a>
											</div>
										</article>
									</div>
									<!-- tarifas -->
								</section>
							</div>
						</div>
						<script>
							function openCity(evt, cityName) {
								var i, tabcontent, tablinks;
								tabcontent = document.getElementsByClassName("tabcontent");
								for (i = 0; i < tabcontent.length; i++) {
								tabcontent[i].style.display = "none";
								}
								tablinks = document.getElementsByClassName("tablinks");
								for (i = 0; i < tablinks.length; i++) {
								tablinks[i].className = tablinks[i].className.replace(" active", "");
								}
								document.getElementById(cityName).style.display = "block";
								evt.currentTarget.className += " active";
							}
							// Get the element with id="defaultOpen" and click on it
							document.getElementById("defaultOpen").click();
						</script>
						<!-- TABS -->
						<div class="single-recent-jobs d-none d-lg-block">
							<h4 class="heading-border"><span>Proyectos para ti</span></h4>
							<div class="list-recent-jobs">
								<?php foreach ($proyectos as $p) : ?>
									<?= view_cell('App\Libraries\FuncionesSistema::cardProyectoDisponible', ['proyecto' => $p]) ?>
								<?php endforeach; ?>
								<div class="col-12 mt-4">
									<div class="d-flex boton-de-mas-proyectos">
										<a class="btn btn-naranja-circular text-font d-flex align-items-center justify-content-center" href="<?=base_url('/buscar-proyecto?search=&area=')?>" style="width: 150px; height: 150px;">
											<i style="font-size: 80px;" class="fas fa-arrow-right" aria-hidden="true"></i>
										</a>
										&nbsp;
										<div style="max-width: 250px; font-size: 24px; font-weight: bold; font-family: 'Poppins', sans-serif;" class="mx-3 fw-bold align-self-center">
										ENCONTRAR<br>MÁS PROYECTOS
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- COLUMNA DERECHA -->
				<div class="col-lg-4 col-md-12 col-sm-12 col-12 pl-40 pl-lg-15 mt-lg-30 columna-der">
					<div class="sidebar-shadow">
						<!-- <h5 class="font-bold">Overview</h5> -->
						<div class="sidebar-heading info-free-no d-md-none">
							<div class="avatar-sidebar">
								<figure><img class="img-perfil-icon" alt="mextemps" src="<?=imagenPerfil($freelancer)?>" /></figure>
								<div class="sidebar-info">
									<span class="d-block titulo-name" id="nombre_freelancer_aside">
										<?=$freelancer['nombre']?> <?=$freelancer['apellidos']?>
									</span>
									<div class="d-none d-lg-block">
										<span class="d-block sidebar-website-text"><?=$freelancer['correo']?></span>
										<?php if($freelancer['cuenta'] == 'predeterminado') :?>
											<span class="d-block text-naranja opciones-ingresar actualizar-contrasena" data-bs-toggle="modal" data-bs-target="#actualizar_contraseña_modal">Actualizar contraseña</span>
										<?php endif ;?>
									</div>
								</div>
							</div>
						</div>
						<!-- SOBRE MI -->
						<div style="display: none;">
							<?php if(!empty($freelancer['historial'])) :?>
								<p class="text-description mt-3 text-font" id="sec-hist"><?= $freelancer['historial'] ?></p>
							<?php endif ;?>
						</div>
						<!-- MI SUSCRIPCION -->
						<div class="sidebar-list-job pt-20 misuscripcion-columna info-free-no d-md-none">
							<h6 class="small-heading mb-20">Mi Suscripción</h6>
							<div class="sidebar-list-member sidebar-list-follower">
								<label class="text-font d-flex justify-content-between"><?=$descripcion?></label>
								<label class="d-flex justify-content-between text-font">Fecha de Inicio: <strong><?= isset($suscripcion_actual['fecha_inicio']) ? strftime('%d/%B/%Y', strtotime(explode(' ', $suscripcion_actual['fecha_inicio'])[0])) : view_cell('App\Libraries\FuncionesSistema::fechaInicioVencida') ?></strong></label>
								<label class="d-flex justify-content-between text-font">Vigente hasta: <strong><?= !empty($suscripcion_actual['fecha_fin']) ? strftime('%d/%B/%Y', strtotime(explode(' ', $suscripcion_actual['fecha_fin'])[0].'- 1 days')) : view_cell('App\Libraries\FuncionesSistema::fechaFinVencida') ?></strong></label>
								<?php if($con_suscripcion){ ?>
									<label class="d-flex justify-content-between text-font mb-20">
										Pagado con: &nbsp; <span class="mb-0 <?= $metodoPago == 'tarjeta'? 'text-end' : '' ?>">
										<?php if($metodoPago == 'efectivo') :?>
											<label class="card p-1 contenedor_checks active m-0 ms-2" for="pagado_con">
											<input class="invisible" type="radio" name="pagado_con" hidden="" id="pagado_con">
												<div class="d-flex align-items-center">
													<div class="flex-shrink-0">
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 20 20" style="color: #3483FA;">
															<path fill="currentColor" fill-rule="nonzero" d="M19.002 4.004v11.981h-18V4.004h18zm-1.2 1.2h-15.6v9.581h15.6V5.204zm-7.8 2.354a2.436 2.436 0 110 4.873 2.436 2.436 0 110-4.873zm0 1.2a1.236 1.236 0 10-.001 2.473 1.236 1.236 0 00.001-2.473zm4.806.03a1.206 1.206 0 11-.002 2.41 1.206 1.206 0 01.002-2.409zm-9.614 0a1.206 1.206 0 110 2.41 1.206 1.206 0 010-2.409z"></path>
														</svg>
														<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="color: #3483FA;">
															<path fill="currentColor" fill-rule="evenodd" d="M11.999.632l9.751 5.67v3.447l-2.244.001v7.52h1.24l2.24 5.265H1.164l2.071-5.265h1.267V9.766l-2.253.004V6.302l9.749-5.67zm7.755 18.138H4.258l-.891 2.265h17.35l-.963-2.265zm-1.748-1.5V9.751l-5.257.006v7.511h5.257zm-6.757-7.51l-5.246.006v7.503h5.246V9.76zm.75-7.392L3.75 7.165v1.104l16.5-.018V7.165l-8.251-4.797z"></path>
														</svg>
													</div>
													<div class="flex-grow-1 ms-2">
														<label class="d-flex text-font m-0" style="font-size: 12px !important;">Efectivo</label>
													</div>
												</div>
											</label>
										<?php elseif($metodoPago == 'tarjeta') :?>
											<div class="card p-0 contenedor_checks active m-0 ms-2">
												<div class="d-flex align-items-center">
													<div class="d-flex flex-shrink-0">
														<img class="ms-3" src="<?= model('autenticacion')->urlImgBanco($tarjeta_suscripcion['brand']) ?>" alt="Procesador de pago bancario">
													</div>
													<div class="flex-grow-1 mx-3">
														<label class="d-flex text-font fw-bold mb-0">Terminación <?= $tarjeta_suscripcion['lastFourDigits'] ?></label>
														<label class="d-flex text-font2 mb-0">Vencimiento: <?= $tarjeta_suscripcion['expMonth'] . '/' . $tarjeta_suscripcion['expYear'] ?></label>
													</div>
												</div>
											</div>
										<?php elseif($metodoPago == 'Sin plan'):?>
											&nbsp;<label class="mb-0"><b><?= empty($suscripcion_actual) ? 'Sin vigencia' : 'Cobro automático cancelado' ?></b></label>
										<?php endif ;?>
										</span>
									</label>
								<?php } ?>
							</div>
						</div>
						<div class="d-flex justify-content-center gap-2 info-free-no d-md-none">
							<?php if(!empty($freelancer['deleted_at'])) :?>
								<button class="btn btn-naranja btn-border" data-bs-toggle="modal" data-bs-target="#recuperar_cuenta_modal">Recuperar cuenta</button>
							<?php else :?>
								<button class="btn btn-naranja btn-border" data-bs-toggle="modal" data-bs-target="#eliminar_cuenta_modal">Eliminar cuenta</button>
							<?php endif ;?>
							<a class="btn btn-primary" data-estatus="<?= (empty($freelancer['ocultar']) || !empty($freelancer['ocultar']) == false)? 'ocultar' : 'mostrar' ?>" id="ocultar-cuenta">
								<?= (empty($freelancer['ocultar']) || !empty($freelancer['ocultar']) == false)? 'Ocultar cuenta' : 'Mostrar cuenta' ?>
							</a>
						</div>
						<!-- QR FREELANCER -->
						<div class="sidebar-list-job pt-20 misuscripcion-columna d-none d-lg-block">
							<h6 class="small-heading mb-20">Compartir mi perfil</h6>
							<div class="text-center row">
								<div class="col-12">
									<img id="contenedor-qr" alt="Compartir perfil QR">
								</div>
								<div class="col-12 mb-3">
									<button class="btn btn-naranja btn-border" id="btnDescargar">Descargar QR</button>
									<a href="<?= route_to('prevista', base64_encode(session('id'))) ?>" class="btn btn-naranja btn-border" target="_blank">Vista de perfil</a>
								</div>
								<div class="col-md-12">
									<div class="d-flex align-items-center mr-10">
										<label class="fw-bold me-auto mb-0" style="display: block;">Compartir en:</label>    
										<a class="rounded-circle iconos-compartir" href="https://www.facebook.com/sharer/sharer.php?u=<?=base_url('Usuarios/prevista/'.base64_encode(session('id')))?>" target="_blank">
											<span class="iconify" data-icon="akar-icons:facebook-fill" style="color: #0b5ed7; padding: 0px; border: 0px;" data-width="25" data-height="25"></span>
										</a>
										<a class="rounded-circle mx-2 iconos-compartir" href="https://wa.me/send?text=<?=base_url('Usuarios/prevista/'.base64_encode(session('id')))?>" target="_blank">
											<span class="iconify" data-icon="akar-icons:whatsapp-fill" style="color: #0b5ed7; padding: 0px; border: 0px;" data-width="25" data-height="25"></span>
										</a>
										<a class="rounded-circle iconos-compartir" href="https://www.linkedin.com/sharing/share-offsite/?url=<?=base_url('Usuarios/prevista/'.base64_encode(session('id')))?>" target="_blank">
											<span class="iconify" data-icon="akar-icons:linkedin-box-fill" style="color: #0b5ed7; padding: 0px; border: 0px;" data-width="25" data-height="25"></span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<!-- SUELDO POR HORA -->
						<form class="needs-validation" enctype="multipart/form-data" id="form-pagoHora" novalidate>
							<div class="sidebar-list-job d-none d-lg-block">
								<h6 class="small-heading">Costo por hora de tu trabajo</h6>
								<div class="sidebar-list-member sidebar-list-follower">
									<div class="input-group expiration-date apartado-costo-hora" style="margin: 20px 0;">
										<div class="input-group-prepend">
											<div class="input-group-text" style="border-radius: .25rem 0 0 .25rem; height: 50px;">$</div>
										</div>
										<input name="pagoHora" id="pagoHora" type="number" value="<?= $freelancer['pagoHora'] ?>" required class="form-control numerico" min="1" maxlength="9" placeholder="Costo por hora" aria-label="monto" aria-describedby="basic-addon1" style="height: 50px; max-width: 200px; margin: 0px;">
										<div class="invalid-tooltip text-font2">Requerido</div>
									</div>
								</div>
								<button id="btn-pagoHora" type="submit" class="btn btn-primary mr-10">Actualizar costo</button>
							</div>
						</form>
						<div class="sidebar-team-member pt-10 m-0 mt-md-4">
							<h6 class="small-heading">Idiomas que dominas</h6>
							<div class="sidebar-list-member sidebar-list-follower p-0 pt-3">
								<ul id="contenido-idiomas" class="mb-0">
									<?php if (isset($freelancer['idiomas'])) : ?>
										<?php foreach ($freelancer['idiomas'] as $key => $idioma) : ?>
											<li class="idioma<?= $key ?> m-0 me-3 mb-2">
												<a data-bs-toggle="modal" data-bs-target="#editar-idioma<?= $key ?>">
													<img class="img-fluid rounded-circle img-idiomas" id="bandera<?= $key ?>" src="<?= $idioma['imagen'] ?>" alt="Bandera <?= $idioma['idioma'] ?>">
													<span class="position-absolute border border-light bg-light rounded-circle bottom-0 end-0 p-1" style="margin-right: -10px; margin-bottom: -5px;">
														<label class="text-font2 m-0" id="text-rango<?= $key ?>"><?= $idioma['rango'] * 5 ?>%</label>
													</span>
												</a>
											</li>
										<?php endforeach; ?>
									<?php endif; ?>
								</ul>
							</div>
						</div>
						<div class="boton-agregar-idiomas">
							<a data-bs-toggle="modal" data-bs-target="#agregar-idioma" class="btn btn-primary mr-10">Agregar idioma</a>
						</div>
						<form class="needs-validation hibilidades-agregar" id="form-areas" accept-charset="utf-8" novalidate>
							<div class="sidebar-list-job mt-10" id="side-areas">
								<h6 class="small-heading">Mis Habilidades</h6>
								<?= view_cell('App\Libraries\FuncionesSistema::areasPerfil', ['freelancer' => $freelancer]) ?>
							</div>
							<div class="sidebar-list-job none-bd" style="margin-top: 0;padding-top: 0;">
								<div class="mb-3">
									<div class="form-group select-style select-style-icon position-relative">
										<select class="form-control form-icons select-active select-hidden-accessible" id="datalist_area" name="area" tabindex="-1" aria-hidden="true" required>
											<option value="" hidden>Selecciona tu experiencia</option>
											<?php foreach ($habilidades as $key => $a) : ?>
												<option value="<?= $a['area'].' - '.$a['superior'] ?>"><?= $a['area'].' - '.$a['superior'] ?></option>
											<?php endforeach; ?>
										</select>
										<i class="fi fi-rr-list"></i>
										<div class="invalid-tooltip text-font2" id="inv-area">Requerido</div>
									</div>
								</div>
								<div class="mb-3">
									<label>
										¿No encuentras la área o habilidad ideal para ti? <span style="cursor: pointer;" class="text-naranja" id="sug-area" data-bs-toggle="modal" data-bs-target="#sugerencia-area">Da clic aquí</span>
									</label>
								</div>
								<div class="mb-3">
									<button type="submit" id="btn-area" form="form-areas" class="btn btn-primary mr-10">Añadir</button>
									<a id="eliminar_experiencias" class="btn btn-border">Eliminar todas</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<!-- MODALS DE IDIOMAS -->
<div id="contenido_editar_idiomas">
	<?php if (isset($freelancer['idiomas'])) : ?>
		<?php foreach ($freelancer['idiomas'] as $key => $idioma) : ?>
			<div class="modal fade" id="editar-idioma<?=$key?>" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered">
					<div class="modal-content">
						<div class="modal-header justify-content-center border-bottom-0">
							<h5 class="modal-title fw-bold">Idioma <?= $idioma['idioma'] ?></h5>
							<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
						</div>
						<div class="modal-body">
							<div class="container">
								<form class="row justify-content-center needs-validation" id="form-editarIdioma<?= $key ?>" enctype="multipart/form-data" novalidate>
									<div class="col-6 my-3">
										<input name="id" value="<?= $key ?>" hidden>
										<!--view_cell('App\Libraries\FuncionesSistema::idiomasActivos', ['atributo' => 'modal_idioma']) -->
										<label for="rango" class="text-font">Dominio del Idioma</label>
										<input class="form-range edit-rango" type="range" max="20" datos="<?= $key ?>" name="rango" id="rango<?= $key ?>" value="<?= $idioma['rango'] ?>">
										<label class="text-center" id="modal-text-rango<?= $key ?>"><?= $idioma['rango'] * 5 ?>%</label>
									</div>
									<div class="col-6 my-3">
										<ul class="ps-0 text-start botones-lista">
											<li class="nav-link px-2">
												<label class="text-check text-font text-dark user-select-none position-relative" for="edit-nativo<?= $key ?>">
													<input class="invisible" type="radio" name="nivel" value="nativo" <?= $idioma['nivel'] == 'nativo' ? 'checked' : '' ?> id="edit-nativo<?= $key ?>" hidden>
													<span class="check"></span>
													Nativo
												</label>
											</li>
											<li class="nav-link px-2">
												<label class="text-check text-font text-dark user-select-none position-relative" for="edit-estudiado<?= $key ?>">
													<input class="invisible" type="radio" name="nivel" value="estudiado" <?= $idioma['nivel'] == 'estudiado' ? 'checked' : '' ?> id="edit-estudiado<?= $key ?>" hidden>
													<span class="check"></span>
													Estudiado
												</label>
											</li>
											<li class="nav-link px-2">
												<label class="text-check text-font text-dark user-select-none position-relative" for="edit-viviendo<?= $key ?>">
													<input class="invisible" type="radio" name="nivel" value="viviendo" <?= $idioma['nivel'] == 'viviendo' ? 'checked' : '' ?> id="edit-viviendo<?= $key ?>" hidden>
													<span class="check"></span>
													Viviendo en el país
												</label>
											</li>
										</ul>
									</div>
									<div class="col-12 text-center my-3 botonesIdioma">
										<a class="btn btn-naranja btn-border text-font actualizar" datos="<?= $key ?>">Actualizar</a>
										<a class="btn btn-naranja btn-border text-font eliminar" datos="<?= $key ?>">Eliminar</a>
										<form id="form<?=$key?>Delete">
											<input type="hidden" name="dato" value="<?=$key?>">
										</form>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>
<?php if(!empty($freelancer['deleted_at'])) :?>
	<div class="modal fade" id="recuperar_cuenta_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header justify-content-center flex-column border-bottom-0">
					<h5 class="modal-title fw-bold mb-2">Restaurar cuenta</h5>
					<h6 class="fw-bold">¿Estás seguro?</h6>
					<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body pt-0">
					<div class="container">
						<div class="row align-items-center text-center">
							<p class="mb-0 text-font">Tu cuenta volverá a seguir en funcionamiento y accediendo a los beneficios disponibles de acorde a tu tipo de plan adquirido.</p>
							<div class="d-flex col-12 justify-content-end">
								<a class="btn btn-naranja" id="recuperar_cuenta">Recuperar cuenta</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php else :?>
	<div class="modal fade" id="eliminar_cuenta_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header justify-content-center flex-column border-bottom-0">
					<h5 class="modal-title fw-bold mb-2">Eliminar cuenta permanentemente</h5>
					<h6 class="fw-bold">¿Estás seguro?</h6>
					<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body pt-0">
					<div class="container">
						<div class="row align-items-center text-center">
							<p class="mb-0 text-font">Tu cuenta será suspendida durante un perdiodo de <b>60 días</b> si usted desea recuperarla, posteriormente de procederá a dar eliminar permanentemente su cuenta con todos sus datos.<br><br>Para formar parte del equipo de <span class="text-naranja">MEXTEMPS FREELANCE</span> tendrá que registrase nuevamente.</p>
							<div class="d-flex col-12 justify-content-end">
								<a class="btn btn-naranja" id="baja_cuenta">Eliminar cuenta</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif ;?>
<!-- MODALS -->
<div class="modal fade" id="agregar-pago" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" style="max-width: 620px;">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0">
				<h5 class="modal-title fw-bold">Agregar método de pago</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body">
				<div class="container">
					<form class="row justify-content-center needs-validation" id="netpay-form" enctype="multipart/form-data" novalidate>
						<input type="hidden" name="token_id" id="token_id">
						<input type="hidden" name="deviceFingerPrint" id="deviceFingerPrint">
						<input type="hidden" name="referencia_cargo" id="referencia_cargo">
						<input type="hidden" name="token_transaccion" id="token_transaccion">
						<div class="text-center">
							<p class="fw-bold h6 text-start mt-3" style="display: inline-block; padding-right: 20px;">Tarjeta Débito/Crédito</p>
							<!-- <img class="binking__form-brand-logo"> -->
							<div class="flex-shrink-0" style="display: inline-block; vertical-align: bottom;">
								<img src="<?= base_url('assets/images/iconos/visa.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
								<img src="<?= base_url('assets/images/iconos/amex.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
								<img src="<?= base_url('assets/images/iconos/mastercard.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
							</div>
						</div>
						<div class="row justify-content-around mt-3">
							<div class="col-12">
								<div class="form-floating mb-3 ">
									<input class="form-control text-font" name="nombre" id="nombrePay" required>
									<label class="text-font" for="nombrePay">Nombre Titular</label>
								</div>
							</div>
							<div class="col-12">
								<div class="form-floating mb-3 ">
									<input class="form-control text-font binking__number-field numerico" autocomplete="cc-number" inputmode="numeric" pattern="[0-9 ]*" name="tarjeta" id="tarjetaPay" maxlength="16" oninput="javascript: if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
									<label class="text-font" for="tarjetaPay">Número Tarjeta</label>
								</div>
							</div>
							<div class="col-12 mes-an-cvv">
								<dv class="row">
									<div class="col-12 col-md-4 filas-t1">
										<div class="form-floating ">
											<input class="form-control text-font binking__month-field numerico" name="mes" id="mesPay" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
											<label class="text-font" for="mesPay">Mes</label>
										</div>
									</div>
									<div class="col-12 col-md-4 filas-t2">
										<div class="form-floating ">
											<input class="form-control text-font binking__year-field numerico" name="año" id="añoPay" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
											<label class="text-font" for="añoPay">Año</label>
										</div>
									</div>
									<div class="col-12 col-md-4 filas-t3">
										<div class="form-floating ">
											<input class="form-control text-font binking__code-field numerico" type="password" name="cvv" id="cvvPay" maxlength="4" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
											<label class="text-font" for="cvvPay">CVV</label>
											<img class="referencia-card" src="<?=base_url('/assets/images/cvv.png')?>" style="position: relative; top: -44px; right: -100px;">
										</div>
									</div>
								</dv>
							</div>
						</div>
						<div class="col-12 my-3">
							<img src="" alt="">
							<button class="btn btn-naranja btn-border text-font" id="btn-agregarTarjeta" type="submit">Agregar tarjeta</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Agregar idioma -->
<div class="modal fade" id="agregar-idioma" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0">
				<h5 class="modal-title text-center fw-bold">Agrega los idiomas que dominas</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body">
				<div class="container">
					<form class="row justify-content-center needs-validation" id="form-nuevoIdioma" enctype="multipart/form-data" novalidate>
						<div class="col-6 mb-3">
							<?=view_cell('App\Libraries\FuncionesSistema::idiomasActivos', ['atributo' => 'idioma'])?>
							<br>
							<label>¿No encuentras el idioma que deseas? <a class="text-naranja" style="cursor: pointer; text-decoration: none;" id="sugerir-idioma">da click aqui</a></label>
							<br>
						</div>
						<div class="col-6 mb-3">
							<ul class="text-start mb-0 ps-0">
								<li class="nav-link px-2">
									<label class="text-check text-font user-select-none position-relative" style="color: #000000; font-size: 13px !important;" for="nativo">
										<input class="invisible" hidden type="radio" name="nivel" value="nativo" id="nativo">
										<span class="check" style="padding-right: 8px;"></span>
										Nativo
									</label>
								</li>
								<li class="nav-link px-2">
									<label class="text-check text-font user-select-none position-relative" style="color: #000000; font-size: 13px !important;" for="estudiado">
										<input class="invisible" hidden type="radio" name="nivel" value="estudiado" id="estudiado">
										<span class="check" style="padding-right: 8px;"></span>
										Estudiado
									</label>
								</li>
								<li class="nav-link px-2">
									<label class="text-check text-font user-select-none position-relative" style="color: #000000; font-size: 13px !important;" for="viviendo">
										<input class="invisible" hidden type="radio" name="nivel" value="viviendo" id="viviendo">
										<span class="check" style="padding-right: 8px;"></span>
										Viviendo en el país
									</label>
								</li>
							</ul>
						</div>
						<div class="col-6">
							<label for="rango" class="text-font">Dominio del Idioma</label>
							<input class="form-range border-0" type="range" max="20" id="rango" name="rango" value="0" required>
							<label class="text-center" id="text-rango"></label>
						</div>
						<div class="col-12 text-center mt-2">
							<a class="btn btn-naranja btn-border text-font" id="btn-nuevoIdioma">Agregar</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Solicitar factura -->
<div class="modal fade" id="solicitar-factura" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo-modal-factura" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0">
				<h5 class="modal-title fw-bold" id="titulo-modal-factura">Solicitud de factura</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body">
				<form id="form-solicitud-factura">
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Sugerencia de area o habilidad -->
<div class="modal fade" id="sugerencia-area" tabindex="-1" aria-labelledby="titulo-sugerencia" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0">
				<h5 class="modal-title fw-bold" id="titulo-sugerencia">Mándanos tu sugerencia</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body">
				<label>Mándanos tu sugerencia para el área o habilidad que deseas que incorporemos.</label>
				<form id="form-sug-area">
					<div class="form-floating mb-3">
						<input type="text" class="form-control" name="nombre" id="sug-area-input" placeholder="name@example.com">
						<label for="sug-area-input">Área o habilidad</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="tipo" value="area" id="tipo-area">
						<label class="form-check-label" for="tipo-area">
							Área
						</label>
					</div>
					<div class="form-check mb-3">
						<input class="form-check-input" type="radio" name="tipo" value="habilidad" id="tipo-habilidad">
						<label class="form-check-label" for="tipo-habilidad">
							Habilidad
						</label>
					</div>
					<div class="mb-3" id="select-areas-div">
						<div class="form-floating">
							<select class="form-select" id="select-areas-sug" name="area" aria-label="Area">
								<?php foreach ($habilidades as $key => $a) : ?>
									<option value="<?=$a['area']?>"><?=$a['area']?></option>
								<?php endforeach; ?>
							</select>
							<label for="select-areas-sug">Área</label>
						</div>
					</div>
					<div class="text-center">
						<a class="btn btn-naranja btn-hover" id="btn-addsugarea">Enviar sugerencia</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<style>
	.text-check input[type="radio"]:checked~.check::after {
		content: '';
		position: absolute;
		width: 8px;
		height: 8px;
		left: -14px;
		bottom: 6px;
		background: black;
		border-radius: 50px;
	}
</style>
<!-- Modal para sugerir idioma -->
<div class="modal fade" id="sug-idioma" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo-sugerir" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0">
				<h5 class="modal-title fw-bold" id="titulo-sugerir">Sugerir idioma</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body">
				<div class="container">
					<form class="needs-validation" id="form-sugerirIdioma" enctype="multipart/form-data" novalidate>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" id="nombre-idioma" name="nombre" placeholder="name@example.com">
							<label for="nombre-idioma">Nombre del idioma</label>
						</div>
						<?= view_cell('App\Libraries\FuncionesSistema::tablaIdiomas') ?>
						<div class="col-12 text-center my-3">
							<a class="btn btn-outline-naranja" id="env-solicitud">Enviar solicitud</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Recortar imagen -->
<div class="modal fade" id="cropImagePop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
				<h5 class="modal-title h5" id="myModalLabel">Ajustar imagen</h5>
			</div>
			<div class="modal-body">
				<div id="upload-demo" class="center-block"></div>
			</div>
			<div class="modal-footer">
				<button type ="button" id="cropImageBtnPerfil" class="btn btn-naranja btn-border">Aceptar</button>
			</div>
		</div>
	</div>
</div>
<!-- Obtener membresia -->
<div class="modal fade" id="obtener_membresia" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header border-bottom-0">
				<h5 class="modal-title fw-bold">Formas de pago</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row align-items-center text-center" id="contenido-tarjetas">
						<div id="selector">
							<?php if(empty($tarjetas)) :?>
								<?= vacio('No tienes tarjetas disponibles')?>
							<?php else :?>
								<?php foreach ($tarjetas as $key => $tarjeta) : ?>
									<?php if ($key == count($tarjetas) - 1) : ?>
										<div class="col-lg-12 mt-3 mb-1" id="contenedor-tarjetaObtenerSubs-<?= $tarjeta['card']['token'] ?>">
											<div class="card p-1">
												<div class="d-flex align-items-center">
													<div class="d-flex flex-shrink-0">
														<img class="ms-3" id="imagen_modal" src="<?= model('autenticacion')->urlImgBanco($tarjeta['card']['brand']) ?>" alt="Procesador de pago bancario">
													</div>
													<div class="flex-grow-1 ms-3">
														<label class="d-flex text-font fw-bold m-0" id="terminacion_modal">Terminación <?= $tarjeta['card']['lastFourDigits'] ?></label>
														<label class="d-flex text-font2 m-0" id="banco_modal"> Tarjeta de <?=str_replace('e', 'é', $tarjeta['card']['type'])?>o</label>
														<label class="d-flex text-font2 m-0" id="vencimiento_modal">Vencimiento: <?= $tarjeta['card']['expMonth'] . '/' . $tarjeta['card']['expYear'] ?></label>
													</div>
													<div class="flex-grow-1 ms-3">
														<a class="text-font justify-content-center user-select-none text-decoration-none text-naranja opciones-ingresar" id="tarj_modal" datos="<?= $tarjeta['card']['token'] ?>">Proceder al pago <i class="fas fa-chevron-right"></i></a>
													</div>
												</div>
											</div>
										</div>
										<div class="d-flex col-12 justify-content-end">
											<a class="text-font2 opciones-ingresar" data-bs-toggle="modal" data-bs-target="#cambiar_tarjeta">Cambiar tarjeta</a>
										</div>
									<?php endif; ?>
								<?php endforeach; ?>
							<?php endif ;?>
						</div>
						<div class="modal-header border-bottom-0 py-0">
							<h5 class="modal-title fw-bold">Opciones de pago</h5>
						</div>
						<div class="col-lg-12 my-2">
							<label class="card p-2 add-tarjeta opciones-ingresar" id="flujo_PagoTarjeta">
								<div class="d-flex align-item-center">
									<div class="flex-shrink-0 align-self-center">
										<img src="<?=base_url('/assets/images/iconos/visa.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
										<img src="<?=base_url('/assets/images/iconos/amex.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
										<img src="<?=base_url('/assets/images/iconos/mastercard.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
									</div>
									<div class="flex-grow-1 ms-3">
										<p class="d-flex text-font fw-bold m-0">Agregar tarjeta</p>
									</div>
								</div>
							</label>
						</div>
						<div class="col-lg-12 my-2" id="columna_tienda">
							<label class="card p-2 contenedor_checks" for="metodo_tienda">
								<input class="check-otrosPagos" type="radio" name="otros_pagos" hidden id="metodo_tienda" value="efectivo">
								<div class="d-flex align-items-center">
									<div class="flex-shrink-0">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 20 20" style="color: #3483FA;">
											<path fill="currentColor" fill-rule="nonzero" d="M19.002 4.004v11.981h-18V4.004h18zm-1.2 1.2h-15.6v9.581h15.6V5.204zm-7.8 2.354a2.436 2.436 0 110 4.873 2.436 2.436 0 110-4.873zm0 1.2a1.236 1.236 0 10-.001 2.473 1.236 1.236 0 00.001-2.473zm4.806.03a1.206 1.206 0 11-.002 2.41 1.206 1.206 0 01.002-2.409zm-9.614 0a1.206 1.206 0 110 2.41 1.206 1.206 0 010-2.409z"></path>
										</svg>
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="color: #3483FA;">
											<path fill="currentColor" fill-rule="evenodd" d="M11.999.632l9.751 5.67v3.447l-2.244.001v7.52h1.24l2.24 5.265H1.164l2.071-5.265h1.267V9.766l-2.253.004V6.302l9.749-5.67zm7.755 18.138H4.258l-.891 2.265h17.35l-.963-2.265zm-1.748-1.5V9.751l-5.257.006v7.511h5.257zm-6.757-7.51l-5.246.006v7.503h5.246V9.76zm.75-7.392L3.75 7.165v1.104l16.5-.018V7.165l-8.251-4.797z"></path>
										</svg>
									</div>
									<div class="flex-grow-1 ms-3">
										<p class="d-flex text-font fw-bold m-0">Efectivo</p>
									</div>
								</div>
							</label>
						</div>
						<!--<?php if($con_suscripcion) :?>-->
			<!--				<div class="col-12">-->
			<!--				    <small class="fst-italic">Al adquirir este plan no se cancela la suscripción actual y no se realizará algún cobro hasta la próxima fecha de facturación</small>-->
			<!--				</div>-->
						<!--<?php endif ;?>-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if($con_suscripcion && $metodoPago != 'cupon') :?>
	<div class="modal fade" id="cambiar_metodo_pago" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="tituloCambiopago" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0">
					<h5 class="modal-title fw-bold" id="tituloCambiopago">Cambio de pago</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row align-items-center text-center" id="contenido-tarjetas">
							<?php if($metodoPago != 'tarjeta') :?>
								<div class="d-flex col-12 justify-content-start">
									<a class="text-font text-naranja opciones-ingresar" data-bs-toggle="modal" data-bs-target="#agregar-pago">Agregar tarjeta</a>
								</div>
								<?php foreach ($tarjetas as $key => $tarjeta) : ?>
									<?php if(!empty($tarjeta_suscripcion)):?>
										<?php if($tarjeta['card']['token'] == $tarjeta_suscripcion['token']): continue; endif;?>
									<?php endif ;?>
									<?php if ($key == count($tarjetas) - 1) : ?>
										<div class="col-lg-12 mt-3 mb-1" id="contenedor-tarjeta-<?= $tarjeta['card']['token'] ?>">
											<div class="card p-1">
												<div class="d-flex align-items-center">
													<div class="d-flex flex-shrink-0">
														<img class="ms-3" id="imagen_modalCambiopago" src="<?= model('autenticacion')->urlImgBanco($tarjeta['card']['brand']) ?>" alt="Procesador de pago bancario">
													</div>
													<div class="flex-grow-1 ms-3">
														<label class="d-flex text-font fw-bold m-0" id="terminacion_modalCambiopago">Terminación <?= $tarjeta['card']['lastFourDigits'] ?></label>
														<label class="d-flex text-font2 m-0" id="banco_modalCambiopago"> Tarjeta de <?=str_replace('e', 'é', $tarjeta['card']['type'])?>o</label>
														<label class="d-flex text-font2 m-0" id="vencimiento_modalCambiopago">Vencimiento: <?= $tarjeta['card']['expMonth'] . '/' . $tarjeta['card']['expYear'] ?></label>
													</div>
													<div class="flex-grow-1 ms-3">
														<a class="text-font justify-content-center user-select-none text-decoration-none text-naranja opciones-ingresar" id="tarj_modalCambiopago" datos="<?= $tarjeta['card']['token'] ?>">Continuar <i class="fas fa-chevron-right"></i></a>
													</div>
												</div>
											</div>
										</div>
										<div class="d-flex col-12 justify-content-end">
											<a class="text-font2 opciones-ingresar" data-bs-toggle="modal" data-bs-target="#cambiar_tarjetaCambiopago">Cambiar tarjeta</a>
										</div>
									<?php endif; ?>
								<?php endforeach; ?>
							<?php endif; ?>
							<?php if($metodoPago != 'efectivo') :?>
								<div class="col-lg-12 my-2">
									<label class="card p-2 contenedor_checks" for="metodo_tiendaCambiopago">
										<input class="check-otrosPagosCambiopago" type="radio" name="Cambiopago" hidden id="metodo_tiendaCambiopago" value="efectivo">
										<div class="d-flex align-items-center">
											<div class="flex-shrink-0">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 20 20" style="color: #3483FA;">
													<path fill="currentColor" fill-rule="nonzero" d="M19.002 4.004v11.981h-18V4.004h18zm-1.2 1.2h-15.6v9.581h15.6V5.204zm-7.8 2.354a2.436 2.436 0 110 4.873 2.436 2.436 0 110-4.873zm0 1.2a1.236 1.236 0 10-.001 2.473 1.236 1.236 0 00.001-2.473zm4.806.03a1.206 1.206 0 11-.002 2.41 1.206 1.206 0 01.002-2.409zm-9.614 0a1.206 1.206 0 110 2.41 1.206 1.206 0 010-2.409z"></path>
												</svg>
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="color: #3483FA;">
													<path fill="currentColor" fill-rule="evenodd" d="M11.999.632l9.751 5.67v3.447l-2.244.001v7.52h1.24l2.24 5.265H1.164l2.071-5.265h1.267V9.766l-2.253.004V6.302l9.749-5.67zm7.755 18.138H4.258l-.891 2.265h17.35l-.963-2.265zm-1.748-1.5V9.751l-5.257.006v7.511h5.257zm-6.757-7.51l-5.246.006v7.503h5.246V9.76zm.75-7.392L3.75 7.165v1.104l16.5-.018V7.165l-8.251-4.797z"></path>
												</svg>
											</div>
											<div class="flex-grow-1 ms-3">
												<p class="d-flex text-font fw-bold m-0">Efectivo</p>
											</div>
										</div>
									</label>
								</div>
							<?php endif ;?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="cambiar_tarjetaCambiopago" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="tituloCambioPago_tarjeta" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0">
					<h5 class="modal-title fw-bold" id="tituloCambioPago_tarjeta">Seleccione tarjeta</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#cambiar_metodo_pago"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row align-items-center text-center">
							<?php foreach ($tarjetas as $key => $tarjeta) : ?>
								<div class="col-lg-12 my-2" tarjeta="<?= $tarjeta['card']['token'] ?>" id="contenedor-tarjetaCambiopago-<?= $tarjeta['card']['token'] ?>">
									<label class="card p-1 contenedor_checks" for="<?= $tarjeta['card']['token'] ?>">
										<input class="check-tarjetasCambiopago" type="radio" name="tarjetas_usuarios_radioCambiopago" hidden id="<?= $tarjeta['card']['token'] ?>">
										<div class="d-flex align-items-center">
											<div class="d-flex flex-shrink-0">
												<img class="ms-3" src="<?= model('autenticacion')->urlImgBanco($tarjeta['card']['brand']) ?>" alt="Procesador de pago bancario">
											</div>
											<div class="flex-grow-1 ms-3">
												<p class="d-flex text-font fw-bold m-0">Terminación <?= $tarjeta['card']['lastFourDigits'] ?></p>
												<p class="d-flex text-font2 m-0"> Tarjeta de <?=str_replace('e', 'é', $tarjeta['card']['type'])?>o</p>
												<p class="d-flex text-font2 m-0">Vencimiento: <?= $tarjeta['card']['expMonth'] . '/' . $tarjeta['card']['expYear'] ?></p>
											</div>
										</div>
									</label>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif ;?>
<div class="modal fade" id="cambiar_tarjeta" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0">
				<h5 class="modal-title fw-bold" id="titulo">Seleccione tarjeta</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#obtener_membresia"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body" >
				<div class="container">
					<div class="row align-items-center text-center gap-2" id="listado-tarjetas" style="max-height: 450px; overflow: auto;">
						<?php if(empty($tarjetas)) :?>
							<?= vacio('No tienes tarjetas disponibles')?>
						<?php else :?>
							<?php foreach ($tarjetas as $key => $tarjeta) : ?>
								<div class="col-lg-12 sel-tar" tarjeta="<?= $tarjeta['card']['token'] ?>">
									<label class="card p-1 contenedor_checks" for="<?= $tarjeta['card']['token'] ?>-mem">
										<input class="check-tarjetas" type="radio" name="tarjetas_usuarios_radio" hidden id="<?= $tarjeta['card']['token'] ?>-mem">
										<div class="d-flex align-items-center">
											<div class="d-flex flex-shrink-0">
												<img class="ms-3" src="<?= model('autenticacion')->urlImgBanco($tarjeta['card']['brand']) ?>" alt="Procesador de pago bancario">
											</div>
											<div class="flex-grow-1 ms-3">
												<p class="d-flex text-font fw-bold m-0">Terminación <?= $tarjeta['card']['lastFourDigits'] ?></p>
												<p class="d-flex text-font2 m-0"> Tarjeta de <?=str_replace('e', 'é', $tarjeta['card']['type'])?>o</p>
												<p class="d-flex text-font2 m-0">Vencimiento: <?= $tarjeta['card']['expMonth'] . '/' . $tarjeta['card']['expYear'] ?></p>
											</div>
										</div>
									</label>
								</div>
							<?php endforeach; ?>
						<?php endif ;?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('.check-tarjetas-mem').change(function(e) {
		alert($(this));
		$(this).parent().addClass('active')
		$('.check-tarjetas-mem').prop('checked', false).parent().removeClass('active');
		$tarjeta = $(this).siblings().children();
		$('#imagen_modal').attr('src', $tarjeta.children('img').attr('src'));
		$('#terminacion_modal').text($tarjeta.children('p:nth-child(1)').text());
		$('#banco_modal').text($tarjeta.children('p:nth-child(2)').text());
		$('#vencimiento_modal').text($tarjeta.children('p:nth-child(3)').text());
	
		$('#tarj_modal').attr('datos', $(this).parent().attr('for'));
	
		$('#cambiar_tarjeta').modal('hide');
		$('#obtener_membresia').modal('show');
	});
</script>
<!-- Resumen de suscripcion con tarjeta -->
<div class="modal fade" id="tarjetaResumen_modal" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0">
				<h5 class="modal-title fw-bold">Resumen de compra con tarjeta</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#obtener_membresia"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body pt-0">
				<div class="container">
					<div class="row align-items-center ">
						<div class="col-12 mt-3 info-detalles-compra" id="suscripcion_seleccionada_tarjeta"></div>
						<div class="seccion-cupon my-2">
							<?= view('front/viewcells/boton-cupon') ?>
						</div>
						<div class="d-flex justify-content-end gap-2">
							<div class="contenedor_cvv">
								<input class="border border-secondary text-font numerico" id="cvv2" name="cvv2" type="password" placeholder="cvv" maxlength="4" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
								<img src="<?=base_url('assets/images/cvv.png')?>" alt="codigo de seguridad tarjeta">
							</div>
						</div>
						<div class="col-12 text-end mt-3">
							<a class="btn btn-naranja" id="btn_confirm_tarjeta">Confirmar compra</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Resumen de suscripcion con efectivo -->
<div class="modal fade pagos" id="efectivo_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo_efectivo" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0 pb-0">
				<h5 class="modal-title fw-bold" id="titulo_efectivo">Resumen de compra con efectivo</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#obtener_membresia"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body pt-0">
				<div class="container">
					<div class="row align-items-center ">
						<h6 class="text-center fw-normal fst-italic"></h6>
						<div class="col-12 mt-3 info-detalles-compra" id="suscripcion_seleccionada_efectivo"></div>
						<div class="seccion-cupon">
							<?= view('front/viewcells/boton-cupon') ?>
						</div>
						<div class="col-12 text-end mt-3">
							<a class="btn btn-naranja" id="btn_confirm_efectivo">Confirmar compra</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Respuesta de pago de suscripción -->
<div class="modal fade" id="respuesta_pago" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0">
				<h5 class="modal-title fw-bold" id="titulo">¡Un paso más!</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#respuesta_pago" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row align-items-center text-center">
						<div class="col-12">
							<p class="h6 fw-normal mb-2">Número de referencia: <span class="fw-bold" id="referencia"></span>.</p>
							<label class="text-font mb-2">Te hemos enviado un correo con los pasos a seguir para poder proceder con la suscripción a tu cuenta. Guarda este número de referencia, lo necesitarás para pagar tu plan.</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if($freelancer['cuenta'] == 'predeterminado') :?>
	<!-- Actualizar contraseña -->
	<div class="modal fade" id="actualizar_contraseña_modal" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="actualizar_contra" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0">
					<h5 class="modal-title fw-bold" id="actualizar_contra">Actualizar contraseña</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<form class="modal-body needs-validation" novalidate encytpe="multipart/form-data" id="form_actualizar_contrasena">
					<div class="form-floating mb-2">
						<input type="password" class="form-control" id="contrasena_actual" name="contrasena_actual" required>
						<label for="contrasena_actual">Contraseña actual</label>
						<div class="invalid-tooltip text-font2">Requerido</div>
					</div>
					<div class="form-floating mb-2">
						<input type="password" class="form-control" id="contrasena_nueva" name="contrasena_nueva" required>
						<label for="contrasena_nueva">Nueva contraseña</label>
						<div class="invalid-tooltip text-font2">Requerido</div>
					</div>
					<div class="form-floating mb-2">
						<input type="password" class="form-control" id="confirmar_contrasena" name="confirmar_contrasena" required>
						<label for="confirmar_contrasena">Confirmar contraseña</label>
						<div class="invalid-tooltip text-font2">Requerido</div>
					</div>
					<button class="btn btn-naranja btn-border mt-3" type="button" id="btn_guardar_contrasena">Guardar</button>
				</form>
			</div>
		</div>
	</div>
<?php endif ;?>
<div class="modal fade" id="postular_a_proyecto" data-bs-backdrop="static" data-bs-keyboard="true" aria-hidden="true" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-0">
				<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#perfil" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
				<h5 class="modal-title h4">Postularme al proyecto</h5>
			</div>
			<div class="modal-body py-0">
				<div class="row align-items-center text-center">
					<div class="col-md-5 p-4">
						<small class="text-font fst-italic">Tu perfil como lo verá el contratante</small>
						<?php $monto = model('ModelPlanes')->selectMax('monto')->where('cuenta', 'freelancer')->get()->getRowArray()['monto'];
							$con_suscripcion_premium = (model('Suscripciones')
							->builder()
							->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
							->where(['id_usuario' => session('id'), 'estatus' => 'Activo', 'monto' => $monto])
							->get()->getRowArray());
						?>
						<?= view_cell('App\Libraries\FuncionesSistema::cardPostulantes', ['id' => session('id'), 'postu' => 0, 'tipo' => 'talento', 'modal' => 0, 'es_premium' => !empty($con_suscripcion_premium) ? true : false]) ?>
					</div>
					<div class="col-md-7">
						<p class="text-font text-start p-2">
							Estás a punto de postularte al proyecto <strong id="titulo_proyecto_modal"></strong>, recuerda que mientras más completo este tu perfil, mayor es la posibilidad de ser escogido para desarrollar el proyecto, recuerda mantener una actitud cordial y respetuosa con cada miembro de nuestra plataforma. De parte del equipo Mextemps te deseamos la mejor de las suertes.
						</p>
						<a class="btn btn-naranja btn-border my-2" id="enviar_postulacion">Postularse</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	let _this = this;
	const respuesta_3ds = function(_this, referenceId) {
		console.log('referenceId: ' + referenceId);
		charges(referenceId);
	}

	const charges = function(referenceId) {
		console.log('charges: ' + referenceId);
		$('#referencia_cargo').val(referenceId);
	}
	
	const confirm = function(processorTransactionId) {
		console.log('confirm: ' + processorTransactionId);
		$.ajax({
			url: '<?= base_url('Pagos/confirmar_pago') ?>',
			type: 'POST',
			data: {transaccion: $('#token_transaccion').val(), procesador: processorTransactionId, usuario: <?=session('id')?>, plan : $('.plan_seleccionado').val()},
			success: function(data) {
				let respuesta = JSON.parse(data);
				$('#tarjetaResumen_modal').modal('hide');
				$('#btn_confirm_tarjeta').removeClass('disabled').children('span').remove();
				$('input[name="otros_pagos"]').prop('checked', false).parent().removeClass('active');
				$('#obtener_membresia .cerrar-modal').click();
				$('.modal').modal('hide');
				$('#listado-historial-pago').load('/usuarios/HistorialPagos');

				if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta);

				alertify.notify(respuesta.mensaje, respuesta.alerta);
				return setTimeout((e) => location.href = "/compra_realizada?token="+respuesta.datos.transactionTokenId, 3000);
			},
			error: function(data) {
				let respuesta = JSON.parse(data.responseText);
				$('#btn_confirm_tarjeta').removeAttr('disabled').children('span').remove();
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
			}
		});
	}

	const callbackProceed = function(_this, processorTransactionId) {
		alertify.notify('processorTransactionId: ' + processorTransactionId, 'custom');
		confirm(processorTransactionId);
	}

	//La generación del device fingerprint debe de estar al momento en que se visualiza el formulario de pago
	function generateDevice(callback) {
		deviceFingerPrint = NetPay.form.generateDeviceFingerPrint();
		callback();
	}
	
	function añadirTarjeta(id, tipo){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Pagos/get_tarjeta_user')?>',
			data:{id:id, tipo:tipo},
			success: function(data){
				let respuesta = JSON.parse(data);
				if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta);

				$('#selector').html(respuesta.selector);
				$('#listado-tarjetas').empty().append(respuesta.listado);
				
				if($('#card-mis-tarjetas').children('.empty-img').length > 0){ //SI NO HAY TARJETAS
					$('#card-mis-tarjetas').children('.empty-img').remove();
					$('#card-mis-tarjetas div').remove();
				} 
				return $('#card-mis-tarjetas').append(respuesta.mis_tarjetas);
			}, error: function(data){
				alertify.error('Ha surgido un error', 10);
			}
		});
	}

	$('#ocultar-cuenta').click(function(){
		let tipo = $(this).attr('data-estatus');
		if(tipo == 'ocultar'){
			alertify.confirm('ATENCIÓN', 'Al ocultar su perfil los contratantes no podrán encontrarlo durante las búsquedas de talentos para sus proyectos. ¿Está seguro de esto? podrá volver a desactivar esta opción cuando desee.', function(){
				$.ajax({
					type: 'POST',
					url: '<?=base_url('/usuarios/ocultarPerfil')?>',
					data:{tipo:'true'},
					success: function(data){
						let cont = JSON.parse(data);
						if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, 'advertencia');
						$('#ocultar-cuenta').html('Mostrar cuenta');
						// $('#ocultar-cuenta').attr('data-bs-content', 'Mostrar su perfil en busqueda de talentos.');
						$('#ocultar-cuenta').attr('data-estatus', 'mostrar');
						return alertify.notify(cont.mensaje, 'correcto');
					}, error: function(data){
						return alertify.error('A surgido error, comuniquese con el equipo de soporte.', 10);
					}
				});
			}, function(){}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'});
		}else{
			$.ajax({
				type: 'POST',
				url: '<?=base_url('/usuarios/ocultarPerfil')?>',
				data:{tipo:'false'},
				success: function(data){
					let cont = JSON.parse(data);
					if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, 'advertencia');
					$('#ocultar-cuenta').html('Ocultar cuenta');
					// $('#ocultar-cuenta').attr('data-bs-content', 'Ocultar su perfil en busqueda de talentos.');
					$('#ocultar-cuenta').attr('data-estatus', 'ocultar');
					return alertify.notify(cont.mensaje, 'correcto');
				}, error: function(data){
					return alertify.error('A surgido error, comuniquese con el equipo de soporte.', 10);
				}
			});
		}
	});

	$(document).ready(function() {
		let deviceFingerPrint;
		<?php $netpay = new NetPay()?>
		<?= $netpay->getModo() ? 'NetPay.setSandboxMode(true); netpay3ds.setSandboxMode(true);' : 'NetPay.setSandboxMode(false); netpay3ds.setSandboxMode(false);' ?>

		// SE OBTIENE REFERENCEID POR 3DS
		netpay3ds.init(() => netpay3ds.config(_this, 100, respuesta_3ds));

		const $imagen = document.querySelector("#contenedor-qr"),
			$boton = document.querySelector("#btnDescargar");
		new QRious({
			element: $imagen,
			value: "<?=base_url('usuarios/prevista/'.base64_encode($freelancer['id']))?>", // La URL o el texto
			size: 150,
			backgroundAlpha: 0, // 0 para fondo transparente
			foreground: "#003c71", // Color del QR
			level: "H", // Puede ser L,M,Q y H (L es el de menor nivel, H el mayor)
		});
		// 	$('#btnDescargar').clone().prependTo('#btnDescargar-movil');
		$boton.onclick = () => {
			const enlace = document.createElement("a");
			enlace.href = $imagen.src;
			enlace.download = "Visita mi perfil";
			enlace.click();
		}
		$('#contenedor-qr').clone().prependTo('#contenedor-qr-movil');
		<?php if($cargar_cv){ ?>
			$('#sec-cv').load('<?=base_url('/usuarios/mostrararchivo/cv')?>');
		<?php } ?>
		<?php if($añadir_portafolio){ ?>
			$('#sec-portafolio').load('<?=base_url('/usuarios/mostrararchivo/portafolio')?>');
		<?php } ?>
		
		let swiper = new Swiper(".mySwiper", {
			slidesPerView: 2,
			spaceBetween: 30,
			loop: true,
		});
		
		$('#fechaFin').prop('disabled', true);
		$('#telefono').mask('999 999 99 99');
		$('#select-areas-div').hide();
		$('input[name="tipo"]').change(function(){
			if($(this).attr('id') != 'tipo-habilidad') return $('#select-areas-div').hide();
			
			if($(this).is(':checked')) $('#select-areas-div').show();
			else $('#select-areas-div').hide();
		});
		$('#fechaInicio').change((event) => {
			const element = event.target;
			$('#fechaFin').prop('disabled', false);
			if(element.value == '') $('#fechaFin').prop('disabled', true);
			else $('#fechaFin').attr('min', element.value).val(element.value);
		});
		$.ajax({
			type: 'POST',
			url: '<?=base_url('Proyectos/mostrarProyectosEstatus')?>',
			data:{tipo:'espera'},
			success: function(data){
				$('#mis-proyectos').html(data);
			}, error: function(data){
				alertify.error('Ha surgido un error, comuníquese con el equipo de soporte.', 10);
			}
		});
	});
	
	$('#env-solicitud').click(function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Administrador/saveSolicitudIdioma')?>',
			data: $('#form-sugerirIdioma').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, cont.tipo);
				$('.modal').modal('hide');
				return alertify.notify(cont.mensaje, cont.tipo);
			}, error(data){
				alertify.notify('Ha surgido un error, comuníquese con el equipo de soporte', 'falla');
			}
		});
	});
	
	$('.btn-listados').click(function(){
		$('.btn-listados').addClass('inactivo');
		$(this).removeClass('inactivo');
		$.ajax({
			type: 'POST',
			url: '<?=base_url('Proyectos/mostrarProyectosEstatus')?>',
			data:{tipo:$(this).attr('data-tipo')},
			success: function(data){
				$('#mis-proyectos').html(data);
			}, error: function(data){
				alertify.error('Ha surgido un error, comuníquese con el equipo de soporte.', 10);
			}
		});
	});
	
	$('.solicitar-factura').click(function(){
		let id = $(this).attr('data-id');
		$.ajax({
			type:'POST',
			url: '<?=base_url('/Pagos/datosFacturacionUsuario')?>',
			data:{id:id},
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, 'advertencia');
				$('#form-solicitud-factura').html(cont.mensaje);
				return $('#solicitar-factura').modal('show');
			}, error: function(data){
				let respuesta = JSON.parse(data.responseText);
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
			}
		});
	});
	
	$('#sugerir-idioma').click(function(){
		$('#agregar-idioma').modal('hide');
		$('#sug-idioma').modal('show');
	});
	
	$('#btn-addsugarea').click(function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Administrador/saveSolicitudArea')?>',
			data: $('#form-sug-area').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, cont.tipo);
				$('#form-sug-area')[0].reset();
				$('.modal').modal('hide');
				return alertify.notify(cont.mensaje, cont.tipo);
			}, error: function(data){
				alertify.error('A surgido un error, comuniquese con el area de soporte', 10);
			}
		});
	});
	
	$('#form-pagoHora, #form-pagoHora_movil').submit(function(evento) {
		evento.preventDefault();

		$form = $(this);
		let pago = $form.serialize().split('=');
		if(pago[1] == 0 || (pago[1] == '')) return true;

		$.ajax({
			url: '<?= base_url('usuarios/guardarPagoHora')?>',
			type: 'POST',
			data: $form.serialize(),
			success: function(data) {
				let respuesta = JSON.parse(data);
				$form.removeClass('was-validated');
				return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
			},
			error: function(data) {
				let respuesta = JSON.parse(data.responseText);
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
			}
		});
	});
	
	$('#btn-area').click(function(event) {
		if(event.target.form.datalist_area[0].selected) return false;

		event.preventDefault();
		$(this).attr('disabled', true);
		$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);

		$areas = $('#form-areas');
		$.ajax({
			url: '<?= base_url('usuarios/guardarAreas')?>',
			type: 'POST',
			data: $areas.serialize(),
			success: function (data) {
				let respuesta = JSON.parse(data);
				$('#btn-area').removeAttr('disabled').children('span').remove();
				if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				
				$areas[0].reset();
				$('#datalist_area').val('').trigger('change');
				$areas.removeClass('was-validated');
				
				$('#side-areas').load('<?=base_url('usuarios/areasUsuario/'.session('id'))?>');
				alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
			},
			error: function (data) {
				let respuesta = JSON.parse(data.responseText);
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
			}
		});
	});
	
	$('#eliminar_experiencias').click(function(evento) {
		$this = $(this);
		evento.preventDefault();
		$(this).attr('disabled', true);
		$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);
		if (alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro de eliminar todas las áreas de experiencia?', function() {
			$.ajax({
				url: '<?= base_url('usuarios/eliminarTodasAreas')?>',
				type: 'POST',
				data: {},
				success: function(data) {
					let respuesta = JSON.parse(data);
					if (respuesta.alerta != 'correcto'){
						$($this).removeAttr('disabled').children('span').remove();
						return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					}

					$('#side-areas').load('<?=base_url('usuarios/areasUsuario/'.session('id'))?>');
					$('#form-areas').next().remove();
					$($this).remove();
					
				},
				error: function(data) {
					let respuesta = JSON.parse(data.responseText);
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
				}
			});
		}, function() {
			$($this).removeAttr('disabled').children('span').remove();
		}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
	});

	$('#btn-agregarTarjeta').click(function(evento) {
		if (evento.currentTarget.form.nombrePay.value.length == 0 || evento.currentTarget.form.tarjetaPay.value.length == 0 ||
			evento.currentTarget.form.mesPay.value.length == 0 || evento.currentTarget.form.cvvPay.value.length == 0) return true;

		evento.preventDefault();
		$(this).attr('disabled', true);
		$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);

		generateDevice(function() { 
			let tarjeta = {
				cardNumber: evento.currentTarget.form.tarjetaPay.value,
				expMonth: evento.currentTarget.form.mesPay.value,
				expYear: evento.currentTarget.form.añoPay.value,
				cvv2: evento.currentTarget.form.cvvPay.value,
				vault: true,
				simpleUse:false,
				deviceFingerPrint : deviceFingerPrint
			};
			NetPay.setApiKey('<?= $netpay->getLlave() ?>');
			var validateNumberLength = NetPay.card.validateNumberLength(tarjeta.cardNumber);
			var validateNumber = NetPay.card.validateNumber(tarjeta.cardNumber);
			var validateExpiry = NetPay.card.validateExpiry(tarjeta.expMonth, tarjeta.expYear);
			var validateCVV = NetPay.card.validateCVV(tarjeta.cvv2, tarjeta.cardNumber);

			if (!validateNumberLength || !validateNumber){
				$('#btn-agregarTarjeta').removeAttr('disabled').children('span').remove();
				return alertify.notify('Número de tarjeta incorrecto', 'advertencia', 10);
			} 
			if (!validateExpiry){
				('#btn-agregarTarjeta').removeAttr('disabled').children('span').remove();
				return alertify.notify('Expiración de tarjeta incorrecto', 'advertencia', 10);
			}
			if (!validateCVV){
				$('#btn-agregarTarjeta').removeAttr('disabled').children('span').remove();
				return alertify.notify('Código de seguridad incorrecto', 'advertencia', 10);
			}

			NetPay.token.create(tarjeta, correcto, falla);

			function correcto(respuesta) {
				console.log("Token creado correctamente");
				let datos_token = JSON.parse(respuesta.message.data);
				console.log(datos_token);
				$('#token_id').val(datos_token.token);
				$('#deviceFingerPrint').val(tarjeta.deviceFingerPrint);
				$.ajax({
					url: '<?= base_url('Pagos/guardarTarjetas') ?>',
					type: 'POST',
					data: $('#netpay-form').serialize(),
					success: function(data) {
						let respuesta = JSON.parse(data);
						$('#btn-agregarTarjeta').removeAttr('disabled').children('span').remove();

						if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
						$('#agregar-pago').modal('hide');
						$('#netpay-form')[0].reset();
						if($('#flujo_PagoTarjeta').is('[agregar]')){
							añadirTarjeta(respuesta.tarjeta);
							$('#obtener_membresia').modal('show');
							$('#flujo_PagoTarjeta').removeAttr('agregar');
						}else setTimeout(function(){ location.reload() }, 2000);
						
						return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					},
					error: function(data) {
						let respuesta = JSON.parse(data.responseText);
						alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
					}
				});
			}

			function falla(error) {
				console.trace(error);
				let mensaje = error.message == 'Empty or invalid card number' ? 'Número de tarjeta inválido' : error.message;
				alertify.notify("ERROR [" + mensaje + "]", 'falla', 10);
				$('#btn-agregarTarjeta').removeAttr('disabled').children('span').remove();
			}
		});
	});
	
	$('#btn_confirm_tarjeta').click(function(){
		let cvv2 = $('#cvv2').val().trim()
		if(cvv2.length == 0) return alertify.notify('Ingrese el código de seguridad de su tarjeta', 'custom');

		$this = $(this);
		generateDevice(() => {$('#deviceFingerPrint').val(deviceFingerPrint)});

		$(this).addClass('disabled');
		$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);
		if (alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro de realizar tu compra?', function() {
			$.ajax({
				url: '/Pagos/pagar/<?=session('id')?>',
				type: 'POST',
				data: {
					cvv : cvv2,
					deviceFingerPrint : $('#deviceFingerPrint').val(),
					referencia_cargo : $('#referencia_cargo').val(),
					plan : $('.plan_seleccionado').val(),
					datos : $this.attr('datos'),
					cupon: $('#tarjetaResumen_modal .cupon_ingresado').val()
				},
				success: function(data) {
					let respuesta = JSON.parse(data);
					$('#tarjetaResumen_modal').modal('hide');
					$($this).removeClass('disabled').children('span').remove();
					$('input[name="otros_pagos"]').prop('checked', false).parent().removeClass('active');
					$('#obtener_membresia .cerrar-modal').click();
					$('.modal').modal('hide');
					$('#listado-historial-pago').load('/usuarios/HistorialPagos');

					if (respuesta.alerta == 'custom'){
						$('#token_transaccion').val(respuesta.datos.transactionTokenId);
						let canProceed = netpay3ds.canProceed(respuesta.datos.status, respuesta.datos.threeDSecureResponse.responseCode, respuesta.datos.threeDSecureResponse.acsUrl);
						console.log("canProceed es: " + canProceed);
						
						if (!canProceed) return alertify.notify('Transacción rechadaza', 'falla');
						
						let proceed = netpay3ds.proceed(_this, respuesta.datos.threeDSecureResponse.acsUrl, respuesta.datos.threeDSecureResponse.paReq, respuesta.datos.threeDSecureResponse.authenticationTransactionID, callbackProceed);
					}
					
					alertify.notify(respuesta.mensaje, respuesta.alerta);
					return setTimeout((e) => location.href = "/compra_realizada?token="+respuesta.datos.transactionTokenId, 3000);
				},
				error: function(data) {
					let respuesta = JSON.parse(data.responseText);
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
				}
			});
		}, function() { 
			$($this).removeClass('disabled').children('span').remove();
		}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
	});
	
	$('#card-mis-tarjetas').delegate('.eliminar-tarjeta', 'click', function() {
		let dato = $(this).attr('datos');
		let tarjeta = $(this).attr('tarjeta');
		let btnEliminar = $(this);
		if (alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro de eliminar tarjeta con terminación <b class="fw-bold">' + tarjeta + '</b>?', function() {
			$.ajax({
				url: '<?= base_url('Pagos/eliminarTarjeta') ?>',
				type: 'POST',
				data: { id: btnEliminar.attr('datos'), usuario : <?=session('id')?> },
				success: function(data) {
					let respuesta = JSON.parse(data);
					if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					//Eliminamos tarjetas en Obtener Membresia, Cambiar Pago y al elegir
					btnEliminar.parents('.contenedor-tarjeta').remove();
					$('.sel-tar[tarjeta="'+dato+'"]').remove();
					$('#contenedor-tarjeta-'+btnEliminar.attr('datos')).remove();
					$('#contenedor-tarjetaCambiopago-'+btnEliminar.attr('datos')).remove();
					$('#contenedor-tarjetaObtenerSubs-'+btnEliminar.attr('datos')).remove();
					return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				},
				error: function(data) {
					let respuesta = JSON.parse(data.responseText);
					if(respuesta.message == 'Can not delete, It is used by Subscription'){
						respuesta.message = 'OPPS... No puedo eliminar esta tarjeta por que es la que tienes activa en tu suscripción, primero da de alta otra tarjeta, realiza el cambio y ahora sí puedo eliminar esta tarjeta.'
						return alertify.notify('Error: ' + respuesta.message, 'advertencia', 0);
					}
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
				}
			});
		}, function(){}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
	});
	
	$('#flujo_PagoTarjeta').click(function(){
		$(this).attr('agregar', true);
		$('#obtener_membresia').modal('hide');
		$('#agregar-pago').modal('show');
	});
	
	$('#datalist_area').change(function(e) {
		let area = $(this).val();
		$.ajax({
			type: 'POST',
			url: '<?=base_url('Habilidades/subareas')?>',
			data:{area:area},
			success: function(data){
				$('#datalist_subareas').html(data);
			}, error: function(data){
				let respuesta = JSON.parse(data.responseText);
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
			}
		});
	});
	
	$('#actualizar-perfil').click(function(e) {
		if(e.target.form.calle.value.length == 0 || e.target.form.colonia.value.length == 0 ||
		e.target.form.municipio.value.length == 0 || e.target.form.estado.value.length == 0 ||
		e.target.form.codigo_postal.value.length == 0) return true;

		e.preventDefault();
		let form = new FormData(document.getElementById("form-editarPerfil"));
		$.ajax({
			url: '<?= base_url('usuarios/editarPerfil')?>',
			type: 'POST',
			contentType: false,
			processData: false,
			data: form,
			success: function (data) {
				let respuesta = JSON.parse(data);
				if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);

				$('#form-editarPerfil').removeClass('was-validated');
				
				$('#contenedor_redes_sociales').empty();
				$('#nombre_freelancer, #nombre_freelancer_aside').text(e.target.form.nombre.value+' '+e.target.form.apellidos.value);
				if(e.target.form.facebook.value.length != 0) $('#contenedor_redes_sociales').append(`<span style="padding-left: 10px;" class="lh-sm"><a class="text-azul" href="https://www.facebook.com/`+e.target.form.facebook.value+`" target="_blank"><i class="fab fa-facebook-f" aria-hidden="true" style="float:none;"></i> `+e.target.form.facebook.value+`</a></span>`);
				if(e.target.form.instagram.value.length != 0) $('#contenedor_redes_sociales').append(`<span style="padding-left: 10px;" class="lh-1"><a style="color: #df0041;" href="https://www.instagram.com/`+e.target.form.instagram.value+`" target="_blank"><i class="fab fa-instagram" aria-hidden="true" style="float:none;"></i> `+e.target.form.instagram.value+`</a></span>`);
				if(e.target.form.whatsapp.value.length != 0) $('#contenedor_redes_sociales').append(`<span style="padding-left: 10px;" class="text-small"><a class="text-success" href="https://wa.me/`+e.target.form.whatsapp.value+`" target="_blank"><i class="fab fa-whatsapp" aria-hidden="true" style="float:none;"></i> `+e.target.form.whatsapp.value+`</a></span>`);
				if(e.target.form.correo.value.length != 0) $('#contenedor_redes_sociales').append(`<span style="padding-left: 10px;" class="text-small"><a class="text-primary" href="mailto:`+e.target.form.correo.value+`" target="_blank"><i class="fas fa-at" aria-hidden="true" style="float:none;"></i> `+e.target.form.correo.value+`</a></span>`);
				<?php if($cargar_cv){ ?>
					$('#sec-cv').load('<?=base_url('/usuarios/mostrararchivo/cv')?>');
				<?php } ?>
				<?php if($añadir_portafolio){ ?>
					$('#sec-portafolio').load('<?=base_url('/usuarios/mostrararchivo/portafolio')?>');
				<?php } ?>
				return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
			},
			error: function(data){
				let respuesta = JSON.parse(data.responseText);
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
			}
		});
	});
	
	$('#btn_guardar_contrasena').click(function(e){
		if(e.target.form.contrasena_actual.value.length == 0 || e.target.form.contrasena_nueva.value.length == 0 || e.target.form.confirmar_contrasena.value.length == 0) return true;
		
		e.preventDefault();
		let p = document.getElementById('contrasena_nueva').value,
			errors = [];
		if (p.length < 8) {
			errors.push("La contraseña debe tener un mínimo de 8 caracteres.");
		}
		if (p.search(/[a-z]/i) < 0) {
			errors.push("La contraseña debe tener al menos una letra."); 
		}
		if(p.search(/[A-Z]/) < 0){
			errors.push("La contraseña debe tener al menos una letra mayúscula.");
		}
		if(p.search(/[!@#$%^&*.,?]/) < 0){
			errors.push("La contraseña debe tener al menos un caracter especial, como: (*, &, #, ?, !)");
		}
		if (p.search(/[0-9]/) < 0) {
			errors.push("La contraseña debe tener al menos un número.");
		}
		if(errors.length <= 0){
			console.log('Contraseña válida');
		}else{
			let cadena = '';
			errors.forEach(element => cadena = cadena + element + '<br>');
			return alertify.notify(cadena, 'advertencia', 20);
		}
		if(e.target.form.contrasena_nueva.value != e.target.form.confirmar_contrasena.value) return alertify.notify('Veo algo raro... Las contraseñas no coinciden.', 'advertencia');

		$this = $(this);
		$this.addClass('disabled');
		$this.add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo($this);
		$.ajax({
			type: 'POST',
			url: '<?=base_url('cambio_accesos')?>',
			data: $('#form_actualizar_contrasena').serialize(),
			success: function(data){
				let datos = JSON.parse(data);
				$this.removeClass('disabled').children('span').remove();
				if(datos.alerta != 'correcto') return alertify.notify(datos.mensaje, datos.alerta);
				
				$('#form_actualizar_contrasena')[0].reset();
				// $('#actualizar_contraseña_modal').modal('hide');
				$('.modal').modal('hide');
				$('.modal-backdrop.fade.show').remove();
				return alertify.notify(datos.mensaje, datos.alerta);
			}, error: function(error){
				alertify.notify('Error: '+error, 'falla', 0)
			}
		});
	});
	
	$('#baja_cuenta').click(function(evento){
		$this = $(this);
		evento.preventDefault();
		$($this).addClass('disabled');
		$($this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo($this);
		$.ajax({
			url: '<?=base_url('usuarios/eliminar_cuenta')?>',
			type: 'POST',
			data: {},
			success: function (data) {
				let respuesta = JSON.parse(data);
				$($this).removeClass('disabled').children('span').remove();
				if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				$('#eliminar_cuenta_modal').modal('hide');
				alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				setTimeout(function(){ location.reload() }, 2000);
			},
			error: function(data){
				let respuesta = JSON.parse(data.responseText);
				$($this).removeClass('disabled').children('span').remove();
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
			}
		});
	});
	
	$('#recuperar_cuenta').click(function(evento){
		$this = $(this);
		evento.preventDefault();
		$($this).addClass('disabled');
		$($this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo($this);
		$.ajax({
			url: '<?=base_url('usuarios/recuperar_cuenta')?>',
			type: 'POST',
			contentType: false,
			processData: false,
			data: {},
			success: function (data) {
				let respuesta = JSON.parse(data);
				$($this).removeClass('disabled').children('span').remove();
				if(respuesta.alerta == 'falla') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				$('#recuperar_cuenta_modal').modal('hide');
				alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				setTimeout(function(){ location.reload() }, 2000);
			},
			error: function(data){
				let respuesta = JSON.parse(data.responseText);
				$($this).removeClass('disabled').children('span').remove();
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
			}
		});
	});
	
	$('.check-otrosPagos').change(function(e) {
		$this = $(this);
		$($this).parent().addClass('active');
		$('.check-otrosPagos').not(':checked').parent().removeClass('active');

		$.ajax({
			url: '<?= base_url('Suscripciones/getPlan')?>',
			type: 'POST',
			data: {id : $('.plan_seleccionado').val(), es_tarjeta: false},
			success: function (data) {
				let respuesta = JSON.parse(data);
				if(respuesta.alerta == 'falla') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				$('#suscripcion_seleccionada_'+$($this).val()).html(respuesta.mensaje);
				
				$('#obtener_membresia').modal('hide');
				return $('#' + $($this).val() + '_modal').modal('show');
			},
			error: function(data){
				alertify.notify('Network', 'falla');
			}
		});
		$('#obtener_membresia').modal('hide');
		$('#' + $(this).val() + '_modal').modal('show');
	});
	
	$('#btn-historial').click(function(e) {
		if(e.target.form.historial.value.length == 0) return true;
		e.preventDefault();
		$this = $(this);
		$($this).attr('disabled', true);
		$($this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo($this);
		
		$.ajax({
			url: '<?= base_url('usuarios/guardarHistorial')?>',
			type: 'POST',
			data: $('#form-historial').serialize(),
			success: function (data) {
				let respuesta = JSON.parse(data);
				$($this).removeAttr('disabled').children('span').remove();
				$('#sec-hist').html($('#historial').val());
				return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
			},
			error: function (data) {
				let respuesta = JSON.parse(data.responseText);
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
			}
		});
	});
	
	$('#btn-experiencia').click(function(evento) {
		if(evento.currentTarget.form.titulo.value.length == 0 || evento.currentTarget.form.fechaInicio.value.length == 0 ||
			evento.currentTarget.form.fechaFin.value.length == 0 || evento.currentTarget.form.descripcion.value.length == 0) return true;

		evento.preventDefault();
		if (evento.currentTarget.form.fechaInicio.value >= evento.currentTarget.form.fechaFin.value) return alertify.notify('HEEY... Algo anda mal, verifica las fechas.', 'advertencia');
		
		$.ajax({
			url: '<?= base_url('Autenticacion/guardarExperiencia')?>',
			type: 'POST',
			data: $('#form-experiencia').serialize(),
			success: function (data) {
				let respuesta = JSON.parse(data);
				$('#form-experiencia')[0].reset();
				if(respuesta.alerta != 'correcto'){
					$('#form-experiencia').removeClass('was-validated');
					return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				} 
				$('#sec-experiencias').load('<?=base_url('usuarios/experienciasUsuario/'.session('id'))?>');
				$('#fechaFin').prop('disabled', true);
				$('#form-experiencia').removeClass('was-validated');
				return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
			},
			error: function (data) {
				let respuesta = JSON.parse(data.responseText);
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
			}
		});
	});
	
	$('#btn-nuevoIdioma').click(function(evento) {
		evento.preventDefault();
		$.ajax({
			url: '<?= base_url('usuarios/guardarIdiomas')?>',
			type: 'POST',
			data: $('#form-nuevoIdioma').serialize(),
			success: function (data) {
				let respuesta = JSON.parse(data);
				if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);

				let indice;
				if(Object.keys(respuesta.id).length-1 > 0) { indice = Object.keys(respuesta.id).length-1;}
				else { indice = 0;}
				
				let checked_n = '', checked_e = '', checked_v = '';
				switch(respuesta.datos['nivel']){
					case 'nativo': checked_n = 'checked';
					case 'estudiado': checked_e = 'checked';
					case 'viviendo': checked_v = 'checked';
					break;
				}
				$('#agregar-idioma').modal('hide');
				$('#form-nuevoIdioma')[0].reset();
				$('#text-rango').text("0%");
				$('#contenido-idiomas').append(`<li class="idioma`+indice+`"><a data-bs-toggle="modal" data-bs-target="#editar-idioma`+indice+`"><img class="img-fluid rounded-circle img-idiomas" src="`+respuesta.datos['imagen']+`" alt=""><span class="position-absolute bg-light border border-light rounded-circle bottom-0 end-0 p-1" style="margin-right: -10px; margin-bottom: -5px;"><label class="text-font2 m-0">`+respuesta.datos['rango']*5+`%</label></span></a>`);
				$('#contenido_editar_idiomas').append(`<div class="modal fade" id="editar-idioma`+indice+`" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered">
						<div class="modal-content">
							<div class="modal-header justify-content-center border-bottom-0">
								<h5 class="modal-title fw-bold">Idioma `+respuesta.id[indice]['idioma']+`</h5>
								<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#perfil" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
							</div>
							<div class="modal-body">
								<div class="container">
									<form class="row justify-content-center needs-validation" id="form-editarIdioma`+indice+`" enctype="multipart/form-data" novalidate>
										<div class="col-6 my-3">
											<input name="id" value="`+indice+`" hidden>
											<label for="rango" class="text-font">Dominio del Idioma</label>
											<input class="form-range edit-rango" type="range" max="20" datos="`+indice+`" name="rango" id="rango`+indice+`" value="`+respuesta.id[indice]['rango']+`">
											<label class="text-center" id="modal-text-rango`+indice+`">`+respuesta.id[indice]['rango']*5+`%</label>
										</div>
										<div class="col-6 my-3">
											<ul class="ps-0 text-start botones-lista">
												<li class="nav-link px-2">
													<label class="text-check text-font text-dark user-select-none position-relative" for="edit-nativo`+indice+`">
														<input class="invisible" type="radio" name="nivel" value="nativo" `+ checked_n+` id="edit-nativo`+indice+`" hidden>
														<span class="check"></span>
														Nativo
													</label>
												</li>
												<li class="nav-link px-2">
													<label class="text-check text-font text-dark user-select-none position-relative" for="edit-estudiado`+indice+`">
														<input class="invisible" type="radio" name="nivel" value="estudiado" `+ checked_e+` id="edit-estudiado`+indice+`"  hidden>
														<span class="check"></span>
														Estudiado
													</label>
												</li>
												<li class="nav-link px-2">
													<label class="text-check text-font text-dark user-select-none position-relative" for="edit-viviendo`+indice+`">
														<input class="invisible" type="radio" name="nivel" value="viviendo" `+ checked_v+` id="edit-viviendo`+indice+`"  hidden>
														<span class="check"></span>
														Viviendo en el país
													</label>
												</li>
											</ul>
										</div>
										<div class="col-12 text-center my-3">
											<a class="btn btn-naranja btn-border text-font actualizar" datos="`+indice+`">Actualizar</a>
											<a class="btn btn-naranja btn-border text-font eliminar" datos="`+indice+`">Eliminar</a>
											<form id="form`+indice+`Delete">
												<input type="hidden" name="dato" value="`+indice+`">
											</form>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>`)
				alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				$('#perfil').modal('show');
			},
			error: function (data) {
				let respuesta = JSON.parse(data.responseText);
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
			}
		});
	});
	
	$('#contenido_editar_idiomas').delegate('a.actualizar, a.eliminar', 'click', function(evento) {
		evento.preventDefault();
		let idioma = evento.target.attributes.datos.value;
		if(evento.target.classList.contains('actualizar')){
			$.ajax({
				url: '<?=base_url('usuarios/editarIdioma')?>',
				type: 'POST',
				data: $('#form-editarIdioma'+idioma).serialize(),
				success: function (data) {
					let respuesta = JSON.parse(data);
					if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
	
					$('#editar-idioma'+idioma).modal('hide');
					$('#contenido-idiomas #text-rango'+idioma).text(5*respuesta.rango+"%");
					return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				},
				error: function (data) {
					let respuesta = JSON.parse(data.responseText);
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
				}
			});
		}else{
			if (alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro de eliminar el idioma?', function() {
				$.ajax({
					url: '<?=base_url('usuarios/eliminarIdioma?id=')?>'+idioma,
					type: 'GET',
					data: {},
					success: function (data) {
						let respuesta = JSON.parse(data);
						if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
						$('#editar-idioma'+idioma).modal('hide').remove();
						$('#contenido-idiomas .idioma'+idioma).remove();
						return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					},
					error: function (data) {
						let respuesta = JSON.parse(data.responseText);
						alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
					}
				});
			}, function(){}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
		}
	});
	
	$('#agregar-tarjeta').click(function(){
		$.ajax({
			type: 'POST',
			url: '<?= base_url('/Autenticacion/validarDireccion') ?>',
			data:{d:1},
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, cont.tipo);
				$('#obtener_membresia').modal('hide');
				$('#agregar-pago').modal('show');
			}, error: function(){
				alertify.error('Lo sentimos, no pudimos procesar la solicitud, intentelo de nuevo., 10')
			}
		});
	});
	
	$('button[data-bs-target="#postular_a_proyecto"]').click(function(){
		$('#titulo_proyecto_modal').text($(this).attr('dato_titulo_proyecto'));
		$('#enviar_postulacion').attr('href', '<?=base_url('/postularme').'/'?>'+$(this).attr('dato_proyecto'));
	});
</script>
<?= $this->endSection() ?>