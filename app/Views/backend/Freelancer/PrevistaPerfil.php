<?= $this->extend('front/main') ?>
<?= $this->section('title') ?>
	Resumen Freelance
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<style>
	.sidebar-team-member .sidebar-list-member.sidebar-list-follower ul li a img {
		object-fit: cover;
	}
</style>
<script src="https://unpkg.com/qrious@4.0.2/dist/qrious.js"></script>
<script src="https://code.iconify.design/3/3.0.0/iconify.min.js"></script>
<main class="main perfil-usuarios-freelancer">
	<section class="section-box mt-50">
		<div class="container">
			<div class="row">
				<!-- COLUMNA 1 -->
				<div class="col-lg-8 col-md-12 col-sm-12 col-12">
					<!-- DETALLE PERFIL -->
					<div class="box-head-single box-head-single-candidate top-perfil-resumen">
						<div class="container">
							<div class="heading-image-rd online">
								<a href="#">
									<figure><img class="gambar img-responsive img-thumbnail img-perfil shadow border border-3 m-auto" alt="mextemps" src="<?= imagenPerfil($usuario) ?>"></figure>
								</a>
							</div>
							<div class="heading-main-info">
								<h4><?= $usuario['nombre'] . ' ' . $usuario['apellidos'] ?></h4>
								<div class="head-info-profile">
									<div class="fila-uno">
										<!-- <span class="text-small mr-20"><i class="fi-rr-marker text-mutted"></i> Apodaca, Nuevo León</span> -->
										<span class="h4 mr-20">Freelancer experto en <strong><?=view_cell('App\Libraries\FuncionesSistema::obtenerHabilidad', ['id' => $usuario['id']])?></strong></span>
										<?=view_cell('App\Libraries\FuncionesSistema::verCalificacionPerfil', ['id' => $usuario['id']])?>
										<?php if($usuario['id'] == session('id') || ($acceso && autorizacion() == 'contratante')){ ?>
											<?php if(isset($usuario['contacto']['facebook']) && !empty($usuario['contacto']['facebook'])) :?>
												<a href="https://www.facebook.com/<?=$usuario['contacto']['facebook']?>" target="_blank">
													<span style="padding-left: 15px; color: #003c71;" class="text-small">
														<i class="fab fa-facebook-f px-1" aria-hidden="true"></i> <?= $usuario['contacto']['facebook']?>
													</span>
												</a>
											<?php endif ;?>
											<?php if(isset($usuario['contacto']['instagram']) && !empty($usuario['contacto']['instagram'])) :?>
												<a href="https://www.instagram.com/<?=$usuario['contacto']['instagram']?>" target="_blank">
													<span style="padding-left: 15px; color: #df0041;" class="text-small">
														<i class="fab fa-instagram px-1" aria-hidden="true"></i> <?= $usuario['contacto']['instagram']?>
													</span>
												</a>
											<?php endif ;?>
											<?php if(isset($usuario['contacto']['whatsapp']) && !empty($usuario['contacto']['whatsapp'])) :?>
													<a href="https://wa.me/<?= $usuario['contacto']['whatsapp']?>" target="_blank">
														<span style="padding-left: 15px; color: #198754;" class="text-small">
															<i class="fab fa-whatsapp px-1" aria-hidden="true"></i> <?= $usuario['contacto']['whatsapp']?>
														</span>
													</a>
											<?php endif ;?>
											<?php if(isset($usuario['contacto']['correo']) && !empty($usuario['contacto']['correo'])) :?>
												<a href="mailto:<?=$usuario['contacto']['correo']?>" target="_blank">
													<span style="padding-left: 15px; color: --color-azul;" class="text-small">
														<i class="fas fa-at" aria-hidden="true"></i> <?= $usuario['contacto']['correo']?>
													</span>
												</a>
											<?php endif ;?>
										<?php } ?>
									</div>
									<div class="fila-dos">
										<span class="text-small sueldo-x-hora">Sueldo por hora: <span>$<?= $usuario['pagoHora'] ?>.00<small>MXN</small></span></span>
										<?php if($acceso){ ?>
											<?php if(autorizacion() == 'contratante' || $usuario['id'] == session('id')){ ?>
												<?php if($usuario['cv'] != null){ ?>
													<a href="<?=base_url().$usuario['cv']?>" class="btn btn-naranja cv-port" download>
														Descargar CV
													</a>
												<?php } ?>
												<?php if($usuario['portafolio'] != null){ ?>
													<a href="<?=base_url().$usuario['portafolio']?>" class="btn btn-naranja cv-port" download>
														Descargar Portafolio
													</a>
												<?php } ?>
											<?php } ?>
										<?php }else{ ?>
											<?php if(!session('logeado')){ ?>
												<a class="btn btn-naranja" id="alerta-ver">
													Contactar a freelancer
												</a>
											<?php }else{ ?>
												<?php if(session('rol') == 'contratante'){ ?>
													<a class="btn btn-naranja" id="alerta-ver">
														Contactar a freelancer
													</a>
												<?php } ?>
											<?php } ?>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- NIVEL DE EXPERIENCIA -->
					<div class="d-none d-lg-block">
						<div class="job-overview mt-40" id="card_pregunta_experiencia">
							<div class="row jusityf-content-center align-items-center">
								<div class="col-5 col-md-2 text-center que-insignia-info">
									<span class="d-inline-block m-0" style="width:60px; height:60px">
										<?= view_cell('App\Libraries\FuncionesSistema::miNivelInsignia', ['cantidad' => view_cell('App\Libraries\FuncionesSistema::cantProyectosFinalizados', ['id' => $usuario['id'], 'rol' => $usuario['rol']]), 'tipo' => 'icono']) ?>
									</span>
								</div>
								<div class="col-7 col-md-4 que-insignia-info">
									<h2 class="h4">¿Qué es el nivel de <strong>Experiencia</strong>?</h2>
								</div>
								<div class="col-md-6">
									<p style="text-align:justify;">
										<?= view_cell('App\Libraries\FuncionesSistema::miNivelInsignia', ['cantidad' => view_cell('App\Libraries\FuncionesSistema::cantProyectosFinalizados', ['id' => $usuario['id'], 'rol' => $usuario['rol']]), 'tipo' => 'descripcion']) ?>
									</p>
								</div>
							</div>
						</div>
					</div>
					<?php if((autorizacion() == 'contratante' || $usuario['id'] == session('id')) && (!empty($usuario['historial']))){ ?>
						<div class="content-single d-none d-md-block">
							<h5>Sobre mí</h5>
							<p>
								<?=$usuario['historial']?>
							</p>
						</div>
						<div class="content-single d-block d-md-none text-center">
							<h5>Sobre mí</h5>
							<p>
								<?=$usuario['historial']?>
							</p>
						</div>
					<?php } ?>
					<!-- MIS IDIOMAS -->
					<?php if (isset($usuario['idiomas'])) : ?>
						<div class="job-overview mt-40">
							<div class="row">
								<div class="col-md-12 que-idiomas-manejo">
									<!-- <p class="h4 text-description mb-10">Idiomas que domina</p> -->
									<h4 class="heading-border"><span>Idiomas que domino</span></h4>
									<div class="sidebar-team-member pt-0" style="border: 0px; margin: 0px; ">
										<div class="sidebar-list-member sidebar-list-follower" style="padding: 0px;">
											<ul class="d-flex gap-3 p-0 m-0">
												<?php foreach ($usuario['idiomas'] as $key => $idioma) : ?>
													<li class="m-0">
														<a>
															<label class="text-font idioma-porcentage" id="text-rango<?=$key?>"><?= $idioma['rango'] * 5 ?>%</label>
															<figure><img alt="mextemps" src="<?= $idioma['imagen'] ?>"></figure>
														</a>
													</li>
												<?php endforeach; ?>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
					<!-- MIS HABILIDADES -->
					<div class="job-overview mt-40">
						<div class="row">
							<div class="col-md-12 que-idiomas-manejo">
								<!-- <p class="h4 text-description mb-10">Áreas de experiencia</p> -->
								<h4 class="heading-border"><span>Mis áreas de experiencia</span></h4>
								<div class="block-tags">
									<?php if (isset($usuario['areas'])) : ?>
										<?php foreach ($usuario['areas'] as $key => $area) : ?>
											<?php if($area['subarea'] != null){ ?>
												<?php $subareas = json_decode($area['subarea'], true); ?>
												<?php foreach($subareas as $s){ ?>
													<a class="btn btn-tags-sm mb-10 mr-5"><?= $s ?></a>
												<?php } ?>
											<?php } ?>
										<?php endforeach ;?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<?php if (isset($usuario['experiencia'])) : ?>
						<!-- MIS EXPERIENCIAS -->
						<div class="job-overview mt-40 califica-frelancers">
							<h4 class="heading-border d-none d-md-block"><span>Mis Experiencias (<?=$usuario['cantidad_experiencias']?>)</span></h4>
							<h4 class="heading-border d-block d-md-none text-center"><span style="padding: 5px 10px 5px 10px;">Mis Experiencias (<?=$usuario['cantidad_experiencias']?>)</span></h4>
							<div class="list-recent-jobs">
								<div class="separador-experiencias-freelancer">
									<div id="contenedor_experiencias"></div>
									<div>
										<?php if($usuario['cantidad_experiencias'] > 3) :?>
											<span style="display: flex;">
												<button class="btn btn-calificaciones" id="ver_mas_experiencias" dato="3">Ver más experiencias</button>
											</span>
										<?php endif ;?>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
					<!-- CALIFICACIONES -->
					<div class="single-recent-jobs califica-frelancers">
						<?php $cantidad_calificaciones = view_cell('App\Libraries\FuncionesSistema::cantidadRegistros', ['tabla' => 'calificaciones', 'id' => $usuario['id'], 'campo' => 'id_freelancer']) ?>
						<h4 class="heading-border d-none d-md-block"><span></span></h4>
						<h4 class="heading-border d-block d-md-none text-center"><span style="padding: 5px 10px 5px 10px;">Calificaciones de clientes (<?= $cantidad_calificaciones?>)</span></h4>
						<div class="list-recent-jobs">
							<div class="card-job hover-up wow animate__animated animate__fadeInUp separador-comentarios-clientes">
								<?= view_cell('App\Libraries\FuncionesSistema::calificacionesUsuario', ['id' => $usuario['id']]) ?>
								<div>
									<?php if($cantidad_calificaciones > 3) :?>
										<span style="display: flex;">
											<button class="btn btn-calificaciones" id="ver_mas_calificaciones" dato="3">Ver más calificaciones</button>
										</span>
									<?php endif ;?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- COLUMNA 2 -->
				<div class="col-lg-4 col-md-12 col-sm-12 col-12 pl-40 pl-lg-15 mt-lg-30">
					<div class="sidebar-shadow actividad-freelancer">
						<h6 class="sidebar-title">Actividad</h6>
						<div class="info-address con-separador">
							<p class="left-info">Proyectos completados
								<span class="der-info centrados"><?=view_cell('App\Libraries\FuncionesSistema::cantProyectosFinalizados', ['id' => $usuario['id'], 'rol' => $usuario['rol']])?></span>
							</p>
							<p class="left-info">Proyectos postulados
								<span class="der-info centrados"><?=view_cell('App\Libraries\FuncionesSistema::cantPostulaciones', ['id' => $usuario['id'], 'rol' => $usuario['rol']])?></span>
							</p>
							<p class="left-info">Proyectos en curso
								<span class="der-info centrados"><?=view_cell('App\Libraries\FuncionesSistema::cantProyectosDesarrollo', ['id' => $usuario['id'], 'rol' => $usuario['rol']])?></span>
							</p>
						</div>
						<h6 class="small-heading">Información</h6>
						<div class="info-address">
							<p class="left-info">Proyectos calificados
								<span class="der-info centrados"><?= view_cell('App\Libraries\FuncionesSistema::cantidadRegistros', ['tabla' => 'calificaciones', 'id' => $usuario['id'], 'campo' => 'id_freelancer']) ?></span>
							</p>
							<p class="left-info">Clientes insatisfechos
								<span class="der-info centrados">
									<?=view_cell('App\Libraries\FuncionesSistema::clientesInsatisfechos', ['id' => $usuario['id']])?>
								</span>
							</p>
							<p class="left-info">Último login
								<span class="der-info">
									<?=view_cell('App\Libraries\FuncionesSistema::diferenciaFechas', ['fecha' => $usuario['ult_conexion']])?>
								</span>
							</p>
							<p class="left-info">Registrado
								<span class="der-info">
									<?= strftime('%d %B %Y', strtotime(explode(' ', $usuario['created_at'])[0])) ?>
								</span>
							</p>
						</div>
					</div>
					<div class="sidebar-shadow">
						<h6 class="sidebar-title">Nivel de Experiencia</h6>
						<div class="info-address d-grid text-center">
							<div class="d-inline-block">
								<span class="insignias-lsitado d-inline-block">
									<?= view_cell('App\Libraries\FuncionesSistema::misInsignias', ['cantidad' => view_cell('App\Libraries\FuncionesSistema::cantProyectosFinalizados', ['id' => $usuario['id'], 'rol' => $usuario['rol']])]) ?>
								</span>
							</div>
							<span style="font-size: 18px;">Nivel actual: <strong><?=view_cell('App\Libraries\FuncionesSistema::nivelInsignia', ['cantidad' => view_cell('App\Libraries\FuncionesSistema::cantProyectosFinalizados', ['id' => $usuario['id'], 'rol' => $usuario['rol']])])?></strong></span>
							 <?= view_cell('App\Libraries\FuncionesSistema::proyectosRestantesInsignia', ['id' => $usuario['id']]) ?> 
							<a class="btn btn-naranja" style="margin-top: 30px;" href="<?=base_url('/insignias')?>">
								Conoce más sobre insignias
							</a>
						</div>
					</div>
					<div class="d-lg-none" id="contenido_info_experiencia"></div>
					<div class="sidebar-shadow">
						<div class="text-description mt-15 text-center">
							<h6 class="sidebar-title" style="text-align: left;">Código de perfil</h6>
							<div class="row">
								<div class="col-md-6 col-sm-12 align-self-center">
									<img id="contenedor-qr" class="w-100 h-auto mw-150" alt="Compartir perfil QR">
								</div>
								<div class="col-md-6 col-sm-12 p-0 align-self-center">
									<button class="btn btn-naranja" id="btnDescargar">
										Descargar QR
									</button>
									<div class="align-items-center mt-3">
										<label class="fw-bold me-auto mb-2 w-100">Compartir en:</label>    
										<a class="rounded-circle iconos-compartir" href="https://www.facebook.com/sharer/sharer.php?u=<?=base_url('Usuarios/prevista/'.base64_encode($usuario['id']))?>" target="_blank">
											<span class="iconify" data-icon="akar-icons:facebook-fill" style="color: #0b5ed7; padding: 0px; border: 0px;" data-width="25" data-height="25"></span>
										</a>
										<a class="rounded-circle mx-2 iconos-compartir" href="https://wa.me/send?text=<?=base_url('Usuarios/prevista/'.base64_encode($usuario['id']))?>" target="_blank">
											<span class="iconify" data-icon="akar-icons:whatsapp-fill" style="color: #0b5ed7; padding: 0px; border: 0px;" data-width="25" data-height="25"></span>
										</a>
										<a class="rounded-circle iconos-compartir" href="https://www.linkedin.com/sharing/share-offsite/?url=<?=base_url('Usuarios/prevista/'.base64_encode($usuario['id']))?>" target="_blank">
											<span class="iconify" data-icon="akar-icons:linkedin-box-fill" style="color: #0b5ed7; padding: 0px; border: 0px;" data-width="25" data-height="25"></span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="sidebar-shadow d-none">
						<h6 class="sidebar-title">Buscar freelancers similares</h6>
						<div class="block-tags">
							<?php if (isset($usuario['areas'])) : ?>
								<?php foreach ($usuario['areas'] as $key => $area) : ?>
									<a href="#" class="btn btn-tags-sm mb-10 mr-5 habilidades-perfiles">
										<?=$area['area']?>
									</a>
								<?php endforeach ;?>
							<?php endif; ?>
							<!--<a href="#" class="btn btn-tags-sm mb-10 mr-5 habilidades-perfiles">Programador Web Wordpres</a>-->
							<!--<a href="#" class="btn btn-tags-sm mb-10 mr-5 habilidades-perfiles">Integrado de HTML 5</a>-->
							<!--<a href="#" class="btn btn-tags-sm mb-10 mr-5 habilidades-perfiles">Programador de Javascript</a>-->
							<!--<a href="#" class="btn btn-tags-sm mb-10 mr-5 habilidades-perfiles">Diseñador Gráfico</a>-->
							<!-- <a class="btn btn-enlace" href="#">Encuentra más freelancers</a> -->
							<button class="btn btn-naranja" id="btnDescargar" style="margin-top: 30px;">
								Encuentra más freelancers
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<div class="modal fade" id="modaldetalle" tabindex="-1" aria-labelledby="modaldetalleTitulo" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0">
				<h5 class="modal-title fw-bold" id="modaldetalleTitulo"></h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body">
				<div class="container" id="detalles">
					
				</div>
				<div class="mt-2">
					<button type="button" class="btn btn-naranja" data-bs-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="compartirperfil" tabindex="-1" aria-labelledby="comptPerfil" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0">
				<h5 class="modal-title fw-bold" id="comptPerfil">Comparte tu perfil</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body">
				<div class="container p-3 text-center">
					<img id="contenedor-qr" alt="Compartir perfil QR">
					<br>
					<button class="btn btn-naranja" id="btnDescargar">
						Descargar codigo
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	// ********************************************************* CODIGO QR  *********************************************************
	$(document).ready(function () {
		$('#contenedor_experiencias').load("<?=base_url("Usuarios/verMasExperienciasFreelancer?id=".$usuario['id']."&cantidad=3")?>");
		$( "#card_pregunta_experiencia" ).clone().appendTo( "#contenido_info_experiencia" );
		const $imagen = document.querySelector("#contenedor-qr"),
			$boton = document.querySelector("#btnDescargar");
		new QRious({
			element: $imagen,
			value: "<?=base_url('usuarios/prevista/'.base64_encode($usuario['id']))?>", // La URL o el texto
			size: 150,
			backgroundAlpha: 0, // 0 para fondo transparente
			foreground: "#003c71", // Color del QR
			level: "H", // Puede ser L,M,Q y H (L es el de menor nivel, H el mayor)
		});
		$boton.onclick = () => {
			const enlace = document.createElement("a");
			enlace.href = $imagen.src;
			enlace.download = "Visita mi perfil";
			enlace.click();
		}
	});
	$('#alerta-ver').click(function(){
		alertify.notify('<div class="d-block">OPPS, para poder contactar al freelancer es necesario tener una suscripción activa, te invitamos a obtener un plan para obtener grandes beneficios dentro de la plataforma. Conocer nuestros planes de contratantes. <a href="<?=base_url('/tarifas-contratista')?>">CLIC AQUÍ</a></div>', 'advertencia');
	});
	
	$('#ver_mas_calificaciones').click(function(e){
		$this = $(this);
		let cantidad = parseInt($this.attr('dato'));
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Usuarios/verMasCalificaciones')?>',
			data:{id:<?=$usuario['id']?>, cantidad: cantidad },
			success: function(html){
				if(html == '') return $this.remove();
				
				$('#contenido_calificaciones').append(html);
				$this.attr('dato', cantidad + 3);
				if(cantidad + 3 >= <?=$cantidad_calificaciones?>) return $this.remove();
			}, error: function(error){
				alertify.error('A surgido un error', 10);
			}
		});
	});
	$('#ver_mas_experiencias').click(function(e){
		$this = $(this);
		let cantidad = parseInt($this.attr('dato'));
		$.ajax({
			type: 'GET',
			url: '<?=base_url('Usuarios/verMasExperienciasFreelancer')?>',
			data:{id:<?=$usuario['id']?>, cantidad: cantidad + 3 },
			success: function(html){
				if(html == '') return $this.remove();
				
				$('#contenedor_experiencias').append(html);
				$this.attr('dato', cantidad + 3);
				if(cantidad + 3 >= <?=$usuario['cantidad_experiencias']?>) return $this.remove();
			}, error: function(error){
				alertify.error('A surgido un error', 10);
			}
		});
	});
</script>
<?= $this->endSection() ?>