<?php
	use App\Libraries\NetPay;
	use App\Controllers\Suscripciones;
	$user = model('Autenticacion')->get_id(session('id'));
?>
<?= $this->extend('front/main') ?>
<?= $this->section('title') ?>
Areas Experiencia
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<style>
	.block-pricing .most-popular {
		padding: 20px;
	}

	.block-pricing .box-pricing-item {
		padding: 20px;
	}

	.mes-tachado {
		text-decoration: line-through;
		color: #88929b;
		font-size: 20px;
		line-height: 20px;
		display: inline-block;
	}

	.mes-tachado.abajo,
	.text-month.abajo {
		display: block;
		font-size: 65% !important;
	}

	.box-pricing-item.most-popular .mes-tachado {
		color: #fff;
	}

	.accordion-flush .accordion-button::after {
		background-image: none;
	}

	.accordion-flush .accordion-button:not(.collapsed)::after {
		background-image: none;
	}

	#contenedor_areas .card {
		border-radius: 25px;
		cursor: pointer;
		transition: all .4s;
	}

	#contenedor_areas .card:hover {
		box-shadow: inset 5px 0px 0px -1px #ff0000;
		border: 1px solid #ff0000;
	}

	.badge_card_area {
		position: relative;
		cursor: pointer;
		overflow: hidden;
	}

	.badge_card_area span {
		transition: transform 0.4s ease-out;
	}

	.badge_card_area span:first-child {
		display: inline-block;
		padding: 10px;
	}

	.badge_card_area span:last-child {
		position: absolute;
		top: 0;
		bottom: 0;
		left: -8px;
		display: flex;
		align-items: center;
		justify-content: start;
		transform: translateY(0%);
	}

	.badge_card_area:hover span:first-child {
		transform: translateY(100%);
	}

	#contenedor_areas .card:hover .badge_card_area span:last-child,
	#contenedor_areas .card:hover .badge_card_area[data-animation] a span:last-child {
		transform: none;
	}

	.badge_card_area span:last-child {
		transform: translateY(100%);
	}
</style>
<main class="main areas-de-experiencia">
	<section class="container mt-5">
		<div class="row align-items-start">
			<form class="col-lg-6 col-sm-12 needs-validation" id="form-areas" novalidate>
				<h1 class="h4 fw-bold">Para saber más de tu profesión</h1>
				<label class="text-font2 text-secundario mb-4">Cuéntanos sobre tu campo de experiencia o habilidades</label>
				<input class="text-font2 inputs-form" hidden value="registrando" name="registrando" id="registrando">
				<div class="seleccion-de-habilidades">
					<div class="form-floating mb-3">
						<?php $habilidades = model('Habilidades')->where('superior !=', '0')->orderBy('area', 'asc')->findAll(); ?>
						<div class="form-group select-style select-style-icon position-relative">
							<select class="form-control form-icons select-active select-hidden-accessible text-font" id="datalist_area" name="area" tabindex="-1" aria-hidden="true" required>
								<option selected hidden>Selecciona tu experiencia</option>
								<?php foreach ($habilidades as $key => $a) : ?>
									<option value="<?= $a['area'].' - '.$a['superior'] ?>"><?= $a['area'].' - '.$a['superior'] ?></option>
								<?php endforeach; ?>
							</select>
							<i class="fi fi-rr-list"></i>
							<div class="invalid-tooltip text-font2" id="inv-area">Requerido</div>
						</div>
					</div>
				</div>
				<div class="d-flex justify-content-start mb-4">
					<button class="btn btn-naranja" id="btn-areaPerfil" type="submit" form="form-areas">Añadir</button>
				</div>
				<div class="row justify-content-start mb-3 d-none" id="contenido_misHabilidades">
					<label class="h5">Tus habilidades</label>
					<div class="row gap-3 mb-4" id="contenedor_areas"></div>
					<div class="col-12">
						<a class="btn btn-naranja" id="continuar">Continuar</a>
					</div>
				</div>
			</form>
			<div class="col-lg-6 col-sm-12 resumen-de-compras">
				<div class="card mb-3" style="border-radius: 30px;">
					<div class="card-body">
						<label>Más información sobre tu cuenta</label>
						<hr>
						<?php if(!empty($user['plan'])){ ?>
							<div id="sec-info-pago-card">
								<?= view_cell('App\Libraries\FuncionesSistema::resumenCompraPlan', ['id' => $user['plan'], 'vista' => 'resumen-compra-registro']) ?>
							</div>
						<?php }else{ ?>
							<div id="seccion_info_cupon">
								<?php if(empty(session('cupon'))) :?>
									<div class="d-flex justify-content-start mb-3">
										<div class="d-inline-flex">
											<input class="border border-secondary text-font" onkeyup="mayus(this)" name="cupon" type="text" placeholder="Ingrese su cupón">
											<a class="btn btn-default mx-1 align-self-center validar-cupon">Validar</a>
										</div>
									</div>
								<?php else :?>
									<?php $cuenta_gratis = model('Suscripciones')->where(['id_usuario' =>  $user['id'], 'tipo_suscripcion' => 'cupon'])->first();
										echo view('backend/viewcells/Informacion_cupon', $cuenta_gratis);?>
								<?php endif ;?>
							</div>
							<hr>
							<h6 class="fw-bold text-start" id="suscripcion_seleccionada_transferencia">
								Tipo de cuenta: <strong>Plan <?= view_cell('App\Libraries\FuncionesSistema::datoPlan', ['planid' => session('plan'), 'dato' => 'name']) ?></strong>
							</h6>
							<label class="text-font d-block text-start" id="costo_seleccionada_transferencia">
								<?= view_cell('App\Libraries\FuncionesSistema::datoPlan', ['planid' => session('plan'), 'dato' => 'monto']) ?>
							</label>
							<label class="text-font2 d-block text-start" id="descripcion_seleccionada_transferencia">
								<?= view_cell('App\Libraries\FuncionesSistema::datoPlan', ['planid' => session('plan'), 'dato' => 'descripcion']) ?>
							</label>
						<?php } ?>
					</div>
				</div>
				<?php if (!Suscripciones::esta_suscrito_por_idUsuario(session('id')) && !empty((session('plan')))) { ?>
					<?php $plan = model("ModelPlanes")->where("id_plan_mensual", $user['plan'])->orWhere("id_plan_anual", $user['plan'])->first() ?>
					<div class="row align-items-center text-center" id="contenido-tarjetas">
						<div class="col-lg-12 my-2" id="columna_tarjeta">
							<label class="card p-2 contenedor_checks cont-mt-pagos" for="metodo_tarjeta">
								<input class="check-otrosPagos" type="radio" name="otros_pagos" hidden="" id="metodo_tarjeta" value="tarjeta">
								<div class="d-flex align-items-center">
									<div class="mr-20">
										<p class="d-flex text-font fw-bold m-0">Tarjeta de crédito / débito</p>
									</div>
									<div class="flex-shrink-0">
										<img src="<?= base_url('/assets/images/iconos/visa.ico') ?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
										<img src="<?= base_url('/assets/images/iconos/amex.ico') ?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
										<img src="<?= base_url('/assets/images/iconos/mastercard.ico') ?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
									</div>
								</div>
							</label>
							<div class="modal fade" id="resumenTarjeta" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" style="max-width: 800px;">
									<div class="modal-content">
										<div class="modal-header justify-content-center border-bottom-0 pb-0">
											<h5 class="modal-title fw-bold" id="titulo">Resumen de compra</h5>
											<a class="btn cerrar-modal px-3 py-2" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
										</div>
										<div class="modal-body">
											<div class="container">
												<form class="row justify-content-start needs-validation binking__form" id="netpay-form" enctype="'multipart/form-data" novalidate>
													<div class="col-12 col-lg-6">
														<div class="row">
															<input type="hidden" name="token_id" id="token_id">
															<input type="hidden" name="deviceFingerPrint" id="deviceFingerPrint">
															<input type="hidden" name="referencia_cargo" id="referencia_cargo">
															<input type="hidden" name="token_transaccion" id="token_transaccion">
															<div class="">
																<p class="fw-bold h6 text-start mt-3" style="display: inline-block; padding-right: 20px;">Tarjeta Débito/Crédito</p>
																<!-- <img class="binking__form-brand-logo"> -->
																<div class="flex-shrink-0" style="display: inline-block; vertical-align: bottom;">
																	<img src="<?= base_url('/assets/images/iconos/visa.ico') ?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
																	<img src="<?= base_url('/assets/images/iconos/amex.ico') ?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
																	<img src="<?= base_url('/assets/images/iconos/mastercard.ico') ?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
																</div>
															</div>
															<div class="row justify-content-around mt-3">
																<div class="col-12">
																	<div class="form-floating mb-3 ">
																		<input class="form-control text-font" name="nombre" id="nombrePay" required>
																		<label class="text-font" for="nombrePay">Nombre Titular</label>
																	</div>
																</div>
																<div class="col-12">
																	<div class="form-floating mb-3 ">
																		<input class="form-control text-font binking__number-field numerico" autocomplete="cc-number" inputmode="numeric" pattern="[0-9 ]*" name="tarjeta" id="tarjetaPay" maxlength="16" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
																		<label class="text-font" for="tarjetaPay">Número Tarjeta</label>
																	</div>
																</div>
																<div class="col-12 ">
																	<dv class="row">
																		<div class="col-12 col-md-4">
																			<div class="form-floating ">
																				<input class="form-control text-font binking__month-field numerico" name="mes" id="mesPay" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
																				<label class="text-font" for="mesPay">Mes</label>
																			</div>
																		</div>
																		<div class="col-12 col-md-4">
																			<div class="form-floating ">
																				<input class="form-control text-font binking__year-field numerico" name="año" id="añoPay" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
																				<label class="text-font" for="añoPay">Año</label>
																			</div>
																		</div>
																		<div class="col-12 col-lg-4">
																			<div class="form-floating ">
																				<input class="form-control text-font binking__code-field numerico" type="password" name="cvv" id="cvvPay" maxlength="4" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
																				<label class="text-font" for="cvvPay">CVV</label>
																				<img src="<?= base_url('/assets/images/cvv.png') ?>" style="position: relative; top: -44px; right: -35px;">
																			</div>
																		</div>
																	</dv>
																</div>
															</div>
															<div class="col-12">
																<input class='invisible text-font inputs-form plan_seleccionado' value="<?= $user['plan'] ?>" hidden>
															</div>
														</div>
													</div>
													<div class="col-12 col-lg-6 info-detalles-compra">
														<?= view_cell('App\Libraries\FuncionesSistema::resumenCompraPlan', ['id' => $user['plan'], 'vista' => 'resumen-compra-registro']) ?>
													</div>
													<div class="col-12 text-center">
														<button class="btn btn-naranja btn-border mx-2" id="btn-membresia">Continuar registro</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-12 my-2" id="columna_tienda">
							<label class="card p-2 contenedor_checks cont-mt-pagos" for="metodo_tienda">
								<input class="check-otrosPagos" type="radio" name="otros_pagos" hidden="" id="metodo_tienda" value="efectivo">
								<div class="d-flex align-items-center">
									<div class="mr-20">
										<p class="d-flex text-font fw-bold m-0">Efectivo</p>
									</div>
									<div class="flex-shrink-0">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 20 20" style="color: #3483FA;">
											<path fill="currentColor" fill-rule="nonzero" d="M19.002 4.004v11.981h-18V4.004h18zm-1.2 1.2h-15.6v9.581h15.6V5.204zm-7.8 2.354a2.436 2.436 0 110 4.873 2.436 2.436 0 110-4.873zm0 1.2a1.236 1.236 0 10-.001 2.473 1.236 1.236 0 00.001-2.473zm4.806.03a1.206 1.206 0 11-.002 2.41 1.206 1.206 0 01.002-2.409zm-9.614 0a1.206 1.206 0 110 2.41 1.206 1.206 0 010-2.409z"></path>
										</svg>
									</div>
								</div>
							</label>
							<div class="modal fade" id="resumenEfectivo" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered">
									<div class="modal-content">
										<div class="modal-header justify-content-center border-bottom-0 pb-0">
											<h5 class="modal-title fw-bold" id="titulo">Resumen de compra</h5>
											<a class="btn cerrar-modal px-3 py-2" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
										</div>
										<div class="modal-body">
											<div class="container">
												<form class="row justify-content-start needs-validation" id="form-editarPerfil-efectivo" enctype="multipart/form-data" novalidate>
													<div class="col-12">
														<div class="row align-items-center text-center">
															<div class="col-12 info-detalles-compra">
																<?= view_cell('App\Libraries\FuncionesSistema::resumenCompraPlanModal', ['id' => $user['plan'], 'es_tarjeta' => false]) ?>
															</div>
															<div class="col-12 text-end mt-3">
																<button class="btn btn-naranja" type="submit" form="form-editarPerfil-efectivo" id="btn_confirm_efectivo_free">Confirmar compra</button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php } elseif (!Suscripciones::esta_suscrito_por_idUsuario(session('id')) && empty((session('plan')))) { ?>
					<div class="mb-3 listado-de-planes listado-planes-registro">
						<div class="w-100">
							<div class="text-center mt-20 switch-pagos">
								<span class="text-lg text-billed">Pago Mensual</span>
								<label class="switch ml-20 mr-20">
									<input type="checkbox" id="cb_billed_type" name="billed_type" onchange="checkBilled()" />
									<span class="slider round"></span>
								</label>
								<span class="text-lg text-billed">Pago anual</span>
							</div>
						</div>
						<div class="block-pricing mt-75 mt-md-50" style="background: #ff510014;">
							<?php $planes = model('ModelPlanes')->where('cuenta', 'freelancer')->orderBy('id', 'ASC')->find() ?>
							<div class="row">
								<?php foreach ($planes as $key => $plan) { ?>
									<?php if ($plan['monto'] == 0) continue; ?>
									<div class="col-lg-4 col-sm-12 wow animate__animated animate__fadeInUp animated <?= ($plan['name'] == 'Intermedio') ? 'order-md-1 order-lg-2' : 'order-2' ?>" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
										<div class="box-pricing-item <?= ($plan['name'] == 'Intermedio') ? 'most-popular' : '' ?>">
											<?php if ($plan['name'] == 'Intermedio') : ?>
												<div class="text-end mb-10 destacado">
													<a class="btn btn-white-sm">Más popular</a>
												</div>
											<?php endif; ?>
											<h4 class="mb-15"><?= $plan['name'] ?></h4>
											<div class="box-info-price">
												<span class="text-price for-month display-month"><?= ($plan['monto'] == '0') ? 'Gratis' : '$' . $plan['monto'] ?></span>
												<span class="text-price for-year">
													<?php if ($plan['monto'] != 0) : ?>
														<span class="mes-tachado d-block"><?= '$' . $plan['monto'] ?></span>
													<?php else : ?>
														<span>Gratis</span>
													<?php endif; ?>
													<?= ($plan['monto'] != '0') ? '$' . $plan['monto_anual'] / 12 : '' ?>
												</span>
												<span class="text-month for-month display-month" style="margin-top: 1px;"><?= ($plan['monto'] == '0') ? 'Vigencia 1 año' : 'Pago mensual' ?></span>
												<span class="text-month for-year" style="margin-top: 1px;"><?= ($plan['monto'] != '0') ? 'Precio mensual' : 'Vigencia 1 año' ?></span>
											</div>
											<div>
												<p class="text-desc-package mb-30">
													<?= $plan['descripcion'] ?>
													<br><br class="for-month">
													<span class="text-facturados for-year">$ <?= $plan['monto_anual'] ?> facturados anualmente</span>
												</p>
											</div>
											<?php if (session('logeado')) : ?>
												<div class="seleccion_planes_registro">
													<a class="btn btn-border for-month display-month" plan="<?= $plan['id_plan_mensual'] ?>" name="<?= $plan['id_plan_mensual'] ?>">Seleccionar</a>
													<a class="btn btn-border for-year" plan="<?= $plan['id_plan_anual'] ?>" name="<?= $plan['id_plan_anual'] ?>">Seleccionar</a>
													<input class='invisible text-font inputs-form plan_seleccionado' hidden>
												</div>
											<?php else : ?>
												<a class="btn btn-border for-month display-month" href="<?= base_url('registrarme/freelancer?planid=' . $plan['id_plan_mensual']) ?>">Seleccionar</a>
												<a class="btn btn-border for-year" href="<?= base_url('registrarme/freelancer?planid=' . $plan['id_plan_anual']) ?>">Seleccionar</a>
											<?php endif; ?>
											<div class="d-block d-lg-none">
												<div class="accordion accordion-flush bg-transparent" id="accordionFlush<?= $key ?>">
													<div class="accordion-item border-0 bg-transparent">
														<h2 class="accordion-header" id="flush-heading-card-<?= $key ?>">
															<button class="accordion-button collapsed contenedor-arrow-v fw-normal d-block text-center" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-card-<?= $key ?>" aria-expanded="false" aria-controls="flush-collapse-card-<?= $key ?>">
																Ver características <i class="arrow-v down-v"></i>
															</button>
														</h2>
														<div id="flush-collapse-card-<?= $key ?>" class="accordion-collapse collapse" aria-labelledby="flush-heading-card-<?= $key ?>" data-bs-parent="#accordionFlush<?= $key ?>">
															<div class="accordion-body p-2 listado-caracteristicas">
																<div class="text-center my-3 border-bottom caract-list">
																	Crear Perfil
																</div>
																<?php if (view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'paneladministracion']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>') { ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		Panel de administración
																	</div>
																<?php } ?>
																<?php if (view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'solicitudtrabajo']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>') { ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		Solicitudes de trabajo
																	</div>
																<?php } ?>
																<?php if (view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'crearcv']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>') { ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		Cargar CV
																	</div>
																<?php } ?>
																<?php if (view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'veroportunidades']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>') { ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		Visualización de oportunidades
																	</div>
																<?php } ?>
																<?php if (view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'historialconversaciones']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>') { ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		Salas de chat
																	</div>
																<?php } ?>
																<!-- <php if (view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'ganarinsignias']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>') { ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		Acceso a insignias
																	</div>
																<php } ?> -->
																<!-- <php if (view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'valoraciontrabajos']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>') { ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		Valoración de trabajos
																	</div>
																<php } ?> -->
																<!-- <php if (view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'recomendacionesclientes']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>') { ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		Testimonios de tus clientes
																	</div>
																<php } ?> -->
																<?php if (view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'anadirportafolio']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>') { ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		Cargar portafolio
																	</div>
																<?php } ?>
																<?php $porSemana = view_cell('App\Libraries\FuncionesSistema::postuSemana', ['plan' => $plan['id']]) ?>
																<?php if ($porSemana > 0) { ?>
																	<div class="text-center my-3 border-bottom caract-list">
																		<?= $porSemana == 9999 ? 'Postulaciones al mes ilimitadas' : $porSemana . ' postulaciones al mes' ?>
																	</div>
																<?php } ?>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<script>
						$('.seleccion_planes_registro a').click(function(){
							let id = $(this).attr('plan');
							$.ajax({
								type: 'POST',
								url: '<?= base_url('/Autenticacion/cambiarPlanRegistro')?>',
								data:{id:id},
								success: (data) => location.reload(),
								error: (data) =>console.log('error')
							});
						});
					</script>
				<?php } ?>
			</div>
		</div>
		<div class="modal fade" id="respuesta_pago" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header justify-content-center border-bottom-0">
						<h5 class="modal-title fw-bold" id="titulo">¡Un paso más!</h5>
						<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#respuesta_pago" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
					</div>
					<div class="modal-body">
						<div class="container">
							<div class="row align-items-center text-center">
								<div class="col-12">
									<p class="h6 fw-normal mb-2">Número de referencia: <span class="fw-bold" id="referencia"></span>.</p>
									<label class="text-font mb-2">Te hemos enviado un correo con los pasos a seguir para poder proceder con la suscripción a tu cuenta. Guarda este número de referencia, lo necesitarás para pagar tu plan.</label>
								</div>
								<div class="col-12">
									<a onclick="location.reload();" class="btn btn-naranja">Continuar con registro</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<script>
	let _this = this;
	const respuesta_3ds = function(_this, referenceId) {
		console.log('referenceId: ' + referenceId);
		charges(referenceId);
	}

	const charges = function(referenceId) {
		console.log('charges: ' + referenceId);
		$('#referencia_cargo').val(referenceId);
	}
	
	const confirm = function(processorTransactionId) {
		console.log('confirm: ' + processorTransactionId);
		$.ajax({
			url: '<?= base_url('Pagos/confirmar_pago') ?>',
			type: 'POST',
			data: {transaccion: $('#token_transaccion').val(), procesador: processorTransactionId, usuario: <?=session('id')?>},
			success: function(data) {
				let respuesta = JSON.parse(data);
				$('#btn-membresia').removeAttr('disabled').children('span').remove();

				if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta);

				$('#resumenTarjeta').modal('hide');
				alertify.notify(respuesta.mensaje, respuesta.alerta);
				return setTimeout((e) => location.href = "/compra_realizada?token="+respuesta.datos.transactionTokenId, 2000);
			},
			error: function(data) {
				let respuesta = JSON.parse(data.responseText);
				$('#btn-membresia').removeAttr('disabled').children('span').remove();
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
			}
		});
	}

	const callbackProceed = function(_this, processorTransactionId) {
		alertify.notify('processorTransactionId: ' + processorTransactionId, 'custom');
		confirm(processorTransactionId);
	}

	//La generación del device fingerprint debe de estar al momento en que se visualiza el formulario de pago
	function generateDevice(callback) {
		deviceFingerPrint = NetPay.form.generateDeviceFingerPrint();
		callback();
	}

	$(document).ready(function() {
		let deviceFingerPrint;
		<?php $netpay = new NetPay() ?>
		<?= $netpay->getModo() ? 'NetPay.setSandboxMode(true); netpay3ds.setSandboxMode(true);' : 'NetPay.setSandboxMode(false); netpay3ds.setSandboxMode(false);' ?> // Esta línea se debe de llamar antes de la generación del device fingerprint

		// SE OBTIENE REFERENCEID POR 3DS
		netpay3ds.init(() => netpay3ds.config(_this, 100, respuesta_3ds));
	});

	$('input[name="otros_pagos"]').change(function() {
		let id = $(this).attr('id');
		$('label[for="' + id + '"]').addClass('active');
		if ($(this).val() == 'efectivo') {
			$('#resumenEfectivo').modal('show');
		} else if ($(this).val() == 'tarjeta') {
			$('#resumenTarjeta').modal('show');
		}
	})
	
	$('.cerrar-modal').click(function() {
		$('.cont-mt-pagos').removeClass('active');
		$('.modal').modal('hide');
		$('input[name="otros_pagos"]').prop('checked', false);
	});
	
	$('#continuar').click(() => location.reload());
	
	$('#btn-areaPerfil').click(function(event) {
		if (event.target.form.datalist_area[0].selected) return false;

		event.preventDefault();
		$(this).addClass('disabled');
		$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);

		$areas = $('#form-areas');
		$.ajax({
			url: 'Autenticacion/guardarAreas',
			type: 'POST',
			data: $areas.serialize(),
			success: function(data) {
				let respuesta = JSON.parse(data);
				if (respuesta.alerta == 'custom') {
					$('#btn-areaPerfil').removeClass('disabled').children('span').remove();
					alertify.notify(respuesta.mensaje, respuesta.alerta, 10);

				} else {
					let area = $('#form-areas #datalist_area').val();

					$('#btn-areaPerfil').removeClass('disabled').children('span').remove();
					$('#contenido_misHabilidades').removeClass('d-none');

					// $habilidad = Object.values(respuesta.datos)[Object.values(respuesta.datos).length - 1];
					// console.log($habilidad);
					$('#contenedor_areas').prepend(`
						<div class="col-12">
							<div class="d-flex">
								<div class="card p-2" area="` + area + `">
									<label class="small badge_card_area mb-0 lh-base">
										<span><i class="fas fa-times text-danger"></i></span>
										&nbsp;&nbsp;&nbsp;&nbsp;` + area + `&nbsp;&nbsp;
									</label>
								</div>
							</div>
						</div>`);

					$areas[0].reset();
					$areas.removeClass('was-validated');
					alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				}
			},
			error: function(data) {
				alertify.notify('Network', 'falla', 0);
			}
		});
	});
	
	$('#contenedor_areas').delegate('.card', 'click', function(evento) {
		$datos = $(this);
		if (alertify.confirm(iconoAlertify + ' ¡Atención!', '¿Estás seguro de eliminar <b class="fw-bold">' + $datos.attr('area') + '</b>?', function() {
				$.ajax({
					url: '<?= base_url('Autenticacion/borrar_subarea') ?>',
					type: 'POST',
					data: {
						area: $datos.attr('area'),
					},
					success: function(data) {
						let respuesta = JSON.parse(data);
						if (respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);

						$datos.parent().parent().remove();
						if ($('#contenedor_areas .card').length) $('#contenido_misHabilidades').removeClass('d-none');
						else $('#contenido_misHabilidades').addClass('d-none');

						return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					},
					error: function(data) {
						let respuesta = JSON.parse(data.responseText);
						alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
					}
				});
			}, function() {}).set({
				transition: 'zoom'
			}).setting('labels', {
				ok: 'Aceptar',
				cancel: 'Cancelar'
			})) {}
	});
	
	$('#btn-membresia').click(function(evento) {
		$this = $(this);
		if (evento.currentTarget.form.nombrePay.value.length == 0 || evento.currentTarget.form.tarjetaPay.value.length == 0 ||
			evento.currentTarget.form.mesPay.value.length == 0 || evento.currentTarget.form.cvvPay.value.length == 0) return true;

		evento.preventDefault();
		$($this).attr('disabled', true);
		$($this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo($this);

		generateDevice(function() {
			let tarjeta = {
				cardNumber: evento.currentTarget.form.tarjetaPay.value,
				expMonth: evento.currentTarget.form.mesPay.value,
				expYear: evento.currentTarget.form.añoPay.value,
				cvv2: evento.currentTarget.form.cvvPay.value,
				// cardHolderName: evento.currentTarget.form.nombrePay.value
				vault: true,
				simpleUse: false,
				deviceFingerPrint: deviceFingerPrint
			};
			NetPay.setApiKey('<?= $netpay->getLlave() ?>');
			var validateNumberLength = NetPay.card.validateNumberLength(tarjeta.cardNumber);
			var validateNumber = NetPay.card.validateNumber(tarjeta.cardNumber);
			var validateExpiry = NetPay.card.validateExpiry(tarjeta.expMonth, tarjeta.expYear);
			var validateCVV = NetPay.card.validateCVV(tarjeta.cvv2, tarjeta.cardNumber);

			if (!validateNumberLength || !validateNumber) {
				$('#btn-membresia').removeAttr('disabled').children('span').remove();
				return alertify.notify('Número de tarjeta incorrecto', 'advertencia', 10);
			}
			if (!validateExpiry) {
				$('#btn-membresia').removeAttr('disabled').children('span').remove();
				return alertify.notify('Expiración de tarjeta incorrecto', 'advertencia', 10);
			}
			if (!validateCVV) {
				$('#btn-membresia').removeAttr('disabled').children('span').remove();
				return alertify.notify('Código de seguridad incorrecto', 'advertencia', 10);
			}

			NetPay.token.create(tarjeta, correcto, falla);

			function correcto(respuesta) {
				console.log("Token creado correctamente");
				let datos_token = JSON.parse(respuesta.message.data);
				console.log(datos_token);
				$('#token_id').val(datos_token.token);
				$('#deviceFingerPrint').val(tarjeta.deviceFingerPrint);

				$.ajax({
					url: '<?= base_url('Pagos/crear') ?>',
					type: 'POST',
					data: $('#netpay-form').serialize(),
					success: function(data) {
						let respuesta = JSON.parse(data);
						$('#btn-membresia').removeAttr('disabled').children('span').remove();

						if (respuesta.alerta == 'custom'){
							$('#token_transaccion').val(respuesta.datos.transactionTokenId);
							let canProceed = netpay3ds.canProceed(respuesta.datos.status, respuesta.datos.threeDSecureResponse.responseCode, respuesta.datos.threeDSecureResponse.acsUrl);
							console.log("canProceed es: " + canProceed);
							
							if (!canProceed) return alertify.notify('Transacción rechadaza', 'falla');
							
							let proceed = netpay3ds.proceed(_this, respuesta.datos.threeDSecureResponse.acsUrl, respuesta.datos.threeDSecureResponse.paReq, respuesta.datos.threeDSecureResponse.authenticationTransactionID, callbackProceed);

						}
						$('#resumenTarjeta').modal('hide');
						alertify.notify(respuesta.mensaje, respuesta.alerta);
						return setTimeout((e) => location.href = "/compra_realizada?token="+respuesta.datos.transactionTokenId, 2000);
					},
					error: function(data) {
						let respuesta = JSON.parse(data.responseText);
						$('#btn-membresia').removeAttr('disabled').children('span').remove();
						alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
					}
				});
			}

			function falla(error) {
				console.trace(error);
				let mensaje = error.message == 'Empty or invalid card number' ? 'Número de tarjeta inválido' : error.message;
				alertify.notify("ERROR [" + mensaje + "] inténtalo nuevamente recargando la página", 'falla', 10);
				$('#btn-membresia').removeAttr('disabled').children('span').remove();
			}
		});
	});
	$('#btn_confirm_efectivo_free').click(function(evento) {
		$this = $(this);
		evento.preventDefault();
		$(this).addClass('disabled');
		$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);
		if (alertify.confirm(iconoAlertify + ' ¡Atención!', '¿Estás seguro de realizar tu compra?', function() {
				$.ajax({
					url: '/Suscripcion_efectivo',
					type: 'POST',
					data: {
						id_plan: $('.plan_seleccionado').val()
					},
					success: function(data) {
						let respuesta = JSON.parse(data);
						$('#efectivo_modal').modal('hide');
						$($this).removeClass('disabled').children('span').remove();
						$('input[name="otros_pagos"]').prop('checked', false).parent().removeClass('active');
						$('#obtener_membresia .cerrar-modal').click();
						$('.modal').modal('hide');
						if (respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
						$('#referencia').text(respuesta.referencia);
						$('#respuesta_pago').modal('show');
						alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					},
					error: function(data) {
						let respuesta = JSON.parse(data.responseText);
						alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
					}
				});
			}, function() {
				$($this).removeClass('disabled').children('span').remove()
			}).set({
				transition: 'zoom'
			}).setting('labels', {
				ok: 'Aceptar',
				cancel: 'Cancelar'
			})) {}
	});
</script>
<?= $this->endSection() ?>