<?=$this->extend('front/main')?>
<?=$this->section('title')?>
	Solicitud para el proyecto: <?=$proyecto['titulo']?>
<?=$this->endSection()?>

<?=$this->section('content')?>
<main class="main solicitudes-free">
	<section class="section-box">
		<div class="box-head-single">
			<div class="container">
				<h3><span data-bs-toggle="tooltip" data-bs-placement="top" title="Titulo del proyecto"><?=$proyecto['titulo']?></span></h3>
				<ul class="breadcrumbs">
					<li><a>Solicitud de proyecto</a></li>
				</ul>
				<button class="btn btn-regresar btn-border" onclick="history.back()">Regresar</button>
			</div>
		</div>
	</section>
	<section class="section-box mt-15">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12 col-sm-12 col-12">
					<div class="single-image-feature overflow-hidden">
						<?=view_cell('App\Libraries\FuncionesSistema::carruselProyecto', ['id' => $proyecto['id']])?>
					</div>
					<div class="content-single">
						<?php if(!empty($proyecto['desc_corta'])) :?>
    						<p>
    						    <h6>Resumen</h6>
    						    <?=$proyecto['desc_corta']?>
    					    </p>
					    <?php endif ;?>
					    <?php if(!empty($proyecto['descripcion'])) :?>
    					    <p>
    					        <h6>Descripción</h6>
    					        <?=$proyecto['descripcion']?>
    				        </p>
				        <?php endif ;?>
					</div>
				</div>
				<div class="col-lg-4 col-md-12 col-sm-12 col-12 pl-40 pl-lg-15 mt-lg-30">
					<?php $contratante = model('Autenticacion')->get_id($proyecto['id_contratista']); ?>
					<div class="sidebar-shadow">
						<div class="sidebar-heading">
							<div class="avatar-sidebar">
								<figure><img alt="jobhub" src="<?=imagenPerfil($contratante)?>" /></figure>
								<div class="sidebar-info">
									<span class="sidebar-company"><?= $contratante['nombre'] ?>  <?= $contratante['apellidos'] ?></span>
								</div>
							</div>
						</div>
						<div class="text-center mt-20" id="sec-chat">
							<div class="row align-items-center">
								<?php if($proyecto['estatus'] == 'espera') :?>
									<div class="col-6">
										<div class="d-grid gap-1">
											<a class="btn btn-primary aceptar-solicitud" data-id="<?=$solicitud['id']?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Aceptar solicitud de trabajo.">
												Aceptar
											</a>
										</div>
									</div>
									<div class="col-6">
										<div class="d-grid gap-1" >
											<a class="btn btn-naranja rechazar-solicitud" data-id="<?=$solicitud['id']?>" data-contratista="<?=$proyecto['id_contratista']?>" data-bs-toggle="modal" data-bs-target="#modal-solicitud">
												Declinar
											</a>
										</div>
									</div>
								<?php else :?>
									<form class="d-flex justify-content-center align-items-center" id="quitar-solicitud" action="<?=base_url('Proyectos/eliminarSolicitudFreelancer')?>" method="POST" enctype="multipart/form-data">
										<input class="btn btn-primary me-3" type="submit" value="Quitar" form="quitar-solicitud">
										<input class="hidden invisible" hidden name="solicitud" value="<?=$solicitud['id']?>" id="solicitud">
										<div class="tooltips"><i style="color: #003B71; font-size:17px;" class="fas fa-info-circle info_proyecto" aria-hidden="true"></i>
											<span class="tooltiptext">Proyecto en desarrollo por otro freelancer.</span>
										</div>
									</form>
								<?php endif ;?>
							</div>
							<!--<a class="btn aceptar-solicitud" data-id="<=$solicitud['id']?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Aceptar solicitud de trabajo."><i class="fas fa-check"></i></a>-->
							<!--<a class="btn rechazar-solicitud" data-id="<=$solicitud['id']?>" data-contratista="<=$proyecto['id_contratista']?>" data-bs-toggle="modal" data-bs-target="#modal-solicitud"><i class="far fa-times-circle"></i></a>-->
						</div>
						<div class="sidebar-list-job">
							<ul>
								<li>
									<div class="sidebar-icon-item"><i class="fi-rr-briefcase"></i></div>
									<div class="sidebar-text-info">
										<span class="text-description">Tipo de proyecto</span>
										<strong class="small-heading"><?=$proyecto['categoria']?> / <?=$proyecto['subcategoria']?></strong>
									</div>
								</li>
								<li>
									<div class="sidebar-icon-item"><i class="fi-rr-marker"></i></div>
									<div class="sidebar-text-info">
										<span class="text-description">Locación</span>
										<strong class="small-heading"><?=view_cell('App\Libraries\FuncionesSistema::direccionContratista', ['id' => $proyecto['id_contratista']])?></strong>
									</div>
								</li>
								<li>
									<div class="sidebar-icon-item"><i class="fi-rr-dollar"></i></div>
									<div class="sidebar-text-info">
										<span class="text-description">Presupuesto</span>
										<?php if(!empty($proyecto['presupuesto'])) :?>
											<strong class="small-heading">$ <?= number_format($proyecto['presupuesto'], 2, '.', ',') ?> MXN</strong>
										<?php else :?>
											<strong class="small-heading">Confidencial</strong>
										<?php endif ;?>
									</div>
								</li>
								<li>
									<div class="sidebar-icon-item"><i class="fi-rr-clock"></i></div>
									<div class="sidebar-text-info">
										<span class="text-description">Fecha de creación</span>
										<strong class="small-heading"><?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $proyecto['created_at']])?></strong>
									</div>
								</li>
								<li>
									<div class="sidebar-icon-item"><i class="fi-rr-time-fast"></i></div>
									<div class="sidebar-text-info">
										<span class="text-description">Fecha de entrega esperado</span>
										<strong class="small-heading"><?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $proyecto['fecha_entrega']])?></strong>
									</div>
								</li>
							</ul>
						</div>
						<!---->
						<?php if($proyecto['estatus'] == 'desarrollo' || $proyecto['estatus'] == 'finalizado' ){ ?>
							<div class="sidebar-team-member pt-40">
								<?php if($proyecto['estatus'] != 'finalizado' && $proyecto['id_freelancer'] == session('id')){ ?>
									<h6 class="small-heading">Tareas</h6>
									<form id="tareas">
										<div class="row mb-1 p-1">
											<div class="col-10 align-self-center">
												<div class="form-floating mb-3">
													<input type="text" class="form-control" id="tarea" name="tarea" placeholder="...">
													<label for="tarea">Agregar una nueva tarea</label>
												</div>
												<input type="hidden" id="id-proyecto" value="<?=$proyecto['id']?>" name="id">
											</div>
											<div class="col-2 align-self-center">
												<a class="btn btn-naranja agregar-tarea rounded-circle">
													<i class="fas fa-plus"></i>
												</a>
											</div>
										</div>
									</form>
								<?php } ?>
								<?php if($proyecto['estatus'] == 'desarrollo' && $proyecto['id_freelancer'] == session('id')){ ?>
									<div class="w-100 border" style="min-height: 20px;">
										<div class="row p-2" id="sec-tareas">
											<?=view_cell('App\Libraries\FuncionesSistema::tareasProyecto', ['id' => $proyecto['id']]);?>
										</div>
									</div>
								<?php }else if($proyecto['estatus'] == 'finalizado'){ ?>
									<div class="w-100 border" style="min-height: 20px;">
										<div class="row p-2" id="sec-tareas">
											<?=view_cell('App\Libraries\FuncionesSistema::tareasProyectoContra', ['id' => $proyecto['id']])?>
										</div>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
					</div>
					<!--<div class="sidebar-normal">-->
						<!--view_cell('App\Libraries\FuncionesSistema::habilidadesFreelance', ['id' => session('id')])?>-->
					<!--</div>-->
				</div>
			</div>
			<div class="single-recent-jobs">
    			<h4 class="heading-border"><span>Más proyectos</span></h4>
    			<div class="list-recent-jobs">
    				<?=view_cell('App\Libraries\FuncionesSistema::proyectosRelacionados', ['id' => session('id'), 'pro' => $proyecto['id']])?>
    				<div class="mb-20">
    					<a href="<?=base_url('/buscar-proyecto?search=&area=')?>" class="btn btn-default">Explorar más proyectos</a>
    				</div>
    			</div>
    		</div>
		</div>
	</section>
	<div class="modal fade" id="modal-solicitud" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo" aria-modal="true" role="dialog">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0">
					<h5 class="modal-title fw-bold" id="titulo">Declinar solicitud</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-12 text-end p-1 p-sm-2">
								<div class="card p-2">
									<form id="form-declinar">
										<p class="text-font text-secundario text-start">Gracias por tomarme en cuenta, lamentablemente no me es posible procesar su solicitud, debido a:</p>
										<div class="form-floating">
											<textarea class="form-control" placeholder="Leave a comment here" id="campo-explicacion" name="explicacion" style="height: 100px"></textarea>
											<label for="campo-explicacion">Ingrese un mensaje para el contratista:</label>
										</div>
										<input type="hidden" id="id-solicitud" name="id" value="">
										<input type="hidden" id="contratista" name="contratista">
									</form>
								</div>
								<a class="btn btn-azul my-2" data-bs-dismiss="modal" aria-label="Close">Cancelar</a>
								<a class="btn btn-rojo my-2" id="confirmar-declinacion">Declinar</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<script>
	$('.rechazar-solicitud').on('click', function(){
		let soli = $(this).attr('data-id');
		let contratista = $(this).attr('data-contratista');
		$('#id-solicitud').val(soli);
		$('#contratista').val(contratista);
	});
	$('.aceptar-solicitud').on('click', function(){
		$this = $(this);
		let id = $this.attr('data-id');
		$this.addClass('disabled');
		$this.add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo($this);

		alertify.confirm('Atención', 'Está a punto de aceptar la solicitud de trabajo. ¿Está seguro de eso?', function(){
			$.ajax({
				type: 'POST',
				url: '<?=base_url('/Proyectos/aceptedSolicitud')?>',
				data:{id:id},
				success: function(data){
					let cont = JSON.parse(data);
					$this.removeClass('disabled').children('span').remove();
					if(cont.tipo != 'correcto') return alertify.notify(cont.mensaje, cont.tipo);
					
					alertify.notify(cont.mensaje, cont.tipo);
					return setTimeout(() => { location.href = '<?= base_url('/trabajo').'/'?>'+cont.proyecto }, 4000);
				}, error: function(data){
					$this.removeClass('disabled').children('span').remove();
					alertify.notify('Al parecer hubo un error, comuníquese con el área de soporte.');
				}
			});
		}, function(){$this.removeClass('disabled').children('span').remove()});
	});
	$('#quitar-solicitud').submit(function(e){
		e.preventDefault();
		$form = $(this);
		if (alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro de eliminar esta solicitud?', function() {
			if(e.target.solicitud.value.length == 0) return alertify.notify('UPPS... no se pudo encontrar esta solicitud, recargue la página e inténtelo nuevamente.', 'falla');
			
			// $form.attr({
			//     action: 
			//     method: 'POST'
			// });
			$form[0].submit();
		}, function(){}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
	});
	$('#confirmar-declinacion').on('click', function(){
		let id = $('#id-solicitud').val();
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Proyectos/declinarSolicitud')?>',
			data:$('#form-declinar').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'advertencia'){
					alertify.notify(cont.mensaje, cont.tipo);
				}else{
					$('#soli'+id+'card').remove();
					$('#form-declinar')[0].reset();
					alertify.notify(cont.mensaje, cont.tipo);
					setTimeout(function(){location.href = '<?=base_url('usuarios/solicitudes')?>';}, 3000);
				}
			}, error: function(data){
				alertify.error('NETWORK');
			}
		});
	});
</script>
<?=$this->endSection()?>
