<?=$this->extend('front/main')?>
<?=$this->section('title')?>
	Solicitudes
<?=$this->endSection()?>

<?=$this->section('content')?>
<main class="main solicitudes-freelancer">
	<section class="section-box">
		<div class="container pt-50">
			<div class="w-100 w-md-100 mx-auto text-center">
				<h3 class="fw-bold section-title-large mb-30 wow animate__animated animate__fadeInUp" style="font-size: 44px; line-height: 54px;">Solicitudes directas de trabajo</h3>
				<p class="mb-30 text-muted wow animate__animated animate__fadeInUp font-md">Te mostramos todas las solicitudes que te han ofrecido, revisa a detalle y acepta las que sean de tu interés.</p>
			</div>
		</div>
	</section>
	<section class="section-box">
		<div class="container">
			<div class="row flex-row-reverse">
				<div class="col-lg-12 col-md-12 col-sm-12 col-12 float-right">
					<div class="content-page">
						<div class="box-filters-job mt-15 mb-10">
							<div class="row justify-content-between justify-content-lg-end">
								<div class="col-lg-7">
									<span class="text-small">Mostrando <strong id="cantidad_solicitudes"><?=count($proyectos)?> </strong>solicitudes</span>
								</div>
								<div class="col-lg-5 text-lg-end mt-15">
									<div class="d-flex justify-content-between justify-content-md-end gap-2">
										<span class="text-sortby">Ordenar por:</span>
										<div class="dropdown dropdown-sort">
											<button class="btn dropdown-toggle" type="button" id="dropdownSort" data-bs-toggle="dropdown" aria-expanded="false" data-bs-display="static"><span>Más nuevos</span> <i class="fi-rr-angle-small-down"></i></button>
											<ul class="dropdown-menu dropdown-menu-light" aria-labelledby="dropdownSort" id="ordenar_solicitudes">
												<li><a class="dropdown-item active" value="recientes">Más nuevos</a></li>
												<li><a class="dropdown-item" value="antiguos">Más viejos</a></li
												<li><a class="dropdown-item" value="mayor_presupuesto">Mayor presupuesto</a></li>
												<li><a class="dropdown-item" value="menor_presupuesto">Menor presupuesto</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row job-listing-grid-2" id="contenido_solicitudes">
							<?php foreach($proyectos as $key => $proyecto) :?>
								<?=view_cell('App\Libraries\FuncionesSistema::cardSolicitud', ['proyecto' => $proyecto])?>
							<?php endforeach ;?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<div class="modal fade" id="modal-solicitud" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo" aria-modal="true" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0">
				<h5 class="modal-title fw-bold" id="titulo">Declinar solicitud</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 text-end p-1 p-sm-2">
							<form id="form-declinar">
								<p class="text-font text-secundario text-start mb-3">Gracias por tomarme en cuenta, lamentablemente no me es posible procesar su solicitud, debido a:</p>
								<div class="form-floating mb-2">
									<textarea class="form-control" placeholder="Leave a comment here" id="campo-explicacion" name="explicacion" style="height: 100px"></textarea>
									<label for="campo-explicacion">Ingrese un mensaje para el contratante:</label>
								</div>
								<input type="hidden" id="id-solicitud" name="id" value="">
								<input type="hidden" id="contratista" name="contratista">
							</form>
							<a class="btn btn-naranja btn-border my-2" data-bs-dismiss="modal" aria-label="Close">Cancelar</a>
							<a class="btn btn-default my-2" id="confirmar-declinacion">Declinar</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- **************************************************** SOLICITUD Y PUBLICAR ****************************************** -->
<div class="modal fade" id="modal-compartir" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="mandar-solicitud" aria-modal="true" role="dialog">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0 pt-4">
				<h5 class="modal-title fw-bold h4" id="mandar-solicitud">Compartir solicitud con Freelancers</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body pt-0">
				<div class="p-3 pt-0">
					<div class="row justify-content-center">
						<div class="col-12 col-sm-12 col-lg-10">
							<label for="search-freelance">Solicitudes disponibles: <b class="fw-bold" id="cont_solicitudes_diponibles">5</b></label>
							<input class="form-control" id="search-freelance" placeholder="Buscar por: Nombre o correo">
						</div>
						<div class="col-12 col-sm-12 col-lg-8">
							<div class="dropdown w-100">
								<ul class="dropdown-menu p-0 w-100" id="lista-freelance" >
								</ul>
							</div>
						</div>
						<div class="p-3"></div>
						<div class="col-12 col-sm-12 col-lg-10">
							<div id="free-seleccionados" class="row">
							</div>
							<input type="hidden" id="free-select">
						</div>
						<div class="col-12 p-3 text-center">
							<a class="btn btn-naranja" id="solicitudes-compartir">
								Compartir solicitud
							</a>
						</div>
						<input id="id-compartir" type="hidden">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('.rechazar-solicitud').on('click', function(){
		let soli = $(this).attr('data-id');
		let contratista = $(this).attr('data-contratista');
		$('#id-solicitud').val(soli);
		$('#contratista').val(contratista);
		$('#modal-solicitud').modal('show');
	});
	$('#confirmar-declinacion').on('click', function(){
		let id = $('#id-solicitud').val();
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Proyectos/declinarSolicitud')?>',
			data:$('#form-declinar').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo != 'advertencia') return alertify.notify(cont.mensaje, cont.tipo, 10);
				
				$('#modal-solicitud').modal('hide');
				$('#soli'+id+'card').remove();
				$('#form-declinar')[0].reset();
				alertify.notify(cont.mensaje, cont.tipo, 10);
			}, error: function(data){
				alertify.error('NETWORK', 10);
			}
		});
	});
	$('#ordenar_solicitudes').delegate('li a.dropdown-item', 'click', function(){
		$this = $(this);

		$.ajax({
			type: "POST",
			url: '<?=base_url('proyectos/ordenar_solicitudes')?>',
			data: {filtro : $this.attr('value')},
			success: function (respuesta) {
				$('#contenido_solicitudes').html(respuesta);
			},
			error: function (error) {
				alertify.error(error, 10);
			}
		});
	});
	$('.btn-compartir-soli').click(function(){
		let id = $(this).attr('data-id');
		$('#id-compartir').val(id);
		$('#modal-compartir').modal('show');
	});
	$('#solicitudes-compartir').click(function(){
		let frees = $('#free-select').val();
		let soli = $('#id-compartir').val();
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Proyectos/shareSoliProyecto')?>',
			data:{frees:frees, soli:soli},
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error') return alertify.notify(cont.mensaje, 'advertencia');
				$('#free-select').val('');
				$('#modal-compartir').modal('hide');
				return alertify.notify(cont.mensaje, 'correcto');
			}, error: function(data){
				let respuesta = JSON.parse(data.responseText);
				alertify.error('Error: ' + respuesta.message + '\n' + respuesta.file);
			}
		});
	});
	$('#search-freelance').keyup(function(){
		let search = $(this).val();
		let frees = $('#free-select').val();
		console.log(frees);
		$('#lista-freelance').addClass('d-block');
		if(search.length == 0){
			console.log('Sin texto');
		}else{
			$.ajax({
				type: 'POST',
				url: '<?=base_url('/Proyectos/buscadorFreelance')?>',
				data:{search:search, frees:frees},
				success: function(data){
					$('#lista-freelance').html(data);
				}, error: function(data){
					alertify.notify('Contáctese con el área de soporte técnico.', 'falla', 10);
				}
			});
		}
	});
</script>
<?=$this->endSection()?>