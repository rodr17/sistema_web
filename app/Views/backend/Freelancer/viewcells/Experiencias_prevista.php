<?php foreach ($experiencias as $key => $experiencia) : $i = 0?>
    <!-- item start -->
    <div class="card-job-top d-none d-md-block">
        <div class="card-job-top--info">
            <h6 class="card-job-top--info-heading titulo-experiencias"><a href="#"><?=$experiencia['titulo']?></a></h6>
            <div class="row">
                <div class="col-lg-12">
                    <span class="tiempo-comentarios"><i class="fi-rr-clock"></i></span>
                    <span class="card-job-top--post-time text-sm">
                        <?= strftime('%d %B, %Y', strtotime($experiencia['inicio'])).' a '.strftime('%d %B, %Y', strtotime($experiencia['fin'])) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="card-job-top d-block d-md-none text-center">
        <div class="card-job-top--info">
            <h6 class="card-job-top--info-heading titulo-experiencias"><a href="#"><?=$experiencia['titulo']?></a></h6>
            <div class="row">
                <div class="col-lg-12">
                    <span class="tiempo-comentarios"><i class="fi-rr-clock"></i></span>
                    <span class="card-job-top--post-time text-sm">
                        <?= strftime('%d %B, %Y', strtotime($experiencia['inicio'])).' a '.strftime('%d %B, %Y', strtotime($experiencia['fin'])) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="card-job-description mt-20 experiencias-freelancer-detalle d-none d-md-block">
        <?= view_cell('App\Libraries\FuncionesSistema::limitar_cadena', ['cadena' => $experiencia['descripcion'], 'limite' => 70, 'sufijo' => '...']) ?>
        <a class="btn btn-border mt-20 ver-detalles" data-desc="<?=$experiencia['descripcion']?>" data-tit="<?=$experiencia['titulo']?>">Continuar leyendo</a>
    </div>
    <div class="card-job-description mt-20 experiencias-freelancer-detalle d-block d-md-none text-center">
        <?= view_cell('App\Libraries\FuncionesSistema::limitar_cadena', ['cadena' => $experiencia['descripcion'], 'limite' => 70, 'sufijo' => '...']) ?>
        <a class="btn btn-border mt-20 ver-detalles" data-desc="<?=$experiencia['descripcion']?>" data-tit="<?=$experiencia['titulo']?>">Continuar leyendo</a>
    </div>
    <!-- item end -->
<?php endforeach; ?>
<script>
    $('.ver-detalles').click(function(){
        $('#modaldetalleTitulo').html($(this).attr('data-tit'));
        $('#detalles').html($(this).attr('data-desc'));
        $('#modaldetalle').modal('show');
    });
</script>