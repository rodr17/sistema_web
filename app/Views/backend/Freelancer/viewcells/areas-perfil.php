<?php if (isset($freelancer['areas'])) : ?>
	<div class="row justify-content-center">
		<div class="col-12">
			<?php foreach ($freelancer['areas'] as $key => $area) : ?>
				<fieldset class="my-3 p-2">
					<legend class="text-start text-font badge_eliminar m-0" data-id="<?=$key?>" name="<?= $area['area'] ?>"><?= $area['area'] ?><span><i class="fas fa-times text-danger"></i></span></legend>
					<?php $subareas = json_decode($area['subarea'], true) ?>
					<?php foreach ($subareas as $i => $subarea) : ?>
						<label class="text-font badge_eliminar" area="<?= $area['area'] ?>" subarea="<?= $subarea ?>">
							<span><i class="fas fa-times text-danger"></i></span>
							&nbsp;&nbsp;&nbsp;&nbsp;<?= $subarea ?>
						</label>
					<?php endforeach; ?>
				</fieldset>
			<?php endforeach; ?>
		</div>
	</div>
	<script>
		$('legend.badge_eliminar').click(function(evento) {
			$area = $(this);
			evento.preventDefault();
			if (alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro de eliminar el área <b>' + $area.attr('name') + '</b>?', function() {
				$.ajax({
					url: '<?= base_url('usuarios/eliminarArea')?>',
					type: 'POST',
					data: {
						area: $area.attr('name')
					},
					success: function(data) {
						let respuesta = JSON.parse(data);
						if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);

						$area.parent().remove();
						return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					},
					error: function(data) {
						let respuesta = JSON.parse(data.responseText);
						alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
					}
				});
			}, function() {}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
		});
		$('label.badge_eliminar').click(function(evento) {
			$subarea = $(this);
			evento.preventDefault();
			if (alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro de eliminar la subárea <b>' + $subarea.attr('subarea') + '</b>?', function() {
				$.ajax({
					url: '<?= base_url('Autenticacion/borrar_subarea')?>',
					type: 'POST',
					data: {
						area: $subarea.attr('area'),
						subarea: $subarea.attr('subarea')
					},
					success: function(data) {
						let respuesta = JSON.parse(data);
						if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				// 		console.log(respuesta.eliminar_area);
						(respuesta.eliminar_area) ? $subarea.parent().remove(): $subarea.remove();
						
						return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					},
					error: function(data) {
						let respuesta = JSON.parse(data.responseText);
						alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
					}
				});
			}, function() {}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
		});
	</script>
<?php endif; ?>