<div class="col-lg-4 col-md-6" id="soli<?=$proyecto['id']?>card">
	<div class="card-grid-2 hover-up wow animate__animated animate__fadeIn br-10" data-wow-delay=".1s">
		<div class="card-block-info">
			<div class="row mb-10">
				<div class="col-lg-12 col-12">
					<a class="card-2-img-text card-grid-2-img-medium">
						<span class="text-gray-100 d-flex text-md">
							<i class="fi-rr-shield-check ms-0 me-1"></i><span class="text-truncate" style="vertical-align: super;"><?= $proyecto['titulo']?></span>
						</span>
					</a>
				</div>
			</div>
			<div class="d-block d-md-none">
				<div class="d-flex">
					<span class="d-inline-flex align-items-center text-sm text-mutted-2 mr-15">
						<i class="fi-rr-briefcase me-1"></i> 
						<span class="align-middle">
							<?=view_cell('App\Libraries\FuncionesSistema::diferenciaCantidadTiempo', ['inicio' => $proyecto['fecha_arranque_esperado'], 'fin' => $proyecto['fecha_entrega']])?>
						</span>
					</span>
					<span class="d-inline-flex align-items-center text-sm text-mutted-2">
						<?= view_cell('App\Libraries\FuncionesSistema::nivelInsigniaSolicitud', ['nivel' => $proyecto['nivel']]) ?>
					</span>
				</div>
			</div>
			<div class="d-none d-md-block">
				<span class="d-inline-flex align-items-center text-sm text-mutted-2 mr-15">
					<i class="fi-rr-briefcase me-1"></i> 
					<span class="align-middle">
						<?=view_cell('App\Libraries\FuncionesSistema::diferenciaCantidadTiempo', ['inicio' => $proyecto['fecha_arranque_esperado'], 'fin' => $proyecto['fecha_entrega']])?>
					</span>
				</span>
				<span class="d-inline-flex align-items-center text-sm text-mutted-2">
					<?= view_cell('App\Libraries\FuncionesSistema::nivelInsigniaSolicitud', ['nivel' => $proyecto['nivel']]) ?>
				</span>
			</div>
			<div class="text-small mt-15 d-none d-md-block">
				<p class="m-0 text-truncate">
					<?=$proyecto['desc_corta']?>
				</p>
			</div>
			<div class="card-2-bottom mt-3">
				<div class="row">
					<div class="col-lg-6 col-6">
						<span class="text-brand-10 text-icon-first d-none d-md-block"><?=view_cell('App\Libraries\FuncionesSistema::nameContratista', ['id' => $proyecto['id_contratista']])?></span>
					</div>
					<div class="col-lg-6 col-12 text-end d-none d-md-block">
						<span class="card-text-price"> $ <?= number_format($proyecto['presupuesto'], 2, '.', ',') ?> <small>MXN</small><span>/Presupuesto</span> </span>
					</div>
					<div class="col-12 text-center d-block d-sm-none">
						<span class="card-text-price"> $ <?= number_format($proyecto['presupuesto'], 2, '.', ',') ?> <small>MXN</small><span>/Presupuesto</span> </span>
					</div>
				</div>
				<div class="row mt-20 align-items-center">
					<?php if($proyecto['estatus'] == 'espera') :?>
						<div class="col-lg-2 col-6 text-center">
							<div class="tooltips">
								<button class="btn btn-compartir-soli" data-id="<?= $proyecto['id'] ?>">
									<i class="fas fa-share-square text-dark"></i>
								</button>
								<span class="tooltiptext">Compartir solicitud</span>
							</div>
						</div>
						<div class="col-lg-4 col-6 text-center">
							<button class="btn btn-default rechazar-solicitud" data-contratista="<?=$proyecto['id_contratista']?>" data-id="<?= $proyecto['id'] ?>" data-modal="#modal-solicitud" style="color: #000;font-size: 14px;border: 0px;text-decoration: underline;padding: 6px 7px;background-color: transparent;border-radius: 10px;">
								Declinar
							</button>
						</div>
						<div class="col-lg-6 col-12 text-end">
							<a class="btn btn-default w-100" style="color: #ffffff; font-size: 14px; border-radius: 10px;" href="<?= base_url('usuarios/solicitud/'.$proyecto['id'])?>">
								Ver propuesta
							</a>
						</div>
					<?php else :?>
						<div class="col-12">
							<form class="d-flex justify-content-center align-items-center" id="quitar-solicitud<?=$proyecto['id']?>" action="<?=base_url('Proyectos/eliminarSolicitudFreelancer')?>" method="POST" enctype="multipart/form-data">
								<div class="tooltips me-4"><i style="color: #003B71; font-size:17px;" class="fas fa-info-circle info_proyecto" aria-hidden="true"></i>
									<span class="tooltiptext">Proyecto en desarrollo por otro freelancer.</span>
								</div>
								<input class="btn btn-primary " type="submit" value="Quitar" form="quitar-solicitud<?=$proyecto['id']?>">
								<input class="hidden invisible" hidden name="solicitud" value="<?=$proyecto['id']?>" id="solicitud<?=$proyecto['id']?>">
							</form>
							<script>
								$('#quitar-solicitud<?=$proyecto['id']?>').submit(function(e){
									e.preventDefault();
									$form = $(this);
									if (alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro de eliminar esta solicitud?', function() {
										if(e.target.solicitud<?=$proyecto['id']?>.value.length == 0) return alertify.notify('UPPS... no se pudo encontrar esta solicitud, recargue la página e inténtelo nuevamente.', 'falla');
										
										$form[0].submit();
									}, function(){}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
								});
							</script>
						</div>
					<?php endif ;?>
				</div>
			</div>
		</div>
	</div>
</div>