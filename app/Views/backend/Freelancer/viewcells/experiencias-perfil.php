<script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css"/>
<?php if (isset($freelancer['experiencia'])) : $es_activo = true; ?>
	<!--<div class="swiper-group-experiencias mySwiper">-->
	<div class="swiper-group-experiencias position-relative mt-2" style="cursor:e-resize;">
		<div class="swiper-wrapper">
			<?php foreach ($freelancer['experiencia'] as $key => $experiencia) : ?>
				<div class="swiper-slide mx-2 d-none d-lg-block">
					<div class="card-grid-3 hover-up d-flex flex-column justify-content-center h-100" style="min-height: 360px;">
						<div class="text-center card-grid-3-image pt-0">
							<h5 class="text-truncate mb-0 mt-0"><?= $experiencia['titulo'] ?></h5>
						</div>
						<div class="card-block-info mt-10" style="min-height: 180px;">
							<p class="text-md text-center overflow-hidden" >
								<?= word_limiter($experiencia['descripcion'], 30) ?>
							</p>
						</div>
						<div class="card-profile text-center">
							<button class="btn btn-default mt-20" data-bs-target="#verExperiencia<?= $key ?>" data-bs-toggle="modal" >Ver descripción completa</button>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
	<div class="d-flex d-lg-none justify-content-center mt-2">
		<button class="btn btn-default" data-bs-target="#verExperiencias" data-bs-toggle="modal">Ver mis experiencias</button>
		<div class="modal fade d-lg-none" id="verExperiencias" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-lg mw-100">
				<div class="modal-content">
					<div class="modal-header justify-content-center border-bottom-0">
						<h5 class="modal-title fw-bold m-0">Mis experiencias</h5>
						<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
					</div>
					<div class="modal-body overflow-hidden p-3">
						<div class="swiper-group-experiencias_movil position-relative">
							<div class="swiper-wrapper">
								<?php foreach ($freelancer['experiencia'] as $key => $experiencia) : ?>
									<div class="swiper-slide mx-2 d-lg-none">
										<div class="card-grid-3 hover-up d-flex flex-column justify-content-center h-100" style="min-height: 360px;">
											<div class="text-center card-grid-3-image pt-0">
												<h5 class="text-truncate mb-0 mt-0"><?= $experiencia['titulo'] ?></h5>
											</div>
											<div class="card-block-info mt-10" style="min-height: 180px;">
												<p class="text-md text-center overflow-hidden" >
													<?= word_limiter($experiencia['descripcion'], 30) ?>
												</p>
											</div>
											<div class="card-profile text-center">
												<button class="btn btn-default mt-20" data-bs-target="#verExperiencia<?= $key ?>" data-bs-toggle="modal" >Ver descripción completa</button>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="contenedor-editar-proyectos">
		<?php foreach ($freelancer['experiencia'] as $key => $experiencia) : ?>
			<div class="modal fade" id="verExperiencia<?= $key ?>" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg">
					<div class="modal-content">
						<div class="modal-header justify-content-center border-bottom-0">
							<h5 class="modal-title fw-bold m-0"><?= $experiencia['titulo'] ?></h5>
							<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
						</div>
						<div class="modal-body">
							<div class="container">
								<label class="leyenda-editar d-none" datos="<?=$key?>">Editar información de experiencia</label>
								<form class="row justify-content-center needs-validation" id="form-editarExperiencia<?= $key ?>" enctype="multipart/form-data" novalidate>
									<div class="col-12">
										<input name="experiencia" value="<?= $key ?>" hidden>
										<textarea class="inputs-form text-font altura" data-key="<?=$key?>" name="descripcion" required readonly><?= $experiencia['descripcion'] ?></textarea>
									</div>
									<div class="col-12 text-end mt-2">
										<a class="btn btn-naranja btn-border btn-editFormExperiencia" datos="<?= $key ?>">Editar</a>
										<button class="btn btn-naranja btn-border btn-editarExperiencia d-none" datos="<?= $key ?>">Actualizar</button>
										<button class="btn btn-naranja btn-eliminiarExperiencia" datos="<?= $key ?>">Eliminar</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
<script>
	var objecto_responsivo_carrusel = {
		slidesPerView: 2,
		centeredSlides: true,
		spaceBetween: 30,
		breakpoints: {
			1199: {
				slidesPerView: 2
			},
			800: {
				slidesPerView: 1
			},
			600: {
				slidesPerView: 1
			},
			400: {
				slidesPerView: 1
			},
			350: {
				slidesPerView: 1
			}
		}
	};

	var swiper = new Swiper('.swiper-group-experiencias', objecto_responsivo_carrusel);
	var swiper_movil = new Swiper('.swiper-group-experiencias_movil', objecto_responsivo_carrusel);
	$('.btn-editFormExperiencia').click(function(){
		let id = $(this).attr('datos');
		$('textarea[data-key="'+id+'"]').addClass('edit-form-experiencia').removeAttr('readonly');
		$(this).addClass('d-none');
		$('.btn-editarExperiencia[datos="'+id+'"]').removeClass('d-none');
		$('.leyenda-editar[datos="'+id+'"]').removeClass('d-none');
	});
	$('.btn-eliminiarExperiencia').click(function(event) {
		event.preventDefault();
		$btn = $(this);
		if (alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro de eliminar el proyecto: <b>'+$('#verExperiencia'+$btn.attr('datos')+' h5').text()+'</b>?', function() {
			$.ajax({
				url: '<?=base_url('usuarios/eliminarExperiencia')?>',
				type: 'POST',
				data: {id: $btn.attr('datos')},
				success: function (data) {
					let respuesta = JSON.parse(data);
					$('#verExperiencia'+$btn.attr('datos')).modal('hide').remove();
					alertify.notify(respuesta.mensaje, respuesta.alerta, 10)
					$('#sec-experiencias').load('<?=base_url('usuarios/experienciasUsuario/'.session('id'))?>');
				},
				error: function (data) {
					let respuesta = JSON.parse(data.responseText);
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
				}
			});
		}, function(){}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
	});
	$('.btn-editarExperiencia').click(function(event) {
		event.preventDefault();
		$btn = $(this);
		$.ajax({
			url: '<?=base_url('usuarios/editarExperiencia')?>',
			type: 'POST',
			data: $('#form-editarExperiencia'+$btn.attr('datos')).serialize(),
			success: function (data) {
				let respuesta = JSON.parse(data);
				$('#verExperiencia'+$btn.attr('datos')).modal('hide');
				alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				$('#sec-experiencias').load('<?=base_url('usuarios/experienciasUsuario/'.session('id'))?>');
			},
			error: function (data) {
				let respuesta = JSON.parse(data.responseText);
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
			}
		});
	});
</script>