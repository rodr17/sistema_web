<div class="mb-3">
    <input id="search" type="text" class="form-control"  placeholder="Buscar bandera para el idioma">
    <br>
    <div class="row">
        <div class="col-6 align-self-center">
            <div class="text-center">
                <label class="fw-bold">
                    Nombre: <span id="nombre-span"></span>
                </label>
                <br>
                <img src="" class="d-none w-50" id="flag">
                <input type="hidden" name="bandera" id="bandera">
            </div>
        </div>
        <div class="col-6" style="max-height: 350px; overflow: auto;">
            <table class="table">
                <thead>
                    <tr>
                        <th>Pais</th>
                    </tr>
                </thead>
                <tbody id="table">
                    <?php foreach($idiomas as $key => $i){ ?>
                        <tr>
                            <td class="px-0">
                                <div class="form-check p-0">
                                    <input class="form-check-input d-none" type="radio" id="pais<?=$key?>sel" name="pais" value="<?=$i['identificador']?>" >
                                    <label class="form-check-label d-flex justify-content-between align-items-center" for="pais<?=$key?>sel">
                                        <?=$i['pais']?>&nbsp;&nbsp;<img src="<?=$i['bandera']?>" style="max-width: 40px;max-height: 40px;">
                                    </label>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $('#nombre-idioma').on('keyup', function(){
        $('#nombre-span').html($(this).val());
    });
    $('input[name="pais"]').change(function(){
        let pais = $(this).val();
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Administrador/verPais')?>',
            data:{pais:pais},
            success: function(data){
                $('#flag').attr('src', data);
                $('#bandera').val(data);
                $('#flag').removeClass('d-none');
            }, error: function(data){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo', 10);
            }
        });
    });
    $("#search").on("keyup", function() {
        let value = $(this).val().toLowerCase();
        $("#table tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
</script>