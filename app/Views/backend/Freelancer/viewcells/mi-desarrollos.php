<?php if($desarrollos != null){ ?>
    <div class="row m-0">
        <?php foreach($desarrollos as $post){ ?>
            <div class="col-4 p-2">
                <div class="card w-100 border text-center card-trabajo-post">
                    <?php if($post['chat'] == true){?>
                        <div class="card-header bg-transparent border-0 text-center p-0 position-relative w-100">
                            <a class="btn position-absolute boton-ir-chat" href="<?=base_url('/usuarios/chat/'.$post['sala'])?>">
                                <i class="fas fa-comments"></i>
                            </a>
                        </div>
                    <?php } ?>
                    <a class="btn btn-card-proyecto text-dark" href="<?=base_url('/trabajo/'.$post['id_proyecto'])?>">
                        <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } ?>