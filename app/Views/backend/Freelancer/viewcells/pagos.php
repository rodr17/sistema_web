<!-- <h3>Historial de Pagos</h3> -->
<div class="">
    <div class="card-body border-0">
        <div class="row justify-content-center">
            <?php $api = get_Openpay();
            $api[0]['modo'] == 'prueba' ? $url = $api[0]['path_prueba'] . 'paynet-pdf/' . $api[0]['ID'] . '/' : $url = $api[0]['path_produccion'] . 'paynet-pdf/' . $api[0]['ProduccionID'] . '/' ?>
            <?php foreach ($historial_pagos as $key => $pagos) : ?>
                <?php if($pagos['tipo'] != 'paypal') :?>
                    <?php $pago = model('Autenticacion')->get_cargoID($pagos['id_compra']);?>
                    <div class="col-lg-12">
                        <div class="row align-items-center text-center inputs-form mb-4 m-0 bg-<?= ($pago->status == 'completed' || $pago->status == 'active') ? 'completado' : 'enproceso' ?>">
                        <?php $estatus = $pago->status == 'completed' || $pago->status == 'active' ? 'Completado' : ($pago->status == 'In_progress' ? 'En progreso' : $pago->status)?>
                            <h3 class="h5 text-white-old"><strong><?= $estatus ?></strong></h3>
                            <div class="col-6">
                                <label class="text-font text-white-old"><?= strftime('%d/%b/%G', strtotime($pago->creation_date)) ?> <i class="far fa-calendar-alt"></i></label>
                                <label class="text-font text-white-old fw-bold">Monto: $<?= $pago->serializableData['amount'] ?></label><br>
                                <label class="text-font2 text-white-old"><?= $pago->serializableData['description'] ?></label>
                            </div>
                            <div class="col-6">
                                <?php if($pago->status != 'completed' || $pago->status != 'active'): ?>
                                    <?php if($pagos['tipo'] != 'openpay'){ ?>
                                        <a class="btn text-font text-white-old" target="_blank" href="<?= $pago->payment_method->type == 'bank_transfer' ? str_replace('paynet', 'spei', $url) . $pago->id : $url . $pago->payment_method->reference ?>">
                                            Abrir archivo <i class="far fa-share-square fw-bold" aria-hidden="true"></i>
                                        </a>
                                    <?php } ?>
                                <?php endif ;?>
                                <?php if($pago->status == 'completed' || $pago->status == 'active'){ ?>
                                    <?php $solfactura = model('Facturas')->where('id_compra', $pago->id)->first(); $fechaBD = new DateTime($pago['created_at']); $fecha_a_Dia= clone $fechaBD;?>
                                    <?php if($solfactura == null){ ?>
                                        <?php $fecha_a_Dia->modify('last day of this month') ?>
                                        <?php if(date('d-m-Y') < $fecha_a_Dia->format('d').'-'.$fechaBD->format('m-Y') && date('d-m-Y') >= '01-'.$fechaBD->format('m-Y')) :?>
                                            <a class="btn text-font text-white-old solicitar-factura" data-id="<?=$pago->id?>" data-user="<?=$pago->customer_id?>">
                                                Solicitar factura <i class="far fa-share-square fw-bold" aria-hidden="true"></i>
                                            </a>
                                        <?php else :?>
                                            <label>Facturación: <span class="text-capitalize fw-bold text-naranja">Fuera de fecha limite</span></label>
                                        <?php endif ;?>
                                    <?php }else{ ?>
                                        <label>Facturación: <span class="text-capitalize fw-bold"><?=$solfactura['estatus']?></span></label>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php else :?>
                    <div class="col-lg-12">
                        <div class="row align-items-center text-center inputs-form mb-4 m-0 bg-<?= ($pagos['estatus'] == 'completed' || $pagos['estatus'] == 'active') ? 'completado' : 'enproceso' ?>">
                        <?php $estatus = $pagos['estatus'] == 'completed' || $pagos['estatus'] == 'active' ? 'Completado' : ($pagos['estatus'] == 'In_progress' ? 'En progreso' : 'Cancelado')?>
                            <h3 class="h5 text-white-old"><strong><?= $estatus ?></strong></h3>
                            <div class="col-6">
                                <label class="text-font text-white-old"><?= strftime('%d/%b/%G', strtotime($pagos['fecha'])) ?> <i class="far fa-calendar-alt"></i></label>
                                <label class="text-font text-white-old fw-bold">Monto: $<?= $pagos['monto'] ?></label><br>
                                <label class="text-font2 text-white-old">Compra de membresía MEXTEMPS a través de Paypal</label>
                            </div>
                            <div class="col-6">
                                <?php if($pagos['estatus'] == 'completed' || $pagos['estatus'] == 'active'){ ?>
                                    <?php $solfactura = model('Facturas')->where('id_compra', $pagos['id_compra'])->first(); $fechaBD = new DateTime($pago['created_at']); $fecha_a_Dia= clone $fechaBD;?>
                                    <?php if($solfactura == null){ ?>
                                        <?php $fecha_a_Dia->modify('last day of next month');?>
                                        <?php if(date('d-m-Y') < $fecha_a_Dia->format('d').'-'.$fechaBD->format('m-Y') && date('d-m-Y') >= '01-'.$fechaBD->format('m-Y')) :?>
                                            <a class="btn text-font text-white-old solicitar-factura" data-id="<?=$pagos['id_compra']?>">
                                                Solicitar factura <i class="far fa-share-square fw-bold" aria-hidden="true"></i>
                                            </a>
                                        <?php else :?>
                                            <label>Facturación: <span class="text-capitalize fw-bold text-naranja">Fuera de fecha limite</span></label>
                                        <?php endif ;?>
                                    <?php }else{ ?>
                                        <label>Facturación: <span class="text-capitalize fw-bold"><?=$solfactura['estatus']?></span></label>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php endif ;?>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<script>
    $('.solicitar-factura').click(function(){
        let id = $(this).attr('data-id');
        $.ajax({
            type:'POST',
            url: '<?=base_url('/Pagos/datosFacturacionUsuario')?>',
            data:{id:id},
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, 'advertencia', 10);
                $('#form-solicitud-factura').html(cont.mensaje);
                return $('#solicitar-factura').modal('show');
            }, error: function(data){
                alertify.notify('A surgido un error, comuniquese con el equipo de soporte.', 'falla', 10);
            }
        });
    });
</script>