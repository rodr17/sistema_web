<?php if($proyectos != null){ ?>
    <div class="row m-0">
        <?php foreach($proyectos as $post){ ?>
            <div class="col-4 p-2">
                <div class="card w-100 border text-center card-trabajo-post">
                    <a class="btn btn-card-proyecto text-dark" href="<?=base_url('/trabajo/'.$post['id'])?>">
                        <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } ?>