<?php $freelance = model("Autenticacion")->where("id", $id)->first();
$plan = model("ModelPlanes")->where("id_paypal", $freelance["plan"])->orWhere("id_paypal_anual", $freelance["plan"])->first() ?>
<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <button class="nav-link text-font active" id="nav-formulario" data-bs-toggle="tab" data-bs-target="#tab-formulario" type="button" role="tab" aria-controls="tab-formulario" aria-selected="true">Tarjeta</button>
        <button class="nav-link text-font" id="nav-efectivo" data-bs-toggle="tab" data-bs-target="#tab-efectivo" type="button" role="tab" aria-controls="tab-efectivo" aria-selected="false">Efectivo</button>
        <button class="nav-link text-font" id="nav-transferencia" data-bs-toggle="tab" data-bs-target="#tab-transferencia" type="button" role="tab" aria-controls="tab-transferencia" aria-selected="false">Transferencia electrónica</button>
        <button class="nav-link text-font" id="nav-paypal" data-bs-toggle="tab" data-bs-target="#tab-paypal" type="button" role="tab" aria-controls="tab-paypal" aria-selected="false">Paypal</button>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade active show" id="tab-formulario" role="tabpanel" aria-labelledby="nav-formulario">
        <form class="row justify-content-start needs-validation" id="form-editarPerfil" enctype="'multipart/form-data" novalidate>
            <input type="hidden" name="token_id" id="token_id">
            <p class="fw-bold h6 text-start mt-3">Tarjeta Débito/Crédito</p>
            <label class="text-font text-secundario mb-4"><em class="fst-italic">Al utilizar la tarjeta se validará datos correctos haciendo un autorización por <b>$10.00</b> los cuales son devueltos en el momento.</em></label>
            <div class="d-flex justify-content-around">
                <div class="form-floating mb-3 w-50 px-2">
                    <input class="form-control text-font" name="nombre" id="nombrePay" data-openpay-card="holder_name" required>
                    <label class="text-font" for="nombrePay">Nombre Titular</label>
                </div>
                <div class="form-floating mb-3 w-25 px-2">
                    <input class="form-control text-font" name="tarjeta" id="tarjetaPay" maxlength="16" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" data-openpay-card="card_number" required>
                    <label class="text-font" for="tarjetaPay">Número Tarjeta</label>
                </div>
                <div class="form-floating mb-3 w-25 px-2">
                    <input class="form-control text-font" name="mes" id="mesPay" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" data-openpay-card="expiration_month" required>
                    <label class="text-font" for="mesPay">Mes</label>
                </div>
                <div class="form-floating mb-3 w-25 px-2">
                    <input class="form-control text-font" name="año" id="añoPay" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" data-openpay-card="expiration_year" required>
                    <label class="text-font" for="añoPay">Año</label>
                </div>
            </div>
            <div class="d-flex justify-content-start">
                <div class="form-floating mb-3 w-25 px-2">
                    <input class="form-control text-font" type="password" name="cvv" id="cvvPay" maxlength="4" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" data-openpay-card="cvv2" required>
                    <label class="text-font" for="cvvPay">CVV</label>
                </div>
            </div>
            <div class="col-12">
                <button class="btn btn-naranja btn-border mx-2" id="btn-membresia">Continuar registro</button>
                 <input class='invisible text-font inputs-form plan_seleccionado' value="<?= $freelance['plan'] ?>" hidden> 
            </div>
        </form>
    </div>
    <div class="tab-pane fade" id="tab-efectivo" role="tabpanel" aria-labelledby="nav-efectivo">
        <div class="container">
            <form class="row justify-content-start needs-validation" id="form-editarPerfil-efectivo" enctype="multipart/form-data" novalidate>
                <div class="col-12">
                    <div class="row align-items-center text-center mt-4">
                        <h6 class="text-start">Resumen de compra</h6>
                        <div class="col-12">
                            <h6 class="fw-bold">Suscripcion: <label class="fw-bold"><?= $plan['name'] ?></label></h6>
                            <label class="text-font">Costo <span class="fw-bold">$<?= $plan['monto'] ?></span></label>
                            <label class="text-font2">Lorem ipsum dolor, sit amet consectetur adipisicing elit. At amet, a voluptates ex est sint sunt consequuntur eos voluptatum natus</label>
                        </div>
                        <div class="col-12 text-end mt-3">
                            <button class="btn btn-naranja" type="submit" form="form-editarPerfil-efectivo" id="btn_confirm_efectivo_free">Confirmar compra</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="tab-pane fade" id="tab-transferencia" role="tabpanel" aria-labelledby="nav-transferencia">
        <div class="container">
            <form class="row justify-content-start needs-validation" id="form-editarPerfil-transferencia" enctype="multipart/form-data" novalidate>
                <div class="col-12">
                    <div class="row align-items-center text-center mt-4">
                        <h6 class="text-start">Resumen de compra</h6>
                        <div class="col-12">
                            <h6 class="fw-bold">Suscripcion: <label class="fw-bold"><?= $plan['name'] ?></label></h6>
                            <label class="text-font">Costo <span class="fw-bold">$<?= $plan['monto'] ?></span></label>
                            <label class="text-font2">Lorem ipsum dolor, sit amet consectetur adipisicing elit. At amet, a voluptates ex est sint sunt consequuntur eos voluptatum natus</label>
                        </div>
                        <div class="col-12 text-end mt-3">
                            <button class="btn btn-naranja" id="btn_confirm_transferencia_free">Confirmar compra</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="tab-pane fade" id="tab-paypal" role="tabpanel" aria-labelledby="nav-paypal">
        <div class="container">
            <div class="row justify-content-center align-items-center text-center mt-4">
                <div class="col-6">
                    <div id="paypal-button-container-free"></div>
                </div>
            </div>
        </div>
    </div>
</div>