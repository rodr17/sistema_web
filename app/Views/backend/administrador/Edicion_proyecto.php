<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Proyecto <?= $proyecto['titulo']?>
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="p-2">
		<a class="text-primary h6" href="<?=base_url('listado-proyectos')?>"><i class="fas fa-arrow-left"></i> Regresar a lista de proyectos</a>
		<div class="py-2 d-grid mt-3">
			<h4 class="titulo-label">Editando información del proyecto:  <strong class="text-truncate" id="titulo_proyecto"><?= $proyecto['titulo']?></strong></h4>
			<label>
				Proyecto creado desde: <strong><?= view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $proyecto['created_at']]) ?></strong>
			</label>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title">Informacion del proyecto</h5>
					<form class="row justify-content-center" id="form_editar_proyecto" enctype="multipart/form-data">
						<input name="proyecto" type="hidden" value="<?=$proyecto['id']?>">
						<div class="col-12 col-md-6 text-center">
							<label class="fw-bold">Imágenes</label>
							<?=view_cell('App\Libraries\FuncionesSistema::carruselProyecto', ['id' => $proyecto['id']])?>
						</div>
						<div class="col-12 col-md-6">
							<div class="row">
								<label class="fw-bold">Datos</label>
								<div class="col-12">
									<div class="form-floating mb-3">
										<input type="text" class="form-control" id="titulo" name="titulo" value="<?=$proyecto['titulo']?>" placeholder="Nombre del proyecto">
										<label for="nombre">Título</label>
									</div>
								</div>
								<div class="col-12">
									<div class="form-floating mb-3">
										<select class="form-select" id="listcategoria" name="categorias" tabindex="-1" aria-hidden="true" required>
											<option hidden value="NaN">Seleccione un área</option>
											<?php foreach ($habilidades as $key => $habilidad) : ?>
												<option value="<?=$habilidad['area']?>" <?= $habilidad['area'] == $proyecto['categoria'] ? 'selected' : ''?>><?=$habilidad['area']?></option>
											<?php endforeach; ?>
										</select>
										<label for="listcategoria">Áreas</label>
									</div>
								</div>
								<div class="col-12">
									<div class="form-floating mb-3">
										<select class="form-select" id="listasubcategoria" name="subcategorias" data-select-id="1" tabindex="-1" aria-hidden="true" required>
											<option value="NaN">Seleccione una opción</option>
											<option value="<?=$proyecto['subcategoria']?>" selected><?=$proyecto['subcategoria']?></option>
										</select>
										<label for="listcategoria">Subáreas</label>
									</div>
								</div>
								<div class="col-12">
									<div class="form-floating">
										<input type="text" class="form-control validanumericos" id="presupuesto" name="presupuesto" value="<?=$proyecto['presupuesto']?>" placeholder="Presupuesto">
										<label for="nombre">Presupuesto</label>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 mt-3">
							<div class="row">
								<div class="col-12 col-md-6">
									<label class="fw-bold">Descripción corta</label>
									<div class="form-floating mb-3">
										<textarea type="text" class="form-control " id="descripcion_corta" name="descripcion_corta" value="" placeholder="Descripción del proyecto" style="height: 118px; resize: none;"><?= $proyecto['desc_corta']?></textarea>
										<label for="telefono">Descrición corta</label>
									</div>
								</div>
								<div class="col-12 col-md-6">
									<label class="fw-bold">Descripcion larga</label>
									<div class="form-floating mb-3">
										<textarea type="text" class="form-control " id="descripcion_larga" name="descripcion_larga" value="" placeholder="Descripción del proyecto" style="height: 118px; resize: none;"><?= $proyecto['descripcion']?></textarea>
										<label for="telefono">Descrición</label>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 py-2 text-center">
							<button class="btn btn-primary" type="submit">Guardar cambios</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#listcategoria').change(function(e) {
			let area = $(this).val();
			$.ajax({
				type: 'POST',
				url: '<?=base_url('Habilidades/subareas')?>',
				data:{area:area},
				success: function(data){
					$('#listasubcategoria').html(data);
				}, error: function(data){
					let respuesta = JSON.parse(data.responseText);
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
				}
			});
		});

		$('#form_editar_proyecto').submit(function(evento){
			evento.preventDefault();
			$this = $(this);
			$.ajax({
				type: 'POST',
				url: '<?=base_url('Administrador/editarProyecto')?>',
				data: $this.serialize(),
				success: function(respuesta){
					let datos = JSON.parse(respuesta);
					if(datos.alerta != 'success') return alertify.warning(datos.mensaje);

					$('#titulo_proyecto').text($('#titulo').val());
					return alertify.success(datos.mensaje);
				}, error: function(data){
					let respuesta = JSON.parse(data.responseText);
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
				}
			});
		});
	});
</script>
<?=$this->endSection()?>