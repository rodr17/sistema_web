<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Agregar habilidad
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="p-2">
		<div class="row">
			<div class="col-12 col-md-6 col-lg-3">
				<h4 class="titulo-label">Agregar habilidad</h4>
			</div>
			<div class="col-12 col-md-6 col-lg-9">
				<a class="btn btn-primary" href="<?=base_url('/habilidades')?>">Listado de habilidades</a>
			</div>
		</div>
	</div>
	<hr>
	<div class="row justify-content-center">
		<div class="col-12 col-lg-10">
			<div id="sec-nueva-habilidad"></div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		// $('#solicitudes').load('<?=base_url('/Administrador/solicitudAreaHabilidad')?>');
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/habilidades/agregarHabilidad')?>',
			data:{id:1},
			success: function(data){
				$('#sec-nueva-habilidad').html(data);
			}, error: function(data){
				alertify.error('Ha surgido un problema, comuniquese con el equipo de desarrollo.', 10);
			}
		});
	});
</script>
<?=$this->endSection()?>