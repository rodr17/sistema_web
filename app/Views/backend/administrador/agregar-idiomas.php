<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Agregar Idioma
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="row align-items-center p-2">
		<div class="col-12 col-md-6 col-lg-3">
			<h4 class="titulo-label mb-0">Agregar idioma</h4>
		</div>
		<div class="col-12 col-md-6 col-lg-9">
			<a class="btn btn-primary btn-sm" href="<?=base_url('/idiomas')?>">Listado de idiomas</a>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-12 col-lg-7">
			<div id="sec-add-idioma"></div>
		</div>
		<div class="col-12 col-lg-5">
			<div id="solicitudes-idioma"></div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#solicitudes-idioma').load('<?=base_url('/administrador/solicitudesIdioma')?>');
		$.ajax({
			type: 'POST',
			url: '<?=base_url('Administrador/formCrearIdioma')?>',
			data:{},
			success: function(data){
				$('#sec-add-idioma').html(data);
			}, error: function(data){
				alertify.error('Ha surgido un error, comuníquese con el equipo de desarrollo.', 10);
			}
		});
	});
</script>
<?=$this->endSection()?>