<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
    Ajustes generales
<?=$this->endSection()?>

<?=$this->section('content')?>
<!-- Editor de codigo -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.32.0/codemirror.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.32.0/codemirror.min.css" />
<!--<link rel="stylesheet" href="<?=base_url('/assets/css/monokai.css')?>">-->
<style>
    .CodeMirror-gutter.CodeMirror-linenumbers{
        width: 29px !important;
    }
    .CodeMirror-sizer{
        min-height: 203px;
        margin-left: 30px !important;
    }
</style>
<!-- Selector de colores -->
<link rel="stylesheet" media="screen" type="text/css" href="<?=base_url('assets/librerias/farbtastic/farbtastic.css')?>" />
<script type="text/javascript" src="<?=base_url('assets/librerias/farbtastic/farbtastic.js')?>"></script>
<div class="container">
    <div class="p-2">
        <h4 class="titulo-label">Ajustes generales</h4>
    </div>
    <hr>
    <div class="row">
        <div class="col-12 col-lg-3">
            <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <button class="nav-link active" id="v-pills-home-tab" data-bs-toggle="pill" data-bs-target="#v-pills-home" type="button" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    Colores de iconos animados
                </button>
                <button class="nav-link" id="btn_codigosGoogle" data-bs-toggle="pill" data-bs-target="#tab-codigosGoogle" type="button" role="tab" aria-controls="tab-codigosGoogle" aria-selected="false">
                    Códigos en cabecera del sitio
                </button>
                <button class="nav-link" id="btn-codigosFooter" data-bs-toggle="pill" data-bs-target="#tab-codigos-footer" type="button" role="tab" aria-controls="tab-codigos-footer" aria-selected="false">
                    Códigos en footer del sitio
                </button>
                <button class="nav-link" id="btn-codigos-meta" data-bs-toggle="pill" data-bs-target="#tab-codigos-meta" type="button" role="tab" aria-controls="tab-codigos-meta" aria-selected="false">
                    Códigos meta de cabecera
                </button>
            </div>
        </div>
        <div class="col-12 col-lg-9">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                    <form id="form-colores">
                        <div class="row">
                            <div class="col-12 col-md-6 text-center">
                                <label>Color principal</label>
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" id="color-prim" name="principal" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'principal'])?>" placeholder="...">
                                    <label for="color-prim">Color principal</label>
                                </div>
                                <div id="colorpicker" style="text-align: -webkit-center;"></div>
                            </div>
                            <div class="col-12 col-md-6 text-center">
                                <label>Color secundario</label>
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" id="color-sec" name="secundario" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'secundario'])?>" placeholder="...">
                                    <label for="color-sec">Color secundario</label>
                                </div>
                                <div id="colorpicker-two" style="text-align: -webkit-center;"></div>
                            </div>
                            <div class="col-12 my-3 text-center">
                                <a class="btn btn-primary" id="save-colores">
                                    Guardar cambios
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="tab-codigosGoogle" role="tabpanel" aria-labelledby="btn_codigosGoogle">
                    <div class="">
                        <label>
                            Ingrese los codigos que requiere en la cabecera del sitio.
                        </label>
                        <textarea id="cabecera"></textarea>
                        <div class="p-3 text-center">
                            <a class="btn btn-primary" id="save-cabecera">
                                Guardar
                            </a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-codigos-footer" role="tabpanel" aria-labelledby="btn-codigosFooter">
                    <div class="">
                        <label>
                            Ingrese los codigos que requiere en el footer del sitio..
                        </label>
                        <textarea id="footer"></textarea>
                        <div class="p-3 text-center">
                            <a class="btn btn-primary" id="save-footer">
                                Guardar
                            </a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-codigos-meta" role="tabpanel" aria-labelledby="btn-codigos-meta">
                    <div class="">
                        <label>
                            Ingrese los codigos meta para su uso posicionamiento web
                        </label>
                        <textarea id="metas"></textarea>
                        <div class="p-3 text-center">
                            <a class="btn btn-primary" id="save-metas">
                                Guardar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var cabecera = CodeMirror.fromTextArea(document.getElementById("cabecera"), {
        lineNumbers: true,
        mode: "javascript",
        tabSize: 2
    });
    var footer = CodeMirror.fromTextArea(document.getElementById("footer"), {
        lineNumbers: true,
        mode: "javascript",
        tabSize: 2
    });
    var metas = CodeMirror.fromTextArea(document.getElementById("metas"), {
        lineNumbers: true,
        mode: "javascript",
        tabSize: 2
    });
    $(document).ready(function(){
        $('#colorpicker').farbtastic('#color-prim');
        $('#colorpicker-two').farbtastic('#color-sec');
    });
    $('#save-cabecera').click(function(){
        let codigos = cabecera.doc.getValue();
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Secciones/saveCodigosCabecera')?>',
            data:{codigos:codigos},
            success: function(data){
                alert(data);
            }, error: function(data){
                alertify.error('Ha surgido un error en el servidor.');
            }
        });
    });
    $('#save-colores').click(function(){
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Secciones/saveColores')?>',
            data: $('#form-colores').serialize(),
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
                return alertify.success(cont.mensaje, 10);
            }, error: function(data){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo', 10);
            }
        });
    });
</script>
<?=$this->endSection()?>