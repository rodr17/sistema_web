<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
    Configuración de Aviso de privacidad
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
    <form id="aviso-priv">
        <div class="col-12">
            <h3>Contenido del aviso de privacidad</h3>
        </div>
        <div class="mb-4">
            <textarea id="postBody" name="postBody">
                <?php
                    if($aviso == null){
                        echo '';
                    }else{
                        $data = json_decode($aviso['info'], true);
                        echo $data['body'];
                    }
                ?>
            </textarea>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <a class="btn btn-primary" id="save-aviso">
                    Guardar cambios
                </a>
            </div>
        </div>
    </form>
</div>
<script>
    var editor = CKEDITOR.replace('postBody');
    CKEDITOR.config.htmlEncodeOutput;
    CKEDITOR.config.entities = false;
</script>
<?=$this->endSection()?>