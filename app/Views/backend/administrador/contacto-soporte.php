<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
    Medios de contacto en soporte
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
    <div class="p-2">
        <h4 class="titulo-label">Medios de contacto en soporte</h4>
    </div>
    <hr>
    <form id="form-soporte">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="card border-0">
                    <div class="card-body">
                        <h2 class="card-title h5">Numero de chat</h2>
                        <div class="form-check form-switch">
                            <input class="form-check-input" name="activarchat" type="checkbox" role="switch" id="chat-activar" value="1" <?= (view_cell('App\Libraries\FuncionesAdmin::datoSeccionDBAdmin', ['seccion' => 'redes-soporte', 'tipo' => '', 'valor' => 'activochat']) == true)? 'checked' : '' ?>>
                            <label class="form-check-label" for="chat-activar">Activar</label>
                        </div>
                        <p class="card-text">Ingrese el numero de chat de soporte.</p>
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" name="chat" id="chat" placeholder="..." value="<?= view_cell('App\Libraries\FuncionesAdmin::datoSeccionDBAdmin', ['seccion' => 'redes-soporte', 'tipo' => '', 'valor' => 'chat']) ?>">
                            <label for="facebook">Chat para WhatsApp</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="card border-0">
                    <div class="card-body">
                        <h2 class="card-title h5">Correo de formulario</h2>
                        <div class="form-check form-switch">
                            <input class="form-check-input" name="activarcorreo" type="checkbox" role="switch" id="chat-correo" value="1" <?= (view_cell('App\Libraries\FuncionesAdmin::datoSeccionDBAdmin', ['seccion' => 'redes-soporte', 'tipo' => '', 'valor' => 'activocorreo']) == true)? 'checked' : '' ?>>
                            <label class="form-check-label" for="chat-correo">Activar</label>
                        </div>
                        <p class="card-text">Ingrese el correo de para el area de soporte.</p>
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" name="correo" id="correo" placeholder="..." value="<?= view_cell('App\Libraries\FuncionesAdmin::datoSeccionDBAdmin', ['seccion' => 'redes-soporte', 'tipo' => '', 'valor' => 'correo']) ?>">
                            <label for="facebook">Correo de soporte</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="text-center mt-4">
        <a class="btn btn-primary" id="guardar-cambios">
            Guardar cambios
        </a>
    </div>
</div>
<script>
    $('#guardar-cambios').click(function(){
        $.ajax({
            type: 'POST',
            url: '<?= base_url('/Administrador/guardarSoporte') ?>',
            data:$('#form-soporte').serialize(),
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error') return alertify.warning(cont.mensaje);
                alertify.success(cont.mensaje);
            }, error: function(data){
                alertify.error('Ha surgido un error, comuniquese con el equipo de desarrollo.')
            }
        });
    });
</script>
<?=$this->endSection()?>