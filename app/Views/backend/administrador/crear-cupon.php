<?= $this->extend('/backend/administrador/main') ?>
<?= $this->section('title') ?>
Crear cupón
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<style>
	.form-label {
		min-height: 40px;
	}
</style>
<div class="container">
	<div class="p-2">
		<h4 class="titulo-label">Crear cupón</h4>
	</div>
	<hr>
	<div class="row justify-content-center">
		<div class="col-12 col-lg-11">
			<form class="mb-3" id="new-cupon">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">Freelancers</h5>
						<div class="row">
							<div class="col-12 col-lg-6">
								<div class="form-label">
									Ingrese el codigo para el nuevo cupón, utilizar letras y números solamente.
								</div>
								<div class="form-floating mb-3">
									<input type="text" class="form-control" name="codigo" id="codigo" onkeyup="mayus(this)" placeholder="...">
									<label for="codigo">Codigo de cupón</label>
								</div>
								<div class="form-label mb-0">Cantidad de usos del cupón. (Cuántas veces se podrá usar en general el cupón)</div>
								<div class="form-check form-switch">
									<input class="form-check-input" type="checkbox" role="switch" value="true" name="switch-limite-total" id="switch-limite-total">
									<label class="form-check-label" for="switch-limite-total">Ilimitados</label>
								</div>
								<div id="input-limite">
									<div class="form-floating mb-3">
										<input type="text" class="form-control" name="limite" id="limite" onkeypress="return event.charCode>=48 && event.charCode<=57" placeholder="...">
										<label for="limite">Limite de usos</label>
									</div>
								</div>
								<div id="input-porcentaje-freelancer" style="display: none;">
									<label class="form-label mb-0">Porcentaje de descuento del plan.</label>
									<div class="input-group mb-3">
										<input type="number" name="descuento" min="1" max="100" maxlength="3" class="form-control" pattern="[0-9 ]*" placeholder="Ejemplo: 25" onkeypress="return event.charCode>=48 && event.charCode<=57" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" aria-label="Pesos mexicanos">
										<span class="input-group-text">%</span>
									</div>
								</div>
							</div>
							<div class="col-12 col-lg-6">
								<div class="form-label">Selecciona el tipo de cupón</div>
								<div class="form-floating mb-3">
									<select class="form-select" id="tipo_cupon" name="tipo_cupon" aria-label="texto_tipo_cupon">
										<option selected hidden value="">Selecciona una opción</option>
										<option value="Gratuitos">Cuentas gratis</option>
										<option value="Paga">Cuentas de Pago</option>
									</select>
									<label for="tipo_cupon">Tipo de cuenta</label>
								</div>
								<div class="form-label mb-0">Fecha de vigencia del cupón.</div>
								<div class="form-check form-switch">
									<input class="form-check-input" type="checkbox" role="switch" value="true" name="switch-vigencia" id="switch-vigencia">
									<label class="form-check-label" for="switch-vigencia">Ilimitada</label>
								</div>
								<div id="input-vigencia">
									<div class="form-floating mb-3">
										<input type="date" class="form-control" min="<?= date('Y-m-d', strtotime(date('Y-m-d') . '+ 1 days')) ?>" name="vigencia" id="vigencia" placeholder="...">
										<label for="vigencia">Fecha de vigencia</label>
									</div>
								</div>
								<div id="input-tipo-plan-freelancer" style="display: none;">
									<label class="d-block mb-3">Tipo de plan</label>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="tipo_plan" value="mensual" id="mensual">
										<label class="form-check-label" for="mensual">Mensual</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="tipo_plan" value="anual" id="anual">
										<label class="form-check-label" for="anual">Anual</label>
									</div>
								</div>
							</div>
							<div class="col-12 py-2 text-center">
								<input type="hidden" name="rol" hidden class="invisible" value="freelancer">
								<a class="btn btn-primary btn-cupon">Crear</a>
							</div>
						</div>
					</div>
				</div>
			</form>
			<form id="new-cupon-contratantes">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">Contratantes</h5>
						<div class="row">
							<div class="col-12 col-lg-6">
								<div class="form-label">
									Ingrese el codigo para el nuevo cupón, recuerde utilizar letras y numeros solamente para el su estructura.
								</div>
								<div class="form-floating mb-3">
									<input type="text" class="form-control" name="codigo" id="codigo" onkeyup="mayus(this)" placeholder="...">
									<label for="codigo">Codigo de cupón</label>
								</div>
								<div class="form-label mb-0">Cantidad de usos del cupon. (Cuántas veces se podra usar en general el cupón)</div>
								<div class="form-check form-switch">
									<input class="form-check-input" type="checkbox" role="switch" value="true" name="switch-limite-total" id="switch-limite-total-contratantes">
									<label class="form-check-label" for="switch-limite-total-contratantes">Ilimitados</label>
								</div>
								<div id="input-limite-contratante">
									<div class="form-floating mb-3">
										<input type="text" class="form-control" name="limite" id="limite" onkeypress="return event.charCode>=48 && event.charCode<=57" placeholder="...">
										<label for="limite">Limite de usos</label>
									</div>
								</div>
								<div id="input-porcentaje-contratante" style="display: none;">
									<label class="form-label mb-0">Porcentaje de descuento del plan.</label>
									<div class="input-group mb-3">
										<input type="number" name="descuento" min="1" max="100" maxlength="3" class="form-control" pattern="[0-9 ]*" placeholder="Ejemplo: 25" onkeypress="return event.charCode>=48 && event.charCode<=57" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" aria-label="Pesos mexicanos">
										<span class="input-group-text">%</span>
									</div>
								</div>
							</div>
							<div class="col-12 col-lg-6">
								<div class="form-label">Selecciona el tipo de cupón</div>
								<div class="form-floating mb-3">
									<select class="form-select" id="tipo_cupon_contratantes" name="tipo_cupon" aria-label="texto_tipo_cupon">
										<option selected hidden value="">Selecciona una opción</option>
										<option value="Gratuitos">Cuentas gratis</option>
										<option value="Paga">Cuentas de Pago</option>
									</select>
									<label for="tipo_cupon_contratantes">Tipo de cuenta</label>
								</div>
								<div class="form-label mb-0">Fecha de vigencia del cupón.</div>
								<div class="form-check form-switch">
									<input class="form-check-input" type="checkbox" role="switch" value="true" name="switch-vigencia" id="switch-vigencia-contratantes">
									<label class="form-check-label" for="switch-vigencia-contratantes">Ilimitada</label>
								</div>
								<div id="input-vigencia-contratante">
									<div class="form-floating mb-3">
										<input type="date" class="form-control" min="<?= date('Y-m-d', strtotime(date('Y-m-d') . '+ 1 days')) ?>" name="vigencia" id="vigencia" placeholder="...">
										<label for="vigencia">Fecha de vigencia</label>
									</div>
								</div>
								<div id="input-tipo-plan-contratante" style="display: none;">
									<label class="d-block mb-3">Tipo de plan</label>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="tipo_plan" value="mensual" id="mensual-contratante">
										<label class="form-check-label" for="mensual-contratante">Mensual</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="tipo_plan" value="anual" id="anual-contratante">
										<label class="form-check-label" for="anual-contratante">Anual</label>
									</div>
								</div>
							</div>
							<div class="col-12 py-2 text-center">
								<input type="hidden" name="rol" hidden class="invisible" value="contratante">
								<a class="btn btn-primary btn-cupon">Crear</a>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
	function mayus(e) { e.value = e.value.toUpperCase();}
	
	function seleccion_tipo_cupon(input, rol){
		if (input.target.value == 'Gratuitos' || input.target.value.length == 0){
			$('#input-porcentaje-'+rol).fadeOut();
			$('#input-tipo-plan-'+rol).fadeOut();
		} else{
			$('#input-tipo-plan-'+rol).fadeIn();
			$('#input-porcentaje-'+rol).fadeIn();
		}
	}
	
	$('#switch-limite-total').change(() => $('#input-limite').fadeToggle());
	$('#switch-limite-total-contratantes').change(() => $('#input-limite-contratante').fadeToggle());
	
	$('#switch-vigencia').change(() => $('#input-vigencia').fadeToggle());
	$('#switch-vigencia-contratantes').change(() => $('#input-vigencia-contratante').fadeToggle());
	
	$('#tipo_cupon').change((e) => seleccion_tipo_cupon(e, 'freelancer'));
	$('#tipo_cupon_contratantes').change((e) => seleccion_tipo_cupon(e, 'contratante'));
	
	$('.btn-cupon').click(function() {
		let formulario = $(this).parents('form').attr('id');
		if(formulario.length == 0) return;

		$.ajax({
			type: 'POST',
			url: '<?= base_url('Pagos/saveCupon') ?>',
			data: $('#'+formulario).serialize(),
			success: function(data) {
				let cont = JSON.parse(data);
				if(cont.tipo == 'error') return alertify.warning(cont.mensaje);
				alertify.success(cont.mensaje);
				
				$('#input-limite').fadeIn();
				$('#input-limite-contratante').fadeIn();
				$('#input-vigencia-contratante').fadeIn();
				$('#input-vigencia').fadeIn();
				$('#input-porcentaje-freelancer, #input-tipo-plan-freelancer, #input-tipo-plan-contratante, #input-porcentaje-contratante').fadeOut();
				return $('#'+formulario)[0].reset();
			},
			error: function(data) {
				alertify.error('Ha surgido un error, inténtelo de nuevo.');
			}
		});
	});
</script>
<?= $this->endSection() ?>