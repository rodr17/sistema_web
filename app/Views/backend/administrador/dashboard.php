<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
    Dashboard
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container-fluid">
    <div class="p-2">
        <h4 class="titulo-label">Escritorio</h4>
    </div>
    <hr>
    <div class="row justify-content-start">
        <div class="col-md-3 mb-3">
            <div class="card shadow-sm p-3 h-100">
                <lord-icon
                    src="https://cdn.lordicon.com/eszyyflr.json"
                    trigger="loop"
                    delay="1000"
                    colors="primary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'principal'])?>,secondary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'secundario'])?>"
                    style="width:50px;height:50px;margin:0 auto;"
                    state="hover-jump">
                </lord-icon>
                <label class="mb-0 text-center fw-bold">Usuarios</label>
                <div class="row mt-2">
                    <div class="col-12">
                        <p class="mb-0" style="font-size:12px;">Administra toda la informacion de los usuarios de sistema.</p>
                    </div>
                    <div class="col-12 mt-2">
                        <ul class="list-group p-0">
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('perfil-administrador')?>">- Mi perfil</a></li>
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?=base_url('lista-freelancers')?>">- Listado de freelancers</a></li>
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?=base_url('lista-contratistas')?>">- Listado de contratantes</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <div class="card shadow-sm p-3 h-100">
                <lord-icon
                    src="https://cdn.lordicon.com/fhtaantg.json"
                    trigger="loop"
                    delay="1000"
                    colors="primary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'principal'])?>,secondary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'secundario'])?>"
                    style="width:50px;height:50px;margin:0 auto;">
                </lord-icon>
                <label class="mb-0 text-center fw-bold">Proyectos</label>
                <div class="row mt-2">
                    <div class="col-12">
                        <p class="mb-0" style="font-size:12px;">Administra todos los proyectos de contratantes.</p>
                    </div>
                    <div class="col-12 mt-2">
                        <ul class="list-group p-0">
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('')?>">- Agregar proyecto</a></li>
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('listado-proyectos')?>">- Todos los proyectos</a></li>
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('listado-proyectos?estatus=cancelado')?>">- Proyectos cancelados</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <div class="card shadow-sm p-3 h-100">
                <lord-icon
                    src="https://cdn.lordicon.com/nocovwne.json"
                    trigger="loop"
                    delay="1000"
                    colors="primary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'principal'])?>,secondary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'secundario'])?>"
                    style="width:50px;height:50px;margin:0 auto;">
                </lord-icon>
                <label class="mb-0 text-center fw-bold">Noticias</label>
                <div class="row mt-2">
                    <div class="col-12">
                        <p class="mb-0" style="font-size:12px;">Administra todas las noticias del blog.</p>
                    </div>
                    <div class="col-12 mt-2">
                        <ul class="list-group p-0">
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('nueva-noticia')?>">- Agregar noticia</a></li>
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('administrar-noticias')?>">- Todas las noticias</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <div class="card shadow-sm p-3 h-100">
                <lord-icon
                    src="https://cdn.lordicon.com/joeivjls.json"
                    trigger="loop"
                    delay="1000"
                    colors="primary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'principal'])?>,secondary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'secundario'])?>"
                    style="width:50px;height:50px;margin:0 auto;">
                </lord-icon>
                <label class="mb-0 text-center fw-bold">Contenidos</label>
                <div class="row mt-2">
                    <div class="col-12">
                        <p class="mb-0" style="font-size:12px;">Gestiona informacion de la portada, ajustes generales y paginas de contenido.</p>
                    </div>
                    <div class="col-12 mt-2">
                        <ul class="list-group p-0">
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('paginas')?>">- Todas las páginas</a></li>
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('ajustes-generales')?>">- Ajustes generales</a></li>
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('editar-testimonios')?>">- Testimonios</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <div class="card shadow-sm p-3 h-100">
                <lord-icon
                    src="https://cdn.lordicon.com/cjieiyzp.json"
                    trigger="loop"
                    delay="1000"
                    colors="primary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'principal'])?>,secondary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'secundario'])?>"
                    style="width:50px;height:50px;margin:0 auto;">
                </lord-icon>
                <label class="mb-0 text-center fw-bold">Membresías</label>
                <div class="row mt-2">
                    <div class="col-12">
                        <p class="mb-0" style="font-size:12px;">Gestiona los planes de pago y sus caracteristicas.</p>
                    </div>
                    <div class="col-12 mt-2">
                        <ul class="list-group p-0">
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="#">- Agregar membresía</a></li>
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('membresias')?>">- Todas las membresías</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <div class="card shadow-sm p-3 h-100">
                <lord-icon
                    src="https://cdn.lordicon.com/qhviklyi.json"
                    trigger="loop"
                    delay="1000"
                    colors="primary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'principal'])?>,secondary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'secundario'])?>"
                    style="width:50px;height:50px;margin:0 auto;">
                </lord-icon>
                <label class="mb-0 text-center fw-bold">Pagos</label>
                <div class="row mt-2">
                    <div class="col-12">
                        <p class="mb-0" style="font-size:12px;">Administra los pagos realizados por los clientes en el sistema.</p>
                    </div>
                    <div class="col-12 mt-2">
                        <ul class="list-group p-0">
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('listado-de-pagos')?>">- Pagos de usuarios</a></li>
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('solicitudes-de-facturacion')?>">- Solicitudes de facturación</a></li>
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('estadisticas-generales')?>">- Estadísticas</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <div class="card shadow-sm p-3 h-100">
                <lord-icon
                    src="https://cdn.lordicon.com/ibgjiwvi.json"
                    trigger="loop"
                    delay="1000"
                    colors="primary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'principal'])?>,secondary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'secundario'])?>"
                    style="width:50px;height:50px;margin:0 auto;">
                </lord-icon>
                <label class="mb-0 text-center fw-bold">Habilidades</label>
                <div class="row mt-2">
                    <div class="col-12">
                        <p class="mb-0" style="font-size:12px;">Gestiona las habilidades disponibles y solicitadas para el sistema.</p>
                    </div>
                    <div class="col-12 mt-2">
                        <ul class="list-group p-0">
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('agregar-habilidad')?>">- Agregar habilidad</a></li>
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('habilidades')?>">- Todas las habilidades</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <div class="card shadow-sm p-3 h-100">
                <lord-icon
                    src="https://cdn.lordicon.com/sbiheqdr.json"
                    trigger="loop"
                    delay="1000"
                    colors="primary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'principal'])?>,secondary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'secundario'])?>"
                    style="width:50px;height:50px;margin:0 auto;">
                </lord-icon>
                <label class="mb-0 text-center fw-bold">Configuraciones</label>
                <div class="row mt-2">
                    <div class="col-12">
                        <p class="mb-0" style="font-size:12px;">Gestiona datos de conexiones api, información de contactos y más.</p>
                    </div>
                    <div class="col-12 mt-2">
                        <ul class="list-group p-0">
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('metodos-de-pago')?>">- Métodos de pago</a></li>
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('metodos-de-ingreso')?>">- Métodos de ingreso</a></li>
                            <li class="list-group-item p-0 border-0"><a class="text-decoration-none" href="<?= base_url('medios-contacto')?>">- Medios de contacto</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="col-md-3 mb-3"></div>-->
        <!--<div class="col-md-3 mb-3"></div>-->
        <!--<div class="col-md-3 mb-3"></div>-->
    </div>
    <div class="row d-none">
        <div class="col-12 col-lg-6 mb-2">
            <div class="card">
                <div class="card-header">
                    Solicitudes de aprobación
                </div>
                <div class="card-body" id="solicitudes" style="max-height: 400px; overflow: auto;">
                    <?=view_cell('App\Libraries\FuncionesAdmin::verSolicitudesAprobacion')?>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 mb-2">
            <div class="card">
                <div class="card-header">
                    Reportes de freelance
                </div>
                <div class="card-body" id="reportes" style="max-height: 400px; overflow: auto;">
                    <?=view_cell('App\Libraries\FuncionesAdmin::verReportesFreelance')?>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4 mb-2">
            <div class="card">
                <div class="card-header">
                    Informes de registros
                </div>
                <div class="card-body" style="max-height: 400px; overflow: auto;">
                    <label>Usuarios registrados: <strong><?= view_cell('App\Libraries\FuncionesAdmin::totalUsers') ?></strong></label><br>
                    <label>Freelancers: <strong><?= view_cell('App\Libraries\FuncionesAdmin::totalFreelancers') ?></strong></label><br>
                    <label>Contratantes: <strong><?= view_cell('App\Libraries\FuncionesAdmin::totalContratista') ?></strong></label><br>
                    <label>Registrados con Facebook: <strong><?= view_cell('App\Libraries\FuncionesAdmin::tipoCuentaUsers', ['cuenta' => 'facebook']) ?></strong></label><br>
                    <label>Registrados con Google: <strong><?= view_cell('App\Libraries\FuncionesAdmin::tipoCuentaUsers', ['cuenta' => 'google']) ?></strong></label><br>
                    <label>Registrados con correo: <strong><?= view_cell('App\Libraries\FuncionesAdmin::tipoCuentaUsers', ['cuenta' => 'predeterminado']) ?></strong></label>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4 mb-2">
            <div class="card">
                <div class="card-header">
                    Solicitudes de soporte
                </div>
                <div class="card-body" style="max-height: 400px; overflow: auto;" id="reportes-soporte">
                    <?= view_cell('App\Libraries\FuncionesAdmin::reportesSoportes') ?>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4 mb-2">
            <div class="card">
                <div class="card-header">
                    Solicitudes de sugerencia
                </div>
                <div class="card-body" style="max-height: 400px; overflow: auto;">
                    <div class="p-3">
                        Solicitues de idioma (<?=view_cell('App\Libraries\FuncionesAdmin::cantSoliIdiomas')?>)
                        <a class="btn btn-primary btn-sm" href="<?=base_url('/idiomas')?>">Ver más</a>
                    </div>
                    <hr>
                    <div class="p-3">
                        Solicitudes de area / habilidades (<?=view_cell('App\Libraries\FuncionesAdmin::cantSoliHabilidades')?>)
                        <a class="btn btn-primary btn-sm" href="<?=base_url('/agregar-habilidad')?>">Ver más</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="validar-info" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="tite" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content border-0">
            <div class="modal-header border-0">
                <h5 class="modal-title" id="tite">Información de usuario</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body border-0">
                <label class="label">Datos del Freelance</label>
                <br>
                <div id="datos-freelances">
                </div>
                <form id="formdesaprobar" class="my-3" autocomplete="off">
                    <div class="mb-3">
                        <label>Ingrese el motivo por el cual a sido declinada la cuenta del usuario para que pueda realizar los cambios a su perfil.</label>
                    </div>
                    <div class="form-floating mb-3">
                        <textarea class="form-control" placeholder="" id="motivo" name="motivo" style="height: 100px"></textarea>
                        <label for="motivo">Ingrese el motivo</label>
                    </div>
                    <input type="hidden" value="" id="idfreelance" name="id">
                    <div class="d-grid gap-2">
                        <button type="button" class="btn btn-primary" id="declinar">Declinar solicitud</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer border-0 d-block">
                <div class="row">
                    <div class="col-12 col-sm-12 col-lg-6 p-1">
                        <div class="d-grid gap-2">
                            <button type="button" class="btn btn-success" id="aprobar-freelance">Aprobar</button>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-6 p-1">
                        <div class="d-grid gap-2">
                            <button type="button" class="btn btn-danger" id="declinar-freelance">Declinar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mostrar-reporte" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="tite-reporte" aria-hidden="">
    <div class="modal-dialog">
        <div class="modal-content border-0">
            <div class="modal-header border-0">
                <h5 class="modal-title" id="tite-reporte">Información de reporte</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body border-0" id="body-reporte">
                
            </div>
            <div class="modal-footer border-0 d-block">
                <div class="row">
                    <div class="col-12 col-md-6 p-1">
                        <div class="d-grid gap-2">
                            <button type="button" class="btn btn-primary" id="validar-reporte" data-id="">Validar reporte</button>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 p-1">
                        <div class="d-grid gap-2">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    //Disable full page
    $("body").on("contextmenu",function(e){
        return false;
    });
    // document.onkeydown = function(){return false};
    $('.ver-reporte').on('click', function(){
        let id = $(this).attr('data-id');
        $('#validar-reporte').attr('data-id', id);
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/administrador/verDatosReporte')?>',
            data:{id:id},
            success: function(data){
                $('#body-reporte').html(data);
            }, error: function(data){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
            }
        });
        $('#mostrar-reporte').modal('show');
    });
    $('#validar-reporte').click(function(){
        let id = $(this).attr('data-id');
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/administrador/validarReporte')?>',
            data:{id:id},
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error'){
                    alertify.warning(cont.mensaje, 10);
                }else{
                    alertify.success(cont.mensaje, 10);
                    setTimeout(function(){
                        location.reload();
                    }, 2000);
                }
            }, error: function(data){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
            }
        });
    });
    $('.validar-info').on('click', function(){
        let id = $(this).attr('data-free');
        $('#idfreelance').val(id);
        $("#validar-info").modal('show');
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/datos-de-freelance')?>',
            data:{id:id},
            success: function(data){
                $('#datos-freelances').html(data);
            }, error: function(data){
                alertify.error('Surgio un error, comuniquese con el area de Desarrollo.', 10);
            }
        });
    });
    $('#declinar').on('click', function(){
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Administrador/declinarUsuario')?>',
            data: $('#formdesaprobar').serialize(),
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error'){
                    alertify.warning(cont.mensaje, 10);
                }else{
                    alertify.success(cont.mensaje, 10);
                    setTimeout(function(){
                        location.reload();
                    }, 3000);
                }
            }, error: function(data){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
            }
        });
    });
    $('#declinar-freelance').on('click', function(){
        $('#formdesaprobar').fadeToggle();
    });
    $('#aprobar-freelance').on('click', function(){
        let id = $('#idfreelance').val();
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/aprobar-usuario')?>',
            data:$('#formdesaprobar').serialize(),
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error'){
                    alertify.error(cont.mensaje);
                }else{
                    $('#validar-info').modal("hide")
                    $('button[data-free="'+id+'"]').remove();
                    alertify.success('Freelancer aprobado.');
                    setTimeout(function(){
                        location.reload();
                    }, 3000);
                }
            }, error: function(data){
                alertify.error('Surgio un error, comuniquese con el area de Desarrollo.', 10);
            }
        });
    });
</script>
<?=$this->endSection()?>