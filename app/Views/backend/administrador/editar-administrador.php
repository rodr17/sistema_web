<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('script')?>
<script src="<?=base_url('/assets/js/imagen.js')?>"></script>
<?=$this->endSection()?>
<?=$this->section('title')?>
    Editar perfil de: <?=$admin['name']?>
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
    <div class="p-2">
        <h4 class="titulo-label">Editar informacion de: <b><?=$admin['name']?></b></h4>
    </div>
    <hr>
    <form id="form-admin" method="POST" enctype="multipart/form-data">
        <div class="row">
            <div class="col-12 col-sm-12 col-lg-4 p-2">
                <h4 class="titulo-label">
                    Imagen de perfil
                </h4>
                <section>
                    <div class="preview">
                        <img src="<?= view_cell('App\Libraries\FuncionesAdmin::imagenAdmin', ['id' => $admin['id']]) ?>" class="img-preview">
                    </div>
                    <a id="triggerUpload" class="btn btn-info">Cambiar imagen</a>
                    <input type="file" id="filePicker" class="imagen-subir" name="filePicker" />
                    <div class="fileName"></div>
                </section>
            </div>
            <div class="col-12 col-sm-12 col-lg-8 p-2">
                <h4 class="titulo-label">
                    Datos de contacto
                </h4>
                <div class="row">
                    <div class="col-12 col-sm-12 col-lg-6">
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" id="name" name="name" placeholder="...." value="<?= view_cell('App\Libraries\FuncionesAdmin::dataAdmin', ['id' => $admin['id'], 'campo' => 'name']) ?>">
                            <label for="name">Usuario</label>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-6">
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="...." value="<?= view_cell('App\Libraries\FuncionesAdmin::dataAdmin', ['id' => $admin['id'], 'campo' => 'nombre']) ?>">
                            <label for="nombre">Nombre</label>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-floating mb-3">
                            <input type="email" class="form-control" id="correo" name="correo" placeholder="...." value="<?= view_cell('App\Libraries\FuncionesAdmin::dataAdmin', ['id' => $admin['id'], 'campo' => 'correo']) ?>">
                            <label for="correo">Correo</label>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-6">
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control input-telefono" id="telefono" name="telefono" placeholder="...." value="<?= view_cell('App\Libraries\FuncionesAdmin::dataAdmin', ['id' => $admin['id'], 'campo' => 'telefono']) ?>">
                            <label for="telefono">Teléfono</label>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-6">
                        <div class="form-floating mb-3">
                            <input type="email" class="form-control" id="correorecuperacion" name="correorecuperacion" placeholder="...." value="<?= view_cell('App\Libraries\FuncionesAdmin::dataAdmin', ['id' => $admin['id'], 'campo' => 'correorecuperacion']) ?>">
                            <label for="correorecuperacion">Correo de recuperación</label>
                        </div>
                    </div>
                </div>
                <hr>
                <input type="hidden" value="<?=$admin['id']?>" name="id" id="id">
                <div class="titulo-label">
                    Permisos de usuario
                </div>
                <div class="p-3 border">
                    <?= view_cell('App\Libraries\FuncionesAdmin::verPermisos', ['id' => $admin['id']]) ?>
                </div>
            </div>
        </div>
    </form>
    <div class="row">
        <div class="col-12 text-center">
            <a class="btn btn-primary" id="actua-administrador">
                Guardar cambios
            </a>
        </div>
    </div>
</div>
<?=$this->endSection()?>