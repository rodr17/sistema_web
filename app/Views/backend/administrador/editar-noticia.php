<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Editar noticia
<?=$this->endSection()?>

<?=$this->section('content')?>
<style>
.image-preview { 
	width: 25%;
	display: block;
	margin: 10px auto 0;
	border-radius: 8px;
}
.input-file-container-razon {
	width: 100%;
}
</style>
<div class="container">
	<form id="aviso-priv">
		<div class="row">
			<div class="col-12 mb-4">
				<div class="titulo-label">
					Editar noticia
				</div>
			</div>
			<div class="col-12 mb-3">
				<div class="form-floating">
					<input type="text" class="form-control" name="titulo" id="input-titulo" value="<?=$noticia['titulo']?>" placeholder="Título de sección">
					<label for="input-titulo">Título de la noticia</label>
				</div>
			</div>
			<div class="col-12 mb-3">
				<div class="form-floating">
					<input type="text" class="form-control" name="autor" id="input-autor" value="<?=$noticia['autor']?>" placeholder="Nombre del autor">
					<label for="input-autor">Nombre del autor</label>
				</div>
			</div>
		</div>
		<input id="id" name="id" type="hidden" value="<?=$noticia['id']?>">
		<div class="col-12">
			<h3>Contenido</h3>
		</div>
		<div class="mb-4">
			<textarea id="postBody" name="postBody">
				<?=$noticia['contenido']?>
			</textarea>
		</div>
		<div class="col-12">
			<h3>Imagen destacada</h3>
		</div>
		<div class="input-file-container-razon">
			<input type="hidden" id="imagencard" name="imagencard" value="<?=str_replace(' ', '+', $noticia['imagen'])?>">
			<input class="form-control" type="file" id="formFile">
			<img src="<?=str_replace(' ', '+', $noticia['imagen'])?>" class="image-preview" alt="preview image">
		</div>
		<div class="row mt-4">
			<div class="col-12 text-center">
				<a class="btn btn-primary" id="save-noticia">Guardar cambios</a>
			</div>
		</div>
	</form>
</div>
<script>
	var editor = CKEDITOR.replace('postBody');
	CKEDITOR.config.htmlEncodeOutput;
	CKEDITOR.config.entities = false;
	CKEDITOR.config.fullPage = false;
	CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
	CKEDITOR.config.removeFormatTags = 'b,big,code,del,dfn,em,font,i,ins,kbd,p';
	$("#save-noticia").click(function(){
		var postTitle = $("#input-titulo").val();
		var id = $('#id').val();
		var img = $('#imagencard').val();
		var editorData = editor.getData();
		var postBody = editorData.replace(/&nbsp;/gi,' ');
		var dataString = 'id=' + id + '&title=' + postTitle + '&body=' + postBody + '&imagen=' + img + '&autor=' + $("#input-autor").val();
		// alert(dataString);
		$.ajax({
			type: "POST",
			url: "<?=base_url('/Administrador/updateNoticia')?>",
			data: dataString,
			cache: false,
			success: function(data){
				let cont = JSON.parse(data);
				alertify.set('notifier','position', 'top-right');
				if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
				
				alertify.success(cont.mensaje, 10);
				$('#new-sec')[0].reset();
			},error:function(data){
				alertify.set('notifier','position', 'top-right');
				alertify.error('Error', 10);
			}
		});
		return false;
	});
	const avatarInput = document.querySelector('#formFile');
	const imagePreview = document.querySelector('.image-preview');
	avatarInput.addEventListener('change', e => {
		let input = e.currentTarget;
		const fileReader = new FileReader();
		fileReader.addEventListener('load', e => {
			let imageData = e.target.result;
			if(imageData == null){
				imagePreview.setAttribute('src','https://i.ibb.co/0Jmshvb/no-image.png');
				$('#imagencard').val('');
			}else{
				imagePreview.setAttribute('src', imageData);
				$('#imagencard').val(imageData);
			}
		})
		if(input.files[0] != null){
			fileReader.readAsDataURL(input.files[0]);
		}else{
			imagePreview.setAttribute('src','https://i.ibb.co/0Jmshvb/no-image.png');
			$('#imagencard').val('');
		}
	});
</script>
<?=$this->endSection()?>