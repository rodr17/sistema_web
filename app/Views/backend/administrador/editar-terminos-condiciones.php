<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
    Editar sección a terminos y condiciones
<?=$this->endSection()?>

<?=$this->section('content')?>
<?php $data = json_decode($termino['info'], true); ?>
<div class="container">
    <form id="edit-sec">
        <div class="row">
            <input type="hidden" id="idsec" value="<?=$termino['id']?>">
            <div class="col-12 mb-3">
                <div class="form-floating">
                    <input type="text" class="form-control" name="titulo" id="input-titulo" value="<?=$data['titulo']?>" placeholder="Título de sección">
                    <label for="input-titulo">Título de sección</label>
                </div>
            </div>
        </div>
        <div class="col-12">
            <h3>Contenido</h3>
        </div>
        <div class="mb-4">
            <textarea id="postBody" name="postBody"><?=$data['body']?></textarea>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <a class="btn btn-primary" id="actua-terminos">
                    Guardar cambios
                </a>
            </div>
        </div>
    </form>
</div>
<script>
    var editor = CKEDITOR.replace('postBody');
    CKEDITOR.config.htmlEncodeOutput;
    CKEDITOR.config.entities = false;
</script>
<?=$this->endSection()?>