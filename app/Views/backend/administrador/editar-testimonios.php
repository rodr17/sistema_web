<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Testimonios
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="p-2">
		<h1 class="titulo-label h4">
			Testimonios
		</h1>
	</div>
	<hr>
	<div class="row">
		<div class="col-12 col-lg-3">
			<div class="text-center d-grid gap-2">
				<a class="btn btn-primary sel-test" data-id="1">
					Testimonio 1
				</a>
				<a class="btn btn-primary sel-test" data-id="2">
					Testimonio 2
				</a>
				<a class="btn btn-primary sel-test" data-id="3">
					Testimonio 3
				</a>
				<a class="btn btn-primary sel-test" data-id="4">
					Testimonio 4
				</a>
				<a class="btn btn-primary sel-test" data-id="5">
					Testimonio 5
				</a>
			</div>
		</div>
		<div class="col-12 col-lg-9">
			<form id="form-testimonio">
				<input id="id" name="id" type="hidden" value="">
				<div class="card">
					<div class="card-body">
						<div class="form-floating mb-3">
							<input type="text" class="form-control" id="nombre" name="nombre" placeholder="...">
							<label for="nombre">Nombre de la persona</label>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" id="profesion" name="profesion" placeholder="...">
							<label for="profesion">Profesión de la persona</label>
						</div>
						<input type="hidden" id="imagen" name="imagen" value="">
						<input class="form-control form-control-sm" id="imagen-testitmonio" type="file" onchange="readUrlImagen(this, 'imagen', 'preview');">
						<div class="text-center m-3">
							<img id="preview" class="w-25" src="" alt="Imagen" />
						</div>
						<div class="my-3">
							<label class="mb-3" for="contenido">Texto de sección</label>
							<textarea name="contenido" id="contenido"></textarea>
						</div>
						<div class="text-center">
							<a class="btn btn-primary" id="actua-testimonio">
								Actualizar testimonio
							</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#form-testimonio').hide();
	});
	
	var editor = CKEDITOR.replace('contenido');
	CKEDITOR.config.htmlEncodeOutput;
	CKEDITOR.config.entities = false;
	CKEDITOR.config.fullPage = false;
	CKEDITOR.config.removeFormatTags = 'b,big,code,del,dfn,em,font,i,ins,kbd,p';
	function readUrlImagen(input, guardar, resultado){
		if(input.files && input.files[0]){
			let reader = new FileReader();
			reader.onload = function (e){
				$('#'+resultado).attr('src', e.target.result);
				$('#'+guardar).val(e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		}else{
			$('#'+resultado).attr('src', '<?=base_url('/assets/themes/imgs/page/about/banner-img.png')?>')
		}
	}
	$('.sel-test').click(function(){
		let id = $(this).attr('data-id');
		$.ajax({
			type: 'POST',
			url: '<?= base_url('/Administrador/mostrarDatosTestimonio') ?>',
			data:{id:id},
			success: function(data){
				// alert(data);
				let cont = JSON.parse(data);
				$('#id').val(id);
				$('#nombre').val(cont.nombre);
				$('#profesion').val(cont.profesion);
				$('#imagen').val(cont.imagen);
				$('#preview').attr('src', cont.imagen);
				editor.setData(cont.contenido);
				// $('#contenido').html(cont.contenido);
				$('#form-testimonio').show();
			}, error: function(data){
				alertify.error('Ha surgido un error, comuniquese con el equipo de desarrollo.');
			}
		});
	});
	$('#actua-testimonio').click(function(){
		let nombre = $('#nombre').val();
		let imagen = $('#imagen').val();
		let profesion = $('#profesion').val();
		let id = $('#id').val();
		let editorData = editor.getData();
		let postBody = editorData.replace(/&nbsp;/gi,' ');
		let dataString = 'nombre='+nombre+'&imagen='+imagen+'&id='+id+'&profesion='+profesion+'&contenido='+postBody;
		$.ajax({
			type: 'POST',
			url: '<?=base_url('Administrador/saveTestimonio')?>',
			data: dataString,
			cache: false,
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error'){
					alertify.warning(cont.mensaje, 10);
				}else{
					alertify.success(cont.mensaje, 10);
				}
			}, error: function(data){
				alertify.error('Error, comuniquese con el equipo de desarrollo', 10);
			}
		});
	});
</script>
<?=$this->endSection()?>