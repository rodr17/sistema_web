<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
    Editar <?=$user['rol']?>
<?=$this->endSection()?>

<?=$this->section('content')?>
<style>
    .img-perfil,
    .img-perfil-card {
        width: 175px;
        height: 175px;
        object-fit: contain;
        border-radius: 50%;
    }
    #upload-demo {
        width: 250px;
        height: 250px;
        padding-bottom: 25px;
    }
    
    figure figcaption {
        position: absolute;
        bottom: 0;
        color: #fff;
        width: 100%;
        padding-left: 9px;
        padding-bottom: 5px;
        text-shadow: 0 0 10px #000;
    }
    label.cabinet {
        display: block;
        cursor: pointer;
    }
    label.cabinet input.file {
        position: relative;
        height: 100%;
        width: auto;
        opacity: 0;
        -moz-opacity: 0;
        filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0);
        margin-top: -30px;
    }
</style>
<div class="container">
    <div class="p-2">
        <a class="btn btn-primary" href="<?=base_url('/')?><?= ($user['rol'] == 'freelancer')? '/lista-freelancers' : '/lista-contratistas' ?>">
            Regresar a lista de <?= ($user['rol'] == 'freelancer')? 'freelancers' : 'contratantes' ?>
        </a>
        <div class="py-2 d-grid mt-3">
            <h4 class="titulo-label">Editando información de <strong><?=$user['nombre']?> <?=$user['apellidos']?></strong></h4>
            <h4 class="titulo-label">
                Plan: <strong><?= ($con_suscripcion)? $suscripcion['name'] : 'Gratuito' ?></strong>
            </h4>
            <label>
                Usuario registrado desde: <strong><?= view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $user['created_at']]) ?></strong>
            </label>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12">
            <form id="edit-user">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            Informacion del usuario
                        </h5>
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-4 text-center">
                                <label class="fw-bold">Foto de perfil</label>
                                <label class="cabinet center-block m-auto" style="width: 180px">
                                    <figure class="m-0 user-select-none" style="width: 180px">
                                        <img src="<?= imagenPerfil($user) ?>" class="gambar img-responsive img-thumbnail img-perfil shadow border border-3 m-auto" id="imagen-perfil">
                                    </figure>
                                    <input type="file" class="item-img file center-block" name="imagenPerfil" id="file_imagen_perfil" hidden>
                                    <input class="inputs-form text-font2" id="img_recortada" name="img_recortada" hidden value="<?= imagenPerfil($user) ?>">
                                </label>
                            </div>
                            <div class="col-12 col-md-6 col-lg-8">
                                <label class="fw-bold">Información personal</label>
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control" id="nombre" name="nombre" value="<?= isset($user['nombre'])? $user['nombre'] : '' ?>" placeholder="...">
                                            <label for="nombre">Nombre</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control" id="apellidos" name="apellidos" value="<?= isset($user['apellidos'])? $user['apellidos'] : '' ?>" placeholder="...">
                                            <label for="apellidos">Apellidos</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-floating mb-3">
                                            <input type="email" class="form-control" id="correo" name="correo" value="<?= isset($user['correo'])? $user['correo'] : '' ?>" placeholder="...">
                                            <label for="correo">Correo</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control" id="rfc" name="rfc" value="<?= isset($user['direccion']['rfc'])? $user['direccion']['rfc'] : '' ?>" placeholder="...">
                                            <label for="rfc">RFC</label>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control" id="curp" name="curp" value="<?= isset($user['direccion']['curp'])? $user['direccion']['curp'] : '' ?>" placeholder="...">
                                            <label for="curp">CURP</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <label class="fw-bold">Contacto</label>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control input-telefono" id="telefono" name="telefono" value="<?= isset($user['contacto']['telefono'])? $user['contacto']['telefono'] : '' ?>" placeholder="...">
                                            <label for="telefono">Teléfono</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control" id="facebook" name="facebook" value="<?= isset($user['contacto']['facebook'])? $user['contacto']['facebook'] : '' ?>" placeholder="...">
                                            <label for="telefono">Facebook</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control" id="instagram" name="instagram" value="<?= isset($user['contacto']['instagram'])? $user['contacto']['instagram'] : '' ?>" placeholder="...">
                                            <label for="telefono">Instagram</label>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control input-telefono" id="whatsapp" name="whatsapp" value="<?= isset($user['contacto']['whatsapp'])? $user['contacto']['whatsapp'] : '' ?>" placeholder="...">
                                            <label for="telefono">WhatsApp</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-8">
                                <label class="fw-bold">Dirección</label>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control" id="calle" name="calle" value="<?= isset($user['direccion']['calle'])? $user['direccion']['calle'] : '' ?>" placeholder="...">
                                            <label for="calle">Calle y No. de domicilio.</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control" id="colonia" name="colonia" value="<?= isset($user['direccion']['colonia'])? $user['direccion']['colonia'] : '' ?>" placeholder="...">
                                            <label for="calle">Colonia</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control" id="cp" name="cp" value="<?= isset($user['direccion']['codigo_postal'])? $user['direccion']['codigo_postal'] : '' ?>" placeholder="...">
                                            <label for="cp">Código postal</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control" id="municipio" name="municipio" value="<?= isset($user['direccion']['municipio'])? $user['direccion']['municipio'] : '' ?>" placeholder="...">
                                            <label for="calle">Municipio/Ciudad</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <div class="form-floating mb-3">
                                            <select class="form-select" id="estado" name="estado" aria-label="...">
                                                <?php foreach($estados as $e){ ?>
                                                    <option value="<?=$e['nombre']?>"><?=$e['nombre']?></option>
                                                <?php } ?>
                                            </select>
                                            <label for="estado">Estado</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <div class="form-floating mb-3">
                                            <input type="text" class="form-control" id="pais" name="pais" value="<?= isset($user['direccion']['pais'])? $user['direccion']['pais'] : '' ?>">
                                            <label for="pais">País</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 py-2 text-center">
                                <input type="hidden" value="<?=$user['id']?>" name="id">
                                <a class="btn btn-primary" id="save-cambios">
                                    Guardar cambios
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Recortar imagen -->
<div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Nueva imagen de perfil</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body align-self-center">
				<div id="upload-demo" class="center-block"></div>
			</div>
			<div class="modal-footer">
				<button type ="button" id="cropImageBtnPerfil" class="btn btn-naranja btn-border">Aceptar</button>
			</div>
		</div>
	</div>
</div>
<script src="https://foliotek.github.io/Croppie/croppie.js"></script>
<link href="https://foliotek.github.io/Croppie/croppie.css" rel="stylesheet" type="text/css">
<script>
    if($("#upload-demo").length == 1){
		let $uploadCropPerfil, tempFilename, rawImg, imageId;
		function readFile(input) { 
			if (input.files && input.files[0]) {
				let reader = new FileReader();
				reader.onload = function (e) {
					$(".upload-demo").addClass("ready");
					$("#cropImagePop").modal("show");
					rawImg = e.target.result;
				};
				reader.readAsDataURL(input.files[0]);
			} else {
				alertify.error("Lo sentimos - Tu navegador no soporte la API FileReader");
			}
		}
		$uploadCropPerfil = $("#upload-demo").croppie({
			viewport: {
				width: 200,
				height: 200
			},
			enforceBoundary: false,
			enableExif: true
		});
		$("#cropImagePop").on("shown.bs.modal", function () {
			$uploadCropPerfil
			.croppie("bind", {url: rawImg})
			.then(function () {
				console.log("jQuery bind complete");
			});
		});
	
		$(".item-img").on("change", function () {
			imageId = $(this).data("id");
			tempFilename = $(this).val();
			$("#cancelCropBtn").data("id", imageId);
			readFile(this);
		});
		$("#cropImageBtnPerfil").on("click", function (ev) {
			$uploadCropPerfil.croppie("result", {
				type: "base64",
				format: "webp",
				size: { width: 300, height: 300 }
			})
			.then(function (resp) {
				$("#imagen-perfil").attr("src", resp);
				$('#img_recortada').val(resp);
				$("#cropImagePop").modal("hide");
			});
		});
	}
	$('#save-cambios').click(function(){
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Administrador/saveEditInfoUsuario')?>',
            data:$('#edit-user').serialize(),
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error') return alertify.warning(cont.mensaje);
                return alertify.success(cont.mensaje);
            }, error: function(data){
                let respuesta = JSON.parse(data.responseText);
                alertify.error('Error: ' + respuesta.message + '\n' + respuesta.file);
            }
        });
	});
</script>
<?=$this->endSection()?>