<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Estadisticas generales
<?=$this->endSection()?>

<?=$this->section('content')?>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<div class="container">
	<div class="p-2">
		<h4 class="titulo-label">Estadisticas generales</h4>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12 mb-3">
			<div class="card">
				<div class="card-body">
					<div class="d-flex justify-content-center align-items-center gap-2 w-100" id="daterange" style="cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
						<i class="fa fa-calendar"></i>&nbsp;
						<span></span> <i class="fa fa-caret-down"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-6">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title">Pagos de membresias</h5>
					<div id="graf-pagos"></div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-6">
			<!-- <div class="card mb-2">
				<div class="card-body">
					<h5 class="card-title">Estadisticas de registro</h5>
					<div class="text-center mb-3">
						<div id="rangoregistros" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;">
							<i class="fa fa-calendar"></i>&nbsp;
							<span></span> <i class="fa fa-caret-down"></i>
						</div>
					</div>
					<div id="graf-registros">
					</div>
				</div>
			</div> -->
			<div class="card">
				<div class="card-body">
					<h5 class="card-title">Cantidad de usuarios</h5>
					<!-- <div class="text-center mb-3">
						<div id="rangousuarios" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;">
							<i class="fa fa-calendar"></i>&nbsp;
							<span></span> <i class="fa fa-caret-down"></i>
						</div>
					</div> -->
					<div id="graf-usuarios"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
	$(document).ready(function(){
		resetar_graficos();
	});
	$(function() {
		$('#daterange').daterangepicker({
			"opens": "center",
			"showDropdowns": true,
			"startDate": "<?= date('m-d-o', strtotime($hoy)) ?>",
			"minDate": "<?= date('m-d-o', strtotime($minimo['fecha'].'- 1 year')) ?>",
			"maxDate": "<?= date('m-d-o', strtotime($hoy)) ?>",
			"locale": {
				"format": "MM/DD/YYYY",
				"separator": " - ",
				"applyLabel": "Filtrar",
				"cancelLabel": "Resetear",
				"fromLabel": "De",
				"toLabel": "a",
				"customRangeLabel": "Custom",
				"daysOfWeek": [
					"Do",
					"Lu",
					"Ma",
					"Mi",
					"Ju",
					"Vi",
					"Sa"
				],
				"monthNames": [
					"Enero",
					"Febrero",
					"Marzo",
					"Abril",
					"Mayo",
					"Junio",
					"Julio",
					"Agosto",
					"Septiembre",
					"Octubre",
					"Noviembre",
					"Diciembre"
				],
				"firstDay": 1
			}
		});
		var start = new Date(moment().subtract(29, 'days')).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
		var end = new Date(moment()).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
		function cb(start, end) {
			$('#daterange span').html(start + ' - ' + end);
		}
		cb(start, end);
	});
	$('#daterange').on('apply.daterangepicker', function(ev, picker) {
		let inicio = picker.startDate.format('YYYY-MM-DD');
		let fin = picker.endDate.format('YYYY-MM-DD');
		let inicio_formateado = new Date(moment(inicio)).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
		let fin_formateado = new Date(moment(fin)).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
		$('#daterange span').html(inicio_formateado + ' - ' + fin_formateado);
		graficar(inicio, fin);
	});
	$('#daterange').on('cancel.daterangepicker', function(ev, picker) {
		resetar_graficos();
	});
	function resetar_graficos(){
		$('#graf-pagos').load('<?=base_url('/Administrador/filtradoPagosGrafica')?>');
		// $('#graf-registros').load('<=base_url('/Administrador/filtradoEstadisticasRegistro')?>');
		$('#graf-usuarios').load('<?= base_url('/Administrador/filtradoCantUsuariosGrafica') ?>');
	}
	// $('#rangoregistros').on('apply.daterangepicker', function(ev, picker){
	// 	registro(picker.startDate.format('YYYY-MM-DD'), picker.endDate.format('YYYY-MM-DD'));
	// });
	// function registro(inicio, fin){
	// 	$('#graf-registros').load('<=base_url('/Administrador/filtradoEstadisticasRegistro')?>?inicio='+inicio+'&fin='+fin);
	// }
	function graficar(inicio, fin){
		$('#graf-pagos').load('<?=base_url('/Administrador/filtradoPagosGrafica')?>?inicio='+inicio+'&fin='+fin);
		$('#graf-usuarios').load('<?= base_url('/Administrador/filtradoCantUsuariosGrafica') ?>?inicio='+inicio+'&fin='+fin);
	}
</script>
<?=$this->endSection()?>