<footer class="text-center">
    <div class="mb-2">
        <small>
            © 2022 Powered by - <a target="_blank" class="text-decoration-none" href="https://tresesenta.mx"> Tresesenta&nbsp;&nbsp;<img class="logo-developer" src="<?=base_url('/assets/images/tresesenta-imagotype.svg')?>"></a>
        </small>
    </div>
    <div>
        <a href="" target="_blank">
            <!--<img alt="GitHub followers" src="https://img.shields.io/github/followers/azouaoui-med?label=github&style=social" />-->
        </a>
        <a href="" target="_blank">
            <!--<img alt="Twitter Follow" src="https://img.shields.io/twitter/follow/azouaoui_med?label=twitter&style=social" />-->
        </a>
    </div>
</footer>