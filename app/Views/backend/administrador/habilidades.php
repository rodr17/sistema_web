<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Listado de habilidades
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="p-2">
		<div class="row">
			<div class="col-12 col-md-6 col-lg-3">
				<h4 class="titulo-label">
					Listado de habilidades
				</h4>
			</div>
			<div class="col-12 col-md-6 col-lg-9">
				<a class="btn btn-primary" href="<?=base_url('agregar-habilidad')?>">
					Añadir nueva habilidad o area
				</a>
				<a class="btn btn-primary" href="<?=base_url('/solicitudes-de-habilidades')?>">
					Solicitudes (<?=view_cell('App\Libraries\FuncionesAdmin::cantSoliHabilidades')?>)
				</a>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-12 col-sm-12 col-lg-6 p-2">
			<div class="card border-0">
				<div class="card-title mb-1 p-3 d-flex justify-content-between align-items-center">
					<h6><strong>Areas</strong></h6>
					<div class="d-flex justify-content-center align-items-center gap-2" id="form_buscar_pagos">
						<label for="buscar">Buscar</label>
						<input class="form-control" id="buscar-areas" name="buscador" type="search">
					</div>
				</div>
				<div class="card-body">
					<div class="div-limitadoY" id="tabla-areas">
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-12 col-lg-6 p-2">
			<div class="card border-0">
				<div class="card-title mb-1 p-3 d-flex justify-content-between align-items-center">
					<h6><strong>Sub Areas</strong></h6>
					<div class="d-flex justify-content-center align-items-center gap-2">
						<label for="buscar">Buscar</label>
						<input class="form-control" id="buscar-subareas" name="buscar-subarea" type="search">
					</div>
				</div>
				<div class="card-body">
					<div class="div-limitadoY" id="subareas">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalarea" tabindex="-1" aria-labelledby="editarea" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="editarea">
						Editar area
					</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<div class="form-floating mb-2">
						<input type="text" class="form-control" id="area" placeholder="...">
						<label for="area">Area</label>
					</div>
					<input type="hidden" id="idareaedit">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
					<button type="button" class="btn btn-primary" id="actua-area">Guardar cambios</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalsubarea" tabindex="-1" aria-labelledby="editsubarea" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="editsubarea">
						Editar subarea
					</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body" id="sec-form-subarea">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
					<button type="button" class="btn btn-primary" id="actua-subarea">Guardar cambios</button>
				</div>
			</div>
		</div>
	</div>
	<modal class="modal fade" id="nuevahabilidad" tabindex="-1" aria-labelledby="new" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="new">
						Nueva habilidad
					</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" arua-label="Close"></button>
				</div>
				<div class="modal-body" id="sec-nueva-habilidad">
				</div>
			</div>
		</div>
	</modal>
	<script>
		$(document).ready(function(){
			$('#tabla-areas').load('<?=base_url('/habilidades/tablaAreas')?>');
		});
		function edit(id, area){
			$('#area').val(area);
			$('#idareaedit').val(id);
			$('#modalarea').modal('show');
		}
		function elimiSuperior(id){
			alertify.confirm('ATENCION', 'Esta a punto de eliminar el area superior con el ID = '+id+', todas las habilidades o sub areas seran movidas a otros, ¿Esta seguro de esto?', function(){
				$.ajax({
					type: 'POST',
					url: '<?=base_url('/Habilidades/deleteSuperior')?>',
					data:{id:id},
					success: function(data){
						let cont = JSON.parse(data);
						if(cont.tipo == 'error'){
							alertify.warning(cont.mensaje, 10);
						}else{
							alertify.success(cont.mensaje, 10);
							$('#tabla-areas').load('<?=base_url('/habilidades/tablaAreas')?>');
						}
					}, error: function(data){
						alertify.error('A surgido un error, comuniquese el equipo de desarrollo.', 10);
					}
				});
			}, function(){});
		} 
		$('#new-habilidad').click(function(){
			$.ajax({
				type: 'POST',
				url: '<?=base_url('/habilidades/agregarHabilidad')?>',
				data:{id:1},
				success: function(data){
					$('#sec-nueva-habilidad').html(data);
					$('#nuevahabilidad').modal('show');
				}, error: function(data){
					alertify.error('A surgido un problema, comuniquese con el equipo de desarrollo.', 10);
				}
			});
		});
		$('#actua-area').click(function(){
			$.ajax({
				type: 'POST',
				url: '<?=base_url('/habilidades/actuaArea')?>',
				data:{area:$('#area').val(), id:$('#idareaedit').val()},
				success: function(data){
					let cont = JSON.parse(data);
					if(cont.tipo == 'error'){
						alertify.warning(cont.mensaje, 10);
					}else{
						alertify.success(cont.mensaje, 10);
						$('#tabla-areas').load('<?=base_url('/habilidades/tablaAreas')?>');
						$('#modalarea').modal('hide');
					}
				}, error: function(data){
					alertify.error('Ha surgido un error, comuniquese con el equipo de desarrollo.', 10);
				}
			});
		});
		$('#actua-subarea').click(function(){
			let area = $(this).attr('data-area');
			console.log($('#form-subarea').serialize());
			$.ajax({
				type: 'POST',
				url: '<?=base_url('/habilidades/actuaSubArea')?>',
				data:{subarea:$('#subarea').val(), superior:$('#superior').val(), idsub:$('#idsub').val()},
				success: function(data){
					let cont = JSON.parse(data);
					if(cont.tipo == 'error'){
						alertify.warning(cont.mensaje, 10);
					}else{
						alertify.success(cont.mensaje, 10);
						$('#tabla-areas').load('<?=base_url('/habilidades/tablaAreas')?>');
						$('#subareas').html(cont.data);
						$('#modalsubarea').modal('hide');
					}
				}, error: function(data){
					alertify.error('Ha surgido un error, comunÍquese con el equipo de desarrollo.', 10);
				}
			});
		});
		$('#buscar-areas').keyup(function (e) { 
			let buscar = $(this).val();
			$.ajax({
				type: 'POST',
				url: '<?=base_url('Habilidades/tablaAreas')?>',
				data: {buscar:buscar},
				success: function(data){
					$('#tabla-areas').html(data);
				}, error: function(data){
					alertify.error('Ha surgido un error, comunÍquese con el equipo de desarrollo.', 10);
				}
			});
		});
	</script>
</div>
<?=$this->endSection()?>