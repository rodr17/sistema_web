<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Idiomas
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="p-2">
		<div class="row">
			<div class="col-12 col-md-6 col-lg-3">
				<h4 class="titulo-label">Idiomas</h4>
			</div>
			<div class="col-12 col-md-6 col-lg-9">
				<a class="btn btn-sm btn-primary" href="<?=base_url('agregar-idioma')?>">Añadir nuevo idioma</a>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-12 col-sm-12 col-lg-6 p-2">
			<div class="table-responsive bg-white" id="table-idiomas" style="max-height: 420px; overflow: auto;"></div>
		</div>
		<div class="col-12 col-sm-12 col-lg-6 p-2">
			<div id="edit-idioma" style="display: none;">
				<form id="form-edit-idioma">
					<div class="form-floating mb-3">
						<input type="text" class="form-control" id="nombre-edit" name="nombre" placeholder="...">
						<label for="nombre-edit">Nombre del idioma</label>
					</div>
					<div class="mb-3">
						<div class="row">
							<div class="col-12 align-self-center">
								<div class="text-center">
									<label class="fw-bold">
										Nombre: <span id="nombre-span-edit"></span>
									</label>
									<br>
									<img src="" class="w-50" id="flag-edit">
									<input id="id-edit" type="hidden" value="" name="id">
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="my-3 text-center">
					<a class="btn btn-primary" id="btn-edit-idioma">Guardar cambios</a>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('#table-idiomas').load('<?=base_url('/administrador/tablaIdiomas')?>');
	
	$('#btn-edit-idioma').click(function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Administrador/saveEditIdioma')?>',
			data: $('#form-edit-idioma').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
				$('#edit-idioma').hide();
				$('#table-idiomas').load('<?=base_url('/administrador/tablaIdiomas')?>');
				return alertify.success(cont.mensaje, 10);
			}, error: function(data){
				alertify.error('Ha surgido un error, comuníquese con el equipo de soporte.', 10);
			}
		});
	});
</script>
<?=$this->endSection()?>