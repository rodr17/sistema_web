<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
    Listado de administradores
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
    <div class="p-2">
        <h4 class="titulo-label">Lista de administradores</h4>
    </div>
    <form method="GET" action="<?=base_url('/listado-administradores')?>">
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="buscar" name="search" placeholder="...">
            <label for="buscar">Buscar administrador por...</label>
        </div>
    </form>
    <?php if($_GET){ ?>
        <div class="mt-1 mb-2">
            <h5>
                Resultados de busqueda para: <strong><?= (isset($_GET['search']))? $_GET['search']: 'N/A' ?></strong>
            </h5>
        </div>
    <?php } ?>
    <hr>
    <?php if($administradores == null){ ?>
        <div class="text-center">
            <div class="alert alert-warning" role="alert">
                Sin usuarios de momento
            </div>
        </div>
    <?php }else{ ?>
        <div class="row">
            <?php foreach($administradores as $admin){ ?>
                <div class="col-6">
                    <div class="border p-2 mb-3 bg-light bg-gradient">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex">
                                    <img src="<?= view_cell('App\Libraries\FuncionesAdmin::imagenAdmin', ['id' => $admin['id']]) ?>" class="imagen-admin img-thumbnail">
                                    <div class="px-2">
                                        <span class="name-user"><?=$admin['name']?></span><br>
                                        <label>
                                            Correo : <span class="fst-italic"><?=$admin['correo']?></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <?php if($admin['id'] == session('id_user')){ ?>
                                    <div class="py-2">
                                        <a class="btn btn-primary btn-sm" type="button" href="<?=base_url('/perfil-administrador')?>">Editar mi perfil</a>
                                        <button class="btn btn-info btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#nuevacontra">Cambiar contraseña</button>
                                    </div>
                                <?php }else{ ?>
                                    <div class="py-2">
                                        <a class="btn btn-primary btn-sm" type="button" href="<?=base_url('/editar-administrador?id='.base64_encode($admin['id']))?>">Editar información</a>
                                        <button class="btn btn-info mandar-correo btn-sm" data-id="<?=$admin['id']?>" data-user="<?=$admin['name']?>" data-correo="<?=$admin['correo']?>" type="button" data-bs-toggle="modal" data-bs-target="#formcorreo">Mandar correo</button>
                                        <?php if($admin['tipo'] != 'superadmin'){ ?>
                                            <button class="btn btn-danger elim-admin btn-sm" data-id="<?=$admin['id']?>" type="button">Eliminar usuario</button>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>
<!--ADMIN-->
<div class="modal fade" id="formcorreo" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content border-0">
            <div class="modal-header border-0">
                <h5 class="modal-title" id="staticBackdropLabel">Mandar correo a <span id="nom-admin"></span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body border-0">
                <form class="modal-openpay" id="form">
                    <textarea id="postBody"></textarea>
                    <input type="hidden" id="user-id" val="">
                </form>
            </div>
            <div class="modal-footer border-0 d-block">
                <div class="row">
                    <div class="col-12 col-sm-12 col-lg-6 p-1">
                        <div class="d-grid gap-2">
                            <button type="button" class="btn btn-primary" id="enviar-correo-admin">Enviar correo</button>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-6 p-1">
                        <div class="d-grid gap-2">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="nuevacontra" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content border-0">
            <div class="modal-header border-0">
                <h5 class="modal-title" id="staticBackdrop">Innformación Freelancer</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body border-0">
                <label class="label">Ingrese su nueva contraseña y confirme que este bien escrita.</label>
                <form id="formnewcontra"  autocomplete="off">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="contra" name="contra" placeholder="..."  autocomplete="off">
                        <label for="contra">Nueva contraseña</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="password" class="form-control" id="confirm" name="confirm" placeholder="..."  autocomplete="off">
                        <label for="confirm">Confirmar contraseña</label>
                    </div>
                </form>
            </div>
            <div class="modal-footer border-0 d-block">
                <div class="row">
                    <div class="col-12 col-sm-12 col-lg-6 p-1">
                        <div class="d-grid gap-2">
                            <button type="button" class="btn btn-primary" id="nueva-contra">Actualizar contraseña</button>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-6 p-1">
                        <div class="d-grid gap-2">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?=$this->endSection()?>