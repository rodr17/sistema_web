<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
    Lista de cupones
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
    <div class="p-2">
        <h4 class="titulo-label">Lista de cupones</h4>
        <a class="btn btn-primary" href="<?=base_url('/crear-cupon')?>">
            Crear cupón
        </a>
    </div>
    <hr>
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10">
            <div id="tabla"></div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#tabla').load('<?=base_url('/Pagos/tablaCupones')?>');
    });
</script>
<?=$this->endSection()?>