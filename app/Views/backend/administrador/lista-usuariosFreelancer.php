<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Lista de freelancers
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="p-2">
		<h4 class="titulo-label">Lista de freelancers</h4>
	</div>
	<div class="row justify-content-between align-items-center">
		<div class="col-md-6">
			<form id="form-buscador-freelancers">
				<div class="form-floating d-flex gap-2">
					<input class="form-control" id="buscar" name="search" type="search">
					<label for="buscar">Buscar freelancer</label>
					<input class="btn btn-primary" type="submit" value="Buscar"/>
				</div>
			</form>
		</div>
		<div class="col-md-6">
			<div class="d-flex justify-content-center align-items-center gap-1 bg-white w-100" id="daterange" style="cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
				<i class="fa fa-calendar"></i>&nbsp;
				<span></span> <i class="fa fa-caret-down"></i>
			</div>
		</div>
	</div>
	<hr>
	<div class="row" id="contenido_freelancers">
		<?php if($freelancers == null){ ?>
			<div class="text-center">
				<div class="alert alert-warning" role="alert">Sin freelancers encontrados</div>
			</div>
		<?php }else{ ?>
			<?php foreach($freelancers as $freelancer){ ?>
				<?= view_cell('App\Libraries\FuncionesAdmin::listadoUsuarios', ['usuario' => $freelancer])?>
			<?php } ?>
		<?php } ?>
	</div>
</div>
<!--CONTRATISTAS Y FREELANCERS-->
<div class="modal fade" id="validar-info" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="tite" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content border-0">
			<div class="modal-header border-0">
				<h5 class="modal-title" id="tite">Datos Freelancer</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body border-0">
				<label class="label">Datos del Freelance</label>
				<br>
				<div id="datos-freelances">
				</div>
				<form id="formdesaprobar" class="my-3" autocomplete="off">
					<div class="mb-3">
						<label>Ingrese el motivo por el cual ha sido declinada la cuenta del usuario para que pueda realizar los cambios a su perfil.</label>
					</div>
					<div class="form-floating mb-3">
						<textarea class="form-control" placeholder="Leave a comment here" id="motivo" name="motivo" style="height: 100px"></textarea>
						<label for="motivo">Ingrese el motivo</label>
					</div>
					<input type="hidden" value="" id="idfreelance" name="id">
					<div class="d-grid gap-2">
						<button type="button" class="btn btn-primary" id="declinar">Mandar solicitud</button>
					</div>
				</form>
			</div>
			<div class="modal-footer border-0 d-block">
				<div class="row">
					<div class="col-12 col-sm-12 col-lg-6 p-1">
						<div class="d-grid gap-2">
							<button type="button" class="btn btn-success" id="aprobar-freelance">Aprobar</button>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-lg-6 p-1">
						<div class="d-grid gap-2">
							<button type="button" class="btn btn-danger" id="declinar-freelance">Declinar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="eliminar-user" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" arai-labelledby="tite-eliminar" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content border-0">
			<div class="modal-header border-0">
				<h5 class="modal-title" id="tite-eliminar">¡ATENCIÓN!</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body border-0">
				Esta a punto de eliminar al freelancer <strong id="name-user-eliminar"></strong>, recuerde que una vez eliminado el usuario no podra tener acceso de nuevo a la plataforma y todos sus datos seran eliminados.<br> ¿Esta seguro de esto?.
				<form id="form-elimi-user" class="mt-3">
					<input type="hidden" value="" id="id" name="id">
					<label class="label">Ingrese el motivo de la eliminación del usuario <span class="fst-italic">(opcional)</span></label>
					<div class="form-floating">
						<textarea class="form-control" placeholder="..." id="motivo" name="motivo" style="height: 100px"></textarea>
						<label for="motivo">Comentarios</label>
					</div>
				</form>
			</div>
			<div class="modal-footer border-0 d-block">
				<div class="row">
					<div class="col-12 col-lg-6">
					</div>
					<div class="col-12 col-lg-6">
						<div class="d-grid gap-2">
							<a class="btn btn-primary" id="submit-elimi">
								Confirmar eliminación
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
	var editor = CKEDITOR.replace('postBody');
	CKEDITOR.config.htmlEncodeOutput;
	CKEDITOR.config.entities = false;
	$(document).ready(function(){
		$('#formdesaprobar').hide();
	});
	$(function() {
		$('#daterange').daterangepicker({
			"showDropdowns": true,
			"startDate": "<?= date('m-d-o', strtotime($hoy)) ?>",
			"maxDate": "<?= date('m-d-o', strtotime($hoy)) ?>",
			"locale": {
				"format": "MM/DD/YYYY",
				"separator": " - ",
				"applyLabel": "Filtrar",
				"cancelLabel": "Cancelar",
				"fromLabel": "De",
				"toLabel": "a",
				"customRangeLabel": "Custom",
				"daysOfWeek": [
					"Do",
					"Lu",
					"Ma",
					"Mi",
					"Ju",
					"Vi",
					"Sa"
				],
				"monthNames": [
					"Enero",
					"Febrero",
					"Marzo",
					"Abril",
					"Mayo",
					"Junio",
					"Julio",
					"Agosto",
					"Septiembre",
					"Octubre",
					"Noviembre",
					"Deciembre"
				],
				"firstDay": 1
			}
		});
		var start = new Date(moment().subtract(29, 'days')).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
		var end = new Date(moment()).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
		function cb(start, end) {
			$('#daterange span').html(start + ' - ' + end);
		}
		cb(start, end);
	});
	function filtrarUsuarios(inicio, fin){
		$.ajax({
			type: 'POST',
			url: '<?= base_url('/Administrador/filtrarUsuarios') ?>',
			data:{inicio:inicio, fin:fin, rol:'freelancer', search:$('form #buscar').val()},
			success:function(data){
				$('#contenido_freelancers').html(data);
			}, error: function(data){
				console.log('Error: ' + data);
			}
		});
	}
	$('#daterange').on('apply.daterangepicker', function(ev, picker) {
		let inicio = picker.startDate.format('YYYY-MM-DD');
		let fin = picker.endDate.format('YYYY-MM-DD');
		console.log('Inicio: ' + inicio + ' 00:00:00');
		console.log('Fin: ' + fin + ' 23:59:59');
		let inicio_formateado = new Date(moment(inicio)).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
		let fin_formateado = new Date(moment(fin)).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
		$('#daterange span').html(inicio_formateado + ' - ' + fin_formateado);
		filtrarUsuarios(inicio, fin);
	});
	$('#form-buscador-freelancers').submit(function(evento){
		$this = $(this);
		evento.preventDefault();
		$.ajax({
			type: 'POST',
			url: '<?= base_url('Administrador/filtrarUsuarios') ?>',
			data: $this.serialize()+'&rol=freelancer',
			success:function(data){
				$('#contenido_freelancers').html(data);
			}, error: function(data){
				console.log(data.responseText);
			}
		});
	});
	$('.validar-info').on('click', function(){
		let id = $(this).attr('data-free');
		$('#idfreelance').val(id);
		$("#validar-info").modal('show');
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/datos-de-freelance')?>',
			data:{id:id},
			success: function(data){
				$('#datos-freelances').html(data);
			}, error: function(data){
				alertify.error('Surgió un error, comuniquese con el area de Desarrollo.', 10);
			}
		});
	});
	$('#declinar').on('click', function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Administrador/declinarUsuario')?>',
			data: $('#formdesaprobar').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error'){
					alertify.warning(cont.mensaje, 10);
				}else{
					alertify.success(cont.mensaje, 10);
					setTimeout(function(){
						location.reload();
					}, 3000);
				}
			}, error: function(data){
				alertify.error('Ha surgido un error, comuniquese con el equipo de desarrollo.', 10);
			}
		});
	});
	$('#declinar-freelance').on('click', function(){
		$('#formdesaprobar').fadeToggle();
	});
	$('.eliminar-user').click(function(){
		var id = $(this).attr('data-id');
		var nameuser = $(this).attr('data-name');
		$('#id').val(id);
		$('#name-user-eliminar').html(nameuser);
		$('#eliminar-user').modal('show');
	});
	$('#submit-elimi').click(function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Administrador/eliminarUsuario')?>',
			data: $('#form-elimi-user').serialize(),
			success:function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error') return alertify.warning(cont.mensaje);
				$('#eliminar-user').modal('hide');
				$('#form-elimi-user')[0].reset();
				$('#card-'+$('#id').val()+'-free').remove();
				return alertify.success(cont.mensaje);
			}, error:function(data){
				let respuesta = JSON.parse(data.responseText);
				return alertify.error('Error: ' + respuesta.message + '\n' + respuesta.file);
			}
		});
	});
	// $('#aprobar-freelance').on('click', function(){
	// 	let id = $('#idfreelance').val();
	// 	$.ajax({
	// 		type: 'POST',
	// 		url: '<=base_url('/aprobar-usuario')?>',
	// 		data:$('#formdesaprobar').serialize(),
	// 		success: function(data){
	// 			let cont = JSON.parse(data);
	// 			if(cont.tipo == 'error'){
	// 				alertify.error(cont.mensaje, 10);
	// 			}else{
	// 				$('#validar-info').modal("hide")
	// 				$('button[data-free="'+id+'"]').remove();
	// 				alertify.success('Freelancer aprobado.', 10);
	// 			}
	// 		}, error: function(data){
	// 			alertify.error('Surgio un error, comuniquese con el area de Desarrollo.', 10);
	// 		}
	// 	});
	// });
</script>
<?=$this->endSection()?>