<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
    Contactos de boletin
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
    <div class="p-2">
        <h4 class="titulo-label mr">Contactos de boletin</h4>
        <a class="btn btn-primary" id="desc">
            Descargar lista de contactos
        </a>
    </div>
    <hr>
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10">
            <div class="card">
                <table class="table table-light">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Correo</th>
                            <th scope="col">Fecha</th>
                        </tr>
                    </thead>
                    <tbody style="max-height: 450px; overflow-y: auto;">
                        <?php foreach($users as $u){ ?>
                            <tr>
                                <td><?=$u['id']?></td>
                                <td><?=$u['correo']?></td>
                                <td><?= view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $u['created_at']]) ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $('#desc').click(function(){
        $.ajax({
            type: 'POST',
            url: '<?= base_url('/Administrador/descargarDBboletin') ?>',
            data:{id:1},
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error') return alertify.warning(cont.mensaje);
                alertify.success(cont.mensaje);
                setTimeout(function(){
                    window.open('<?=base_url('/usuarios-boletin.csv')?>', '_blank');
                }, 3000);
            }, error: function(data){
                alertify.error('Ha surgido un error, comuniquese con el equipo de desarrollo.');
            }
        });
    });
</script>
<?=$this->endSection()?>