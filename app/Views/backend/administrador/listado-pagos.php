<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Órdenes de pagos
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="p-2">
		<h4 class="titulo-label mr">Órdenes de pagos</h4>
		<label class="titulo-label">Ventas totales: <strong id="v-totales">$<?=number_format($montos['total'], 2, '.', ',')?> MXN</strong></label>
	</div>
	<hr>
	<div class="row">
		<div class="col-12 col-lg-6">
			<?php if($pagos == NULL){ ?>
				<div class="text-center">
					<span class="fw-bold">No se han realizado pagos en la plataforma aún.</span>
				</div>
			<?php }else{ ?>
				<form id="filtro-pagos">
					<div class="text-center mb-5">
						<div id="daterange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
							<i class="fa fa-calendar"></i>&nbsp;
							<span></span> <i class="fa fa-caret-down"></i>
						</div>
					</div>
					<div class="form-floating mb-3" id="form_buscar_pagos">
						<input class="form-control" id="buscar" name="buscador" type="search">
						<label for="buscar">Buscar pagos</label>
					</div>
					<div class="d-flex justify-content-center gap-2 mb-4">
    					<select class="form-select" aria-label="Default select example" id="buscar_cupon" name="cupon">
    						<option selected>Buscar cupón</option>
    						<?php foreach($cupones as $cupon) :?>
    							<option value="<?=$cupon['id']?>"><?=$cupon['codigo']?></option>
    						<?php endforeach ;?>
    					</select>
    					<select class="form-select" id="estatus_pagos" name="estatus">
    						<option selected>Estatus de pago</option>
    						<option value="Activo">Pagado</option>
    						<option value="En proceso">Pendiente</option>
    					</select>
					</div>
					<div class="d-flex justify-content-around mb-3">
					    <input class="btn btn-primary border-0" type="submit" value="Filtrar">
					    <input class="btn btn-secondary border-0" type="reset" value="Resetear">
						<button class="btn btn-success btn-sm" type="button" style="background-color: #217346" id="btn_exportar">
							Exportar
							<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" preserveAspectRatio="xMidYMid meet" viewBox="0 0 256 256"><path fill="currentColor" d="m219.4 171.1l-20 48a8 8 0 0 1-14.8 0l-20-48a8 8 0 1 1 14.8-6.2l12.6 30.3l12.6-30.3a8 8 0 1 1 14.8 6.2ZM66 176a12.9 12.9 0 0 1 8.6 3.4a7.9 7.9 0 0 0 11.3-.6a8 8 0 0 0-.5-11.3A29.3 29.3 0 0 0 66 160c-16.5 0-30 14.4-30 32s13.5 32 30 32a29.3 29.3 0 0 0 19.4-7.5a8 8 0 0 0 .5-11.3a7.9 7.9 0 0 0-11.3-.6A12.9 12.9 0 0 1 66 208c-7.7 0-14-7.2-14-16s6.3-16 14-16Zm134-80h-48a8 8 0 0 1-8-8V40H56v88a8 8 0 0 1-16 0V40a16 16 0 0 1 16-16h96a8.1 8.1 0 0 1 5.7 2.3l55.9 56A7.8 7.8 0 0 1 216 88v40a8 8 0 0 1-16 0Zm-40-16h28.7L160 51.3Zm-30.3 103.8c-3.1-.8-8.6-2.3-9.7-3.6v-.2c0-3.3 2.4-5 7-5a18.4 18.4 0 0 1 10 3.2h-.1a8.3 8.3 0 0 0 5.1 1.8a8 8 0 0 0 8-8a7.6 7.6 0 0 0-2.9-6.1A33.4 33.4 0 0 0 127 159c-13.5 0-23 8.6-23 21s13 17 21.6 19.3c3.4.9 7 1.8 9.2 3.1s1.2 1 1.2 1.6s0 5-9 5a18.4 18.4 0 0 1-10-3.2h.1a8.3 8.3 0 0 0-5.1-1.8a8 8 0 0 0-8 8a7.6 7.6 0 0 0 2.9 6.1A33.4 33.4 0 0 0 127 225c15.6 0 25-7.9 25-21s-14-18-22.3-20.2Z"/></svg>
						</button>
					</div>
					<label class="d-block mb-3">Resultados: <span class="fw-bold" id="texto_totales"><?= $pagos_totales?></span></label>
					<input class="invisible d-none" id="inicio" name="inicio" type="hidden" value="<?= date('Y-m-d', strtotime($minimo['fecha'])) ?>">
					<input class="invisible d-none" id="fin" name="fin" type="hidden" value="<?= date('Y-m-d', strtotime($hoy)) ?>">
				</form>
				<div id="list-pagos" style="overflow-y: auto;">
					<?= view(administrador('viewcells/card-pago'), compact('pagos'))?>
				</div>
			<?php } ?>
		</div>
		<div class="col-12 col-lg-6">
			<div id="sec-info-pago">
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
	function ajustarContenidoPagos(){
		let altura_contenido = document.querySelector('#sidebar .sidebar-menu').clientHeight;
		$('#list-pagos').css('max-height', altura_contenido);
	}
	
	$(document).resize(() => ajustarContenidoPagos());
	
	$(function() {
		ajustarContenidoPagos();

		$('#daterange').daterangepicker({
			"showDropdowns": true,
			"minDate": "<?= date('m-d-o', strtotime($minimo['fecha'])) ?>",
			"maxDate": "<?= date('m-d-o', strtotime($hoy)) ?>",
			"locale": {
				"format": "MM/DD/YYYY",
				"separator": " - ",
				"applyLabel": "Filtrar",
				"cancelLabel": "Cancelar",
				"fromLabel": "De",
				"toLabel": "a",
				"customRangeLabel": "Custom",
				"daysOfWeek": [
					"Do",
					"Lu",
					"Ma",
					"Mi",
					"Ju",
					"Vi",
					"Sa"
				],
				"monthNames": [
					"Enero",
					"Febrero",
					"Marzo",
					"Abril",
					"Mayo",
					"Junio",
					"Julio",
					"Agosto",
					"Septiembre",
					"Octubre",
					"Noviembre",
					"Deciembre"
				],
				"firstDay": 1
			}
		});
		let fecha_inicio = $('#filtro-pagos #inicio').val();
		var start = new Date(moment(fecha_inicio)).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
		var end = new Date(moment()).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
		function cb(start, end) {
			$('#daterange span').html(start + ' - ' + end);
		}
		cb(start, end);
	});
	
	$('#daterange').on('apply.daterangepicker', function(ev, picker) {
		var inicio = picker.startDate.format('YYYY-MM-DD');
		var fin = picker.endDate.format('YYYY-MM-DD');
		$('#filtro-pagos #inicio').val(inicio);
		$('#filtro-pagos #fin').val(fin);

		let inicio_formateado = new Date(moment(inicio)).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
		let fin_formateado = new Date(moment(fin)).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
		
		$('#filtro-pagos')[0].reset()
		$('#daterange span').html(inicio_formateado + ' - ' + fin_formateado);
	});

	$('#filtro-pagos').submit(function (e) {
		e.preventDefault();
		
		let datos = $(this).serialize();
		$.post("<?= base_url('Administrador/filtrarPagos') ?>", datos)
		.done(( respuesta ) => {
			alertify.notify( "Datos Cargados: " + respuesta, "success");
			$('#list-pagos').html(respuesta);
		})
		.fail(( error ) => {
			alertify.notify( "Error: " + error.responseJSON.message, "error");
		});
	});
	
	$('#btn_exportar').click(function(e){
		e.preventDefault();
		
		let datos = $('#filtro-pagos').serialize();
		$.post({
			url: '<?= base_url('Administrador/exportarPagos') ?>',
			data: datos,
			xhrFields: {responseType: 'blob'},
			success: function(response , status, xhr) {
				let filename = xhr.getResponseHeader('Content-Disposition').match(/filename=(.*)/)[1];
				let url = window.URL.createObjectURL(new Blob([response]));
				let a = document.createElement('a');
				document.body.appendChild(a);
				a.href = url;
				a.download = filename;
				a.click();
				window.URL.revokeObjectURL(url);
			}
		});
	});
</script>
<?=$this->endSection()?>