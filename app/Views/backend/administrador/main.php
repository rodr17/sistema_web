<!doctype html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
		<!--jQuery-->
		<script src="<?=base_url('assets/js/jquery/jquery_3_2_1.min.js')?>"></script>
		<!--Script para mostrar los iconos-->
		<script defer src="<?=base_url('assets/js/fontawesome/fontawesome.js')?>" crossorigin="anonymous"></script>
		<!-- Fonts -->
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
		<!-- Bootstrap CSS -->
		<link href="<?=base_url('assets/css/bootstrap/bootstrap.min.css')?>" rel="stylesheet">

		<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
		<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<!--Alertify-->
		<link href="<?=base_url('assets/css/alertify/alertify.min.css')?>" rel="stylesheet"/>
		<link rel="stylesheet" href="<?=base_url('assets/css/alertify/bootstrap.min.css')?>"/>
		<script src="<?=base_url('assets/js/alertify/alertify.min.js')?>"></script>
		<!--Editor de texto-->
		<script src="<?=base_url('/assets/js/ckeditor/ckeditor.js')?>"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.16.2/adapters/jquery.min.js"></script>
		<!--Libreria para telefonos-->
		<script defer src="<?=base_url('assets/js/jquery/mask.min.js')?>"></script>
		<!--Chart.js-->
		<script src="<?=base_url('assets/js/chart/chart.js')?>"></script>
		<!--Datapickers-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.standalone.min.css" integrity="sha512-TQQ3J4WkE/rwojNFo6OJdyu6G8Xe9z8rMrlF9y7xpFbQfW5g8aSWcygCQ4vqRiJqFsDsE1T6MoAOMJkFXlrI9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css" integrity="sha512-rxThY3LYIfYsVCWPCW9dB0k+e3RZB39f23ylUYTEuZMDrN/vRqLdaCBo/FbvVT6uC2r0ObfPzotsfKF9Qc5W5g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.standalone.min.css" integrity="sha512-p4vIrJ1mDmOVghNMM4YsWxm0ELMJ/T0IkdEvrkNHIcgFsSzDi/fV7YxzTzb3mnMvFPawuIyIrHcpxClauEfpQg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.es.min.js" integrity="sha512-5pjEAV8mgR98bRTcqwZ3An0MYSOleV04mwwYj2yw+7PBhFVf/0KcE+NEox0XrFiU5+x5t5qidmo5MgBkDD9hEw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
		<!--Hoja de estilos-->
		<link rel="stylesheet" type="text/css" href="<?=base_url('/assets/css/backend.css')?>">
		<script src="<?=base_url('assets/js/lordicon/lordicon.min.js')?>"></script>
		<?php header('Content-Type: application/json');?>
		<title><?=$this->renderSection('title');?>&nbsp;-&nbsp;MEXTEMPS</title>
	</head>
	<body id="body">
		<?php if(session('alerta')) :?>
			<script defer>
				$(document).ready(function(){
					alertify.<?= session('alerta.tipo')?>('<?= session('alerta.mensaje')?>', 10);
				});
			</script>
		<?php endif; ?>
		<div class="page-wrapper chiller-theme toggled <?=session('side')?>">
			<a id="show-sidebar" class="btn btn-sm btn-dark">
				<i class="fas fa-bars"></i>
			</a>
			<?=$this->include('/backend/administrador/side')?>
			<main class="page-content">
				<?=$this->renderSection('content')?>
				<div class="container">
					<?=$this->include('/backend/administrador/footer')?>
				</div>
			</main>
		</div>
		<!-- Bootstrap Scripts-->
		<script defer src="<?=base_url('assets/js/bootstrap/popper.min.js')?>"></script>
		<script defer src="<?=base_url('assets/js/bootstrap/bootstrap.min.js')?>"<></script>
		<script src="<?=base_url('/assets/js/function.js')?>"></script>
		<?=$this->renderSection('script')?>
	</body>
</html>