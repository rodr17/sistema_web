<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
    Medios de contacto
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
    <h1 class="titulo-label h4 p-2">Medios de contacto</h1>
    <hr>
    <form class="row" id="form-cont">
        <div class="col-12 col-sm-12 col-lg-6 p-3">
            <div class="card border-0">
                <div class="card-body">
                    <h2 class="card-title h5">Facebook</h2>
                    <p class="card-text">Ingrese la liga de la pagina de Facebook.</p>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" name="facebook" id="facebook" placeholder="..." value="<?= get_redSocial('facebook') ?>">
                        <label for="facebook">Facebook</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-lg-6 p-3">
            <div class="card border-0">
                <div class="card-body">
                    <h2 class="card-title h5">Linkedin</h2>
                    <p class="card-text">Ingrese la liga del perfil del linkedin.</p>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" name="linkedin" id="linkedin" placeholder="..." value="<?= get_redSocial('linkedin') ?>">
                        <label for="linkedin">Linkedin</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-lg-6 p-3">
            <div class="card border-0">
                <div class="card-body">
                    <h2 class="card-title h5">Youtube</h2>
                    <p class="card-text">Ingrese la liga del canal de Youtube.</p>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" name="youtube" id="youtube" placeholder="..." value="<?= get_redSocial('youtube') ?>">
                        <label for="youtube">Youtube</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-lg-6 p-3">
            <div class="card border-0">
                <div class="card-body">
                    <h2 class="card-title h5">Instagram</h2>
                    <p class="card-text">Ingrese la liga del perfil de Instagram.</p>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" name="instagram" id="instagram" placeholder="..." value="<?= get_redSocial('instagram') ?>">
                        <label for="instagram">Instagram</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-lg-6 p-3">
            <div class="card border-0">
                <div class="card-body">
                    <h2 class="card-title h5">Correo de contacto</h2>
                    <p class="card-text">Ingrese el correo para recibir los correos de alerta de la plataforma</p>
                    <div class="form-floating mb-3">
                        <input type="email" class="form-control" name="correo" id="correo" placeholder="..." value="<?= get_redSocial('correo') ?>">
                        <label for="correo">Correo de notificaciones</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-lg-6 p-3">
            <div class="card border-0">
                <div class="card-body">
                    <h2 class="card-title h5">Teléfono para chat</h2>
                    <p class="card-text">Ingrese el teléfono para el chat de WhatsApp para el área de soporte.</p>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control input-telefono" name="chat" id="chat" placeholder="..." value="<?= get_redSocial('chat') ?>">
                        <label for="twitter">Chat</label>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="row">
        <div class="col-12 text-center">
            <button class="btn btn-primary" id="actua-medios-contacto">Actualizar información</button>
        </div>
    </div>
</div>
<?=$this->endSection()?>