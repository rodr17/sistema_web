<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Membresias
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="p-2">
		<h1 class="titulo-label h4">Membresias</h1>
		<a class="btn btn-primary" id="nueva-membresia">Añadir nueva membresia</a>
	</div>
	<hr>
	<div class="row">
		<div class="col-12 col-sm-12 col-lg-5 p-2">
			<div id="new-plan" style="display:none;">
				<form id="form">
					<div class="form-floating mb-2">
						<input type="text" class="form-control" name="nombre" id="nombrePlan" placeholder="...">
						<label for="nombrePlan">Nombre del plan</label>
					</div>
					<label class="label">Tipo de costo</label>
					<div class="form-check form-switch">
						<input class="form-check-input" type="checkbox" role="switch" value="1" name="gratuito" id="input-gratuito">
						<label class="form-check-label" for="input-gratuito">Membresía gratuita</label>
					</div>
					<div class="m-2" id="div-gratuito" style="display:none;">
						<div class="text-center">Membresía gratuita</div>
					</div>
					<div class="form-floating mb-2 div-precio">
						<input type="text" class="form-control" name="costo" id="costoPlan" placeholder="...">
						<label for="costoPlan">Costo mensual</label>
					</div>
					<div class="form-floating mb-2 div-precio">
						<input type="text" class="form-control" name="costo_anual" id="costoPlanAnual" placeholder="...">
						<label for="costoPlanAnual">Costo anual</label>
					</div>
					<div class="form-floating mb-2">
						<textarea type="text" class="form-control" name="descripcion" id="descripcion" style="height: 130px;" placeholder="..."></textarea>
						<label for="descripcion">Descripción membresía</label>
					</div>
					<label class="label">Seleccione para qué tipo de usuario será el plan</label>
					<div class="form-check">
						<input class="form-check-input" type="radio" value="freelancer" name="perfil" id="radio-free">
						<label class="form-check-label" for="radio-free" style="cursor: pointer;">
							Freelancer
						</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" value="contratante" name="perfil" id="radio-contra">
						<label class="form-check-label" for="radio-contra" style="cursor: pointer;">
							Contratante
						</label>
					</div>
					<div class="text-center mb-2">
						<a class="btn btn-primary" id="crear-plan">Crear plan</a>
					</div>
				</form>
			</div>
			<div id="zone-members"></div>
		</div>
		<div class="col-12 col-sm-12 col-lg-7 p-2">
			<table class="table table-light">
				<thead>
					<tr>
						<th scope="col"># ID</th>
						<th scope="col">Membresía</th>
						<th scope="col">Perfil</th>
					</tr>
				</thead>
				<tbody>
					<?php if($planes != null){ ?>
						<?php foreach($planes as $p){ ?>
							<tr>
								<th scope="col">
									<?=$p['id']?>
								</th>
								<th scope="col">
									<?=$p['name']?><br>
									<small>
										<a class="cursor text-decoration-none text-primary edit-member" data-id="<?=$p['id']?>">
											Editar
										</a>
									</small> | 
									<small>
										<a class="cursor text-decoration elimi-membership text-danger" data-id="<?=$p['id']?>">
											Eliminar
										</a>
									</small>
								</th>
								<th scope="col"><?=$p['cuenta']?></th>
							</tr>
						<?php } ?>
					<?php }else{ ?>
						<tr>
							<th scope="col">
								0
							</th>
							<th scope="col">Sin membresías creadas</th>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?=$this->endSection()?>