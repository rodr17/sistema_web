<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Métodos de pago
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="p-2">
		<h1 class="titulo-label h4">Pasarelas de Pago</h1>
	</div>
	<hr>
	<h2 class="d-flex align-items-center gap-2 h4">¿Como configurar NetPay? <label class="text-decoration badge bg-primary" data-bs-toggle="modal" data-bs-target="#infoopenpay"><i class="far fa-question-circle" aria-hidden="true"></i></label></h2 class="h4">
	<form id="form-netpay" autocomplete="off">
		<div class="row justify-content-md-center mb-3">
			<div class="col-12 col-sm-12 col-lg-8">
				<div class="card border-0">
					<div class="card-title mb-1 p-3 d-flex">
						<h6><strong>Módulo de configuración NetPay</strong></h6>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-12 col-sm-12 col-lg-6">
								<label class="label">Seleccione el modo en el que funcionará el método de pago</label>
							</div>
							<div class="col-12 col-sm-12 col-lg-6">
								<div class="form-check">
									<input class="form-check-input" type="radio" name="modo" id="mod-pruebas-netpay" value="prueba" <?= !empty($netpay[0]['modo']) ? ($netpay[0]['modo'] == 'prueba'? 'checked' : '') : ''?>>
									<label class="form-check-label" for="mod-pruebas-netpay">Modo de pruebas</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="radio" name="modo" id="mod-prod-netpay" value="produccion" <?= !empty($netpay[0]['modo']) ? ($netpay[0]['modo'] == 'produccion'? 'checked' : '') : ''?>>
									<label class="form-check-label" for="mod-prod-netpay">Modo de producción</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-sm-12 col-lg-6 p-1">
				<div class="card border-0">
					<div class="card-title mb-1 p-3 d-flex">
						<h6><strong>Credenciales modo de pruebas</strong></h6>
					</div>
					<div class="card-body">
						<label class="fw-bold mb-2">NETPAY LOOP</label>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" name="publica" id="publicaNetpay" value="<?= !empty($netpay[0]['publica']) ? $netpay[0]['publica'] : ''?>">
							<label for="publicaNetpay">Pública Pruebas</label>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" name="privada" id="privadaNetpay" value="<?= !empty($netpay[0]['privada']) ? $netpay[0]['privada'] : ''?>">
							<label for="privadaNetpay">Privada Pruebas</label>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" name="path_prueba" id="path_pruebaNetpay" value="<?= !empty($netpay[0]['path_prueba']) ? $netpay[0]['path_prueba'] : ''?>">
							<label for="path_pruebaNetpay">URL Pruebas</label>
						</div>
						<hr>
						<label class="fw-bold mb-2">NETPAY CHECKOUT</label>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" name="publicaCheckout" id="publicaCheckout" value="<?= !empty($netpay[0]['publicaCash']) ? $netpay[0]['publicaCash'] : ''?>">
							<label for="publicaCheckout">Pública Pruebas</label>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" name="privadaCheckout" id="privadaCheckout" value="<?= !empty($netpay[0]['privadaCash']) ? $netpay[0]['privadaCash'] : ''?>">
							<label for="privadaCheckout">Privada Pruebas</label>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" name="path_pruebaCheckout" id="path_pruebaCheckout" value="<?= !empty($netpay[0]['path_pruebaCash']) ? $netpay[0]['path_pruebaCash'] : ''?>">
							<label for="path_pruebaCheckout">URL Pruebas</label>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-lg-6 p-1">
				<div class="card border-0">
					<div class="card-title mb-1 p-3 d-flex">
						<h6><strong>Credenciales modo de producción</strong></h6>
					</div>
					<div class="card-body">
						<label class="fw-bold mb-2">NETPAY LOOP</label>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" name="publica_produccion" id="publicaProduccion_netpay" value="<?= !empty($netpay[0]['publicaProduccion']) ? $netpay[0]['publicaProduccion'] : ''?>">
							<label for="publicaProduccion_netpay">Pública Producción</label>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" name="privada_produccion" id="secretProduccion_netpay" value="<?= !empty($netpay[0]['privadaProduccion']) ? $netpay[0]['privadaProduccion'] : ''?>">
							<label for="secretProduccion_netpay">Privada Producción</label>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" name="path_produccion" id="path_produccionNetpay" value="<?= !empty($netpay[0]['path_produccion']) ? $netpay[0]['path_produccion'] : ''?>">
							<label for="path_produccionNetpay">URL Producción</label>
						</div>
						<hr>
						<label class="fw-bold mb-2">NETPAY CHECKOUT</label>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" name="publica_produccionCheckout" id="publicaProduccion_checkout" value="<?= !empty($netpay[0]['publicaProduccionCash']) ? $netpay[0]['publicaProduccionCash'] : ''?>">
							<label for="publicaProduccion_checkout">Pública Producción</label>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" name="privada_produccionCheckout" id="secretProduccion_checkout" value="<?= !empty($netpay[0]['privadaProduccionCash']) ? $netpay[0]['privadaProduccionCash'] : ''?>">
							<label for="secretProduccion_checkout">Privada Producción</label>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" name="path_produccionCheckout" id="path_produccion_checkout" value="<?= !empty($netpay[0]['path_produccionCash']) ? $netpay[0]['path_produccionCash'] : ''?>">
							<label for="path_produccion_checkout">URL Producción</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	<div class="row p-4">
		<div class="col-12 text-center">
			<btn class="btn btn-primary" id="save-netpay">Guardar cambios</btn>
		</div>
	</div>
</div>
<div class="container">
	<h1 class="titulo-label h4 p-2">Redes Sociales</h1>
	<hr>
	<div class="row justify-content-center">
		<div class="col-12 col-sm-12 col-lg-6 p-3">
			<div class="card border-0">
				<form id="formfacebook">
					<div class="card-body">
						<h2 class="card-title h5">Facebook</h2>
						<p class="card-text">Ingrese los datos solicitados para la configuración de inicio de sesión por medio de Facebook</p>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" id="app_id" name="id" placeholder="..." value="<?= get_Facebook('app_id') ?>">
							<label for="app_id">App ID</label>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" id="app_secret" name="secreta" placeholder="..." value="<?= get_Facebook('app_secret') ?>">
							<label for="app_secret">App Secreta</label>
						</div>
						<input type="hidden" name="red_social" value="Facebook">
						<div class="text-center">
							<a class="btn btn-primary save-ingreso" data-tipo="facebook">Guardar cambios</a>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="col-12 col-sm-12 col-lg-6 p-3">
			<div class="card border-0">
				<form id="formgoogle">
					<div class="card-body">
						<h2 class="card-title h5">Gmail</h2>
						<p class="card-text">Ingrese los datos solicitados para la configuración de inicio de sesión por medio de su cuenta de Google.</p>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" id="clienteID" name="id" placeholder="..." value="<?= get_Google('clienteID') ?>">
							<label for="clienteID">Cliente ID</label>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" id="clienteSecret" name="secreta" placeholder="..." value="<?= get_Google('clienteSecret') ?>">
							<label for="clienteSecret">Cliente Secreta</label>
						</div>
						<input type="hidden" name="red_social" value="Google">
						<div class="text-center">
							<a class="btn btn-primary save-ingreso" data-tipo="google">Guardar cambios</a>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="col-lg-6 p-3">
			<div class="card border-0">
				<form id="formrecaptcha">
					<div class="card-body">
						<h2 class="card-title h5">reCAPTCHA</h2>
						<p class="card-text">Ingrese las claves solicitadas para la configuración de registros de usuarios.</p>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" id="claveSitio" name="id" placeholder="..." value="<?= get_recaptcha('clave sitio') ?>">
							<label for="claveSitio">Clave de Sitio Web</label>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" id="claveSecreta" name="secreta" placeholder="..." value="<?= get_recaptcha('clave secreta') ?>">
							<label for="claveSecreta">Clave Secreta</label>
						</div>
						<input type="hidden" name="red_social" value="Recaptcha">
						<div class="text-center">
							<a class="btn btn-primary save-ingreso" data-tipo="recaptcha">Guardar cambios</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="infoopenpay" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
		<div class="modal-content">
			<div class="modal-header border-0">
				<h5>Obtener llaves de API Netpay</h5>
				<button type="button" class="btn-close boton-cerrar-modal" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body modal-openpay">
				<div class="div-blanc p-3 rounded">
					<p>Para poder interactuar obtener las claves APIs es necesario que tengas tu cuenta en NetPay para poder obtener llaves sandbox.</p>
					<p>puedes obtener tus llaves registrándote desde <a href="https://manager.netpay.com.mx/" target="_blank">Netpay Manager</a></p>
					<h6>Pasos</h6>
					<label class="mb-2">1.- Darte de alta o iniciar sesión en <a href="https://manager.netpay.com.mx/" target="_blank">Netpay Manager</a></label>
					<img src="<?=base_url('assets/images/Backend/netpay_manager.webp')?>" class="w-100" height="auto" width="auto">
					<label class="my-2">2.- Ingresar a la sección de <b>developers</b> -> <b>sandbox</b> y activar las llaves de pruebas o solamente copiar e ingresarlas aquí en el admin de MEXTEMPS.</label>
					<img src="<?=base_url('assets/images/Backend/netpay_manager_credenciales.webp')?>" class="w-100" height="auto" width="auto">
					<p class="mb-0 mt-2">Para tener la plataforma en funcionamiento al público solamente deberá habilitar el <b>modo producción</b> que se encuentra en la parte superior del formulario.</p>
				</div>
			</div>
			<div class="modal-footer border-0">
				<div class="text-center w-100">
					<button type="button" class="btn btn-primary" data-bs-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<?=$this->endSection()?>