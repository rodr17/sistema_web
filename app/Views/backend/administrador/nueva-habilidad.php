<form id="formarea">
	<p class="mb-2">
		Para crear nuevas areas principales escriba el nombre de la nueva area y deje el campo de area superior en blanco.<br>
		En caso de ser una sub area seleccione el area superior al que se le asignara.
	</p>
	<div class="row g-2 mb-2">
		<div class="col-md">
			<div class="form-floating">
				<input type="text" class="form-control" id="area" name="area" placeholder="..." value="">
				<label for="area">Area o habilidad</label>
			</div>
		</div>
		<div class="col-md">
			<div class="form-floating">
				<select class="form-select" id="superior" name="superior" aria-label="Floating label select example">
					<option selected value="0">Seleccione una opción</option>
				</select>
				<label for="superior">Area superior</label>
			</div>
		</div>
	</div>
</form>
<div>
	<a class="btn btn-info" id="guardar-area">
		Guardar            
	</a>
</div>
<script>
	$(document).ready(function(){
		mostrarAreas();
	});
	$('#guardar-area').click(function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Habilidades/nuevaArea')?>',
			data:$('#formarea').serialize(),
			success: function(data){
			    let cont = JSON.parse(data);
			    if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
				$('#formarea')[0].reset();
				mostrarAreas();
				alertify.success(cont.mensaje, 10);
			}, error: function(data){
				console.log('Error');
			}
		});
	});
	function mostrarAreas(){
	    $.ajax({
			type: 'POST',
			url: '<?=base_url('/Habilidades/superiores')?>',
			data:{id:1},
			success: function(data){
				$('#superior').html(data);
			}, error: function(data){
				console.log('Error');
			}
		});
	}
</script>