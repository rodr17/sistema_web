<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Nuevo usuario administrador
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="p-2">
		<h4 class="titulo-label">
			Nuevo administrador
		</h4>
	</div>
	<hr>
	<div class="row">
		<div class="col-12 col-sm-12 col-lg-4">
			<div class="p-3">
				<div class="card border-0">
					<div class="card-body">
						<h5 class="card-title">Tomar a en cuenta antes de crear administrador.</h5>
						<p class="card-text mt-3 mb-3">
							- El nuevo administrador tendra control sobre las credenciales de ingreso.<br>
							- El nuevo administrador tendra acceso al control sobre las credenciales de los metodos de pago.<br>
							- El nuevo administrador tendra control sobre las preferencias de las membresias.<br>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-12 col-lg-8">
			<div class="p-3">
				<form id="nuevo-admin">
					<div class="form-floating mb-3">
						<input type="text" class="form-control" id="user" name="usuario" placeholder="Admin">
						<label for="user">Nombre de usuario</label>
					</div>
					<div class="form-floating mb-3">
						<input type="email" class="form-control" id="exampleInputEmail1" name="correo" placeholder="name@mextemps.com">
						<label for="exampleInputEmail1">Correo</label>
					</div>
					<div class="form-floating mb-3">
						<input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
						<label for="exampleInputPassword1">Contraseña</label>
					</div>
					<div class="p-3 mb-3 border">
						<label class="mb-3">Acceso a secciones</label>
						<div class="form-check form-switch">
							<input class="form-check-input" value="true" type="checkbox" name="membresias" id="membresias" role="switch">
							<label class="form-check-label" for="membresias">Membresías</label>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input" value="true" type="checkbox" role="switch" id="pagos" name="pagos">
							<label class="form-check-label" for="pagos">Pagos</label>
						</div>
						<div class="ms-4" id="inputs_pagos" style="display:none;">
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="lista_pagos" id="lista_pagos" role="switch">
								<label class="form-check-label" for="lista_pagos">Listado de pagos</label>
							</div>
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="estadisticas_generales" id="estadisticas_generales" role="switch">
								<label class="form-check-label" for="estadisticas_generales">Estadísticas generales</label>
							</div>
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="solicitudes_facturacion" id="solicitudes_facturacion" role="switch">
								<label class="form-check-label" for="solicitudes_facturacion">Solicitudes de facturación</label>
							</div>
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="crear_cupon" id="crear_cupon" role="switch">
								<label class="form-check-label" for="crear_cupon">Crear cupón</label>
							</div>
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="lista_cupones" id="lista_cupones" role="switch">
								<label class="form-check-label" for="lista_cupones">Lista cupones</label>
							</div>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input" value="true" type="checkbox" role="switch" id="proyectos" name="proyectos">
							<label class="form-check-label" for="proyectos">Proyectos</label>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input" value="true" type="checkbox" role="switch" id="usuarios" name="usuarios">
							<label class="form-check-label" for="usuarios">Usuarios</label>
						</div>
						<div class="ms-4" id="inputs_usuarios" style="display:none;">
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="mi_perfil" id="mi_perfil" role="switch">
								<label class="form-check-label" for="mi_perfil">Mi perfil</label>
							</div>
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="listado_administradores" id="listado_administradores" role="switch">
								<label class="form-check-label" for="listado_administradores">Listado de administradores</label>
							</div>
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="listado_freelancers" id="listado_freelancers" role="switch">
								<label class="form-check-label" for="listado_freelancers">Listado de freelancers</label>
							</div>
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="listado_contratantes" id="listado_contratantes" role="switch">
								<label class="form-check-label" for="listado_contratantes">Listado de contratantes</label>
							</div>
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="agregar_usuario_admin" id="agregar_usuario_admin" role="switch">
								<label class="form-check-label" for="agregar_usuario_admin">Agregar usuario admin</label>
							</div>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input" value="true" type="checkbox" role="switch" id="habilidades" name="habilidades">
							<label class="form-check-label" for="habilidades">Habilidades</label>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input" value="true" type="checkbox" role="switch" id="noticias" name="noticias">
							<label class="form-check-label" for="noticias">Noticias</label>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input" value="true" type="checkbox" role="switch" id="idiomas" name="idiomas">
							<label class="form-check-label" for="idiomas">Idiomas</label>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input" value="true" type="checkbox" role="switch" id="contenidos" name="contenidos">
							<label class="form-check-label" for="contenidos">Contenidos</label>
						</div>
						<div class="ms-4" id="inputs_contenidos" style="display:none;">
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="editar_portada" id="editar_portada" role="switch">
								<label class="form-check-label" for="editar_portada">Editar portada</label>
							</div>
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="listado_paginas" id="listado_paginas" role="switch">
								<label class="form-check-label" for="listado_paginas">Listado de páginas</label>
							</div>
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="ajustes_generales" id="ajustes_generales" role="switch">
								<label class="form-check-label" for="ajustes_generales">Ajustes generales</label>
							</div>
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="aviso_privacidad" id="aviso_privacidad" role="switch">
								<label class="form-check-label" for="aviso_privacidad">Aviso de privacidad</label>
							</div>
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="terminos_condiciones" id="terminos_condiciones" role="switch">
								<label class="form-check-label" for="terminos_condiciones">Términos y condiciones</label>
							</div>
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="faq_tarifas" id="faq_tarifas" role="switch">
								<label class="form-check-label" for="faq_tarifas">FAQ tarífas</label>
							</div>
							<div class="form-check form-switch">
								<input class="form-check-input" value="true" type="checkbox" name="faq_soporte" id="faq_soporte" role="switch">
								<label class="form-check-label" for="faq_soporte">FAQ soporte</label>
							</div>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input" value="true" type="checkbox" role="switch" id="contacto" name="medios_contacto">
							<label class="form-check-label" for="contacto">Medios de contacto</label>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input" value="true" type="checkbox" role="switch" id="llaves" name="llaves_api">
							<label class="form-check-label" for="llaves">Llaves API</label>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input" value="true" type="checkbox" role="switch" id="contactos_soporte" name="contactos_soporte">
							<label class="form-check-label" for="contactos_soporte">Contactos soporte</label>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input" value="true" type="checkbox" role="switch" id="contactos_boletin" name="contactos_boletin">
							<label class="form-check-label" for="contactos_boletin">Contactos boletín</label>
						</div>
					</div>
					<div class="text-center">
						<a class="btn btn-primary" id="agregar">Crear usuario</a>
					</div>
				</form>
			</div> 
		</div>
	</div>
</div>
<script>
	$('#pagos').change(() => $('#inputs_pagos').fadeToggle());
	$('#usuarios').change(() => $('#inputs_usuarios').fadeToggle());
	$('#contenidos').change(() => $('#inputs_contenidos').fadeToggle());
</script>
<?=$this->endSection()?>