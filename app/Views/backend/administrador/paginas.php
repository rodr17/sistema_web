<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Páginas
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="p-2">
		<h4 class="titulo-label">
			Páginas
		</h4>
	</div>
	<hr>
	<div class="p-3">
		<div class="card border-0">
			<div class="card-title mb-1 p-3 d-flex">
				<h6><strong>Páginas y opciones</strong></h6>
			</div>
			<div class="card-body">
				<div class="div-limitadoY">
					<table class="table">
						<thead>
							<tr>
								<th scope="col">Páginas</th>
								<th scope="col">Opciones</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row">Inicio</th>
								<td>
									<small>
										<a class="link-info" href="<?=base_url('/editar-seccion?id='.base64_encode(1).'&page=inicio')?>">Editar</a>
									</small>
									&nbsp;
									<small>
										<a class="link-info" href="<?=base_url()?>" target="_blank">Ver</a>
									</small>
									&nbsp;
								</td>
							</tr>
							<tr>
								<th scope="row">Como Funciona Freelance</th>
								<td>
									<small>
										<a class="link-info" href="<?=base_url('/editar-seccion?id='.base64_encode(2).'&page=como-funciona-freelance')?>">Editar</a>
									</small>
									&nbsp;
									<small>
										<a class="link-info" href="<?=base_url('/como-funciona-freelance')?>" target="_blank">Ver</a>
									</small>
								</td>
							</tr>
							<tr>
								<th scope="row">Como funciona Contratante</th>
								<td>
									<small>
										<a class="link-info" href="<?=base_url('/editar-seccion?id='.base64_encode(3).'&page=como-funciona-contratista')?>">Editar</a>
									</small>
									&nbsp;
									<small>
										<a class="link-info" href="<?=base_url('/como-funciona-contratista')?>" target="_blank">Ver</a>
									</small>
								</td>
							</tr>
							<tr>
								<th scope="row">Tarifas para usuarios</th>
								<td>
									<small>
										<a class="link-info" href="<?=base_url('/editar-seccion?id='.base64_encode(4).'&page=paquetes')?>">Editar</a>
									</small>
									&nbsp;
									<small>
										<a class="link-info" href="<?=base_url('/tarifas-freelance')?>" target="_blank">Ver</a>
									</small>
								</td>
							</tr>
							<tr>
								<th scope="row">Soporte</th>
								<td>
									<small>
										<a class="link-info" href="<?=base_url('faq-soporte')?>">Editar</a>
									</small>
									&nbsp;
									<small>
										<a class="link-info" href="<?=base_url('soporte')?>" target="_blank">Ver</a>
									</small>
								</td>
							</tr>
							<tr>
								<th scope="row">Blog</th>
								<td>
									<small>
										<a class="link-info" href="<?=base_url('administrar-noticias')?>">Editar</a>
									</small>
									&nbsp;
									<small>
										<a class="link-info" href="<?=base_url('/blog')?>" target="_blank">Ver</a>
									</small>
								</td>
							</tr>
							<tr>
								<th scope="row">Términos y condiciones</th>
								<td>
									<small>
										<a class="link-info" href="<?=base_url('administrar-terminos-condiciones')?>">Editar</a>
									</small>
									&nbsp;
									<small>
										<a class="link-info" href="<?=base_url('terminos-y-condiciones')?>" target="_blank">Ver</a>
									</small>
								<td>
							</tr>
							<tr>
							<th scope="row">Aviso de privacidad</th>
								<td>
									<small>
										<a class="link-info" href="<?=base_url('administrar-aviso-de-privacidad')?>">Editar</a>
									</small>
									&nbsp;
									<small>
										<a class="link-info" href="<?=base_url('aviso-de-privacidad')?>" target="_blank">Ver</a>
									</small>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?=$this->endSection()?>