<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('script')?>
<?=$this->endSection()?>
<?=$this->section('title')?>
    Editar página de Como funciona freelance
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
    <div class="p-2">
        <h4 class="titulo-label">
            Editar página de "Como funciona freelance"
        </h4>
    </div>
    <hr>
    <div class="row">
        <div class="col-12 col-lg-3">
            <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <button class="nav-link active" id="tab-titulo" data-bs-toggle="pill" data-bs-target="#titulo" type="button" role="tab" aria-controls="titulo" aria-selected="true">
                    Seccion de Titulo
                </button>
                <button class="nav-link" id="tab-slide" data-bs-toggle="pill" data-bs-target="#slide" type="button" role="tab" aria-controls="slide" aria-selected="true">
                    Slides
                </button>
                <button class="nav-link" id="tab-crear" data-bs-toggle="pill" data-bs-target="#crear" type="button" role="tab" aria-controls="crear" aria-selected="true">
                    Sección "Crea una cuenta"
                </button>
                <button class="nav-link" id="tab-3-pasos" data-bs-toggle="pill" data-bs-target="#pasos" type="button" role="tab" aria-controls="pasos" aria-selected="true">
                    Sección "3 pasos"
                </button>
                <button class="nav-link" id="tab-oportunidades" data-bs-toggle="pill" data-bs-target="#oportunidades" type="button" role="tab" aria-controls="oportunidades" aria-selected="true">
                    Sección "Miles de oportunidades"
                </button>
                <button class="nav-link" id="tab-comentarios" data-bs-toggle="pill" data-bs-target="#comentarios" type="button" role="tab" aria-controls="comentarios" aria-selected="true">
                    Sección "Comentarios"
                </button>
            </div>
        </div>
        <div class="col-12 col-lg-9">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="titulo" role="tabpanel" aria-labelledby="tab-titulo">
                    <form id="form-titulo">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <label>Ingrese el titulo para la sección</label>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" id="titulo-seccion-1" name="titulo" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'area-slides', 'valor' => 'titulo'])?>" placeholder="...">
                                    <label for="titulo-seccion-1">Titulo en negro</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" id="titulo-seccion-2" name="destacado" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'area-slides', 'valor' => 'destacado'])?>" placeholder="...">
                                    <label for="titulo-seccion-2">Titulo en color secundario</label>
                                </div>
                            </div>
                            <div class="col-12 mb-3">
                                <label>Ingrese el subtitulo para la sección</label><br>
                                <textarea id="subtitulo"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'area-slides', 'valor' => 'subtitulo'])?></textarea>
                            </div>
                        </div>
                        <div class="text-center">
                            <a class="btn btn-primary" id="guardar-titulo">
                                Guardar
                            </a>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="slide" role="tabpanel" aria-labelledby="tab-slide">
                    <form id="form-slide">
                        
                    </form>
                </div>
                <div class="tab-pane fade" id="crear" role="tabpanel" aria-labelledby="tab-crear">
                    <form id="form-crear">
                        <div class="row">
                            <div class="col-12 mb-3">
                                <label>Texto superior de la sección</label><br>
                                <textarea id="superior"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'crea-una-cuenta', 'valor' => 'superior'])?></textarea>
                            </div>
                            <div class="col-12 mb-3">
                                <label>Titulo de sección</label><br>
                                <textarea id="titulo-crea"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'crea-una-cuenta', 'valor' => 'titulo'])?></textarea>
                            </div>
                            <div class="col-12 mb-3">
                                <label>Subtitulo de sección</label>
                                <textarea id="subtitulo-crea"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'crea-una-cuenta', 'valor' => 'subtitulo'])?></textarea>
                            </div>
                            <div class="col-12 mb-3">
                                <input type="hidden" id="fondo-crea" name="fondo" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'crea-una-cuenta', 'valor' => 'imagen'])?>">
                                <input class="form-control form-control-sm" id="fondo-empezemos" type="file" onchange="readUrlImagen(this, 'fondo-crea', 'preview-fondo-crea');">
                                <div class="text-center m-3">
                                    <img id="preview-fondo-crea" class="w-25" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'crea-una-cuenta', 'valor' => 'imagen'])?>" alt="your image" />
                                </div>
                            </div>
                            <div class="text-center">
                                <a class="btn btn-primary" id="guardar-crea">
                                    Guardar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="pasos" role="tabpanel" aria-labelledby="tab-3-pasos">
                    <form id="form-pasos">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <label>Ingrese el titulo para la sección</label>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" id="titulo-crea-1" name="titulo" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'tres-pasos', 'valor' => 'titulo'])?>" placeholder="...">
                                    <label for="titulo-crea-1">Titulo en negro</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" id="titulo-crea-2" name="destacado" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'tres-pasos', 'valor' => 'destacado'])?>" placeholder="...">
                                    <label for="titulo-crea-2">Titulo en color secundario</label>
                                </div>
                            </div>
                            <div class="col-12 mb-3">
                                <textarea id="inicial3pasos"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'tres-pasos', 'valor' => 'contenido'])?></textarea>
                            </div>
                            <div class="col-12 text-center">
                                <a class="btn btn-primary" id="guardar-seccion-pasos">
                                    Guardar cambios
                                </a>
                            </div>
                        </div>
                    </form>
                    <div class="row my-4">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-md-4 text-center">
                                    <a class="btn btn-primary edit-paso" data-paso="1">
                                        Editar paso 1
                                    </a>
                                </div>
                                <div class="col-12 col-md-4 text-center">
                                    <a class="btn btn-primary edit-paso" data-paso="2">
                                        Editar paso 2
                                    </a>
                                </div>
                                <div class="col-12 col-md-4 text-center">
                                    <a class="btn btn-primary edit-paso" data-paso="3">
                                        Editar paso 3
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="edit-paso" class="mt-4">
                    </div>
                </div>
                <div class="tab-pane fade" id="oportunidades" role="tabpanel" aria-labelledby="tab-oportunidades">
                    <form id="form-oportunidades">
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" name="superior" id="superior-oportunidades" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'oportunidades', 'valor' => 'superior'])?>" placeholder="...">
                            <label for="superior-oportunidades">Texto superior</label>
                        </div>
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" name="titulo" id="titulo-oportunidades" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'oportunidades', 'valor' => 'titulo'])?>" placeholder="...">
                            <label for="titulo-oportunidades">Titulo de sección</label>
                        </div>
                        <div class="mb-3">
                            <label>Descripción de la sección</label>
                            <textarea id="descripcion-oportunidades"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'oportunidades', 'valor' => 'descripcion'])?></textarea>
                        </div>
                        <div class="mb-3">
                            <input type="hidden" id="fondo-oportunidades" name="fondo" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'oportunidades', 'valor' => 'imagen'])?>">
                            <input class="form-control form-control-sm" id="oport-fondo" type="file" onchange="readUrlImagen(this, 'fondo-oportunidades', 'preview-fondo-oportunidades');">
                            <div class="text-center m-3">
                                <img id="preview-fondo-oportunidades" class="w-25" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'oportunidades', 'valor' => 'imagen'])?>" alt="your image" />
                            </div>
                        </div>
                        <div class="text-center">
                            <a class="btn btn-primary" id="guardar-oportunidades">
                                Guardar
                            </a>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="comentarios" role="tabpanel" aria-labelledby="tab-comentarios">
                    <form id="form-comentarios">
                        <div class="row">
                            <div class="col-12 mb-2">
                                <label>Ingrese el titulo para la sección</label>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'comentarios', 'valor' => 'titulo'])?>" id="titulo-comentarios-1" name="titulo" placeholder="...">
                                    <label for="titulo-comentarios-1">Titulo en negro</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'comentarios', 'valor' => 'destacado'])?>" id="titulo-comentarios-2" name="destacado" placeholder="...">
                                    <label for="titulo-comentarios-2">Titulo en color secundario</label>
                                </div>
                            </div>
                            <div class="col-12 mb-3">
                                <label>Texto inferior</label>
                                <textarea id="inferior-comentarios"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'comentarios', 'valor' => 'inferior'])?></textarea>
                            </div>
                            <div class="text-center p-4">
                                <div class="text-center fw-bold">
                                    Los comentarios se modifican desde el area de ajustes generales
                                </div>
                            </div>
                            <div class="text-center">
                                <a class="btn btn-primary" id="guardar-comentarios">
                                    Guardar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    CKEDITOR.config.htmlEncodeOutput;
    CKEDITOR.config.entities = false;
    CKEDITOR.config.removePlugins = 'save';
    CKEDITOR.config.height = 100;
    CKEDITOR.config.fullPage = false;
    CKEDITOR.config.removeFormatTags = 'b,big,code,del,dfn,em,font,i,ins,kbd,p';
    var editor = CKEDITOR.replace('subtitulo');
    var textSuperior = CKEDITOR.replace('superior');
    var tituloCrea = CKEDITOR.replace('titulo-crea');
    var subtituloCrea = CKEDITOR.replace('subtitulo-crea');
    var inicial3pasos = CKEDITOR.replace('inicial3pasos');
    var descOportuinidades = CKEDITOR.replace('descripcion-oportunidades');
    var inferior = CKEDITOR.replace('inferior-comentarios');
    // guardar seccion de comentarios
    $('#guardar-comentarios').click(function(){
        let titulo = $("#titulo-comentarios-1").val();
        let destacado = $('#titulo-comentarios-2').val();
        let editorData = inferior.getData();
        let postBody = editorData.replace(/&nbsp;/gi,' ');
        let dataString = 'titulo='+ titulo +'&destacado='+destacado+'&inferior='+postBody;

        $.ajax({
            type: "POST",
            url: "<?=base_url('/Secciones/saveSeccComentariosComoFreelance')?>",
            data: dataString,
            cache: false,
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error'){
                    alertify.warning(cont.mensaje, 10);
                }else{
                    alertify.success(cont.mensaje, 10);
                }
            },error:function(data){
                alertify.error('Error', 10);
            }
        });
        return false;
    });
    // guardar seccion de oportunidades
    $('#guardar-oportunidades').click(function(){
        let superiorOportunidades = $('#superior-oportunidades').val();
        let tituloOportunidades = $('#titulo-oportunidades').val();
        let editorData = descOportuinidades.getData();
        let postBody = editorData.replace(/&nbsp;/gi,' ');
        let imagen = $('#fondo-oportunidades').val();
        let dataString = 'superior='+superiorOportunidades+'&titulo='+tituloOportunidades+'&descripcion='+postBody+'&imagen='+imagen;
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Secciones/saveMilesOportunidadesFreelance')?>',
            data: dataString,
            cache: false,
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error'){
                    alertify.warning(cont.mensaje, 10);
                }else{
                    alertify.success(cont.mensaje, 10);
                }
            },error:function(data){
                alertify.error('Error', 10);
            }
        });
        return false;
    });
    // guardar seccion de 3 pasos
    $('#guardar-seccion-pasos').click(function(){
        let textblack = $('#titulo-crea-1').val();
        let textnaranja = $('#titulo-crea-2').val();
        let editorData = inicial3pasos.getData();
        let postBody = editorData.replace(/&nbsp;/gi,' ');
        let dataString = 'titulo='+textblack+'&destacado='+textnaranja+'&contenido='+postBody;
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Secciones/saveTresPasosFreelance')?>',
            data: dataString,
            cache: false,
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
                return alertify.success(cont.mensaje, 10);
            },error:function(data){
                alertify.error('Error', 10);
            }
        });
    });
    // guardar seccion de crea una cuenta
    $('#guardar-crea').click(function(){
        let editsuperior = textSuperior.getData();
        let postSuperior = editsuperior.replace(/&nbsp;/gi,' ');
        let editTitulo = tituloCrea.getData();
        let postTitulo = editTitulo.replace(/&nbsp;/gi,' ');
        let editSubtitulo = subtituloCrea.getData();
        let postSubtitulo = editSubtitulo.replace(/&nbsp;/gi,' ');
        let imagen = $('#fondo-crea').val();
        let dataString = 'superior='+postSuperior+'&titulo='+postTitulo+'&subtitulo='+postSubtitulo+'&imagen='+imagen;
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Secciones/saveCrearCuentaFreelance')?>',
            data: dataString,
            cache: false,
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
                return alertify.success(cont.mensaje, 10);
            },error:function(data){
                alertify.error('Error', 10);
            }
        });
    });
    // guardar seccion de inicio con los slides
    $('#guardar-titulo').click(function(){
        let titulo = $('#titulo-seccion-1').val();
        let destacado = $('#titulo-seccion-2').val();
        let subtitulo = editor.getData();
        let postBody = subtitulo.replace(/&nbsp;/gi,' ');
        let dataString = 'titulo='+titulo+'&destacado='+destacado+'&subtitulo='+postBody;
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Secciones/saveHeaderComoFuncionaFreelance')?>',
            data: dataString,
            cache: false,
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error') return alertify.warning(cont.mensaje);
                return alertify.success(cont.mensaje, 10);
            },error:function(data){
                alertify.error('Error', 10);
            }
        });
    });
    // Previsualizar imagen
    function readUrlImagen(input, guardar, resultado){
        if(input.files && input.files[0]){
            let reader = new FileReader();
            reader.onload = function (e){
                $('#'+resultado).attr('src', e.target.result);
                $('#'+guardar).val(e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }else{
            $('#'+resultado).attr('src', '<?=base_url('/assets/themes/imgs/page/about/banner-img.png')?>')
        }
    }
    // Mostrar form para editar paso
    $('.edit-paso').click(function(){
        let id = $(this).attr('data-paso');
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Secciones/verPasoFreelance')?>',
            data:{id:id},
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
                return $('#edit-paso').html(cont.mensaje);
            }, error: function(data){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
            }
        });
    });
</script>
<style>
    .cke_toolbar_break,
    .cke_toolbox .cke_toolbar:nth-child(2),
    .cke_toolbox .cke_toolbar:nth-child(3),
    .cke_toolbox .cke_toolbar:nth-child(4),
    .cke_toolbox .cke_toolbar:nth-child(7),
    .cke_toolbox .cke_toolbar:nth-child(8),
    .cke_toolbox .cke_toolbar:nth-child(9),
    .cke_toolbox .cke_toolbar:nth-child(13),
    .cke_toolbox .cke_toolbar:nth-child(14){
        /*display: none;*/
    }
</style>
<?=$this->endSection()?>