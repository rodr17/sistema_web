<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('script')?>
<?=$this->endSection()?>
<?=$this->section('title')?>
	Editar página de Inicio
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="p-2">
		<h4 class="titulo-label">
			Editar página de "Inicio"
		</h4>
	</div>
	<hr>
	<div class="row">
		<div class="col-12 col-lg-3">
			<div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
				<button class="nav-link active" id="tab-banner" data-bs-toggle="pill" data-bs-target="#banner" type="button" role="tab" aria-controls="banner" aria-selected="true">
					Banner principal
				</button>
				<button class="nav-link" id="tab-lo-mas-buscado" data-bs-toggle="pill" data-bs-target="#lo-mas-buscado" type="button" role="tab" aria-controls="lo-mas-buscado" aria-selected="false">
					Sección lo mas buscado
				</button>
				<button class="nav-link" id="tab-quienes-somos" data-bs-toggle="pill" data-bs-target="#quienes-somos" type="button" role="tab" aria-controls="quienes-somos" aria-selected="false">
					Sección "Quienes somos"
				</button>
				<button class="nav-link" id="tab-categorias" data-bs-toggle="pill" data-bs-target="#categorias" type="button" role="tab" aria-controls="categorias" aria-selected="false">
					Categorias
				</button>
				<button class="nav-link" id="tab-freelancers" data-bs-toggle="pill" data-bs-target="#freelancers" type="button" role="tab" aria-controls="freelancers" aria-selected="false">
					Grid de Freelancers
				</button>
				<button class="nav-link" id="tab-trabajos" data-bs-toggle="pill" data-bs-target="#trabajos" type="button" role="tab" aria-controls="trabajos" aria-selected="false">
					Seccion "Trabajos"
				</button>
				<button class="nav-link" id="tab-latinoamerica" data-bs-toggle="pill" data-bs-target="#latinoamerica" type="button" role="tab" aria-controls="latinoamerica" aria-selected="false">
					Sección "Freelance en Latinoamerica"
				</button>
				<button class="nav-link" id="tab-mextemps" data-bs-toggle="pill" data-bs-target="#mextemps" type="button" role="tab" aria-controls="mextemps" aria-selected="false">
					Sección "¿Porque usar Mextemps?"
				</button>
				<button class="nav-link" id="tab-empezemos" data-bs-toggle="pill" data-bs-target="#empezemos" type="button" role="tab" aria-controls="empezemos" aria-selected="false">
					Sección "¡Empezemos el viaje!"
				</button>
			</div>
		</div>
		<div class="col-12 col-lg-9">
			<div class="tab-content" id="v-pills-tabContent">
				<div class="tab-pane fade show active" id="banner" role="tabpanel" aria-labelledby="tab-banner">
					<div id="">
						<?=view_cell('App\Libraries\FuncionesAdmin::mostrarSlides')?>
					</div>
					<form id="sec-banner">
						<input id="idslide" name="id" value="" type="hidden">
						<div class="form-floating mb-3">
							<input type="text" class="form-control" id="titulo-banner" name="titulo" placeholder="...">
							<label for="titulo-banner">Ingrese el título para el banner principal del sitio</label>
						</div>
						<div class="form-floating mb-3">
							<textarea class="form-control" placeholder="..." id="subtitulo-banner" name="subtitulo" style="height: 100px"></textarea>
							<label for="subtitulo-banner">Ingrese un texto como subtitulo para el banner principal</label>
						</div>
						<div class="mb-3">
                            <label class="form-label">Tipo de buscador</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="buscador" value="talentos" id="buscador-talentos">
                                <label class="form-check-label" for="buscador-talentos">
                                    Buscador de Contratantes
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="buscador" value="proyectos" id="buscador-proyectos">
                                <label class="form-check-label" for="buscador-proyectos">
                                    Buscador de Freelacers
                                </label>
                            </div>
                        </div>
						<div class="mb-3">
							<input type="hidden" id="slide" name="slide"><br>
							<label for="formFileSlide" class="form-label">Imagen para slider formato desktop.</label>
							<input class="form-control" type="file" id="formFileSlide" onchange="readURL(this, 'result-slide');">
						</div>
						<div class="p-3 text-center" >
							<img class="w-25" id="result-slide" src="<?=base_url('/assets/themes/imgs/page/about/banner-img.png')?>">
						</div>
						<div class=mb-3>
							<input type="hidden" id="slide-tablet" name="tablet">
							<label for="fondo-tablet" class="form-label">Imagen para slider formato Tableta.</label>
							<input class="form-control form-control-sm" id="fondo-tablet" type="file" onchange="readUrlImagen(this, 'slide-tablet', 'preview-tablet');">
							<div class="text-center m-3">
								<img id="preview-tablet" class="w-25"  />
							</div>
						</div>
						<div class="mb-3">
							<input type="hidden" id="slide-movil" name="movil">
							<label for="fondo-movil" class="form-label">Imagen para slider formato movil.</label>
							<input class="form-control form-control-sm" id="fondo-movil" type="file" onchange="readUrlImagen(this, 'slide-movil', 'preview-movil');">
							<div class="text-center m-3">
								<img id="preview-movil" class="w-25"  />
							</div>
						</div>
						<div class="text-center">
							<a class="btn btn-primary" id="save-slide">
								Guardar slide
							</a>
						</div>
					</form>
				</div>
				<div class="tab-pane fade" id="lo-mas-buscado" role="tabpanel" aria-labelledby="tab-lo-mas-buscado">
					<form id="form-mas-buscados">
						<div class="mb-5">
							<label>Sección de categorias más buscadas</label>
							<div class="form-floating my-3">
								<input type="text" class="form-control" id="titulo-mas-buscados" name="titulo" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mas-buscados', 'valor' => 'titulo'])?>" placeholder="...">
								<label for="titulo-mas-buscados">Titulo de sección</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="subtitulo-mas-buscados" name="subtitulo" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mas-buscados', 'valor' => 'subtitulo'])?>" placeholder="...">
								<label for="subtitulo-mas-buscados">Subtitulo de sección</label>
							</div>
							<div class="text-center mb-3">
								<a class="btn btn-primary" id="guardar-mas-buscados">
									Guardar cambios
								</a>
							</div>
						</div>
					</form>
					<div class="row">
						<div class="col-12 col-md-6 col-lg-3 text-center">
							<a class="btn btn-primary btn-mas-buscado" data-cat="1">
								Categoria 1
							</a>
						</div>
						<div class="col-12 col-md-6 col-lg-3 text-center">
							<a class="btn btn-primary btn-mas-buscado" data-cat="2">
								Categoria 2
							</a>
						</div>
						<div class="col-12 col-md-6 col-lg-3 text-center">
							<a class="btn btn-primary btn-mas-buscado" data-cat="3">
								Categoria 3
							</a>
						</div>
						<div class="col-12 col-md-6 col-lg-3 text-center">
							<a class="btn btn-primary btn-mas-buscado" data-cat="4">
								Categoria 4
							</a>
						</div>
					</div>
					<div class="my-3" id="edit-cat-mas-buscada">
					</div>
				</div>
				<div class="tab-pane fade" id="quienes-somos" role="tabpanel" aria-labelledby="tab-quienes-somos">
					<form id="form-quienes-somos">
						<div class="mb-5">
							<label>Sección de quienes somos</label>
							<div class="form-floating my-3">
								<input type="text" class="form-control" id="titulo-quienes-somos" name="titulo" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'quienes-somos', 'valor' => 'titulo'])?>" placeholder="...">
								<label form="titulo-quienes-somos">Título</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="frase-titulo" name="frase" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'quienes-somos', 'valor' => 'frase'])?>" placeholder="...">
								<label for="frase-titulo">Frase de título</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="..." id="texto-quienes-somos" name="parrafo" style="height: 75px"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'quienes-somos', 'valor' => 'parrafo'])?></textarea>
								<label for="texto-quienes-somos">Texto informativo principal</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="..." id="texto2-quienes-somos" name="parrafo2" style="height: 100px"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'quienes-somos', 'valor' => 'parrafo2'])?></textarea>
								<label for="texto-quienes-somos">Texto informativo</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="subtitulo" name="subtitulo" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'quienes-somos', 'valor' => 'subtitulo'])?>" placeholder="...">
								<label for="subtitulo">Subtítulo de sección</label>
							</div>
							<input type="hidden" id="imagen-quienes_somos" name="imagen" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'quienes-somos', 'valor' => 'imagen'])?>">
							<label for="archivo_quienes_somos" class="form-label">Imagen</label>
							<input class="form-control form-control-sm" id="archivo_quienes_somos" type="file" onchange="readUrlImagen(this, 'imagen-quienes_somos', 'visualizador-quienes_somos');">
							<div class="text-center m-3">
								<img id="visualizador-quienes_somos" class="w-25" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'quienes-somos', 'valor' => 'imagen'])?>"/>
							</div>
							<div class="text-center">
								<a class="btn btn-primary" id="save-quienes_somos">Guardar cambios</a>
							</div>
						</div>
					</form>
				</div>
				<div class="tab-pane fade" id="categorias" role="tabpanel" aria-labelledby="tab-categorias">
					<form id="form-categorias">
						<div class="form-floating mb-3"> 
							<input type="text" class="form-control" id="titulo-categorias" name="titulo" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categorias', 'valor' => 'titulo'])?>" placeholder="...">
							<label for="titulo-categorias">Titulo de sección</label>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" id="boton-categorias" name="boton" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categorias', 'valor' => 'boton'])?>" placeholder="...">
							<label for="boton-categorias">Texto del botón</label>
						</div>
						<div class="text-center">
							<a class="btn btn-primary" id="save-categorias">
								Guardar cambios
							</a>
						</div>
					</form>
					<div class="my-3">
						<div class="row">
							<div class="col-12 col-md-6 col-lg-3 text-center p-2">
								<a class="btn btn-primary btn-categoria" data-cat="1">
									Categoria 1
								</a>
							</div>
							<div class="col-12 col-md-6 col-lg-3 text-center p-2">
								<a class="btn btn-primary btn-categoria" data-cat="2">
									Categoria 2
								</a>
							</div>
							<div class="col-12 col-md-6 col-lg-3 text-center p-2">
								<a class="btn btn-primary btn-categoria" data-cat="3">
									Categoria 3
								</a>
							</div>
							<div class="col-12 col-md-6 col-lg-3 text-center p-2">
								<a class="btn btn-primary btn-categoria" data-cat="4">
									Categoria 4
								</a>
							</div>
							<div class="col-12 col-md-6 col-lg-3 text-center p-2">
								<a class="btn btn-primary btn-categoria" data-cat="5">
									Categoria 5
								</a>
							</div>
							<div class="col-12 col-md-6 col-lg-3 text-center p-2">
								<a class="btn btn-primary btn-categoria" data-cat="6">
									Categoria 6
								</a>
							</div>
							<div class="col-12 col-md-6 col-lg-3 text-center p-2">
								<a class="btn btn-primary btn-categoria" data-cat="7">
									Categoria 7
								</a>
							</div>
							<div class="col-12 col-md-6 col-lg-3 text-center p-2">
								<a class="btn btn-primary btn-categoria" data-cat="8">
									Categoria 8
								</a>
							</div>
						</div>
						<div class="my-3" id="edit-categoria">

						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="freelancers" role="tabpanel" aria-labelledby="tab-freelancers">
					<form id="form-freelancers">
						<label class="label fw-bold my-3">Titulo</label>
						<div class="row mb-3">
							<div class="col-12 col-md-6">
								<div class="form-floating">
									<input type="text" class="form-control" id="textnormal-freelancers" name="colnegro" placeholder="..." value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'freelancers', 'valor' => 'colnegro'])?>">
									<label for="textnormal-freelancers">Parte en color negro</label>
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="form-floating">
									<input type="text" class="form-control" id="textnaranja-freelancers" name="colnaranja" placeholder="..." value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'freelancers', 'valor' => 'colnaranja'])?>">
									<label for="textnaranja-freelancers">Parte en color naranja</label>
								</div>
							</div>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" id="subtitulo-freelancers" name="subtitulo" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'freelancers', 'valor' => 'subtitulo'])?>" placeholder="...">
							<label for="subtitulo-freelancers">Subtitulo de sección</label>
						</div>
						<div class="text-center">
							<a class="btn btn-primary" id="save-freeelancers">
								Guardar cambios
							</a>
						</div>
					</form>
				</div>
				<div class="tab-pane fade" id="trabajos" role="tabpanel" aria-labelledby="tab-trabajos">
					<form id="form-trabajos">
						<label class="label fw-bold my-3">Titulo</label>
						<div class="row mb-3">
							<div class="col-12 col-md-6">
								<div class="form-floating">
									<input type="text" class="form-control" id="textnormal-trabajos" name="colnegro" placeholder="..." value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'trabajos', 'valor' => 'colnegro'])?>">
									<label for="textnormal-trabajos">Parte en color negro</label>
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="form-floating">
									<input type="text" class="form-control" id="textnaranja-trabajos" name="colnaranja" placeholder="..." value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'trabajos', 'valor' => 'colnaranja'])?>">
									<label for="textnaranja-trabajos">Parte en color naranja</label>
								</div>
							</div>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" id="subtitulo-trabajos" name="subtitulo" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'trabajos', 'valor' => 'subtitulo'])?>" placeholder="...">
							<label for="subtitulo-trabajos">Subtitulo de sección</label>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" id="inferior-trabajos" name="inferior" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'trabajos', 'valor' => 'inferior'])?>" placeholder="...">
							<label for="inferior-trabajos">Texto inferior</label>
						</div>
						<div class="text-center">
							<a class="btn btn-primary" id="save-trabajos">
								Guardar cambios
							</a>
						</div>
					</form>
				</div>
				<div class="tab-pane fade" id="latinoamerica" role="tabpanel" aria-labelledby="tab-latinoamerica">
					<div class="p-2">
						<form id="freelance-latinoamerica">
							<label class="label fw-bold my-3">Titulo</label>
							<div class="row">
								<div class="col-12 col-md-6">
									<div class="form-floating">
										<input type="text" class="form-control" id="textnormal" name="colnegro" placeholder="..." value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'latinoamerica', 'valor' => 'colnegro'])?>">
										<label for="textnormal">Parte en color negro</label>
									</div>
								</div>
								<div class="col-12 col-md-6">
									<div class="form-floating">
										<input type="text" class="form-control" id="textnaranja" name="colnaranja" placeholder="..." value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'latinoamerica', 'valor' => 'colnaranja'])?>">
										<label for="textnaranja">Parte en color naranja</label>
									</div>
								</div>
							</div>
							<label class="label fw-bold my-3">Texto superior</label>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="superior" name="superior" placeholder="..." value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'latinoamerica', 'valor' => 'superior'])?>">
								<label for="superior">Texto superior al titulo</label>
							</div>
							<div class="mb-3">
								<label class="mb-3" for="contenido-latinoamerica">Texto de sección</label>
								<textarea name="contenido" id="contenido-latinoamerica"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'latinoamerica', 'valor' => 'contenido'])?></textarea>
							</div>
							<div class="text-center my-3">
								<a class="btn btn-primary" id="save-latinoamerica">
									Guardar cambios
								</a>
							</div>
						</form>
					</div>
				</div>
				<div class="tab-pane fade" id="mextemps" role="tabpanel" aria-labelledby="tab-mextemps">
					<form id="form-mextemps">
						<label class="label fw-bold my-3">Titulo</label>
						<div class="row mb-3">
							<div class="col-12 col-md-6">
								<div class="form-floating">
									<input type="text" class="form-control" id="textnormalmextemps" name="colnegro" placeholder="..." value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps', 'valor' => 'colnegro'])?>">
									<label for="textnormalmextemps">Parte en color negro</label>
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="form-floating">
									<input type="text" class="form-control" id="textnaranjamextemps" name="colnaranja" placeholder="..." value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps', 'valor' => 'colnaranja'])?>">
									<label for="textnaranjamextemps">Parte en color naranja</label>
								</div>
							</div>
						</div>
						<div class="mb-3">
							<div class="form-floating">
								<textarea class="form-control" placeholder="..." name="contenido" id="contenido-mextemps" style="height: 200px"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps', 'valor' => 'contenido'])?></textarea>
								<label for="contenido-mextemps">Texto de sección</label>
							</div>
						</div>
						<div class="text-center mb-3">
							<a class="btn btn-primary" id="guardar-mextemps">
								Guardar cambios  
							</a>
						</div>
					</form>
					<div class="row justify-content-center">
						<div class="col-12 col-md-auto text-center">
							<a class="btn btn-primary boton-icon" data-icono="1">
								Icono 1
							</a>
						</div>
						<div class="col-12 col-md-auto text-center">
							<a class="btn btn-primary boton-icon" data-icono="2">
								Icono 2
							</a>
						</div>
						<div class="col-12 col-md-auto text-center">
							<a class="btn btn-primary boton-icon" data-icono="3">
								Icono 3
							</a>
						</div>
						<div class="col-12 col-md-auto text-center">
							<a class="btn btn-primary boton-icon" data-icono="4">
								Icono 4
							</a>
						</div>
						<div class="col-12 col-md-auto text-center">
							<a class="btn btn-primary boton-icon" data-icono="5">
								Icono 5
							</a>
						</div>
						<div class="col-12 py-3">
							<form id="form-icono-mextemps">
								<input type="hidden" id="idicono" name="id">
								<div class="form-floating mb-3">
									<input type="text" class="form-control" id="titulo-icono" name="titulo" placeholder="...">
									<label for="titulo-icono">Titulo</label>
								</div>
								<div class="form-floating mb-3">
									<textarea class="form-control" placeholder="..." name="texto" id="texto-icono-mextemps" style="height: 200px"></textarea>
									<label for="texto-icono-mextemps">Texto de icono</label>
								</div>
								<input type="hidden" id="icono" name="icono">
								<input class="form-control form-control-sm" id="icon" type="file" onchange="readUrlIcono(this);">
								<div class="text-center m-3">
									<img id="preview-icono" class="w-25" src="http://placehold.it/180" alt="your image" />
								</div>
								<div class="text-center my-3">
									<a class="btn btn-primary" id="save-icono">
										Guardar cambios
									</a>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="empezemos" role="tabpanel" aria-labelledby="tab-empezemos">
					<form id="form-empezemos">
						<div class="form-floating mb-3">
							<input type="text" class="form-control" id="titulo-empezemos" name="titulo" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'empezemos', 'valor' => 'titulo'])?>" placeholder="">
							<label for="titulo-empezemos">Titulo de sección</label>
						</div>
						<div class="form-floating mb-3">
							<input type="text" class="form-control" id="texto-empezemos" name="texto" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'empezemos', 'valor' => 'texto'])?>" placeholder="">
							<label for="texto-empezemos">Texto a lado de botón</label>
						</div>
						<div class="mb-3">
							<label class="label">Fondo de seccion para escritorio</label>
							<input type="hidden" id="fondo-empezemos64" name="fondo" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'empezemos', 'valor' => 'fondo'])?>">
							<input class="form-control form-control-sm" id="fondo-empezemos" type="file" onchange="readUrlImagen(this, 'fondo-empezemos64', 'preview-fondo');">
							<div class="text-center m-3">
								<img id="preview-fondo" class="w-25" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'empezemos', 'valor' => 'fondo'])?>" alt="your image" />
							</div>
						</div>
						<div class="mb-3">
							<label class="label">Fondo de seccion para tablets</label>
							<input type="hidden" id="fondo-empezemos64-tableta" name="fondo-tablet" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'empezemos', 'valor' => 'fondo-tableta'])?>">
							<input class="form-control form-control-sm" id="fondo-empezemos-tableta" type="file" onchange="readUrlImagen(this, 'fondo-empezemos64-tableta', 'preview-fondo-tableta');">
							<div class="text-center m-3">
								<img id="preview-fondo-tableta" class="w-25" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'empezemos', 'valor' => 'fondo-tableta'])?>" alt="your image" />
							</div>
						</div>
						<div class="mb-3">
							<label class="label">Fondo de seccion para dispositivos moviles</label>
							<input type="hidden" id="fondo-empezemos64-movil" name="fondo-movil" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'empezemos', 'valor' => 'fondo-movil'])?>">
							<input class="form-control form-control-sm" id="fondo-empezemos-movil" type="file" onchange="readUrlImagen(this, 'fondo-empezemos64-movil', 'preview-fondo-movil');">
							<div class="text-center m-3">
								<img id="preview-fondo-movil" class="w-25" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'empezemos', 'valor' => 'fondo-movil'])?>" alt="your image" />
							</div>
						</div>
						<div class="text-center my-3">
							<a class="btn btn-primary" id="save-empezemos">
								Guardar cambios
							</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var editor = CKEDITOR.replace('contenido-latinoamerica');
	CKEDITOR.config.htmlEncodeOutput;
	CKEDITOR.config.entities = false;
	CKEDITOR.config.fullPage = false;
	CKEDITOR.config.removeFormatTags = 'b,big,code,del,dfn,em,font,i,ins,kbd,p';

	$(document).ready(function(){
		$('#form-icono-mextemps').hide();
		$('#edit-categoria').hide();
	});
	let banderaTamano = false;
	function readURL(input, resultado) {
		if (input.files && input.files[0]) {
			let reader = new FileReader();
			reader.onload = function (e) {
				$('#'+resultado).attr('src', e.target.result);
				$('#slide').val(e.target.result);
				var img = new Image();
				img.onload = function dimension() {
					console.log('ancho: '+this.width.toFixed(0));
					console.log('alto: '+this.height.toFixed(0));
					if (this.width.toFixed(0) != 2000 && this.height.toFixed(0) != 600) {
						alertify.error('Las medidas deben ser: 2000px x 600px');
						$('#'+resultado).attr('src', '<?=base_url('/assets/themes/imgs/page/about/banner-img.png')?>');
						$('#slide').val(null);
					}
				};
				img.src = URL.createObjectURL(input.files[0]);
				// Devolvemos false porque falta validar el tamaño de la imagen
				console.log(img);
				return false;
			};
			reader.readAsDataURL(input.files[0]);
		}else{
			$('#'+resultado).attr('src', '<?=base_url('/assets/themes/imgs/page/about/banner-img.png')?>');
			$('#slide').val(null);
		}
	}
	function readUrlIcono(input){
		if (input.files && input.files[0]) {
			let reader = new FileReader();
			reader.onload = function (e) {
				$('#preview-icono').attr('src', e.target.result);
				$('#icono').val(e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
	function readUrlImagen(input, guardar, resultado){
		if(input.files && input.files[0]){
			let reader = new FileReader();
			reader.onload = function (e){
				$('#'+resultado).attr('src', e.target.result);
				$('#'+guardar).val(e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		}else{
			$('#'+resultado).attr('src', '<?=base_url('/assets/themes/imgs/page/about/banner-img.png')?>')
		}
	}
	$('#save-freeelancers').click(function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Secciones/saveGridFreelancers')?>',
			data: $('#form-freelancers').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error'){
					alertify.warning(cont.mensaje, 10);
				}else{
					alertify.success(cont.mensaje, 10);
				}
			}, error: function(data){
				alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
			}
		});
	});
	$('#save-categorias').click(function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Secciones/saveSeccionCategorias')?>',
			data: $('#form-categorias').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error'){
					alertify.warning(cont.mensaje, 10);
				}else{
					alertify.success(cont.mensaje, 10);
				}
			}, error: function(data){
				alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
			}
		});
	});
	$('#save-slide').click(function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('Secciones/saveSlide')?>',
			data: $('#sec-banner').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error'){
					alertify.warning(cont.mensaje, 10);
				}else{
					$('#result-slide').attr('src', '<?=base_url('/assets/themes/imgs/page/about/banner-img.png')?>');
					$('#slide').val(null);
					$('#sec-banner')[0].reset();
					alertify.success(cont.mensaje, 10);
				}
			}, error: function(data){
				alertify.error('Error, comuniquese con el area de desarrollo.', 10);
			}
		});
	});
	$('#save-latinoamerica').click(function(){
		let colnegro = $('#textnormal').val();
		let colnaranja = $('#textnaranja').val();
		let superior = $('#superior').val();
		let editorData = editor.getData();
		let postBody = editorData.replace(/&nbsp;/gi,' ');
		let dataString = 'colnegro='+colnegro+'&colnaranja='+colnaranja+'&superior='+superior+'&contenido='+postBody;
		$.ajax({
			type: 'POST',
			url: '<?=base_url('Secciones/saveLatinoamerica')?>',
			data: dataString,
			cache: false,
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error'){
					alertify.warning(cont.mensaje, 10);
				}else{
					alertify.success(cont.mensaje, 10);
				}
			}, error: function(data){
				alertify.error('Error, comuniquese con el equipo de desarrollo', 10);
			}
		});
	});
	$('#guardar-mextemps').click(function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('Secciones/saveMextemps')?>',
			data:$('#form-mextemps').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error'){
					alertify.warning(cont.mensaje, 10);
				}else{
					alertify.success(cont.mensaje, 10);
				}
			}, error: function(data){
				alertify.error('Error, comuniquese con el equipo de desarrollo', 10);
			}
		});
	});
	$('.boton-icon').click(function(){
		let idicono = $(this).attr('data-icono');
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Secciones/saveIconoMextemps')?>',
			data:{id:idicono},
			success: function(data){
				$('#form-icono-mextemps').show();
				$('#idicono').val(idicono);
				let cont = JSON.parse(data);
				$('#titulo-icono').val(cont.titulo);
				$('#texto-icono-mextemps').val(cont.texto);
				$('#icono').val(cont.icono);
				$('#preview-icono').attr('src', cont.icono);
			}, error: function(data){
				alertify.error('Error, comuniquese con el equipo de desarrollo', 10);
			}
		});
	});
	$('#save-quienes_somos').click(function (e) { 
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Secciones/guardar_Quienes_somos')?>',
			data: $('#form-quienes-somos').serialize(),
			success: function(data){
				let respuesta = JSON.parse(data);
				if(respuesta.alerta == 'error') return alertify.error(respuesta.mensaje, 10);
				
				return alertify.success(respuesta.mensaje, 10);
			}, error: function(data){
				alertify.error('Error, comuniquese con el equipo de desarrollo', 10);
			}
		});
	});
	$('.btn-categoria').click(function(){
		let id = $(this).attr('data-cat');
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Secciones/verCategoriaInicio')?>',
			data:{id:id},
			success: function(data){
				$('#edit-categoria').show();
				$('#edit-categoria').html(data);
			}, error: function(data){
				alertify.error('Error, comuniquese con el equipo de desarrollo', 10);
			}
		});
	});
	$('#save-icono').click(function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/secciones/guardarIconoMextemps')?>',
			data:$('#form-icono-mextemps').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error'){
					alertify.warning(cont.mensaje, 10);
				}else{
					alertify.success(cont.mensaje, 10);
				}
			}, error: function(data){
				alertify.error('Error, comuniquese con el equipo de soporte', 10);
			}
		});
	});
	$('#save-empezemos').click(function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Secciones/saveEmpezemos')?>',
			data: $('#form-empezemos').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error'){
					alertify.warning(cont.mensaje, 10);
				}else{
					$('#preview-fondo').attr('src', '<?=base_url('/assets/themes/imgs/page/about/banner-img.png')?>');
					$('#form-empezemos')[0].reset();
					alertify.success(cont.mensaje, 10);
				}
			}, error: function(data){
				alertify.error('Error, comuniquese con el equipo de desarrollo', 10);
			}
		});
	});
	$('#save-trabajos').click(function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Secciones/saveTrabajosInicio')?>',
			data: $('#form-trabajos').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error'){
					alertify.warning(cont.mensaje, 10);
				}else{
					$('#form-trabajos')[0].reset();
					alertify.success(cont.mensaje, 10);
				}
			}, error: function(data){
				alertify.error('Error, comuniquese con el equipo de desarrollo., 10');
			}
		});
	});
	$('#guardar-mas-buscados').click(function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Secciones/saveMasBuscadosInicio')?>',
			data: $('#form-mas-buscados').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
				return alertify.success(cont.mensaje, 10);
			}, error: function(data){
				alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
			}
		});
	});
	$('.btn-mas-buscado').click(function(){
		let cat = $(this).attr('data-cat');
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Secciones/verCatMasBuscada')?>',
			data:{cat:cat},
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
				return $('#edit-cat-mas-buscada').html(cont.mensaje);
			}, error: function(data){
				alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
			}
		});
	});
</script>
<style>
	.cke_toolbar_break,
	.cke_toolbox .cke_toolbar:nth-child(2),
	.cke_toolbox .cke_toolbar:nth-child(3),
	.cke_toolbox .cke_toolbar:nth-child(4),
	.cke_toolbox .cke_toolbar:nth-child(7),
	.cke_toolbox .cke_toolbar:nth-child(8),
	.cke_toolbox .cke_toolbar:nth-child(9),
	.cke_toolbox .cke_toolbar:nth-child(13),
	.cke_toolbox .cke_toolbar:nth-child(14){
		display: none;
	}
</style>
<?=$this->endSection()?>