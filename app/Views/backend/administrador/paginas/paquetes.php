<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('script')?>
<?=$this->endSection()?>
<?=$this->section('title')?>
    Editar página Tarifas
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
    <div class="p-2">
        <h4 class="titulo-label">
            Editar página de "Tarifas"
        </h4>
    </div>
    <hr>
    <div class="row">
        <div class="col-12 col-lg-3">
            <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <button class="nav-link active" id="tab-textos" data-bs-toggle="pill" data-bs-target="#textos" type="button" role="tab" aria-controls="textos" aria-selected="true">
                    Textos
                </button>
                <button class="nav-link" id="tab-preguntas" data-bs-toggle="pill" data-bs-target="#preguntas" type="button" role="tab" aria-controls="preguntas" aria-selected="false">
                    Preguntas frecuentes
                </button>
            </div>
        </div>
        <div class="col-12 col-lg-9">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="textos" role="tabpanel" aria-labelledby="tab-textos">
                    <form id="sec-textos">
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" id="titulo" name="titulo" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'paquetes', 'tipo' => 'info', 'valor' => 'titulo'])?>" placeholder="...">
                            <label for="titulo">Titulo de sección</label>
                        </div>
                        <div class="form-floating mb-3">
                            <textarea class="form-control" placeholder="..." id="subtitulo" name="subtitulo" style="height: 100px"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'paquetes', 'tipo' => 'info', 'valor' => 'subtitulo'])?></textarea>
                            <label for="subtitulo">Subtitulo de sección</label>
                        </div>
                        <div class="text-center">
                            <a class="btn btn-primary" id="save-cambios">
                                Guardar cambios
                            </a>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="preguntas" role="tabpanel" aria-labelledby="tab-preguntas">
                    <div class="row">
                        <div class="col-12 col-lg-3" id="sec-preguntas">
                            <?=view_cell('App\Libraries\FuncionesAdmin::viewPreguntaPaquetes')?>
                        </div>
                        <div class="col-12 col-lg-9">
                            <form id="form-preguntas">
                                <input type="hidden" id="id" name="id">
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" id="pregunta" name="pregunta" value="" placeholder="...">
                                    <label for="titulo">Pregunta</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <textarea class="form-control" placeholder="..." id="respuesta" name="respuesta" style="height: 100px"></textarea>
                                    <label for="subtitulo">Respuesta</label>
                                </div>
                                <div class="text-center">
                                    <a class="btn btn-primary" id="guardar-pregunta">
                                        Guardar pregunta
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#save-cambios').click(function(){
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Secciones/savePaquetes')?>',
            data:$('#sec-textos').serialize(),
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error'){
                    alertify.warning(cont.mensaje, 10);
                }else{
                    alertify.success(cont.mensaje, 10);
                }
            }, error: function(data){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo', 10);
            }
        });
    });
    $('#guardar-pregunta').click(function(){
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Secciones/savePreguntaPaquetes')?>',
            data: $('#form-preguntas').serialize(),
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error'){
                    alertify.warning(cont.mensaje, 10);
                }else{
                    $('#form-preguntas')[0].reset();
                    alertify.success(cont.mensaje, 10);
                    $('#sec-preguntas').load('<?=base_url('/Secciones/pregFrecuentesPaquetes')?>');
                    $('#guardar-pregunta').html('Guardar pregunta');
                }
            }, error: function(data){
                alertify.error('A surgido un error comuniquese con el equipo de desarrollo', 10);
            }
        });
    });
</script>
<?=$this->endSection()?>