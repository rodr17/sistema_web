<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Preguntas frecuentes
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="p-2">
		<h1 class="titulo-label h4">Soporte</h1>
	</div>
	<hr>
	<div class="text-center">
		<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
			<li class="nav-item" role="presentation">
				<button class="nav-link active" id="facturacion" data-bs-toggle="pill" data-bs-target="#pills-facturacion" type="button" role="tab" aria-controls="pills-facturacion" aria-selected="true">
					Seccion "Facturación y pagos"
				</button>
			</li>
			<li class="nav-item" role="presentation">
				<button class="nav-link" id="licencias" data-bs-toggle="pill" data-bs-target="#pills-licencias" type="button" role="tab" aria-controls="pills-licencias" aria-selected="false">
					Seccion "Cómo elegir las licencias adecuadas"
				</button>
			</li>
			<li class="nav-item" role="presentation">
				<button class="nav-link" id="cuenta" data-bs-toggle="pill" data-bs-target="#pills-cuenta" type="button" role="tab" aria-controls="pills-cuenta" aria-selected="false">
					Seccion "Cuenta"
				</button>
			</li>
			<li class="nav-item" role="presentation">
				<button class="nav-link" id="asistencia" data-bs-toggle="pill" data-bs-target="#pills-asistencia" type="button" role="tab" aria-controls="pills-asistencia" aria-selected="false">
					Seccion "Asistencia técnica"
				</button>
			</li>
		</ul>
	</div>
	<div class="tab-content" id="pills-tabContent">
		<div class="tab-pane fade show active" id="pills-facturacion" role="tabpanel" aria-labelledby="facturacion">
			<div class="row">
				<div class="col-12 mb-2">
					<div class="row">
						<div class="col-6 text-center align-self-center">
							Ingrese el codigo del icono para la sección.<br>
							<a class="btn btn-primary actua-icono" data-id="1">
								Actualizar icono
							</a>
						</div>
						<div class="col-6">
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="icono-1" placeholder="..." value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'icono-soporte', 'tipo' => '1', 'valor' => 'icono'])?>">
								<label for="icono-1">Codigo de lord icon</label>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 1</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-1-sec-1" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '1', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-1-sec-1">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-1-sec-1" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '1', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-1-sec-1">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-1-sec-1" data-respuesta="respuesta-soporte-1-sec-1" data-seccion="pregunta-soporte-1" data-id="1">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 2</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-2-sec-1" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '2', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-2-sec-1">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-2-sec-1" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '2', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-2-sec-1">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-2-sec-1" data-respuesta="respuesta-soporte-2-sec-1" data-seccion="pregunta-soporte-1" data-id="2">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 3</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-3-sec-1" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '3', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-3-sec-1">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-3-sec-1" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '3', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-3-sec-1">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-3-sec-1" data-respuesta="respuesta-soporte-3-sec-1" data-seccion="pregunta-soporte-1" data-id="3">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 4</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-4-sec-1" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '4', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-4-sec-1">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-4-sec-1" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '4', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-4-sec-1">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-4-sec-1" data-respuesta="respuesta-soporte-4-sec-1" data-seccion="pregunta-soporte-1" data-id="4">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 5</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-5-sec-1" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '5', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-5-sec-1">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-5-sec-1" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '5', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-5-sec-1">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-5-sec-1" data-respuesta="respuesta-soporte-5-sec-1" data-seccion="pregunta-soporte-1" data-id="5">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="pills-licencias" role="tabpanel" aria-labelledby="licencias">
			<div class="row">
				<div class="col-12 mb-2">
					<div class="row">
						<div class="col-6 text-center align-self-center">
							Ingrese el codigo del icono para la sección.<br>
							<a class="btn btn-primary actua-icono" data-id="2">
								Actualizar icono
							</a>
						</div>
						<div class="col-6">
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="icono-2" placeholder="..." value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'icono-soporte', 'tipo' => '2', 'valor' => 'icono'])?>">
								<label for="icono-2">Codigo de lord icon</label>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 1</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-1-sec-2" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-2', 'tipo' => '1', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-1-sec-2">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-1-sec-2" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-2', 'tipo' => '1', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-1-sec-2">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-1-sec-2" data-respuesta="respuesta-soporte-1-sec-2" data-seccion="pregunta-soporte-2" data-id="1">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 2</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-2-sec-2" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-2', 'tipo' => '2', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-2-sec-2">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-2-sec-2" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-2', 'tipo' => '2', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-2-sec-2">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-2-sec-2" data-respuesta="respuesta-soporte-2-sec-2" data-seccion="pregunta-soporte-2" data-id="2">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 3</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-3-sec-2" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-2', 'tipo' => '3', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-3-sec-2">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-3-sec-2" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-2', 'tipo' => '3', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-3-sec-2">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-3-sec-2" data-respuesta="respuesta-soporte-3-sec-2" data-seccion="pregunta-soporte-2" data-id="3">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 4</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-4-sec-2" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-2', 'tipo' => '4', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-4-sec-2">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-4-sec-2" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-2', 'tipo' => '4', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-4-sec-2">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-4-sec-2" data-respuesta="respuesta-soporte-4-sec-2" data-seccion="pregunta-soporte-2" data-id="4">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="pills-cuenta" role="tabpanel" aria-labelledby="cuenta">
			<div class="row">
				<div class="col-12 mb-2">
					<div class="row">
						<div class="col-6 text-center align-self-center">
							Ingrese el codigo del icono para la sección.<br>
							<a class="btn btn-primary actua-icono" data-id="3">
								Actualizar icono
							</a>
						</div>
						<div class="col-6">
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="icono-3" placeholder="..." value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'icono-soporte', 'tipo' => '3', 'valor' => 'icono'])?>">
								<label for="icono-3">Codigo de lord icon</label>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 1</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-1-sec-3" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '1', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-1-sec-3">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-1-sec-3" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '1', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-1-sec-3">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-1-sec-3" data-respuesta="respuesta-soporte-1-sec-3" data-seccion="pregunta-soporte-3" data-id="1">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 2</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-2-sec-3" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '2', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-2-sec-3">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-2-sec-3" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '2', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-2-sec-3">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-2-sec-3" data-respuesta="respuesta-soporte-2-sec-3" data-seccion="pregunta-soporte-3" data-id="2">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 3</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-3-sec-3" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '3', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-3-sec-3">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-3-sec-3" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '3', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-3-sec-3">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-3-sec-3" data-respuesta="respuesta-soporte-3-sec-3" data-seccion="pregunta-soporte-3" data-id="3">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 4</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-4-sec-3" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '4', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-4-sec-3">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-4-sec-3" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '4', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-4-sec-3">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-4-sec-3" data-respuesta="respuesta-soporte-4-sec-3" data-seccion="pregunta-soporte-3" data-id="4">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 5</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-5-sec-3" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '5', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-5-sec-3">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-5-sec-3" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '5', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-5-sec-3">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-5-sec-3" data-respuesta="respuesta-soporte-5-sec-3" data-seccion="pregunta-soporte-3" data-id="5">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="pills-asistencia" role="tabpanel" aria-labelledby="asistencia">
			<div class="row">
				<div class="col-12 mb-2">
					<div class="row">
						<div class="col-6 text-center align-self-center">
							Ingrese el codigo del icono para la sección.<br>
							<a class="btn btn-primary actua-icono" data-id="4">
								Actualizar icono
							</a>
						</div>
						<div class="col-6">
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="icono-4" placeholder="..." value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'icono-soporte', 'tipo' => '4', 'valor' => 'icono'])?>">
								<label for="icono-4">Codigo de lord icon</label>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 1</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-1-sec-4" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-4', 'tipo' => '1', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-1-sec-4">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-1-sec-4" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-4', 'tipo' => '1', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-1-sec-4">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-1-sec-4" data-respuesta="respuesta-soporte-1-sec-4" data-seccion="pregunta-soporte-4" data-id="1">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 2</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-2-sec-4" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-4', 'tipo' => '2', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-2-sec-4">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-2-sec-4" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-4', 'tipo' => '2', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-2-sec-4">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-2-sec-4" data-respuesta="respuesta-soporte-2-sec-4" data-seccion="pregunta-soporte-4" data-id="2">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 3</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-3-sec-4" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-4', 'tipo' => '3', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-3-sec-4">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-3-sec-4" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-4', 'tipo' => '3', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-3-sec-4">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-3-sec-4" data-respuesta="respuesta-soporte-3-sec-4" data-seccion="pregunta-soporte-4" data-id="3">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-2">
					<div class="card">
						<div class="card-body">
							<div class="text-center mb-2">
								<label class="fw-bold">Pregunta 4</label>
							</div>
							<div class="form-floating mb-3">
								<input type="text" class="form-control" id="preg-soporte-4-sec-4" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-4', 'tipo' => '4', 'valor' => 'pregunta'])?>" placeholder="...">
								<label for="preg-soporte-4-sec-4">Pregunta</label>
							</div>
							<div class="form-floating mb-3">
								<textarea class="form-control" placeholder="Leave a comment here" id="respuesta-soporte-4-sec-4" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-4', 'tipo' => '4', 'valor' => 'respuesta'])?></textarea>
								<label for="respuesta-soporte-4-sec-4">Respuesta</label>
							</div>
							<div class="text-center">
								<a class="btn btn-primary btn-sm actua-preg" data-preg="preg-soporte-4-sec-4" data-respuesta="respuesta-soporte-4-sec-4" data-seccion="pregunta-soporte-4" data-id="4">
									<small>Actualizar</small>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('.actua-icono').click(function(){
		let id = $(this).attr('data-id');
		$.ajax({
			type: 'POST',
			url: '<?= base_url('/Administrador/saveIconoSoporte') ?>',
			data:{id:id, icono:$('#icono-'+id).val()},
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error') return alertify.warning(cont.mensaje);
				return alertify.success(cont.mensaje);
			}, error: function(data){
				alertify.error('Ha surgido un error, comuniquese con el equipo de desarrollo.');
			}
		});
	});
	$('.actua-preg').click(function(){
		let id = $(this).attr('data-id');
		let preg = $(this).attr('data-preg');
		let resp = $(this).attr('data-respuesta');
		let seccion = $(this).attr('data-seccion');
		let pregunta = $('#'+preg).val();
		let respuesta = $('#'+resp).val();
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Administrador/actuaPregSoporte')?>',
			data:{id:id, preg:pregunta, resp:respuesta, seccion:seccion},
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error') return alertify.warning(cont.mensaje);
				return alertify.success(cont.mensaje);
			}, error: function(data){
				alertify.error('Ha surgido un error, comuniquese con el equipo de desarrollo.');
			}
		});
	});
</script>
<?=$this->endSection()?>