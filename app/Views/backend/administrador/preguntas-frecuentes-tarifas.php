<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
    Preguntas frecuentes area de tarifas
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
    <div class="p-2">
        <h1 class="titulo-label h4">
            Preguntas frecuentes area de tarifas
        </h1>
    </div>
    <hr>
    <div class="row">
        <div class="col-12 col-lg-6 mb-2">
            <div class="card">
                <div class="card-body">
                    <div class="text-center mb-2">
                        <label class="fw-bold">Pregunta 1</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="preg-tarifa-1" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => '1', 'valor' => 'pregunta'])?>" placeholder="...">
                        <label for="preg-tarifa-1">Pregunta</label>
                    </div>
                    <div class="form-floating mb-3">
                        <textarea class="form-control" placeholder="Leave a comment here" id="respuesta-1" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => '1', 'valor' => 'respuesta'])?></textarea>
                        <label for="respuesta-1">Respuesta</label>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-primary btn-sm actua-preg" data-preg="preg-tarifa-1" data-respuesta="respuesta-1" data-id="1">
                            <small>Actualizar</small>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 mb-2">
            <div class="card">
                <div class="card-body">
                    <div class="text-center mb-2">
                        <label class="fw-bold">Pregunta 2</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="preg-tarifa-2" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => '2', 'valor' => 'pregunta'])?>" placeholder="...">
                        <label for="preg-tarifa-2">Pregunta</label>
                    </div>
                    <div class="form-floating mb-3">
                        <textarea class="form-control" placeholder="Leave a comment here" id="respuesta-2" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => '2', 'valor' => 'respuesta'])?></textarea>
                        <label for="respuesta-2">Respuesta</label>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-primary btn-sm actua-preg" data-preg="preg-tarifa-2" data-respuesta="respuesta-2" data-id="2">
                            <small>Actualizar</small>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 mb-2">
            <div class="card">
                <div class="card-body">
                    <div class="text-center mb-2">
                        <label class="fw-bold">Pregunta 3</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="preg-tarifa-3" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => '3', 'valor' => 'pregunta'])?>" placeholder="...">
                        <label for="preg-tarifa-3">Pregunta</label>
                    </div>
                    <div class="form-floating mb-3">
                        <textarea class="form-control" placeholder="Leave a comment here" id="respuesta-3" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => '3', 'valor' => 'respuesta'])?></textarea>
                        <label for="respuesta-3">Respuesta</label>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-primary btn-sm actua-preg" data-preg="preg-tarifa-3" data-respuesta="respuesta-3" data-id="3">
                            <small>Actualizar</small>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 mb-2">
            <div class="card">
                <div class="card-body">
                    <div class="text-center mb-2">
                        <label class="fw-bold">Pregunta 4</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="preg-tarifa-4" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => '4', 'valor' => 'pregunta'])?>" placeholder="...">
                        <label for="preg-tarifa-4">Pregunta</label>
                    </div>
                    <div class="form-floating mb-3">
                        <textarea class="form-control" placeholder="Leave a comment here" id="respuesta-4" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => '4', 'valor' => 'respuesta'])?></textarea>
                        <label for="respuesta-4">Respuesta</label>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-primary btn-sm actua-preg" data-preg="preg-tarifa-4" data-respuesta="respuesta-4" data-id="4">
                            <small>Actualizar</small>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 mb-2">
            <div class="card">
                <div class="card-body">
                    <div class="text-center mb-2">
                        <label class="fw-bold">Pregunta 5</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="preg-tarifa-5" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => '5', 'valor' => 'pregunta'])?>" placeholder="...">
                        <label for="preg-tarifa-5">Pregunta</label>
                    </div>
                    <div class="form-floating mb-3">
                        <textarea class="form-control" placeholder="Leave a comment here" id="respuesta-5" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => '5', 'valor' => 'respuesta'])?></textarea>
                        <label for="respuesta-5">Respuesta</label>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-primary btn-sm actua-preg" data-preg="preg-tarifa-5" data-respuesta="respuesta-5" data-id="5">
                            <small>Actualizar</small>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 mb-2">
            <div class="card">
                <div class="card-body">
                    <div class="text-center mb-2">
                        <label class="fw-bold">Pregunta 6</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="preg-tarifa-6" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => '6', 'valor' => 'pregunta'])?>" placeholder="...">
                        <label for="preg-tarifa-6">Pregunta</label>
                    </div>
                    <div class="form-floating mb-3">
                        <textarea class="form-control" placeholder="Leave a comment here" id="respuesta-6" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => '6', 'valor' => 'respuesta'])?></textarea>
                        <label for="respuesta-6">Respuesta</label>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-primary btn-sm actua-preg" data-preg="preg-tarifa-6" data-respuesta="respuesta-6" data-id="6">
                            <small>Actualizar</small>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 mb-2">
            <div class="card">
                <div class="card-body">
                    <div class="text-center mb-2">
                        <label class="fw-bold">Pregunta 7</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="preg-tarifa-7" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => '7', 'valor' => 'pregunta'])?>" placeholder="...">
                        <label for="preg-tarifa-7">Pregunta</label>
                    </div>
                    <div class="form-floating mb-3">
                        <textarea class="form-control" placeholder="Leave a comment here" id="respuesta-7" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => '7', 'valor' => 'respuesta'])?></textarea>
                        <label for="respuesta-7">Respuesta</label>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-primary btn-sm actua-preg" data-preg="preg-tarifa-7" data-respuesta="respuesta-7" data-id="7">
                            <small>Actualizar</small>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 mb-2">
            <div class="card">
                <div class="card-body">
                    <div class="text-center mb-2">
                        <label class="fw-bold">Pregunta 8</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="preg-tarifa-8" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => '8', 'valor' => 'pregunta'])?>" placeholder="...">
                        <label for="preg-tarifa-8">Pregunta</label>
                    </div>
                    <div class="form-floating mb-3">
                        <textarea class="form-control" placeholder="Leave a comment here" id="respuesta-8" style="height: 100px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => '8', 'valor' => 'respuesta'])?></textarea>
                        <label for="respuesta-8">Respuesta</label>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-primary btn-sm actua-preg" data-preg="preg-tarifa-8" data-respuesta="respuesta-8" data-id="8">
                            <small>Actualizar</small>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.actua-preg').click(function(){
        let id = $(this).attr('data-id');
        let preg = $(this).attr('data-preg');
        let resp = $(this).attr('data-respuesta');
        let pregunta = $('#'+preg).val();
        let respuesta = $('#'+resp).val();
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Administrador/actuaPregTarifas')?>',
            data:{id:id, preg:pregunta, resp:respuesta},
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error') return alertify.warning(cont.mensaje);
                return alertify.success(cont.mensaje);
            }, error: function(data){
                alertify.error('Ha surgido un error, comuniquese con el equipo de desarrollo.');
            }
        });
    });
</script>
<?=$this->endSection()?>