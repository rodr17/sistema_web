<nav id="sidebar" class="sidebar-wrapper">
	<div class="sidebar-content">
		<div class="sidebar-brand">
			<a>Administrador</a>
			<div id="close-sidebar">
				<i class="fas fa-times"></i>
			</div>
		</div>
		<div class="sidebar-header">
			<div class="user-pic">
				<img class="img-responsive img-rounded" src="<?= view_cell('App\Libraries\FuncionesAdmin::imagenAdmin', ['id' => session('id_user')]) ?>" id="img_icon_administrador" alt="User picture">
			</div>
			<div class="user-info">
				<span class="user-name">
					<strong>
						<?= view_cell('App\Libraries\FuncionesAdmin::dataAdmin', ['id' => session('id_user'), 'campo' => 'name']) ?>
					</strong>
				</span>
				<span class="user-role">Administrador</span>
				<span class="user-status">
					<i class="fa fa-circle" ></i>
					<span>Online</span>
				</span>
			</div>
		</div>
		<!-- sidebar-header  -->
		<?php if(view_cell('App\Libraries\FuncionesAdmin::verBuscadorSide', ['id' => session('id_user')]) == true){ ?>
			<div class="sidebar-search">
				<div>
					<template id="listtemplate">
						<option data-link="/dashboard" value="Inicio">
						<option data-link="/metodos-de-ingres" value="Métodos de ingreso">
						<option data-link="/metodos-de-pago" value="Métodos de pago">
						<option data-link="/membresias" value="Membresias">
						<option data-link="/todos-los-trabajos" value="Todos los trabajos">
						<option data-link="/paginas" value="Páginas">
						<option data-link="/administrar-aviso-de-privacidad" value="Aviso de privacidad">
						<option data-link="/administrar-terminos-condiciones" value="Terminos y condiciones">
						<option data-link="/perfil-administrador" value="Mi perfil">
						<option data-link="/nuevo-administrador" value="Nuevo administrador">
						<option data-link="/lista-usuarios" value="Lista de usuarios">
						<option data-link="/habilidades" value="Habilidades">
						<option data-link="/administrar-noticias" value="Ver noticias">
						<option data-link="/nueva-noticia" value="Nueva noticia">
						<option data-link="/medios-contacto" value="Medios de contacto">
					</template>
					<!--<label for="buscador" class="form-label">Buscar...</label>-->
					<!--<input class="form-control" list="datalist" id="buscador" placeholder="Buscar...">-->
					<!--<datalist id="datalist">-->
					<!--</datalist>-->
				</div>
			</div>
		<?php } ?>
		<div class="sidebar-search">
			<div class="d-grid gap-2">
				<a class="btn btn-info btn-block" href="<?=base_url('')?>" target="_blank">
					<i class="fas fa-tv"></i>&nbsp;Ver sitio
				</a>
			</div>
		</div>
		<!-- sidebar-search  -->
		<div class="sidebar-menu">
			<ul>
				<li class="header-menu">
					<span>General</span>
				</li>
				<li>
					<a href="<?=base_url('/dashboard')?>">
						<i class="fas fa-house-user"></i>
						<span>Escritorio</span>
					</a>
				</li>
				<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'membresias']) == true){ ?>
					<li class="sidebar-dropdown">
						<a>
							<i class="fas fa-id-card"></i>
							<span>Membresías</span>
						</a>
						<div class="sidebar-submenu">
							<ul>
								<li>
									<a href="<?=base_url('/membresias')?>">
										Listado de membresias
									</a>
								</li>
								<!-- <li>
									<a>
										Agregar membresía
									</a>
								</li> -->
							</ul>
						</div>
					</li>
				<?php } ?>
				<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'pagos']) == true){ ?>
					<li class="sidebar-dropdown">
						<a>
							<i class="fas fa-credit-card"></i>
							<span>Pagos</span>
						</a>
						<div class="sidebar-submenu">
							<ul>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'lista_pagos']) == true) :?>
									<li>
										<a href="<?= base_url('/listado-de-pagos') ?>">
											Listado órdenes de pagos
										</a>
									</li>
								<?php endif ;?>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'estadisticas_generales']) == true) :?>
									<li>
										<a href="<?= base_url('/estadisticas-generales') ?>">
											Estadisticas generales
										</a>
									</li>
								<?php endif ;?>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'solicitudes_facturacion']) == true) :?>
									<li>
										<a href="<?= base_url('/solicitudes-de-facturacion') ?>">
											Solicitudes de facturación
										</a>
									</li>
								<?php endif ;?>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'crear_cupon']) == true) :?>
									<li>
										<a href="<?=base_url('/crear-cupon')?>">
											Crear cupon
										</a>
									</li>
								<?php endif ;?>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'lista_cupones']) == true) :?>
								<li>
									<a href="<?=base_url('/lista-de-cupones')?>">
										Lista de cupones
									</a>
								</li>
								<?php endif ;?>
							</ul>
						</div>
					</li>
				<?php } ?>
				<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'proyectos']) == true){ ?>
					<li class="sidebar-dropdown">
						<a>
							<i class="fas fa-laptop-code"></i>
							<span>Proyectos</span>
						</a>
						<div class="sidebar-submenu">
							<ul>
								<li>
									<a href="<?= base_url('/listado-proyectos') ?>">
										Listado de proyectos
									</a>
								</li>
								<!-- <li>
									<a href="<= base_url('/listado-proyectos') ?>?estatus=espera">
										Trabajos en espera
									</a>
								</li>
								<li>
									<a href="<= base_url('/listado-proyectos') ?>?estatus=desarrollo">
										Trabajos en desarrollo
									</a>
								</li>
								<li>
									<a href="<= base_url('/listado-proyectos') ?>?estatus=finalizado">
										Trabajos finalizados
									</a>
								</li>
								<li>
									<a href="<= base_url('/listado-proyectos') ?>?estatus=cancelado">
										Trabajos cancelados
									</a>
								</li> -->
							</ul>
						</div>
					</li>
				<?php } ?>
				<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'usuarios']) == true){ ?>
					<li class="sidebar-dropdown">
						<a>
							<i class="fas fa-users"></i>
							<span>Usuarios</span>
						</a>
						<div class="sidebar-submenu">
							<ul>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'mi_perfil']) == true) :?>
									<li>
										<a href="<?=base_url('perfil-administrador')?>">
											Mi perfil
										</a>
									</li>
								<?php endif ;?>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'listado_administradores']) == true) :?>
									<li>
										<a href="<?=base_url('listado-administradores')?>">
											Listado de administradores
										</a>
									</li>
								<?php endif ;?>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'listado_freelancers']) == true) :?>
									<li>
										<a href="<?=base_url('lista-freelancers')?>">
											Listado de freelancers
										</a>
									</li>
								<?php endif ;?>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'listado_contratantes']) == true) :?>
									<li>
										<a href="<?=base_url('lista-contratistas')?>">
											Listado de contratantes
										</a>
									</li>
								<?php endif ;?>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'agregar_usuario_admin']) == true) :?>
									<li>
										<a href="<?=base_url('/nuevo-administrador')?>">
											Agregar usuario admin
										</a>
									</li>
								<?php endif ;?>
							</ul>
						</div>
					</li>
				<?php } ?>
				<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'habilidades']) == true){ ?>
					<li class="sidebar-dropdown">
						<a>
							<i class="fas fa-list-ul"></i>
							<span>Habilidades</span>
						</a>
						<div class="sidebar-submenu">
							<ul>
								<li>
									<a href="<?=base_url('/habilidades')?>">
										Listado de habilidades
									</a>
								</li>
								<!-- <li>
									<a href="<=base_url('/agregar-habilidad')?>">
										Agregar habilidad
									</a>
								</li>
								<li>
									<a href="<=base_url('/solicitudes-de-habilidades')?>">
										Solicitudes de areas / habilidades
									</a>
								</li> -->
							</ul>
						</div>
					</li>
				<?php } ?>
				<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'noticias']) == true){ ?>
					<li class="sidebar-dropdown">
						<a>
							<i class="far fa-newspaper"></i>
							<span>Noticias</span>
						</a>
						<div class="sidebar-submenu">
							<ul>
								<li>
									<a href="<?=base_url('/administrar-noticias')?>">
										Listado de noticias
									</a>
								</li>
								<!-- <li>
									<a href="<=base_url('/nueva-noticia')?>">
										Agregar noticia
									</a>
								</li> -->
							</ul>
						</div>
					</li>
				<?php } ?>
				<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'idiomas']) == true){ ?>
					<li class="sidebar-dropdown">
						<a>
							<i class="fas fa-language"></i>
							<span>Idiomas</span>
						</a>
						<div class="sidebar-submenu">
							<ul>
								<li>
									<a href="<?=base_url('/idiomas')?>">
										Listado de idiomas
									</a>
								</li>
								<!-- <li>
									<a href="<=base_url('/agregar-idioma')?>">
										Agregar idioma
									</a>
								</li> -->
							</ul>
						</div>
					</li>
				<?php } ?>
				<li class="header-menu">
					<span>Extra</span>
				</li>
				<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'contenidos']) == true){ ?>
					<li class="sidebar-dropdown">
						<a>
							<i class="far fa-file"></i>
							<span>Contenidos</span>
						</a>
						<div class="sidebar-submenu">
							<ul>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'editar_portada']) == true) :?>
									<li>
										<a href="<?=base_url('/editar-seccion?id=MQ%3D%3D&page=inicio')?>">
											Editar portada
										</a>
									</li>
								<?php endif ;?>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'listado_paginas']) == true) :?>
								<li>
									<a href="<?=base_url('/paginas')?>">
										Listado de páginas
									</a>
								</li>
								<?php endif ;?>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'ajustes_generales']) == true) :?>
								<li>
									<a href="<?=base_url('/ajustes-generales')?>">
										Ajustes generales
									</a>
								</li>
								<?php endif ;?>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'aviso_privacidad']) == true) :?>
								<li>
									<a href="<?=base_url('/administrar-aviso-de-privacidad')?>">
										Aviso de privacidad
									</a>
								</li>
								<?php endif ;?>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'terminos_condiciones']) == true) :?>
								<li>
									<a href="<?=base_url('/administrar-terminos-condiciones')?>">
										Términos y condiciones
									</a>
								</li>
								<?php endif ;?>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'faq_tarifas']) == true) :?>
								<li>
									<a href="<?=base_url('/editar-testimonios')?>">
										Testimonios
									</a>
								</li>
								<?php endif ;?>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'faq_soporte']) == true) :?>
								<li>
									<a href="<?= base_url('/faq-tarifas') ?>">
										FAQ Tarífas
									</a>
								</li>
								<?php endif ;?>
								<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'editar_portada']) == true) :?>
								<li>
									<a href="<?= base_url('/faq-soporte') ?>">
										FAQ Soporte
									</a>
								</li>
								<?php endif ;?>
							</ul>
						</div>
					</li>
				<?php } ?>
				<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'medios_contacto']) == true){ ?>
					<li>
						<a href="<?=base_url('/medios-contacto')?>">
							<i class="far fa-address-card"></i>
							<span>Medios de contacto</span>
						</a>
					</li>
				<?php } ?>
				<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'llaves_api']) == true){ ?>
					<li>
						<a href="<?=base_url('/apis')?>">
							<i class="fas fa-key"></i>
							Llaves API
						</a>
					</li>
				<?php } ?>
				<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'contactos_soporte']) == true){ ?>
					<li>
						<a href="<?=base_url('/contactos-soporte')?>">
							<i class="fas fa-headset"></i>
							Contactos Soporte
						</a>
					</li>
				<?php } ?>
				<?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => session('id_user'), 'seccion' => 'contactos_boletin']) == true){ ?>
					<li>
						<a href="<?=base_url('/contactos-de-boletin')?>">
							<i class="fas fa-mail-bulk"></i>
							Contactos de boletín
						</a>
					</li>
				<?php } ?>
			</ul>
		</div>
		<!-- sidebar-menu  -->
	</div>
	<!-- sidebar-content  -->
	<div class="sidebar-footer">
		<a href="<?=base_url('/logout')?>">
			<i class="fa fa-power-off"></i>
		</a>
	</div>
</nav>