<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Lista de solicitudes de facturacion
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="p-2">
		<h4 class="titulo-label">Lista de solicitudes de facturacion</h4>
	</div>
	<hr>
	<div class="row">
		<div class="col-12 col-lg-6">
			<div id="listado-facturas">
				<?php if($solicitudes == null){ ?>
					<div class="text-center">Aún no hay solicitudes de facturación</div>
				<?php }else{ ?>
					<div class="text-center mb-3">
						<div id="daterange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
							<i class="fa fa-calendar"></i>&nbsp;
							<span></span> <i class="fa fa-caret-down"></i>
						</div>
					</div>
					<div class="mb-3 d-none">
						<input class="form-control m-1" id="inicio" placeholder="Fecha inicio">
						<input class="form-control m-1" id="fin" placeholder="Fecha fin">
						<a class="btn btn-primary btn-sm m-1" style="place-self: center;">
							<small>Filtrar</small>
						</a>
					</div>
					<label class="text-primary">Totales: (<strong id="cantidad_facturas"><?=count($solicitudes)?></strong>)</label>
					<div class="mt-2"id="contenedor_resumen_facturas" style="max-height: 500px; overflow-y: auto;">
						<?= view(administrador('viewcells/card_resumen_facturas'), array('facturas' => $solicitudes))?>
					</div>
				<?php } ?>
				<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
				<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
				<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
				<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
				<script>
					$('#daterange').daterangepicker({
						"startDate": "<?= date('m/d/o', strtotime($fecha_actual)) ?>",
						"minDate": "<?= date('m/d/o', strtotime($fecha_minima)) ?>",
						"maxDate": "<?= date('m/d/o', strtotime($fecha_actual)) ?>",
						"locale": {
							"format": "MM/DD/YYYY",
							"separator": " - ",
							"applyLabel": "Filtrar",
							"cancelLabel": "Cancelar",
							"fromLabel": "De",
							"toLabel": "a",
							"customRangeLabel": "Custom",
							"daysOfWeek": [
								"Do",
								"Lu",
								"Ma",
								"Mi",
								"Ju",
								"Vi",
								"Sa"
							],
							"monthNames": [
								"Enero",
								"Febrero",
								"Marzo",
								"Abril",
								"Mayo",
								"Junio",
								"Julio",
								"Agosto",
								"Septiembre",
								"Octubre",
								"Noviembre",
								"Deciembre"
							],
							"firstDay": 1
						}
					}, cb);
					var start = new Date(moment().subtract(29, 'days')).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
					var end = new Date(moment()).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
					function cb(start, end) {
						$('#daterange span').html(start + ' - ' + end);
					}
					cb(start, end);
					$('#daterange').on('apply.daterangepicker', function(ev, picker) {
						let inicio = picker.startDate.format('YYYY-MM-DD');
						let fin = picker.endDate.format('YYYY-MM-DD');
						facturas(inicio, fin);
					});
					function facturas(inicio, fin){
						$.ajax({
							type: 'POST',
							url: '<?= base_url('/Administrador/filtrarFacturas') ?>',
							data:{inicio:inicio, fin:fin},
							success:function(json){
								let respuesta = JSON.parse(json);
								
								$('#contenedor_resumen_facturas').html(respuesta.html);
								$('#cantidad_facturas').text(respuesta.cantidad_facturas);
							}, error: function(data){
								console.log('Error: '+data.responseText);
							}
						});
					}
				</script>
			</div>
		</div>
		<div class="col-12 col-lg-6">
			<div id="info-factura-soli"></div>
		</div>
	</div>
</div>
<?=$this->endSection()?>