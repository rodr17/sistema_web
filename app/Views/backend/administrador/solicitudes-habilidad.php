<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
    Solicitudes de areas / habilidad
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
    <div class="p-2">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-3">
                <h4 class="titulo-label">
                    Solicitudes de areas / habilidad
                </h4>
            </div>
            <div class="col-12 col-md-6 col-lg-9">
                <a class="btn btn-primary" href="<?=base_url('/habilidades')?>">
                    Listado de habilidades
                </a>
                <a class="btn btn-primary" href="<?=base_url('/agregar-habilidad')?>">
                    Agregar nueva area / habilidad
                </a>
            </div>
        </div>
    </div>
    <hr>
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10">
            <div id="solicitudes">
                
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#solicitudes').load('<?=base_url('/Administrador/solicitudAreaHabilidad')?>');
    });
</script>
<?=$this->endSection()?>