<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Todas las noticias
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="row">
		<div class="d-flex align-items-center gap-2 mb-2">
			<h1 class="titulo-label d-inline m-0">Todas las noticias</h1>
			<a class="btn btn-sm btn-primary" href="<?=base_url('/nueva-noticia')?>">Agregar noticia</a>
		</div>
		<form method="GET" action="<?=base_url('/administrar-noticias')?>">
			<div class="form-floating mb-3">
				<input type="text" class="form-control" id="buscar" name="search">
				<label for="buscar">Buscar noticia por</label>
			</div>
		</form>
		<?php if($_GET){ ?>
			<div class="col-12 mb-2">
				<h5>Resultados de busqueda para: <strong><?= (isset($_GET['search']))? $_GET['search']: 'N/A' ?></strong></h5>
			</div>
		<?php } ?>
	</div>
	<hr>
	<div class="p-3">
		<div class="card border-0">
			<div class="card-body">
				<div class="div-limitadoY">
					<table class="table">
						<thead>
							<tr>
								<th scope="col">ID</th>
								<th scope="col">Noticia</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($noticias as $n){ ?>
								<tr id="notice<?=$n['id']?>row">
									<td>
										<?=$n['id']?>
									</td>
									<td>
										<?=$n['titulo']?><br>
										<small>Fecha de publicación: <?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $n['created_at']])?></small>
										<br>
										<small>Ultima actualización: <?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $n['updated_at']])?></small>
										<br>
										<a class="text-decoration-none" href="<?=base_url('/editar-noticia?cont='.base64_encode($n['id']).'&notice='.$n['titulo'])?>"><small class="text-primary">Editar</small></a>
										&nbsp;|&nbsp;
										<a class="cursor text-decoration-none eliminar-noticia" data-row="notice<?=$n['id']?>row" data-id="<?=$n['id']?>">
											<small class="text-danger">Eliminar</small>
										</a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('.eliminar-noticia').on('click', function(){
	let id = $(this).attr('data-id');
	let row = $(this).attr('data-row');
	alertify.confirm('ATENCIÓN', 'Esta a punto de eliminar la siguiente noticia, ¿Esta seguro de esto?', function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Administrador/eliminarNoticia')?>',
			data:{id:id},
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error'){
					alertify.warning(cont.mensaje, 10);
				}else{
					$('#'+row).fadeOut().remove();
					alertify.success(cont.mensaje, 10);
				}
			}, error: function(data){
				alertify.error('Comuniquese con el equipo de desarrollo.', 10);
			}
		});
	}, function(){});
});
</script>
<?=$this->endSection()?>