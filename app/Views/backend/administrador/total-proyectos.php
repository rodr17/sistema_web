<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
    Proyectos en la plataforma
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
    <div class="p-2">
        <h4 class="titulo-label">
            Proyectos en la plataforma
        </h4>
    </div>
    <hr>
    <div class="row">
        <div class="col-12 col-lg-3">
            <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <button class="nav-link filt-trabajo active" id="trabajos-todos" data-tipo="todos" type="button" data-bs-toggle="pill" data-bs-target="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    Todos
                </button>
                <button class="nav-link filt-trabajo" id="trabajos-espera" data-tipo="espera" type="button" data-bs-toggle="pill" data-bs-target="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    En espera
                </button>
                <button class="nav-link filt-trabajo" id="trabajos-desarrollo" data-tipo="desarrollo" type="button" data-bs-toggle="pill" data-bs-target="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    En desarrollo
                </button>
                <button class="nav-link filt-trabajo" id="trabajos-finalizados" data-tipo="finalizado" type="button" data-bs-toggle="pill" data-bs-target="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    Finalizados
                </button>
                <button class="nav-link filt-trabajo" id="trabajos-cancelados" data-tipo="cancelado" type="button" data-bs-toggle="pill" data-bs-target="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                    Cancelados
                </button>
            </div>
        </div>
        <div class="col-12 col-lg-9">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="zona-proyectos" role="tabpanel" aria-labelledby="v-pills-home-tab">
                    <?php foreach($proyectos as $p){ ?>
                        <?php 
                            $model = model('Autenticacion');
                            $contratista = $model->get_id($p['id_contratista']);
                        ?>
                        <?=view_cell('App\Libraries\FuncionesAdmin::cardTrabajoAdministracion', ['proyecto' => $p, 'contratista' => $contratista])?>
                    <?php } ?>
                </div>
            </div>
            <div class="text-center" id="icono-loading">
                <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                <lottie-player src="<?=base_url('assets/animaciones/loading.json')?>"  background="transparent"  speed="1"  style="width: 250px; height: 250px; margin: auto;"  loop autoplay></lottie-player>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#icono-loading').hide();
    });
    $('.filt-trabajo').on('click', function(){
        let tipo = $(this).attr('data-tipo');
        $('#zona-proyectos').fadeOut();
        $('#icono-loading').fadeIn();
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/filtrar-proyectos')?>',
            data:{tipo:tipo},
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error'){
                    alertify.error(cont.mensaje, 10);
                }else{
                    $('#zona-proyectos').html(cont.mensaje, 10);
                    setTimeout(function(){
                        $('#icono-loading').fadeOut();
                        $('#zona-proyectos').fadeIn();
                    }, 2000);
                }
            }, error: function(data){
                alertify.error('Surgio un error comoniquese con el equipo de desarrollo.', 10);
            }
        });
    });
</script>
<?=$this->endSection()?>