<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	Listado de proyectos
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="p-2">
		<h4 class="titulo-label">Listado de proyectos</h4>
	</div>
	<div class="row justify-content-between align-items-center">
		<div class="col-md-6">
			<div class="form-floating d-flex gap-2">
				<select class="form-control" id="estatus" name="estatus">
					<option value="todos" selected>Todos</option>
					<option value="espera">Espera</option>
					<option value="desarrollo">Desarrollo</option>
					<option value="finalizado">Finalizados</option>
					<option value="cancelado">Cancelados</option>
				</select>
				<label for="estatus">Buscar freelancer</label>
			</div>
		</div>
		<div class="col-md-6">
			<div class="d-flex justify-content-center align-items-center gap-1 bg-white w-100" id="daterange" style="cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
				<i class="fa fa-calendar"></i>&nbsp;
				<span></span> <i class="fa fa-caret-down"></i>
			</div>
		</div>
	</div>
	<hr>
	<div class="row justify-content-center">
		<div class="col-12" id="contenedor_proyectos">
			<?php if($proyectos != null){ ?>
				<?php foreach($proyectos as $p){ ?>
					<?php $contratista = model('Autenticacion')->get_id($p['id_contratista']); ?>
					<?=view_cell('App\Libraries\FuncionesAdmin::cardTrabajoAdministracion', ['proyecto' => $p, 'contratista' => $contratista])?>
				<?php } ?>
			<?php }else{ ?>
				<div class="text-center">
					<span class="fw-bold">No se encontraron proyectos</span>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="modal fade" id="confirmar_accion_proyecto" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header border-0">
					<h5 class="modal-title" id="staticBackdropLabel">¿Estás seguro de esto?</h5>
					<button type="button" class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></button>
				</div>
				<div class="modal-body border-0" id="cuerpo_confirmacion_accion"></div>
				<div class="modal-footer border-0 toggle_Susp_Reac">
					<a class="btn btn-sm delegar" type="button" id="boton_confirmacion_accion"></a>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
	$(function() {
		$('#daterange').daterangepicker({
			"showDropdowns": true,
			"startDate": "<?= date('m-d-o', strtotime($hoy)) ?>",
			"maxDate": "<?= date('m-d-o', strtotime($hoy)) ?>",
			"locale": {
				"format": "MM/DD/YYYY",
				"separator": " - ",
				"applyLabel": "Filtrar",
				"cancelLabel": "Cancelar",
				"fromLabel": "De",
				"toLabel": "a",
				"customRangeLabel": "Custom",
				"daysOfWeek": [
					"Do",
					"Lu",
					"Ma",
					"Mi",
					"Ju",
					"Vi",
					"Sa"
				],
				"monthNames": [
					"Enero",
					"Febrero",
					"Marzo",
					"Abril",
					"Mayo",
					"Junio",
					"Julio",
					"Agosto",
					"Septiembre",
					"Octubre",
					"Noviembre",
					"Deciembre"
				],
				"firstDay": 1
			}
		});
		var start = new Date(moment().subtract(29, 'days')).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
		var end = new Date(moment()).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
		function cb(start, end) {
			$('#daterange span').html(start + ' - ' + end);
		}
		cb(start, end);
	});
	function filtrarProyectos(inicio = "", fin = ""){
		$.ajax({
			type: 'POST',
			url: '<?= base_url('/Administrador/filtrarProyectos') ?>',
			data:{inicio:inicio, fin:fin, estatus:$("#estatus option:selected").val()},
			success:function(data){
				$('#contenedor_proyectos').html(data);
			}, error: function(data){
				console.log('Error: ' + data);
			}
		});
	}
	$('#daterange').on('apply.daterangepicker', function(ev, picker) {
		let inicio = picker.startDate.format('YYYY-MM-DD');
		let fin = picker.endDate.format('YYYY-MM-DD');
		console.log('Inicio: ' + inicio + ' 00:00:00');
		console.log('Fin: ' + fin + ' 23:59:59');
		let inicio_formateado = new Date(moment(inicio)).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
		let fin_formateado = new Date(moment(fin)).toLocaleDateString("es", {month: 'long', day: "numeric", year:"numeric"});
		$('#daterange span').html(inicio_formateado + ' - ' + fin_formateado);
		filtrarProyectos(inicio, fin);
	});
	$('#estatus').change(function (e) { 
		e.preventDefault();
		filtrarProyectos();
	});
	$('.delegar').click(function(e){
		$this = $(this);
		$.ajax({
			url : '<?=base_url('Administrador/cambiarEstatusProyecto')?>',
			method : 'POST',
			data : {proyecto : $this.attr('dato'), accion: $this.attr('estatus'), mensaje : $('#mensaje').length == 0 ? 'NaN' : $('#mensaje').val()},
			success: function(json){
				let respuesta = JSON.parse(json);
				if(respuesta.tipo == 'warning') return alertify.warning(respuesta.mensaje);
				if(respuesta.tipo == 'error') return alertify.error(respuesta.mensaje);
				
				let nuevo_estatus = $this.attr('estatus') == 'suspencion' ? 'reactivacion' : 'suspencion';
				$('#confirmar_accion_proyecto').modal('hide');
				
				if($this.attr('estatus') == 'suspencion'){
					$this.removeClass('btn-danger').addClass('btn-success').html('Reactivar <i class="fas fa-toggle-on"></i>').attr('estatus', nuevo_estatus);
					$('.proyecto_unico[dato="'+$this.attr('dato')+'"]').removeClass('btn-danger').addClass('btn-success').html('Reactivar <i class="fas fa-toggle-on"></i>').attr('estatus', nuevo_estatus);
				}
				else{
					$this.removeClass('btn-success').addClass('btn-danger').html('Suspender <i class="fas fa-toggle-off"></i>').attr('estatus', nuevo_estatus);
					$('.proyecto_unico[dato="'+$this.attr('dato')+'"]').removeClass('btn-success').addClass('btn-danger').html('Suspender <i class="fas fa-toggle-off"></i>').attr('estatus', nuevo_estatus);
				}
				alertify.success(respuesta.mensaje);
			},
			error: function(data){
				alertify.error(data.responseText);
				console.log(data);
			}
		});
	});
	$('.proyecto_unico').click(function(){
		$this = $(this);
		if($this.attr('estatus') == 'suspencion'){
			$('#confirmar_accion_proyecto #cuerpo_confirmacion_accion').html(`<div class='form-floating'>
							<textarea class='form-control' placeholder='Ingresa tu motivo' id='mensaje' name='mensaje'></textarea>
							<label for='mensaje'>Ingresa tu motivo</label>
						</div>`);
			return $('#confirmar_accion_proyecto #boton_confirmacion_accion').removeClass('btn-success').addClass('btn-danger').html('Suspender <i class="fas fa-toggle-off"></i>').attr({estatus : 'suspencion', dato : $this.attr('dato')});
		} 
		$('#confirmar_accion_proyecto #cuerpo_confirmacion_accion').html(`<p class="mb-0 ">Al realizar está operación se les notificará los usuarios que procederán con la continuación del proyecto</p>`);
		$('#confirmar_accion_proyecto #boton_confirmacion_accion').removeClass('btn-danger').addClass('btn-success').html('Reactivar <i class="fas fa-toggle-on"></i>').attr({estatus : 'reactivacion', dato : $this.attr('dato')});
	});
</script>
<?=$this->endSection()?>