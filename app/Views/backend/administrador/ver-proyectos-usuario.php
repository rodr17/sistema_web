<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
    Proyectos creados por contratista
<?=$this->endSection()?>

<?=$this->section('content')?>
<?php $model = model('Autenticacion'); ?>
<div class="container">
    <div class="p-2">
        <h4 class="titulo-label">
            Proyectos en creados por <span class="fw-bold"><?=$contratista['nombre']?> <?=$contratista['apellidos']?></span>
        </h4>
    </div>
    <hr>
    <div class="row">
        <div class="col-12 col-lg-3">
            <div class="p-3">
                <div class="card border-0 mb-3">
                    <div class="card-body">
                        <h2 class="card-title h5">Contratista</h2>
                        <p class="card-text"><span class="fw-bold">Nombre: </span><?=$contratista['nombre']?> <?=$contratista['apellidos']?></p>
                        <div class="text-center">
                            <img src="<?= imagenPerfil($contratista) ?>" class="w-75" alt="...">
                        </div>
                        <div class="my-3">
                            Correo: <a class="text-decoration-none" href="mailto:<?=$contratista['correo']?>"><?=$contratista['correo']?></a><br>
                            Teléfono: <a class="text-decoration-none" href="tel:<?=!empty($contratista['contacto']['telefono']) ? $contratista['contacto']['telefono'] : ''?>"><?=!empty($contratista['contacto']['telefono']) ? $contratista['contacto']['telefono'] : ''?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <button class="nav-link active" id="v-pills-espera-tab" data-bs-toggle="pill" data-bs-target="#v-pills-espera" type="button" role="tab" aria-controls="v-pills-espera" aria-selected="true">
                    En espera
                </button>
                <button class="nav-link" id="v-pills-desarrollo-tab" data-bs-toggle="pill" data-bs-target="#v-pills-desarrollo" type="button" role="tab" aria-controls="v-pills-desarrollo" aria-selected="false">
                    En espera
                </button>
                <button class="nav-link" id="v-pills-finalizado-tab" data-bs-toggle="pill" data-bs-target="#v-pills-finalizado" type="button" role="tab" aria-controls="v-pills-finalizado" aria-selected="false">
                    Finalizados
                </button>
                <button class="nav-link" id="v-pills-cancelado-tab" data-bs-toggle="pill" data-bs-target="#v-pills-cancelado" type="button" role="tab" aria-controls="v-pills-cancelado" aria-selected="false">
                    Cancelados
                </button>
            </div>
        </div>
        <div class="col-12 col-lg-9">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-espera" role="tabpanel" aria-labelledby="v-pills-espera-tab">
                    <?php foreach($proyectos as $p){ ?>
                        <?php if($p['estatus'] == 'espera'){ ?>
                            <?php
                                $contratista = $model->get_id($p['id_contratista']);
                            ?>
                            <?=view_cell('App\Libraries\FuncionesAdmin::cardTrabajoAdministracion', ['proyecto' => $p, 'contratista' => $contratista])?>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="tab-pane fade" id="v-pills-desarrollo" role="tabpanel" aria-labelledby="v-pills-desarrollo-tab">
                    <?php foreach($proyectos as $p){ ?>
                        <?php if($p['estatus'] == 'desarrollo'){ ?>
                            <?php
                                $contratista = $model->get_id($p['id_contratista']);
                            ?>
                            <?=view_cell('App\Libraries\FuncionesAdmin::cardTrabajoAdministracion', ['proyecto' => $p, 'contratista' => $contratista])?>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="tab-pane fade" id="v-pills-finalizado" role="tabpanel" aria-labelledby="v-pills-finalizado-tab">
                    <?php foreach($proyectos as $p){ ?>
                        <?php if($p['estatus'] == 'finalizado'){ ?>
                            <?php
                                $contratista = $model->get_id($p['id_contratista']);
                            ?>
                            <?=view_cell('App\Libraries\FuncionesAdmin::cardTrabajoAdministracion', ['proyecto' => $p, 'contratista' => $contratista])?>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="tab-pane fade" id="v-pills-cancelado" role="tabpanel" aria-labelledby="v-pills-cancelado-tab">
                    <?php foreach($proyectos as $p){ ?>
                        <?php if($p['estatus'] == 'cancelado'){ ?>
                            <?php
                                $contratista = $model->get_id($p['id_contratista']);
                            ?>
                            <?=view_cell('App\Libraries\FuncionesAdmin::cardTrabajoAdministracion', ['proyecto' => $p, 'contratista' => $contratista])?>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div class="text-center" id="icono-loading">
                <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                <lottie-player src="<?=base_url('assets/animaciones/loading.json')?>"  background="transparent"  speed="1"  style="width: 250px; height: 250px; margin: auto;"  loop autoplay></lottie-player>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#icono-loading').hide();
    });
</script>
<?=$this->endSection()?>