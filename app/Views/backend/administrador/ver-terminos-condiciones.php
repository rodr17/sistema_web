<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
    Editar términos y condiciones
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
    <form id="edit-sec">
        <div class="col-12">
            <h3>Contenido</h3>
        </div>
        <div class="mb-4">
            <textarea id="postBody" name="postBody"><?=isset($texto_terminos['info']) ? $texto_terminos['info'] : ''?></textarea>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <a class="btn btn-primary" id="actua-terminos">Guardar cambios</a>
            </div>
        </div>
    </form>
</div>
<script>
    var editor = CKEDITOR.replace('postBody');
    CKEDITOR.config.htmlEncodeOutput;
    CKEDITOR.config.entities = false;
</script>
<?=$this->endSection()?>