<?php if($freelances != null){ ?>
	<?php foreach($freelances as $free){ ?>
		<div class="border p-2 mb-3 bg-light bg-gradient">
			<div class="row">
				<div class="col-12 col-sm-12 col-lg-9">
					<div class="row">
						<div class="col-12 col-sm-12 col-lg-4">
							<img src="<?= imagenPerfil($free) ?>" class="img-thumbnail">
						</div>
						<div class="col-12 col-sm-12 col-lg-8 p-3">
							<span class="name-user"><?=$free['nombre']?> <?=$free['apellidos']?></span><br>
							<label>
								<?php if($free['cuenta'] == 'facebook'){?>
									<span class="fst-italic">Registrado con cuenta Facebook</span>
								<?php }elseif($free['cuenta'] == 'google'){ ?>
									<span class="fst-italic">Registrado con cuenta de Google</span>
								<?php }else{ ?>
									<span class="fst-italic">Registrado con correo electrónico.</span>
								<?php } ?>
							</label>
							<label>
								<?php if($free['aprobado'] == NULL && $free['created_at'] == $free['updated_at']){?>
									<span class="fst-italic">
										En espera de revisión, usuario nuevo.
									</span>
								<?php }elseif($free['aprobado'] == NULL && $free['created_at'] != $free['updated_at']){ ?>
									<span class="fst-italic">
										En espera de revisión.
									</span>
								<?php }else{ ?>
									<span class="fst-italic">
										Perfil declinado, el usuario modificara sus datos.
									</span>
								<?php } ?>
							</label>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-lg-3">
					<div class="d-grid gap-2">
						<a class="btn btn-primary btn-sm" target="_blank" href="<?=base_url('/usuarios/prevista/'.base64_encode($free['id']))?>">Ver información</a>
						<?php if($free['aprobado'] == null || $free['aprobado'] == false){?>
							<button class="btn btn-info validar-info btn-sm" data-free="<?=$free['id']?>">
								Validar cuenta
							</button>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
<?php }else{ ?>
	<div class="text-center">
		Sin Solicitudes de aprobación
	</div>
<?php } ?>