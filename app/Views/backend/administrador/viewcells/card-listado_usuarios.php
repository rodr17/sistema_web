<div class="col-12 col-lg-6" id="card-<?=$usuario['id']?>-free">
	<div class="border mb-3 bg-light bg-gradient">
		<div class="row">
			<div class="col-12 col-sm-12 col-lg-9">
				<div class="row align-items-center">
					<div class="col-12 col-sm-12 col-lg-4 text-center">
						<img src="<?= imagenPerfil($usuario) ?>" class="image-user-card">
					</div>
					<div class="col-12 col-sm-12 col-lg-8 p-3 pl-0">
						<p class="name-user text-truncate"><?=$usuario['nombre']?> <?=$usuario['apellidos']?></p>
						<p class="text-truncate m-0">
							Correo : <?=$usuario['correo']?>
						</p>
						<label>
							Fecha de registro: <?= strftime('%d/%B/%Y', strtotime(explode(' ', $usuario['created_at'])[0])) ?>
						</label>
						<label>
							<?php if($usuario['cuenta'] == 'facebook'){?>
								<span class="fst-italic">Registrado con cuenta Facebook</span>
							<?php }elseif($usuario['cuenta'] == 'google'){ ?>
								<span class="fst-italic">Registrado con cuenta de Google</span>
							<?php }else{ ?>
								<span class="fst-italic">Registrado con correo electrónico.</span>
							<?php } ?>
						</label>
						<div class="d-flex">
							<label>Calificación: &nbsp;&nbsp;</label>
							<?=view_cell('App\Libraries\FuncionesSistema::verCalificación', ['id' => $usuario['id']])?>
						</div>
						<label>Suscripción: <span class="fw-bold text-<?=$suscripcion_activa ? 'success' : 'warning'?>"><?=$suscripcion_activa ? "Activo (".$suscripcion['tipo_suscripcion'].")" : 'Inactivo'?></span></label>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-lg-3 p-2 mr-10-negative">
				<div class="d-grid gap-2">
					<!-- <php if($usuario['aprobado'] == null || $usuario['aprobado'] == false){?>
						<button class="btn btn-info validar-info btn-sm" data-free="<=$usuario['id']?>">
							<small>Validar cuenta</small>
						</button>
					<php } ?> -->
					<?php if($usuario['rol'] == 'contratante') :?>
						<button class="btn btn-primary btn-sm ver-info" data-free="<?=$usuario['id']?>">
							<small>Información</small>
						</button>
						<a class="btn btn-info btn-sm" href="<?=base_url('/ver-proyectos-contratista?id='.base64_encode($usuario['id']))?>">
							<small>Proyectos</small>
						</a>
					<?php else :?>
						<a class="btn btn-primary btn-sm" target="_blank" href="<?=base_url('/usuarios/prevista/'.base64_encode($usuario['id']))?>">
							<small>Información</small>
						</a>
					<?php endif ;?>
					<a class="btn btn-secondary btn-sm" href="<?=base_url('/editar-usuario?id='.base64_encode($usuario['id']))?>">
						Editar
					</a>
					<a class="btn btn-danger btn-sm eliminar-user" data-id="<?=$usuario['id']?>" data-name="<?=$usuario['nombre']?> <?= $usuario['apellidos'] ?>">
						Suspender
					</a>
				</div>
			</div>
		</div>
	</div>
</div>