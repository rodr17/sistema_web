<?php $ventas_totales = 0; ?>
<?php foreach($pagos as $key => $p) :?>
	<?php $cupon_usado =  model('UsosCupones')->where('usos_cupones.id_compra', $p['id_compra'])->join('cupones', 'usos_cupones.id_cupon = cupones.id')->first();
		$ventas_totales += $p['estatus'] == 'Activo' ? $p['monto'] : 0;
	?>
	<div class="card mb-2 mx-2">
		<div class="card-body">
			<div class="row">
				<div class="col-12 col-lg-3">
					<img src="<?= imagenPerfil($p) ?>" class="img-thumbnail">
				</div>
				<div class="col-12 col-lg-9">
					<label><strong><?=$p['nombre_completo']?></strong></label><br>
					<label><strong>Monto: $<?= number_format($p['monto'], 2, '.', ',') ?> MXN</strong></label><br>
					<label><strong>Método de pago: <?= $p['tipo']?></strong></label><br>
					<label><strong>Estatus: <?= $p['estatus'] == 'Activo' ? '<span class="text-success">Pagado</span>' : '<span class="text-warning">Pendiente</span>' ?></strong></label><br>
					<?php if(!empty($cupon_usado)) :?>
						<label><strong>Cupón: <span class="text-info"><?=$cupon_usado['codigo']?></span></strong></label><br>
					<?php endif ;?>
					<a class="btn btn-primary btn-sm ver-mas-pago" data-id="<?=$p['pagosID']?>">
						<small>Ver más</small>
					</a>
				</div>
			</div>
		</div>
	</div>
<?php endforeach ;?>
<script>
	$('#texto_totales').text('<?=$pagos_totales?>');
	$('#v-totales').html('<?='$'.number_format($ventas_totales, 2, '.', ',').'MXN'?>');
	
	$('.ver-mas-pago').click(function(){
		let id = $(this).attr('data-id');
		$.ajax({
			type: 'POST',
			url: '<?= base_url('/Administrador/verInfoPagoUsuario') ?>',
			data:{id:id},
			success: function(data){
				$('#sec-info-pago').html(data).fadeIn();
			}, error: function(data){
				alertify.error('Inténtelo de nuevo, ha surgido un error en la consulta a la base de datos.');
			}
		});
	});
</script>