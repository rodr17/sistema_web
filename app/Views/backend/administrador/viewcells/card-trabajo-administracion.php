<?php if($contratista != null){ ?>
    <div class="border p-2 mb-3 bg-light bg-gradient">
        <div class="row">
            <div class="col-12 col-lg-6 text-start">
                <span class="fw-bold">
                    <?=$p['titulo']?>
                </span><br>
                <span class="fst-italic">Categoria: <?=$p['categoria']?></span><br>
                <span class="fst-italic">Subcategoria: <?=$p['subcategoria']?></span><br>
                <span class="fst-italic">ID de proyecto: <?=$p['id']?></span>
            </div>
            <div class="col-12 col-lg-6 text-end">
                <span class="fw-bold"><i class="far fa-calendar-alt"></i> Fecha de creacion: <?= view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $p['created_at']])?></span><br>
                <span class="fw-bold"><i class="far fa-calendar-alt"></i> Fecha de arranque: <?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $p['fecha_arranque_esperado']])?></span><br>
                <span class="fw-bold"><i class="far fa-calendar-alt"></i> Fecha de entrega: <?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $p['fecha_entrega']])?></span>
            </div>
            <div class="d-flex flex-wrap flex-lg-nowrap justify-content-center justify-content-lg-between align-items-center gap-3 gap-lg-2 mt-3">
                <div class="">
                    <img src="<?=imagenPerfil($contratista)?>" class="img-thumbnail-trabajos" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Contratista: <?=$contratista['nombre'].' '.$contratista['apellidos']?>">
                </div>
                <div class="">
                    <label class="mb-2"><?=word_limiter($p['descripcion'], 15, '...')?></label>
                    <?php if($p['estatus'] == 'desarrollo' || $p['estatus'] == 'finalizado'){ ?>
                        <?=view_cell('App\Libraries\FuncionesSistema::barraProgreso', ['id' => $p['id']])?>
                    <?php } ?>
                </div>
                <div class="toggle_Susp_Reac">
                    <div class="d-flex flex-nowrap gap-1">
                        <a class="btn btn-dark btn-sm" href="<?=base_url('/visualizar-proyecto?id='.$p['id'])?>">Ver <i class="fas fa-briefcase"></i></a>
                        <form action="<?=base_url('Administrador/edicion_proyecto')?>" method="POST" enctype="multipart/form-data">
                            <input class="invisible" type="hidden" name="proyecto" value="<?=$p['id']?>">
                            <button class="btn btn-primary btn-sm" type="submit">Editar <i class="fas fa-edit"></i></button>
                        </form>
                        <?php if($p['estatus'] != 'finalizado' && $p['estatus'] != 'cancelado') :?>
                            <button class="btn <?=$p['deleted'] ? 'btn-success' : 'btn-danger'?> btn-sm proyecto_unico" dato="<?=$p['id']?>" data-bs-toggle="modal" estatus="<?=$p['deleted'] ? 'reactivacion' : 'suspencion'?>" data-bs-target="#confirmar_accion_proyecto"><?=$p['deleted'] ? 'Reactivar <i class="fas fa-toggle-on"></i>' : 'Suspender <i class="fas fa-toggle-off"></i>'?></button>
                        <?php endif ;?>
                    </div>
                    <?php if($p['estatus'] == 'espera'){ ?>
                        <span class="d-block text-center fw-bold text-secondary mt-2">Proyecto en espera</span>
                    <?php }elseif($p['estatus'] == 'desarrollo'){ ?>
                        <span class="d-block text-center fw-bold text-info mt-2">Proyecto en desarrollo</span>
                    <?php }elseif($p['estatus'] == 'finalizado'){ ?>
                        <span class="d-block text-center fw-bold text-success mt-2">Proyecto en finalizado</span>
                    <?php }else{ ?>
                        <span class="d-block text-center fw-bold text-danger mt-2">Proyecto en cancelado</span>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>