<?php foreach($facturas as $factura) :?>
    <div class="card mb-2">
        <div class="card-body">
            <label>Folio de factura: <span class="fw-bold">FA-<?= strftime('%d%m%y', strtotime(explode(' ', $factura['created_at'])[0])) ?>-<?=$factura['id']?></span></label><br>
            <label>Folio de compra: <span class="fw-bold"><?=view_cell('App\Libraries\FuncionesSistema::folioCompra', ['id' => $factura['id_compra']])?></span></label><br>
            <label>Fecha: <?= view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $factura['created_at']]) ?></label><br>
            <label>Estatus: <span class="mb-0 fw-bold text-<?= ($factura['estatus'] != 'enviado')? 'danger' : 'success' ; ?>"><?= ucfirst($factura['estatus']) ?></span></label><br>
            <a class="btn btn-primary btn-sm ver-info-factura" data-id="<?=$factura['id']?>" dato-folio="FACT-<?= strtotime($factura['created_at']) ?>-<?=$factura['id']?>">
                <small>Ver información</small>
            </a>
        </div>
    </div>
<?php endforeach ;?>
<script>
    $('.ver-info-factura').click(function(){
        let id = $(this).attr('data-id');
        let folio = $(this).attr('dato-folio');
        $.ajax({
            type: 'POST',
            url: '<?= base_url('/Administrador/verDatosFactura') ?>',
            data:{id:id, folio:folio},
            success: function(data){
                $('#info-factura-soli').html(data);
            }, error: function(data){
                console.log('Error: '+data);
            }
        });
    });
</script>