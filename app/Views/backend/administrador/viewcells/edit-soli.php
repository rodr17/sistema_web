<form id="edit-soli">
    <?php $contenido = json_decode($solicitud['contenido'], true);?>
    <div class="form-floating mb-3">
        <input type="text" class="form-control" name="nombre" value="<?=$contenido['nombre']?>" id="sug-area-input" placeholder="name@example.com">
        <label for="sug-area-input">Area o habilidad</label>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="tipo" value="area" id="tipo-area" <?=($contenido['tipo'] == 'area')? 'checked' : '' ?>>
        <label class="form-check-label" for="tipo-area">
            Area
        </label>
    </div>
    <div class="form-check mb-3">
        <input class="form-check-input" type="radio" name="tipo" value="habilidad" id="tipo-habilidad" <?=($contenido['tipo'] == 'habilidad')? 'checked' : '' ?>>
        <label class="form-check-label" for="tipo-habilidad">
            Habilidad
        </label>
    </div>
    <input type="hidden" name="id" value="<?=$solicitud['id']?>">
    <div class="mb-3" id="select-areas-div" <?=($contenido['tipo'] == 'area')? 'style="display: none;"' : '' ?>>
        <div class="form-floating">
            <select class="form-select" id="select-areas-sug" name="area" aria-label="Area">
                <?php foreach ($habilidades as $key => $a) : ?>
                    <option value="<?=$a['area']?>" <?=($contenido['area'] == $a['area'])? 'selected' : '' ?>><?=$a['area']?></option>
                <?php endforeach; ?>
            </select>
            <label for="select-areas-sug">Area</label>
        </div>
    </div>
    <div class="text-center">
        <a class="btn btn-primary" id="btn-addsugarea">Añadir area/habilidad</a>
    </div>
</form>
<script>
    $('#btn-addsugarea').click(function(){
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Administrador/saveAreaSugerencia')?>',
            data: $('#edit-soli').serialize(),
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
                $('#modal-confirm-soli .modal-body').html('');
                $('#modal-confirm-soli').modal('hide');
                $('#solicitudes').load('<?=base_url('/Administrador/solicitudAreaHabilidad')?>');
                cantSolicitudesAreas();
                return alertify.success(cont.mensaje, 10);
            }, error: function(data){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
            }
        });
    });
    function cantSolicitudesAreas(){
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Administrador/cantSolicitudesAreas')?>',
            data:{},
            success: function(data){
                $('#soli-habilidades').html(data);
            }, error: function(){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
            }
        });
    }
    $(document).ready(function(){
        <?php if($contenido['tipo'] == 'area'){ ?>
            $('#select-areas-div').hide();
        <?php } ?>
        $('input[name="tipo"]').change(function(){
            if($(this).attr('id') == 'tipo-habilidad'){
                if($(this).is(':checked') == true){
                    $('#select-areas-div').show();
                }else{
                    $('#select-areas-div').hide();
                }
            }else{
                $('#select-areas-div').hide();
            }
        });
    });
</script>