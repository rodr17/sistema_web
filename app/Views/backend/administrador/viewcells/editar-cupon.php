<form id="form-editar-cupon">
	<input type="hidden" value="<?=$cupon['id']?>" name="id" id="id">
	<div class="row">
		<div class="col-12 col-lg-6">
			<div class="form-label">Ingrese el codigo para el nuevo cupón, utilizar letras y números solamente.</div>
			<div class="form-floating mb-3">
				<input type="text" class="form-control" name="codigo" id="codigo" onkeyup="mayus(this)" value="<?=$cupon['codigo']?>" placeholder="...">
				<label for="codigo">Codigo de cupón</label>
			</div>
			<div class="form-label">Cantidad de usos del cupon. (Cuantas veces se podrá usar en general el cupón)</div>
			<div class="form-check form-switch">
				<input class="form-check-input" type="checkbox" role="switch" value="true" name="switch-limite-total" id="switch-limite-total" <?= empty($cupon['limite'])? 'checked' : '' ?>>
				<label class="form-check-label" for="switch-limite-total">Ilimitados</label>
			</div>
			<div id="input-limite" <?= empty($cupon['limite'])? 'style="display: none;"' : 'style="display: block;"' ?>>
				<div class="form-floating mb-3">
					<input type="text" class="form-control" name="limite" id="limite" value="<?=$cupon['limite']?>" onkeypress="return event.charCode>=48 && event.charCode<=57" placeholder="...">
					<label for="limite">Limite de usos</label>
				</div>
			</div>
			<div id="input-porcentaje" <?= $cupon['tipo'] != 'Gratuitos' ? 'style="display: block;"' : 'style="display: none;"'?>>
				<label class="form-label mb-0">Porcentaje de descuento.</label>
				<div class="input-group mb-3">
					<input type="number" name="descuento" min="1" max="100" maxlength="3" class="form-control" pattern="[0-9 ]*" placeholder="Ejemplo: 25" value="<?=$cupon['descuento']?>" onkeypress="return event.charCode>=48 && event.charCode<=57" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" aria-label="Pesos mexicanos">
					<span class="input-group-text">%</span>
				</div>
			</div>
		</div>
		<div class="col-12 col-lg-6">
			<div class="form-label mb-4">Selecciona el tipo de cupón</div>
			<div class="form-floating mb-3">
				<select class="form-select" id="tipo_cupon" name="tipo_cupon" aria-label="texto_tipo_cupon">
					<option value="Gratuitos" <?= $cupon['tipo'] == 'Gratuitos' ? 'selected' : ''?>>Cuentas gratis</option>
					<option value="Paga" <?= $cupon['tipo'] == 'Paga' ? 'selected' : ''?>>Cuentas de Pago</option>
				</select>
				<label for="tipo_cupon">Tipo de cuenta</label>
			</div>
			<div class="form-label">Fecha de vigencia del cupón.</div>
			<div class="form-check form-switch">
				<input class="form-check-input" type="checkbox" role="switch" value="true" name="switch-vigencia" id="switch-vigencia" <?= empty($cupon['fecha_limite'])? 'checked' : '' ?>>
				<label class="form-check-label" for="switch-vigencia">Ilimitada</label>
			</div>
			<div id="input-vigencia" <?= empty($cupon['fecha_limite']) ? 'style="display: none;"' : 'style="display: block;"'?>>
				<div class="form-floating mb-3">
					<input type="date" class="form-control" min="<?= date('Y-m-d', strtotime(date('Y-m-d') . '+ 1 days')) ?>" name="vigencia" id="vigencia" value="<?=$cupon['fecha_limite']?>">
					<label for="vigencia">Fecha de vigencia</label>
				</div>
			</div>
			<div id="input-tipo-plan" <?= $cupon['tipo'] == 'Gratuitos' ? 'style="display: none;"' : 'style="display: block;"'?>>
				<label class="d-block mb-3">Tipo de plan</label>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="tipo_plan" value="mensual" id="mensual" <?=$cupon['tipo_plan'] == 'mensual' ? 'checked' : ''?>>
					<label class="form-check-label" for="mensual">Mensual</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="tipo_plan" value="anual" id="anual" <?=$cupon['tipo_plan'] == 'anual' ? 'checked' : ''?>>
					<label class="form-check-label" for="anual">Anual</label>
				</div>
			</div>
		</div>
		<div class="col-12 py-2 text-center">
			<a class="btn btn-primary" id="actua-cupon">Actualizar cupón</a>
		</div>
	</div>
</form>
<script>
	function mayus(e) {e.value = e.value.toUpperCase();}
	
	function seleccion_tipo_cupon(input){
		if (input.target.value == 'Gratuitos' || input.target.value.length == 0){
			$('#input-porcentaje').fadeOut();
			$('#input-tipo-plan').fadeOut();
		} else{
			$('#input-tipo-plan').fadeIn();
			$('#input-porcentaje').fadeIn();
		}
	}

	$('#switch-limite-total').change(() => $('#input-limite').fadeToggle());
	$('#switch-vigencia').change(() => $('#input-vigencia').fadeToggle());
	$('#tipo_cupon').change((e) => seleccion_tipo_cupon(e));

	$('#actua-cupon').click(function(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Pagos/saveEditCupon')?>',
			data: $('#form-editar-cupon').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error') return alertify.warning(cont.mensaje);
				
				alertify.success(cont.mensaje);
				$('#tabla').load('<?=base_url('/Pagos/tablaCupones')?>');
				return $('#editar-cupon').modal('hide');
			}, error: function(data){
				let respuesta = JSON.parse(data.responseText);
				alertify.error('Error: ' + respuesta.message + '\n' + respuesta.file);
			}
		});
	});
</script>