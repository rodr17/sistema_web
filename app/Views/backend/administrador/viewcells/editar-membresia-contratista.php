<?php if($membresia == null){ ?>
	<div class="text-center">
		<div class="alert alert-warning" role="alert">
			Sin informacion de la membresia
		</div>
	</div>
<?php }else{ ?>
	<form id="edit-form">
		<div class="form-floating mb-2">
			<input type="text" class="form-control" name="nombre" id="nombrePlan" value="<?=$membresia['name']?>" placeholder="...">
			<label for="nombrePlan">Nombre del plan</label>
		</div>
		<small class="fst-italic"><?=$membresia['cuenta']?></small>
		<input type="hidden" name="id" value="<?=$membresia['id']?>">
		<?php if($membresia['monto'] != 0) :?>
			<div class="form-floating mb-2">
				<input type="text" class="form-control" name="costo" id="costoPlan" value ="<?=$membresia['monto']?>" placeholder="...">
				<label for="costoPlan">Costo mensual</label>
			</div>
			<div class="form-floating mb-2">
				<input type="text" class="form-control" name="costo_anual" id="costoPlanAnual" value ="<?=$membresia['monto_anual']?>" placeholder="...">
				<label for="costoPlanAnual">Costo anual</label>
			</div>
		<?php endif ;?>
		<div class="mb-2">
			<label class="titulo-label">Preferencias</label>
		</div>
		<div class="mt-4 mb-4">
			<!-- Crear perfil de contratante -->
			<div class="form-check form-switch mb-2">
				<input class="form-check-input" type="checkbox" role="switch" value="1" name="perfilcontratante" id="perfilcontratante" readonly checked <?= view_cell('App\Libraries\FuncionesSistema::verificarPreferencia', ['plan' => $membresia['id'], 'valor' => 'perfilcontratante'])?>>
				<label class="form-check-label" for="perfilcontratante">Crear perfil de Contratante</label>
			</div>
			<!-- Panel de administracion -->
			<div class="form-check form-switch mb-2">
				<input class="form-check-input" type="checkbox" role="switch" value="1" name="paneladministracion" id="paneladministracion" readonly checked <?=view_cell('App\Libraries\FuncionesSistema::verificarPreferencia', ['plan' => $membresia['id'], 'valor' => 'paneladministracion'])?>>
				<label class="form-check-label" for="paneladministracion">Panel de administración</label>
			</div>
			<!-- Valoracion de trabajos -->
			<div class="form-check form-switch mb-2">
				<input class="form-check-input" type="checkbox" role="switch" value="1" name="valoraciontrabajos" id="valoraciontrabajos" readonly checked <?=view_cell('App\Libraries\FuncionesSistema::verificarPreferencia', ['plan' => $membresia['id'], 'valor' => 'valoraciontrabajos'])?>>
				<label class="form-check-label" for="valoraciontrabajos">Valoracion de trabajos</label>
			</div>
			<!-- Acceso a freelancer -->
			<div class="form-check form-switch mb-2">
				<input class="form-check-input" type="checkbox" role="switch" value="1" name="accesofreelancer" id="accesofreelancer" <?=view_cell('App\Libraries\FuncionesSistema::verificarPreferencia', ['plan' => $membresia['id'], 'valor' => 'accesofreelancer'])?>>
				<label class="form-check-label" for="accesofreelancer">Acceso a freelancer</label>
			</div>
			<!-- Historial de conversaciones -->
			<div class="form-check form-switch mb-2">
				<input class="form-check-input" type="checkbox" role="switch" name="historialconversaciones" id="historialconversaciones" <?=view_cell('App\Libraries\FuncionesSistema::verificarPreferencia', ['plan' => $membresia['id'], 'valor' => 'historialconversaciones'])?>>
				<label class="form-check-label" for="historialconversaciones">Salas de chat</label>
			</div>
			<!-- Contactar Freelancer -->
			<div class="form-check form-switch mb-2">
				<input class="form-check-input" type="checkbox" role="switch" name="contactarFreelancer" id="contactarFreelancer" <?=view_cell('App\Libraries\FuncionesSistema::verificarPreferencia', ['plan' => $membresia['id'], 'valor' => 'contactarFreelancer'])?>>
				<label class="form-check-label" for="contactarFreelancer">Contactar Freelancer</label>
			</div>
			<!-- Buscador de freelancer -->
			<div class="mb-2">
				<label class="label">Buscador de freelancer</label>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="buscadorfreelancer" value="restringido" id="tipobuscador1" <?=view_cell('App\Libraries\FuncionesSistema::buscadorfreelancer', ['plan' => $membresia['id'], 'valor' => 'restringido'])?>>
					<label class="form-check-label" for="tipobuscador1">
						Restringido
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="buscadorfreelancer" value="intermedio" id="tipobuscador2" <?=view_cell('App\Libraries\FuncionesSistema::buscadorfreelancer', ['plan' => $membresia['id'], 'valor' => 'intermedio'])?>>
					<label class="form-check-label" for="tipobuscador2">
						Intermedio
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="buscadorfreelancer" value="avanzado" id="tipobuscador3" <?=view_cell('App\Libraries\FuncionesSistema::buscadorfreelancer', ['plan' => $membresia['id'], 'valor' => 'avanzado'])?>>
					<label class="form-check-label" for="tipobuscador3">
						Avanzado
					</label>
				</div>
			</div>
			<!-- Acceso a freelancer por nivel -->
			<div class="mb-2">
				<label class="label">Acceso a Freelancer por nivel</label>
				<div class="form-check">
					<input class="form-check-input" type="checkbox" name="accesofreelancernivel[]" value="basicos" id="accesofreelancernivel1" <?=view_cell('App\Libraries\FuncionesSistema::accesoNivelFreelancer', ['plan' => $membresia['id'], 'valor' => 'basicos'])?>>
					<label class="form-check-label" for="accesofreelancernivel1">
						Basicos
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="checkbox" name="accesofreelancernivel[]" value="nuevos" id="accesofreelancernivel2" <?=view_cell('App\Libraries\FuncionesSistema::accesoNivelFreelancer', ['plan' => $membresia['id'], 'valor' => 'nuevos'])?>>
					<label class="form-check-label" for="accesofreelancernivel2">
						Nuevos
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="checkbox" name="accesofreelancernivel[]" value="intermedios" id="accesofreelancernivel3" <?=view_cell('App\Libraries\FuncionesSistema::accesoNivelFreelancer', ['plan' => $membresia['id'], 'valor' => 'intermedios'])?>>
					<label class="form-check-label" for="accesofreelancernivel3">
						Intermedios
					</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="checkbox" name="accesofreelancernivel[]" value="avanzados" id="accesofreelancernivel4" <?=view_cell('App\Libraries\FuncionesSistema::accesoNivelFreelancer', ['plan' => $membresia['id'], 'valor' => 'avanzados'])?>>
					<label class="form-check-label" for="accesofreelancernivel4">
						Avanzados
					</label>
				</div>
			</div>
			<!-- Envio de solicitudes por proyecto -->
			<div class="form-floating mb-2">
				<input type="text" class="form-control" id="enviosolicitudes" value="<?=view_cell('App\Libraries\FuncionesSistema::enviosolicitudes', ['plan' => $membresia['id']])?>" placeholder="..." name="enviosolicitudes">
				<label for="enviosolicitudes">Envio de solicitudes por proyecto</label>
			</div>
			<!-- Proyectos activos -->
			<div class="form-floating mb-2">
				<input type="text" class="form-control" id="proyectosActivos" value="<?=view_cell('App\Libraries\FuncionesSistema::proyectosActivos', ['plan' => $membresia['id']])?>" placeholder="..." name="proyectosActivos">
				<label for="proyectosActivos">Proyectos activos</label>
			</div>
			<!-- Historial de trabajos -->
			<div class="form-floating mb-2">
				<input type="text" class="form-control" id="historialtrabajos" value="<?=view_cell('App\Libraries\FuncionesSistema::historialtrabajos', ['plan' => $membresia['id']])?>" placeholder="..." name="historialtrabajos">
				<label for="historialtrabajos">Historial de trabajos</label>
			</div>
			<!-- Proyectos destacados -->
			<div class="form-floating mb-2">
				<input type="text" class="form-control" id="proyectosDestacados" value="<?=view_cell('App\Libraries\FuncionesSistema::proyectosDestacados', ['plan' => $membresia['id']])?>" name="proyectosDestacados" placeholder="...">
				<label for="proyectosDestacados">Proyectos destacados</label>
			</div>
			<div class="form-floating mb-2">
				<textarea type="text" class="form-control" name="descripcion" id="descripcion" style="height: 130px;" placeholder="..."><?= $membresia['descripcion']?></textarea>
				<label for="descripcion">Descripción membresía</label>
			</div>
		</div>
		<div class="text-center mb-2">
			<a class="btn btn-primary" onclick="editarMemb()" id="actua-plan">Actualizar plan</a>
		</div>
	</form>
<?php } ?>