<?php if($membresia == null){ ?>
	<div class="text-center">
		<div class="alert alert-warning" role="alert">
			Sin información de la membresia
		</div>
	</div>
<?php }else{ ?>
	<form id="edit-form">
		<div class="form-floating mb-2">
			<input type="text" class="form-control" name="nombre" id="nombrePlan" value="<?=$membresia['name']?>" placeholder="...">
			<label for="nombrePlan">Nombre del plan</label>
		</div>
		<small class="fst-italic"><?=$membresia['cuenta']?></small>
		<input type="hidden" name="id" value="<?=$membresia['id']?>">
		<?php if($membresia['monto'] != 0) :?>
			<div class="form-floating mb-2">
				<input type="text" class="form-control" name="costo" id="costoPlan" value ="<?=$membresia['monto']?>" placeholder="...">
				<label for="costoPlan">Costo mensual</label>
			</div>
			<div class="form-floating mb-2">
				<input type="text" class="form-control" name="costo_anual" id="costoPlanAnual" value ="<?=$membresia['monto_anual']?>" placeholder="...">
				<label for="costoPlanAnual">Costo anual</label>
			</div>
		<?php endif ;?>
		<div class="mb-2">
			<label class="titulo-label">Preferencias</label>
		</div>
		<div class="mt-4 mb-4">
			<div class="form-check form-switch mb-2">
				<input class="form-check-input" type="checkbox" role="switch" value="1" name="crearcv" id="crearcv" <?=view_cell('App\Libraries\FuncionesSistema::verificarPreferencia', ['plan' => $membresia['id'], 'valor' => 'crearcv'])?>>
				<label class="form-check-label" for="crearcv" style="cursor: pointer;">Cargar CV</label>
			</div>
			<div class="form-check form-switch mb-2">
				<input class="form-check-input" type="checkbox" role="switch" value="1" name="veroportunidades" id="veroportunidades" <?=view_cell('App\Libraries\FuncionesSistema::verificarPreferencia', ['plan' => $membresia['id'], 'valor' => 'veroportunidades'])?>>
				<label class="form-check-label" for="veroportunidades" style="cursor: pointer;">Visualización de oportunidades</label>
			</div>
			<div class="form-check form-switch mb-2">
				<input class="form-check-input" type="checkbox" role="switch" value="1" name="paneladministracion" id="paneladministracion" <?=view_cell('App\Libraries\FuncionesSistema::verificarPreferencia', ['plan' => $membresia['id'], 'valor' => 'paneladministracion'])?>>
				<label class="form-check-label" for="paneladministracion" style="cursor: pointer;">Panel de administración</label>
			</div>
			<div class="form-floating mb-2">
				<input type="text" class="form-control" id="postuporsemana" value="<?=view_cell('App\Libraries\FuncionesSistema::postuSemana', ['plan' => $membresia['id']])?>" placeholder="..." name="postuporsemana">
				<label for="postuporsemana">Postulaciones por semana</label>
			</div>
			<div class="form-check form-switch mb-2">
				<input class="form-check-input" type="checkbox" role="switch" value="1" name="anadirportafolio" id="anadirportafolio" <?=view_cell('App\Libraries\FuncionesSistema::verificarPreferencia', ['plan' => $membresia['id'], 'valor' => 'anadirportafolio'])?>>
				<label class="form-check-label" for="anadirportafolio" style="cursor: pointer;">Añadir portafolio</label>
			</div>
			<div class="form-check form-switch mb-2">
				<input class="form-check-input" type="checkbox" role="switch" value="1" name="solicitudtrabajo" id="solicitudtrabajo" <?=view_cell('App\Libraries\FuncionesSistema::verificarPreferencia', ['plan' => $membresia['id'], 'valor' => 'solicitudtrabajo'])?>>
				<label class="form-check-label" for="solicitudtrabajo" style="cursor: pointer;">Solicitud de trabajo</label>
			</div>
			<div class="form-check form-switch mb-2">
				<input class="form-check-input" type="checkbox" role="switch" value="1" name="historialconversaciones" id="historialconversaciones" <?=view_cell('App\Libraries\FuncionesSistema::verificarPreferencia', ['plan' => $membresia['id'], 'valor' => 'historialconversaciones'])?>>
				<label class="form-check-label" for="historialconversaciones" style="cursor: pointer;">Salas de chat</label>
			</div>
			<div class="form-check form-switch mb-2">
				<input class="form-check-input" type="checkbox" role="switch" value="1" name="freelancer_destacado" id="freelancer_destacado" <?=view_cell('App\Libraries\FuncionesSistema::verificarPreferencia', ['plan' => $membresia['id'], 'valor' => 'freelancer_destacado'])?>>
				<label class="form-check-label" for="freelancer_destacado" style="cursor: pointer;">Freelancer destacado</label>
			</div>
			<div class="form-floating mb-2">
				<textarea type="text" class="form-control" name="descripcion" id="descripcion" style="height: 130px;" placeholder="..."><?= $membresia['descripcion']?></textarea>
				<label for="descripcion">Descripción membresía</label>
			</div>
		</div>
		<div class="text-center mb-2">
			<a class="btn btn-primary" onclick="editarMemb()" id="actua-plan">Actualizar plan</a>
		</div>
	</form>
<?php } ?>