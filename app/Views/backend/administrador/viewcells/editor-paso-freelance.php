<?php if($datos != null){ ?>
    <input id="idpaso" name="idpaso" value="<?=$id?>" type="hidden">
    <div class="form-floating mb-3">
        <input type="text" class="form-control" id="titulo-paso" value="<?=$datos['titulo']?>" placeholder="...">
        <label for="titulo-paso">Titulo de paso</label>
    </div>
    <div class="mb-3">
        <textarea id="descripcion-paso"><?=$datos['contenido']?></textarea>
    </div>
    <input type="hidden" id="paso-icono" name="icono" value="<?=$datos['imagen']?>">
    <input class="form-control form-control-sm" id="icono-paso" type="file" onchange="readUrlImagen(this, 'paso-icono', 'preview-icono-paso');">
    <div class="text-center m-3">
        <img id="preview-icono-paso" class="w-25" src="<?=$datos['imagen']?>" alt="your image" />
    </div>
    <div class="text-center my-3">
        <a class="btn btn-primary" id="save-paso">
            Guardar cambios
        </a>
    </div>
<?php }else{ ?>
    <input id="idpaso" name="idpaso" value="<?=$id?>" type="hidden">
    <div class="form-floating mb-3">
        <input type="text" class="form-control" id="titulo-paso" value="" placeholder="...">
        <label for="titulo-paso">Titulo de paso</label>
    </div>
    <div class="mb-3">
        <textarea id="descripcion-paso"></textarea>
    </div>
    <input type="hidden" id="paso-icono" name="icono" value="">
    <input class="form-control form-control-sm" id="icono-paso" type="file" onchange="readUrlImagen(this, 'paso-icono', 'preview-icono-paso');">
    <div class="text-center m-3">
        <img id="preview-icono-paso" class="w-25" src="" alt="your image" />
    </div>
    <div class="text-center my-3">
        <a class="btn btn-primary" id="save-paso">
            Guardar cambios
        </a>
    </div>
<?php } ?>
<script>
    var descripcionPaso = CKEDITOR.replace('descripcion-paso');
    // Funcion para guardar paso
    $('#save-paso').click(function(){
        let id = $('#idpaso').val();
        let titulo = $('#titulo-paso').val();
        let editorData = descripcionPaso.getData();
        let postBody = editorData.replace(/&nbsp;/gi,' ');
        let imagen = $('#paso-icono').val();
        let dataString = 'id='+id+'&titulo='+titulo+'&contenido='+postBody+'&imagen='+imagen;
        // return console.log(dataString);
        $.ajax({
            type:'POST',
            url: '<?=base_url('/Secciones/updatePasoFreelance')?>',
            data: dataString,
            cache: false,
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
                return alertify.success(cont.mensaje, 10);
            },error:function(data){
                alertify.error('Error', 10);
            }
        });
    });
</script>