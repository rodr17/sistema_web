<form id="form-subarea">
    <div class="form-floating mb-2">
        <input type="text" class="form-control" id="subarea" name="subarea" placeholder="..." value="<?=$subarea['area']?>">
        <label for="subarea">Area</label>
    </div>
    <div class="form-floating mb-2">
        <select class="form-select" id="superior" name="superior" aria-label="Floating label select example">
            <option value="0">Seleccione una opción</option>
            <?php foreach($areas as $as){ ?>
                <option value="<?=$as['area']?>" <?=($as['area'] == $subarea['superior'])? 'selected' : '' ?>><?=$as['area']?></option>
            <?php } ?>
        </select>
        <label for="superior">Area o habilidad superior</label>
    </div>
    <input name="idsub" id="idsub" type="hidden" value="<?=$subarea['id']?>">
</form>