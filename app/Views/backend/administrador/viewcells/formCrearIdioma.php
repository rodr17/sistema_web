<form id="form-new-idioma">
    <div class="form-floating mb-3">
        <input type="text" class="form-control" id="nombre-nuevo" name="nombre" placeholder="...">
        <label for="nombre-nuevo">Nombre del idioma</label>
    </div>
    <div class="mb-3">
        <input id="search" type="text" class="form-control"  placeholder="Buscar bandera para el idioma">
        <br>
        <div class="row">
            <div class="col-6 align-self-center">
                <div class="text-center">
                    <label class="fw-bold">
                        Nombre: <span id="nombre-span"></span>
                    </label>
                    <br>
                    <img src="" class="d-none w-50" id="flag">
                    <input type="hidden" name="bandera" id="bandera">
                </div>
            </div>
            <div class="col-6" style="max-height: 350px; overflow: auto;">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Pais</th>
                        </tr>
                    </thead>
                    <tbody id="table">
                        <?php foreach($idiomas as $key => $i){ ?>
                            <tr>
                                <td>
                                    <div class="form-check">
                                        <input class="form-check-input d-none" type="radio" id="pais<?=$key?>sel" name="pais" value="<?=$i['identificador']?>" >
                                        <label class="form-check-label d-flex justify-content-between w-100" for="pais<?=$key?>sel">
                                            <?=$i['pais']?>&nbsp;&nbsp;<img src="<?=$i['bandera']?>" style="max-width: 40px;">
                                        </label>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="text-center">
        <a class="btn btn-primary" id="add-idioma">
            Añadir idioma
        </a>
    </div>
</form>
<script>
    $('#nombre-nuevo').on('keyup', function(){
        $('#nombre-span').html($(this).val());
    });
    $('input[name="pais"]').change(function(){
        let pais = $(this).val();
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Administrador/verPais')?>',
            data:{pais:pais},
            success: function(data){
                $('#flag').attr('src', data);
                $('#bandera').val(data);
                $('#flag').removeClass('d-none');
            }, error: function(data){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo', 10);
            }
        });
    });
    $("#search").on("keyup", function() {
        let value = $(this).val().toLowerCase();
        $("#table tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    $('#add-idioma').on('click', function(){
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Administrador/nuevoIdioma')?>',
            data: $('#form-new-idioma').serialize(),
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error'){
                    alertify.warning(cont.mensaje, 10);
                }else{
                    alertify.success(cont.mensaje, 10);
                    $('#form-new-idioma')[0].reset();
                    $('#flag').attr('src', '');
                    $('tr').attr('style', 'display: grid;');
                    // $('#sec-add-idioma').html('<div class="text-center"><a class="btn btn-primary" href="<?=base_url('/idiomas')?>">Ver lista de idiomas</a></div>');
                }
            }, error: function(data){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
            }
        });
    });
</script>