<div class="p-1">
    <canvas id="grafica-pagos"></canvas>
</div>
<script>
    let da<?=strtotime($hoy)?>ta = {
        labels: [
            "Pago con Tarjetas ()",
            'Pago en Efectivo'
        ],
        datasets: [{
            label: 'Reporte de pagos',
            data: [<?=$montos['tarjetas']?>, <?=$montos['efectivo']?>],
            backgroundColor: [
                '#00176A',
                '#00B05F'
            ],
            hoverOffset: 1
        }]
    };
    let con<?=strtotime($hoy)?>fig = {
        type: 'pie',
        data: da<?=strtotime($hoy)?>ta,
        options: {
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function(context) {
                            let label = context.dataset.label || '';
                            if (label) {
                                label += ': ';
                            }
                            if (context.parsed.y !== null) {
                                label += new Intl.NumberFormat('en-US', { style: 'currency', currency: 'MXN' }).format(context.parsed);
                            }
                            return label;
                        }
                    }
                }
            }
        }
    };
    let my<?=strtotime($hoy)?>Chart = new Chart(
        document.getElementById('grafica-pagos'),
        con<?=strtotime($hoy)?>fig
    );
</script>