<div class="p-1">
    <canvas id="grafica-registro"></canvas>
</div>
<script>
    let data<?=strtotime($hoy)?>registro = {
        labels: [
            'Correo',
            'Facebook',
            'Google'
        ],
        datasets: [{
            label: 'Estadisticas de registro',
            data: [<?=$montos['correo']?>, <?=$montos['facebook']?>, <?=$montos['google']?>],
            backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(255, 159, 64, 0.2)',
              'rgba(255, 205, 86, 0.2)',
            ],
            borderColor: [
              'rgb(255, 99, 132)',
              'rgb(255, 159, 64)',
              'rgb(255, 205, 86)',
            ],
            borderWidth: 1
        }]
    };
    let config<?=strtotime($hoy)?>registro = {
        type: 'bar',
        data: data<?=strtotime($hoy)?>registro,
        options: {
            indexAxis: 'y',
        }
    };
    let myChart<?=strtotime($hoy)?>registro = new Chart(
        document.getElementById('grafica-registro'),
        config<?=strtotime($hoy)?>registro
    );
</script>