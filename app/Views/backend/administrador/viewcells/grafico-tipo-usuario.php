<div class="mb-4">
	<canvas id="grafica-tipo"></canvas>
</div>
<div class="">
	<canvas id="grafica-tipo2"></canvas>
</div>
<script>
	let data<?=strtotime($hoy)?>tipo = {
		labels: [
			'Freelancers',
		],
		datasets: [
			{
				label: 'Total',
				data: [<?=$montos['freelancer']?>],
				backgroundColor: [
				'rgba(255, 99, 132, 0.2)',
				],
				borderColor: [
				'rgb(255, 99, 132)',
				
				],
				borderWidth: 1
			},
			{
				label: 'Correo',
				type: 'bar',
				data: [<?=$montos['correo-freelancers']?>],
				backgroundColor: [
					'rgba(0, 23, 106, .2)'
				],
				borderColor: [
					'rgba(0, 23, 106, 1)',
				],
				borderWidth: 1
			},
			{
				label: 'Google',
				data: [<?=$montos['google-freelancers']?>],
				backgroundColor: [
					'rgba(17, 172, 34, .2)'
				],
				borderColor: [
					'rgba(17, 172, 34, 1)',
				],
				borderWidth: 1
			},
			{
				label: 'Facebook',
				data: [<?=$montos['facebook-freelancers']?>],
				backgroundColor: [
					'rgba(34, 206, 206, .2)'
				],
				borderColor: [
					'rgba(34, 206, 206, 1)',
				],
				borderWidth: 1
			}
		]
	};

	let config<?=strtotime($hoy)?>tipo = {
		type: 'bar',
		data: data<?=strtotime($hoy)?>tipo,
		options: {
			indexAxis: 'y',
		}
	};

	let myChart<?=strtotime($hoy)?>tipo = new Chart(
		document.getElementById('grafica-tipo'),
		config<?=strtotime($hoy)?>tipo
	);

	let data<?=strtotime($hoy)?>tipo2 = {
		labels: ['Contratantes'],
		datasets: [
			{
				label: 'Total',
				data: [<?=$montos['contratante']?>],
				backgroundColor: [
				'rgba(255, 99, 132, 0.2)',
				],
				borderColor: [
				'rgb(255, 99, 132)',
				
				],
				borderWidth: 1
			},
			{
				label: 'Correo',
				type: 'bar',
				data: [<?=$montos['correo-contratantes']?>],
				backgroundColor: [
					'rgba(0, 23, 106, .2)'
				],
				borderColor: [
					'rgba(0, 23, 106, 1)',
				],
				borderWidth: 1
			},
			{
				label: 'Google',
				data: [<?=$montos['google-contratantes']?>],
				backgroundColor: [
					'rgba(17, 172, 34, .2)'
				],
				borderColor: [
					'rgba(17, 172, 34, 1)',
				],
				borderWidth: 1
			},
			{
				label: 'Facebook',
				data: [<?=$montos['facebook-contratantes']?>],
				backgroundColor: [
					'rgba(34, 206, 206, .2)'
				],
				borderColor: [
					'rgba(34, 206, 206, 1)',
				],
				borderWidth: 1
			}
		]
	};

	let config<?=strtotime($hoy)?>tipo2 = {
		type: 'bar',
		data: data<?=strtotime($hoy)?>tipo2,
		options: {
			indexAxis: 'y',
		}
	};

	let myChart<?=strtotime($hoy)?>tipo2 = new Chart(
		document.getElementById('grafica-tipo2'),
		config<?=strtotime($hoy)?>tipo2
	);
</script>