<label class="d-block">Cantidad de veces utilizados: <b><?=$cantidad_utilizados?></b></label>
<label class="d-block mt-2">Cantidad de veces canjeados: <b><?=$cantidad_canjeados?></b></label>
<label class="d-block mt-2">Cantidad restante de uso: <b><?= empty($cupon['limite']) ? 'Ilimitado' : $cupon['limite'] - $cantidad_canjeados;?></b></label>