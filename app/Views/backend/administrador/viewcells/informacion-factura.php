<div class="card bg-white">
    <div class="card-body">
        <div class="text-center">
            Información de la compra
        </div>
        <hr>
        <div class="d-grid">
            <label>
                Folio de compra: <span class="fw-bold"><?=view_cell('App\Libraries\FuncionesSistema::folioCompra', ['id' => $solicitud['id_compra']])?></span>
            </label>
            <label>
                Fecha de compra: <span class="fw-bold"><?= view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $pago['fecha']]) ?></span>
            </label>
            <label>
                Plan: <span class="fw-bold"><?= view_cell('App\Libraries\FuncionesSistema::datoPlanAdmin', ['planid' => $pago['id_membresia'], 'dato' => 'name', 'tipo' => $pago['tipo']]) ?></span>
            </label>
            <label>
                Total: <span class="fw-bold">$ <?= number_format($pago['monto'], 2, '.', ',') ?> MXN</span>
            </label>
            <label>
                Pagado a través de: <strong><?= ($pago['tipo'] == 'openpay')? 'Tarjeta debito / credito' : (($pago['tipo'] == 'paypal')? 'Paypal' : (($pago['tipo'] == 'tienda')? 'Tiendas de conveniencia' : 'Transferencia bancaria') )?></strong>
            </label>
        </div>
        <div class="text-center">
            Información de facturación
        </div>
        <hr>
        <div class="d-grid">
            <?php $datosf = json_decode($solicitud['datos'], true); ?>
            <label>
                Nombre: <span class="fw-bold"><?= $datosf['nombre'] ?></span>
            </label>
            <label>
                RFC: <span class="fw-bold"><?= $datosf['rfc'] ?></span>
            </label>
            <label>
                Correo: <span class="fw-bold"><?=$datosf['correo']?></span>
            </label>
            <label>
                Telefono: <span class="fw-bold"><?= $datosf['telefono'] ?></span>
            </label>
        </div>
        <label class="w-100 text-center my-2">
            Dirección de facturación
        </label>
        <div class="d-grid">
            <label>
                Calle: <span class="fw-bold"><?=$datosf['direccion']['calle']?></span>
            </label>
            <label>
                Colonia: <span class="fw-bold"><?=$datosf['direccion']['colonia']?></span>
            </label>
            <label>
                Codigo postal: <span class="fw-bold"><?=$datosf['direccion']['zip']?></span>
            </label>
            <label>
                Ciudad: <span class="fw-bold"><?=$datosf['direccion']['ciudad']?></span>
            </label>
            <label>
                Estado: <span class="fw-bold"><?=$datosf['direccion']['estado']?></span>
            </label>
            <label>
                Pais: <span class="fw-bold"><?=$datosf['direccion']['pais']?></span>
            </label>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="text-center" id="estatus">Estatus: <label class="mb-0 fw-bold text-<?= ($solicitud['estatus'] != 'enviado')? 'danger' : 'success' ; ?>"><?=ucfirst($solicitud['estatus'])?></label></div>
            </div>
            <!--<div class="col-12 col-lg-6 text-center">-->
            <!--    <select class="form-select mb-3" id="select-estatus" aria-label="Default select example">-->
            <!--        <option selected>Seleccione una opción</option>-->
            <!--        <option value="espera" <?= ($solicitud['estatus'] == 'espera')? 'selected' : '' ; ?>>Espera</option>-->
            <!--        <option value="en proceso" <?= ($solicitud['estatus'] == 'en proceso')? 'selected' : '' ; ?>>En proceso</option>-->
            <!--        <option value="enviada" <?= ($solicitud['estatus'] == 'enviada')? 'selected' : '' ; ?>>Enviada</option>-->
            <!--    </select>-->
            <!--</div>-->
        </div>
    </div>
</div>
<div clas="row">
    <div class="col-12">
        <div class="card bg-white mt-4">
            <div class="card-header">
                <label class="card-title fw-bold m-0">Adjuntar</label>
            </div>
            <div class="card-body">
                <label for="archivos_factura" class="form-label fst-italic small">Solo se permite archivos <span class="fw-bold">XML</span> ó <span class="fw-bold">PDF</span></label>
                <div id="contenido_botones">
                    <form class="input-group" id="form_archivos_factura" enctype="multipart/form-data">
                        <input class="invisible d-none" value="<?=$solicitud['id']?>" type="hidden" id="input-id-solicitud" name="solicitud">
                        <?php if($solicitud['estatus'] == 'en proceso') :?>
                            <input class="form-control form-control-sm" id="archivos_factura" type="file" accept=".xml, .pdf" name="facturacion[]" multiple>
                            <a class="btn btn-primary btn-sm" id="actua-soli">Enviar facturación</a>
                        <?php else :?>
                            <a class="btn btn-primary btn-sm reenviar" id="reenviar_facturacion">Reenviar correo</a>
                        <?php endif ;?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#archivos_factura').change(function(evento){
        validarArchivos(evento.target);
    });
    $('#actua-soli').click(function(){
        
        if(validarArchivos($('#archivos_factura')[0]) != undefined) return ;
        let formulario = new FormData($('#form_archivos_factura')[0]);
        formulario.append('folio', '<?=$folio?>');
        formulario.append('plan', '<?= view_cell('App\Libraries\FuncionesSistema::datoPlanAdmin', ['planid' => $pago['id_membresia'], 'dato' => 'name', 'tipo' => $pago['tipo']]) ?>');
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Administrador/actualizarSolFactura')?>',
            data: formulario,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo != 'success') return alertify.warning(cont.mensaje);
                
                $('#listado-facturas').load('/administrador/recargaFacturas');
                $('#estatus').html('Estatus: <label class="mb-0 fw-bold text-success">Enviada</label>');
                $('input:file').remove();
                $('#actua-soli').addClass('reenviar').text('Reenviar correo').attr('id', 'reenviar_facturacion');
                return alertify.success('Solicitud de facturación actualizada.');
            }, error: function(data){
                alertify.error('Ha surgido un error, comuníquese con el equipo de desarrollo. '+ data.responseText);
            }
        });
    });
    $('#contenido_botones').delegate('a.reenviar', 'click', function(evento) {
        if (alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro de volver a enviar la facturación?', function() {
            $.ajax({
                type: 'POST',
                url: '<?=base_url('/Administrador/reenviarFacturacion')?>',
                data: {folio: '<?=$folio?>', id : <?=$solicitud['id']?>, plan : '<?= view_cell('App\Libraries\FuncionesSistema::datoPlanAdmin', ['planid' => $pago['id_membresia'], 'dato' => 'name', 'tipo' => $pago['tipo']]) ?>'},
                success: function(data){
                    let respuesta = JSON.parse(data);
                    if(respuesta.tipo != 'success') return alertify.warning(respuesta.mensaje);
                    
                    return alertify.success(respuesta.mensaje);
                }, error: function(data){
                    alertify.error('Ha surgido un error, comuníquese con el equipo de desarrollo. '+ data.responseText);
                }
            });
        }, function(){}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
    });
    function validarArchivos(input){
        if(input.files.length == 0) return alertify.warning('Adjunta al menos un archivo PDF o XML');
        if(input.files.length > 2){ 
            alertify.warning('Solo se permiten máximo 2 archivos');
            return input.value = "";
        }
        for (let archivo of input.files) {
            let archivo_arreglo = archivo.name.split('.');
            
            if(archivo_arreglo[1] == 'pdf' || archivo_arreglo[1] == 'xml') continue;
            else{
                input.value = "";
                return alertify.warning('Solo se permiten archivos XML ó PDF. Cambia el archivo "'+archivo_arreglo[0]+'" por otro permitido');
            }
        }
    }
</script>