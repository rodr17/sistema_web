<div class="card">
	<div class="card-body">
		<label>Folio de compra: <span class="fw-bold"><?=view_cell('App\Libraries\FuncionesSistema::folioCompra', ['id' => $pago['id_compra']])?></span></label><br>
		<label>ID de compra: <span class="fw-bold"><?=$pago['id_compra']?></span></label>
		<div class="mt-3">
			<div class="d-grid mb-2">
				<hr>
				<div class="text-center">Información de compra</div>
				<label>Plan: <?= view_cell('App\Libraries\FuncionesSistema::datoPlanAdmin', ['planid' => $pago['id_membresia'], 'dato' => 'name']) ?> <?=$tipo?></label>
				<label>Total: <b>$ <?= number_format($pago['monto'], 2, '.', ',') ?> MXN</b></label>
				<label>Fecha de compra: <?= view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $pago['fecha']]) ?></label>
				<label>Vigencia hasta: <?=$vigencia?></label>
				<label>Pagado a través de: <strong><?= $pago['tipo']?></strong></label>
				<?php if(isset($cupon_usado['codigo'])) :?>
				    <label>Cupón utilizado: <strong><?= $cupon_usado['codigo']?></strong></label>
				<?php endif ;?>
				<?= view_cell('App\Libraries\FuncionesAdmin::verTarjetaCompra', ['tipo' => $pago['tipo'], 'id' => $pago['id_compra'], 'user' => $pago['id_usuario']]) ?>
			</div>
			<div class="d-grid mb-2">
				<hr>
				<div class="text-center my-1">Información de usuario</div>
				<?php if($user == null){ ?>
					<div class="text-center">
						<span class="fw-bold">No se logró encontrar información del usuario.</span>
					</div>
				<?php }else{ ?>
					<label>Nombre: <?= $user['nombre']?> <?= $user['apellidos'] ?></label>
					<label>Correo: <?= $user['correo'] ?></label>
					<label>Teléfono: <?= isset($user['contacto']['telefono']) ? $user['contacto']['telefono'] : '' ?></label>
					<label>Tipo de cuenta: <b><?= $user['rol'] ?></b></label>
				<?php } ?>
			</div>
		</div>
	</div>
</div>