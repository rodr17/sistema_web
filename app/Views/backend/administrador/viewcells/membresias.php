<?php if($membresias == null){ ?>
    <div class="text-center">
        Sin membresias aun.
    </div>
<?php }else{ ?>
    <div class="row">
        <div class="col-6 text-start">
            <a class="btn" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                <i class="fas fa-chevron-left"></i>
            </a>
        </div>
        <div class="col-6 text-end">
            <a class="btn" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                <i class="fas fa-chevron-right"></i>
            </a>
        </div>
    </div>
    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel" data-bs-interval="false">
        <div class="carousel-inner">
            <?php foreach($membresias as $key=>$m){ ?>
                <div class="carousel-item <?php if($key == 0) echo 'active';?>">
                    <table class="table table-borderless text-center">
                        <thead>
                            <tr>
                                <td class="text-center">
                                    <label class="m-0">
                                        <?=$m['name']?>
                                    </label>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><i class="fas <?=view_cell('App\Libraries\FuncionesSistema::checkedPaquetes', ['plan' => $m['id'], 'valor' => 'perfilcontratante'])?>"></i></td>
                            </tr>
                            <tr>
                                <td><i class="fas <?=view_cell('App\Libraries\FuncionesSistema::checkedPaquetes', ['plan' => $m['id'], 'valor' => 'accesofreelancer'])?>"></i></td>
                            </tr>
                            <tr>
                                <td><label class="m-0 text-info"><?=view_cell('App\Libraries\FuncionesSistema::valorBneficio', ['plan' => $m['id'], 'valor' => 'enviosolicitudes'])?></label></td>
                            </tr>
                            <tr>
                                <td><label class="m-0 text-info"><?=view_cell('App\Libraries\FuncionesSistema::valorBneficio', ['plan' => $m['id'], 'valor' => 'buscadorfreelancer'])?></label></td>
                            </tr>
                            <tr>
                                <td><label class="m-0 text-info"><?=view_cell('App\Libraries\FuncionesSistema::valorBneficio', ['plan' => $m['id'], 'valor' => 'publicartrabajos'])?></label></td>
                            </tr>
                            <tr>
                                <td><label class="m-0 text-info"><?=view_cell('App\Libraries\FuncionesSistema::valorBneficio', ['plan' => $m['id'], 'valor' => 'promocionartrabajos'])?></label></td>
                            </tr>
                            <tr>
                                <td><i class="fas <?=view_cell('App\Libraries\FuncionesSistema::checkedPaquetes', ['plan' => $m['id'], 'valor' => 'paneladministracion'])?>"></i></td>
                            </tr>
                            <tr>
                                <td><label class="m-0 text-info"><?=view_cell('App\Libraries\FuncionesSistema::valorBneficio', ['plan' => $m['id'], 'valor' => 'nivelsoporte'])?></label></td>
                            </tr>
                            <tr>
                                <td><i class="fas <?=view_cell('App\Libraries\FuncionesSistema::checkedPaquetes', ['plan' => $m['id'], 'valor' => 'accesoinsignias'])?>"></i></td>
                            </tr>
                            <tr>
                                <td><i class="fas <?=view_cell('App\Libraries\FuncionesSistema::checkedPaquetes', ['plan' => $m['id'], 'valor' => 'valoraciontrabajos'])?>"></i></td>
                            </tr>
                            <tr>
                                <td><label class="m-0 text-info"><?=view_cell('App\Libraries\FuncionesSistema::valorBneficio', ['plan' => $m['id'], 'valor' => 'historialtrabajos'])?></label></td>
                            </tr>
                            <tr>
                                <td><i class="fas <?=view_cell('App\Libraries\FuncionesSistema::checkedPaquetes', ['plan' => $m['id'], 'valor' => 'historialconversaciones'])?>"></i></td>
                            </tr>
                            <tr>
                                <td><i class="fas <?=view_cell('App\Libraries\FuncionesSistema::checkedPaquetes', ['plan' => $m['id'], 'valor' => 'compartirdocumentos'])?>"></i></td>
                            </tr>
                            <tr>
                                <td><i class="fas <?=view_cell('App\Libraries\FuncionesSistema::checkedPaquetes', ['plan' => $m['id'], 'valor' => 'compartirdocumentos'])?>"></i></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><label class="m-0">$ <?=$m['monto']?></label></td>
                            </tr>
                            <tr>
                                <td><a class="btn btn-outline-naranja px-3">Obtener</a></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>