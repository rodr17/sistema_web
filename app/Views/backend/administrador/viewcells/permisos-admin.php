<label class="mb-3">Acceso a secciones</label>
<!--Inicia los nuevos campos-->
<div class="form-check form-switch">
	<input class="form-check-input" value="true" type="checkbox" role="switch" id="membresias" name="membresias" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'membresias']) == true) echo 'checked'; ?>>
	<label class="form-check-label" for="membresias">Membresias</label>
</div>
<div class="form-check form-switch">
	<input class="form-check-input" value="true" type="checkbox" role="switch" id="pagos" name="pagos" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'pagos']) == true) echo 'checked'; ?>>
	<label class="form-check-label" for="pagos">Pagos</label>
</div>
<div class="ms-4" id="inputs_pagos" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'pagos']) == true) echo 'style="display:none;"'; ?>>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="lista_pagos" id="lista_pagos" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'lista_pagos']) == true) echo 'checked'; ?>>
		<label class="form-check-label" for="lista_pagos">Listado de pagos</label>
	</div>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="estadisticas_generales" id="estadisticas_generales" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'estadisticas_generales']) == true) echo 'checked'; ?>>
		<label class="form-check-label" for="estadisticas_generales">Estadísticas generales</label>
	</div>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="solicitudes_facturacion" id="solicitudes_facturacion" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'solicitudes_facturacion']) == true) echo 'checked'; ?>>
		<label class="form-check-label" for="solicitudes_facturacion">Solicitudes de facturación</label>
	</div>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="crear_cupon" id="crear_cupon" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'crear_cupon']) == true) echo 'checked'; ?>>
		<label class="form-check-label" for="crear_cupon">Crear cupón</label>
	</div>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="lista_cupones" id="lista_cupones" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'lista_cupones']) == true) echo 'checked'; ?>>
		<label class="form-check-label" for="lista_cupones">Lista cupones</label>
	</div>
</div>
<div class="form-check form-switch">
	<input class="form-check-input" value="true" type="checkbox" role="switch" id="proyectos" name="proyectos" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'proyectos']) == true) echo 'checked'; ?>>
	<label class="form-check-label" for="proyectos">Proyectos</label>
</div>
<div class="form-check form-switch">
	<input class="form-check-input" value="true" type="checkbox" role="switch" id="usuarios" name="usuarios" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'usuarios']) == true) echo 'checked'; ?>>
	<label class="form-check-label" for="usuarios">Usuarios</label>
</div>
<div class="ms-4" id="inputs_usuarios" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'usuarios']) == true) echo 'style="display:none;"'; ?>>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="mi_perfil" id="mi_perfil" role="switch">
		<label class="form-check-label" for="mi_perfil">Mi perfil</label>
	</div>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="listado_administradores" id="listado_administradores" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'listado_administradores']) == true) echo 'checked'; ?>>
		<label class="form-check-label" for="listado_administradores">Listado de administradores</label>
	</div>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="listado_freelancers" id="listado_freelancers" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'listado_freelancers']) == true) echo 'checked'; ?>>
		<label class="form-check-label" for="listado_freelancers">Listado de freelancers</label>
	</div>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="listado_contratantes" id="listado_contratantes" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'listado_contratantes']) == true) echo 'checked'; ?>>
		<label class="form-check-label" for="listado_contratantes">Listado de contratantes</label>
	</div>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="agregar_usuario_admin" id="agregar_usuario_admin" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'agregar_usuario_admin']) == true) echo 'checked'; ?>>
		<label class="form-check-label" for="agregar_usuario_admin">Agregar usuario admin</label>
	</div>
</div>
<div class="form-check form-switch">
	<input class="form-check-input" value="true" type="checkbox" role="switch" id="habilidades" name="habilidades" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'habilidades']) == true) echo 'checked'; ?>>
	<label class="form-check-label" for="habilidades">Habilidades</label>
</div>
<div class="form-check form-switch">
	<input class="form-check-input" value="true" type="checkbox" role="switch" id="noticias" name="noticias" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'noticias']) == true) echo 'checked'; ?>>
	<label class="form-check-label" for="noticias">Noticias</label>
</div>
<div class="form-check form-switch">
	<input class="form-check-input" value="true" type="checkbox" role="switch" id="idiomas" name="idiomas" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'idiomas']) == true) echo 'checked'; ?>>
	<label class="form-check-label" for="idiomas">Idiomas</label>
</div>
<div class="form-check form-switch">
	<input class="form-check-input" value="true" type="checkbox" role="switch" id="contenidos" name="contenidos" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'contenidos']) == true) echo 'checked'; ?>>
	<label class="form-check-label" for="contenidos">Contenidos</label>
</div>
<div class="ms-4" id="inputs_contenidos" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'contenidos']) == true) echo 'style="display:none;"'; ?>>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="editar_portada" id="editar_portada" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'editar_portada']) == true) echo 'checked'; ?>>
		<label class="form-check-label" for="editar_portada">Editar portada</label>
	</div>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="listado_paginas" id="listado_paginas" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'listado_paginas']) == true) echo 'checked'; ?>>
		<label class="form-check-label" for="listado_paginas">Listado de páginas</label>
	</div>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="ajustes_generales" id="ajustes_generales" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'ajustes_generales']) == true) echo 'checked'; ?>>
		<label class="form-check-label" for="ajustes_generales">Ajustes generales</label>
	</div>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="aviso_privacidad" id="aviso_privacidad" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'aviso_privacidad']) == true) echo 'checked'; ?>>
		<label class="form-check-label" for="aviso_privacidad">Aviso de privacidad</label>
	</div>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="terminos_condiciones" id="terminos_condiciones" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'terminos_condiciones']) == true) echo 'checked'; ?>>
		<label class="form-check-label" for="terminos_condiciones">Términos y condiciones</label>
	</div>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="faq_tarifas" id="faq_tarifas" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'faq_tarifas']) == true) echo 'checked'; ?>>
		<label class="form-check-label" for="faq_tarifas">FAQ tarífas</label>
	</div>
	<div class="form-check form-switch">
		<input class="form-check-input" value="true" type="checkbox" name="faq_soporte" id="faq_soporte" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'faq_soporte']) == true) echo 'checked'; ?>>
		<label class="form-check-label" for="faq_soporte">FAQ soporte</label>
	</div>
</div>
<div class="form-check form-switch">
	<input class="form-check-input" value="true" type="checkbox" role="switch" id="medios_contacto" name="medios_contacto" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'medios_contacto']) == true) echo 'checked'; ?>>
	<label class="form-check-label" for="medios_contacto">Medios de contacto</label>
</div>
<div class="form-check form-switch">
	<input class="form-check-input" value="true" type="checkbox" name="llaves_api" id="llaves_api" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'llaves_api']) == true) echo 'checked'; ?>>
	<label class="form-check-label" for="llaves_api">Llaves API</label>
</div>
<div class="form-check form-switch">
	<input class="form-check-input" value="true" type="checkbox" name="contactos_soporte" id="contactos_soporte" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'contactos_soporte']) == true) echo 'checked'; ?>>
	<label class="form-check-label" for="contactos_soporte">Contactos soporte</label>
</div>
<div class="form-check form-switch">
	<input class="form-check-input" value="true" type="checkbox" name="contactos_boletin" id="contactos_boletin" role="switch" <?php if(view_cell('App\Libraries\FuncionesAdmin::verSeccionSide', ['id' => $id, 'seccion' => 'contactos_boletin']) == true) echo 'checked'; ?>>
	<label class="form-check-label" for="contactos_boletin">Contactos de boletín</label>
</div>
<script>
	$('#pagos').change(() => $('#inputs_pagos').fadeToggle());
	$('#usuarios').change(() => $('#inputs_usuarios').fadeToggle());
	$('#contenidos').change(() => $('#inputs_contenidos').fadeToggle());
</script>