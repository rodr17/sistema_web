<label>Asunto: <span class="fw-bold"><?=$reporte['asunto']?></span></label><br>
<label>Usuario: <span class="fw-bold"><?=$reporte['nombre']?></span></label><br>
<label>Teléfono: <span class="fw-bold"><?=$reporte['telefono']?></span></label><br>
<label>Correo: <span class="fw-bold"><?=$reporte['correo']?></span></label><br>
<div class="py-3">
    <label class="fw-bold">Descripción</label>
    <p>
        <?=$reporte['descripcion']?>
    </p>
</div>
<?php if(!empty($reporte['archivos'])){ ?>
    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <?php $imagenes = json_decode($reporte['archivos'], true);?>
        <div class="carousel-indicators">
            <?php foreach($imagenes as $key => $img){ ?>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="<?=$key?>" <?= ($key == 0)? 'class="active"' : '' ?> aria-current="true" aria-label="Slide <?=$key+1?>"></button>
            <?php } ?>
        </div>
        <div class="carousel-inner">
            <?php foreach($imagenes as $key => $img){ ?>
                <div class="carousel-item <?=($key == 0)? 'active' : '' ?>">
                    <img src="<?=base_url('/assets/images/fondo-gris.png')?>" class="d-block w-100" alt="fondo">
                    <div class="carousel-caption d-block">
                        <img class="w-50" src="<?=$img?>"><br>
                        <a class="btn btn-primary" href="<?=$img?>" target="_blank">
                            Expandir imagen
                        </a>
                    </div>
                </div>
            <?php } ?>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
<?php } ?>
<div class="mt-3 d-none" id="form-reporte-cont">
    <form id="form-reporte">
        <input name="id" value="<?=$reporte['id']?>" type="hidden">
        <div class="form-floating">
            <textarea class="form-control" placeholder="Leave a comment here" id="mensaje-rep" name="mensaje" style="height: 150px"></textarea>
            <label for="floatingTextarea2">Mensaje</label>
        </div>
    </form>
    <div class="mt-2 text-center">
        <a class="btn btn-primary" id="enviar-cont">
            Enviar respuesta
        </a>
    </div>
</div>
<script>
    $('#enviar-cont').click(function(){
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Administrador/responderSoporte')?>',
            data: $('#form-reporte').serialize(),
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
                alertify.success(cont.mensaje, 10);
                $('#reportes-soporte').load('<?=base_url('/Administrador/mostrarReportesSoporte')?>');
                $('.modal').modal('hide');
            }, error: function(data){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
            }
        });
    });
</script>