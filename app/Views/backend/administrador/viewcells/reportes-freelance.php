<?php foreach($reportes as $r){ ?>
<div class="border p-2 mb-3 bg-light bg-gradient">
    <div class="row">
        <div class="col-12 col-sm-12 col-lg-9">
            <p><?=word_limiter($r['comentario'], 15, '...')?></p>
            <label>
                Fecha: <?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $r['created_at']])?>
            </label>
        </div>
        <div class="col-12 col-sm-12 col-lg-3">
            <div class="d-grid gap-2">
                <a class="btn btn-primary btn-sm ver-reporte" data-id="<?=$r['id']?>">Ver</a>
            </div>
        </div>
    </div>
</div>
<?php } ?>