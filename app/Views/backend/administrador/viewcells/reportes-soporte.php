<?php foreach($reportes as $r){ ?>
    <div class="border p-2 mb-3 bg-light bg-gradient">
        <div class="row">
            <div class="col-12 col-sm-12 col-lg-9">
                <p>Asunto: <b><?=$r['asunto']?></b></p>
                <label>
                    Fecha: <?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $r['created_at']])?>
                </label>
            </div>
            <div class="col-12 col-sm-12 col-lg-3 align-self-center">
                <div class="d-grid gap-2">
                    <a class="btn btn-primary btn-sm ver-reporte-soporte" data-id="<?=$r['id']?>">Ver</a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<div class="modal fade" id="modal-reporte-soporte" tabindex="-1" aria-labelledby="titulo-modal-soporte" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titulo-modal-soporte">Solicitud de reporte</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="mostrar-reporte-soporte">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="cont-reporte">Contestar reporte</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('#cont-reporte').click(function(){
        $('#form-reporte-cont').toggleClass('d-none');
    });
    $('.ver-reporte-soporte').click(function(){
        let id = $(this).attr('data-id');
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Administrador/verReporteSoporte')?>',
            data:{id:id},
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
                $('#mostrar-reporte-soporte').html(cont.mensaje, 10);
                return $('#modal-reporte-soporte').modal('show');
            }, error: function(data){
                alertify.error('A surgido un error, comuniquese el equipo de desarrollo.', 10);
            }
        });
    });
</script>