<?php if($categoria == null){ ?>
    <form id="form-cate-icono">
        <div class="form-floating mb-3">
            <select class="form-select" id="select-categoria" name="categoria" aria-label="...">
                <?php foreach($habilidades as $h){ ?>
                    <option value="<?=$h['area']?>">
                        <?=$h['area']?>
                    </option>
                <?php } ?>
            </select>
            <label for="select-categoria">Seleccione la categoria</label>
        </div>
        <div class="mb-3">
            <input type="hidden" value="<?=$id?>" name="idcategoria">
            <div class="form-floating mb-3">
                <input type="text" class="form-control" id="icocategoria" name="icocategoria" placeholder="...">
                <label for="icocategoria">Icono de lord icon</label>
            </div>
        </div>
        <div class="text-center">
            <a class="btn btn-primary" id="guardar-categoria">
                Actualizar categoria
            </a>
        </div>
    </form>
<?php }else{ ?>
    <form id="form-cate-icono">
        <?php $info = json_decode($categoria['info'], true);?>
        <div class="form-floating mb-3">
            <select class="form-select" id="select-categoria" name="categoria" aria-label="...">
                <?php foreach($habilidades as $h){ ?>
                    <?php if($info['categoria'] == $h['area']){ ?>
                        <option value="<?=$h['area']?>" selected>
                            <?=$h['area']?>
                        </option>
                    <?php }else{ ?>
                        <option value="<?=$h['area']?>">
                            <?=$h['area']?>
                        </option>
                    <?php } ?>
                <?php } ?>
            </select>
            <label for="select-categoria">Seleccione la categoria</label>
        </div>
        <div class="mb-3">
            <input type="hidden" value="<?=$id?>" name="idcategoria">
            <div class="form-floating mb-3">
                <input type="text" class="form-control" id="icocategoria" name="icocategoria" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-inicio-'.$id, 'valor' => 'icono'])?>" placeholder="...">
                <label for="icocategoria">Icono de lord icon</label>
            </div>
        </div>
        <div class="text-center">
            <a class="btn btn-primary" id="guardar-categoria">
                Actualizar categoria
            </a>
        </div>
    </form>
<?php } ?>
<script>
    $('#guardar-categoria').click(function(){
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Secciones/saveCardCategoria')?>',
            data: $('#form-cate-icono').serialize(),
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error'){
                    alertify.warning(cont.mensaje, 10);
                }else{
                    alertify.success(cont.mensaje, 10);
                }
            }, error: function(data){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
            }
        });
    });
</script>