<div class="mb-3">
    <div class="row">
        <?php foreach($slides as $s){ ?>
            <?php $info = json_decode($s['info'], true)?>
            <div class="col-12 col-md-4">
                <div class="card">
                    <img src="<?=$info['slide']?>" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title"><?=$info['titulo']?></h5>
                        <a class="btn btn-primary editar-slide" data-slide="<?=$s['id']?>">Editar</a>
                        <a class="btn btn-danger eliminar-slide" data-slide="<?=$s['id']?>">Eliminar</a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<script>
    $('.editar-slide').click(function(){
        let id = $(this).attr('data-slide');
        $.ajax({
            type: 'POST',
            url: '<?=base_url('Secciones/mostrarSlide')?>',
            data:{id:id},
            success: function(data){
                let cont = JSON.parse(data);
                $('#idslide').val(id);
                $('#titulo-banner').val(cont.titulo);
                $('#subtitulo-banner').val(cont.subtitulo);
                $('#slide').val(cont.slide);
                $('#result-slide').attr('src', cont.slide);
                $('#slide-tablet').val(cont.tableta);
                $('#preview-tablet').attr('src', cont.tableta);
                $('#slide-movil').val(cont.movil);
                $('#preview-movil').attr('src', cont.movil);
                $('input[name="buscador"][value="'+cont.buscador+'"]').prop('checked', true);
            }, error: function(data){
                alertify.error('A surgido un error, comuniquese con el area de soporte.', 10);
            }
        });
    });
    $('.eliminar-slide').click(function(){
        let id = $(this).attr('data-slide');
        alertify.error(id);
    });
</script>