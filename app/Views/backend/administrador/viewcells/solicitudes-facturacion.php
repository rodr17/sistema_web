<?php if($solicitudes == null){ ?>
    <div class="text-center">
        Aun no hay solicitudes de facturación
    </div>
<?php }else{ ?>
    <?php foreach($solicitudes as $s){ ?>
        <div class="card m-2">
            <div class="card-body">
                <label>Folio: <span class="fw-bold">FACT-<?= strtotime($s['created_at']) ?>-<?=$s['id']?></span></label><br>
                <label>Fecha: <?= view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $s['created_at']]) ?></label><br>
                <label>Estatus: <?= $s['estatus'] ?></label><br>
                <a class="btn btn-primary btn-sm ver-info-factura" data-id="<?=$s['id']?>">
                    <small>Ver información</small>
                </a>
            </div>
        </div>
    <?php } ?>
<?php } ?>
<script>
    $('.ver-info-factura').click(function(){
        let id = $(this).attr('data-id');
        $.ajax({
            type: 'POST',
            url: '<?= base_url('/Administrador/verDatosFactura') ?>',
            data:{id:id},
            success: function(data){
                $('#info-factura-soli').html(data);
            }, error: function(data){
                console.log('Error');
            }
        });
    });
</script>