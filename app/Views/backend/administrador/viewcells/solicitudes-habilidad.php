<div class="card">
    <div class="card-header">
        Solicitudes de sugerencia
    </div>
    <div class="card-body" style="max-height: 400px; overflow: auto;">
        <?php if($solicitudes == null){ ?>
            <div class="text-center">
                <strong>Sin solicitudes de habilidades</strong>
            </div>
        <?php }else{ ?>
            <?php
                $db = \Config\Database::connect();
            ?>
            <?php foreach($solicitudes as $soli){ ?>
                <?php $usuario = model('Autenticacion')->get_id($soli['id_usuario'])?>
                <div class="border p-2 mb-3 bg-light bg-gradient">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-6">
                                    <label>Usuario: <?=$usuario['nombre']?> <?=$usuario['apellidos']?></label><br>
                                    <label>Tipo de usuario: <span class="text-capitalize"><?=$usuario['rol']?></span></label>
                                </div>
                                <div class="col-6">
                                    <label>Fecha: <?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $soli['created_at']])?></label>
                                </div>
                            </div>
                            <hr>
                            <?php $contenido = json_decode($soli['contenido'], true);?>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <?php
                                        $builder = $db->table('habilidades');
                                        $builder->like('area', $contenido['nombre']);
                                        $data = $builder->get()->getResultArray();
                                        // d($data);
                                    ?>
                                    <?php if($data != null){ ?>
                                        <label>Resultados encontrados en:</label><br>
                                        <?php foreach($data as $d){ ?>
                                            <?php if($d['superior'] == '0'){ ?>
                                                <label>Area: <?=$d['area']?></label><br>
                                            <?php }else{ ?>
                                                <label>Area: <?=$d['superior']?></label><br>
                                                <label>Habilidad: <?=$d['area']?></label><br>
                                            <?php } ?>
                                            <hr>
                                        <?php } ?>
                                    <?php }?>
                                    <label>Solicitud</label><br>
                                    <label>Area / Habilidad: <strong><?=$contenido['nombre']?></strong></label><br>
                                    <label>Tipo: <strong><?=$contenido['tipo']?></strong></label><br>
                                    <?php if($contenido['tipo'] == 'habilidad'){ ?>
                                        <label>Area:&nbsp;&nbsp;&nbsp;<strong><?=$contenido['area']?></strong></label>
                                    <?php } ?>
                                </div>
                                <div class="col-12 col-md-6 text-center">
                                    <div class="d-grid gap-2">
                                        <button class="btn btn-primary aceptar-soli" data-id="<?=$soli['id']?>" type="button">Aceptar</button>
                                        <button class="btn btn-primary declinar-soli" data-id="<?=$soli['id']?>" type="button">Declinar</button>
                                        <button class="btn btn-primary notifi-exi-soli" data-id="<?=$soli['id']?>" type="button">
                                            Notificar area ya existente
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }?>
        <?php } ?>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-confirm-soli" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Añadir area / habilidad</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                ...
            </div>
        </div>
    </div>
</div>
<script>
    $('.aceptar-soli').click(function(){
        let id = $(this).attr('data-id');
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Administrador/editSoliArea')?>',
            data:{id:id},
            success: function(data){
                $('#modal-confirm-soli .modal-body').html(data);
                $('#modal-confirm-soli').modal('show');
            }, error: function(data){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo', 10);
            }
        });
    });
    $('.declinar-soli').click(function(){
        let id = $(this).attr('data-id');
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Administrador/declinarSoliArea')?>',
            data:{id:id},
            success: function(data){
                $('#solicitudes').load('<?=base_url('/Administrador/solicitudAreaHabilidad')?>');
                alertify.success('Solicitud declinada', 'correcto', 10);
            }, error: function(data){
                alertify.error('A surgido un error, comniquese con el equipo de desarrollo.', 10);
            }
        });
    });
    $('.notifi-exi-soli').click(function(){
        let id = $(this).attr('data-id');
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Administrador/notifiExistenteHabilidad')?>',
            data:{id:id},
            success: function(data){
                $('#solicitudes').load('<?=base_url('/Administrador/solicitudAreaHabilidad')?>');
                alertify.success('Se le ha notificado al usuario de la existencia del area.');
            }, error: function(data){
                alertify.error('A surgido un error, comniquese con el equipo de desarrollo.', 10);
            }
        });
    });
</script> 