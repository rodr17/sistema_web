<div class="card">
    <div class="card-header">
        Solicitudes de sugerencia
    </div>
    <div class="card-body" style="max-height: 400px; overflow: auto;">
        <?php if($solicitudes == null){ ?>
            <div class="text-center">
                <strong>Sin solicitudes de idioma</strong>
            </div>
        <?php }else{ ?>
            <?php foreach($solicitudes as $soli){ ?>
                <?php $usuario = model('Autenticacion')->get_id($soli['id_usuario'])?>
                <div class="border p-2 mb-3 bg-light bg-gradient">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-lg-9">
                            <div class="row">
                                <div class="col-6">
                                    <label>Usuario: <?=$usuario['nombre']?> <?=$usuario['apellidos']?></label><br>
                                    <label>Tipo de usuario: <span class="text-capitalize"><?=$usuario['rol']?></span></label>
                                </div>
                                <div class="col-6">
                                    <label>Fecha: <?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $soli['created_at']])?></label>
                                </div>
                            </div>
                            <hr>
                            <?php $contenido = json_decode($soli['contenido'], true);?>
                            <div class="row">
                                <div class="col-12 col-md-6 text-center">
                                    <label>Idioma: <strong><?=$contenido['nombre']?></strong></label><br>
                                    <?php if(!view_cell('App\Libraries\FuncionesAdmin::validaridioma', ['idioma' => $contenido['identificador']])){ ?>
                                        <label>Estatus:&nbsp;&nbsp;&nbsp;<i class="fa fa-circle text-danger" aria-hidden="true"></i> Ya registrado.</label>
                                    <?php }else{ ?>
                                        <label>Estatus:&nbsp;&nbsp;&nbsp;<i class="fa fa-circle text-info" aria-hidden="true"></i> No registrado.</label>
                                    <?php } ?>
                                </div>
                                <div class="col-12 col-md-6 text-center">
                                    <img src="<?=$contenido['bandera']?>" style="width: inherit;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-lg-3 align-self-center">
                            <div class="d-grid gap-2">
                                <?php if(view_cell('App\Libraries\FuncionesAdmin::validaridioma', ['idioma' => $contenido['identificador']])){ ?>
                                    <a class="btn btn-primary btn-sm add-idioma" data-id="<?=$soli['id']?>">
                                        Añadir idioma
                                    </a>
                                <?php }else{ ?>
                                    <a class="btn btn-primary btn-sm noti-idioma-reg" data-id="<?=$soli['id']?>">
                                        Notificar que ya fue agregado el idioma
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }?>
        <?php } ?>
    </div>
</div>
<script>
$('.add-idioma').click(function(){
    let id = $(this).attr('data-id');
    $.ajax({
        type: 'POST',
        url: '<?=base_url('/administrador/addIdiomaSolicitud')?>',
        data:{id:id},
        success: function(data){
            let cont = JSON.parse(data);
            if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
            alertify.success(cont.mensaje);
            $('#table-idiomas').load('<?=base_url('/administrador/tablaIdiomas')?>');
            $('#solicitudes-idioma').load('<?=base_url('/administrador/solicitudesIdioma')?>');
        }, error: function(data){
            alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
        }
    });
});
$('.noti-idioma-reg').click(function(){
    let id = $(this).attr('data-id');
    $.ajax({
        type: 'POST',
        url: '<?=base_url('/Administrador/notiSoliIdioma')?>',
        data:{id:id},
        success: function(data){
            let cont = JSON.parse(data);
            if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
            alertify.success(cont.mensaje, 10);
            $('#table-idiomas').load('<?=base_url('/administrador/tablaIdiomas')?>');
            $('#solicitudes-idioma').load('<?=base_url('/administrador/solicitudesIdioma')?>');
        }, error: function(data){
            alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
        }
    });
});
</script>