<table class="table">
	<thead>
		<tr>
			<th scope="col">ID</th>
			<th scope="col">Subárea</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($subareas as $a){ ?>
			<tr>
				<th scope="row"><?=$a['id']?></th>
				<td>
					<?=$a['area']?><br>
					<small>
						<a class="link-info edit-sub-area" data-idsubarea="<?=$a['id']?>" data-area="<?=$a['area']?>">
							Editar
						</a>
					</small>
					&nbsp;
					<small>
						<a class="link-danger eliminar-subarea" data-idsubarea="<?=$a['id']?>">
							Eliminar
						</a>
					</small>
					&nbsp;
				</td>
			</tr>
		<?php } ?>
	</tbody>
</table>
<script>
	$('.edit-sub-area').click(function(){
		$('#modalsubarea').modal('show');
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/habilidades/formEditSubarea')?>',
			data:{id:$(this).attr('data-idsubarea')},
			success: function(data){
				$('#sec-form-subarea').html(data);
			}, error: function(data){
				alertify.error('Ha surgido un error, comunquese con el equipo de desarrollo.', 10);
			}
		});
	});
	$('.eliminar-subarea').click(function(){
		let id = $(this).attr('data-idsubarea');
		alertify.confirm('ATENCION', 'Está a punto de eliminar una subarea, ¿está seguro de esto?', function(){
			$.ajax({
				type: 'POST',
				url: '<?=base_url('/habilidades/eliminar')?>',
				data:{id:id},
				success: function(data){
					let cont = JSON.parse(data);
					if(cont.tipo == 'error'){
						alertify.warning(cont.mensaje, 10);
					}else{
						alertify.success(cont.mensaje, 10);
						$('#subareas').html(cont.data);
					}
				}, error: function(data){
					alertify.error('A surgido un problema, comuníquese con el equipo de desarrollo.', 10);
				}
			});
		},function(){});
	});
</script>