<table class="table">
	<thead>
		<tr>
			<th scope="col">ID</th>
			<th scope="col">Área</th>
			<input class="disabled invisible" type="hidden" hidden id="area_superior">
		</tr>
	</thead>
	<tbody>
		<?php foreach($areas as $a){ ?>
			<tr>
				<th scope="row"><?=$a['id']?></th>
				<td>
					<?=$a['area']?><br>
					<small>
						<a class="link-info link-subareas text-decoration-none" data-area="<?=$a['area']?>">
							Ver (<?= view_cell('App\Libraries\FuncionesAdmin::cantidadSubAreas', ['area' => $a['area']]) ?>)
						</a>
					</small>
					&nbsp;
					<?php if($a['area'] != 'Otros'){ ?>
						<small>
							<a class="link-info edit-area text-decoration-none" data-area="<?=$a['area']?>" data-id="<?=$a['id']?>">Editar</a>
						</small>
						&nbsp;
						<small>
							<a class="link-danger eliminar-superior text-decoration-none" data-id="<?=$a['id']?>">Eliminar</a>
						</small>
					<?php } ?>
				</td>
			</tr>
		<?php } ?>
	</tbody>
</table>
<script>
	$('.edit-area').click(function(){
		$('#area').val($(this).attr('data-area'));
		$('#idareaedit').val($(this).attr('data-id'));
		$('#modalarea').modal('show');
	});
	$('.link-subareas').on('click', function(){
		let area = $(this).attr('data-area');
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Habilidades/versubareas')?>',
			data:{area:area},
			success: function(data){
				$('#area_superior').val(area);
				$('#subareas').html(data);
			}, error: function(data){
				alertify.error('NETWORK', 10);
			}
		});
	});
	$('.eliminar-superior').click(function(){
		let id = $(this).attr('data-id');
		alertify.confirm('ATENCION', 'Está a punto de eliminar el area superior con el ID = '+id+', todas las habilidades o sub areas seran movidas a otros, ¿Está seguro de esto?', function(){
			$.ajax({
				type: 'POST',
				url: '<?=base_url('/Habilidades/deleteSuperior')?>',
				data:{id:id},
				success: function(data){
					let cont = JSON.parse(data);
					if(cont.tipo == 'error'){
						alertify.warning(cont.mensaje, 10);
					}else{
						alertify.success(cont.mensaje, 10);
						$('#tabla-areas').load('<?=base_url('/habilidades/tablaAreas')?>');
					}
				}, error: function(data){
					alertify.error('Ha surgido un error, comuníquese el equipo de desarrollo.', 10);
				}
			});
		}, function(){});
	});
	$('#buscar-subareas').keyup(function (e) { 
		let buscar = $(this).val();
		let area = $('#area_superior').val();
		$.ajax({
			type: 'POST',
			url: '<?=base_url('Habilidades/versubareas')?>',
			data: {buscar_subarea:buscar, area:area},
			success: function(data){
				$('#subareas').html(data);
			}, error: function(data){
				alertify.error('Ha surgido un error, comuníquese con el equipo de desarrollo.', 10);
			}
		});
	});
</script>