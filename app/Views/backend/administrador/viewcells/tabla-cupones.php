<?php if($cupones == null){ ?>
	<div class="alert alert-dark" role="alert">
		Aún no se han creado cupones dentro de la plataforma.
	</div>
<?php }else{ ?>
	<div class="card bg-white">
		<table class="table m-0">
			<thead>
				<tr class="text-center">
					<th scope="col">ID</th>
					<th scope="col">Código</th>
					<th scope="col">Rol</th>
					<th scope="col">Tipo</th>
					<th scope="col">Canjeados</th>
					<th scope="col">Limite usos</th>
					<th scope="col">Fecha de vigencia</th>
					<th scope="col">Estatus</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($cupones as $c){ ?>
					<tr class="text-center align-middle">
						<th scope="row"><?=$c['id']?></th>
						<td class="text-start">
							<div class="">
								<?=$c['codigo']?>
							</div>
							<div class="py-2">
								<a class="link-primary mx-2 cursor btn-est-cupon" data-id="<?= $c['id'] ?>" data-name="<?=$c['codigo']?>">
									Estadisticas
								</a>
								<a class="link-secondary mx-2 cursor btn-edit-cupon" data-id="<?= $c['id'] ?>" data-name="<?=$c['codigo']?>">
									Editar
								</a>
								<a class="link-warning mx-2 cursor btn-desac-cupon" data-id="<?= $c['id'] ?>" data-name="<?=$c['codigo']?>" data-estado="<?= ($c['activo'])? 'activo' : 'no-activo' ?>">
									<?= ($c['activo'])? 'Desactivar' : 'Activar' ?>
								</a>
								<a class="link-danger cursor btn-elim-cupon" data-id="<?= $c['id'] ?>" data-name="<?=$c['codigo']?>">
									Eliminar
								</a>
							</div>
						</td>
						<td><?=$c['rol']?></td>
						<td><?=$c['tipo']?></td>
						<td>
						    <?= !empty($c['limite'])
						        ? model('UsosCupones')->selectCount('id_cupon')->where(['id_cupon' => $c['id'], 'canjeado' => true])->countAllResults()
						        : 'Ilimitado';
				            ?>
						</td>
						<td><?=!empty($c['limite']) ? $c['limite'] : 'Ilimitado' ?></td>
						<td><?=!empty($c['fecha_limite']) ? $c['fecha_limite'] : 'Ilimitado' ?></td>
						<td class="<?= ($c['activo'])? 'icono-sonar-activo' : 'icono-sonar-desactivado' ?>">
							<?= ($c['activo'])? '<i class="fa fa-circle" aria-hidden="true"></i> <span class="text-success fw-bold">Activo</span>' : '<i class="fa fa-circle" aria-hidden="true"></i> <span class="text-danger fw-bold">Desactivado</span>' ?>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="estadisticas-cupon" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header border-0">
					<h5 class="modal-title" id="staticBackdropLabel">Estadisticas del cupon <span id="nombre-cupon"></span></h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body border-0" id="sec-est-cupon">
				</div>
				<div class="modal-footer border-0">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="editar-cupon" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticEditarLabel" aria-hidden="true">
		<div class="modal-dialog" style="max-width: 650px;">
			<div class="modal-content">
				<div class="modal-header border-0">
					<h5 class="modal-title" id="staticEditarLabel">Editar cupón <span id="nombre-cupon-editar"></span></h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body border-0" id="sec-edit-cupon">
				</div>
			</div>
		</div>
	</div>
	<script>
		// Estadisticas del cupon
		$('.btn-est-cupon').click(function(){
			let id = $(this).attr('data-id');
			let codigo = $(this).attr('data-name');
			$('#nombre-cupon').html(codigo);
			$.ajax({
				type: 'POST',
				url: '<?= base_url('/Pagos/informacionCupon') ?>',
				data:{id:id},
				success: function(data){
					let cont = JSON.parse(data);
					if(cont.tipo == 'error') return alertify.warning(cont.mensaje);
					$('#sec-est-cupon').html(cont.mensaje);
					return $('#estadisticas-cupon').modal('show');
				}, error: function(data){
					let respuesta = JSON.parse(data.responseText);
					return alertify.error('Error: ' + respuesta.message + '\n' + respuesta.file);
				}
			});
		});
		// Editar cupon
		$('.btn-edit-cupon').click(function(){
			let id = $(this).attr('data-id');
			let codigo = $(this).attr('data-name');
			$.ajax({
				type: 'POST',
				url: '<?=base_url('/Pagos/editarCupon')?>',
				data:{id:id},
				success: function(data){
					let cont = JSON.parse(data);
					if(cont.tipo == 'error') return alertify.warning(cont.mensaje);
					
					$('#nombre-cupon-editar').html(codigo);
					$('#sec-edit-cupon').html(cont.mensaje);
					return $('#editar-cupon').modal('show');
				}, error: function(data){
					let respuesta = JSON.parse(data.responseText);
					alertify.error('Error: ' + respuesta.message + '\n' + respuesta.file);
				}
			});
		});
		// Desactivar cupon
		$('.btn-desac-cupon').click(function(){
			let id = $(this).attr('data-id');
			let codigo = $(this).attr('data-name');
			let estado = $(this).attr('data-estado');
			if(estado != 'activo'){
				$.ajax({
					url: '<?=base_url('/Pagos/dasctivarCupon')?>',
					type: 'POST',
					data: {id:id},
					success: function(data) {
						let cont = JSON.parse(data);
						if(cont.tipo == 'error') return alertify.warning(cont.mensaje);
						$('#tabla').load('<?=base_url('/Pagos/tablaCupones')?>');
						return alertify.success(cont.mensaje);
					}, error: function(data) {
						let respuesta = JSON.parse(data.responseText);
						alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
					}
				});
			}else{
				if (alertify.confirm('¡Atención!', '¿Estás seguro de desactivar el cupon <b>'+codigo+'</b>? Recuerda que podras activarlo nuevamente si asi lo deseas.', function() {
					$.ajax({
						url: '<?=base_url('/Pagos/dasctivarCupon')?>',
						type: 'POST',
						data: {id:id},
						success: function(data) {
							let cont = JSON.parse(data);
							if(cont.tipo == 'error') return alertify.warning(cont.mensaje);
							$('#tabla').load('<?=base_url('/Pagos/tablaCupones')?>');
							return alertify.success(cont.mensaje);
						}, error: function(data) {
							let respuesta = JSON.parse(data.responseText);
							alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
						}
					});
				}, function() {
				}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
			}
		});
		// Eliminar cupon
		$('.btn-elim-cupon').click(function(){
			let id = $(this).attr('data-id');
			let codigo = $(this).attr('data-name');
			if (alertify.confirm('¡Atención!', '¿Estás seguro de eliminar el cupon <b>'+codigo+'</b>?', function() {
				$.ajax({
					url: '<?=base_url('/Pagos/eliminarCupon')?>',
					type: 'POST',
					data: {id:id},
					success: function(data) {
						let cont = JSON.parse(data);
						if(cont.tipo == 'error') return alertify.warning(cont.mensaje);
						$('#tabla').load('<?=base_url('/Pagos/tablaCupones')?>');
						return alertify.success(cont.mensaje);
					}, error: function(data) {
						let respuesta = JSON.parse(data.responseText);
						alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
					}
				});
			}, function() {
			}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
		});
	</script>
<?php } ?>