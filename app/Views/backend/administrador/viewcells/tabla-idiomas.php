<table class="table">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Idioma</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($lenguajes as $l){ ?>
            <tr>
                <th>
                    # <?=$l['id']?>
                </th>
                <td>
                    <div class="">
                        <img src="<?=$l['bandera']?>" style="max-width: 40px;">&nbsp;<?=$l['nombre']?>
                    </div>
                    <a class="text-decoration-none cursor editar-idioma" data-id="<?=$l['id']?>" data-name="<?=$l['nombre']?>" data-img="<?=$l['bandera']?>">Editar</a>
                    &nbsp;
                    <a class="text-decoration-none cursor eliminar-idioma" data-id="<?=$l['id']?>">Eliminar</a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<script>
    $('.eliminar-idioma').click(function(){
        let id = $(this).attr('data-id');
        alertify.confirm('Atencion', 'Esta apunto de eliminar el idioma, ¿esta seguro de esto?', function(){
            $.ajax({
                type: 'POST',
                url: '<?=base_url('/Administrador/eliminarIdioma')?>',
                data:{id:id},
                success: function(data){
                    let cont = JSON.parse(data);
                    if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
                    $('#table-idiomas').load('<?=base_url('/administrador/tablaIdiomas')?>');
                    $('#solicitudes-idioma').load('<?=base_url('/administrador/solicitudesIdioma')?>');
                    return alertify.success(cont.mensaje, 10);
                }, error: function(data){
                    alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
                }
            });
        }, function(){});
    });
    $('.editar-idioma').click(function(){
        $('#edit-idioma').show();
        $('#nombre-edit').val($(this).attr('data-name'));
        $('#flag-edit').attr('src', $(this).attr('data-img'));
        $('#id-edit').val($(this).attr('data-id'));
    });
    
</script>