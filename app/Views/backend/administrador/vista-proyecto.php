<?=$this->extend('/backend/administrador/main')?>
<?=$this->section('title')?>
	<?=$proyecto['titulo']?>
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="container">
	<div class="p-2">
		<h4 class="titulo-label">Proyecto publicado</h4>
	</div>
	<hr>
	<div class="p-4 text-center">
		Estatus del proyecto:<br>
		<?php if($proyecto['estatus'] == 'espera'){ ?>
			<span class="fw-bold">Proyecto en espera</span>
		<?php }elseif($proyecto['estatus'] == 'desarrollo'){ ?>
			<span class="fw-bold">Proyecto en desarrollo</span>
		<?php }elseif($proyecto['estatus'] == 'finalizado'){ ?>
			<span class="fw-bold">Proyecto en finalizado</span>
		<?php }else{ ?>
			<span class="fw-bold">Proyecto cancelado</span>
		<?php } ?>
	</div>
	<div class="row">
		<div class="col-12 col-lg-4">
			<div class="card border-0 mb-3">
				<div class="card-body">
					<h2 class="card-title h5">Contratante</h2>
					<p class="card-text"><span class="fw-bold">Nombre: </span><?=$contratista['nombre']?> <?=$contratista['apellidos']?></p>
					<div class="text-center">
						<img src="<?= imagenPerfil($contratista) ?>" class="w-75" alt="...">
					</div>
					<div class="my-3">
						Correo: <a class="text-decoration-none" href="mailto:<?=$contratista['correo']?>"><?=$contratista['correo']?></a><br>
						Teléfono: <a class="text-decoration-none" href="tel:<?=isset($contratista['contacto']['telefono']) ? $contratista['contacto']['telefono'] : ''?>"><?=isset($contratista['contacto']['telefono']) ? $contratista['contacto']['telefono'] : ''?></a>
					</div>
				</div>
			</div>
			<?php if($proyecto['estatus'] != 'espera'){ ?>
				<div class="card border-0 mb-3">
					<div class="card-body">
						<h2 class="card-title h5">Freelancer asignado</h2>
						<p class="card-text"><span class="fw-bold">Nombre: <?=$freelancer['nombre']?> <?=$freelancer['apellidos']?></span></p>
						<div class="d-flex mb-3">Calificación: &nbsp;&nbsp;&nbsp;&nbsp;<?=view_cell('App\Libraries\FuncionesSistema::verCalificación', ['id' => $freelancer['id']])?></div>
						<div class="text-center">
							<img src="<?= imagenPerfil($freelancer) ?>" class="w-75" alt="...">
						</div>
						<div class="my-3">
							Correo: <a class="text-decoration-none" href="mailto:<?=$freelancer['correo']?>"><?=$freelancer['correo']?></a><br>
							Teléfono: <a class="text-decoration-none" href="tel:<?=isset($contratista['contacto']['telefono']) ? $contratista['contacto']['telefono'] : ''?>"><?=isset($contratista['contacto']['telefono']) ? $contratista['contacto']['telefono'] : ''?></a>
						</div>
						<div class="text-center">
							<a class="btn btn-primary" target="_blank" href="<?=base_url('/usuarios/prevista/'.base64_encode($freelancer['id']))?>">Ver perfil</a>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
		<div class="col-12 col-lg-8">
			<div class="form-floating mb-3">
				<input type="text" class="form-control" id="titulo" name="titulo" placeholder="..." value="<?=$proyecto['titulo']?>" disabled>
				<label for="titulo">Título del proyecto</label>
			</div>
			<div class="row mb-3">
				<div class="col-12 col-lg-6">
					<div class="form-floating">
						<textarea class="form-control" placeholder="Leave a comment here" id="descorta" name="descorta" style="height: 150px" disabled><?=$proyecto['desc_corta']?></textarea>
						<label for="descorta">Descripción corta</label>
					</div>
				</div>
				<div class="col-12 col-lg-6">
					<div class="form-floating">
						<textarea class="form-control" placeholder="Leave a comment here" id="descripcion" name="descripcion" style="height: 150px" disabled><?=$proyecto['descripcion']?></textarea>
						<label for="descripcion">Descripción</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-lg-4">
					<div class="form-floating mb-3">
						<input type="text" class="form-control" id="categoria" name="categoria" disabled value="<?=$proyecto['categoria']?>" placeholder="name@example.com">
						<label for="categoria">Categoría</label>
					</div>
				</div>
				<div class="col-12 col-lg-4">
					<div class="form-floating mb-3">
						<input type="text" class="form-control" id="subcategoria" name="subcategoria" disabled value="<?=$proyecto['subcategoria']?>" placeholder="name@example.com">
						<label for="subcategoria">Subcategoría</label>
					</div>
				</div>
				<div class="col-12 col-lg-4">
					<div class="form-floating mb-3">
						<input type="text" class="form-control" id="presupuesto" name="presupuesto" disabled value="$ <?=$proyecto['presupuesto']?>" placeholder="name@example.com">
						<label for="presupuesto">Presupuesto</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-lg-4 text-center mb-3">
					<img src="<?=base_url('assets/images/iconos/calendario.png')?>" class="w-50"><br><br>
					<span class="fw-bold">Fecha de creación:</span><br>
					<span class="fw-bold"><?= view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $proyecto['created_at']])?></span>
				</div>
				<div class="col-12 col-lg-4 text-center mb-3">
					<img src="<?=base_url('assets/images/iconos/calendario.png')?>" class="w-50"><br><br>
					<span class="fw-bold">Fecha de arranque:</span><br>
					<span class="fw-bold"><?= view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $proyecto['fecha_arranque_esperado']])?></span>
				</div>
				<div class="col-12 col-lg-4 text-center mb-3">
					<img src="<?=base_url('assets/images/iconos/calendario.png')?>" class="w-50"><br><br>
					<span class="fw-bold">Fecha de entrega:</span><br>
					<span class="fw-bold"><?= view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $proyecto['fecha_entrega']])?></span>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-12 col-lg-6 p-4">
					<?=view_cell('App\Libraries\FuncionesSistema::carruselProyecto', ['id' => $proyecto['id']])?>
				</div>
			</div>
			<div class="row justify-content-center m-3">
				<div class="col-12 col-lg-6 text-center">
					<?php if($proyecto['estatus'] == 'desarrollo' || $proyecto['estatus'] == 'finalizado'){ ?>
						<?=view_cell('App\Libraries\FuncionesSistema::barraProgreso', ['id' => $proyecto['id']])?>
					<?php }elseif($proyecto['estatus'] == 'cancelado'){ ?>
						<span class="fw-bold">Comentario de reporte sobre el freelance:</span><br>
						<?= view_cell('App\Libraries\FuncionesSistema::verReporte', ['id' => $proyecto['id']]) ?>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
</div>
<?=$this->endSection()?>