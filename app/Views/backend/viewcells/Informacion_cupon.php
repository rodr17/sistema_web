<h6 class="h6">Cupon aplicado</h6>
<?php if($tipo == 'Gratuitos') :?>
	<? $plan = model('ModelPlanes')->select('name', 'id_plan_mensual')->where('id_plan_mensual', $id_plan)->first();?>
	<label class>Cuenta <b class="fw-bold"><?=$plan['name']?></b> 1 mes gratis.</label>
	<label class="d-block text-capitalize mt-2">Fecha Inicio: <?=strftime('%d %B %G', strtotime(explode(' ', $fecha_inicio)[0]))?> </label>
	<label class="d-block text-capitalize fw-bold">Fecha Fin: <?=strftime('%d %B %G', strtotime(explode(' ', $fecha_fin)[0]))?> </label>
	<label class="d-block">Después de esta fecha seguirás teniendo tu cuenta gratuita. Anímate a obtener mejores beneficios con nuestros planes para ti</label>
<?php else :?>
	<? $cuponBD = model('UsosCupones')->select(['descuento', 'usos_cupones.updated_at', 'tipo', 'canjeado', 'fecha_limite'])
							->where(['usos_cupones.id_usuario' => $id_usuario])
							->join('cupones', 'cupones.id = usos_cupones.id_cupon')
							->orderBy('usos_cupones.id', 'DESC')->first();?>
	<label class="d-block text-start">Descuento del <b class="fw-bold"><?=$cuponBD['descuento']?>%</b> en tu segundo pago del plan.</label>
	<?php if($cuponBD['canjeado']) :?>
		<?$suscripcion = model('Suscripciones')->where(['id_usuario' => $id_usuario, 'tipo_suscripcion' => 'tarjeta'])->first();
		$plan = model('Suscripciones')->informacion_plan($suscripcion['id_plan']) ?>
		<label class="d-block text-start">Fecha a pagar con descuento <b class="fw-bold text-capitalize"><?=strftime('%d %B %G', strtotime(explode(' ', $suscripcion['fecha_fin'])[0]))?></b></label>
	<?php else :?>
		<? $dias_frecuencia = $plan['frecuencia'] == 'Mensual' ? '+ 1 month': '+ 1 year' ?>
		<label class="d-block text-start">Fecha a pagar con descuento <b class="fw-bold text-capitalize"><?= strftime('%d %B %G', strtotime(date('Y-m-d').$dias_frecuencia))?></b> comprando hoy mismo al precio <b class="fw-bold">total</b>.</label>
	<?php endif ;?>
		<?$monto_descuento = ((100-$cuponBD['descuento']) * $plan['precio']) / 100;?>
		<?$monto_descontar = ($cuponBD['descuento'] * $plan['precio']) / 100;?>
		<label class="d-block text-start">Monto a descontar <b class="fw-bold text-capitalize">$<?= number_format($monto_descontar, 2, '.', ',') ?> <small>MXN</small></b>.</label>
		<label class="d-block text-start">Total con descuento <b class="fw-bold text-capitalize">$<?= number_format($monto_descuento, 2, '.', ',') ?> <small>MXN</small></b>.</label>
	<?php if(!empty($cuponBD['fecha_limite'])) :?>
		<label class="d-block text-start">Fecha limite a usar <b class="fw-bold"><?=strftime('%d %B %G', strtotime($cuponBD['fecha_limite']))?>%</b>. Pasado este día no podrá utilizarse el descuento aún así se haya ingresado el cupón correctamente</label>
	<?php endif ;?>
<?php endif ;?>