<?php 
    $mensajes = model('Salas')->where('id', $postu['sala'])->first();
    if($mensajes['mensajes'] == null){
        $total = 0;
    }else{
        $d = json_decode($mensajes['mensajes'], TRUE);
        $total = $d[session('rol')];
    }
?>
<div class="tooltips">
    <a class="text-azul" data-sala="<?= $postu['sala'] ?>" href="<?= base_url('/usuarios/chat/'.$postu['sala']) ?>">
        <i class="fas fa-sms" style="font-size: x-large;border-radius: 50px;border-bottom: groove;padding: 8px;color: var(--color-azul);"></i></a>
    <span class="tooltiptext">Ingresar al chat</span>
</div>