<div id="contenido_calificaciones">
    <?php foreach($calificaciones as $c){ ?>
        <?php
            $proyecto = model('Proyectos')->where('id', $c['id_proyecto'])->first();
            ($proyecto == null)? $user = null : $user = model('Autenticacion')->get_id($proyecto['id_contratista']);
        ?>
        <!-- item start -->
        <div class="card-job-top">
            <div class="card-job-top--info">
                <h6 class="card-job-top--info-heading titulo-comentarios"><a href="#"><?= ($proyecto != null)? $proyecto['titulo'] : 'Proyecto no encontrado' ?></a></h6>
                <div class="row">
                    <div class="col-lg-12">
                        <span class="tiempo-comentarios"><i class="fi-rr-user"></i></span>
                        <span class="card-job-top--post-time text-sm"><strong><?= ($user != null)? $user['nombre'].' '.$user['apellidos'] : 'Usuaro no encontrado' ?></strong></span>
                        <span class="rating-calificacion-clientes">
                            <?= view('viewcells/estrellasPerfil', array('promedio' => $c['calificacion'], 'total' => $c['calificacion'])) ?>
                        </span>
                        <span class="tiempo-comentarios"><i class="fi-rr-clock"></i></span>
                        <span class="card-job-top--post-time text-sm">
                            <?=view_cell('App\Libraries\FuncionesSistema::diferenciaFechas', ['fecha' => $c['created_at']])?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-job-description mt-20">
            <?= $c['comentario'] ?>
        </div>
        <div class="card-job-bottom mt-25 separador-comentarios-clientes ">
            <div class="row">
                <div class="col-12">
                    <?php if($proyecto != null){ ?>
                        <a class="btn btn-tags-sm mb-10 mr-5">
                            <?= $proyecto['categoria'] ?>
                        </a>
                        <a class="btn btn-tags-sm mb-10 mr-5">
                            <?= $proyecto['subcategoria'] ?>
                        </a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- item end -->
    <?php } ?>
</div>