<ul class="list-group">
    <li class="list-group-item border-0 px-0 d-grid">
        <strong>Visitar perfil de freelancer</strong>
		<a class="btn px-0 text-font" target="_blank" href="<?= base_url('Usuarios/prevista/' . base64_encode($usuario['id'])) ?>">
            <div class="position-relative">
                <img alt="mextemps" src="<?= imagenPerfil($usuario) ?>" class="align-middle img-perfil-icon"/>
                <span class="sidebar-company"><?=$usuario['nombre'].' '.$usuario['apellidos']?></span>
            </div>
		</a>
	</li>
</ul>