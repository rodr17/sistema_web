<?php foreach ($pagos as $key => $pago) :?>
	<div class="col-lg-12">
		<div class="row align-items-center text-center inputs-form mb-4 m-0 bg-<?= str_replace(' ', '', $pago['estatus']) ?>">
			<?php $estatus = view_cell('App\Libraries\FuncionesSistema::EstatusPago', ['pago' => $pago])?>
			<h3 class="h5 text-white-old"><strong><?= $estatus ?></strong></h3>
			<?php if($estatus == 'En progreso'){ ?>
				<div class="w-100 text-center">El pago puede tardar hasta 48 horas en verse reflejado.</div>
			<?php } ?>
			<div class="col-md-6">
				<label class="text-font text-white-old"><?= strftime('%d/%b/%G', strtotime($pago['created_at'])) ?> <i class="far fa-calendar-alt"></i></label>
				<label class="text-font text-white-old fw-bold">Monto: $<?= number_format($pago['monto'], 2, '.', ',') ?></label><br>
				<label class="text-font2 text-white-old">
					Compra de membresia MEXTEMPS a través de <?= ucfirst($pago['tipo'])?>
				</label>
			</div>
			<div class="col-md-6">
				<div class="w-100 text-center">
					Folio de compra: <strong><?=view_cell('App\Libraries\FuncionesSistema::folioCompra', ['id' => $pago['id_compra']])?></strong>
				</div>
				<?php if($pago['tipo'] == 'efectivo'){ ?>
					<?php if($estatus != 'Cancelado'){ ?>
						<?= view_cell('App\Libraries\FuncionesSistema::mostrarArchivoPago', ['id' => $pago['id']]) ?>
					<?php } ?>
				<?php }else{ ?>
					<?= view_cell('App\Libraries\FuncionesSistema::mostrarArchivoPagoTarjeta', ['id' => $pago['id_compra'], 'tipo' => $pago['tipo']]) ?>
				<?php } ?>
				<?php if($pago['estatus'] == 'Activo'){ ?>
					<?php $solfactura = model('Facturas')->where('id_compra', $pago['id_compra'])->first();  $fechaBD = new DateTime($pago['created_at']); $fecha_a_Dia = clone $fechaBD;?>
					<?php if($solfactura == null){ ?>
						<?php $fecha_a_Dia->modify('last day of this month');  ?>
						<?php if(verificar_rangoFechas('01-'.$fechaBD->format('m-Y'), $fecha_a_Dia->format('d').'-'.$fechaBD->format('m-Y'), date('d-m-Y'))) :?>
							<a class="btn text-font text-white-old solicitar-factura" data-id="<?=$pago['id_compra']?>" data-user="<?=$user['id_cliente']?>">
								Solicitar factura <i class="far fa-share-square fw-bold" aria-hidden="true"></i>
							</a>
						<?php else :?>
							<label>Facturación: <span class="text-capitalize fw-bold text-naranja">Fuera de fecha limite</span></label>
						<?php endif ;?>
					<?php }else{ ?>
						<label>Facturación: <span class="text-capitalize fw-bold"><?=$solfactura['estatus']?></span></label>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	</div>
<?php endforeach; ?>
<script>
	$('.solicitar-factura').click(function(){
		let id = $(this).attr('data-id');
		$.ajax({
			type:'POST',
			url: '<?=base_url('/Pagos/datosFacturacionUsuario')?>',
			data:{id:id},
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, 'advertencia');
				
				$('#form-solicitud-factura').html(cont.mensaje);
				return $('#solicitar-factura').modal('show');
			}, error: function(data){
				alertify.notify('Ha surgido un error, comuníquese con el equipo de soporte.', 'falla');
			}
		});
	});
</script>