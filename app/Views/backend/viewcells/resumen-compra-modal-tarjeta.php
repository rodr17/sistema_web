<div class="row">
	<div class="col-12 align-self-center text-start">
		<strong class="h5">Plan <?=str_replace('Plan', '', $suscripcion)?> <?=$frecuencia?></strong>
	</div>
</div>
<div class="text-start">
	<small class="fst-italic descripcion-compra-paquete" style="font-size: 12px;">
		<?=$descripcion?>
	</small>
</div>
<hr>
<?php if($frecuencia == 'Mensual'){ ?>
	<div class="row">
		<div class="col-5 align-self-center text-start">
			Pago mensual
		</div>
		<div class="col-7 align-self-center text-end">
			$<?= number_format($precio, 2, '.', ',') ?> <small>MXN</small>
			<?php if($es_tarjeta) {
				$hoy = new DateTime();
				$hoy->modify('last day of this month');
				$dias_restantes = ($hoy->format('d') - date('d')) == 0 ? 1 : ($hoy->format('d') - date('d'));
			}?>
		</div>
	</div>
	<hr>
<?php }else{ ?>
	<div class="row">
		<div class="col-5 align-self-center text-start">
			Precio base
		</div>
		<div class="col-7 align-self-center text-end">
			$<?= number_format(($precio / 12), 2, '.', ',') ?> <small>MXN</small> x mes
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-5 align-self-center text-start">
			Pago anual
		</div>
		<div class="col-7 align-self-center text-end">
			$<?= number_format($precio, 2, '.', ',') ?> <small>MXN</small>
		</div>
	</div>
	<hr>
<?php } ?>
<div class="row">
	<div class="col-4 align-self-center text-start">
		Total
	</div>
	<div class="col-8 align-self-center text-end">
		<h6 class="fw-bold <?= ($frecuencia == 'Mensual') && $es_tarjeta ? 'text-decoration-line-through opacity-75' : '' ?>">
			$<?= number_format($precio, 2, '.', ',') ?> <small>MXN</small>
		</h6>
		<?php if(($frecuencia == 'Mensual') && $es_tarjeta): ?>
			<h5 class="fw-bold">
				$<?= number_format(round(($precio/30)*$dias_restantes), 2, '.', ',')?> <small>MXN</small>
			</h5>
		<?php endif ;?>
	</div>
</div>
<?php if(($frecuencia == 'Mensual') && $es_tarjeta): ?>
	<div class="row my-2">
		<div class="col-12 align-self-center">
			<small class="small fst-italic">El cobro total está facturado hasta el día <b class="fw-bold"><?= $hoy->format('d')?></b> del mes en curso.</small>
		</div>
	</div>
<?php endif ;?>