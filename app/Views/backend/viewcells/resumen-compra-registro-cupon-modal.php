<div class="row">
    <div class="col-12 align-self-center">
        <strong class="h4">Plan <?=str_replace('Plan', '', $plan['suscripcion'])?> <?=$plan['frecuencia']?></strong>
    </div>
</div>
<div>
    <small class="fst-italic descripcion-compra-paquete" style="font-size: 12px;">
        <?=$plan['descripcion']?>
    </small>
</div>
<hr>
<?php if($plan['frecuencia'] == 'Mensual'){ ?>
    <div class="row">
        <div class="col-5 align-self-center">
            Pago mensual
        </div>
        <div class="col-7 align-self-center text-end">
            $<?= number_format($plan['precio'], 2, '.', ',') ?> <small>MXN</small>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-5 align-self-center">
            Descuento del cupón
        </div>
        <div class="col-7 align-self-center text-end">
            30 días gratis
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-5 align-self-center">
            Próxima fecha de pago
        </div>
        <div class="col-7 align-self-center text-end">
            <?php setlocale(LC_TIME, 'es_ES.UTF-8'); ?>
            <span class="text-capitalize"><?=strftime('%d-%B-%Y', strtotime($hoy.' + 30 days'))?></span> por $<?= number_format($plan['precio'], 2, '.', ',') ?> <small>MXN</small>
        </div>
    </div>
    <hr>
<?php }else{ ?>
    <div class="row">
        <div class="col-5 align-self-center">
            Precio base
        </div>
        <div class="col-7 align-self-center text-end">
            $<?= number_format(($plan['precio'] / 12), 2, '.', ',') ?> <small>MXN</small> x mes
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-5 align-self-center">
            Descuento del cupón
        </div>
        <div class="col-7 align-self-center text-end">
            30 días gratis
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-5 align-self-center">
            Próxima fecha de pago
        </div>
        <div class="col-7 align-self-center text-end">
            <?php setlocale(LC_TIME, 'es_ES.UTF-8'); ?>
            <span class="text-capitalize"><?=strftime('%Y-%m-%d', strtotime($hoy.' + 30 days'))?></span> por $<?= number_format($plan['precio'], 2, '.', ',') ?> <small>MXN</small>
        </div>
    </div>
    <hr>
<?php } ?>
<div class="row">
    <div class="col-5 align-self-center">
        Total
    </div>
    <div class="col-7 align-self-center text-end">
        <h4 class="h4 fw-bold">
            $0.00 <small>MXN</small>
        </h4>
    </div>
</div>