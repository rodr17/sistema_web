<div class="row">
	<div class="col-12 align-self-center">
		<strong class="h4">Plan <?= str_replace('Plan', '', $suscripcion) ?> <?= $frecuencia ?></strong>
	</div>
</div>
<div>
	<small class="fst-italic descripcion-compra-paquete" style="font-size: 12px;">
		<?= $descripcion ?>
	</small>
</div>
<hr>
<div class="row">
	<div class="col-5 align-self-center">
		<?= $frecuencia == 'Mensual' ? 'Pago mensual' : 'Precio base' ?>
	</div>
	<div class="col-7 align-self-center text-end">
		<?php if ($frecuencia == 'Mensual') : ?>
			$<?= number_format($precio, 2, '.', ',') ?> <small>MXN</small>
		<?php else : ?>
			$<?= number_format(($precio / 12), 2, '.', ',') ?> <small>MXN</small> x mes
		<?php endif; ?>
	</div>
</div>
<hr>
<div class="row">
	<div id="seccion_info_cupon">
		<?php if(empty(session('cupon'))) :?>
		<div class="d-flex justify-content-start">
			<div class="d-inline-flex">
				<input class="border border-secondary text-font" onkeyup="mayus(this)" name="cupon" type="text" placeholder="Ingrese su cupón">
				<a class="btn btn-default mx-1 align-self-center validar-cupon">Validar</a>
			</div>
		</div>
		<?php else :?>
			<? $plan = model('Suscripciones')->informacion_plan($id_plan);
				echo view('backend/viewcells/Informacion_cupon', ['id_usuario' => session('id'), 'tipo' => 'Paga', 'plan' => $plan]);?>
		<?php endif ;?>
	</div>
</div>
<hr id="divisor-cupon">
<?php if ($frecuencia != 'Mensual') : ?>
	<div class="row">
		<div class="col-5 align-self-center">
			Pago anual
		</div>
		<div class="col-7 align-self-center text-end">
			$<?= number_format($precio, 2, '.', ',') ?> <small>MXN</small>
		</div>
	</div>
	<hr>
<?php endif; ?>
<div class="row">
	<div class="col-5 align-self-center">
		Total
	</div>
	<div class="col-7 align-self-center text-end">
		<h4 class="h4 fw-bold">
			$<?= number_format($precio, 2, '.', ',') ?> <small>MXN</small>
		</h4>
	</div>
</div>