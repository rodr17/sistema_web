<div class="row">
	<div class="col-12 align-self-center">
		<strong class="h4">Plan <?=str_replace('Plan', '', $suscripcion)?> <i><?=$frecuencia?></i></strong>
	</div>
</div>
<div>
	<small class="fst-italic descripcion-compra-paquete" style="font-size: 12px;">
		<?=$descripcion?>
	</small>
</div>
<hr>
<?php if($frecuencia == 'Mensual'){ ?>
	<div class="row">
		<div class="col-5 align-self-center">
			Pago mensual
		</div>
		<div class="col-7 align-self-center text-end">
			$<?= number_format($precio, 2, '.', ',') ?> <small>MXN</small>
			<?php if($es_tarjeta) {
				$hoy = new DateTime();
				$hoy->modify('last day of this month');
				$dias_restantes = ($hoy->format('d') - date('d')) == 0 ? 1 : ($hoy->format('d') - date('d'));
			}?>
		</div>
	</div>
	<hr>
<?php }else{ ?>
	<div class="row">
		<div class="col-5 align-self-center">
			Precio base
		</div>
		<div class="col-7 align-self-center text-end">
			$<?= number_format(($precio / 12), 2, '.', ',') ?> <small>MXN</small> x mes
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-5 align-self-center">
			Pago anual
		</div>
	<div class="col-7 align-self-center text-end">
			$<?= number_format($precio, 2, '.', ',') ?> <small>MXN</small>
		</div>
	</div>
	<hr>
<?php } ?>
<div class="row">
	<?php if(!empty($monto_descuento) && (!empty($precio_total))) :?>
		<div class="col-5 align-self-center text-naranja">
			Descuento del cupón ($<?= number_format($monto_descuento, 2, '.', ',') ?>)
		</div>
		<div class="col-7 align-self-center text-end text-naranja">
			<label>- $<?= number_format($monto_descuento, 2, '.', ',') ?> <small>MXN</small></label>
		</div>
		<hr>
		<div class="col-5 align-self-center ">Total</div>
		<div class="col-7 align-self-center text-end">
			<h4 class="fw-bold">$<?= number_format($precio_total, 2, '.', ',')?> <small>MXN</small></h4>
		</div>
	<?php else :?>
		<div class="col-5 align-self-center ">
			Total
		</div>
		<div class="col-7 align-self-center text-end">
			<h4 class="fw-bold <?= (($frecuencia == 'Mensual') && $es_tarjeta) ? 'text-decoration-line-through opacity-75 h5' : '' ?>">
				$<?= number_format($precio, 2, '.', ',') ?> <small>MXN</small>
			</h4>
			<?php if(($frecuencia == 'Mensual') && $es_tarjeta) : ?>
				<h4 class="fw-bold">
					$<?= number_format(round(($precio/30)*$dias_restantes), 2, '.', ',')?> <small>MXN</small>
				</h4>
			<?php endif ;?>
		</div>
	<?php endif ;?>
</div>
<?php if(($frecuencia == 'Mensual') && $es_tarjeta): ?>
	<div class="row my-2">
		<div class="col-12 align-self-center">
			<small class="small fst-italic">El cobro total está facturado hasta el día <b class="fw-bold"><?= $hoy->format('d')?></b> del mes en curso.</small>
		</div>
	</div>
<?php endif ;?>