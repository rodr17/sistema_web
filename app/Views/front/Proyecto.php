<?php use App\Controllers\Suscripciones;?>
<?=$this->extend('front/main')?>
<?=$this->section('librerias')?>
	<link href="https://code.jquery.com/ui/1.12.1/themes/ui-darkness/jquery-ui.css" rel="stylesheet"/>
	<style type="text/css">
		.modal-backdrop.show {
			opacity: 0.75 !important;
		}
		.thumb-image{
			float:left;width:100px;
			position:relative;
			padding:5px;
		}
		.btn-siguiente{
			border: 1px solid rgba(6, 18, 36, 0.1);
			padding: 7px 15px;
			border-radius: 6px;
			background: #ffffff;
			color: #111112;
			font-size: 14px;
			-webkit-transition-duration: 0.2s;
			transition-duration: 0.2s;
		}
		.btn-siguiente:hover{
			color: #ffffff;
			background-color: #FE5000;
			-webkit-transition-duration: 0.2s;
			transition-duration: 0.2s;
			-webkit-transform: translateY(-3px);
			transform: translateY(-3px);
		}
		.publicar-proyecto-cont .card.shadow {
			border-radius: 30px;
		}
		.publicar-proyecto-cont input {
			height: 50px;
		}
		.publicar-proyecto-cont .progress {
			height: 50px;
		}
		.publicar-proyecto-cont .progress-bar {
			background-color: #FE5000;
		}
		.publicar-proyecto-cont textarea {
			min-height: 150px;
		}
		.publicar-proyecto-cont.images-proyecto input {
			height: 50px;
			font-size: 13px;
			padding-top: 9px;
			padding-left: 10px;
		}
		body{background-color: rgba(238, 247, 255, 1);}
	</style>
<?=$this->endSection()?>
<?=$this->section('title')?>
	Publicar Proyecto
<?=$this->endSection()?>

<?=$this->section('content')?>
<main class="container-fluid main publicar-proyecto-cont">
	<div class="container pb-5 pt-lg-5">
		<div class="row justify-content-center text-center py-5 py-lg-0">
			<section class="col-lg-10">
				<div class="card shadow p-4">
					<div class="card-header bg-transparent border-0 py-4">
						<h1 class="card-title h3 fw-bold">Publicar tu proyecto</h1>
					</div>
					<nav class="shadow-none pb-4 bg-transparent d-none" style="z-index: 5;">
						<div class="nav nav-tabs justify-content-around border-0" id="nav-tab" role="tablist" style="z-index: 2;">
							<button class="nav-link h3 fw-bold active" id="nav-cats-tab" style="z-index: -1;" data-bs-toggle="tab" data-bs-target="#nav-cats" type="button" role="tab" aria-controls="nav-cats" aria-selected="true"><span class="mx-1">1</span></button>
							<button class="nav-link h3 fw-bold" id="nav-titulo-tab" style="z-index: -1;" data-bs-toggle="tab" data-bs-target="#nav-titulo" type="button" role="tab" aria-controls="nav-titulo" aria-selected="false">2</button>
							<button class="nav-link h3 fw-bold" id="nav-galeria-tab" style="z-index: -1;" data-bs-toggle="tab" data-bs-target="#nav-galeria" type="button" role="tab" aria-controls="nav-galeria" aria-selected="false">3</button>
							<button class="nav-link h3 fw-bold" id="nav-final-tab" style="z-index: -1;" data-bs-toggle="tab" data-bs-target="#nav-final" type="button" role="tab" aria-controls="nav-final" aria-selected="false">4</button>
						</div>
					</nav>
					<form class="needs-validation" id="nuevo-proyecto" enctype="'multipart/form-data" novalidate>
						<div class="row">
							<div class="col-12 col-sm-12 col-lg-6 p-3 text-start">
								<div class="progress">
									<div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
								</div>
								<div class="pt-4 info-sec">
									<h3 class="h3">Comencemos conociendo un poco sobre tu proyecto.</h3>
									<p>Ingresa un título adecuado para que tu proyecto destaque para los candidatos adecuados para ti, además de brindarnos una descripción contándonos más sobre tu proyecto.</p>
								</div>
							</div>
							<div class="col-12 col-sm-12 col-lg-6 ">
								<div class="tab-content mt-3" id="nav-tabContent">
									<div class="tab-pane fade show active" id="nav-cats" role="tabpanel" aria-labelledby="nav-cats-tab">
										<div class="row justify-content-center">
											<div class="col-12">
												<div class="position-relative mb-3">
													<input class="inputs-form text-font" name="titulo" id="titulo" type="text" placeholder="Título del proyecto" required>
													<div class="invalid-tooltip text-font">Requerido</div>
												</div>
												<div class="position-relative mb-3">
													<textarea class="inputs-form tarea text-font" name="corta" id="corta" placeholder="Descripción breve de su proyecto" rows="2" required></textarea>
													<span class="d-block text-end" id="contador">0/200</span>
													<div class="invalid-tooltip text-font">Requerido</div>
												</div>
												<div class="position-relative mb-3">
													<textarea class="inputs-form text-font" name="descripcion" id="descripcion" placeholder="Descripción detallada del proyecto" required></textarea>
													<div class="invalid-tooltip text-font">Requerido</div>
												</div>
											</div>
											<div class="col-12 my-4">
												<button class="btn btn-siguiente" data-progess="50%" data-sig="nav-titulo-tab" data-tipo="siguiente" type="submit" data-tab="1">
													Siguiente <i class="fas fa-angle-right"></i>
												</button>
											</div>
										</div>
									</div>
									<div class="tab-pane fade" id="nav-titulo" role="tabpanel" aria-labelledby="nav-titulo-tab">
										<div class="row">
											<div class="col-12 mb-3">
												<div class="form-group select-style select-style-icon position-relative">
            										<select class="form-control form-icons select-active select-hidden-accessible" id="datalist_area" name="area" tabindex="-1" aria-hidden="true" required>
            											<option selected>Seleccione un área</option>
            											<?php foreach ($habilidades as $key => $a) : ?>
            												<option value="<?= $a['area'].' - '.$a['superior'] ?>"><?= $a['area'].' - '.$a['superior'] ?></option>
            											<?php endforeach; ?>
            										</select>
            										<i class="fi fi-rr-list"></i>
            										<div class="invalid-tooltip text-font2" id="inv-area">Requerido</div>
												</div>
											</div>
											<div class="col-12 position-relative mb-3">
												<div class="d-flex justify-content-between gap-4 align-items-center">
    												<div class="tooltips"><i style="color: #003B71; font-size:17px;" class="fas fa-info-circle info_proyecto" aria-hidden="true"></i>
    													<span class="tooltiptext" style="top: -20px; left: 25px;">El nivel de experiencia de un freelancer es determinado por la cantidad de proyectos terminados</span>
    												</div>
    												<div class="form-group select-style select-style-icon w-100 mb-0">
    													<select class="form-control form-icons select-active select-hidden-accessible" id="listaNiveles" name="nivel_requerido" data-select-id="1" tabindex="-1" aria-hidden="true" required>
    														<option selected>Seleccione un nivel requerido</option>
    														<option value="0">Básico</option>
    														<option value="1">Intermedio</option>
    														<option value="2">Avanzado</option>
    														<option value="3">Experto</option>
    														<option value="4">Platino</option>
    														<option value="5">Diamante</option>
    													</select>
    													<i class="fi fi-rr-list"></i>
    													<div class="invalid-tooltip text-font2" id="inv-nivel">Requerido</div>
    												</div>
												</div>
											</div>
											<div class="col-12 my-4">
												<button class="btn btn-siguiente" data-progess="25%" data-sig="nav-cats-tab" data-tipo="regresar" type="submit" data-tab="0">
													<i class="fas fa-angle-left"></i> Regresar
												</button>
												<button class="btn btn-siguiente" data-progess="75%" data-sig="nav-galeria-tab" data-tipo="siguiente" type="submit" data-tab="2">
													Siguiente <i class="fas fa-angle-right"></i>
												</button>
											</div>
										</div>
									</div>
									<div class="tab-pane fade" id="nav-galeria" role="tabpanel" aria-labelledby="nav-galeria-tab">
										<div class="row">
											<div class="col-12 p-0">
												<div id="wrapper images-proyecto">
													<input class="p-2" id="fileUpload" name="galeria[]" multiple="multiple" accept=".jpg, .jpeg, .png, .webp, .bmp" type="file"/> 
													<div class="d-flex flex-wrap" id="image-holder"></div>
												</div>
												<label class="fst-italic text-start d-block small mb-0">* Extensiones de archivo permitidas: jpg, jpeg, png, webp, bmp.</label>
												<label class="fst-italic text-start d-block small mb-0">* Tamaño máximo de archivos: 4MB.</label>
												<label class="fst-italic text-start d-block small mb-0">* Hasta 5 archivos máximo.</label>
											</div>
											<div class="col-12 my-4">
												<button class="btn btn-siguiente" data-progess="50%" data-sig="nav-titulo-tab" data-tipo="regresar" type="submit" data-tab="1">
													<i class="fas fa-angle-left"></i> Regresar
												</button>
												<button class="btn btn-siguiente" data-progess="100%" data-sig="nav-final-tab" data-tipo="siguiente" type="submit" data-tab="3">
													Siguiente  <i class="fas fa-angle-right"></i>
												</button>
											</div>
										</div>
									</div>
									<div class="tab-pane fade " id="nav-final" role="tabpanel" aria-labelledby="nav-final-tab">
										<div class="row justify-content-center">
											<div class="col-md-10">
												<div class="position-relative mb-3">
													<label for="arranque" class="form-label">Fecha estimada de inicio del proyecto</label>
													<input type="date" id="startDate" name="arranque" min="<?=date('Y-m-d')?>">
												</div>
												<div class="position-relative mb-3">
													<label for="fecha" class="form-label">Fecha de entrega estimada</label>
													<input type="date" id="endDate" name="fecha" min="<?=date('Y-m-d')?>">
													<div class="invalid-tooltip text-font">Requerido</div>
												</div>
												<div class="position-relative mb-3">
													<input class="inputs-form text-font numerico my-1" type="text" placeholder="Presupuesto para este proyecto" id="presupuesto" name="presupuesto"></input>
													<div class="invalid-tooltip text-font">Requerido</div>
												</div>
												<div class="position-relative mb-3">
													<label for="con_presupuesto" class="d-block text-start opciones-ingresar mb-0">
														<input class="form-check-input" type="checkbox" id="con_presupuesto" name="toggle_presupuesto" style="height: 1em; margin-top: 0.4em;">
														Presupuesto confidencial
													</label>
													<div class="invalid-tooltip text-font">Requerido</div>
												</div>
											</div>
											<div class="col-12 my-4">
												<button class="btn btn-siguiente" data-sig="nav-galeria-tab" data-tipo="regresar" data-progess="75%" data-tab="0" type="submit">
													<i class="fas fa-angle-left"></i>  Regresar
												</button>
												<a class="btn btn-primary btn-publicar" data-bs-toggle="modal" data-bs-target="#modalpublicar">
													Publicar <i class="fas fa-check-circle"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="p-3">
							<div id="resultado"></div>
						</div>
					</form>
				</div>
			</section>
		</div>
	</div>
</main>
<!-- ***************************************** PROYECTO PUBLICADO Y LLEGO A SU LIMITE ************************************* -->
<div class="modal fade" id="publicado-limite" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo-limite" aria-modal="true" role="dialog">
	<div class="modal-dialog modal-dialog-centered" style="min-height: min-content;">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0 pt-4">
				<h5 class="modal-title fw-bold h4" id="titulo-limite">¡Felicidades!</h5>
			</div>
			<div class="modal-header justify-content-center border-bottom-0 p-0">
				<h6 class="modal-title fw-bold h5">
					Tu proyecto fue publicado correctamente.
				</h6>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="text-center">
						<p id="mensaje-respuesta">Al parecer has llegado a tu limite de proyectos, si deseas publicar un nuevo proyecto te invitamos obtener un plan <strong>PREMIUM</strong>.</p>
					</div>
					<div class="row">
						<div class="col-12 text-center py-2">
							<a class="btn btn-border" href="<?=base_url('/usuarios/perfil')?>">
								Ir a Panel
							</a>
							<a class="btn btn-primary" href="<?=base_url('/tarifas-contratista')?>">
								Ir a Planes
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- *************************************************** PROYECTO PUBLICADO *********************************************** -->
<div class="modal fade" id="proyecto-publicado" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo-exitoso" aria-modal="true" role="dialog">
	<div class="modal-dialog modal-dialog-centered" style="min-height: min-content;">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0 pt-4">
				<h5 class="modal-title fw-bold h4" id="titulo-exitoso">¡Felicidades!</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-header justify-content-center border-bottom-0 pt-2">
				<h6 class="modal-title fw-bold h5">Tu proyecto fue publicado correctamente</h6>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row text-center">
						
					</div>
					<div class="row">
						<div class="col-12 text-center py-2">
							<a class="btn btn-border" href="<?=base_url('/usuarios/perfil')?>">Ir a Panel</a>
							<a class="btn btn-primary" id="publicar-otro" data-bs-dismiss="modal" aria-label="Close">¿Publicar otro proyecto?</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- **************************************************** COMO PUBLICAR PROYECTO ****************************************** -->
<div class="modal fade" id="modalpublicar" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="como-publicar" aria-modal="true" role="dialog">
	<div class="modal-dialog modal-dialog-centered" style="max-width: 650px; min-height: min-content;">
		<div class="modal-content" style="background-color: #00000000; border: 0;">
			<div class="modal-header justify-content-center border-bottom-0 pt-4">
				<h5 class="modal-title fw-bold h4 text-white" id="como-publicar">¿Cómo desea publicar su proyecto?</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body burbuja">
				<div class="row" style="min-height: 200px;">
					<div class="col-12 col-sm-12 col-md-6 mt-15">
						<div class="text-center donde-imagen" style="border: 1px solid #dddddd;border-radius: 30px;padding: 20px;min-height: 450px;background: #fff;">
							<span class="medida-imagenes" style="border-radius: 100px;width: 200px;height: 200px;display: flex;overflow: hidden;margin: 0 auto;"><img style="object-fit: cover;" src="https://img.freepik.com/foto-gratis/atractiva-joven-trabajadora-independiente-enfocada-que-navega-sitios-web-contratacion-trabajo-publica-su-curriculum-busca-nuevos-proyectos-clientes_343059-426.jpg?w=1380&amp;t=st=1663708301~exp=1663708901~hmac=53e8dc66000ec9848a840684c8dd873d19164510aeef23fc20acc61ca457cf8c" width="300"></span>
							<p style="padding-top: 20px;line-height: 20px;min-height: 140px;">Publica rápidamente tu proyecto para que cualquier freelancer pueda postularse a el, después de publicar te llevaremos a tu panel de usuario.</p>
							<div class="tooltips" style="margin: 20px 0 0 0;">
								<a id="publicar" class="btn btn-naranja btn-border">Solo Publicar</a>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-6 mt-15">
						<div class="text-center donde-imagen" style="border-radius: 30px;border: 1px solid #dddddd;padding: 20px;min-height: 450px;background: #fff;">
							<span class="medida-imagenes" style="border-radius: 100px;width: 200px;height: 200px;display: flex;overflow: hidden;margin: 0 auto;"><img style="object-fit: cover;" src="https://img.freepik.com/foto-gratis/vista-superior-equipo-companeros-trabajo-trabajando-oficina_329181-12055.jpg?w=1380&amp;t=st=1663708228~exp=1663708828~hmac=d5c9a721a76485a967197575efa99db716f03474414a92a7a8d8e6a10f44794f" width="300"></span>
							<p style="line-height: 20px;padding-top: 20px;min-height: 140px;">Enviar solicitudes a freelancees para aceptar desarrollar su proyecto, de igual manera se publicará para que otros freelancers puedan postularse al proyecto.</p>
							<div class="tooltips" style="margin: 20px 0 0 0;">
								<button class="btn btn-naranja btn-border" id="solicitud-publicar">Solicitar y publicar</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- **************************************************** SOLICITUD Y PUBLICAR ****************************************** -->
<div class="modal fade" id="solicitudmodal" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="mandar-solicitud" aria-modal="true" role="dialog">
	<div class="modal-dialog modal-dialog-centered modal-lg" style="min-height: min-content;">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0 pt-4">
				<h5 class="modal-title fw-bold h4" id="mandar-solicitud">Enviar solicitud a Freelancers</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body">
				<div class="p-3">
					<div class="row justify-content-center">
						<div class="col-12 col-sm-12 col-lg-10">
							<label for="search-freelance">Solicitudes disponibles: <b class="fw-bold" id="cont_solicitudes_diponibles"><?=($solicitudes_disponibles > 2000)? 'Ilimitadas' : $solicitudes_disponibles ?></b></label>
							<input class="form-control" id="search-freelance" placeholder="Buscar por: Nombre o correo">
						</div>
						<div class="col-12 col-sm-12 col-lg-8">
							<div class="dropdown w-100">
								<ul class="dropdown-menu p-0 w-100" id="lista-freelance" >
								</ul>
							</div>
						</div>
						<div class="p-3"></div>
						<div class="col-12 col-sm-12 col-lg-10">
							<div id="free-seleccionados" class="row">
							</div>
							<input type="hidden" id="free-select">
						</div>
						<div class="col-12 p-3 text-center">
							<a class="btn btn-naranja" id="solicitudes-publicar">
								Publicar y enviar solicitudes
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '< Ant',
		nextText: 'Sig >',
		currentText: 'Hoy',
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
		weekHeader: 'Sm',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	};
	$.datepicker.setDefaults($.datepicker.regional['es']);
	$(document).ready(function(){
		var longitudMax = 200;
		$('#contador').html(`${$('#corta').val().length}/${longitudMax}`);

		$('#corta').on('input', function(e) {
			const longitudAct = $(this).val().length;

			$(this).attr('maxlength', longitudMax);
			$('#contador').html(`${longitudAct}/${longitudMax}`);
		});
		
		$('#startDate').change(function(e){
			if($('#endDate').val() < $(this).val()) $('#endDate').val($(this).val());
			$('#endDate').attr('min', $(this).val());
		});

		$('#presupuesto').mask('#,##0.00', {reverse: true});
	});
	$('#publicar-otro').click(function(){
		$('#nuevo-proyecto')[0].reset();
		$('#nav-cats-tab').click();
		$('.progress-bar').attr('aria-valuenow', '25%').css('width', '25%').html('25%');
	});
	$('#proyecto-publicado .cerrar-modal').click(function(){
		$('#publicar-otro').click();
	});
	$('.btn-siguiente').click(function(evento){
		let tab = $(this).attr('data-tab');
		if(tab == 1){
			if(evento.currentTarget.form.titulo.value.length == 0 || evento.currentTarget.form.descripcion.value.length == 0){ 
				return true;
			}
		}else if(tab == 2){
			if($('#datalist_area').val() == 'Seleccione un área'){
				$('#inv-area').show();
				return evento.preventDefault();
			}else{
				$('#inv-area').hide();
			}
			
			
			if($('#listaNiveles').val() == 'Seleccione un nivel requerido'){
				$('#inv-nivel').show();
				return evento.preventDefault();
			}
			else{
				$('#inv-nivel').hide();
			}
		}
		evento.preventDefault();
		let tipo = $(this).attr('data-tipo');
		let btn = $(this).attr('data-sig');
		$('#nuevo-proyecto').removeClass('was-validate');
		$('#'+btn).click();
		let nivel = $(this).attr('data-progess');
		$('.progress-bar').attr('aria-valuenow', nivel).css('width', nivel).html(nivel);
		if(nivel == '50%'){
			$('.info-sec h3').html('Seleccione la categoría para su proyecto.');
			$('.info-sec p').html('Díganos en que categoría se encuentra su proyecto, de esta manera podremos encontrar al freelance ');
		}else if(nivel == '25%'){
			$('.info-sec h3').html('Comencemos conociendo un poco sobre tu proyecto.');
			$('.info-sec p').html('Ingresa un título adecuado para que tu proyecto destaque para los candidatos adecuados para ti, además de brindarnos una descripción contándonos más sobre tu proyecto.');
		}else if(nivel == '75%'){
			$('.info-sec h3').html('Continuemos con más información para tu proyecto.');
			$('.info-sec p').html('Compártenos algunas imágenes de muestra para conocer de mejor manera de tu proyecto y ser más asertivo para conectarte con los freelancers ideales para ti.');
		}else if(nivel == '100%'){
			$('.info-sec h3').html('Ya casi finalizamos.');
			$('.info-sec p').html('Cuéntenos si tiene fecha entrega su proyecto así como el presupuesto, esto nos ayudará a emparejarlo con los freelancers ideales para tu proyecto.');
		}
	});
	$("html").click(function() {
		$('#lista-freelance').removeClass('d-block');
	});
	$('#caja').click(function (e) {
		e.stopPropagation();
	});
	$('#search-freelance').keyup(function(){
		let search = $(this).val();
		let frees = $('#free-select').val();
		console.log(frees);
		$('#lista-freelance').addClass('d-block');
		if(search.length == 0) return console.log('Sin texto');
		
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Proyectos/buscadorFreelance')?>',
			data:{search:search, frees:frees},
			success: function(data){
				$('#lista-freelance').html(data);
			}, error: function(data){
				alertify.notify('Contáctese con el área de soporte técnico.', 'falla', 10);
			}
		});
	});
	$('#solicitud-publicar').click(function(){
		$('#modalpublicar').modal('hide');
		$('#solicitudmodal').modal('show');
	});
	$('#con_presupuesto').change(function(e){
		if($(this).prop('checked')) $('#presupuesto').val('').attr({'disabled': true, 'placeholder': 'Confidencial'});
		else $('#presupuesto').removeAttr('disabled').attr('placeholder', 'Presupuesto para este proyecto');
	});
	$('#publicar').click(function(evento){
		$(this).attr('disabled', true);
		$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);
		
		var form = $('#nuevo-proyecto')[0];
		var formData = new FormData(form);
		$.ajax({
			type: "POST",
			method: "POST",
			url: "<?=base_url('/Proyectos/crear')?>",
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function(data) {
				$('#modalpublicar').modal('hide');
				let respuesta = JSON.parse(data);
				if(respuesta.alerta != 'correcto'){
					$('#publicar').removeAttr('disabled').children('span').remove();
					if(respuesta.alerta == 'modal'){
						$('#body-alerta').html(respuesta.mensaje);
						return $('#aumentar-membresia').modal('show');
					}
					$('#'+respuesta.seccion).click();
					return alertify.notify(respuesta.mensaje, respuesta.tipo, 10);
				}
				$.ajax({
					type: 'POST',
					url: '<?=base_url('/Home/validarCantProyectos')?>',
					data:{id:<?=session('id')?>},
					success: function(data){
						let respuesta = JSON.parse(data);
						if(respuesta.tipo == 'error'){
							return alerify.notify(respuesta.mensaje, 'advertencia');
						}else if(respuesta.tipo == 'false'){
							$('#mensaje-respuesta').html(respuesta.mensaje);
							$('.inputs-form').prop('readonly', true);
							$('.btn.btn-siguiente').remove();
							$('.btn-publicar').remove();
							return $('#publicado-limite').modal('show');
						}else{
							return $('#proyecto-publicado').modal('show');
						}
					}, error: function(data){
						let respuesta = JSON.parse(data.responseText);
						return alertify.error('Error: ' + respuesta.message + '\n' + respuesta.file);
					}
				});
				$('#publicar').removeAttr('disabled').children('span').remove();
				return alertify.notify(respuesta.mensaje, 'correcto', 10);
			}, error: function(data) {
				return alertify.notify('Error: '+data.responseText, 'falla', 10);
			}
		});
	});
	$('#solicitudes-publicar').click(function(){
		$(this).addClass('disabled');
		$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);
		var form = $('#nuevo-proyecto')[0];
		var formData = new FormData(form);
		formData.append('frees', $('#free-select').val());
		$.ajax({
			type: 'POST',
			method: 'POST',
			url: '<?=base_url('/Proyectos/crearSolicitudes')?>',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function(data){
				let respuesta = JSON.parse(data);
				if(respuesta.tipo != 'correcto'){
					if(respuesta.seccion == 'modal'){
						alertify.notify(respuesta.mensaje, respuesta.tipo, 10, 10);
						$('#solicitudes-publicar').removeClass('disabled').children('span').remove();
					}else if(respuesta.tipo == 'modalsus'){
						$('#body-alerta').html(respuesta.mensaje);
						return $('#aumentar-membresia').modal('show');
					}else{
						$('#solicitudmodal').modal('hide');
						$('#'+respuesta.seccion).click();
						alertify.notify(respuesta.mensaje, respuesta.tipo, 10, 10);
						$('#solicitudes-publicar').removeClass('disabled').children('span').remove();
					}
				}else{
					$.ajax({
						type: 'POST',
						url: '<?=base_url('/Home/validarCantProyectos')?>',
						data:{id:<?=session('id')?>},
						success: function(data){
							let respuesta = JSON.parse(data);
							if(respuesta.tipo == 'error'){
								return alerify.notify(respuesta.mensaje, 'advertencia');
							}else if(respuesta.tipo == 'false'){
								$('#mensaje-respuesta').html(respuesta.mensaje);
								$('.inputs-form').prop('readonly', true);
								$('.btn.btn-siguiente').remove();
								$('.btn-publicar').remove();
								return $('#publicado-limite').modal('show');
							}else{
								$('#free-seleccionados').html('');
								$('#free-select').val('');
								$('#solicitudes-publicar').removeClass('disabled').children('span').remove();
								$('#solicitudmodal').modal('hide');
								return $('#proyecto-publicado').modal('show');
							}
						}, error: function(data){
							let respuesta = JSON.parse(data.responseText);
							alertify.error('Error: ' + respuesta.message + '\n' + respuesta.file);
						}
					});
				}
			}
		});
	});
	$("#fileUpload").on('change', function(evento) {
		let archivos = event.target.files;
		for (let i = 0; i < archivos.length; i++) {
			let archivo = archivos[i];
			console.log(archivo.name);
			if (archivo.size > 4194304) {
				$(this).val("");
				return alertify.notify("OPPS... La imagen '"+archivo.name+"' es muy pesada, solo puedes enviar como máximo 4MB.", 'advertencia', 10);
			}
		}
		if($(this)[0].files.length > 5){
			alertify.notify('HEEY... No puedes cargar más de 5 imagenes para tu proyecto.', 'advertencia', 10);
			$("#fileUpload").val(null);
			$('#image-holder').html('');
		}else{
			//Get count of selected files
			var countFiles = $(this)[0].files.length;
			var imgPath = $(this)[0].value;
			var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
			var image_holder = $("#image-holder");
			image_holder.empty();
			if ($(this)[0].size > 4194304) {
				$(this).val("");
				return alertify.notify("OPPS... La imagen es muy pesada, solo puedes enviar como máximo 4MB.", 'advertencia', 10);
			}
			if (extn == "png" || extn == "jpg" || extn == "jpeg" || extn == "webp") {
				if (typeof(FileReader) != "undefined") {
					//loop for each file selected for uploaded.
					for (var i = 0; i < countFiles; i++) {
						var reader = new FileReader();
						reader.onload = function(e) {
							$("<img />", {
								"src": e.target.result,
								"class": "thumb-image"
							}).appendTo(image_holder);
						}
						image_holder.show();
						reader.readAsDataURL($(this)[0].files[i]);
					}
				} else {
					alertify.notify("Su navegador no soporta la funcion de vista previa.", 'falla', 10);
					$("#fileUpload").val(null);
					$('#image-holder').html('');
				}
			} else {
				alertify.notify("HEEY... Solo acepto archivos de imágenes, en esta sección", "advertencia", 10);
				$("#fileUpload").val(null);
				$('#image-holder').html('');
			}
		}
	});
</script>
<?=$this->endSection()?>