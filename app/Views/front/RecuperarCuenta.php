<!doctype html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
        <!--Google Autenticacion-->
        <meta name="google-signin-scope" content="profile email">
        <meta name="google-signin-client_id" content="198033966029-a75kfv97som76od1s41e6gr26po9ppvh.apps.googleusercontent.com">
        <!--jQuery-->
        <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
        <!-- <script src="<base_url('assets/themes/js/vendor/jquery-3.6.0.min.js')?>"></script> -->
        <!--Script para mostrar los iconos-->
        <script defer src="https://kit.fontawesome.com/d47f1b7d62.js" crossorigin="anonymous"></script>
        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous"> -->
        <!-- Bootstrap Scripts-->
        <script defer src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
        <!--Libreria para telefonos-->
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
        <!--Alertify-->
        <link href="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>
        <script defer src="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>
        <!--Google RECAPTCHA-->
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <!--Google maps-->
        <script defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAc3TX0tSUMk0psMUFxQBwkdqVzgxQJSgY"></script>
        <!--Datetimepickers-->
        <script defer src="<?=base_url('/assets/librerias/datetimepicker/jquery.datetimepicker.full.js')?>"></script>
        <script defer src="<?=base_url('/assets/librerias/datetimepicker/jquery.datetimepicker.full.min.js')?>"></script>
        <script defer src="<?=base_url('/assets/librerias/datetimepicker/jquery.datetimepicker.min.js')?>"></script>
        <link rel="stylesheet" type="text/css" href="<?=base_url('/assets/librerias/datetimepicker/jquery.datetimepicker.min.css')?>">
        <!--OpenPay-->
        <script defer type="text/javascript" src="<?=base_url('assets/js/openpay.v1.min.js')?>"></script>
        <script defer type='text/javascript' src="<?=base_url('assets/js/openpay-data.v1.min.js')?>"></script>
        <!--Hoja de estilos-->
        <script src="<?=base_url('assets/themes/js/vendor/jquery-migrate-3.3.0.min.js')?>"></script>
        <link rel="stylesheet" type="text/css" href="<?=base_url('/assets/css/style.css')?>">
        <!--Estilos del tema-->
        <link rel="stylesheet" href="<?=base_url('/assets/themes/css/plugins/animate.min.css')?>">
        <link rel="stylesheet" href="<?=base_url('/assets/themes/css/main.css')?>?v=1.0">
        <!-- Template CSS -->
        <!-- <link rel="stylesheet" href="assets/css/plugins/animate.min.css" /> -->
        <!-- <link rel="stylesheet" href="assets/css/main.css?v=1.0" /> -->
        <script src="https://cdn.lordicon.com/xdjxvujz.js"></script>
        <title>Recuperar Cuenta&nbsp;-&nbsp;MEXTEMPS</title>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v13.0&appId=273149384935534&autoLogAppEvents=1" nonce="L4gxvrCc"></script>
    </head>
    <body>
        <div id="preloader-active">
            <div class="preloader d-flex align-items-center justify-content-center">
                <div class="preloader-inner position-relative">
                    <div class="text-center">
                        <img src="<?=base_url('assets/themes/imgs/theme/loading.gif')?>" alt="jobhub" />
                    </div>
                </div>
            </div>
        </div>
        <main class="main-no login-mt">
            <section class="section-box">
                <div class="container pt-0 login-container">
                    <div class="logo" style="text-align: center;">
                        <a href="index.php"><img alt="Mextemps" src="<?=base_url('assets/themes/imgs/theme/mextemps.svg')?>" width="245"></a>
                    </div>
                    <div class="row login-content">
                        <div class="col-xl-12 col-md-12 col-sm-12 mx-auto shadow card-grid-4 mt-10 mb-10" style="padding: 0px;">
                            <div class="row">
                                <div class="contact-from-area padding-20-row-col col-md-12 col-sm-12" style="padding: 20px 30px 20px 30px;">
                                <h4 style="text-align: center;" class="login-title">Ingresa</h4>
                                    <form class="contact-form-style mt-10 needs-validation" id="form-recuperar" enctype="multipart/form-data" novalidate>
                                        <div class="row wow animate__ animate__fadeInUp animated" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                            <div class="col-lg-12 col-md-12">
                                                <div class="input-style position-relative">
                                                    <span class="contenedor-ojo mb-10 my-2">
                                                        <input name="contrasena_nueva" type="password" placeholder="Nueva contraseña" required style="height: 40px;">
                                                        <a class="btn icon-ojo" style="font-size: 22px;"><i class="fas fa-eye"></i></a>
                                                        <div class="invalid-tooltip">Requerido</div>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12">
                                                <div class="input-style position-relative">
                                                    <span class="contenedor-ojo mb-10 my-2">
                                                        <input style="height: 40px;" name="confirmar_contrasena" type="password" placeholder="Confirmar contraseña:" required>
                                                        <a class="btn icon-ojo" style="font-size: 22px;"><i class="fas fa-eye"></i></a>
                                                        <div class="invalid-tooltip">Requerido</div>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 text-center">
                                                <input class="invisible" hidden name="token" value="<?= $token ?>">
                                                <input class="invisible" hidden name="usuario" value="<?= $id_usuario ?>">
                                                <button class="submit submit-auto-width" type="submit" id="btn-recuperar" style="padding: 10px 40px; border-radius: 30px; margin-top: 10px; width: 100%;">Recuperar</button>
                                                <p style="margin-top: 20px;">¿Aún no has creado tu cuenta en Mextemps? <a href="<?=base_url('/registrarme-como')?>">Regístrate</a> </p>
                                            </div>
                                        </div>
                                    </form>
                                    <p class="form-messege"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <div class="modal fade" id="recuperar-contraseña" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo-recuperar" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header justify-content-center border-bottom-0 py-4">
                        <h5 class="modal-title fw-bold" id="titulo-recuperar">Recupera tú contraseña</h5>
                        <a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></a>
                    </div>
                    <div class="modal-body contact-from-area">
                        <div class="container contact-form-style">
                            <form class="row text-center needs-validation" id="form-recuperar-contra" enctype="multipart/form-data" novalidate>
                                <div class="position-relative">
                                    <input style="height: 50px;" name="correo_resetar" type="email" placeholder="¿Cúal es tu correo electrónico?" required>
                                    <div class="invalid-tooltip">Ingresa un correo válido</div>
                                </div>
                                <div class="col-12 my-4">
                                    <button style="padding: 10px 40px; border-radius: 30px; margin-top: 20px; width: 100%;" class="submit submit-auto-width" type="submit" id="btn-recuperar-contraseña" data-bs-toggle="modal" data-bs-target="#contraseña-recuperada">Recuperar contraseña <i class="fas fa-angle-right" aria-hidden="true"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <p style="text-align: center; margin-bottom: 20px;"> No tienes una cuenta? <a id="lig-reg">Registrate aquí</a></p>
                </div>
            </div>
        </div>
        <div class="modal fade" id="contraseña-recuperada" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo-recuperado" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header justify-content-center border-bottom-0 py-4">
                        <h5 class="modal-title fw-bold">Recupera tú contraseña</h5>
                        <a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></a>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row text-center">
                                <div class="col-12 my-2">
                                    <p class="text-font text-secundario">Si la cuenta de correo existe en nuestros registros, se ha enviado un correo con los pasos para realizar la recuperación de tu contraseña.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center py-2">
                                    <label class="fw-bold h6">Gracias</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://accounts.google.com/gsi/client" async defer></script>
        <script>
            (function () {
                'use strict'
                let forms = document.querySelectorAll('.needs-validation');

                Array.prototype.slice.call(forms).forEach(function (form) {
                    form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated')}, false)
                })
            })()
            $(document).ready(function() {
                $('.contenedor-ojo .icon-ojo').click(function() {
                    if($(this).siblings().attr('type') == 'password') {
                        $(this).siblings().attr('type', 'text'); 
                        $(this).find('i').replaceWith('<i class="fas fa-eye-slash"></i>');
                    }else{ 
                        $(this).siblings().attr('type', 'password');
                        $(this).find('i').replaceWith('<i class="fas fa-eye"></i>');
                    }
                });
                $('#form-recuperar').submit(function(e) {
                    e.preventDefault();
                    $.ajax({
                        url: '<?= base_url('Autenticacion/restablecerCuentaContrasena')?>',
                        type: 'POST',
                        data: $('#form-recuperar').serialize(),
                        success: function (data) {
                            return data;
                        },
                        error: function(data){
                            return alertify.error(data.responseText, 10);
                        }
                    });
                });
            });
        </script>
        <style>
            #contraseña-recuperada .modal-dialog, #recuperar-contraseña .modal-dialog {
                max-width: 600px;
            }
            #lig-reg{
                color: #FE5000;
            }
        </style>
        <!--Scripts de creados para funciones-->
        <?php if(session('alerta')) :?>
            <script defer>
                $(document).ready(function(){
                    alertify.notify('<?= session('alerta.mensaje')?>', '<?= session('alerta.tipo')?>', 10);
                });
            </script>
        <?php endif; ?>
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <script defer src="<?=base_url('/assets/js/front.js')?>"></script>
        <!-- Scripts del tema -->
        <!-- Vendor JS-->
        <script src="<?=base_url('assets/themes/js/vendor/modernizr-3.6.0.min.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/vendor/jquery-3.6.0.min.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/vendor/jquery-migrate-3.3.0.min.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/vendor/bootstrap.bundle.min.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/plugins/waypoints.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/plugins/wow.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/plugins/magnific-popup.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/plugins/perfect-scrollbar.min.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/plugins/select2.min.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/plugins/isotope.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/plugins/scrollup.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/plugins/swiper-bundle.min.js')?>"></script>
        <!-- Template  JS -->
        <script src="<?=base_url('assets/themes/js/main.js?v=1.0')?>"></script>
    </body>
</html>