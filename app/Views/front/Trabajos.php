<?=$this->extend('front/main')?>
<?=$this->section('title')?>
	<?=$proyecto['titulo']?>
<?=$this->endSection()?> 

<?=$this->section('content')?>
<?php $contratista = model('Autenticacion')->get_id($proyecto['id_contratista']); ?>
<style>
	#sec-chat .btn{
		font-size: 14px !important;
	}
	.title-proyecto{
		font-size: 40px;
	}
	.info_proyecto {
		transition: all .8s;
		position: relative;
	}
	.info_proyecto:after{
		content: '';
		border: 1em solid #fe500096;
		border-radius: 50%;
		width: 17px;
		height: 17px;
		position: absolute;
		top: -22px;
		left: -9px;
		animation: animacion 2s linear infinite;
	}
	.info_proyecto:before{
		position: absolute;
		top: -14px;
	}
	@keyframes animacion{
		0%{
			transform: scale(0);
			opacity: 0;
		}
		50%{opacity: 1;}
		100%{
			transform: scale(1);
			opacity: 0;
		}
	}
	@media only screen and (max-width: 600px) {
		.author-single {
			padding-left: 0;
			text-align: center;
		}
	}
</style>
<main class="main proyectos-detalle">
	<section class="section-box">
		<div class="box-head-single">
			<div class="container d-none d-md-block">
				<h3 class="title-proyecto"><span data-bs-toggle="tooltip" data-bs-placement="top" title="Titulo del proyecto"><?=$proyecto['titulo']?></span></h3>
				<ul class="breadcrumbs">
					<li><a>
						<?php if($proyecto['estatus'] == 'espera'){ ?>
							Trabajo en postulación
						<?php }elseif($proyecto['estatus'] == 'desarrollo'){ ?>
							Trabajo en progreso
						<?php }elseif($proyecto['estatus'] == 'finalizado'){ ?>
							Trabajo finalizado
						<?php }else{?>
							Trabajo
						<?php } ?>
					</a></li>
				</ul>
				<button class="btn btn-regresar btn-border" onclick="history.back()">Regresar</button>
			</div>
			<div class="container text-center d-block d-md-none">
				<h3 class="title-proyecto"><span data-bs-toggle="tooltip" data-bs-placement="top" title="Titulo del proyecto"><?=$proyecto['titulo']?></span></h3>
				<ul class="breadcrumbs">
					<li><a>
						<?php if($proyecto['estatus'] == 'espera'){ ?>
							Trabajo en postulación
						<?php }elseif($proyecto['estatus'] == 'desarrollo'){ ?>
							Trabajo en progreso
						<?php }elseif($proyecto['estatus'] == 'finalizado'){ ?>
							Trabajo finalizado
						<?php }else{?>
							Trabajo
						<?php } ?>
					</a></li>
				</ul>
				<button class="btn btn-regresar btn-border" onclick="history.back()">Regresar</button>
			</div>
		</div>
	</section>
	<section class="section-box mt-10">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12 col-sm-12 col-12">
					<div class="single-image-feature-proyecto">
						<?=view_cell('App\Libraries\FuncionesSistema::carruselProyecto', ['id' => $proyecto['id']])?>
					</div>
					<div class="content-single d-none d-md-block">
						<?php if(!empty($proyecto['desc_corta'])) :?>
							<p>
								<h6>Resumen</h6>
								<?=$proyecto['desc_corta']?>
							</p>
						<?php endif ;?>
						<?php if(!empty($proyecto['descripcion'])) :?>
							<p>
								<h6>Descripción</h6>
								<?=$proyecto['descripcion']?>
							</p>
						<?php endif ;?>
					</div>
					<div class="content-single text-center d-block d-md-none">
						<h5>Descripción</h5>
						<p class="text-center">
							<?=$proyecto['descripcion']?>
						</p>
					</div>
					<div class="author-single w-100 barra-de-progreso">
						<!-- <span>AliThemes</span> -->
						<?php if(($proyecto['estatus'] == 'desarrollo' && $proyecto['id_freelancer'] == session('id')) || ($proyecto['estatus'] == 'finalizado' && $proyecto['id_freelancer'] == session('id'))){ ?>
							<span class="titulo-barra">Progreso del proyecto</span>
							<div class="progress">
								<div class="progress-bar progress-bar-striped progress-bar-animated" id="progreso" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
							</div>
							<br><br>
						<?php } ?>
					</div>
				</div>
				<div class="col-lg-4 col-md-12 col-sm-12 col-12 pl-40 pl-lg-15 mt-lg-30">
					<div class="sidebar-shadow">
						<div class="sidebar-heading">
							<div class="avatar-sidebar">
								<figure><img alt="mextemps" src="<?=imagenPerfil($contratista)?>" /></figure>
								<div class="sidebar-info">
									<span class="sidebar-company"><?=$contratista['nombre'].' '.$contratista['apellidos']?></span>
								</div>
							</div>
						</div>
						<div class="text-center mt-20" id="sec-chat">
							<?php if($proyecto['estatus'] == 'espera' && (session('rol') == 'freelancer')){ ?>
								<?= view_cell('App\Libraries\FuncionesSistema::validarPostulacion', ['idproyecto' => $proyecto['id'], 'idusuario' => session('id')]) ?>
								<?php $postulacion = model('Postulaciones')->where(['id_proyecto' => $proyecto['id'], 'id_postulante' => session('id')])->first();?>
								<?php if($postulacion != null){?>
									<?= $postulacion['sala'] != 0? '<span>Sala de chat</span><br>': ''?>
									<?= view_cell('App\Libraries\FuncionesSistema::mostrarChat', ['idproyecto' => $proyecto['id'], 'id' => session('id')]) ?>
								<?php } ?>
							<?php }elseif(($proyecto['estatus'] == 'desarrollo' || $proyecto['estatus'] == 'finalizado') && session('rol') == 'freelancer'){ ?>
								<div class="d-flex justify-content-around">
									<?= view_cell('App\Libraries\FuncionesSistema::chatProyectoInterior', ['proyecto' => $proyecto['id'], 'freelance' => session('id')]) ?>
								</div>
								<div class="d-grid my-2" id="sec-boton-fin">
									<?php $finsol = model('Finalizar')->where('id_proyecto', $proyecto['id'])->first(); ?>
									<?php if($finsol == null){ ?>
										<a class="btn btn-primary" onclick="soliFinalizarProyecto(<?=$proyecto['id']?>)" data-id="<?=$proyecto['id']?>">
											Solicitar finalizar proyecto
										</a>
									<?php }else{ ?>
										<?php if($finsol['estatus_contrante'] == 'true'){ ?>
											<div class="alert btn-sm alert-info text-center" role="alert">
												<i class="fas fa-check-circle"></i> El proyecto ya ha finalizado.
											</div>
										<?php }elseif($finsol['estatus_contrante'] == null && $finsol['estatus_freelance'] == 'true'){ ?>
											<div class="alert btn-sm text-center alert-info" role="alert">
												<i class="fas fa-info-circle"></i> Se le ha notificado al contratante que ha solicitado finalizar el proyecto.
											</div>
										<?php }elseif($finsol['estatus_contrante'] == 'false' && $finsol['estatus_freelance'] == 'false'){ ?>
											<div class="alert alert-info btn-sm text-center" role="alert">
												<i class="fas fa-info-circle"></i> El contratante nos ha notificado que aún no se han completado el proyecto, verifíque cada una de las tareas.
											</div>
											<a class="btn btn-primary" onclick="soliFinalizarProyecto(<?=$proyecto['id']?>)" data-id="<?=$proyecto['id']?>">
												Solicitar finalizar proyecto
											</a>
											<script>
												$(document).ready(function(){
													setTimeout(function(){
														resetSoli(<?=$proyecto['id']?>);
													}, 5000);
												});
											</script>
										<?php }elseif($finsol['estatus_contrante'] == null && $finsol['estatus_freelance'] == null){ ?>
											<a class="btn btn-primary" onclick="soliFinalizarProyecto(<?=$proyecto['id']?>)" data-id="<?=$proyecto['id']?>">
												Solicitar finalizar proyecto
											</a>
										<?php } ?>
									<?php } ?>
								</div>
								<script>
									setInterval(function(){
										verificarEstSoli(<?=$proyecto['id']?>);
									}, 3500);
								</script>
							<?php } ?>
						</div>
						<!-- TAREAS -->
						 <!---->
						<?php if($proyecto['estatus'] == 'desarrollo' || $proyecto['estatus'] == 'finalizado'){ ?>
							<div class="sidebar-team-member pt-40">
								<h6 class="small-heading mb-20">Tareas del proyecto</h6>
								<?php if($proyecto['estatus'] != 'finalizado'){ ?>
									<form id="tareas">
										<div class="row align-items-center mb-1 p-1">
											<div class="col-10 align-self-center <?= $proyecto['deleted'] ? 'order-2' : ''?>">
												<div class="form-floating">
													<input type="text" class="form-control <?= $proyecto['deleted'] ? 'disabled"' : ''?>" id="tarea" name="tarea" <?= $proyecto['deleted'] ? 'disabled' : ''?>>
													<label for="tarea">Agregar una nueva tarea</label>
												</div>
												<?php if(!$proyecto['deleted']) :?>
													<input type="hidden" id="id-proyecto" value="<?=$proyecto['id']?>" name="id">
												<?php endif ;?>
											</div>
											<?php if(!$proyecto['deleted']) :?>
												<div class="col-2 align-self-center boton-agregar-tareas">
													<a class="btn btn-naranja agregar-tarea rounded-circle">
														<i class="fas fa-plus"></i>
													</a>
												</div>
											<?php else :?>
												<div class="col-2 order-1">
													<div class="tooltips"><i style="color: #003B71; font-size:17px;" class="fas fa-info-circle info_proyecto" aria-hidden="true"></i>
														<span class="tooltiptext">Proyecto temporalmente suspendido.</span>
													</div>
												</div>
											<?php endif ;?>
										</div>
									</form>
								<?php } ?>
								<div class="w-100 border listado-de-tareas mt-2" style="min-height: 20px;">
									<div class="row p-2" id="sec-tareas">
										<?php if($proyecto['estatus'] == 'desarrollo'){ ?>
											<?=view_cell('App\Libraries\FuncionesSistema::tareasProyecto', ['id' => $proyecto['id']]);?>
										<?php }else if($proyecto['estatus'] == 'finalizado'){ ?>
											<?=view_cell('App\Libraries\FuncionesSistema::tareasProyectoContra', ['id' => $proyecto['id']])?>
										<?php } ?>
									</div>
								</div>
							</div>
						<?php } ?>

						<!-- TIPO DE PROYECTO -->
						<div class="sidebar-list-job">
							<ul>
								<li>
									<div class="sidebar-icon-item"><i class="fi-rr-briefcase"></i></div>
									<div class="sidebar-text-info">
										<span class="text-description">Tipo de proyecto</span>
										<strong class="small-heading"><?=$proyecto['categoria']?> / <?=$proyecto['subcategoria']?></strong>
									</div>
								</li>
								<li>
									<div class="sidebar-icon-item"><i class="fi-rr-marker"></i></div>
									<div class="sidebar-text-info">
										<span class="text-description">Locación</span>
										<strong class="small-heading"><?=view_cell('App\Libraries\FuncionesSistema::direccionContratista', ['id' => $proyecto['id_contratista']])?></strong>
									</div>
								</li>
								<li>
									<div class="sidebar-icon-item"><i class="fi-rr-dollar"></i></div>
									<div class="sidebar-text-info">
										<span class="text-description">Presupuesto</span>
										<strong class="small-heading">
											<?php if($proyecto['presupuesto'] > 0) :?>
												$ <?= number_format($proyecto['presupuesto'], 2, '.', ',') ?> <small>MXN</small>
											<?php else :?>
												Confidencial
											<?php endif ;?>
										</strong>
									</div>
								</li>
								<li>
									<div class="sidebar-icon-item"><i class="fi-rr-clock"></i></div>
									<div class="sidebar-text-info">
										<span class="text-description">Fecha de creación</span>
										<strong class="small-heading"><?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $proyecto['created_at']])?></strong>
									</div>
								</li>
								<li>
									<div class="sidebar-icon-item"><i class="fi-rr-time-fast"></i></div>
									<div class="sidebar-text-info">
										<span class="text-description">Fecha de entrega esperado</span>
										<strong class="small-heading"><?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $proyecto['fecha_entrega']])?></strong>
									</div>
								</li>
							</ul>
						</div>
					   
					</div>
					<!--<div class="sidebar-normal">-->
						<!--=view_cell('App\Libraries\FuncionesSistema::habilidadesFreelance', ['id' => session('id')])-->
					<!--</div>-->
				</div>
				<!-- MAS PROYECTOS -->
				<div class="single-recent-jobs">
						<h4 class="heading-border"><span>Más proyectos</span></h4>
						<div class="list-recent-jobs">
							<?=view_cell('App\Libraries\FuncionesSistema::proyectosRelacionados', ['id' => session('id'), 'pro' => $proyecto['id']])?>
							<div class="mb-20">
								<a href="<?=base_url('/buscar-proyecto?search=&area=')?>" class="btn btn-default">Explorar más proyectos</a>
							</div>
						</div>
					</div>
			</div>
		</div>
	</section>
</main>
<div class="modal fade" id="postular_a_proyecto" data-bs-backdrop="static" data-bs-keyboard="true" aria-hidden="true" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-0">
				<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#perfil" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
				<h5 class="modal-title h4">Postularme al proyecto</h5>
			</div>
			<div class="modal-body py-0">
				<div class="row align-items-center text-center">
					<div class="col-md-5 p-4 listado-talentos">
						<small class="text-font fst-italic">Tu perfil como lo verá el contratante</small>
						<?php $monto = model('ModelPlanes')->selectMax('monto')->where('cuenta', 'freelancer')->get()->getRowArray()['monto'];
							$con_suscripcion_premium = (model('Suscripciones')
							->builder()
							->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
							->where(['id_usuario' => session('id'), 'estatus' => 'Activo', 'monto' => $monto])
							->get()->getRowArray());
						?>
						<?= view_cell('App\Libraries\FuncionesSistema::cardPostulantes', ['id' => session('id'), 'postu' => 0, 'tipo' => 'talento', 'modal' => 0, 'es_premium' => !empty($con_suscripcion_premium) ? true : false]) ?>
					</div>
					<div class="col-md-7">
						<p class="text-font text-start p-2">
							Estás a punto de postularte al proyecto <strong><?=$proyecto['titulo']?></strong>, recuerda que mientras más completo este tu perfil, mayor es la posibilidad de ser escogido para desarrollar el proyecto, recuerda mantener una actitud cordial y respetuosa con cada miembro de nuestra plataforma. De parte del equipo Mextemps te deseamos la mejor de las suertes.
						</p>
						<a class="btn btn-naranja btn-border my-2" href="<?=base_url('/postularme/'.$proyecto['id'])?>">Postularse</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function resetSoli(id){
		$.ajax({
			type: 'POST',
			url: '<?= base_url('/Proyectos/actuaSoliFin') ?>',
			data:{id:id},
			success: function(data){
				console.log(data);
			}, error: function(data){
				console.log('Error al actualizar el estatus de la solicitud');
			}
		});
	}
	function verificarEstSoli(id){
		let fre = $('#est-free').val();
		let contra = $('#est-cont').val();
		$.ajax({
			type: 'POST',
			url: '<?= base_url('/Proyectos/VerificarEstadoSoliFin') ?>',
			data:{id:id, fre:fre, contra:contra},
			success:function(data){
				let cont = JSON.parse(data);
				if(cont.tipo != 'error'){
					$('#sec-boton-fin').html(cont.html);
					if(cont.con == 'true' && cont.free == 'true'){
						$('#enviar_mensaje').addClass('d-none');
					}else{
						$('#enviar_mensaje').removeClass('d-none');
					}
				}
			}, error: function(data){
				console.log('Error en la conexion con el servidor.');
			}
		});
	}
	function soliFinalizarProyecto(id){
		$.ajax({
			type: 'POST',
			url: '<?= base_url('/Proyectos/soliFinalizarProyecto') ?>',
			data:{id:id},
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'advertencia') return alertify.notify(cont.mensaje, 'advertencia');
				$('#sec-boton-fin').html('<div class="alert btn-sm text-center text-white" role="alert" style="background-color: #003C71cb"><i class="fas fa-info-circle"></i> Se le ha notificado al contratante que ha solicitado finalizar el proyecto.</div>');
				return alertify.notify(cont.mensaje, 'advertencia');
			}, error: function(data){
				alertify.notify('Intentelo más tarde, nuestros servidores se encuentran en mantenimiento.', 'advertencia');
			}
		});
	}
	function progreso(){
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Proyectos/progresoProyectoLoading')?>',
			data:{id:<?=$proyecto['id']?>},
			success: function(data){
				console.log(data);
				$('#progreso').css('width', data);
				$('#progreso').html(data);
				if(data == '100%'){
					$('#progreso').addClass('bg-success');
				}else{
					$('#progreso').removeClass('bg-success');
				}
			}, error: function(data){
				alertify.error('A surgido un error, comuniquese con el area de soporte.', 'falla', 10);
			}
		});
	}
	$(document).ready(function(){
		progreso();
	});
	$('.agregar-tarea').on('click', function(){
		console.log($('#tareas').serialize());
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Proyectos/crearTarea')?>',
			data:$('#tareas').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'success'){
					$('#tarea').val('');
					$('#sec-tareas').html(cont.contenido);
					progreso();
				}else{
					alertify.notify(cont.mensaje, 'advertencia', 10);
				}
			}, error: function(data){
				alertify.npotify('A surgido un error, comuniquese con el area de soporte.', 'falla', 10);
			}
		});
	});
</script>
<?=$this->endSection()?>
