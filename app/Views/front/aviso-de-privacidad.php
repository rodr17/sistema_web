<?=$this->extend('front/main')?>
<?=$this->section('title')?>
    Aviso de Privacidad
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="paginas-info">
<div class="banner-seccion" data-banner="">
    <div class="container">
        <div class="titulo-banner">
            <h1>Aviso de Privacidad</h1>
        </div>
    </div>
</div>
<div class="container pb-5 pt-3">
    <div class="container">
        <?php if($aviso == null){ ?>
            <div class="text-center">
                <h2>
                    Proximamente...
                </h2>
            </div>
        <?php }else{ ?>
            <?php $data = json_decode($aviso['info'], true); ?>
            <?=$data['body']?>
        <?php } ?>
    </div>
</div>
</div>
<?=$this->endSection()?>