<?=$this->extend('front/main')?>
<?=$this->section('title')?>
	Blog
<?=$this->endSection()?>

<?=$this->section('content')?>
<style>
	.pagination .pages {
		padding: 0 10px;
		line-height: 40px;
	}
	.pagination .page-link, .page-item.disabled .page-link {
		background-color: #FE5000 !important;
		border-radius: 50px !important;
		width: 40px !important;
		height: 40px !important;
		line-height: 38px !important;
		padding: 0 14px !important;
		font-size: 24px !important;
		border: 0px !important;
		color: #fff !important;
	}
	.pagination .page-link:hover, .page-item.disabled .page-link:hover{
		background-color: #FE5000 !important;
	}
	.paginations .pager li {
		margin: 0 3px;
	}
</style>
<main class="main blog-main">
	<div class="archive-header pt-50 pb-50">
		<div class="container">
			<h3 class="mb-30 titulo text-center w-75 mx-auto" style="font-size: 45px; line-height: 52px;">
				Noticias relevantes y más para ti. <br>Bienvenido a nuestro blog
			</h3>
			<!-- <div class="text-center">
				<div class="sub-categories">
					<a href="#" class="btn btn-tags-sm mb-10 mr-5">Tecnología</a>
					<a href="#" class="btn btn-tags-sm mb-10 mr-5">Branding</a>
					<a href="#" class="btn btn-tags-sm mb-10 mr-5">Programación</a>
					<a href="#" class="btn btn-tags-sm mb-10 mr-5">Comunicación</a>
					<a href="#" class="btn btn-tags-sm mb-10 mr-5">Agencias</a>
				</div>
			</div> -->
		</div>
	</div>
	<div class="post-loop-grid listado-blog">
		<div class="container">
			<div class="row justify-content-center pr-15 pl-15">
				<div class="col-12">
					<form class="input-group mb-3" id="form_buscador_blog">
						<input class="form-control text-font" placeholder="¿Qué deseas buscar?" id="buscador_blog" name="buscador_blog"/>
						<button class="btn btn-primary" type="submit">
							<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
								<path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
							</svg> Buscar
						</button>
					</form>
					<div class="d-flex justify-content-between gap-2">
						<div class="d-flex justify-content-between justify-content-md-end">
							<span class="text-sortby">Ordenar por:</span>
							<div class="dropdown dropdown-sort">
								<select class="btn dropdown-toggle text-font" type="button" id="ordernar_noticias" data-bs-toggle="dropdown" aria-expanded="false" data-bs-display="static">
									<i class="fi-rr-angle-small-down"></i>
									<option class="dropdown-item text-font" selected value="titulo">Título</option>
									<option class="dropdown-item text-font" value="recientes">Más recientes</option>
									<option class="dropdown-item text-font" value="antiguos">Más antiguos</option>
								</select>
							</div>
						</div>
						<div id="contenido_texto_buscado"></div>
						<span class="text-small d-block text-end mb-4">Mostrando <strong id="total_noticias"><?=count($noticias)?></strong> noticias para ti.</span>
					</div>
				</div>
				<div class="row justify-content-start" id="contenedor_noticias">
					<?php foreach($noticias as $key => $noti) :?>
						<?=view_cell('App\Libraries\FuncionesSistema::tarjetas_Blog', ['noticia' => $noti, 'conteo' => $key])?>
					<?php endforeach ;?>
				</div>
			</div>
			<?php if($pages > 1){ ?>
				<?php if(!$_GET){ ?>
					<div class="pagination paginations text-center wow animate__animated animate__fadeIn">
						<ul class="pager">
							<li class="page-item">
								<a class="page-link disabled" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
								</a>
							</li>
							<?php for($i = 1; $i <= $pages; $i++){ ?>
								<li class="page-item">
									<a class="pager-number <?= ($i == 1)? 'active' : '' ?>" href="<?=base_url('/blog?page='.($i))?>">
										<?= $i ?>
									</a>
								</li>
							<?php } ?>
							<li class="page-item">
								<a href="<?=base_url('/blog').'?page=2' ?>" class="page-link" aria-label="Next">
									<span aria-hidden="true">&raquo;</span>
								</a>
							</li>
						</ul>
					</div>
				<?php }else{ ?>
					<div class="pagination paginations text-center wow animate__animated animate__fadeIn">
						<ul class="pager">
							<?php if($pagina == 1){ ?>
								<li class="page-item">
									<a class="page-link" aria-label="Previous">
										<span aria-hidden="true">&laquo;</span>
									</a>
								</li>
							<?php }else{ ?>
								<li class="page-item">
									<a href="<?=base_url('/blog?page=').($pagina - 1) ?>" class="page-link <?=($pagina == 1)? 'disabled' : '' ?>" aria-label="Previous">
										<span aria-hidden="true">&laquo;</span>
									</a>
								</li>
							<?php } ?>
							<?php for($i = 1; $i <= $pages; $i++){ ?>
								<li class="page-item">
									<a class="pager-number <?= ($pagina == $i)? 'active' : '' ?>" href="<?=base_url('/blog?page='.($i))?>">
										<?= $i ?>
									</a>
								</li>
							<?php } ?>
							<?php if($pagina != $pages){ ?>
								<li class="page-item">
									<a href="<?=base_url('/blog')?><?= ($pagina > 1)? '?page='.($pagina + 1) : '' ?>" class="page-link <?=($pagina == $pages)? 'disabled' : '' ?>" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
									</a>
								</li>
							<?php }else{ ?>
								<li class="page-item">
									<a class="page-link <?=($pagina == $pages)? 'disabled' : '' ?>" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
									</a>
								</li>
							<?php } ?>
						</ul>
					</div>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
</main>
<script>
	$('#form_buscador_blog').submit(function(evento){
		evento.preventDefault();
		$this = $(this);
		$.ajax({
			type: 'POST',
			url: '<?=base_url('Home/buscar_noticias')?>',
			data: $this.serialize(),
			success: function(respuesta){
				let contenido = JSON.parse(respuesta);
				if(contenido.alerta != 'correcto') return alertify.notify(contenido.mensaje, contenido.alerta);
				
				$('#contenido_texto_buscado').html('<span class="badge text-font rounded-pill bg-primary resetar_filtrado lh-sm me-3 mb-2" style="font-weight:500">Filtrado: '+ $('#buscador_blog').val() +' <i class="fas fa-times "></i></span>');
				$('#contenedor_noticias').html(contenido.html);
				$('#total_noticias').text(contenido.total);
			},
			error: function(error){ alertify.notify('Error: ' + error.responseText, 'falla')}
		});
	});
	
	$('#contenido_texto_buscado').delegate('span.resetar_filtrado', 'click', function(evento){
		$this = $(this);
		$.ajax({
			type: 'POST',
			url: '<?=base_url('Home/resetar_filtrado_noticias')?>',
			success: function(respuesta){
				let contenido = JSON.parse(respuesta);
				if(contenido.alerta != 'correcto') return alertify.notify(contenido.mensaje, contenido.alerta);
				
				$this.remove();
				$('#buscador_blog').val('');
				$('#contenedor_noticias').html(contenido.html);
				$('#total_noticias').text(contenido.total);
			},
			error: function(error){ alertify.notify('Error: ' + error.responseText, 'falla')}
		});
	});
	$('#ordernar_noticias').change(function(){
		$this = $(this);
		let buscar = $('#resetar_filtrado').length != 0 ? $('#resetar_filtrado').val() : '';
		$.ajax({
			type: "POST",
			url: '<?=base_url('Home/ordernar_noticias')?>',
			data: {filtro : $this.val(), buscado : buscar},
			success: function (respuesta) {
				let contenido = JSON.parse(respuesta);
				if(contenido.alerta != 'correcto') return alertify.notify(contenido.mensaje, contenido.alerta);

				if(buscar.length != 0) $('#contenido_texto_buscado').html('<span class="badge text-font rounded-pill bg-primary resetar_filtrado lh-sm me-3 mb-2" style="font-weight:500">Filtrado: '+ $('#buscador_blog').val() +' <i class="fas fa-times "></i></span>');
				$('#contenedor_noticias').html(contenido.html);
			},
			error: function (error) {
				alertify.error(error, 10);
			}
		});
	});
</script>
<?=$this->endSection()?>