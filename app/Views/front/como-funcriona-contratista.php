<?=$this->extend('front/main')?>
<?=$this->section('title')?>
    Como funciona Contratante
<?=$this->endSection()?>

<?=$this->section('content')?>
<main class="main como-funciona-pages">
    <section class="section-box bg-banner-about contratantes">
        <div class="banner-hero banner-about pt-20">
            <div class="container banner-inner">
                <div class="row justify-content-center align-items-center">
                    <div class="col-lg-6 col-sm-7">
                        <div class="block-banner">
                            <h1 class="heading-banner heading-lg text-center text-sm-start"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'area-slides', 'valor' => 'titulo'])?> <span class="texto-naranja"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'area-slides', 'valor' => 'destacado'])?></span></h1>
                            <div class="banner-imgs d-sm-none">
                                <img alt="Contratantes" src="<?=base_url('assets/themes/imgs/como/contratante.png')?>" class="img-responsive" />
                            </div>
                            <div class="banner-description mt-30"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'area-slides', 'valor' => 'subtitulo'])?></div>
                            <div class="mt-30">
                                <?php if(!session('logeado')){ ?>
                                    <div class="mr-10 text-center text-sm-start">
                                        <a href="<?=base_url('/registrarme/contratante')?>" class="btn btn-azul-blanco">Regístrate</a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-5 d-lg-block">
                        <div class="banner-imgs d-none d-sm-block">
                            <img alt="Contratantes" src="<?=base_url('assets/themes/imgs/como/contratante.png')?>" class="img-responsive" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-box mt-0 mb-80 buscar-un">
        <div class="container">
            <div class="block-job-bg block-job-bg-homepage-2 ">
                <div class="row">
                    <div class="col-lg-6 col-md-5 col-sm-12 col-12 d-none d-md-block" style="padding-right: 0px; padding-left: 0px;">
                        <div class="box-image-findjob findjob-homepage-2 ml-0 wow animate__animated animate__fadeIn h-100">
                            <figure class="">
                                <img alt="Como funciona" class="" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'crea-una-cuenta', 'valor' => 'imagen'])?>" />
                            </figure>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-7 col-sm-12 col-12 como-buscar">
                        <div class="box-info-job pl-60 pt-90 pr-60">
                            <div class="wow animate__animated animate__fadeInUp titulo text-center text-sm-start">
                                <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'crea-una-cuenta', 'valor' => 'superior'])?>
                            </div>
                            <h5 class="heading-36 mb-30 mt-30 wow animate__animated animate__fadeInUp">
                            <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'crea-una-cuenta', 'valor' => 'titulo'])?>
                            </h5>
                            <p class="text-lg wow animate__animated animate__fadeInUp pt-50 pb-50">
                                <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'crea-una-cuenta', 'valor' => 'subtitulo'])?>
                            </p>
                            <div class="d-flex">
                                <?php if(session('logeado')){ ?>
                                    <?php if(session('rol') == 'freelancer'){?>
                                        <a class="wesktop-view btn btn-circulo-naranja text-font px-3" id="bt-bus-pro">
                                            <i class="fas fa-arrow-right" aria-hidden="true"></i>
                                        </a>
                                        &nbsp;
                                        <div class="wesktop-view encuentra-freelancer-txt" style="max-width: 250px; font-size: 20px; line-height: 30px; font-weight: bold; font-family: 'Poppins', sans-serif;" class="mx-3 fw-bold align-self-center">
                                            ENCUENTRA<br>UN FREELANCER
                                        </div>
                                        <a href="<?=base_url('/usuarios/talento')?>" class="movile-view text-dark btn btn-empecemos mt-4 mb-1">Encuentra un freelancer <i class="fas fa-arrow-right" aria-hidden="true"></i></a>
                                        <script>
                                            $('#bt-bus-pro').click(function(){
                                                alertify.warning('Lo sentimos, para buscar proyectos debe iniciar sesion como freelancer.', 'advertencia');
                                            });
                                        </script>
                                    <?php }else{ ?>
                                        <a class="wesktop-view btn btn-circulo-naranja text-font px-3" href="<?=base_url('/usuarios/talento')?>">
                                            <i class="fas fa-arrow-right" aria-hidden="true"></i>
                                        </a>
                                        &nbsp;
                                        <div class="wesktop-view encuentra-freelancer-txt" style="max-width: 250px; font-size: 20px; line-height: 30px; font-weight: bold; font-family: 'Poppins', sans-serif;" class="mx-3 fw-bold align-self-center">
                                            ENCUENTRA<br>UN FREELANCER
                                        </div>
                                        <a href="<?=base_url('/usuarios/talento')?>" class="movile-view text-dark btn btn-empecemos mt-4 mb-1">Encuentra un freelancer <i class="fas fa-arrow-right" aria-hidden="true"></i></a>
                                    <?php } ?>
                                <?php }else{ ?>
                                    <a class="wesktop-view btn btn-circulo-naranja text-font" href="<?=base_url('/registrarme/contratante')?>">
                                        <i class="fas fa-arrow-right" aria-hidden="true"></i>
                                    </a>
                                    &nbsp;
                                    <div class="wesktop-view encuentra-freelancer-txt" style="max-width: 250px; font-size: 20px; line-height: 30px; font-weight: bold; font-family: 'Poppins', sans-serif;" class="mx-3 fw-bold align-self-center">
                                        ENCUENTRA<br>UN FREELANCER
                                    </div>
                                    <a href="<?=base_url('/registrarme/contratante')?>" class="movile-view text-dark btn btn-empecemos mt-4 mb-1">Encuentra un freelancer <i class="fas fa-arrow-right" aria-hidden="true"></i></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="w-100 mt-50 mb-50 porqueusar">
        <div class="container ">
            <div class="titulos col-lg-12 offset-lg-1 text-center text-md-start">
                <h2 ><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'tres-pasos', 'valor' => 'titulo'])?> <span class="text-naranja text-center text-md-start"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'tres-pasos', 'valor' => 'destacado'])?></span></h2>
            </div>
            <div class="m-1 row porqueusar-desktop">
                <div class="col-12 col-lg-6 align-self-center offset-lg-1">
                    <div id="trespasos" class="texto-porque-usar-mextemps">
                        <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'tres-pasos', 'valor' => 'contenido'])?>
                    </div>
                </div>
                <div class="col-12 col-lg-5">
                    <?php for($i = 1; $i <= 3; $i++){ ?>
                        <div class="w-100">
                            <div class="d-flex">
                                <div data-desc="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'tres-pasos-'.$i, 'valor' => 'contenido'])?>" class="cir-paso btn-naranja-circular" style="min-width: 100px; max-width: 100px; min-height: 100px; max-height: 100px;">
                                    <img  src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'tres-pasos-'.$i, 'valor' => 'imagen'])?>" alt="">
                                </div>
                                <div class="px-3 align-self-center">
                                    <span class=" perfhov">
                                        <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'tres-pasos-'.$i, 'valor' => 'titulo'])?>
                                    </span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="pt-100 row porqueusar-movil">
            <div class="w-100">
                <span class="btn-imagen-naranja"><img src="https://tresesenta.lat/mextemps/pantallas/assets/imgs/como/pm1.svg" alt=""></span>
                <div class="px-2 align-self-center">
                    <span class="fw-bold font-lg eltitulo">Busca a tu freelance</span>
                    <p style="font-size: 13px;" class="elparrafo d-none d-sm-block">
                        <strong>Mextemps</strong> es la plataforma para encontrar a las personas adecuadas en el momento adecuado. Te ayudamos a localizar a los candidatos perfectos para tu proyecto. Esta búsqueda de talento te da la oportunidad de encontrar expertos que pueden encajar perfectamente en tus necesidades.
                        Contacta a aquellos que te interesen por medio de nuestro buscador con palabras clave o por medio de nuestras categorías, utilizando los filtros para dar con el correcto para ti.
                    </p>
                </div>
            </div>
            <div class="w-100">
                <span class="btn-imagen-naranja"><img src="https://tresesenta.lat/mextemps/pantallas/assets/imgs/como/pm2.svg" alt=""></span>
                <div class="px-2 align-self-center">
                    <span class="fw-bold font-lg eltitulo">Sube tu proyecto</span>
                    <p style="font-size: 13px;" class="elparrafo d-none d-sm-block">
                        Crea tu proyecto con los requisitos que necesitas, y con el presupuesto que estés dispuesto a pagar por el trabajo. <br>
                        Asígnale las categorías de freelancers que necesites y listo!; <strong>tu proyecto estará dentro de nuestra red.</strong>
                    </p>
                </div>
            </div>
            <div class="w-100">
                <span class="btn-imagen-naranja"><img src="https://tresesenta.lat/mextemps/pantallas/assets/imgs/como/pm3.svg" alt=""></span>
                <div class="px-2 align-self-center">
                    <span class="fw-bold font-lg eltitulo">Contrata</span>
                    <p style="font-size: 13px;" class="elparrafo d-none d-sm-block">
                        Charla con los diferentes <strong>freelancers.</strong> <br>
                        Selecciona a los que desees contratar y llega a un acuerdo sobre el alcance del trabajo, las condiciones de pago y el cronograma. <br>
                        Paga el proyecto a tu freelance seleccionado y cierra el proyecto dándole una evaluación por las tareas asignadas.
                    </p>
                </div>
            </div>
        </div>
        </div>
    </section>
    <script>
        $('.cir-paso').hover(function(){
            $('#trespasos').html($(this).attr('data-desc'));
        });
    </script>
    <section class="section-box mt-90 mt-md-0 oportunidades">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-7 col-sm-12 col-12">
                    <span class="text-lg text-brand wow animate__animated animate__fadeInUp"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'oportunidades', 'valor' => 'superior'])?></span>
                    <h3 class="mt-30 mb-40 wow animate__animated animate__fadeInUp" style="max-width: 500px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'oportunidades', 'valor' => 'titulo'])?></h3>
                    <div class="banner-imgs banner-imgs-about d-block d-md-none">
                        <img alt="Mextemps" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'oportunidades', 'valor' => 'imagen'])?>" class="w-75" />
                    </div>
                    <p class="mb-0 wow animate__animated animate__fadeInUp">
                        <?= str_replace(array('<p>', '</p>'), array('', ''), view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'oportunidades', 'valor' => 'descripcion'])) ?>
                    </p>
                    <div class="mt-40 wow animate__animated animate__fadeInUp">
                        <?php if(!session('logeado')){ ?>
                            <a href="<?=base_url('/registrarme/contratante')?>" class="btn btn-default">Regístrate</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-5 col-sm-12 col-12 d-none d-md-block">
                    <div class="banner-imgs banner-imgs-about">
                        <img style="margin-bottom: -6px;" alt="Mextemps" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'oportunidades', 'valor' => 'imagen'])?>" class="img-responsive" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-box mt-80 mt-md-50 comentarios d-none d-lg-block">
        <div class="container">
            <h2 class="text-center titulos mb-15 section-title wow animate__animated animate__fadeInUp"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'comentarios', 'valor' => 'titulo'])?> <span class="naranja"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'comentarios', 'valor' => 'destacado'])?></span></h2>
            <div class="text-normal descripcion text-center color-black-5 box-mw-60 wow animate__animated animate__fadeInUp">
                <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-contratante', 'tipo' => 'comentarios', 'valor' => 'inferior'])?>
            </div>
            <?=view_cell('App\Libraries\FuncionesSistema::testimonios')?>
        </div>
    </section>
    <section class="section-box mt-50 mb-60 suscribete-ahora">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-8">
                    <div class="box-newsletter d-none d-md-block">
                        <h5 class="text-lg-newsletter text-center">Suscríbete ahora</h5>
                        <h6 class="text-md-newsletter text-center">Recibe aviso de los últimos proyectos publicados</h6>
                        <div class="box-form-newsletter mt-30">
                           <form class="form-newsletter needs-validation position-relative" id="boletines_suscripcion" novalidation encytpe="multipart/form-data">
                                <input type="email" class="input-newsletter" name="correo" id="correo" placeholder="usuario@gmail.com" required>
                                <div class="invalid-tooltip text-font">Requerido</div>
                                <button class="btn btn-default font-heading icon-send-letter" type="submit">Suscribirme</button>
                            </form>
                        </div>
                    </div>
                    <div class="box-newsletter d-block d-md-none">
                        <h5 class="text-white fw-bold">Suscríbete ahora</h5>
                        <h6 class="text-white fw-normal text-md mt-2">Recibe aviso de los últimos proyectos publicados</h6>
                        <div class="box-form-newsletter mt-30">
                           <form class="form-newsletter needs-validation position-relative" id="boletines_suscripcion" novalidation encytpe="multipart/form-data">
                                <input type="email" class="input-newsletter" name="correo" id="correo" placeholder="usuario@gmail.com" required>
                                <div class="invalid-tooltip text-font">Requerido</div>
                                <button class="btn btn-default font-heading icon-send-letter" type="submit" style="background-position: right 20px center;"></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?=$this->endSection()?>