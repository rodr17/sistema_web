<?=$this->extend('front/main')?>
<?=$this->section('title')?>
    Como funciona Freelance
<?=$this->endSection()?>

<?=$this->section('content')?>
<main class="main como-funciona-pages">
    <section class="section-box bg-banner-about">
        <div class="banner-hero banner-about pt-20">
            <div class="container banner-inner">
                <div class="row">
                    <div class="col-lg-6 col-sm-7">
                        <div class="block-banner">
                            <h1 class="heading-banner heading-lg text-center text-sm-start"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'area-slides', 'valor' => 'titulo'])?> <span class="texto-naranja"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'area-slides', 'valor' => 'destacado'])?></span></h1>
                            <div class="banner-imgs d-sm-none">
                                <img alt="Contratantes" src="<?=base_url('assets/themes/imgs/como/freelancer.png')?>" class="img-responsive" />
                            </div>
                            <div class="banner-description mt-30 text-center text-sm-start"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'area-slides', 'valor' => 'subtitulo'])?></div>
                            <div class="mt-30">
                                <?php if(!session('logeado')){?>
                                    <div class="mr-10 text-center text-sm-start">
                                        <a href="<?=base_url('/registrarme/freelancer')?>" class="btn btn-azul-blanco">Regístrate</a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-5 d-lg-block">
                        <div class="banner-imgs d-none d-sm-block">
                            <img alt="Contratantes" src="<?=base_url('assets/themes/imgs/como/freelancer.png')?>" class="img-responsive" />
                        </div>
                        <!--<div id="slider" class="carousel slide mtslide" data-bs-ride="carousel">-->
                        <!--    <div class="carousel-indicators">-->
                        <!--        <button type="button" data-bs-target="#slider" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>-->
                        <!--        <button type="button" data-bs-target="#slider" data-bs-slide-to="1" aria-label="Slide 2"></button>-->
                        <!--        <button type="button" data-bs-target="#slider" data-bs-slide-to="2" aria-label="Slide 3"></button>-->
                        <!--    </div>-->
                        <!--    <div class="carousel-inner">-->
                        <!--        <div class="carousel-item active">-->
                        <!--            <img src="<?=base_url('assets/themes/imgs/como/freelancer.png')?>" class="img-responsive" alt="...">-->
                        <!--        </div>-->
                        <!--        <div class="carousel-item">-->
                        <!--            <img src="<?=base_url('assets/themes/imgs/como/freelancer.png')?>" class="img-responsive" alt="...">-->
                        <!--        </div>-->
                        <!--        <div class="carousel-item">-->
                        <!--            <img src="<?=base_url('assets/themes/imgs/como/freelancer.png')?>" class="img-responsive" alt="...">-->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</div>-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-box mt-0 mb-80 buscar-un">
        <div class="container">
            <div class="block-job-bg block-job-bg-homepage-2 ">
                <div class="row">
                    <div class="col-lg-6 col-md-5 col-sm-12 col-12 d-none d-md-block" style="padding-right: 0px; padding-left: 0px;">
                        <div class="box-image-findjob findjob-homepage-2 ml-0 wow animate__animated animate__fadeIn h-100">
                            <figure class=""><img class="" alt="Como funciona" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'crea-una-cuenta', 'valor' => 'imagen'])?>" /></figure>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-7 col-sm-12 col-12 como-buscar">
                        <div class="box-info-job pl-60 pt-90 pr-60">
                            <div class="wow animate__animated animate__fadeInUp titulo text-center text-sm-start">
                                <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'crea-una-cuenta', 'valor' => 'superior'])?>
                            </div>
                            <h5 class="heading-36 mb-30 mt-30 wow animate__animated animate__fadeInUp">
                                <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'crea-una-cuenta', 'valor' => 'titulo'])?>
                            </h5>
                            <p class="text-lg wow animate__animated animate__fadeInUp pt-50 pb-50">
                                <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'crea-una-cuenta', 'valor' => 'subtitulo'])?>
                            </p>
                            <div class="d-flex">
                                <?php if(session('logeado')){ ?>
                                    <?php if(session('rol') == 'contratista'){?>
                                        <a class="wesktop-view btn btn-circulo-naranja text-font px-3" id="bt-bus-pro" >
                                            <i class="fas fa-arrow-right" aria-hidden="true"></i>
                                        </a>
                                        &nbsp;
                                        <div class="wesktop-view encuentra-proyecto-txt" style="max-width: 250px; font-size: 20px; line-height: 30px; font-weight: bold; font-family: 'Poppins', sans-serif;" class="mx-3 fw-bold align-self-center">
                                            ENCUENTRA<br>UN PROYECTO
                                        </div>
                                        <a href="<?=base_url('/buscar-proyecto?search=&area=')?>" class="movile-view text-dark btn btn-empecemos mt-4 mb-1">Encuentra un proyecto <i class="fas fa-arrow-right" aria-hidden="true"></i></a>
                                        <script>
                                            $('#bt-bus-pro').click(function(){
                                                alertify.warning('Lo sentimos, para buscar proyectos debe iniciar sesion como freelancer.', 'advertencia');
                                            });
                                        </script>
                                    <?php }else{ ?>
                                        <a class="wesktop-view btn btn-circulo-naranja text-font px-3" href="<?=base_url('/buscar-proyecto?search=&area=')?>" >
                                            <i class="fas fa-arrow-right" aria-hidden="true"></i>
                                        </a>
                                        &nbsp;
                                        <div class="wesktop-view encuentra-proyecto-txt" style="max-width: 250px; font-size: 20px; line-height: 30px; font-weight: bold; font-family: 'Poppins', sans-serif;" class="mx-3 fw-bold align-self-center">
                                            ENCUENTRA<br>UN PROYECTO
                                        </div>
                                        <a href="<?=base_url('/buscar-proyecto?search=&area=')?>" class="movile-view text-dark btn btn-empecemos mt-4 mb-1">Encuentra un proyecto <i class="fas fa-arrow-right" aria-hidden="true"></i></a>
                                    <?php } ?>
                                <?php }else{ ?>
                                    <a class="wesktop-view btn btn-circulo-naranja text-font px-3" href="<?=base_url('/registrarme/freelancer')?>" >
                                        <i class="fas fa-arrow-right" aria-hidden="true"></i>
                                    </a>
                                    &nbsp;
                                    <div class="wesktop-view encuentra-proyecto-txt" style="max-width: 250px; font-size: 20px; line-height: 30px; font-weight: bold; font-family: 'Poppins', sans-serif;" class="mx-3 fw-bold align-self-center">
                                        ENCUENTRA<br>UN PROYECTO
                                    </div>
                                    <a href="<?=base_url('/registrarme/freelancer')?>" class="movile-view text-dark btn btn-empecemos mt-4 mb-1">Encuentra un proyecto <i class="fas fa-arrow-right" aria-hidden="true"></i></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="w-100 mt-50 mb-50 porqueusar">
        <div class="container ">
            <div class="titulos col-lg-12 offset-lg-1 text-center text-md-start">
                <h2><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'tres-pasos', 'valor' => 'titulo'])?> <span class="text-naranja text-center text-md-start"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'tres-pasos', 'valor' => 'destacado'])?></span></h2>
            </div>
            <div class="m-1 row porqueusar-desktop">
                <div class="col-12 col-lg-6 align-self-center offset-lg-1">
                    <div id="trespasos" class="texto-porque-usar-mextemps">
                        <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'tres-pasos', 'valor' => 'contenido'])?>
                    </div>
                </div>
                <div class="col-12 col-lg-5">
                    <?php for($i = 1; $i <= 3; $i++){ ?>
                        <div class="w-100">
                            <div class="d-flex">
                                <div onmouseover="hover('<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'tres-pasos-'.$i, 'valor' => 'contenido'])?>')" class="btn-naranja-circular" style="min-width: 100px; max-width: 100px; min-height: 100px; max-height: 100px;">
                                    <img  src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'tres-pasos-'.$i, 'valor' => 'imagen'])?>" alt="">
                                </div>
                                <div class="px-3 align-self-center">
                                    <span class=" perfhov">
                                        <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'tres-pasos-'.$i, 'valor' => 'titulo'])?>
                                    </span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="pt-100 row porqueusar-movil">
            <div class="w-100">
                <span class="btn-imagen-naranja"><img src="https://tresesenta.lat/mextemps/pantallas/assets/imgs/como/pm1.svg" alt=""></span>
                <div class="px-2 align-self-center">
                    <span class="fw-bold font-lg eltitulo">Crea tu perfil de freelance en la plataforma</span>
                    <p style="font-size: 13px;" class="elparrafo d-none d-sm-block">
                        <strong>Únete a nuestra red.</strong> Regístrate completando tu perfil describe los servicios que ofreces, muestra tus talentos, portafolio y si deseas tu Curriculum e incluye todos aquellos detalles que harán que tu perfil se destaque entre la multitud.
                    </p>
                </div>
            </div>
            <div class="w-100">
                <span class="btn-imagen-naranja"><img src="https://tresesenta.lat/mextemps/pantallas/assets/imgs/como/pm2.svg" alt=""></span>
                <div class="px-2 align-self-center">
                    <span class="fw-bold font-lg eltitulo">Conectamos sin intermediarios</span>
                    <p style="font-size: 13px;" class="elparrafo d-none d-sm-block">
                        Busca el trabajo adecuado a tus habilidades y experiencia entre un sinfín de proyectos. <strong>Elije el paquete que mejor se adapte para ti</strong> y postúlate para ese proyecto. Contacta al contratista sin intermediarios y chatea en tiempo real el acuerdo para comenzar a trabajar. Prepárate para trabajar una vez que te contraten. Realiza tu trabajo de alta calidad y gana la cantidad acordada.
                    </p>
                </div>
            </div>
            <div class="w-100">
                <span class="btn-imagen-naranja"><img src="https://tresesenta.lat/mextemps/pantallas/assets/imgs/como/pm3.svg" alt=""></span>
                <div class="px-2 align-self-center">
                    <span class="fw-bold font-lg eltitulo">Administra tus proyectos</span>
                    <p style="font-size: 13px;" class="elparrafo d-none d-sm-block">
                        Mantenga un registro del progreso de las tareas con avances a través de la plataforma, en un entorno <strong>privado y seguro.</strong>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <script>
        function hover(description) {
            document.getElementById('trespasos').innerHTML = description;
        }
    </script>
    <section class="section-box mt-90 mt-md-0 oportunidades">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-7 col-sm-12 col-12">
                    <span class="text-lg text-brand wow animate__animated animate__fadeInUp"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'oportunidades', 'valor' => 'superior'])?></span>
                    <h3 class="mt-30 mb-40 wow animate__animated animate__fadeInUp" style="max-width: 500px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'oportunidades', 'valor' => 'titulo'])?></h3>
                    <div class="banner-imgs banner-imgs-about d-block d-md-none">
                        <img style="margin-bottom: -6px;" alt="Mextemps" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'oportunidades', 'valor' => 'imagen'])?>" class="img-responsive" />
                    </div>
                    <p class="mb-0 wow animate__animated animate__fadeInUp">
                        <?= str_replace(array('<p>', '</p>'), array('', ''), view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'oportunidades', 'valor' => 'descripcion'])) ?>
                    </p>
                    <div class="mt-40 boton-registrate wow animate__animated animate__fadeInUp">
                        <?php if(!session('logeado')){ ?>
                            <a href="<?=base_url('/registrarme/freelancer')?>" class="btn btn-default">Regístrate</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-5 col-sm-12 col-12 d-none d-md-block">
                    <div class="banner-imgs banner-imgs-about">
                        <img style="margin-bottom: -6px;" alt="Mextemps" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'oportunidades', 'valor' => 'imagen'])?>" class="img-responsive" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-box mt-80 mt-md-50 comentarios d-none d-lg-block">
        <div class="container">
            <h2 class="text-center titulos mb-15 section-title wow animate__animated animate__fadeInUp"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'comentarios', 'valor' => 'titulo'])?> <span class="naranja"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'comentarios', 'valor' => 'destacado'])?></span></h2>
            <div class="text-normal descripcion text-center color-black-5 box-mw-60 wow animate__animated animate__fadeInUp">
                <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'como-funciona-freelance', 'tipo' => 'comentarios', 'valor' => 'inferior'])?>
            </div>
            <?=view_cell('App\Libraries\FuncionesSistema::testimonios')?>
        </div>
    </section>
    <section class="section-box mt-50 mb-60 suscribete-ahora">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-8">
                    <div class="box-newsletter d-none d-md-block">
                        <h5 class="text-lg-newsletter text-center">Suscríbete ahora</h5>
                        <h6 class="text-md-newsletter text-center">Recibe aviso de los últimos proyectos publicados</h6>
                        <div class="box-form-newsletter mt-30">
                            <form class="form-newsletter needs-validation position-relative" id="boletines_suscripcion" novalidation encytpe="multipart/form-data">
                                <input type="email" class="input-newsletter" name="correo" id="correo" placeholder="usuario@gmail.com" required>
                                <div class="invalid-tooltip text-font">Requerido</div>
                                <button class="btn btn-default font-heading icon-send-letter" type="submit">Suscribirme</button>
                            </form>
                        </div>
                    </div>
                    <div class="box-newsletter d-block d-md-none">
                        <h5 class="text-white fw-bold">Suscríbete ahora</h5>
                        <h6 class="text-white fw-normal text-md mt-2">Recibe aviso de los últimos proyectos publicados</h6>
                        <div class="box-form-newsletter mt-30">
                           <form class="form-newsletter needs-validation position-relative" id="boletines_suscripcion" novalidation encytpe="multipart/form-data">
                                <input type="email" class="input-newsletter" name="correo" id="correo" placeholder="usuario@gmail.com" required>
                                <div class="invalid-tooltip text-font">Requerido</div>
                                <button class="btn btn-default font-heading icon-send-letter" type="submit" style="background-position: right 20px center;"></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?=$this->endSection()?>