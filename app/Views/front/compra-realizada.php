<?= $this->extend('front/main') ?>
<?= $this->section('title') ?> Pago completado <?= $this->endSection() ?>

<?= $this->section('content') ?>
<main class="container-fluid" <?= session('rol') != 'freelancer' ? 'style="background: #EFB506;"' : ''?>>
    <section class="position-relative p-5 pb-0 pe-0 ps-0">
        <img src="<?= base_url('assets/images/front/'.session('rol').'.webp')?>" alt="Fondo pagina gracias <?= session('rol')?>" style="object-fit:contain; width: 75%">
        <div class="d-flex justify-content-center aling-items-center flex-column gap-4 position-absolute start-50" style="top: 20%;">
            <?php $imagen = session('rol') == "freelancer" ? "/aprobada-freelancer.png" : "/aprobada.png"?>
            <img class="m-auto" src="<?= base_url("assets/images/emails/$imagen")?>" alt="Imagen de pago aprobado" style="width:200px; height:175px">
            <h1 class="display-3">¡Compra realizada!</h1>
            <p class="h5" style="text-align:justify;">
                Muchas gracias, te hemos enviado un correo de confirmación con todos los detalles.<br><br>
                <?php if(session('rol') == 'freelancer') :?>
                    Ahora ve, postulate a los mejores proyectos para ti y disfruta de una mejor experiencia
                <?php else :?>
                    Ahora ve, publica proyectos y encuentra el mejor talento. No esperes más.
                <?php endif ;?>
            </p>
            <div class="">
                <a class="btn btn-primary" href="<?= base_url('usuarios/perfil')?>">Continuar</a>
            </div>
        </div>
    </section>
</main>
<?= $this->endSection() ?>