<?=$this->extend('front/main')?>
<?=$this->section('title')?>
    Contacto
<?=$this->endSection()?>

<?=$this->section('content')?>
<main class="main">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 col-lg-12 m-auto">
                <section class="mb-50">
                    <h5 class="text-blue text-center wow animate__animated animate__fadeInUp" data-wow-delay=".1s">Ponte en contacto con nosotros</h5>
                    <div class="row">
                        <div class="col-xl-9 col-md-12 mx-auto">
                            <div class="contact-from-area padding-20-row-col">
                                <h2 class="section-title mt-15 mb-10 text-center wow animate__animated animate__fadeInUp" data-wow-delay=".1s">Escribenos</h2>
                                <div class="row mt-50">
                                    <div class="col-md-4 text-center wow animate__animated animate__fadeInUp" data-wow-delay=".1s">
                                        <img src="<?=base_url('assets/themes/imgs/theme/icons/headset-color.svg')?>" style="width: 40px;" alt="">
                                        <p class="text-muted font-xs mb-10">Teléfono</p>
                                        <p class="mb-0 font-lg">
                                            81 8181 8181
                                        </p>
                                    </div>
                                    <div class="col-md-4 mt-sm-30 text-center wow animate__animated animate__fadeInUp" data-wow-delay=".3s">
                                        <img src="<?=base_url('assets/themes/imgs/theme/icons/marker-color.svg')?>" style="width: 40px;" alt="">
                                        <p class="text-muted font-xs mb-10">Correo</p>
                                        <p class="mb-0 font-lg">
                                            contacto@mextemps.com
                                        </p>
                                    </div>
                                    <div class="col-md-4 mt-sm-30 text-center wow animate__animated animate__fadeInUp" data-wow-delay=".5s">
                                        <img src="<?=base_url('assets/themes/imgs/theme/icons/plane-color.svg')?>" style="width: 40px;" alt="">
                                        <p class="text-muted font-xs mb-10">Dirección</p>
                                        <p class="mb-0 font-lg">
                                            Nuevo León, México
                                        </p>
                                    </div>
                                </div>
                                <form class="contact-form-style mt-80 needs-validation" id="form-contacto" enctype="'multipart/form-data" novalidate>
                                    <div class="row wow animate__animated animate__fadeInUp" data-wow-delay=".1s">
                                        <div class="col-lg-4 col-md-4">
                                            <div class="input-style mb-20 position-relative">
                                                <input name="nombre" id="nombre" placeholder="Nombre:" required type="text" />
                                                <div class="invalid-tooltip text-font2">Requerido</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                            <div class="input-style mb-20 position-relative">
                                                <input name="correo" id="correo" placeholder="Correo electrónico:" required type="email" />
                                                <div class="invalid-tooltip text-font2">Requerido</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                            <div class="input-style mb-20 position-relative">
                                                <input name="telefono" id="telefono" placeholder="Teléfono:" required type="tel" />
                                                <div class="invalid-tooltip text-font2">Requerido</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 text-center">
                                            <div class="textarea-style mb-30 position-relative">
                                                <textarea placeholder="Mensaje:" name="mensaje" id="mensaje" required></textarea>
                                                <div class="invalid-tooltip text-font2">Requerido</div>
                                            </div>
                                            <button class="submit submit-auto-width" type="submit" id="enviar_correo">Enviar mensaje</button>
                                        </div>
                                    </div>
                                </form>
                                <p class="form-messege"></p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</main>
<script>
    $('#enviar_correo').click(function (e) { 
        if(e.target.form.nombre.value.length == 0 || e.target.form.telefono.value.length == 0 ||
        e.target.form.correo.value.length == 0 || e.target.form.mensaje.value.length == 0) return true;

        e.preventDefault();

        $(this).attr('disabled', true);
        $(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);
        $.ajax({
            type: "POST",
            url: "<?=base_url('Administrador/enviarContacto')?>",
            data: $('#form-contacto').serialize(),
            success: function (response) {
                let respuesta = JSON.parse(response);
                $('#form-contacto')[0].reset();
                alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
                $('#enviar_correo').removeAttr('disabled').children('span').remove();
            },
            error: function (response){
                alertify.notify(response.responseText, 'falla', 10);
            }
        });
    });
</script>
<?=$this->endSection()?>