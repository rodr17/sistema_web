<?php if(session('logeado')){ ?>
    <script type="module">
        // Initialize Firebase
        // Import the functions you need from the SDKs you need
        import {
            initializeApp
        } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
        import {
            getAnalytics
        } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-analytics.js";
        import {
            getFirestore
        } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";
        // TODO: Add SDKs for Firebase products that you want to use
        // https://firebase.google.com/docs/web/setup#available-libraries

        // Your web app's Firebase configuration
        // For Firebase JS SDK v7.20.0 and later, measurementId is optional
        let firebaseConfig = {
            apiKey: "AIzaSyCmtOCzTL7RjCi2ZkoJExiBmrGTxgnxN6Q",
            authDomain: "mextemps-341000.firebaseapp.com",
            projectId: "mextemps-341000",
            storageBucket: "mextemps-341000.appspot.com",
            messagingSenderId: "198033966029",
            appId: "1:198033966029:web:12659433a00b1e05e1fa84",
            measurementId: "G-SZY88RED1D"
        };
        // Initialize Firebase
        let app = initializeApp(firebaseConfig);
        let analytics = getAnalytics(app);
        let db = getFirestore(app);
        import {
            collection,
            addDoc,
            getDocs,
            query,
            onSnapshot,
            orderBy,
            serverTimestamp
        } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";
        
        <?php 
            (session('rol') == 'freelancer')? $columna = 'id_freelancer' : $columna = 'id_contratista';
            $data = model('Salas')->where([$columna => session('id')])->find();
        ?>
        <?php foreach($data as $d){ ?>
            <?php
                if($d['mensajes'] == null){
                    $total = 0;
                }else{
                    $datos = json_decode($d['mensajes'], true);
                    (session('rol') == 'freelancer')? $total = $datos['freelancer'] : $total = $datos['contratista'];
                }
            ?>
            $(function() {
                let q<?= $d['id'] ?> = query(collection(db, "chats/<?= $d['id'] ?>/messages"), orderBy("timestamp"));
                let unsubscribe = onSnapshot(q<?= $d['id'] ?>, (querySnapshot) => {
                    //const cities = [];
                    $('#contenedor-mensajes').empty();
                    // console.log(doc.data());
                    setInterval(function(){
                        let i = 0;
                        querySnapshot.forEach((doc) => {
                            i++;
                        });
                        if(i != <?=$total?>){
                            $('.btn[data-sala="<?=$d['id']?>"]').append('<span class="position-absolute start-100 translate-middle badge rounded-pill bg-danger" style="margin-top: -8px"><span class="tooltiptext" style="top: 0;left: 100%; font-size: 10px;">Nuevos mensajes</span></span>');
                            // $('#noti-header').removeClass('d-none');
                            // $('#noti-header').html('Nuevos mensajes');
                        }else{
                            // $('#noti-header').addClass('d-none');
                            // $('#noti-header').html('0');
                        }
                        console.log('Totales: '+i+'  Guardados: '+<?=$total?>);
                        i = 0;
                    }, 2000);
                });
            });
        <?php } ?>
    </script>
<?php } ?>