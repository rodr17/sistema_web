<!-- Footer -->
<footer class="footer <?=(!empty($navbar) == 'inicio')? 'bg-white' : ''?> mt-50">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-4">
				<a href="<?=base_url('')?>"><img alt="Mextemps" src="<?=base_url('assets/themes/imgs/theme/mextemps.svg')?>" style="width: 245px; margin-bottom: 20px;"/></a>
				<div class="col-md-12 text-md-start text-start">
					<div class="footer-social" style="padding-left: 25px;">
						<?php if(!empty(get_redSocial('facebook'))) :?>
							<a target="_blank" href="<?= get_redSocial('facebook') ?>" class="icon-socials icon-facebook"></a>
						<?php endif ;?>
						<?php if(!empty(get_redSocial('instagram'))) :?>
							<a target="_blank" href="<?= get_redSocial('instagram') ?>" class="icon-socials icon-instagram"></a>
						<?php endif ;?>
						<?php if(!empty(get_redSocial('linkedin'))) :?>
							<a target="_blank" href="<?= get_redSocial('linkedin') ?>" class="icon-socials icon-linkedin"></a>
						<?php endif ;?>
						<?php if(!empty(get_redSocial('youtube'))) :?>
							<a target="_blank" href="<?= get_redSocial('youtube') ?>" class="icon-socials icon-youtube"></a>
						<?php endif ;?>
					</div>
				</div>
			</div>
			<!-- <p class="contenedor-arrow-v">Down arrow: <i class="arrow-v down-v"></i></p> -->
			<div class="col-12 col-lg-8">
				<div class="row">
					<div class="col-6 col-md-3 col-lg-3">
						<div class="d-block d-lg-none">
							<h6 class="titulo-footer contenedor-arrow-v" data-bs-toggle="collapse" href="#footer-collapse-1" role="button" aria-expanded="false" aria-controls="footer-collapse-1">
								¿Cómo funciona? <i class="arrow-v down-v"></i>
							</h6>
							<div class="collapse" id="footer-collapse-1">
								<ul class="menu-footer mt-40 p-0">
									<li><a href="<?=base_url('/como-funciona-freelance')?>">Freelancers</a></li>
									<li><a href="<?=base_url('/como-funciona-contratista')?>">Contratantes</a></li>
									<?php if(!session('logeado')){?>
										<li><a href="<?=base_url('/ingresar')?>">Iniciar sesión</a></li>
									<?php }else{ ?>
										<li><a href="<?=base_url('/logout')?>">Cerrar sesión</a></li>
									<?php } ?>
								</ul>
							</div>
						</div>
						<div class="d-none d-lg-block">
							<h6 class="titulo-footer">
								¿Cómo funciona?
							</h6>
							<ul class="menu-footer mt-40 p-0">
								<li><a href="<?=base_url('/como-funciona-freelance')?>">Freelancers</a></li>
								<li><a href="<?=base_url('/como-funciona-contratista')?>">Contratantes</a></li>
								<?php if(!session('logeado')){?>
									<li><a href="<?=base_url('/ingresar')?>">Iniciar sesión</a></li>
								<?php }else{ ?>
									<li><a href="<?=base_url('/logout')?>">Cerrar sesión</a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
					<div class="col-6 col-md-3 col-lg-3">
						<div class="d-block d-lg-none">
							<h6 class="titulo-footer contenedor-arrow-v" data-bs-toggle="collapse" href="#footer-collapse-2" role="button" aria-expanded="false" aria-controls="footer-collapse-2">
								Legal <i class="arrow-v down-v"></i>
							</h6>
							<div class="collapse" id="footer-collapse-2">
								<ul class="menu-footer mt-40 p-0">
									<li><a href="<?=base_url('/terminos-y-condiciones')?>">Términos y Condiciones</a></li>
									<li><a href="<?=base_url('/aviso-de-privacidad')?>">Aviso de Privacidad</a></li>
									<li><a href="<?=base_url('/soporte')?>">FAQ</a></li>
								</ul>
							</div>
						</div>
						<div class="d-none d-lg-block">
							<h6 class="titulo-footer">
								Legal
							</h6>
							<ul class="menu-footer mt-40 p-0">
								<li><a href="<?=base_url('/terminos-y-condiciones')?>">Términos y Condiciones</a></li>
								<li><a href="<?=base_url('/aviso-de-privacidad')?>">Aviso de Privacidad</a></li>
								<li><a href="<?=base_url('/soporte')?>">FAQ</a></li>
							</ul>
						</div>
					</div>
					<div class="col-6 col-md-3 col-lg-3">
						<div class="d-block d-lg-none">
							<h6 class="titulo-footer contenedor-arrow-v" data-bs-toggle="collapse" href="#footer-collapse-3" role="button" aria-expanded="false" aria-controls="footer-collapse-3">
								Ayuda <i class="arrow-v down-v"></i>
							</h6>
							<div class="collapse" id="footer-collapse-3">
								<ul class="menu-footer mt-40 p-0">
									<li><a href="<?=base_url('/soporte')?>">Contacto</a></li>
									<li><a href="<?=base_url('/soporte')?>">Centro de ayuda</a></li>
									<li><a href="<?=base_url('/blog')?>">Blog</a></li>
								</ul>
							</div>
						</div>
						<div class="d-none d-lg-block">
							<h6 class="titulo-footer">Ayuda</h6>
							<ul class="menu-footer mt-40 p-0">
								<li><a href="<?=base_url('/soporte')?>">Contacto</a></li>
								<li><a href="<?=base_url('/soporte')?>">Centro de ayuda</a></li>
								<li><a href="<?=base_url('/blog')?>">Blog</a></li>
							</ul>
						</div>
					</div>
					<div class="col-6 col-md-3 col-lg-3">
						<div class="d-block d-lg-none">
							<h6 class="titulo-footer contenedor-arrow-v" data-bs-toggle="collapse" href="#footer-collapse-4" role="button" aria-expanded="false" aria-controls="footer-collapse-4">
								Comunidad <i class="arrow-v down-v"></i>
							</h6>
							<div class="collapse" id="footer-collapse-4">
								<ul class="menu-footer mt-40 p-0">
									<li><a href="<?=base_url('/registrarme-como')?>">Regístrate</a></li>
									<li><a href="<?=base_url('/registrarme/contratista')?>">Registra tu Empresa</a></li>
									<li><a href="<?=base_url('/registrarme/freelancer')?>">Sube tu portafolio</a></li>
								</ul>
							</div>
						</div>
						<div class="d-none d-lg-block">
							<h6 class="titulo-footer">Comunidad</h6>
							<ul class="menu-footer mt-40 p-0">
								<li><a href="<?=base_url('/registrarme-como')?>">Regístrate</a></li>
								<li><a href="<?=base_url('/registrarme/contratista')?>">Registra tu Empresa</a></li>
								<li><a href="<?=base_url('/registrarme/freelancer')?>">Sube tu portafolio</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-bottom mt-50">
			<div class="row">
				<div class="col-md-12" style="text-align: center;">
					Todos los derechos reservados por MexTemps 2022
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- End Footer -->
<?php if(session('logeado')) :?>
	<a class="d-none d-md-block" id="freelancer-buscar" href="<?= session('rol') == 'freelancer' ? base_url('/buscar-proyecto?search=&area=') : base_url('/proyecto')?>" style="position: fixed;" title="<?= session('rol') == 'freelancer' ? 'Buscar proyectos' : 'Publicar proyecto'?>">
		<div class="tooltips">
			<?= session('rol') == 'freelancer' ? '<i class="fi-rr-search"></i>' : '<i class="fi-rr-plus"></i>'?>
			<span class="tooltiptext" style="top: 0;left: 100%;"><?= session('rol') == 'freelancer' ? 'Buscar proyectos' : 'Publicar proyecto'?></span>
		</div>
	</a>
<?php endif ;?>