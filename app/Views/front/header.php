<?php
	$notificaciones = view_cell('App\Libraries\FuncionesSistema::cantAlertas', ['id' => session('id')]);
	$usuario = word_limiter(session('usuario') ?? '', 2, '');
?>
<style type="text/css">
	ul.nav a:focus, ul.nav button:focus {
		outline: 0 !important;
	}
	ul.nav li{
		color: #FE5000;
	}
	ul.nav a{
		font-size: 15px;
		line-height: 1;
		text-transform: capitalize;
		font-weight: 500;
		position: relative;
		display: flex;
		align-items: center;
		color: #636477;
		-webkit-transition-duration: 0.2s;
		transition-duration: 0.2s;
	}
	.btn.text-start i{
		position: absolute;
		right: 5px;
	}
</style>
<header class="header sticky-bar">
	<div class="container">
		<!-- <div class="main-header"> -->
		<div class="nuevo-header">
			<div class="header-left d-flex justify-content-between align-items-start">
				<div class="header-logo">
					<a href="<?=base_url('')?>" class="d-flex"><img alt="Logo Mextemps Freelancer" src="<?=base_url('assets/themes/imgs/theme/mextemps.svg')?>" width="218" /></a>
				</div>
				<div class="search-box d-block d-lg-none">
				<a class="btn-search" href="<?= session('rol') == 'freelancer' ? base_url('buscar-proyecto?search=&area=') : (session('rol') == 'contratante' ? base_url('usuarios/talento') : base_url('ingresar'))?>"><i class="fas fa-search"></i></a>
					<input type="search" class="input-search" name="search" placeholder="Buscar...">
				</div>
				<div class="header-nav">
					<nav class="nav-main-menu d-none d-xl-block">
						<ul class="main-menu">
							<li class="">
								<a class="active" href="<?=base_url('')?>">Inicio</a>
							</li>
							<li class="has-children">
								<a href="#">Cómo funciona</a>
								<ul class="sub-menu">
									<li>
										<a href="<?=base_url('/como-funciona-freelance')?>">
											Freelancer
											<div class="tooltips"><i style="color: #003B71;" class="fas fa-info-circle"></i>
												<span class="tooltiptext">Freelancer: es aquella persona que trabaja por su cuenta, de manera independiente otorgando algún servicio.</span>
											</div>
										</a>
									</li>
									<li>
										<a href="<?=base_url('/como-funciona-contratista')?>">
											Contratante 
											<div class="tooltips"><i style="color: #003B71;" class="fas fa-info-circle"></i>
												<span class="tooltiptext">Contratante: es aquella persona que contrata al Freelancer.</span>
											</div>
										</a>
									</li>
								</ul>
							</li>
							<li class="has-children">
								<a href="#">Tarifas</a>
								<ul class="sub-menu">
									<li><a href="<?=base_url('/tarifas-freelance')?>">Tarifas Freelancer</a></li>
									<li><a href="<?=base_url('/tarifas-contratista')?>">Tarifas Contratante</a></li>
								</ul>
							</li>
							<li class="">
								<a href="<?=base_url('soporte')?>">Soporte</a>
							</li>
							<li class="">
								<a href="<?=base_url('blog')?>">Blog</a>
							</li>
							<?php if(session('logeado')) :?>
								<li class="">
									<a href="<?= session('rol') == 'freelancer' ? base_url('buscar-proyecto?search=&area=') : (session('rol') == 'contratante' ? base_url('usuarios/talento') : base_url('ingresar'))?>">
										<?= session('rol') == 'freelancer' ? 'Buscar proyectos' : (session('rol') == 'contratante' ? 'Buscar talentos' : '')?>
									</a>
								</li>
							<?php endif ;?>
							<li class="has-children">
								<?php if(autorizacion() == 'freelancer') :?>
									<a style="margin-top: -10px;" class="position-relative" href="<?=base_url('/usuarios/perfil')?>"><img style="margin-top: -8px;" class="align-middle img-perfil-icon" src="<?= imagenPerfil(session('imagen')) ?>"> 
										<?= $usuario ?>
										<?php if(!empty($notificaciones)) :?>
											<span id="noti-header" class="translate-middle-y notificacion-header bg-danger" style="top: 50%; left: 95%;"><?= $notificaciones ?></span>
										<?php endif;?>
									</a>
									<ul class="sub-menu">
										<li>
											<a href="<?= base_url('usuarios/solicitudes') ?>">
												Solicitudes
												<?php $cantidad_solicitudes = model('Solicitudes')->where(['solicitudes.id_freelancer' => session('id'), 'solicitudes.estatus' => 'espera'])->join('proyectos', 'solicitudes.id_proyecto = proyectos.id')->countAllResults() ?>
												<?php if(!empty($cantidad_solicitudes)) :?>
													<span class="translate-middle bg-danger notificacion-header">
            											<?= $cantidad_solicitudes ?>
            										</span>
        										<?php endif;?>
										    </a>
										</li>
										<li><a href="<?= base_url('usuarios/perfil') ?>">Mi perfil</a></li>
										<li><a href="<?= base_url('logout') ?>">Cerrar sesión</a></li>
									</ul>
								<?php elseif(autorizacion() == 'contratante') :?>
									<a style="margin-top: -10px;" class="position-relative" href="<?=base_url('/usuarios/perfil')?>">
										<img style="margin-top: -8px;" class="align-middle img-perfil-icon" src="<?= imagenPerfil(session('imagen')) ?>"> 
										<?= $usuario ?>
										<?php if(!empty($notificaciones)) :?>
											<span id="noti-header" class="translate-middle-y notificacion-header bg-danger" style="top: 50%; left: 95%;"><?= $notificaciones ?></span>
										<?php endif;?>
									</a>
									<ul class="sub-menu">
										<li><a href="<?= base_url('usuarios/talento') ?>">Buscar talentos</a></li>
										<li><a href="<?= base_url('proyecto') ?>">Publicar proyecto</a></li>
										<li><a href="<?= base_url('proyectos-postulados') ?>">Proyectos con postulaciones</a></li>
										<li><a href="<?= base_url('usuarios/perfil') ?>">Mi perfil</a></li>
										<li><a href="<?= base_url('logout') ?>">Cerrar sesión</a></li>
									</ul>
								<?php else :?>
									<a><i class="fas fa-user-circle" aria-hidden="true"></i>Mi cuenta</a>
									<ul class="sub-menu">
										<li><a href="<?=base_url('/ingresar')?>">Iniciar sesión</a></li>
										<li><a href="<?=base_url('/registrarme-como')?>">Registrarse</a></li>
									</ul>
								<?php endif ;?>
							</li>
						</ul>
					</nav>
					<div class="burger-icon burger-icon-white">
						<span class="burger-icon-top"></span>
						<span class="burger-icon-mid"></span>
						<span class="burger-icon-bottom"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- MOVIL -->
	<div class="burger-icon burger-icon-white" type="button" data-bs-toggle="offcanvas" data-bs-target="#menu_headerMovil" aria-controls="menu_headerMovil">
		<span class="burger-icon-top"></span>
		<span class="burger-icon-mid"></span>
		<span class="burger-icon-bottom"></span>
	</div>
	<div class="offcanvas offcanvas-end" tabindex="-1" id="menu_headerMovil" aria-labelledby="menu_headerMovil_titulo">
		<div class="offcanvas-header">
			<?php if(autorizacion() == 'freelancer') :?>
			<div class="user-account">
					<img src="<?= imagenPerfil(session('imagen'))?>" alt="Foto perfil de <?= $usuario ?>" />
					<div class="content">
						<h6 class="user-name">Hola, <span class="text-brand"><?=session('usuario')?></span> <span onclick="location.href = '<?=base_url('/usuarios/perfil')?>';" class="badge bg-danger notificacion-header"><?= $notificaciones ?></span></h6>
					</div>
				</div>
			<?php elseif(autorizacion() == 'contratante') :?>
				<div class="user-account">
					<img src="<?= imagenPerfil(session('imagen'))?>" alt="Foto perfil de <?= $usuario ?>" />
					<div class="content">
						<h6 class="user-name">Hola, <span class="text-brand"><?=session('usuario')?></span> <span onclick="location.href = '<?=base_url('/usuarios/perfil')?>';" class="badge bg-danger notificacion-header"><?= $notificaciones ?></span></h6>
					</div>
				</div>
			<?php endif ;?>
		</div>
		<div class="offcanvas-body">
			<ul class="nav flex-column">
				<li class="nav-item">
					<a class="btn text-start" href="<?=base_url()?>">Inicio</a>
				</li>
				<li class="nav-item">
					<a class="btn text-start collapsed" data-bs-toggle="collapse" href="#collapseComo" role="button" aria-expanded="false" aria-controls="collapseComo">
						<span>Como funciona</span><i class="arrow-v down-v"></i>
					</a>
					<div class="collapse px-4" id="collapseComo">
						<ul class="d-block">
							<li>
								<a href="<?=base_url('/como-funciona-freelance')?>">Freelancer</a>
							</li>
							<li>
								<a href="<?=base_url('/como-funciona-contratista')?>">Contratante</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="nav-item">
					<a class="btn text-start collapsed" data-bs-toggle="collapse" href="#collapseTarifas" role="button" aria-expanded="false" aria-controls="collapseTarifas">
						<span>Tarifas</span><i class="arrow-v down-v"></i>
					</a>
					<div class="collapse px-4" id="collapseTarifas">
						<ul class="d-block">
							<li>
								<a href="<?=base_url('/tarifas-freelance')?>">Tarifas Freelancer</a>
							</li>
							<li>
								<a href="<?=base_url('/tarifas-contratista')?>">Tarifas Contratante</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="nav-item">
					<a href="<?=base_url('/soporte')?>" class="btn text-start">Soporte</a>
				</li>
				<li class="nav-item">
					<a href="<?=base_url('/blog')?>" class="btn text-start">Blog</a>
				</li>
				<li class="nav-item">
					<?php if(session('logeado')){ ?>
						<a class="btn text-start" href="<?= session('rol') == 'freelancer' ? base_url('buscar-proyecto?search=&area=') : (session('rol') == 'contratante' ? base_url('usuarios/talento') : base_url('ingresar'))?>">
							<?= session('rol') == 'freelancer' ? 'Buscar Proyectos' : (session('rol') == 'contratante' ? 'Buscar Talentos' : '')?>
						</a>
					<?php } ?>
				</li>
			</ul>
			<div class="mobile-account ps-2">
				<h6 class="mb-10">Mi cuenta</h6>
				<ul class="mobile-menu font-heading">
					<?php if(autorizacion() == 'freelancer') :?>
						<li style="display: none;"><a href="<?= base_url('buscar-proyecto?search=&area=') ?>">Buscar proyectos</a></li>
						<li><a href="<?= base_url('usuarios/solicitudes') ?>">Solicitudes</a></li>
						<li><a href="<?= base_url('usuarios/perfil') ?>">Mi perfil</a></li>
						<li><a href="<?= base_url('logout') ?>">Cerrar sesión</a></li>
					<?php elseif(autorizacion() == 'contratante') :?>
						<li><a href="<?= base_url('usuarios/talento') ?>">Buscar talentos</a></li>
						<li><a href="<?= base_url('proyecto') ?>">Publicar proyecto</a></li>
						<li><a href="<?= base_url('usuarios/perfil') ?>">Mi perfil</a></li>
						<li><a href="<?= base_url('logout') ?>">Cerrar sesión</a></li>
					<?php else :?>
						<li><a href="<?=base_url('ingresar')?>">Iniciar sesión</a></li>
						<li><a href="<?=base_url('registrarme-como')?>">Registrarse</a></li>
					<?php endif ;?>
				</ul>
			</div>
		</div>
	</div>
<!--End header-->