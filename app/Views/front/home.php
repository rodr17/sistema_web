<?=$this->extend('front/main')?>
<?=$this->section('title')?>
	Inicio
<?=$this->endSection()?>
<?=$this->section('estilos')?>
<style>
	/* Make the image fully responsive */
	.carousel-inner img {
		width: 100vmin;
		height: auto;
		object-fit: fill;
		max-height: 600px;
	}
	.quienes-somos{
		background-image: url('<?=base_url('/assets/images/fondo-quienes-somos.webp')?>');
		padding-top: 20px;
		padding-bottom: 20px;
		background-size: cover;
		background-position: center;
	}
	.quienes-somos li{
		font-size: 12px;
	}
	.img-quienes-somos{
		max-width: 350px;
	}
	.list-group-item{
		border: 0;
		padding-top: .25rem;
		padding-bottom: .25rem;
		padding-left: 1.5rem;
		background: #DFDFDF;
	}
	.list-group .list-group-item:first-child{
		padding-top: .75rem;
		padding-bottom: .75rem;
		font-weight: bold;
		font-size: 18px;
	} 
</style>
<?= view('/front/viewcells/styles') ?>
<?=$this->endSection()?>

<?=$this->section('content')?>
<div class="portada d-none d-lg-block">
	<div id="slider" class="carousel slide mtslide mt-70 portada-slider" data-bs-ride="carousel">
		<div class="carousel-indicators">
			<?php foreach($slides as $key => $s) { ?>
				<button type="button" data-bs-target="#slider" data-bs-slide-to="<?=$key?>" <?= ($key == 0)? 'class="active" aria-current="true"' : '' ?> aria-label="Slide <?=($key + 1)?>"></button>
			<?php } ?>
		</div>
		<div class="carousel-inner">
			<?php foreach($slides as $key => $s) {?>
				<?php $datos = json_decode($s['info'], true); ?>
				<div class="carousel-item <?= ($key == 0)? 'active' : '' ?> slide-<?=$key?>-home" data-bs-interval="5000" style="text-align: right;  background-size: auto; width: 100%; max-width: 100%; max-height: 600px; height: 600px; background-position: right; background-size: cover;">
					<div class="carousel-caption" style="text-align: justify; position: absolute; bottom: 30%;">
						<h1 class="text-white d-inline-block mt-4 fw-bold" style="text-shadow: #000 2px 2px 10px;"><?=$datos['titulo']?></h1>
						<p style="margin-bottom: 10px; text-shadow: #000 2px 2px 5px; font-size: 18px;"><?=$datos['subtitulo']?></p>
						<?= view_cell('App\Libraries\FuncionesSistema::formBuscador', ['buscador' => $datos['buscador']]) ?>
						<div class="d-flex gap-2 mt-3">
							<?php if(session('rol') == 'contratante') : ?>
								<a class="btn btn-azul-home-slider shadow" href="<?=base_url('/proyecto')?>">Publica tu proyecto</a>
							<?php endif ?>
							<?php if(!session('logeado')) : ?>
								<a class="btn btn-blanco shadow" href="<?=base_url('/registrarme-como')?>">Regístrate</a>
							<?php endif ?>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
		<button class="carousel-control-prev" type="button" data-bs-target="#slider" data-bs-slide="prev" style="top: 50%;">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="visually-hidden">Anterior</span>
		</button>
		<button class="carousel-control-next" type="button" data-bs-target="#slider" data-bs-slide="next" style="top: 50%;">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="visually-hidden">Siguiente</span>
		</button>
	</div>
</div>
<div class="portada d-block d-lg-none">
	<div id="slider-movil" class="carousel slide mtslide mt-70 portada-slider" data-bs-ride="carousel">
		<div class="carousel-indicators">
			<?php foreach($slides as $key => $s) { ?>
				<button type="button" data-bs-target="#slider" data-bs-slide-to="<?=$key?>" <?= ($key == 0)? 'class="active" aria-current="true"' : '' ?> aria-label="Slide <?=($key + 1)?>"></button>
			<?php } ?>
		</div>
		<div class="carousel-inner" style="background: #ffffff;">
			<?php foreach($slides as $key => $s) {?>
				<?php $datos = json_decode($s['info'], true);?>
				<div class="carousel-item <?= ($key == 0)? 'active' : '' ?> slide-<?=$key?>-home" data-bs-interval="5000" style="text-align: right; background-color: transparent; background-size: auto; width: 100%; max-width: 100%; background-position: right; background-size: cover;">
					<picture>
						<source srcset="<?=$datos['tableta']?>" media="(min-width: 600px)">
						<img src="<?=$datos['movil']?>" class="d-block w-100" alt="banner">
					</picture>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="container text-center slider-separado">
		<?php $datos = json_decode($slides[0]['info'], true);?>
		<h3 class=" d-inline-block mt-4 fw-bold h3" >
			<?=$datos['titulo']?>
		</h3>
		<p style="margin-bottom: 10px; font-size: 12px;" class="d-none-s subtitulos">
			<?=$datos['subtitulo']?>
		</p>
		<div class="d-none d-lg-block">
			<?= view_cell('App\Libraries\FuncionesSistema::formBuscador') ?>
		</div>
		<div class="col-12 mt-4 pb-4 pb-lg-5 botones-portada-mn">
			<?php if(!session('logeado')){ ?>
				<a class="btn btn-azul-home-slider shadow btn-sm-banner" href="<?=base_url('/ingresar')?>"><small>Publica tu proyecto</small></a>
			<?php }else{ ?>
				<?php if(session('rol') == 'contratante'){ ?>
					<a class="btn btn-azul-home-slider shadow btn-sm-banner" href="<?=base_url('/usuarios/talento')?>"><small>Buscar talento</small></a>
				<?php }elseif(session('rol') == 'freelancer'){ ?>
					<a class="btn btn-azul-home-slider shadow btn-sm-banner" href="<?= base_url('/buscar-proyecto?search=&area=') ?>"><small>Buscar proyectos</small></a>
				<?php } ?>
			<?php } ?>
			<?php if(!session('logeado')){ ?>
				<a class="btn btn-blanco shadow btn-sm-banner" style="margin-left: 10px;" href="<?=base_url('/registrarme-como')?>"><small>Regístrate</small></a>
			<?php } ?>
		</div>
	</div>
</div>
<div class="portada">
	<main class="main main-portada" style="padding-top: 50px;">
		<!--- CATEGORIAS MAS BUSCADAS --->
		<div class="d-none d-md-block los-mas-buscador-modulo">
			<div class="section-box wow animate__animated animate__fadeIn mt-0 masbslide">
				<div class="container">
					<div class="text-center">
						<h2 class="fw-bold">Las más buscadas</h2>
						<p class="text-secundario">Te presentamos las categorías más buscadas en Mextemps.</p>
					</div>
					<!-- ESCRITORIO -->
					<div class="d-none d-lg-block">
						<div class="mt-40 mb-80 row justify-content-center carousel slide overflow-hidden px-0" data-bs-ride="carousel" id="carrusel-escritorio" style="border-radius:30px">
							<div class="col-10 carousel-inner px-0">
								<?php for($a = 0; $a < 3; $a++){ ?>
									<div class="carousel-item <?=($a == 0)? 'active' : ''?>">
										<div class="row">
											<?php for($b = 1; $b <= 4; $b++){ ?>
												<div class="col-lg-3 col-sm-6">
													<div class="contenedor-buscado">
														<a href="<?=base_url('/buscar-proyecto?search=&area='.view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-'.$b, 'valor' => 'categoria']))?>" class="text-decoration-none">
															<img class="img-carrusel rounded-3" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-'.$b, 'valor' => 'imagen'])?>">
															<p class="text-white position-relative text-truncate bottom-0 ms-3" style="margin-top: -30px; font-size: 20px;"><i class="fas fa-laptop-code d-none" aria-hidden="true" ></i> <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-'.$b, 'valor' => 'titulo'])?></p>
														</a>
													</div>
												</div>
											<?php } ?>
										</div>
									</div>
								<?php } ?>
							</div>
							<button class="carousel-control-prev btn-Iqz" type="button" data-bs-target="#carrusel-escritorio" data-bs-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="visually-hidden">Anterior</span>
							</button>
							<button class="carousel-control-next btn-Der" type="button" data-bs-target="#carrusel-escritorio" data-bs-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="visually-hidden">Siguiente</span>
							</button>
						</div>
					</div>
					<!-- TABLETS -->
					<div class="d-none d-md-block d-lg-none">
						<div class="mt-40 mb-80 justify-content-center carousel slide overflow-hidden px-0" data-bs-ride="carousel" id="carrusel-tablet" style="border-radius: 20px;">
							<?php for($a = 1; $a < 3; $a++){ ?>
								<div class="carousel-item <?= ($a == 1)? 'active' : '' ?>">
									<div class="row">
										<div class="col pe-0">
											<div class="contenedor-buscado">
												<a href="<?=base_url('/buscar-proyecto?search=&area='.view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-1', 'valor' => 'categoria']))?>" class="text-decoration-none">
													<img class="img-carrusel rounded-3 h-100" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-1', 'valor' => 'imagen'])?>">
													<p class="text-white position-relative text-truncate bottom-0 px-2" style="margin-top: -30px; font-size: 14px;"><i class="fas fa-laptop-code d-none" aria-hidden="true" ></i> <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-1', 'valor' => 'titulo'])?></p>
												</a>
											</div>
										</div>
										<div class="col pe-0">
											<div class="contenedor-buscado">
												<a href="<?=base_url('/buscar-proyecto?search=&area='.view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-2', 'valor' => 'categoria']))?>" class="text-decoration-none">
													<img class="img-carrusel rounded-3 h-100" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-2', 'valor' => 'imagen'])?>">
													<p class="text-white position-relative text-truncate bottom-0 px-2" style="margin-top: -30px; font-size: 14px;"><i class="fas fa-laptop-code d-none" aria-hidden="true" ></i> <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-2', 'valor' => 'titulo'])?></p>
												</a>
											</div>
										</div>
										<div class="col pe-0">
											<div class="contenedor-buscado">
												<a href="<?=base_url('/buscar-proyecto?search=&area='.view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-3', 'valor' => 'categoria']))?>" class="text-decoration-none">
													<img class="img-carrusel rounded-3 h-100" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-3', 'valor' => 'imagen'])?>">
													<p class="text-white position-relative text-truncate bottom-0 px-2" style="margin-top: -30px; font-size: 14px;"><i class="fas fa-laptop-code d-none" aria-hidden="true" ></i> <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-3', 'valor' => 'titulo'])?></p>
												</a>
											</div>
										</div>
										<div class="col pe-0">
											<div class="contenedor-buscado">
												<a href="<?=base_url('/buscar-proyecto?search=&area='.view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-4', 'valor' => 'categoria']))?>" class="text-decoration-none">
													<img class="img-carrusel rounded-3 h-100" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-4', 'valor' => 'imagen'])?>">
													<p class="text-white position-relative text-truncate bottom-0 px-2" style="margin-top: -30px; font-size: 14px;"><i class="fas fa-laptop-code d-none" aria-hidden="true" ></i> <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-4', 'valor' => 'titulo'])?></p>
												</a>
											</div>
										</div>
									</div>
								</div>
							<?php } ?>
							<!-- <button class="carousel-control-prev btn-Iqz" style="top: 35%;" type="button" data-bs-target="#carrusel-tablet" data-bs-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="visually-hidden">Anterior</span>
							</button>
							<button class="carousel-control-next btn-Der" style="top: 35%;" type="button" data-bs-target="#carrusel-tablet" data-bs-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="visually-hidden">Siguiente</span>
							</button> -->
						</div>
					</div>
					<!-- CELULARES -->
					<div class="d-block d-md-none d-lg-none">
						<div class="row justify-content-center">
							<div class="col-10">
								<div class="mt-40 mb-80 justify-content-center carousel slide overflow-hidden px-0" data-bs-ride="carousel" id="carrusel-movil" style="border-radius: 30px;">
									<?php for($a = 1; $a < 5; $a++){ ?>
										<div class="carousel-item <?= ($a == 1)? 'active' : '' ?>">
											<div class="contenedor-buscado">
												<a href="<?=base_url('/buscar-proyecto?search=&area='.view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-'.$a, 'valor' => 'categoria']))?>" class="text-decoration-none">
													<img class="img-carrusel rounded-3 h-100" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-'.$a, 'valor' => 'imagen'])?>">
													<p class="text-white position-relative text-truncate bottom-0 ms-3" style="margin-top: -30px; font-size: 20px;"><i class="fas fa-laptop-code d-none" aria-hidden="true" ></i> <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-'.$a, 'valor' => 'titulo'])?></p>
												</a>
											</div>
										</div>
									<?php } ?>
									<button class="carousel-control-prev btn-Iqz" type="button" data-bs-target="#carrusel-movil" data-bs-slide="prev">
										<span class="carousel-control-prev-icon" aria-hidden="true"></span>
										<span class="visually-hidden">Anterior</span>
									</button>
									<button class="carousel-control-next btn-Der" type="button" data-bs-target="#carrusel-movil" data-bs-slide="next">
										<span class="carousel-control-next-icon" aria-hidden="true"></span>
										<span class="visually-hidden">Siguiente</span>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--- SECCION QUIENES SOMOS --->
		<div class="section-box quienes-somos d-none d-md-block d-lg-none">
			<div class="container">
				<div class="row justify-content-center align-items-center mt-50 mb-50">
                    <div class="col-12">
                        <h2 class="text-center fw-bold"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'quienes-somos', 'valor' => 'titulo'])?></h2>
                    </div>
					<div class="col-5 text-center align-self-center">
						<img class="img-quienes-somos w-100 border border-3 shadow-sm" style="border-radius: 16px;" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'quienes-somos', 'valor' => 'imagen'])?>">
					</div>
					<div class="col-7 align-self-center">
						<div class="cont-quienes-somos">
							<label class="superior text-start"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'quienes-somos', 'valor' => 'frase'])?></label>
							<div class="fw-bold h5 text-start mb-0"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'quienes-somos', 'valor' => 'parrafo'])?></div>
							<div class="my-3 mb-4 text-start descripcion">
								<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'quienes-somos', 'valor' => 'parrafo2'])?>
							</div>
							<p class="h6"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'quienes-somos', 'valor' => 'subtitulo'])?></p>
							<div class="row justify-content-start">
								<div class="col-12 col-md-6">
									<ul class="lista-quienes-somos">
										<li>Conectamos a expertos profesionales sin intermediarios</li>
									</ul>
								</div>
								<div class="col-12 col-md-6">
									<ul class="lista-quienes-somos">
										<li>Más ofertas de trabajo para freelancers</li>
									</ul>
								</div>
								<div class="col-10">
									<ul class="lista-quienes-somos">
										<li>Paquetes flexibles para permitirle trabajar de la manera que desee</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--- CATEGORIAS ICONOS --->
		<section class="section-box categorias-portada">
			<div class="container">
				<div class="row align-items-end">
					<div class="col--lg-12 text-center">
						<h2 class="text-titulo-categoria p-5">
							<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categorias', 'valor' => 'titulo'])?>
						</h2>
					</div>
				</div>
				<div class="d-none d-lg-block">
					<div class="row mt-70 iconos-d-categorias">
						<?php for($i = 1; $i <= 8; $i++){ ?>
							<div class="col-lg-3 col-md-4 col-sm-6 col-12">
								<div class="card-grid border-naranja hover-up wow animate__animated animate__fadeInUp sinborde">
									<div class="text-center">
										<a href="<?=base_url('/buscar-proyecto?search=&area='.view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-inicio-'.$i, 'valor' => 'categoria']))?>">
											<lord-icon
												src="https://cdn.lordicon.com/<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-inicio-'.$i, 'valor' => 'icono'])?>.json"
												trigger="loop"
												delay="2500"
												colors="primary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'principal'])?>,secondary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'secundario'])?>"
												style="width:105px;height:105px">
											</lord-icon>
										</a>
									</div>
									<h5 class="text-center mt-20 card-heading ">
										<a href="<?=base_url('/buscar-proyecto?search=&area='.view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-inicio-'.$i, 'valor' => 'categoria']))?>">
											<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-inicio-'.$i, 'valor' => 'categoria'])?>
										</a>
									</h5>
								</div>
							</div>
						<?php } ?>
						<div class="col-12 text-center mt-4">
							<div class="circulo-portada-nj">
								<a class="btn btn-naranja-circular-home-dos text-font px-3" href="<?=base_url('/buscar-proyecto?search=&area=')?>">TODOS LOS<br>PROYECTOS</a>
							</div>
						</div>
					</div>
				</div>
				<div class="d-block d-lg-none">
					<div class="row mt-70">
						<?php for($i = 1; $i <= 8; $i++){ ?>
							<?php if((view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-inicio-'.$i, 'valor' => 'categoria']) != 'Educación') && (view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-inicio-'.$i, 'valor' => 'categoria']) != 'Ingeniería y Arquitectura')){ ?>
								<div class="col-6 col-md-4">
									<div class="card-grid border-naranja hover-up wow animate__animated animate__fadeInUp sinborde">
										<div class="text-center">
											<a href="<?=base_url('/buscar-proyecto?search=&area='.view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-inicio-'.$i, 'valor' => 'categoria']))?>">
												<lord-icon
													src="https://cdn.lordicon.com/<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-inicio-'.$i, 'valor' => 'icono'])?>.json"
													trigger="loop"
													delay="1500"
													colors="primary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'principal'])?>,secondary:<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'general', 'tipo' => 'colores', 'valor' => 'secundario'])?>"
													style="width:80px;height:80px">
												</lord-icon>
											</a>
										</div>
										<h6 class="text-center mt-20 card-heading fs-12">
											<a href="<?=base_url('/buscar-proyecto?search=&area='.view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-inicio-'.$i, 'valor' => 'categoria']))?>">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-inicio-'.$i, 'valor' => 'categoria'])?>
											</a>
										</h6>
									</div>
								</div>
							<?php } ?>
						<?php } ?>
						<div class="col-12 text-center mt-4">
							<a class="btn btn-naranja-circular-home text-font px-3" href="<?=base_url('/buscar-proyecto?search=&area=')?>">TODOS LOS<br>PROYECTOS</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Freelancers destacados -->
		<section class="container-fluid py-5 destacados-portada">
			<div class="container sec-freelancers">
				<div class="row mt-70">
					<?php if(count($freelance) >= 5){ ?>
						<?php foreach($freelance as $key => $user){
							if($key == 1){
								echo view_cell('App\Libraries\FuncionesSistema::cardTopPostulante', ['id' => $user['id']]); ?>
								<div class="col-lg-6 col-md-6 col-sm-12 col-12 wow animate__animated animate__fadeInUp" data-wow-delay=".2s">
									<div class="texto-detalles">
										<h3><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'freelancers', 'valor' => 'colnegro'])?></h3>
										<h4><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'freelancers', 'valor' => 'colnaranja'])?></h4>
										<p><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'freelancers', 'valor' => 'subtitulo'])?></p>
									</div>
								</div>
							<?php }elseif($key == 4 && count($freelance) ){ 
								echo view_cell('App\Libraries\FuncionesSistema::cardTopPostulante', ['id' => $user['id']]); ?> 
								<div class="col-12 col-lg-3">

								</div>
							<?php }else{
								echo view_cell('App\Libraries\FuncionesSistema::cardTopPostulante', ['id' => $user['id']]);
							}
						}?>
						<div class="col-12 col-lg-3 text-center">
							<?php if(session('logeado')){ ?>
								<?php if(session('rol') == 'contratante'){ ?>
									<div class="flecha-vermas" data-wow-delay=".2s">
										<a class="text-dark btn btn-blanco-redondo border-0" style="outline: none;" type="button" href="<?=base_url('/usuarios/talento')?>">
											<i class="fas fa-arrow-right" aria-hidden="true"></i> 
										</a>
									</div>
									<p class="vermas-flecha">VER MÁS</p>
								<?php }else{ ?>
									<div class="flecha-vermas" data-wow-delay=".2s">
										<a class="text-dark btn btn-blanco-redondo border-0 cursor" style="outline: none;" type="button" id="ir-talentos">
											<i class="fas fa-arrow-right" aria-hidden="true"></i> 
										</a>
									</div>
									<p class="vermas-flecha">VER MÁS</p>
									<script>
										$('#ir-talentos').click(function(){
											alertify.warning('Lo sentimos, para poder acceder a esta sección debe iniciar sesión como contratante.', 'advertencia');
										});
									</script>
								<?php } ?>
							<?php }else{ ?>
								<div class="flecha-vermas" data-wow-delay=".2s">
									<a class="text-dark btn btn-blanco-redondo border-0" style="outline: none;" type="button" href="<?=base_url('/registrarme/freelancer')?>">
										<i class="fas fa-arrow-right" aria-hidden="true"></i> 
									</a>
								</div>
								<p class="vermas-flecha">VER MÁS</p>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</section>
		<!-- Proyectos recientes -->
		<div class="d-none d-md-block">
			<section class="container-fluid trabajos-recientes-portada">
				<div class="container py-5 px-md-0">
					<div class="row my-4 my-lg-0">
						<div class="col-12">
							<div class="row">
								<div class="col-12 ultimos-trabajos">
									<h2 class="titulo-ultimos"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'trabajos', 'valor' => 'colnegro'])?> <label class="text-naranja"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'trabajos', 'valor' => 'colnaranja'])?></label></h2>
									<p class="mb-4 parrafo-ultimos"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'trabajos', 'valor' => 'subtitulo'])?></p>
								</div>
								<div class="col-lg-9 col-md-8 col-sm-12 list-recent-jobs">
									<?php foreach($proyectos as $pro){ ?>
										<?=view_cell('App\Libraries\FuncionesSistema::cardProyectoDisponible', ['proyecto' => $pro])?>
									<?php } ?>
								</div>
								<div class="col-lg-3 col-md-4 col-sm-12 banner-portada-premium">
									<div class="" style="margin-top: 40px;">
										<ul class="list-group">
											<li class="list-group-item">Buscar por categoría</li>
											<?php foreach ($categorias as $i => $categoria) :?>
												<li class="list-group-item">
													<a class="text-dark" href="<?=base_url('buscar-proyecto?search=&area='.$categoria['area'])?>"><?= $categoria['area']?></a>
												</li>
												<?php if($i == 5) break;?>
											<?php endforeach ;?>
											<li class="list-group-item">
												<a class="text-naranja text-decoration-underline" href="<?=base_url('buscar-proyecto?search=&area=')?>">Ver más</a>
											</li>
										</ul>

									</div>
									<a href="<?=base_url('/tarifas-freelance')?>">
										<img style="margin-top: 20px; border-radius: 30px;" class=" w-100" src="<?=base_url('assets/themes/imgs/banner/banneR99.jpg')?>" alt="Publicidad Mextemps">
									</a>
									<a href="<?=base_url('/proyecto')?>">
										<img style="margin-top: 20px; border-radius: 30px;" class=" w-100" src="<?=base_url('assets/images/GRATIS.webp')?>" alt="Publicidad Mextemps">
									</a>
								</div>
								<div class="col-12 mt-4">
									<div class="d-flex">
										<a class="btn btn-circulo-naranja mas-proy text-font px-3 wh-180" href="<?=base_url('/buscar-proyecto?search=&area=')?>">
											<i class="fas fa-arrow-right" aria-hidden="true"></i>
										</a>
										&nbsp;
										<div style="max-width: 250px; font-size: 24px; font-weight: bold; font-family: 'Poppins', sans-serif;" class="mx-3 fw-bold align-self-center">
											<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'trabajos', 'valor' => 'inferior'])?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<!-- Freelance en latinoamerica -->
		<section class="w-100 fondo-secundario lared-portada">
			<div class="container">
				<div class="row text-center pb-80 pt-80">
					<div class="col-12">
						<p class="h5 separte"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'latinoamerica', 'valor' => 'superior'])?></p>
						<h2><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'latinoamerica', 'valor' => 'colnegro'])?> <label for="" class="text-naranja"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'latinoamerica', 'valor' => 'colnaranja'])?></label></h2>
						<p class="text-secondary losparrafos mt-20 d-none d-md-block">
							<?= str_replace(array('<p>', '</p>'), array('', ''), view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'latinoamerica', 'valor' => 'contenido'])) ?>
						</p>
					</div>
				</div>
			</div>
		</section>
		<!-- Porque usar mextemps -->
		<div class="d-none d-md-block">
			<section class="w-100 mt-50 mb-50 porqueusar deportada">
				<div class="container">
					<div class="titulos">
						<h2 style="max-width: 600px;"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps', 'valor' => 'colnegro'])?> <span class="text-naranja"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps', 'valor' => 'colnaranja'])?></span></h2>
					</div>
					<div class="m-1 row porque-textos">
						<div class="col-12 col-lg-6 align-self-center offset-lg-1">
							<p id="trespasos" class="texto-porque-usar-mextemps" style="text-align: justify; padding-top: 0;">
								<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps', 'valor' => 'contenido'])?>
							</p>
						</div>
						<div class="col-12 col-lg-4">
							<div class="w-100">
								<div class="d-flex">
									<div onmouseover="hover('<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-1', 'valor' => 'texto'])?>')" class="btn-naranja-circular" style="min-width: 100px; max-width: 100px; min-height: 100px; max-height: 100px;">
										<img style="height: 40px; width: 40px;" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-1', 'valor' => 'icono'])?>" alt="">
									</div>
									<div class="pl-10 align-self-center">
										<span class="perfhov"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-1', 'valor' => 'titulo'])?></span>
									</div>
								</div>
							</div>
							<div class="w-100 d-none">
								<div class="d-flex">
									<div onmouseover="hover('<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-2', 'valor' => 'texto'])?>')" class="btn-naranja-circular" style="min-width: 100px; max-width: 100px; min-height: 100px; max-height: 100px;">
										<img style="height: 40px; width: 40px;" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-2', 'valor' => 'icono'])?>" alt="">
									</div>
									<div class="pl-10 align-self-center">
										<span class="perfhov"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-2', 'valor' => 'titulo'])?></span>
									</div>
								</div>
							</div>
							<div class="w-100">
								<div class="d-flex">
									<div onmouseover="hover('<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-3', 'valor' => 'texto'])?>')" class="btn-naranja-circular" style="min-width: 100px; max-width: 100px; min-height: 100px; max-height: 100px;">
										<img style="height: 40px; width: 40px;" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-3', 'valor' => 'icono'])?>" alt="">
									</div>
									<div class="pl-10 align-self-center">
										<span class="perfhov"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-3', 'valor' => 'titulo'])?></span>
									</div>
								</div>
							</div>
							<div class="w-100">
								<div class="d-flex">
									<div onmouseover="hover('<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-4', 'valor' => 'texto'])?>')" class="btn-naranja-circular" style="min-width: 100px; max-width: 100px; min-height: 100px; max-height: 100px;">
										<img style="height: 40px; width: 40px; margin-top: -4px;" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-4', 'valor' => 'icono'])?>" alt="">
									</div>
									<div class="pl-10 align-self-center">
										<span class="perfhov"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-4', 'valor' => 'titulo'])?></span>
									</div>
								</div>
							</div>
							<div class="w-100">
								<div class="d-flex">
									<div onmouseover="hover('<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-5', 'valor' => 'texto'])?>')" class="btn-naranja-circular" style="min-width: 100px; max-width: 100px; min-height: 100px; max-height: 100px;">
										<img style="height: 40px; width: 40px;" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-5', 'valor' => 'icono'])?>" alt="">
									</div>
									<div class="pl-10 align-self-center">
										<span class="perfhov"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-5', 'valor' => 'titulo'])?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- MOVIL -->
					<div class="m-1 row porque-textos-movil">
						<div class="col-12">
							<p id="trespasos" class="texto-porque-usar-mextemps" style="text-align: justify; padding-top: 0;">
								<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps', 'valor' => 'contenido'])?>
							</p>
						</div>
						<!-- <div class="mx-md-auto"> -->
							<div class="col-md-6 col-sm-12">
								<span class="btn-imagen-naranja"><img  src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-2', 'valor' => 'icono'])?>" alt="" /></span>
								<div class="px-2 align-self-center">
									<span class="fw-bold font-lg eltitulo"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-1', 'valor' => 'titulo'])?></span>
									<p style="font-size: 13px;" class="elparrafo">
										<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-1', 'valor' => 'texto'])?>
									</p>
								</div>
							</div>
							<div class="col-md-6 col-sm-12">
								<span class="btn-imagen-naranja"><img  src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-2', 'valor' => 'icono'])?>" alt="" /></span>
								<div class="px-2 align-self-center">
									<span class="fw-bold font-lg eltitulo"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-2', 'valor' => 'titulo'])?></span>
									<p style="font-size: 13px;" class="elparrafo">
										<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-2', 'valor' => 'texto'])?>
									</p>
								</div>
							</div>
							<div class="col-md-6 col-sm-12">
								<span class="btn-imagen-naranja"><img  src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-3', 'valor' => 'icono'])?>" alt="" /></span>
								<div class="px-2 align-self-center">
									<span class="fw-bold font-lg eltitulo"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-3', 'valor' => 'titulo'])?></span>
									<p style="font-size: 13px;" class="elparrafo">
										<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-3', 'valor' => 'texto'])?>
									</p>
								</div>
							</div>
							<div class="col-md-6 col-sm-12">
								<span class="btn-imagen-naranja"><img  src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-4', 'valor' => 'icono'])?>" alt="" /></span>
								<div class="px-2 align-self-center">
									<span class="fw-bold font-lg eltitulo"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-4', 'valor' => 'titulo'])?></span>
									<p style="font-size: 13px;" class="elparrafo">
										<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'mextemps-icono-4', 'valor' => 'texto'])?>
									</p>
								</div>
							</div>
						<!-- </div> -->
					</div>
					<script>
						function hover(description) {
							document.getElementById('trespasos').innerHTML = description;
						}
					</script>
				</div>
			</section>
		</div>
		<?php if(!session('logeado')){ ?>
			<!-- Empezemos el viaje -->
			<section class="section-box mt-50 mb-60 empieza-viaje deportada">
				<div class="container">
					<div class="box-newsletter" id="sec-empecemos" style="padding: 25px 0 25px 0;">
						<div class="d-none d-lg-block">
							<div class="row">
								<div class="col-12 col-lg-7 m-0">
								</div>
								<div class="col-12 col-lg-5 m-0 pt-20">
									<h3 class="title-sec-registro-home">
										<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'empezemos', 'valor' => 'titulo'])?>
									</h3>
									<div class="w-100 sec-boton-reg-home pt-100">
										<div class="row">
											<div class="col-6 align-self-center">
												<h3 class="subtitle-sec-registro-home">
													<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'empezemos', 'valor' => 'texto'])?> 
												</h3>
											</div>
											<div class="col-6 align-self-center">
												<a href="<?=base_url('/registrarme-como')?>" class="text-dark btn btn-blanco-redondo-home" type="button" >
													<i class="fas fa-arrow-right" aria-hidden="true"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="d-block d-lg-none text-center">
							<a href="<?=base_url('/registrarme-como')?>" class="text-dark btn btn-empecemos mt-4 mb-1">
								<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'empezemos', 'valor' => 'texto'])?> <i class="fas fa-arrow-right" aria-hidden="true"></i>
							</a>
						</div>
					</div>
					<div class="box-newsletter-bottom">
						<div class="newsletter-bottom"></div>
					</div>
				</div>
			</section>
		<?php } ?>
	</main>
</div>
<?=$this->endSection()?>