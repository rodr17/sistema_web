<!doctype html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
		<!--Google Autenticacion-->
		<meta name="google-signin-scope" content="profile email">
		<meta name="google-signin-client_id" content="198033966029-a75kfv97som76od1s41e6gr26po9ppvh.apps.googleusercontent.com">
		<!--jQuery-->
		<script src="<?=base_url('assets/js/jquery/jquery_3_2_1.min.js')?>"></script>
		<!--Script para mostrar los iconos-->
		<script defer src="<?=base_url('assets/js/fontawesome/fontawesome.js')?>" crossorigin="anonymous"></script>
		<!-- Fonts -->
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="<?=base_url('assets/css/fonts_google/fonts_google.css')?>" rel="stylesheet">
		<!-- Bootstrap CSS -->
		<link href="<?=base_url('assets/css/bootstrap/bootstrap.min.css')?>" rel="stylesheet">
		<!--Alertify-->
		<link href="<?=base_url('assets/css/alertify/alertify.min.css')?>" rel="stylesheet"/>
		<link rel="stylesheet" href="<?=base_url('assets/css/alertify/bootstrap.min.css')?>"/>
		<script defer src="<?=base_url('assets/js/alertify/alertify.min.js')?>"></script>
		<!--Google RECAPTCHA-->
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
		<!--Hoja de estilos-->
		<link rel="stylesheet" type="text/css" href="<?=base_url('/assets/css/style.css')?>">
		<!--Estilos del tema-->
		<link rel="stylesheet" href="<?=base_url('/assets/themes/css/plugins/animate.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('/assets/themes/css/main.css')?>?v=1.0">
		<title>Ingresar&nbsp;-&nbsp;MEXTEMPS</title>
		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v13.0&appId=<?= get_Facebook('app_id') ?>&autoLogAppEvents=1" nonce="L4gxvrCc"></script>
		<script async defer>
			window.fbAsyncInit = function() {
				FB.init({
					appId      : '<?=get_Facebook('app_id')?>',
					cookie     : true,                     // Enable cookies to allow the server to access the session.
					xfbml      : true,                     // Parse social plugins on this webpage.
					version    : 'v13.0'           // Use this Graph API version for this call.
				});
				FB.AppEvents.logPageView();
			};
			(function(d, s, id){
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.src = "https://connect.facebook.net/en_US/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
	</head>
	<body>
		<div id="preloader-active">
			<div class="preloader d-flex align-items-center justify-content-center">
				<div class="preloader-inner position-relative">
					<div class="text-center">
						<img src="<?=base_url('assets/themes/imgs/theme/loading.gif')?>" alt="jobhub"/>
					</div>
				</div>
			</div>
		</div>
		<main class="main-no login-mt">
			<section class="section-box">
				<div class="container pt-0 login-container">
					<div class="logo" style="text-align: center;">
						<a href="index.php"><img alt="Mextemps" src="<?=base_url('assets/themes/imgs/theme/mextemps.svg')?>" width="245"></a>
					</div>
					<div class="row login-content">
						<div class="col-xl-12 col-md-12 col-sm-12 mx-auto shadow card-grid-4 mt-10 mb-10" style="padding: 0px;">
							<div class="row">
								<div class="contact-from-area padding-20-row-col col-md-12 col-sm-12" style="padding: 20px 30px 20px 30px;">
								<h4 style="text-align: center;" class="login-title">Ingresa</h4>
									<form class="contact-form-style mt-10 needs-validation" id="form-ingresar" enctype="multipart/form-data" novalidate>
										<div class="row wow animate__ animate__fadeInUp animated" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
											<div class="col-lg-12 col-md-12">
												<!-- <div class="input-style mb-10">
													<a title="Facebook" id="continuar_facebook" url="<?=$urlFacebook?>" class="btn btn-social btn-block facebook " style="background-color: #3669b7; font-size: 14px; color: #fff; line-height: 25px; width: 100%;"><img style="vertical-align: middle;" src="https://wkncdn.com/newx/assets/build/img/svg/icon-facebook-inverted.7dbd739a0.svg"> <span>Ingresar con Facebook</span></a>
												</div> -->
											</div> 
											<div class="col-lg-12 col-md-12">
												<div class="input-style mb-10">
													<a title="Google" id="continuar_google" url="<?=$urlGoogle?>" class="btn btn-social btn-block google " style="background-color: #efefef; font-size: 14px; color: #333; line-height: 25px; width: 100%;"><img style="vertical-align: middle;" src="https://wkncdn.com/newx/assets/build/img/svg/icon-google-plus.4e58f2332.svg"> <span>Ingresar con Google</span></a>
												</div>
											</div> 
											<div class="text-separator">O</div>
											<div class="col-lg-12 col-md-12">
												<div class="input-style position-relative">
													<input name="correo" type="email" placeholder="Correo:" required style="height: 40px;">
													<div class="invalid-tooltip">Requerido</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12">
												<div class="input-style position-relative">
													<span class="contenedor-ojo mb-10 my-2">
														<input style="height: 40px;" name="contraseña" type="password" placeholder="Contraseña:" required>
														<a class="btn icon-ojo" style="font-size: 22px;"><i class="fas fa-eye"></i></a>
														<div class="invalid-tooltip">Requerido</div>
													</span>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 text-center">
												<label for="agree_tac" class="small text-end" style="width: 100%;cursor:pointer">
													<a data-bs-toggle="modal" data-bs-target="#recuperar-contraseña">¿Olvidaste tu contraseña?</a></span></label>
												<button class="submit submit-auto-width" type="submit" id="btn-ingresar" style="padding: 10px 40px; border-radius: 30px; margin-top: 10px; width: 100%;">Ingresar</button>
												<p style="margin-top: 20px;" class="noestas-registrado">¿Aún no has creado tu cuenta en Mextemps? <a href="<?=base_url('/registrarme-como')?>">Regístrate</a> </p>
											</div>
										</div>
									</form>
									<p class="form-messege"></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>
		<div class="modal fade" id="recuperar-contraseña" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo-recuperar" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header justify-content-center border-bottom-0 py-4">
						<h5 class="modal-title fw-bold" id="titulo-recuperar">Recupera tú contraseña</h5>
						<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></a>
					</div>
					<div class="modal-body contact-from-area">
						<div class="container contact-form-style">
							<form class="row text-center needs-validation" id="form-recuperar-contra" enctype="multipart/form-data" novalidate>
								<div class="position-relative">
									<input style="height: 50px;" name="correo_resetar" type="email" placeholder="¿Cúal es tu correo electrónico?" required>
									<div class="invalid-tooltip">Ingresa un correo válido</div>
								</div>
								<div class="col-12 my-4">
									<button style="padding: 10px 40px; border-radius: 30px; margin-top: 20px; width: 100%;" class="submit submit-auto-width" type="submit" id="btn-recuperar-contraseña">Recuperar contraseña <i class="fas fa-angle-right" aria-hidden="true"></i></button>
								</div>
							</form>
						</div>
					</div>
					<!-- <p style="text-align: center; margin-bottom: 20px;"> No tienes una cuenta? <a id="lig-reg">Registrate aquí</a></p> -->
				</div>
			</div>
		</div>
		<div class="modal fade" id="contraseña-recuperada" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo-recuperado" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header justify-content-center border-bottom-0 py-4">
						<h5 class="modal-title fw-bold">Recupera tú contraseña</h5>
						<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></a>
					</div>
					<div class="modal-body">
						<div class="container">
							<div class="row text-center">
								<div class="col-12 my-2">
									<p class="text-font text-secundario">Si la cuenta de correo existe en nuestros registros, se ha enviado un correo con los pasos para realizar la recuperación de tu contraseña.</p>
								</div>
								<!-- <div class="mextemps-hr position-relative my-3">
									<img class="logo-hr" src="<?= base_url('assets/images/logos/Logo-hr.webp')?>" alt="Logo MEXTEMPS escala grises">
								</div> -->
							</div>
							<div class="row">
								<div class="col-12 text-center py-2">
									<label class="fw-bold h6">GRACIAS</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="https://accounts.google.com/gsi/client" async defer></script>
		<script>
			(function () {
				'use strict'
				let forms = document.querySelectorAll('.needs-validation');

				Array.prototype.slice.call(forms).forEach(function (form) {
					form.addEventListener('submit', function (event) {
					if (!form.checkValidity()) {
						event.preventDefault();
						event.stopPropagation();
					}
					form.classList.add('was-validated')}, false)
				})
			})()
			$(document).ready(function() {
				$('#continuar_facebook').click(function() {
					$(location).attr('href', $(this).attr('url'));
				});
				$('#continuar_google').click(function() {
					$(location).attr('href', $(this).attr('url'));
				});
				$('.contenedor-ojo .icon-ojo').click(function() {
					if($(this).siblings().attr('type') == 'password') {
						$(this).siblings().attr('type', 'text'); 
						$(this).find('i').replaceWith('<i class="fas fa-eye-slash"></i>');
					}else{ 
						$(this).siblings().attr('type', 'password');
						$(this).find('i').replaceWith('<i class="fas fa-eye"></i>');
					}
				});
				$('#btn-ingresar').click(function(e) {
					e.preventDefault();
					$.ajax({
						url: '<?= base_url('Autenticacion/ingresar')?>',
						type: 'POST',
						data: $('#form-ingresar').serialize(),
						success: function (data) {
							respuesta = JSON.parse(data);
							if(respuesta.alerta == 'correcto') return $(location).attr('href', '<?=base_url('usuarios/perfil')?>');
							if(respuesta.alerta == 'registro') return $(location).attr('href', respuesta.mensaje);
							return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
						},
						error: function(data){
							return alertify.notify(data.responseText, 'falla', 10);
						}
					});
				});
				$('#btn-recuperar-contraseña').click(function(e) {
					e.preventDefault();
					$.ajax({
						url: '<?= base_url('Autenticacion/resetar_contrasena')?>',
						type: 'POST',
						data: $('#form-recuperar-contra').serialize(),
						success: function (data) {
							respuesta = JSON.parse(data);
							if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
							
							$('#contraseña-recuperada').modal('show');
							return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
						},
						error: function(data){
							return alertify.notify(data.responseText, 'falla', 10);
						}
					});
				});
			});
			$('#lig-reg').click(function(){
				$('#recuperar-contraseña').modal('hide');
				$('#registrate').modal('show');
			});
		</script>
		<style>
			#contraseña-recuperada .modal-dialog, #recuperar-contraseña .modal-dialog {
				max-width: 600px;
			}
			#lig-reg{
				color: #FE5000;
			}
		</style>
		<!--Scripts de creados para funciones-->
		<?php if(session('alerta')) :?>
			<script defer>
				$(document).ready(function(){
					alertify.notify('<?= session('alerta.mensaje')?>', '<?= session('alerta.tipo')?>', 10);
				});
			</script>
		<?php endif; ?>
		<script src="<?=base_url('assets/js/google_api/google_api.js')?>" async defer></script>
		<script defer src="<?=base_url('assets/js/bootstrap/popper.min.js')?>"></script>
		<script defer src="<?=base_url('assets/js/bootstrap/bootstrap.min.js')?>"<></script>
		<!-- Scripts del tema -->
		<script src="<?=base_url('assets/themes/js/plugins/waypoints.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/wow.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/magnific-popup.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/perfect-scrollbar.min.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/select2.min.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/isotope.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/scrollup.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/swiper-bundle.min.js')?>"></script>
		<!-- Template  JS -->
		<script src="<?=base_url('assets/themes/js/main.js?v=1.0')?>"></script>
		<script defer src="<?=base_url('/assets/js/front.js')?>"></script>
	</body>
</html>