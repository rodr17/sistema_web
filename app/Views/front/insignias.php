<?=$this->extend('front/main')?>
<?=$this->section('title')?>
    Insignias
<?=$this->endSection()?>
<?=$this->section('estilos')?>

<?=$this->endSection()?>

<?=$this->section('content')?>
<main class="main soporte-page">
    <section class="section-box">
        <div class="container pt-30">
            <div class="w-100 w-md-100 mx-auto text-center">
                <h3 class="section-title-large mb-30 wow animate__animated animate__fadeInUp">Insignias</h3>
                <h4>¿Qué necesito para subir de nivel en mi perfil?</h4>
                <p class="mb-30 text-muted wow animate__animated animate__fadeInUp font-md">Una de las dudas frecuentes es como aumentar de nivel, por lo que te contamos como hacerlo.</p>
            </div>
            <div class="py-5 py-lg-0 pt-50">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <h4 class="pb-50">Puntos claves</h4>
                        <ul>
                            <li class="pb-3">
                                Tu historial de proyectos terminados juega un gran papel para determinar el nivel en el que te encuentras.
                            </li>
                            <li class="pb-3">
                                Pide a tu contratante que te evalué y adicional que te finalice el proyecto satisfactoriamente.
                            </li>
                            <li class="pb-3">
                                Mantener un trabajo de calidad en tiempo y forma te ayudará a crecer, ya que serás recomendado, tendrás una mejor valoración y ranqueo por lo que más contratantes pueden darte más proyectos. Recuerda que entre más proyectos tengas finalizados más rápido podrás subir de nivel.
                            </li>
                            <li class="pb-3">
                                Es importante tener un buen nivel porque esto querrá decir que eres un buen freelancer, que tu trabajo lo realizas con buena calidad y que los contratantes pueden confiar en ti.
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-6">
                        <h4 class="mb-3">Rango de insignias</h4>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col" class="text-center">Insignia</th>
                                    <th scope="col">Rango</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">
                                        <div class="perfil-usuarios-freelancer">
                                            <span class="insignias-lsitado h-100">
                                                <img src="<?=base_url('/assets/images/insignias/obtenidas/insignia-1.svg')?>" alt="Mextemps">
                                            </span>
                                            <span class="fw-bold">Básico</span>
                                        </div>
                                    </td>
                                    <td>De 1 a 15 proyectos</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="perfil-usuarios-freelancer">
                                            <span class="insignias-lsitado h-100">
                                                <img src="<?=base_url('/assets/images/insignias/obtenidas/insignia-2.svg')?>" alt="Mextemps">
                                            </span>
                                            <span class="fw-bold">Intermedio</span>
                                        </div>
                                    </td>
                                    <td>De 16 a 30 proyectos</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="perfil-usuarios-freelancer">
                                            <span class="insignias-lsitado h-100">
                                                <img src="<?=base_url('/assets/images/insignias/obtenidas/insignia-3.svg')?>" alt="Mextemps">
                                            </span>
                                            <span class="fw-bold">Avanzado</span>
                                        </div>
                                    </td>
                                    <td>De 31 a 45 proyectos</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="perfil-usuarios-freelancer">
                                            <span class="insignias-lsitado h-100">
                                                <img src="<?=base_url('/assets/images/insignias/obtenidas/insignia-4.svg')?>" alt="Mextemps">
                                            </span>
                                            <span class="fw-bold">Experto</span>
                                        </div>
                                    </td>
                                    <td>De 46 a 60 proyectos</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="perfil-usuarios-freelancer">
                                            <span class="insignias-lsitado h-100">
                                                <img src="<?=base_url('/assets/images/insignias/obtenidas/insignia-5.svg')?>" alt="Mextemps">
                                            </span>
                                            <span class="fw-bold">Platino</span>
                                        </div>
                                    </td>
                                    <td>De 61 a 99 proyectos</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="perfil-usuarios-freelancer">
                                            <span class="insignias-lsitado h-100">
                                                <img src="<?=base_url('/assets/images/insignias/obtenidas/insignia-6.svg')?>" alt="Mextemps">
                                            </span>
                                            <span class="fw-bold">Diamante</span>
                                        </div>
                                    </td>
                                    <td>Más de 100  proyectos</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?=$this->endSection()?>

<?=$this->section('librerias')?>
<?=$this->endSection()?>