<!doctype html>
<html lang="es" prefix="og:<?=base_url('')?>">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="Content-Type" content="text/html">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico" sizes="16x16 24x24 36x36 48x48">
		<meta name="description" content="Mextemps te ayuda a encontrar a los mejores freelancers para desarrollar tus proyectos, administrando una red de búsqueda de talento, en donde podrás elegir con quién y cuando trabajar. Descubre y localiza las habilidades deseables y sobresalientes en un sin número de personas con experiencias laborales idóneas para realizar esas tareas que necesitas."/>
		<meta name="keywords" content="Freelancers, Mextemps, Contratantes, Proyectos, Mextemps Freelance"/>
		<!-- Website to visit when clicked in fb or WhatsApp-->
		<meta property="og:url" content="<?=base_url('')?>">
		<meta property="og:title"content="Soy parte de la red más grande de Freelancers en Latinoamérica">
		<meta property="og.type" content="website">
		<meta property="og:description" content="Conoce mis habilidades y encuentra el aliado perfecto para tu proyecto."/>
		<meta property="og:image" content="<?=base_url('assets/themes/imgs/theme/mextemps-logo-meta.png')?>" />
		<meta property="og:image:type" content="image/png"/>
		<meta property="og:image:alt" content="Mextemps Freelance Latinoamerica"/>
		<meta property="og:locale" content="es_ES" />
		<meta property="og:site_name" content="MEXTEMPS" />
		<meta name="facebook-domain-verification" content="vjh0rckbokblgno7g6ze8ewl5xe041" />
		<!--Google Autenticacion-->
		<meta name="google-signin-scope" content="profile email">
		<meta name="google-signin-client_id" content="198033966029-a75kfv97som76od1s41e6gr26po9ppvh.apps.googleusercontent.com">
		<!--NETPAY 3DS-->
		<script type="text/javascript" src="<?= base_url('assets/js/NetPay/3DSecure.js')?>"></script>
		<!--jQuery-->
		<script src="<?=base_url('assets/js/jquery/jquery_3_2_1.min.js')?>"></script>
		<!--Script para mostrar los iconos-->
		<script defer src="<?=base_url('assets/js/fontawesome/fontawesome.js')?>" crossorigin="anonymous"></script>
		<!-- Fonts -->
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="<?=base_url('assets/css/fonts_google/fonts_google.css')?>" rel="stylesheet">
		<!-- Bootstrap CSS -->
		<link href="<?=base_url('assets/css/bootstrap/bootstrap.min.css')?>" rel="stylesheet">
		<!--Libreria para telefonos-->
		<script defer src="<?=base_url('assets/js/jquery/mask.min.js')?>"></script>
		<!--Alertify-->
		<link href="<?=base_url('assets/css/alertify/alertify.min.css')?>" rel="stylesheet"/>
		<link rel="stylesheet" href="<?=base_url('assets/css/alertify/bootstrap.min.css')?>"/>
		<script defer src="<?=base_url('assets/js/alertify/alertify.min.js')?>"></script>
		<!--Hoja de estilos-->
		<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/style.css')?>">
		<!--Google RECAPTCHA-->
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
		<!--NETPAY-->
		<script type="text/javascript" src="<?= get_netPay('modo') == 'prueba' ? base_url('assets/js/NetPay/Suscripcion_prueba.js') : base_url('assets/js/NetPay/Suscripcion.min.js')?>"></script>
		<!--Estilos del tema-->
		<link rel="stylesheet" href="<?=base_url('/assets/themes/css/plugins/animate.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('/assets/themes/css/main.css')?>?v=1.0">
		<script src="<?=base_url('assets/js/lordicon/lordicon.min.js')?>"></script>
		<script src="<?=base_url('assets/js/Carrusel.min.js')?>"></script>
		<link rel="stylesheet" href="<?=base_url('assets/css/Carrusel.min.css')?>">
		<!--=$this->include('front/firebase')?>-->
		<?=$this->renderSection('estilos')?>
		<title><?=$this->renderSection('title');?>&nbsp;-&nbsp;MEXTEMPS</title>
	</head>
	<body>
		<div id="preloader-active">
			<div class="preloader d-flex align-items-center justify-content-center">
				<div class="preloader-inner position-relative">
					<div class="text-center">
						<img src="<?=base_url('assets/themes/imgs/theme/loading.gif')?>" alt="jobhub"/>
					</div>
				</div>
			</div>
		</div>
		<?=$this->include('front/header')?>
		<?=$this->renderSection('content')?>
		<!--Scripts de creados para funciones-->
		<?=$this->include('front/footer')?>
		<?php if(session('alerta')) :?>
			<script defer>
				$(document).ready(function(){
					<?php if(session('alerta.tipo') == 'modal'){ ?>
						setTimeout(function(){
							$('#body-alerta').html('<?= session('alerta.mensaje')?>');
							$('#aumentar-membresia').modal('show');
						}, 1500);
					<?php }else{ ?>
						alertify.notify('<?= session('alerta.mensaje')?>', '<?= session('alerta.tipo')?>', 10);
					<?php } ?>
				});
			</script>
		<?php endif; ?>
		<?php if(session('logeado')){ ?>
			<div class="modal fade" id="aumentar-membresia" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered">
					<div class="modal-content">
						<div class="modal-header border-0">
							<h5 class="modal-title" id="staticBackdropLabel">Mejora tu plan</h5>
							<button type="button" class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></button>
						</div>
						<div class="modal-body border-0" id="body-alerta"></div>
						<div class="modal-footer border-0">
							<a type="button" href="<?= (session('rol') == 'freelancer')? base_url('/tarifas-freelance') : base_url('/tarifas-contratista') ?>" class="btn btn-primary">
								Ir a planes
							</a>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		<script src="<?=base_url('assets/js/google_api/google_api.js')?>" async defer></script>
		<!-- Bootstrap Scripts-->
		<script defer src="<?=base_url('assets/js/bootstrap/popper.min.js')?>" async></script>
		<script defer src="<?=base_url('assets/js/bootstrap/bootstrap.min.js')?>" async></script>
		<!-- Scripts del tema -->
		<script defer src="<?=base_url('assets/themes/js/plugins/wow.js')?>" async></script>
		<script defer src="<?=base_url('assets/themes/js/plugins/select2.min.js')?>" async></script>
		<script defer src="<?=base_url('assets/themes/js/plugins/scrollup.js')?>" async></script>

		<script src="<?=base_url('assets/themes/js/plugins/waypoints.js')?>" async></script>
		<script src="<?=base_url('assets/themes/js/plugins/magnific-popup.js')?>" async></script>
		<script src="<?=base_url('assets/themes/js/plugins/perfect-scrollbar.min.js')?>" async></script>
		<script src="<?=base_url('assets/themes/js/plugins/isotope.js')?>" async></script>
		<script src="<?=base_url('assets/themes/js/plugins/swiper-bundle.min.js')?>" async></script>
		<!-- Template JS -->
		<script src="<?=base_url('assets/themes/js/main.js')?>" async></script>
		<script src="<?=base_url('assets/js/front.js')?>" async></script>
		<?=$this->renderSection('librerias')?>
	</body>
</html>