<?=$this->extend('front/main')?>
<?=$this->section('title')?>
    Como funciona
<?=$this->endSection()?>

<?=$this->section('content')?>
<main class="main">
    <section class="section-box bg-banner-about">
        <div class="banner-hero banner-about pt-20">
            <div class="banner-inner">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="block-banner">
                            <h1 class="heading-banner heading-lg">
                                La bolsa de trabajo n.º 1 para freelancers
                            </h1>
                            <div class="banner-description box-mw-70 mt-30">
                                Busque y conecte con los candidatos adecuados más rápidamente. Esta búsqueda de talento le da la oportunidad de encontrar candidatos que pueden encajar perfectamente en su puesto
                            </div>
                            <div class="mt-60">
                                <div class="box-button-shadow mr-10">
                                    <a href="<?=base_url('/contacto')?>" class="btn btn-default">Contactanos</a>
                                </div>
                                <a href="<?=base_url('/soporte')?>" class="btn">Centro de soporte</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 d-none d-lg-block">
                        <div class="banner-imgs">
                            <img alt="jobhub" src="<?=base_url('assets/themes/imgs/page/about/banner-img.png')?>" class="img-responsive main-banner shape-3" />
                            <span class="banner-sm-1"><img alt="jobhub" src="<?=base_url('assets/themes/imgs/page/about/banner-sm-1.png')?>" class="img-responsive shape-1" /></span>
                            <span class="banner-sm-2"><img alt="jobhub" src="<?=base_url('assets/themes/imgs/page/about/banner-sm-2.png')?>" class="img-responsive shape-1" /></span>
                            <span class="banner-sm-3"><img alt="jobhub" src="<?=base_url('assets/themes/imgs/page/about/banner-sm-3.png')?>" class="img-responsive shape-2" /></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-box mt-90 mb-80">
        <div class="container">
            <div class="block-job-bg block-job-bg-homepage-2">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-12 d-none d-md-block">
                        <div class="box-image-findjob findjob-homepage-2 ml-0 wow animate__animated animate__fadeIn">
                            <figure><img alt="jobhub" src="<?=base_url('assets/themes/imgs/page/about/img-findjob.png')?>" /></figure>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="box-info-job pl-90 pt-30 pr-90">
                            <span class="text-blue wow animate__animated animate__fadeInUp">Find jobs</span>
                            <h5 class="heading-36 mb-30 mt-30 wow animate__animated animate__fadeInUp">Create free count and start apply your dream job today</h5>
                            <p class="text-lg wow animate__animated animate__fadeInUp">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is
                                simply dummy.
                            </p>
                            <div class="mt-30 wow animate__animated animate__fadeInUp">
                                <a href="job-grid.html" class="btn btn-default">Explore more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-box mt-90 mt-md-0">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                    <span class="text-lg text-brand wow animate__animated animate__fadeInUp">Online Marketing</span>
                    <h3 class="mt-20 mb-30 wow animate__animated animate__fadeInUp">Committed to top quality and results</h3>
                    <p class="mb-20 wow animate__animated animate__fadeInUp">Proin ullamcorper pretium orci. Donec necscele risque leo. Nam massa dolor imperdiet neccon sequata congue idsem. Maecenas malesuada faucibus finibus. </p>
                    <p class="mb-30 wow animate__animated animate__fadeInUp">Proin ullamcorper pretium orci. Donec necscele risque leo. Nam massa dolor imperdiet neccon sequata congue idsem. Maecenas malesuada faucibus finibus. </p>
                    <div class="mt-10 wow animate__animated animate__fadeInUp">
                        <a href="#" class="btn btn-default">Learn more</a>
                    </div>
                </div>
                <div class="col-lg-7 col-md-12 col-sm-12 col-12 pl-200 d-none d-lg-block">
                    <div class="banner-imgs banner-imgs-about">
                        <img alt="jobhub" src="<?=base_url('assets/themes/imgs/page/about/banner-online-marketing.png')?>" class="img-responsive main-banner shape-3" />
                        <span class="banner-sm-4"><img alt="jobhub" src="<?=base_url('assets/themes/imgs/banner/congratulation.svg')?>" class="img-responsive shape-2" /></span>
                        <span class="banner-sm-5"><img alt="jobhub" src="<?=base_url('assets/themes/imgs/banner/web-dev.svg')?>" class="img-responsive shape-1" /></span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-box mt-80 mt-md-50">
        <div class="container">
            <h2 class="text-center mb-15 section-title wow animate__animated animate__fadeInUp">Our Happy Customer</h2>
            <div class="text-normal text-center color-black-5 box-mw-60 wow animate__animated animate__fadeInUp">
                When it comes to choosing the right web hosting provider, we know how easy it is to get overwhelmed with the number.
            </div>
            <div class="row mt-70">
                <div class="box-swiper">
                    <div class="swiper-container swiper-group-3">
                        <div class="swiper-wrapper pb-70 pt-5">
                            <div class="swiper-slide">
                                <div class="card-grid-3 hover-up">
                                    <div class="text-center card-grid-3-image card-grid-3-image-circle">
                                        <a href="#">
                                            <figure><img alt="jobhub" src="<?=base_url('assets/themes/imgs/page/about/profile.png')?>" /></figure>
                                        </a>
                                    </div>
                                    <div class="card-block-info mt-10">
                                        <p class="text-lg text-center">We are on the hunt for a designer who is exceptional in both making incredible product interfaces as well as</p>
                                        <div class="text-center mt-20 mb-25">
                                            <span><img alt="jobhub" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                            <span><img alt="jobhub" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                            <span><img alt="jobhub" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                            <span><img alt="jobhub" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                            <span><img alt="jobhub" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                        </div>
                                        <div class="card-profile text-center">
                                            <strong>Sarah Harding</strong>
                                            <span>Visual Designer</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card-grid-3 hover-up">
                                    <div class="text-center card-grid-3-image card-grid-3-image-circle">
                                        <a href="#">
                                            <figure><img alt="jobhub" src="<?=base_url('assets/themes/imgs/page/about/profile2.png')?>" /></figure>
                                        </a>
                                    </div>
                                    <div class="card-block-info mt-10">
                                        <p class="text-lg text-center">We are on the hunt for a designer who is exceptional in both making incredible product interfaces as well as</p>
                                        <div class="text-center mt-20 mb-25 card-block-rating">
                                            <span><img alt="jobhub" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                            <span><img alt="jobhub" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                            <span><img alt="jobhub" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                            <span><img alt="jobhub" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                            <span><img alt="jobhub" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                        </div>
                                        <div class="card-profile text-center">
                                            <strong>Sarah Harding</strong>
                                            <span>Visual Designer</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card-grid-3 hover-up">
                                    <div class="text-center card-grid-3-image card-grid-3-image-circle">
                                        <a href="#">
                                            <figure><img alt="jobhub" src="<?=base_url('assets/themes/imgs/page/about/profile3.png')?>" /></figure>
                                        </a>
                                    </div>
                                    <div class="card-block-info mt-10">
                                        <p class="text-lg text-center">We are on the hunt for a designer who is exceptional in both making incredible product interfaces as well as</p>
                                        <div class="text-center mt-20 mb-25">
                                            <span><img alt="jobhub" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                            <span><img alt="jobhub" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                            <span><img alt="jobhub" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                            <span><img alt="jobhub" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                            <span><img alt="jobhub" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                        </div>
                                        <div class="card-profile text-center">
                                            <strong>Sarah Harding</strong>
                                            <span>Visual Designer</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-pagination swiper-pagination3"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?=$this->endSection()?>