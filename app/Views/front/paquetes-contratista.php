<?=$this->extend('front/main')?>
<?=$this->section('title')?>
	Tarifas para Contratantes
<?=$this->endSection()?>

<?=$this->section('content')?>
<?php
	use App\Libraries\NetPay;
	$suscripcion = model('Suscripciones')->builder()->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')->where('id_usuario', session('id'))->get()->getRowArray();
?>
<script defer src="https://kit.fontawesome.com/d47f1b7d62.js" crossorigin="anonymous"></script>
<style type="text/css">
	.tabla-tarifas-border {
		border-radius: 2em;
		border: 1px solid #d6d6d6;
	}
	.tabla-tarifas {
		font-size: 18px;
	}
	.tr-color-blanco{
		background-color: #fff !important;
	}
	
	table.tabla-tarifas tr:nth-child(odd)
	{ background-color: #ff510014;
		;
	}
	
	table.tabla-tarifas tr:nth-child(even)
	{ background-color: #fff;
	}
	
	.tabla-tarifas td {
		padding: 1rem .5rem;
		font-size: 18px;
	}
	.alto-fila {
		padding: 25px 0px !important;
		font-family: 'Poppins', sans-serif;
		font-weight: 400;
		font-size: 18px;
	}
	.tabla-tarifas-danger{
		color: #fe5000;
	}
	.tabla-tarifas-success{
		color:#003B71;
	}
	p.tabla-tarifas-texto-boton-inferiror {
		font-size: 16px;
		margin-top: 6px;
	}
	.btn.tarifas {
		border-radius: 25px;
	}
	.mes-tachado {
		text-decoration: line-through; 
		color: #88929b; 
		font-size: 20px; 
		line-height: 20px;
		display: inline-block;
	}
	.mes-tachado.abajo, .text-month.abajo {
		display: block;
		font-size: 65% !important;
	}
	.mxn-p {
		font-size: 50%;
	}
	.box-pricing-item.most-popular .mes-tachado {
		color: #fff;
	}
	.box-pricing-item.abajo {
		padding: 10px 0px;
		width: auto;
		display: block;
	}
	.box-pricing-item.abajo .box-info-price {
		padding-bottom: 0px;
	}
	.col-uno, .col-dos, .col-tres, .col-cuatro {
		width: 17%;
	}
	.btn-tooltip:hover {
		cursor: pointer;
	}

	.tooltips {
	  position: relative;
	  display: inline-block;
	  border-bottom: 0px dotted black;
	}
	
	.tooltips .tooltiptext { font-size: 12px !important;}
	/*.tooltips .tooltiptext {
	  visibility: hidden;
	  width: 240px;
	  background-color: black;
	  color: #fff;
	  opacity: 0.9;
	  font-size: 12px;
	  line-height: 15px;
	  text-align: center;
	  border-radius: 10px;
	  padding: 8px;
	  position: absolute;
	  z-index: 1;
	  border-top: 2px solid #fe5000;
	  top: -20px;
	  left: 140%;
	}*/

	.tooltips i:hover {
		cursor: pointer;
	}

	.tooltips .tooltiptext::after {
	  content: "";
	  position: absolute;
	  top: 50%;
	  right: 100%;
	  margin-top: -5px;
	  border-width: 5px;
	  border-style: solid;
	  border-color: transparent black transparent transparent;
	}
	.tooltips:hover .tooltiptext {
	  visibility: visible;
	  
	}
	.tabla-desplegada tr{
		height: 45px;
	}
	.tabla-desplegada tr td{
		vertical-align: middle;
	}
</style>
<main class="main  tarifas-contratantes listado-paquetes">
	<section class="section-box">
		<div class="container pt-50">
			<div class="w-100 w-md-100 mx-auto text-center">
				<h3 class="section-title-large mb-30 wow animate__animated animate__fadeInUp">Tarifas para contratantes</h3>
				<h4 class="subtitulo-seccion" style="font-size: 28px; font-weight:700;">Elige el mejor plan de acuerdo a tus necesidades</h4>
				<p class="mb-30 descripcion-seccion-info text-muted wow animate__animated animate__fadeInUp font-md">Ahorra con nuestros planes anuales. Puedes cambiar de plan en cualquier momento. Consulta las Preguntas Frecuentes.</p>
			</div>
		</div>
	</section>
	<!--Tarifas comparativas-->
	<section class="section-box mt-0 mb-0">
		<div class="container">
			<div class="text-center mt-20 switch-pagos">
				<span class="text-lg text-billed">Pago Mensual</span>
				<label class="switch ml-20 mr-20">
					<input type="checkbox" id="cb_billed_type" name="billed_type" onchange="checkBilled()" />
					<span class="slider round"></span>
				</label>
				<span class="text-lg text-billed">Pago Anual</span>
			</div>
			<div class="block-pricing mt-125 mt-md-50" style="background: #ff510014;">
				<?php $planes = model('ModelPlanes')->where('cuenta', 'contratante')->orderBy('id', 'ASC')->find()?>
				<div class="row recuadros-paquetes">
					<?php foreach($planes as $key => $plan){ ?>
						<?= (($suscripcion != null) && ($plan['id_plan_mensual'] == $suscripcion['id_plan_mensual'] || ($plan['id_plan_anual'] == $suscripcion['id_plan_anual'])))? '<div class="col-12"><input id="key-plan-sus" value="'.$key.'" type="hidden"></div>' : '' ?>
					<?php } ?>
					<?php foreach($planes as $key => $plan){ ?>
						<div class="col-lg-4 col-12 wow animate__animated animate__fadeInUp <?= ($plan['name'] == 'Intermedio')? 'order-md-1 order-lg-2' : 'order-2'?>" data-wow-delay=".1s">
							<div class="box-pricing-item <?= ($plan['name'] == 'Intermedio')? 'most-popular' : ''?> <?= (($suscripcion != null) && ($plan['id_plan_mensual'] == $suscripcion['id_plan_mensual'] || ($plan['id_plan_anual'] == $suscripcion['id_plan_anual'])))? 'p-44 m-0' : '' ?>">
								<?php if($suscripcion != null){ ?>
									<?php if($plan['id_plan_mensual'] != $suscripcion['id_plan_mensual'] || ($plan['id_plan_anual'] != $suscripcion['id_plan_anual'])) :?>
										<?php if($plan['name'] == 'Intermedio') :?>
											<div class="text-end mb-10 destacado">
												<a class="btn btn-white-sm">Más popular</a>
											</div>
										<?php endif ;?>
										<?php else :?>
											<div class="text-end mb-10 d-none">
												<a class="btn btn-white-sm">Plan actual</a>
											</div>
									<?php endif ;?>
								<?php }else{ ?>
									<?php if($plan['name'] == 'Intermedio') :?>
										<div class="text-end mb-10 destacado">
											<a class="btn btn-white-sm">Más popular</a>
										</div>
									<?php endif ;?>
								<?php } ?>
								<h4 class="mb-1 mb-lg-3"><?=$plan['name']?></h4>
								<div class="box-info-price pb-2 pb-lg-4">
									<span class="text-price for-month display-month"><?= ($plan['monto'] == '0')? 'Gratis' : '$'.number_format($plan['monto'], 2, '.', ',').' <small>MXN</small>' ?></span>
									<span class="text-price for-year">
										<?php if($plan['monto'] != 0) :?>
											<span class="mes-tachado d-block"><?= '$'.number_format($plan['monto'], 2, '.', ',').' <small>MXN</small>'?></span>
										<?php else :?>
											<small class="diasgratis">365 días</small>
											<span>Gratis</span>
										<?php endif ;?>
										<?= ($plan['monto'] != '0')? '$'.number_format(($plan['monto_anual'] / 12), 2, '.', ',').' <small>MXN</small>' : '' ?>
									</span>
									<span class="text-month for-month display-month" style="margin-top: 1px;"><?= ($plan['monto'] == '0') ? 'Vigencia 1 año' : 'Pago mensual'?></span>
									<span class="text-month for-year" style="margin-top: 1px;"><?= ($plan['monto'] != '0') ? 'Precio mensual' : 'Vigencia 1 año'?></span>
								</div>
								<div>
									<p class="text-desc-package mb-3 min-h-desc-paquetes">
										<span class="min-h-desc-paquetes">
											<?=$plan['descripcion']?>
										</span>
										<?php if($plan['monto'] != 0){ ?>
											<br><br class="for-month">
											<span class="text-facturados for-year">$<?=number_format($plan['monto_anual'], 2, '.', ',')?> MXN facturados anualmente</span>
										<?php } else { ?>
											<br><br class="for-month">
											<span class="text-facturados for-year">365 días de vigencia</span>
										<?php } ?>
									</p>
								</div>
								<?php if(session('logeado')) :?>
									<?php if(session('rol') == 'contratante'){ ?>
										<?php if($suscripcion != null){ ?>
											<?php if($plan['monto'] != 0){ ?>
												<?php if($plan['id_plan_mensual'] != $suscripcion['id_plan_mensual'] || ($plan['id_plan_anual'] != $suscripcion['id_plan_anual'])) :?>
													<div class="botones-tar">
														<a class="btn btn-border for-month display-month" plan="<?=$plan['id_plan_mensual']?>" name="<?=$plan['id_plan_mensual']?>" data-key="<?=$key?>">Seleccionar</a>
														<a class="btn btn-border for-year anual" plan="<?=$plan['id_plan_anual']?>" name="<?=$plan['id_plan_anual']?>" data-key="<?=$key?>">Seleccionar</a>
														<input class='invisible text-font inputs-form plan_seleccionado' hidden>
													</div>
												<?php else :?>
													<div class="d-grid">
														<label class="btn btn-white-sm">Plan Actual</label>
													</div>
												<?php endif ;?>
											<?php }else{ ?>
												<!--<div class="botones-tar">-->
												<!--    <a class="btn btn-border for-month display-month" plan="<?=$plan['id_plan_mensual']?>" name="<?=$plan['id_plan_mensual']?>">Probar</a>-->
												<!--    <a class="btn btn-border for-year anual" plan="<?=$plan['id_plan_anual']?>" name="<?=$plan['id_plan_anual']?>">Probar</a>-->
												<!--    <input class='invisible text-font inputs-form plan_seleccionado' hidden>-->
												<!--</div>-->
											<?php } ?>
										<?php }else{ ?>
											<?php if($plan['monto'] != 0){ ?>
												<div class="botones-tar">
													<a class="btn btn-border for-month display-month" plan="<?=$plan['id_plan_mensual']?>" name="<?=$plan['id_plan_mensual']?>" data-key="<?=$key?>">Seleccionar</a>
													<a class="btn btn-border for-year anual" plan="<?=$plan['id_plan_anual']?>" name="<?=$plan['id_plan_anual']?>" data-key="<?=$key?>">Seleccionar</a>
													<input class='invisible text-font inputs-form plan_seleccionado' hidden>
												</div>
											<?php }else{ ?>
												<div class="botones-tar">
													<label class="btn btn-border for-month display-month" plan="<?=$plan['id_plan_mensual']?>" name="<?=$plan['id_plan_mensual']?>" data-key="<?=$key?>">Actual</label>
													<label class="btn btn-border for-year anual" plan="<?=$plan['id_plan_anual']?>" name="<?=$plan['id_plan_anual']?>">Actual</label>
													<!--<input class='invisible text-font inputs-form plan_seleccionado' hidden>-->
												</div>
											<?php } ?>
										<?php } ?>
									<?php }elseif(session('rol') == 'freelancer'){ ?>
										<a class="btn btn-border for-month display-month" href="<?= base_url('/tarifas-freelance')?>"><?= ($plan['monto'] != 0) ? 'Seleccionar' : 'Probar' ?></a>
										<a class="btn btn-border for-year" href="<?= base_url('/tarifas-freelance')?>"><?= ($plan['monto'] != 0) ? 'Seleccionar' : 'Probar' ?></a>
									<?php } ?>
								<?php else :?>
									<a class="btn btn-border for-month display-month" href="<?= base_url('registrarme/contratante?planid=')?><?= $plan['id_plan_mensual'] ?>"><?= ($plan['monto'] != 0) ? 'Seleccionar' : 'Probar' ?></a>
									<a class="btn btn-border for-year" href="<?= base_url('registrarme/contratante?planid=')?><?= $plan['id_plan_anual'] ?>"><?= ($plan['monto'] != 0) ? 'Seleccionar' : 'Probar' ?></a>
								<?php endif ;?>
								<div class="d-block d-lg-none acordion-plan-perfil">
									<div class="accordion accordion-flush" id="accordionFlush<?=$key?>">
										<div class="accordion-item border-0">
											<h2 class="accordion-header" id="flush-heading-card-<?=$key?>">
												<button class="accordion-button collapsed contenedor-arrow-v d-block text-center py-2 pb-0" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-card-<?=$key?>" aria-expanded="false" aria-controls="flush-collapse-card-<?=$key?>">
													Ver características <i class="arrow-v down-v"></i>
												</button>
											</h2>
											<div id="flush-collapse-card-<?=$key?>" class="accordion-collapse collapse" aria-labelledby="flush-heading-card-<?=$key?>" data-bs-parent="#accordionFlush<?=$key?>">
												<div class="accordion-body p-2 listado-caracteristicas">
													<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'perfilcontratante']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
														<div class="text-center my-3 border-bottom caract-list">
															Crear perfil de Contratante
														</div>
													<?php } ?>
													<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'paneladministracion']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
														<div class="text-center my-3 border-bottom caract-list">
															Panel de administración
														</div>
													<?php } ?>
													<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'valoraciontrabajos']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
														<div class="text-center my-3 border-bottom caract-list">
															Valoracion de trabajos
														</div>
													<?php } ?>
													<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'historialconversaciones']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
														<div class="text-center my-3 border-bottom caract-list">
															Salas de chat
														</div>
													<?php } ?>
													<?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'contactarFreelancer']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
														<div class="text-center my-3 border-bottom caract-list">
															Contactar freelancer
														</div>
													<?php } ?>
													<div class="text-center my-3 border-bottom caract-list">
														Buscador de freelancer
													</div>
													<?php $solicitudes = intval(view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'enviosolicitudes']))?>
													<?php if($solicitudes > 0){ ?>
														<div class="text-center my-3 border-bottom caract-list">
															<?= $solicitudes == 9999 ? 'Envio de solicitudes por proyecto ilimitadas' : $solicitudes.' envio(s) de solicitudes por proyecto' ?>
														</div>
													<?php } ?>
													<?php $proyectos_activos = intval(view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'proyectosActivos']))?>
													<?php if($proyectos_activos > 0){?>
														<div class="text-center my-3 border-bottom caract-list">
															<?= $proyectos_activos == 9999 ? 'Proyectos activos ilimitados' : $proyectos_activos.' proyecto(s) activos'?>
														</div>
													<?php } ?>
													<?php $historial_trabajos = intval(view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'historialtrabajos'])) ?>
													<?php if($historial_trabajos > 0){ ?>
														<div class="text-center my-3 border-bottom caract-list">
															<?= $historial_trabajos == 9999 ? 'Historial de proyectos ilimitado' : 'Historial de '.$historial_trabajos.' proyectos'?>
														</div>
													<?php } ?>
													<?php $destacados = intval(view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'proyectosDestacados'])); ?>
													<?php if($destacados > 0){ ?>
														<div class="text-center my-3 border-bottom caract-list">
															<?= $destacados > 9998 ? 'Proyectos destacados ilimitados' : $destacados.' proyecto(s) destacado(s)'?>
														</div>
													<?php } ?>
													<table class="w-100 tabla-desplegada d-none">
														<tbody>
															<tr class="border-bottom">
																<td scope="row">Crear perfil de Contratante
																	<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																		<span class="tooltiptext">Puedes crear tu perfil con tus datos.</span>
																	</div>
																</td>
																<td class="text-center">
																	<?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'perfilcontratante'])?>
																</td>
															</tr>
															<tr class="border-bottom">
																<td scope="row">Panel de administración
																	<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																		<span class="tooltiptext">Es donde podrás ver los proyectos y el status de ellos. Así como el historial de compras, métodos de pago entre otros.</span>
																	</div>
																</td>
																<td class="text-center">
																	<?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'paneladministracion'])?>
																</td>
															</tr>
															<tr class="border-bottom">
																<td scope="row">Valoracion de trabajos
																	<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																		<span class="tooltiptext">Tendrás la oportunidad valorar los trabajos realizados por el freelancer contratado.</span>
																	</div>
																</td>
																<td class="text-center">
																	<?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'valoraciontrabajos'])?>
																</td>
															</tr>
															<tr class="border-bottom">
																<td scope="row">Salas de chat
																	<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																		<span class="tooltiptext">Acceso donde podrás chatear y consultar todas las conversaciones que hayas tenido previamente en algún proyecto.</span>
																	</div>
																</td>
																<td class="text-center">
																	<?= view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'historialconversaciones'])?>
																</td>
															</tr>
															<tr class="border-bottom">
																<td scope="row">Contactar Freelancer
																	<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																		<span class="tooltiptext">Acceso donde podrás visualizar, obtener y comunicarte con el freelancer de tu preferencia</span>
																	</div>
																</td>
																<td class="text-center">
																	<?= view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'contactarFreelancer'])?>
																</td>
															</tr>
															<tr class="border-bottom">
																<td scope="row">Buscador de freelancer
																	<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																		<span class="tooltiptext">Es donde podrás buscar los talentos que necesites de acuerdo a diferentes categorías.</span>
																	</div>
																</td>
																<td class="text-center <?=($key == 0)? '' : 'd-none' ?>">
																	Restringido
																</td>
																<td class="text-center <?=($key == 1)? '' : 'd-none' ?>">
																	Estándar
																</td>
																<td class="text-center <?=($key == 2)? '' : 'd-none' ?>">
																	Ilimitado
																</td>
															</tr>
															<tr class="border-bottom">
																<td scope="row">Envio de solicitudes por proyecto
																	<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																		<span class="tooltiptext">Te da la oportunidad de contactar al freelancer de tu interés directamente.</span>
																	</div>
																</td>
																
																<td class="text-center">
																	<?= $solicitudes == 9999 ? 'Ilimitado' : $solicitudes?>
																</td>
															</tr>
															<tr class="border-bottom">
																<td scope="row">Proyectos activos
																	<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																		<span class="tooltiptext">Es la cantidad de proyectos que puedes publicar.</span>
																	</div>
																</td>
																
																<td class="text-center">
																	
																</td>
															</tr>
															<tr class="border-bottom">
																<td scope="row">Historial de proyectos
																	<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																		<span class="tooltiptext">Es la cantidad de trabajos finalizados publicados que puedes visualizar.</span>
																	</div>
																</td>
																<?php $historial_trabajos = view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'historialtrabajos'])?>
																<td class="text-center">
																	<?= $historial_trabajos == 9999 ? 'Ilimitado' : $historial_trabajos?>
																</td>
															</tr>
															<tr class="border-bottom">
																<td scope="row">Proyectos Destacados
																	<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
																		<span class="tooltiptext">Cantidad de proyectos publicados que puedes destacar para que sean los primeros que aparezcan en la lista de búsqueda.</span>
																	</div>
																</td>
																<td class="text-center">
																	<?=view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'proyectosDestacados'])?>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
	<!-- tarifas -->
	<section class="mt-40 seccion-uno-tarifas">
		<div class="container">
			<div class="row mb-12">
				<div class="col-12 col-lg-12 wow animate__ animate__fadeInUp mb-3 animated block-pricing bg-transparent" style="visibility: visible; animation-name: fadeInUp;">
					<!-- Tabla desktop inicio -->
					<div class="table-responsive tabla-tarifas-border solo-desktop-precios" style="overflow-y: hidden;">
						<table class="table tabla-tarifas">
							<thead>
								<tr class="tr-color-blanco">
									<td scope="col"></td>
									<?php foreach($planes as $plan){ ?>
										<td scope="col" class="text-center alto-fila">Plan <?= $plan['name']?></td>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td scope="row">Crear perfil de Contratante
										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
											<span class="tooltiptext">Puedes crear tu perfil con tus datos.</span>
										</div>
									</td>
									<?php foreach($planes as $plan){ ?>
										<td class="text-center"><?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'perfilcontratante'])?></td>
									<?php } ?>
								</tr>
								<tr>
									<td scope="row">Panel de administración
										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
											<span class="tooltiptext">Es donde podrás ver los proyectos y el status de ellos. Así como el historial de compras, métodos de pago entre otros.</span>
										</div>
									</td>
									<?php foreach($planes as $plan){ ?>
										<td class="text-center"><?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'paneladministracion'])?></td>
									<?php } ?>
								</tr>
								<tr>
									<td scope="row">Valoracion de trabajos
										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
											<span class="tooltiptext">Tendrás la oportunidad valorar los trabajos realizados por el freelancer contratado.</span>
										</div>
									</td>
									<?php foreach($planes as $plan){ ?>
										<td class="text-center"><?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'valoraciontrabajos'])?></td>
									<?php } ?>
								</tr>
								<tr>
									<td scope="row">Salas de chat
										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
											<span class="tooltiptext">Acceso donde podrás chatear y consultar todas las conversaciones que hayas tenido previamente en algún proyecto.</span>
										</div>
									</td>
									<?php foreach($planes as $plan){ ?>
										<td class="text-center"><?= view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'historialconversaciones'])?></td>
									<?php } ?>
								</tr>
								<tr>
									<td scope="row">Contactar Freelancer
										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
											<span class="tooltiptext">Acceso donde podrás visualizar, obtener y comunicarte con el freelancer de tu preferencia.</span>
										</div>
									</td>
									<?php foreach($planes as $plan){ ?>
										<td class="text-center"><?= view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'contactarFreelancer'])?></td>
									<?php } ?>
								</tr>
								<tr>
									<td scope="row">Buscador de freelancer
										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
											<span class="tooltiptext">Es donde podrás buscar los talentos que necesites de acuerdo a diferentes categorías.</span>
										</div>
									</td>
									<td class="text-center">
										Restringido
									</td>
									<td class="text-center">
										Estándar
									</td>
									<td class="text-center">
										Ilimitado
									</td>
								</tr>
								<tr>
									<td scope="row">Envio de solicitudes por proyecto
										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
											<span class="tooltiptext">Te da la oportunidad de contactar al freelancer de tu interés directamente.</span>
										</div>
									</td>
									<?php foreach($planes as $plan){ ?>
										<?php $solicitudes = view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'enviosolicitudes'])?>
										<td class="text-center"><?= $solicitudes == 9999 ? 'Ilimitado' : $solicitudes?></td>
									<?php } ?>
								</tr>
								<tr>
									<td scope="row">Proyectos activos
										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
											<span class="tooltiptext">Es la cantidad de proyectos que puedes publicar.</span>
										</div>
									</td>
									<?php foreach($planes as $plan){ ?>
										<?php $proyectos_activos = view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'proyectosActivos'])?>
										<td class="text-center"><?= $proyectos_activos == 9999 ? 'Ilimitado' : $proyectos_activos?></td>
									<?php } ?>
								</tr>
								<tr>
									<td scope="row">Historial de proyectos
										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
											<span class="tooltiptext">Es la cantidad de trabajos finalizados publicados que puedes visualizar.</span>
										</div>
									</td>
									<?php foreach($planes as $plan){ ?>
										<?php $historial_trabajos = view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'historialtrabajos'])?>
										<td class="text-center"><?= $historial_trabajos == 9999 ? 'Ilimitado' : $historial_trabajos?></td>
									<?php } ?>
								</tr>
								<tr>
									<td scope="row">Proyectos Destacados
										<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
											<span class="tooltiptext">Cantidad de proyectos publicados que puedes destacar para que sean los primeros que aparezcan en la lista de búsqueda.</span>
										</div>
									</td>
									<?php foreach($planes as $plan){ ?>
										<td class="text-center"><?=view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'proyectosDestacados'])?></td>
									<?php } ?>
								</tr>
								<?php if(session('logeado')) :?>
									<?php if(session('rol') == 'contratante'){ ?>
										<tr class="botones box-pricing-item d-table-row p-0">
											<td scope="row"></td>
											<?php foreach($planes as $plan){ ?>
												<?php if($suscripcion != null){ ?>
													<?php if($plan['id_plan_mensual'] != $suscripcion['id_plan_mensual'] || ($plan['id_plan_anual'] != $suscripcion['id_plan_anual'])) :?>
														<td scope="row" class="text-center" style="font-size: 21px;">
															<?php if($plan['monto'] != 0){ ?>
																<a class="btn btn-primary tarifas for-month display-month" plan="<?=$plan['id_plan_mensual']?>" name="<?=$plan['id_plan_mensual']?>">Suscribirse</a>
																<a class="btn btn-primary tarifas for-year" plan="<?=$plan['id_plan_anual']?>" name="<?=$plan['id_plan_anual']?>">Suscribirse</a>
															<?php } ?>
															<div class="box-pricing-item abajo">
																<div class="box-info-price d-grid">
																	<?php if($plan['monto'] != 0) :?>
																		<span class="tabla-tarifas-texto-boton-inferiror for-month display-month">$<?=$plan['monto']?> <span class="mxn-p">MXN</span></span>
																		<span class="tabla-tarifas-texto-boton-inferiror for-year anual">$<?=$plan['monto_anual']?> <span class="mxn-p">MXN</span></span>
																	<?php else :?>
																		<span class="tabla-tarifas-texto-boton-inferiror">Gratis</span>
																	<?php endif ;?>
																	<span class="tabla-tarifas-texto-boton-inferiror for-year <?=($plan['monto'] == 0)? 'd-none' : ''?>"><span class="mes-tachado abajo">$<?=$plan['monto'] ?> MXN</span>$<?=$plan['monto_anual'] / 12?> <span class="mxn-p">MXN</span></span>
																	<span class="for-month abajo">plan mensual</span><br class="for-month">
																	<span class="for-year abajo">pago mensual</span>
																</div>
															</div>
															<input class='invisible text-font inputs-form plan_seleccionado' hidden>
														</td>
													<?php else :?>
														<td scope="row" class="text-center" style="font-size: 21px;">
															<label class="btn btn-primary tarifas">Actual</label>
														</td>
													<?php endif ;?>
												<?php }else{ ?>
													<?php if($plan['monto'] != 0){ ?>
														<td scope="row" class="text-center" style="font-size: 21px;">
															<a class="btn btn-primary tarifas for-month display-month" plan="<?=$plan['id_plan_mensual']?>" name="<?=$plan['id_plan_mensual']?>">Suscribirse</a>
															<a class="btn btn-primary tarifas for-year" plan="<?=$plan['id_plan_anual']?>" name="<?=$plan['id_plan_anual']?>">Suscribirse</a>
															<div class="box-pricing-item abajo">
																<div class="box-info-price d-grid">
																	<?php if($plan['monto'] != 0) :?>
																		<span class="tabla-tarifas-texto-boton-inferiror for-month display-month">$<?=$plan['monto']?> <span class="mxn-p">MXN</span></span>
																		<span class="tabla-tarifas-texto-boton-inferiror for-year">$<?=$plan['monto_anual']?> <span class="mxn-p">MXN</span></span>
																	<?php else :?>
																		<span class="tabla-tarifas-texto-boton-inferiror">Gratis</span>
																	<?php endif ;?>
																	<span class="tabla-tarifas-texto-boton-inferiror for-year <?=($plan['monto'] == 0)? 'd-none' : ''?>"><span class="mes-tachado abajo">$<?=$plan['monto'] ?> MXN</span>$<?=$plan['monto_anual'] / 12?> <span class="mxn-p">MXN</span></span>
																	<span class="for-month abajo">plan mensual</span><br class="for-month">
																	<span class="for-year abajo">pago mensual</span>
																</div>
															</div>
															<input class='invisible text-font inputs-form plan_seleccionado' hidden>
														</td>
													<?php }else{ ?>
														<td scope="row" class="text-center" style="font-size: 21px;">
															<label class="btn btn-primary tarifas">Actual</label>
														</td>
													<?php } ?>
												<?php } ?>
											<?php } ?>
										</tr>
									<?php }elseif(session('rol') == 'freelancer'){ ?>
										<tr>
											<td scope="row"></td>
											<?php foreach($planes as $plan){ ?>
												<td scope="row" class="text-center" style="font-size: 21px;">
													<a class="btn btn-primary tarifas" href="<?= base_url('/tarifas-freelance')?>"><?=($plan['monto'] != 0)? 'Suscribirse' : 'Probar'?></a>
													<div class="box-pricing-item abajo">
														<div class="box-info-price d-grid">
															<?php if($plan['monto'] != 0) :?>
																<span class="tabla-tarifas-texto-boton-inferiror for-month display-month">$<?=$plan['monto']?> <span class="mxn-p">MXN</span></span>
																<span class="tabla-tarifas-texto-boton-inferiror for-year">$<?=$plan['monto_anual']?> <span class="mxn-p">MXN</span></span>
															<?php else :?>
																<span class="tabla-tarifas-texto-boton-inferiror">Gratis</span>
															<?php endif ;?>
															<span class="tabla-tarifas-texto-boton-inferiror for-year <?=($plan['monto'] == 0)? 'd-none' : ''?>"><span class="mes-tachado abajo">$<?=$plan['monto'] ?> MXN</span>$<?=$plan['monto_anual'] / 12?> <span class="mxn-p">MXN</span></span>
															<span class="for-month abajo"><?=($plan['monto'] == 0)? 'por un año' : 'plan mensual'?></span><br class="for-month">
															<span class="for-year abajo"><?=($plan['monto'] == 0)? 'por un año' : 'precio mensual'?></span>
														</div>
													</div>
												</td>
											<?php } ?>
										</tr>
									<?php } ?>
								<?php else :?>
									<tr class="box-pricing-item d-table-row p-0">
										<td scope="row"></td>
										<?php foreach($planes as $plan){ ?>
											<td scope="row" class="text-center" style="font-size: 21px;">
												<a class="btn btn-primary tarifas for-month display-month" href="<?= base_url('registrarme/contratante?planid=')?><?= $plan['id_plan_mensual'] ?>"><?=($plan['monto'] != 0)? 'Suscribirse' : 'Probar'?></a>
												<a class="btn btn-primary tarifas for-year" href="<?= base_url('registrarme/contratante?planid=')?><?= $plan['id_plan_anual'] ?>"><?=($plan['monto'] != 0)? 'Suscribirse' : 'Probar'?></a>
												<div class="box-pricing-item abajo">
													<div class="box-info-price d-grid">
														<?php if($plan['monto'] != 0) :?>
															<span class="tabla-tarifas-texto-boton-inferiror for-month display-month">$<?=$plan['monto']?> <span class="mxn-p">MXN</span></span>
															<span class="tabla-tarifas-texto-boton-inferiror for-year">$<?=$plan['monto_anual']?> <span class="mxn-p">MXN</span></span>
														<?php else :?>
															<span class="tabla-tarifas-texto-boton-inferiror">Gratis</span>
														<?php endif ;?>
														<span class="tabla-tarifas-texto-boton-inferiror for-year <?=($plan['monto'] == 0)? 'd-none' : ''?>"><span class="mes-tachado abajo">$<?=$plan['monto'] ?> MXN</span>$<?=$plan['monto_anual'] / 12?> <span class="mxn-p">MXN</span></span>
														<span class="for-month abajo"><?=($plan['monto'] == 0)? 'por un año' : 'plan mensual'?></span><br class="for-month">
														<span class="for-year abajo"><?=($plan['monto'] == 0)? 'por un año' : 'precio mensual'?></span>
													</div>
												</div>
											</td>
										<?php } ?>
									</tr>
								<?php endif ;?>
							</tbody>
						</table>
					</div>
					<!-- table desktop fin -->
					<?php foreach($planes as $key => $plan){ ?>
						<!-- Tabla individual movil start -->
						<div class="table-responsive tabla-tarifas-border d-md-none mb-4" style="overflow-y: hidden;">
							<table class="table tabla-tarifas">
								<thead>
									<tr class="tr-color-blanco">
										<td scope="col"></td>
										<td scope="col" class="text-center alto-fila columna-ancho">Plan <?= $plan['name']?></td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td scope="row">Crear perfil de Contratante
											<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
												<span class="tooltiptext">Puedes crear tu perfil con tus datos.</span>
											</div>
										</td>
										<td class="text-center"><?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'perfilcontratante'])?></td>
									</tr>
									<tr>
										<td scope="row">Panel de administración
											<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
												<span class="tooltiptext">Es donde podrás ver los proyectos y el status de ellos. Así como el historial de compras, métodos de pago entre otros.</span>
											</div>
										</td>
										<td class="text-center"><?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'paneladministracion'])?></td>
									</tr>
									<tr>
										<td scope="row">Valoracion de trabajos
											<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
												<span class="tooltiptext">Tendrás la oportunidad valorar los trabajos realizados por el freelancer contratado.</span>
											</div>
										</td>
										<td class="text-center"><?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'valoraciontrabajos'])?></td>
									</tr>
									<tr class="d-none">
										<td scope="row">Acceso a freelancer
											<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
												<span class="tooltiptext">Podrás ver los datos de contacto de los diferentes freelancers dentro de la plataforma.</span>
											</div>
										</td>
										<td class="text-center"><?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'accesofreelancer'])?></td>
									</tr>
									<tr>
										<td scope="row">Buscador de freelancer
											<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
												<span class="tooltiptext">Es donde podrás buscar los talentos que necesites de acuerdo a diferentes categorías.</span>
											</div>
										</td>
										<td class="text-center"><?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'buscadorfreelancer'])?></td>
									</tr>
									<tr>
										<td scope="row">Acceso a Freelancer por nivel
											<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
												<span class="tooltiptext">Podrás ver a los freelancers de acuerdo a la categoría de como estén registrados.</span>
											</div>
										</td>
										<td class="text-center"><?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'accesofreelancernivel'])?> </td>
									</tr>
									<tr>
										<td scope="row">Contactar Freelancer
											<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
												<span class="tooltiptext">Acceso donde podrás visualizar, obtener y comunicarte con el freelancer de tu preferencia</span>
											</div>
										</td>
										<td class="text-center"><?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'contactarFreelancer'])?> </td>
									</tr>
									<tr>
										<td scope="row">Envio de solicitudes por proyecto
											<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
												<span class="tooltiptext">Te da la oportunidad de contactar al freelancer de tu interés directamente.</span>
											</div>
										</td>
										<?php $solicitudes = view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'enviosolicitudes'])?>
										<td class="text-center"><?= $solicitudes == 9999 ? 'Ilimitado' : $solicitudes?></td>
									</tr>
									<tr>
										<td scope="row">Proyectos activos
											<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
												<span class="tooltiptext">Es la cantidad de proyectos que puedes publicar.</span>
											</div>
										</td>
										<?php $proyectos_activos = view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'proyectosActivos'])?>
										<td class="text-center"><?= $proyectos_activos == 9999 ? 'Ilimitado' : $proyectos_activos?></td>
									</tr>
									<tr>
										<td scope="row">Historial de proyectos
											<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
												<span class="tooltiptext">Es la cantidad de trabajos finalizados publicados que puedes visualizar.</span>
											</div>
										</td>
										<?php $historial_trabajos = view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'historialtrabajos'])?>
										<td class="text-center"><?= $historial_trabajos == 9999 ? 'Ilimitado' : $historial_trabajos?></td>
									</tr>
									<tr>
										<td scope="row">Historial de conversaciones
											<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
												<span class="tooltiptext">Es donde puedes ver el historial de conversaciones que hayas tenido con los freelancers.</span>
											</div>
										</td>
										<?php $historial_conversaciones = view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'historialconversaciones'])?>
										<td class="text-center"><?= $historial_conversaciones == 9999 ? 'Ilimitado' : $historial_conversaciones?></td>
									</tr>
									<tr>
										<td scope="row">Proyectos Destacados
											<div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
												<span class="tooltiptext">Cantidad de proyectos publicados que puedes destacar para que sean los primeros que aparezcan en la lista de búsqueda.</span>
											</div>
										</td>
										<td class="text-center"><?=view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'proyectosDestacados'])?></td>
									</tr>
									<?php if(session('logeado')) :?>
										<?php if(session('rol') == 'contratante'){ ?>
											<tr class="botones box-pricing-item d-table-row p-0">
												<td scope="row"></td>
												<?php if($suscripcion != null){ ?>
													<?php if($plan['id_plan_mensual'] != $suscripcion['id_plan_mensual'] || ($plan['id_plan_anual'] != $suscripcion['id_plan_anual'])) :?>
														<td scope="row" class="text-center" style="font-size: 21px;">
															<a class="btn btn-primary tarifas for-month display-month" plan="<?=$plan['id_plan_mensual']?>" name="<?=$plan['id_plan_mensual']?>"><?=($plan['monto'] != 0)? 'Seleccionar' : 'Probar'?></a>
															<a class="btn btn-primary tarifas for-year" plan="<?=$plan['id_plan_mensual']?>" name="<?=$plan['id_plan_mensual']?>"><?=($plan['monto'] != 0)? 'Seleccionar' : 'Probar'?></a>
															<div class="box-pricing-item abajo">
																<div class="box-info-price d-grid">
																	<?php if($plan['monto'] != 0) :?>
																		<span class="tabla-tarifas-texto-boton-inferiror for-month display-month">$<?=$plan['monto']?> <span class="mxn-p">MXN</span></span>
																		<span class="tabla-tarifas-texto-boton-inferiror for-year">$<?=$plan['monto_anual']?> <span class="mxn-p">MXN</span></span>
																	<?php else :?>
																		<span class="tabla-tarifas-texto-boton-inferiror">Gratis</span>
																	<?php endif ;?>
																	<span class="tabla-tarifas-texto-boton-inferiror for-year <?=($plan['monto'] == 0)? 'd-none' : ''?>"><span class="mes-tachado abajo">$<?=$plan['monto'] ?> MXN</span>$<?=$plan['monto_anual'] / 12?> <span class="mxn-p">MXN</span></span>
																	<span class="for-month abajo">plan mensual</span><br class="for-month">
																	<span class="for-year abajo">pago mensual</span>
																</div>
															</div>
															<input class='invisible text-font inputs-form plan_seleccionado' hidden>
														</td>
													<?php else :?>
														<td scope="row" class="text-center" style="font-size: 21px;">
															<label class="btn btn-primary tarifas">Actual</label>
														</td>
													<?php endif ;?>
												<?php }else{ ?>
													<td scope="row" class="text-center" style="font-size: 21px;">
														<a class="btn btn-primary tarifas for-month display-month" plan="<?=$plan['id_plan_mensual']?>" name="<?=$plan['id_plan_mensual']?>"><?=($plan['monto'] != 0)? 'Seleccionar' : 'Probar'?></a>
														<a class="btn btn-primary tarifas for-year" plan="<?=$plan['id_plan_anual']?>" name="<?=$plan['id_plan_anual']?>"><?=($plan['monto'] != 0)? 'Seleccionar' : 'Probar'?></a>
														<div class="box-pricing-item abajo">
															<div class="box-info-price d-grid">
																<?php if($plan['monto'] != 0) :?>
																	<span class="tabla-tarifas-texto-boton-inferiror for-month display-month">$<?=$plan['monto']?> <span class="mxn-p">MXN</span></span>
																	<span class="tabla-tarifas-texto-boton-inferiror for-year">$<?=$plan['monto_anual']?> <span class="mxn-p">MXN</span></span>
																<?php else :?>
																	<span class="tabla-tarifas-texto-boton-inferiror">Gratis</span>
																<?php endif ;?>
																<span class="tabla-tarifas-texto-boton-inferiror for-year <?=($plan['monto'] == 0)? 'd-none' : ''?>"><span class="mes-tachado abajo">$<?=$plan['monto'] ?> MXN</span>$<?=$plan['monto_anual'] / 12?> <span class="mxn-p">MXN</span></span>
																<span class="for-month abajo">plan mensual</span><br class="for-month">
																<span class="for-year abajo">pago mensual</span>
															</div>
														</div>
														<input class='invisible text-font inputs-form plan_seleccionado' hidden>
													</td>
												<?php } ?>
											</tr>
										<?php }elseif(session('rol') == 'freelancer'){ ?>
											<tr>
												<td scope="row"></td>
												<td scope="row" class="text-center" style="font-size: 21px;">
													<a class="btn btn-primary tarifas" href="<?= base_url('/tarifas-freelance')?>"><?=($plan['monto'] != 0)? 'Seleccionar' : 'Probar'?></a>
													<div class="box-pricing-item abajo">
														<div class="box-info-price d-grid">
															<?php if($plan['monto'] != 0) :?>
																<span class="tabla-tarifas-texto-boton-inferiror for-month display-month">$<?=$plan['monto']?> <span class="mxn-p">MXN</span></span>
															<?php else :?>
																<span class="tabla-tarifas-texto-boton-inferiror">Gratis</span>
															<?php endif ;?>
															<span class="tabla-tarifas-texto-boton-inferiror for-year <?=($plan['monto'] == 0)? 'd-none' : ''?>"><span class="mes-tachado abajo">$<?=$plan['monto'] ?> MXN</span>$<?=$plan['monto_anual'] / 12?> <span class="mxn-p">MXN</span></span>
															<span class="for-month abajo"><?=($plan['monto'] == 0)? 'por un año' : 'plan mensual'?></span><br class="for-month">
															<span class="for-year abajo"><?=($plan['monto'] == 0)? 'por un año' : 'precio mensual'?></span>
														</div>
													</div>
												</td>
											</tr>
										<?php } ?>
									<?php else :?>
										<tr class="botones box-pricing-item d-table-row p-0">
											<td scope="row"></td>
											<td scope="row" class="text-center" style="font-size: 21px;">
												<a class="btn btn-primary tarifas for-month display-month" href="<?= base_url('registrarme/contratante?planid=')?><?= $plan['id_plan_mensual']?>"><?= ($plan['monto'] != 0) ? 'Seleccionar' : 'Probar' ?></a>
												<a class="btn btn-primary tarifas for-year" href="<?= base_url('registrarme/contratante?planid=')?><?= $plan['id_plan_anual']?>"><?= ($plan['monto'] != 0) ? 'Seleccionar' : 'Probar' ?></a>
												<div class="box-pricing-item abajo">
													<div class="box-info-price d-grid">
														<?php if($plan['monto'] != 0) :?>
															<span class="tabla-tarifas-texto-boton-inferiror for-month display-month">$<?=$plan['monto']?> <span class="mxn-p">MXN</span></span>
															<span class="tabla-tarifas-texto-boton-inferiror for-year">$<?=$plan['monto_anual']?> <span class="mxn-p">MXN</span></span>
														<?php else :?>
															<span class="tabla-tarifas-texto-boton-inferiror">Gratis</span>
														<?php endif ;?>
														<span class="tabla-tarifas-texto-boton-inferiror for-year <?=($plan['monto'] == 0)? 'd-none' : ''?>"><span class="mes-tachado abajo">$<?=$plan['monto'] ?> MXN</span>$<?=$plan['monto_anual'] / 12?> <span class="mxn-p">MXN</span></span>
														<span class="for-month abajo"><?=($plan['monto'] == 0)? 'por un año' : 'plan mensual'?></span><br class="for-month">
														<span class="for-year abajo"><?=($plan['monto'] == 0)? 'por un año' : 'precio mensual'?></span>
													</div>
												</div>
											</td>
										</tr>
									<?php endif ;?>
								</tbody>
							</table>
						</div>
					<?php } ?>
					<!-- Tabla individual end -->
				</div>
				<div class="my-3 text-center">
					<a class="btn btn-naranja conoce-paquetes" href="<?=base_url('/tarifas-freelance')?>">
						Para conocer planes de freelancers, haz clic aquí.
					</a>
				</div>
			</div>
			<!-- tarifas -->
		</div>
	</section>
	<section class="mt-80 seccion-dos-tarifas">
		<div class="container">
			<div class="row align-items-center mb-50">
				<div class="col-lg-12">
					<!-- <span class="text-blue wow animate__animated animate__fadeInUp">Questions</span> -->
					<h4 class="mt-20 subs wow animate__animated animate__fadeInUp" style="text-align: center;">Preguntas frecuentes</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="accordion accordion-flush" id="accordionFlushExample">
						<div class="accordion-item">
							<h2 class="accordion-header" id="flush-headingOne">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
									¿Cómo funciona los planes de suscripción mensual y anual?
								</button>
							</h2>
							<div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
								<div class="accordion-body">
									<p class="mb-15">Nuestros planes de suscripción brindan la mejor oferta para ti. Si te inscribes en una suscripción mensual esta se restablecerá cada mes al final de cada ciclo de facturación. Puedes cancelar nuestra suscripción mensual en cualquier momento. Para hacerlo, desactiva la función de renovación automática. <br>
									Con la suscripción anual requiere un compromiso de 12 meses y te permite ahorrar hasta un 50% en comparación con la opción mensual.</p>
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="flush-headingTwo">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
									¿Cómo funciona la renovación automática?
								</button>
							</h2>
							<div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
								<div class="accordion-body">Tu suscripción se renovará automáticamente si no la cancelas. Para cancelar la renovación automática de tu suscripción y prevenir posibles futuros pagos no deseados, debes cancelar tu suscripción en tu perfil de usuario antes del corte.</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="flush-headingThree">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
									¿Qué diferencia hay entre una cuenta de Freelancer y Contratante?
								</button>
							</h2>
							<div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
								<div class="accordion-body">La cuenta de contratante es aquella que puede subir proyectos, buscar el freelancer que se adapte a las necesidades del trabajo requerido y contratar. <br>La cuenta de freelancer postularse para ser contratado en cualquiera de los proyectos de un contratante</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="flush-headingFour">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
									¿Puedo contratar un plan superior o inferior?
								</button>
							</h2>
							<div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
								<div class="accordion-body">Puedes cambiarte en cualquier momento; para ello, entra a la sección de facturación de tu cuenta. Selecciona el plan al que deseas cambiarte y podrás empezar a usar las nuevas funciones de inmediato.</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="accordion accordion-flush" id="accordionFlushExample2">
						<div class="accordion-item">
							<h2 class="accordion-header" id="flush-headingOne2">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne2" aria-expanded="false" aria-controls="flush-collapseOne2">
									¿Puedo cancelar mi plan si ya no lo necesito?
								</button>
							</h2>
							<div id="flush-collapseOne2" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample2">
								<div class="accordion-body">
									<p class="mb-15">En el caso de las suscripciones mensuales, puedes cancelar en cualquier momento. Para hacerlo, solo debes desactivar la renovación automática en la configuración de tu plan. Dado que ya pagaste por la suscripción mensual al principio del ciclo de facturación, aún podrás seguir utilizando hasta el final de tu ciclo de facturación. Si necesitas rescindir tu suscripción anual (facturada mensualmente) antes de que finalice el plazo de 12 meses, se te aplicará una tarifa equivalente al ahorro que hayas obtenido en comparación con la tarifa mensual . Comunícate con nosotros para recibir asistencia con tu cancelación o si tienes alguna pregunta.</p>
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="flush-headingTwo2">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo2" aria-expanded="false" aria-controls="flush-collapseTwo2">
									¿Puedo obtener un reembolso si cancelo mi plan?
								</button>
							</h2>
							<div id="flush-collapseTwo2" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo2" data-bs-parent="#accordionFlushExample2">
								<div class="accordion-body">Con mucho gusto te reembolsaremos las compras realizadas dentro de los primeros 14 días. Una vez transcurridos los 14 días o cuando se haya utilizado un plan, ya no podrás recibir un reembolso.</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="flush-headingThree2">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree2" aria-expanded="false" aria-controls="flush-collapseThree2">
									¿Qué métodos de pago tienen? 
								</button>
							</h2>
							<div id="flush-collapseThree2" class="accordion-collapse collapse" aria-labelledby="flush-headingThree2" data-bs-parent="#accordionFlushExample2">
								<div class="accordion-body">Aceptamos todas las tarjetas Visa y Master Card, Open Pay, Pago en Oxxo y Pagos en tiendas de conveniencia.</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="flush-headingFour2">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour2" aria-expanded="false" aria-controls="flush-collapseFour2">
									¿Cuándo es mi fecha de corte?
								</button>
							</h2>
							<div id="flush-collapseFour2" class="accordion-collapse collapse" aria-labelledby="flush-headingFour2" data-bs-parent="#accordionFlushExample2">
								<div class="accordion-body">Para validar tu fecha de corte ingresa a tu cuenta en tu historial de pagos.</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section-box bg-blue-medio mt-90 mb-50 eresagencia">
		<div class="container">
			<h3 class="mb-20 titulos" style="text-align: center;">¿Eres una agencia? <br>Regístrate ahora mismo.</h3>
			<div class="row">
				<div class="col-lg-12">
					<p class="text-gray-200 descripcion" style="text-align: center;">Encuentra el mejor talento para tus proyectos.</p>
				</div>
				<div class="col-lg-12 col-lg-12 pt-40 text-center">
					<div class="" style="display: inline-block;">
						<a href="<?=base_url('/registrarme-como')?>" class="btn btn-default">Registrarme</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section-box mt-50 mb-60 suscribete-ahora">
		<div class="container">
			<div class="row justify-content-center">
                <div class="col-8">
                    <div class="box-newsletter d-none d-md-block">
                        <h5 class="text-lg-newsletter text-center">Suscríbete ahora</h5>
                        <h6 class="text-md-newsletter text-center">Recibe aviso de los últimos proyectos publicados</h6>
                        <div class="box-form-newsletter mt-30">
                            <form class="form-newsletter needs-validation position-relative" id="boletines_suscripcion_1" novalidation encytpe="multipart/form-data">
                                <input type="email" class="input-newsletter" name="correo" id="correo" placeholder="usuario@gmail.com" required>
                                <div class="invalid-tooltip text-font">Requerido</div>
                                <button class="btn btn-default font-heading icon-send-letter" type="submit">Suscribirme</button>
                            </form>
                        </div>
                    </div>
                    <div class="box-newsletter d-block d-md-none">
                        <h5 class="text-white fw-bold">Suscríbete ahora</h5>
                        <h6 class="text-white fw-normal text-md mt-2">Recibe aviso de los últimos proyectos publicados</h6>
                        <div class="box-form-newsletter mt-30">
                           <form class="form-newsletter needs-validation position-relative" id="boletines_suscripcion_2" novalidation encytpe="multipart/form-data">
                                <input type="email" class="input-newsletter" name="correo" id="correo" placeholder="usuario@gmail.com" required>
                                <div class="invalid-tooltip text-font">Requerido</div>
                                <button class="btn btn-default font-heading icon-send-letter" type="submit" style="background-position: right 20px center;"></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</section>
	<?php if(session('logeado')){ ?>
	<?php $tarjetas = model('Autenticacion')->get_tarjetas_usuario_ID(session('id')); ?>
	<div class="modal fade" id="cambiar_tarjeta" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0">
					<h5 class="modal-title fw-bold" id="titulo">Seleccione tarjeta</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#obtener_membresia"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row align-items-center text-center" id="listado-tarjetas">
                        <?php if(empty($tarjetas)) :?>
							<?= vacio('No tienes tarjetas disponibles')?>
						<?php else :?>
							<?php foreach ($tarjetas as $key => $tarjeta) : ?>
								<div class="col-lg-12 my-3" tarjeta="<?= $tarjeta['card']['token'] ?>">
									<label class="card p-1 contenedor_checks" for="<?= $tarjeta['card']['token'] ?>">
										<input class="invisible check-tarjetas" type="radio" name="tarjetas_usuarios_radio" hidden id="<?= $tarjeta['card']['token'] ?>">
										<div class="d-flex align-items-center">
											<div class="flex-shrink-0">
												<img class="ms-3" src="<?= model('autenticacion')->urlImgBanco($tarjeta['card']['brand']) ?>" alt="Procesador de pago bancario">
											</div>
											<div class="flex-grow-1 ms-3">
												<p class="d-flex text-font fw-bold">Terminación <?= $tarjeta['card']['lastFourDigits'] ?></p>
												<!-- <p class="d-flex text-font2"><= $tarjeta->bank_name ?></p> -->
												<p class="d-flex text-font2">Vencimiento: <?= $tarjeta['card']['expMonth'] . '/' . $tarjeta['card']['expYear'] ?></p>
											</div>
										</div>
									</label>
								</div>
							<?php endforeach; ?>
                        <?php endif ;?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="obtener_membresia" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header pb-0 border-bottom-0">
					<h5 class="modal-title fw-bold" id="titulo">Formas de pago</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row align-items-center text-center" id="contenido-tarjetas">
							<div id="selector">
                                <?php if(empty($tarjetas)) :?>
                                    <?= vacio('No tienes tarjetas disponibles')?>
                                <?php else :?>
                                    <?php foreach ($tarjetas as $key => $tarjeta) : ?>
                                        <?php if ($key == count($tarjetas) - 1) : ?>
                                            <div class="col-lg-12 mt-3 mb-1" id="contenedor-tarjetaObtenerSubs-<?= $tarjeta['card']['token'] ?>">
                                                <div class="card p-1">
                                                    <div class="d-flex align-items-center">
                                                        <div class="flex-shrink-0">
                                                            <img class="ms-3" id="imagen_modal" src="<?= model('autenticacion')->urlImgBanco($tarjeta['card']['brand']) ?>" alt="Procesador de pago bancario">
                                                        </div>
                                                        <div class="flex-grow-1 ms-3">
                                                            <label class="d-flex text-font fw-bold" id="terminacion_modal">Terminación <?= $tarjeta['card']['lastFourDigits'] ?></label>
                                                            <!-- <label class="d-flex text-font2" id="banco_modal"><= $tarjeta->bank_name ?></label> -->
                                                            <label class="d-flex text-font2" id="vencimiento_modal">Vencimiento: <?= $tarjeta['card']['expMonth'] . '/' . $tarjeta['card']['expYear'] ?></label>
                                                        </div>
                                                        <div class="flex-grow-1 ms-3">
                                                            <a class="text-font justify-content-center user-select-none text-decoration-none text-naranja opciones-ingresar" id="tarj_modal" datos="<?= $tarjeta['card']['token'] ?>">Proceder al pago <i class="fas fa-chevron-right"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="d-flex col-12 justify-content-end">
                                                <a class="text-font2 opciones-ingresar" data-bs-toggle="modal" data-bs-target="#cambiar_tarjeta">Cambiar tarjeta</a>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
							</div>
							<div class="modal-header border-bottom-0 py-0">
								<h5 class="modal-title fw-bold" id="titulo">Opciones de pago</h5>
							</div>
							<div class="col-lg-12 my-2">
								<label class="card p-2 add-tarjeta opciones-ingresar" id="flujo_PagoTarjeta">
									<div class="d-flex align-item-center">
										<div class="flex-shrink-0 align-self-center">
											<img src="<?=base_url('/assets/images/iconos/visa.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
											<img src="<?=base_url('/assets/images/iconos/amex.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
											<img src="<?=base_url('/assets/images/iconos/mastercard.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
										</div>
										<div class="flex-grow-1 ms-3">
											<p class="d-flex text-font fw-bold m-0">Agregar tarjeta</p>
										</div>
									</div>
								</label>
							</div>
							<div class="col-lg-12 my-2" id="columna_tienda">
								<label class="card p-2 contenedor_checks" for="metodo_tienda">
									<input class="check-otrosPagos" type="radio" name="otros_pagos" hidden id="metodo_tienda" value="efectivo">
									<div class="d-flex align-items-center">
										<div class="flex-shrink-0">
											<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 20 20" style="color: #3483FA;">
												<path fill="currentColor" fill-rule="nonzero" d="M19.002 4.004v11.981h-18V4.004h18zm-1.2 1.2h-15.6v9.581h15.6V5.204zm-7.8 2.354a2.436 2.436 0 110 4.873 2.436 2.436 0 110-4.873zm0 1.2a1.236 1.236 0 10-.001 2.473 1.236 1.236 0 00.001-2.473zm4.806.03a1.206 1.206 0 11-.002 2.41 1.206 1.206 0 01.002-2.409zm-9.614 0a1.206 1.206 0 110 2.41 1.206 1.206 0 010-2.409z"></path>
											</svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="color: #3483FA;">
												<path fill="currentColor" fill-rule="evenodd" d="M11.999.632l9.751 5.67v3.447l-2.244.001v7.52h1.24l2.24 5.265H1.164l2.071-5.265h1.267V9.766l-2.253.004V6.302l9.749-5.67zm7.755 18.138H4.258l-.891 2.265h17.35l-.963-2.265zm-1.748-1.5V9.751l-5.257.006v7.511h5.257zm-6.757-7.51l-5.246.006v7.503h5.246V9.76zm.75-7.392L3.75 7.165v1.104l16.5-.018V7.165l-8.251-4.797z"></path>
											</svg>
										</div>
										<div class="flex-grow-1 ms-3">
											<p class="d-flex text-font fw-bold m-0">Efectivo</p>
										</div>
									</div>
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="agregar-pago" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" style="max-width: 620px;">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0">
					<h5 class="modal-title fw-bold" id="titulo">Agregar método de pago</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body">
					<div class="container">
						<form class="row justify-content-center needs-validation" id="netpay-form" enctype="multipart/form-data" novalidate>
							<input type="hidden" name="token_id" id="token_id">
							<input type="hidden" name="deviceFingerPrint" id="deviceFingerPrint">
							<input type="hidden" name="referencia_cargo" id="referencia_cargo">
							<input type="hidden" name="token_transaccion" id="token_transaccion">
							<div class="text-center">
								<p class="fw-bold h6 text-start mt-3" style="display: inline-block; padding-right: 20px;">Tarjeta Débito/Crédito</p>
								<!-- <img class="binking__form-brand-logo"> -->
								<div class="flex-shrink-0" style="display: inline-block; vertical-align: bottom;">
									<img src="<?= base_url('assets/images/iconos/visa.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
									<img src="<?= base_url('assets/images/iconos/amex.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
									<img src="<?= base_url('assets/images/iconos/mastercard.ico')?>" style="width: 32px; height: 20px;" alt="Procesador de pago bancario">
								</div>
							</div>
							<div class="row justify-content-around mt-3">
								<div class="col-12">
									<div class="form-floating mb-3 ">
										<input class="form-control text-font" name="nombre" id="nombrePay" required>
										<label class="text-font" for="nombrePay">Nombre Titular</label>
									</div>
								</div>
								<div class="col-12">
									<div class="form-floating mb-3 ">
										<input class="form-control text-font binking__number-field numerico" autocomplete="cc-number" inputmode="numeric" pattern="[0-9 ]*" name="tarjeta" id="tarjetaPay" maxlength="16" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
										<label class="text-font" for="tarjetaPay">Número Tarjeta</label>
									</div>
								</div>
								<div class="col-12 ">
									<dv class="row">
										<div class="col-12 col-md-4">
											<div class="form-floating ">
												<input class="form-control text-font binking__month-field numerico" name="mes" id="mesPay" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
												<label class="text-font" for="mesPay">Mes</label>
											</div>
										</div>
										<div class="col-12 col-md-4">
											<div class="form-floating ">
												<input class="form-control text-font binking__year-field numerico" name="año" id="añoPay" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
												<label class="text-font" for="añoPay">Año</label>
											</div>
										</div>
										<div class="col-12 col-md-4">
											<div class="form-floating ">
												<input class="form-control text-font binking__code-field numerico" type="password" name="cvv" id="cvvPay" maxlength="4" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
												<label class="text-font" for="cvvPay">CVV</label>
												<img src="<?=base_url('/assets/images/cvv.png')?>" style="position: relative; top: -44px; right: -100px;">
											</div>
										</div>
									</dv>
								</div>
							</div>
							<div class="col-12 my-3">
								<img src="" alt="">
								<button class="btn btn-naranja btn-border text-font" id="btn-agregarTarjeta" type="submit">Agregar </button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="tarjetaResumen_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0">
					<h5 class="modal-title fw-bold">Tarjeta</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#obtener_membresia"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row align-items-center text-center">
							<h6 class="text-center fw-normal fst-italic">Resumen de compra</h6>
							<div class="col-12 info-detalles-compra mt-3" id="suscripcion_seleccionada_tarjeta"></div>
							<div class="seccion-cupon my-2">
								<?= view('front/viewcells/boton-cupon') ?>
							</div>
							<div class="d-flex justify-content-end gap-2">
								<div class="contenedor_cvv">
									<input class="border border-secondary text-font numerico" id="cvv2" name="cvv2" type="password" placeholder="cvv" maxlength="4" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
									<img src="<?=base_url('assets/images/cvv.png')?>" alt="codigo de seguridad tarjeta">
								</div>
							</div>
							<div class="col-12 text-end my-3">
								<a class="btn btn-naranja" id="btn_confirm_tarjeta">Confirmar compra</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade pagos" id="efectivo_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo_efectivo" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0 pb-0">
					<h5 class="modal-title fw-bold" id="titulo_efectivo">Efectivo</h5>
					<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#obtener_membresia"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row align-items-center text-center">
							<h6 class="text-center fw-normal fst-italic">Resumen de compra</h6>
							<div class="col-12 info-detalles-compra mt-3" id="suscripcion_seleccionada_efectivo"></div>
							<div class="seccion-cupon">
								<?= view('front/viewcells/boton-cupon') ?>
							</div>
							<div class="col-12 text-end mt-3">
								<a class="btn btn-naranja" id="btn_confirm_efectivo">Confirmar compra</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="respuesta_pago" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header justify-content-center border-bottom-0">
					<h5 class="modal-title fw-bold" id="titulo">¡Un paso más!</h5>
					<a class="btn cerrar-modal px-3 py-2" script="location.reload()"><i class="fas fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row align-items-center text-center">
							<div class="col-12">
								<label class="text-font mb-2">Te hemos enviado un correo con los pasos a seguir para poder proceder con la suscripcion a tu cuenta. Ó</label>
								<label class="text-font">descarga <a class="text-decoration-none text-naranja" id="referencia">aquí</a> tu comprobante de pago.</label>
							</div>
							<div class="col-12">
								<a class="btn btn-naranja" href="<?=base_url('/usuarios/perfil')?>">Ir a mi perfil</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		let _this = this;
		const respuesta_3ds = function(_this, referenceId) {
			console.log('referenceId: ' + referenceId);
			charges(referenceId);
		}

		const charges = function(referenceId) {
			console.log('charges: ' + referenceId);
			$('#referencia_cargo').val(referenceId);
		}
		
		const confirm = function(processorTransactionId) {
			console.log('confirm: ' + processorTransactionId);
			$.ajax({
				url: '<?= base_url('Pagos/confirmar_pago') ?>',
				type: 'POST',
				data: {transaccion: $('#token_transaccion').val(), procesador: processorTransactionId, usuario: <?=session('id')?>},
				success: function(data) {
					let respuesta = JSON.parse(data);
					$('#tarjetaResumen_modal').modal('hide');
					$('#btn_confirm_tarjeta').removeClass('disabled').children('span').remove();
					$('input[name="otros_pagos"]').prop('checked', false).parent().removeClass('active');
					$('#obtener_membresia .cerrar-modal').click();
					$('.modal').modal('hide');

					if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta);

					alertify.notify(respuesta.mensaje, respuesta.alerta);
					return setTimeout((e) => location.href = "/compra_realizada?token="+respuesta.datos.transactionTokenId, 2000);
				},
				error: function(data) {
					let respuesta = JSON.parse(data.responseText);
					$('#btn_confirm_tarjeta').removeAttr('disabled').children('span').remove();
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
				}
			});
		}

		const callbackProceed = function(_this, processorTransactionId) {
			alertify.notify('processorTransactionId: ' + processorTransactionId, 'custom');
			confirm(processorTransactionId);
		}

		//La generación del device fingerprint debe de estar al momento en que se visualiza el formulario de pago
		function generateDevice(callback) {
			deviceFingerPrint = NetPay.form.generateDeviceFingerPrint();
			callback();
		}
		
		function añadirTarjeta(id, tipo){
			$.ajax({
				type: 'POST',
				url: '<?=base_url('/Pagos/get_tarjeta_user')?>',
				data:{id:id, tipo:tipo},
				success: function(data){
					let respuesta = JSON.parse(data);
					if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta);

					$('#selector').html(respuesta.selector);
					$('#listado-tarjetas').empty().append(respuesta.listado);
					
					if($('#card-mis-tarjetas').children('.empty-img').length > 0){ //SI NO HAY TARJETAS
						$('#card-mis-tarjetas').children('.empty-img').remove();
						$('#card-mis-tarjetas div').remove();
					} 
					return $('#card-mis-tarjetas').append(respuesta.mis_tarjetas);
				}, error: function(data){
					alertify.error('Ha surgido un error', 10);
				}
			});
		}

		$(document).ready(function () {
			let deviceFingerPrint;
			<?php $netpay = new NetPay()?>
			<?= $netpay->getModo() ? 'NetPay.setSandboxMode(true); netpay3ds.setSandboxMode(true);' : 'NetPay.setSandboxMode(false); netpay3ds.setSandboxMode(false);' ?>

			// SE OBTIENE REFERENCEID POR 3DS
			netpay3ds.init(() => netpay3ds.config(_this, 100, respuesta_3ds));
		});
		$('.check-otrosPagos').change(function(e) {
			$this = $(this);
			$($this).parent().addClass('active');
			$('.check-otrosPagos').not(':checked').parent().removeClass('active');

			$.ajax({
				url: '<?= base_url('Suscripciones/getPlan')?>',
				type: 'POST',
				data: {id : $('.plan_seleccionado').val(), es_tarjeta: false},
				success: function (data) {
					let respuesta = JSON.parse(data);
					if(respuesta.alerta == 'falla') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					$('#suscripcion_seleccionada_'+$($this).val()).html(respuesta.mensaje);
					
					$('#obtener_membresia').modal('hide');
					return $('#' + $($this).val() + '_modal').modal('show');
				},
				error: function(data){
					alertify.notify('Network', 'falla');
				}
			});
			$('#obtener_membresia').modal('hide');
			$('#' + $(this).val() + '_modal').modal('show');
		});
		
		$('#btn-agregarTarjeta').click(function(evento) {
			if (evento.currentTarget.form.nombrePay.value.length == 0 || evento.currentTarget.form.tarjetaPay.value.length == 0 ||
				evento.currentTarget.form.mesPay.value.length == 0 || evento.currentTarget.form.cvvPay.value.length == 0) return true;

			evento.preventDefault();
			$(this).attr('disabled', true);
			$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);

			generateDevice(function() { 
				let tarjeta = {
					cardNumber: evento.currentTarget.form.tarjetaPay.value,
					expMonth: evento.currentTarget.form.mesPay.value,
					expYear: evento.currentTarget.form.añoPay.value,
					cvv2: evento.currentTarget.form.cvvPay.value,
					vault: true,
					simpleUse:false,
					deviceFingerPrint : deviceFingerPrint
				};
				NetPay.setApiKey('<?= $netpay->getLlave() ?>');
				var validateNumberLength = NetPay.card.validateNumberLength(tarjeta.cardNumber);
				var validateNumber = NetPay.card.validateNumber(tarjeta.cardNumber);
				var validateExpiry = NetPay.card.validateExpiry(tarjeta.expMonth, tarjeta.expYear);
				var validateCVV = NetPay.card.validateCVV(tarjeta.cvv2, tarjeta.cardNumber);

				if (!validateNumberLength || !validateNumber){
					$('#btn-agregarTarjeta').removeAttr('disabled').children('span').remove();
					return alertify.notify('Número de tarjeta incorrecto', 'advertencia', 10);
				} 
				if (!validateExpiry){
					('#btn-agregarTarjeta').removeAttr('disabled').children('span').remove();
					return alertify.notify('Expiración de tarjeta incorrecto', 'advertencia', 10);
				}
				if (!validateCVV){
					$('#btn-agregarTarjeta').removeAttr('disabled').children('span').remove();
					return alertify.notify('Código de seguridad incorrecto', 'advertencia', 10);
				}

				NetPay.token.create(tarjeta, correcto, falla);

				function correcto(respuesta) {
					console.log("Token creado correctamente");
					let datos_token = JSON.parse(respuesta.message.data);
					console.log(datos_token);
					$('#token_id').val(datos_token.token);
					$('#deviceFingerPrint').val(tarjeta.deviceFingerPrint);
					$.ajax({
						url: '<?= base_url('Pagos/guardarTarjetas') ?>',
						type: 'POST',
						data: $('#netpay-form').serialize(),
						success: function(data) {
							let respuesta = JSON.parse(data);
							$('#btn-agregarTarjeta').removeAttr('disabled').children('span').remove();

							if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
							$('#agregar-pago').modal('hide');
							$('#netpay-form')[0].reset();
							if($('#flujo_PagoTarjeta').is('[agregar]')){
								añadirTarjeta(respuesta.tarjeta);
								$('#obtener_membresia').modal('show');
								$('#flujo_PagoTarjeta').removeAttr('agregar');
							}else setTimeout(function(){ location.reload() }, 2000);
							
							return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
						},
						error: function(data) {
							let respuesta = JSON.parse(data.responseText);
							alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
						}
					});
				}

				function falla(error) {
					console.trace(error);
					let mensaje = error.message == 'Empty or invalid card number' ? 'Número de tarjeta inválido' : error.message;
					alertify.notify("ERROR [" + mensaje + "]", 'falla', 10);
					$('#btn-agregarTarjeta').removeAttr('disabled').children('span').remove();
				}
			});
		});
		$('#btn_confirm_tarjeta').click(function(){
			let cvv2 = $('#cvv2').val().trim()
			if(cvv2.length == 0) return alertify.notify('Ingrese el código de seguridad de su tarjeta', 'custom');

			$this = $(this);
			generateDevice(function() {
				let datos = {deviceFingerPrint : deviceFingerPrint}
				$('#deviceFingerPrint').val(datos.deviceFingerPrint);
			});

			$(this).addClass('disabled');
			$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);
			if (alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro de realizar tu compra?', function() {
				$.ajax({
					url: '/Pagos/pagar/<?=session('id')?>',
					type: 'POST',
					data: {
						cvv : cvv2,
						deviceFingerPrint : $('#deviceFingerPrint').val(),
						referencia_cargo : $('#referencia_cargo').val(),
						plan : $('.plan_seleccionado').val(),
						datos : $this.attr('datos'),
						cupon: $('#tarjetaResumen_modal .cupon_ingresado').val()
					},
					success: function(data) {
						let respuesta = JSON.parse(data);
						$('#tarjetaResumen_modal').modal('hide');
						$($this).removeClass('disabled').children('span').remove();
						$('input[name="otros_pagos"]').prop('checked', false).parent().removeClass('active');
						$('#obtener_membresia .cerrar-modal').click();
						$('.modal').modal('hide');

						if (respuesta.alerta == 'custom'){
							$('#token_transaccion').val(respuesta.datos.transactionTokenId);
							let canProceed = netpay3ds.canProceed(respuesta.datos.status, respuesta.datos.threeDSecureResponse.responseCode, respuesta.datos.threeDSecureResponse.acsUrl);
							console.log("canProceed es: " + canProceed);
							
							if (!canProceed) return alertify.notify('Transacción rechadaza', 'falla');
							
							let proceed = netpay3ds.proceed(_this, respuesta.datos.threeDSecureResponse.acsUrl, respuesta.datos.threeDSecureResponse.paReq, respuesta.datos.threeDSecureResponse.authenticationTransactionID, callbackProceed);
						}
						
						alertify.notify(respuesta.mensaje, respuesta.alerta);
						return setTimeout((e) => location.href = "/compra_realizada?token="+respuesta.datos.transactionTokenId, 2000);
					},
					error: function(data) {
						let respuesta = JSON.parse(data.responseText);
						alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
					}
				});
			}, function() { 
				$($this).removeClass('disabled').children('span').remove();
			}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
		});
	</script>
	<?php } ?>
</main>
<?=$this->endSection()?>