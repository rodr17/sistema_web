<?=$this->extend('front/main')?>
<?=$this->section('title')?>
    ¿Quienes somos?
<?=$this->endSection()?>

<?=$this->section('content')?>
<section class="section-box bg-banner-about">
    <div class="banner-hero banner-about pt-20">
        <div class="banner-inner">
            <div class="row">
                <div class="col-lg-7 align-self-center">
                    <div class="block-banner">
                        <h1 class="heading-banner heading-lg">
                            ¿Quienes somos?
                        </h1>
                        <div class="banner-description mt-30">
                            Somos una plataforma de trabajo donde nos interesa unir y encontrar talento. Un lugar donde encuentres a las mejores personas para trabajar y en donde puedas desarrollar de manera independiente.
                            <br><br>
                            Una nueva forma de trabajar más libre, independiente y donde encuentres igualdad de oportunidades.
                            <br><br>
                            Donde tengas un proyecto en puerta y encuentres a expertos que te orienten y te apoyen para desarrollarlo. Un espacio en donde empresas, emprendedores y pymes puedan conectar con talentos para crear un negocio, sueño o marca.
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="banner-imgs">
                        <img alt="jobhub" src="<?=base_url('/assets/themes/imgs/page/about/banner-img.png')?>" class="img-responsive main-banner shape-3">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container mt-4">
    <div class="row">
        <div class="col-12 col-lg-4 my-1">
            <div class="card border-1 shadow p-3 card-hover">
                <div class="row">
                    <div class="col-3">
                        <img src="<?=base_url('/assets/images/iconos/achievement.svg')?>">
                    </div>
                    <div class="col-9">
                        <h5 class="titulo-card-quienes-somos">Nuestra mision</h5>
                        <p class="contenido-card-quienes-somos">
                            enim ut sem viverra aliquet eget sit amet tellus cras adipiscing enim eu turpis egestas pretium aenean pharetra magna ac
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4 my-1">
            <div class="card border-1 shadow p-3 card-hover">
                <div class="row">
                    <div class="col-3">
                        <img src="<?=base_url('/assets/images/iconos/mission.svg')?>">
                    </div>
                    <div class="col-9">
                        <h5 class="titulo-card-quienes-somos">Nuestra vision</h5>
                        <p class="contenido-card-quienes-somos">
                            enim ut sem viverra aliquet eget sit amet tellus cras adipiscing enim eu turpis egestas pretium aenean pharetra magna ac
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4 my-1">
            <div class="card border-1 shadow p-3 card-hover">
                <div class="row">
                    <div class="col-3">
                        <img src="<?=base_url('/assets/images/iconos/megaphone.svg')?>">
                    </div>
                    <div class="col-9">
                        <h5 class="titulo-card-quienes-somos">Nuestro objetivo</h5>
                        <p class="contenido-card-quienes-somos">
                            enim ut sem viverra aliquet eget sit amet tellus cras adipiscing enim eu turpis egestas pretium aenean pharetra magna ac
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-4">
        <h3 class="fs-3 my-5">Nuestros valores</h3>
    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-lg-2">
            Flexibilidad
        </div>
        <div class="col-12 col-lg-2">
            Respeto
        </div>
        <div class="col-12 col-lg-2">
            Honestidad
        </div>
        <div class="col-12 col-lg-2">
            Seguridad
        </div>
        <div class="col-12 col-lg-2">
            Innovación
        </div>
    </div>
</div>
<hr>
<?=$this->endSection()?>