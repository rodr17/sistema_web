<!doctype html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
        <!--Google Autenticacion-->
        <meta name="google-signin-scope" content="profile email">
        <meta name="google-signin-client_id" content="198033966029-a75kfv97som76od1s41e6gr26po9ppvh.apps.googleusercontent.com">
        <!--jQuery-->
        <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
        <!--Script para mostrar los iconos-->
        <script defer src="https://kit.fontawesome.com/d47f1b7d62.js" crossorigin="anonymous"></script>
        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous"> -->
        <!-- Bootstrap Scripts-->
        <script defer src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
        <!--Libreria para telefonos-->
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
        <!--Alertify-->
        <link href="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>
        <script defer src="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>
        <!--Google RECAPTCHA-->
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <!--Google maps-->
        <script defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAc3TX0tSUMk0psMUFxQBwkdqVzgxQJSgY"></script>
        <!--Datetimepickers-->
        <script defer src="<?=base_url('/assets/librerias/datetimepicker/jquery.datetimepicker.full.js')?>"></script>
        <script defer src="<?=base_url('/assets/librerias/datetimepicker/jquery.datetimepicker.full.min.js')?>"></script>
        <script defer src="<?=base_url('/assets/librerias/datetimepicker/jquery.datetimepicker.min.js')?>"></script>
        <link rel="stylesheet" type="text/css" href="<?=base_url('/assets/librerias/datetimepicker/jquery.datetimepicker.min.css')?>">
        <!--OpenPay-->
        <script defer type="text/javascript" src="<?=base_url('assets/js/openpay.v1.min.js')?>"></script>
        <script defer type='text/javascript' src="<?=base_url('assets/js/openpay-data.v1.min.js')?>"></script>
        <!--Hoja de estilos-->
        <link rel="stylesheet" type="text/css" href="<?=base_url('/assets/css/style.css')?>">
        <!--Estilos del tema-->
        <link rel="stylesheet" href="<?=base_url('/assets/themes/css/plugins/animate.min.css')?>">
        <link rel="stylesheet" href="<?=base_url('/assets/themes/css/main.css')?>?v=1.0">
        <!-- Template CSS -->
        <!-- <link rel="stylesheet" href="assets/css/plugins/animate.min.css" /> -->
        <!-- <link rel="stylesheet" href="assets/css/main.css?v=1.0" /> -->
        <script src="https://cdn.lordicon.com/xdjxvujz.js"></script>
        <title>Recuperar Cuenta&nbsp;-&nbsp;MEXTEMPS</title>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v13.0&appId=273149384935534&autoLogAppEvents=1" nonce="L4gxvrCc"></script>
    </head>
    <body>
        <div id="preloader-active">
            <div class="preloader d-flex align-items-center justify-content-center">
                <div class="preloader-inner position-relative">
                    <div class="text-center">
                        <img src="<?=base_url('assets/themes/imgs/theme/loading.gif')?>" alt="jobhub" />
                    </div>
                </div>
            </div>
        </div>
        <main class="main-no login-mt">
            <section class="section-box">
                <div class="container pt-0 login-container">
                    <div class="logo" style="text-align: center;">
                        <a href="index.php"><img alt="Mextemps" src="<?=base_url('assets/themes/imgs/theme/mextemps.svg')?>" width="245"></a>
                    </div>
                    <div class="row login-content">
                        <div class="col-xl-12 col-md-12 col-sm-12 mx-auto shadow card-grid-4 mt-10 mb-10" style="padding: 0px;">
                            <div class="row">
                                <div class="contact-from-area padding-20-row-col col-md-12 col-sm-12" style="padding: 20px 30px 20px 30px;">
                                <h4 style="text-align: center;" class="login-title">Recuperar cuenta</h4>
                                    <form class="contact-form-style mt-10 needs-validation" id="form-recuperar" enctype="multipart/form-data" novalidate>
                                        <div class="row wow animate__ animate__fadeInUp animated" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                            <div class="col-lg-12 col-md-12">
                                                <div class="input-style position-relative">
                                                    <span class="mb-10 my-2">
                                                        <input name="correo" type="email" placeholder="Ingrese su correo" required style="height: 40px;">
                                                        <div class="invalid-tooltip">Requerido</div>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 text-center">
                                                <button class="submit submit-auto-width" type="submit" id="btn-recuperar" style="padding: 10px 40px; border-radius: 30px; margin-top: 10px; width: 100%;">Recuperar</button>
                                                <p style="margin-top: 20px;">¿Aún no has creado tu cuenta en Mextemps? <a href="<?=base_url('/registrarme-como')?>">Regístrate</a> </p>
                                            </div>
                                        </div>
                                    </form>
                                    <p class="form-messege"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <script src="https://accounts.google.com/gsi/client" async defer></script>
        <script>
            $('#form-recuperar').submit(function(e) {
                e.preventDefault();
                $.ajax({
                    url: '<?= base_url('Autenticacion/buscarReactivarCuentaUsuario')?>',
                    type: 'POST',
                    data: $('#form-recuperar').serialize(),
                    success: function (data) {
                        let cont = JSON.parse(data);
                        return alertify.notify(cont.mensaje, cont.tipo);
                    },
                    error: function(data){
                        return alertify.error(data.responseText);
                    }
                });
            });
        </script>
        <!--Scripts de creados para funciones-->
        <?php if(session('alerta')) :?>
            <script defer>
                $(document).ready(function(){
                    alertify.notify('<?= session('alerta.mensaje')?>', '<?= session('alerta.tipo')?>');
                });
            </script>
        <?php endif; ?>
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <script defer src="<?=base_url('/assets/js/front.js')?>"></script>
        <!-- Scripts del tema -->
        <!-- Vendor JS-->
        <script src="<?=base_url('assets/themes/js/plugins/waypoints.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/plugins/wow.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/plugins/magnific-popup.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/plugins/perfect-scrollbar.min.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/plugins/select2.min.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/plugins/isotope.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/plugins/scrollup.js')?>"></script>
        <script src="<?=base_url('assets/themes/js/plugins/swiper-bundle.min.js')?>"></script>
        <!-- Template  JS -->
        <script src="<?=base_url('assets/themes/js/main.js?v=1.0')?>"></script>
    </body>
</html>