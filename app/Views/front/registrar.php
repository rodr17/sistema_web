<!doctype html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
		<!--Google Autenticacion-->
		<meta name="google-signin-scope" content="profile email">
		<meta name="google-signin-client_id" content="198033966029-a75kfv97som76od1s41e6gr26po9ppvh.apps.googleusercontent.com">
		<!--jQuery-->
		<script src="<?=base_url('assets/js/jquery/jquery_3_2_1.min.js')?>"></script>
		<!--Script para mostrar los iconos-->
		<script defer src="<?=base_url('assets/js/fontawesome/fontawesome.js')?>" crossorigin="anonymous"></script>
		<!-- Fonts -->
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="<?=base_url('assets/css/fonts_google/fonts_google.css')?>" rel="stylesheet">
		<!-- Bootstrap CSS -->
		<link href="<?=base_url('assets/css/bootstrap/bootstrap.min.css')?>" rel="stylesheet">
		<!--Alertify-->
		<link href="<?=base_url('assets/css/alertify/alertify.min.css')?>" rel="stylesheet"/>
		<link rel="stylesheet" href="<?=base_url('assets/css/alertify/bootstrap.min.css')?>"/>
		<script defer src="<?=base_url('assets/js/alertify/alertify.min.js')?>"></script>
		<!--Hoja de estilos-->
		<link rel="stylesheet" type="text/css" href="<?=base_url('/assets/css/style.css')?>">
		<!--Google RECAPTCHA-->
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
		<link rel="stylesheet" href="<?=base_url('/assets/themes/css/plugins/animate.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('/assets/themes/css/main.css')?>?v=1.0">
		<style>
			.g-recaptcha > div{ text-align: center !important;}
		</style>
		<title>Registro&nbsp;-&nbsp;MEXTEMPS</title>
		<script async defer>
			window.fbAsyncInit = function() {
				FB.init({
					appId      : '<?=get_Facebook('app_id')?>',
					cookie     : true,                     // Enable cookies to allow the server to access the session.
					xfbml      : true,                     // Parse social plugins on this webpage.
					version    : 'v16.0'           // Use this Graph API version for this call.
				});
				FB.AppEvents.logPageView();
			};
			(function(d, s, id){
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.src = "https://connect.facebook.net/es_LA/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
	</head>
	<body>
		<div id="preloader-active">
			<div class="preloader d-flex align-items-center justify-content-center">
				<div class="preloader-inner position-relative">
					<div class="text-center">
						<img src="<?=base_url('assets/themes/imgs/theme/loading.gif')?>" alt="jobhub"/>
					</div>
				</div>
			</div>
		</div>
		<main class="main-no registro-mt">
			<section class="section-box">
				<div class="container pt-0 login-container">
					<div class="logo" style="text-align: center;">
						<a href="<?=base_url()?>"><img alt="Mextemps" src="<?=base_url('assets/themes/imgs/theme/mextemps.svg')?>" width="245"></a>
					</div>
					<div class="row login-content">
						<div class="col-xl-12 col-md-12 col-sm-12 mx-auto shadow card-grid-4 mt-10 mb-10" style="padding: 0px;">
							<div class="row">
								<div class="contact-from-area padding-20-row-col col-md-12 col-sm-12" style="padding: 20px 30px 30px 30px;">
									<h4 style="text-align: center;" class="login-title">
										Crear cuenta <?= (session('pre-rol') == 'contratante')? 'Contratante' : 'freelancer' ?>
									</h4>
									<form class="contact-form-style mt-10 needs-validation" id="form-registrar" enctype="multipart/form-data" novalidate>
									<input hidden name="membresia-seleccionada" value="<?=session('plan')?>" id="membresia-seleccionada">
										<div class="row justify-content-center wow animate__ animate__fadeInUp animated" data-wow-delay=".1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
											<div class="col-lg-12 col-md-12">
												<div class="input-style mb-0 redes_sociales">
													<!-- <a title="Facebook" id="continuar_facebook" url="<?= $urlFacebook ?>" class="btn btn-social btn-block facebook mb-10" style="background-color: #3669b7; font-size: 14px; color: #fff; line-height: 25px; width: 100%;"><img style="vertical-align: middle;" src="https://wkncdn.com/newx/assets/build/img/svg/icon-facebook-inverted.7dbd739a0.svg"> Ingresar con Facebook</a> -->
													<a title="Google" id="continuar_google" url="<?= $urlGoogle ?>" class="btn btn-social btn-block google " style="background-color: #efefef; font-size: 14px; color: #333; line-height: 25px; width: 100%;"><img style="vertical-align: middle;" src="https://wkncdn.com/newx/assets/build/img/svg/icon-google-plus.4e58f2332.svg"> Ingresar con Google</a>
												</div>
											</div>
											<div class="text-separator">O</div>
											<div class="col-lg-12 col-md-12">
												<div class="input-style mb-10 position-relative">
													<input name="nombre" id="nombre" placeholder="Nombre" type="text" style="height: 40px;" required>
													<div class="invalid-feedback">
														Hola soy Mextemps, ¿cuáles son tus nombres?
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12">
												<div class="input-style mb-10 position-relative">
													<input name="apellidos" id="apellidos" placeholder="Apellidos" type="text" style="height: 40px;" required>
													<div class="invalid-feedback">
														Y ¿cuáles son tus apellidos?
													</div>
												</div>
											</div>   
											<div class="col-lg-12 col-md-12">
												<div class="input-style mb-10 position-relative">
													<input name="correo" id="correo" placeholder="Email" type="email" style="height: 40px;" required>
													<div class="invalid-feedback">
														¿Qué correo vas a registrar?
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12">
												<div class="input-style mb-10 position-relative">
													<input name="contraseña" id="contraseña" placeholder="Contraseña" type="password" style="height: 40px;" required>
													<div class="invalid-feedback">
														Se nos olvida un pequeño detalle... Si la contraseña.
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12">
												<div class="input-style mb-10 position-relative">
													<input name="confirm_contra" id="confirm_contra" placeholder="Confirmar contraseña" type="password" style="height: 40px;" required>
													<div class="invalid-feedback">
														Ayudame a confirmar por seguridad tu contraseña
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12">
												<small class="small ms-2" style="font-size: 11px;">Mínimo 8 caracteres, al menos 1 mayúscula y 1 caracter especial (#,*,%,$,?)</small>
											</div>
											<div class="g-recaptcha col-12 my-2" data-sitekey="<?= get_recaptcha('clave sitio') ?>"></div>
											<div class="col-lg-12 col-md-12 text-center">
												<label for="aceptar-terminos" class="small"><input style="width: 16px; vertical-align: middle; margin-right: 4px;" name="aceptar-terminos" id="aceptar-terminos" id="checkbox" type="checkbox"> Al crear tu cuenta, estás aceptando los <a href="<?=base_url('terminos-y-condiciones')?>" rel="noopener" target="_blank">términos de servicio</a> y la <a href="<?=base_url('aviso-de-privacidad')?>" target="_blank" rel="noopener">política de privacidad</a> de Mextemps.</label>
												<button class="submit submit-auto-width" type="submit" id="registro_membresia" style="padding: 10px 40px; border-radius: 30px; margin-top: 10px; width: 100%;">Crear cuenta</button>
												<p style="margin-top: 20px;" class="yaestas-registrado"> ¿Ya estás registrado en Mextemps? <a href="<?=base_url('ingresar')?>">Ingresa aquí</a> </p>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<div class="modal fade" id="elegir-membresia" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
				<div class="modal-dialog modal-xl modal-dialog-centered">
					<div class="modal-content">
						<div class="modal-header justify-content-center border-bottom-0">
							<h5 class="modal-title fw-bold" id="titulo">¿Cómo deseas registrarte?</h5>
						</div>
						<div class="modal-body">
							<div class="container">
								<?=view_cell('App\Libraries\FuncionesSistema::planesRol', ['rol' => session('pre-rol')])?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<script src="https://accounts.google.com/gsi/client" async defer></script>
			<script>
				$('.redes_sociales').click('a, img', function(event) {
					if (event.target.classList.contains('btn-social')) {
						let response = grecaptcha.getResponse();
						if (!$('#aceptar-terminos').is(':checked') || response == 0) {
							alertify.set('notifier', 'position', 'top-right');
							return alertify.notify('Ya casi terminamos, pero requiere que aceptes términos y condiciones. ¡Ah! y no se te olvide llenar el recaptca, no queremos ROBOTS', 'advertencia', 10);
						}
						$(location).attr('href', $(event.target).attr('url'));
					}
				});

				$('#registro_membresia').click(function(event) {
					if (event.target.form.nombre.value.length == 0 || event.target.form.apellidos.value.length == 0 ||
						event.target.form.contraseña.value.length == 0 || event.target.form.confirm_contra.value.length == 0) return true;
					event.preventDefault();
					$('.adver').removeClass('text-danger');
					let p = document.getElementById('contraseña').value,
						errors = [];
					if (p.length < 8) {
						errors.push("La contraseña debe tener un mínimo de 8 caracteres.");
					}
					if (p.search(/[a-z]/i) < 0) {
						errors.push("La contraseña debe tener al menos una letra."); 
					}
					if(p.search(/[A-Z]/) < 0){
						errors.push("La contraseña debe tener al menos una letra mayúscula.");
					}
					if(p.search(/[!@#$%^&*.?,]/) < 0){
						errors.push("La contraseña debe tener al menos un caracter especial, como: (*, &, #, ?, !)");
					}
					if (p.search(/[0-9]/) < 0) {
						errors.push("La contraseña debe tener al menos un número.");
					}
					if(errors.length <= 0){
						console.log('Contraseña valida');
					}else{
						let cadena = '';
						errors.forEach(element => cadena = cadena + element + '<br>');
						return alertify.notify(cadena, 'advertencia', 20);
					}
					
					if (event.target.form.contraseña.value != event.target.form.confirm_contra.value) return alertify.notify('Veo algo raro... Las contraseñas no coinciden.', 'custom', 10);

					$(this).attr('disabled', true);
					$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);
					alertify.set('notifier', 'position', 'top-right');

					let response = grecaptcha.getResponse();
					if(!$('#aceptar-terminos').is(':checked') || response == 0){
						$(this).removeAttr('disabled');
						$('#registro_membresia span').remove();
						return alertify.notify('Ya casi terminamos, pero requiere que aceptes términos y condiciones. ¡Ah! y no se te olvide llenar el recaptcha, no queremos ROBOTS', 'custom', 10);
					}

					$.ajax({
						url: '<?= base_url('Autenticacion/validacion_preRegistro') ?>',
						type: 'POST',
						data: $('#form-registrar').serialize(),
						success: function(data) {
							let respuesta = JSON.parse(data);
							if (respuesta.alerta != 'correcto') {
								$('#registro_membresia').removeAttr('disabled').children('span').remove();
								return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
							}
							<?php if(session('plan') == ''){ ?>
								$('#elegir-membresia').modal('show');
							<?php }else{ ?>
								enviarRegistrar();
							<?php } ?>
						},
						error: function(data) {
							return alertify.error(data.responseText, 'falla', 10);
						}
					});
				});
				$('.boton').click('a', function(e) {
					if (e.target.name != undefined) {
						$(e.target).addClass('disabled', true);
						$(e.target).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(e.target);
						$('#membresia-seleccionada').val(e.target.name);
						$.ajax({
							url: '<?= base_url('Autenticacion/registrar') ?>',
							type: 'POST',
							data: $('#form-registrar').serialize(),
							success: function(data) {
								let respuesta = JSON.parse(data);
								// $(e.target).removeClass('disabled').children('span').remove();
								$('#elegir-membresia').modal('hide');
								// $('#registro_membresia').removeAttr('disabled').childre('span').remove();
								if (respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
								<?php if(session('pre-rol') == 'freelancer'){ ?>   
									return $(location).attr('href', '<?= base_url('Llenar_perfil') ?>');
								<?php }else{ ?>
									return $(location).attr('href', '<?=base_url('Completar_registro')?>')
								<?php } ?>
							},
							error: function(data) {
								return alertify.error(data.responseText, 'falla', 10);
							}
						});
					}
				});
				<?php if(session('plan') != ''){ ?>
					function enviarRegistrar(){
						$.ajax({
							url: '<?= base_url('Autenticacion/registrar') ?>',
							type: 'POST',
							data: $('#form-registrar').serialize(),
							success: function(data) {
								let respuesta = JSON.parse(data);
								// $(e.target).removeClass('disabled').children('span').remove();
								$('#elegir-membresia').modal('hide');
								// $('#registro_membresia').removeAttr('disabled').childre('span').remove();
								if (respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
								<?php if(session('pre-rol') == 'freelancer'){ ?>   
									return $(location).attr('href', '<?= base_url('Llenar_perfil') ?>');
								<?php }else{ ?>
									return $(location).attr('href', '<?=base_url('Completar_registro')?>')
								<?php } ?>
							},
							error: function(data) {
								return alertify.error(data.responseText, 'falla', 10);
							}
						});
					}
				<?php } ?>
			</script>
		</main>

		<!--Scripts de creados para funciones-->
		<?php if(session('alerta')) :?>
			<script defer>
				$(document).ready(function(){
					alertify.notify('<?= session('alerta.mensaje')?>', '<?= session('alerta.tipo')?>', 10);
				});
			</script>
		<?php endif; ?>
		<script src="<?=base_url('assets/js/google_api/google_api.js')?>" async defer></script>
		<script defer src="<?=base_url('assets/js/bootstrap/popper.min.js')?>"></script>
		<script defer src="<?=base_url('assets/js/bootstrap/bootstrap.min.js')?>"<></script>
		<!-- Scripts del tema -->
		<!-- Vendor JS-->
		<script src="<?=base_url('assets/themes/js/plugins/waypoints.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/wow.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/magnific-popup.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/perfect-scrollbar.min.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/select2.min.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/isotope.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/scrollup.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/swiper-bundle.min.js')?>"></script>
		<!-- Template  JS -->
		<script src="<?=base_url('assets/themes/js/main.js?v=1.0')?>"></script>
		<script defer src="<?=base_url('/assets/js/front.js')?>"></script>
	</body>
</html>