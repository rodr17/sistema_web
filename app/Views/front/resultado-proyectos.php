<?= $this->extend('front/main') ?>
<?= $this->section('librerias')?>
<style>
.form-group input:focus {
	background: #ffffff !important;
}
.input-h{
	height: 27px !important;
}
</style>
<?= $this->endSection()?>
<?= $this->section('title') ?>
	Resultados de "<?=$busqueda?>"
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<script src="<?=base_url('assets/themes/js/noUISlider.js')?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinysort/3.2.5/tinysort.min.js" integrity="sha512-P3lisBxY1PusizpelihVrBeciBfWA9r7Ff/8j16b4K399OWJIrY1UdzdE7/BctqKv6Xujr8hp3IA2XiQX3Bsew==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<main class="main resultado-proyectos">
	<section class="section-box-2">
		<div class="box-head-single none-bg">
			<div class="container">
				<?php if(view_cell('App\Libraries\FuncionesSistema::cantDisponiblesProyectos') > 50){ ?>
					<h4>¡Tenemos más de <?= view_cell('App\Libraries\FuncionesSistema::cantDisponiblesProyectos') ?> Oportunidades para tí!</h4>
				<?php }else{ ?>
					<h4>¡Tenemos diferentes oportunidades para ti!</h4>
				<?php } ?>
				<div class="row mt-15 mb-40 descubriendo">
					<div class="col-lg-12 col-md-12">
						<span class="text-mutted">Descubre tu próximo proyecto profesional.</span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section-box contenedor-informaicion">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-12 col-sm-12 col-12">
					<div class="sidebar-shadow none-shadow mb-30">
						<div class="sidebar-filters">
							<div class="filter-block">
								<h5 class="medium-heading mb-15 text-center text-md-start">Palabra clave</h5>
								<form class="" id="form_buscador_palabra">
									<div class="form-group">
										<input type="text" class="form-control form-icons" id="search" name="search" placeholder="Buscar..." style="height: 50px;">
										<i class="fi fi-rr-search"></i>
									</div>
									<div class="row">
										<div class="col-6 col-lg-4 d-grid gap-1">
											<button class="btn btn-default" type="submit" id="filtrar-palabra">Buscar</button>
										</div>
										<div class="col-6 col-lg-8 d-grid gap-1">
											<a class="btn btn-default d-lg-none" type="button" data-bs-toggle="offcanvas" data-bs-target="#contenido_canvas" aria-controls="contenido_canvas">Filtro avanzado</a>
										</div>
									</div>
								</form>
							</div>
							<div class="buttons-filter text-center text-md-start">
								<div class="offcanvas offcanvas-bottom d-lg-none p-2" tabindex="-1" id="contenido_canvas" aria-labelledby="titulo_canvas" style="height: 75vh;">
									<div class="offcanvas-header">
										<h5 class="offcanvas-title" id="titulo_canvas">Filtrar Proyectos</h5>
										<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
									</div>
									<div class="offcanvas-body" id="contenido_filtro_canvas" style="overflow-x: clip;">
										<form id="search-proyectos-canvas">
											<!--CATEGORIA-->
											<div class="filter-block mb-30">
												<h5 class="medium-heading mb-15 text-center text-md-start">Categoría</h5>
												<div class="form-group select-style select-style-icon">
													<select class="form-control form-icons select-active select2-hidden-accessible" id="area-canvas" name="area" data-select2-id="1" tabindex="-1" aria-hidden="true">
														<option selected hidden value="">Seleccione una opción</option>
														<?php foreach($areas as $a){ ?>
															<option value="<?=$a['area']?>" <?=($a['area'] == $area)? 'selected' : '' ?>><?=$a['area']?></option>
														<?php } ?>
													</select>
													<i class="fi fi-rr-list"></i>
												</div>
											</div>
											<!--AREA DE EXPERIENCIA-->
											<div class="filter-block mb-30">
												<h5 class="medium-heading mb-15 text-center text-md-start">Área de experiencia</h5>
												<div class="form-group select-style select-style-icon">
													<select class="form-control form-icons select-active select-hidden-accessible" id="habilidad-canvas" name="habilidades" data-select-id="1" tabindex="-1" aria-hidden="true">
														<option value="" selected>Seleccione una opción</option>
														<?php if($area == null){ ?>
															<?php foreach($subareas as $s){ ?>
																<option value="<?=$s['area']?>"><?=$s['area']?></option>
															<?php } ?>
														<?php }else{ ?>
															<?php foreach($subareas as $s){ ?>
																<?php if($s['superior'] == $area){ ?>
																	<option value="<?=$s['area']?>"><?=$s['area']?></option>
																<?php } ?>
															<?php } ?>
														<?php } ?>
													</select>
													<i class="fi fi-rr-list"></i>
												</div>
											</div>
											<!--UBICACION-->
											<div class="filter-block mb-30">
												<h5 class="medium-heading mb-15 text-center text-md-start">Estado</h5>
												<div class="form-group select-style select-style-icon">
													<select class="form-control form-icons select-active select-hidden-accessible" id="estado-canvas" name="estado" data-select3-id="1" tabindex="1" aria-hidden="true">
														<option value="" selected>Seleccione una opción</option>
														<?php foreach($estados as $e){ ?>
															<option value="<?=$e['nombre']?>"><?=$e['nombre']?></option>
														<?php } ?>
													</select>
													<i class="fi-rr-marker"></i>
												</div>
											</div>
											<!--DURACION DEL PROYECTO-->
											<div class="filter-block mb-30">
												<h5 class="medium-heading mb-15 text-center text-md-start">Duración del proyecto</h5>
												<div class="form-group">
													<ul class="list-checkbox mb-0">
														<li>
															<label class="cb-container">
																<input class="input-h" type="checkbox" name="tiempo[]" value="1"> <span class="text-small">1 - 10 días</span>
																<span class="checkmark"></span>
															</label>
															<span class="number-item">
																<?= view_cell('App\Libraries\FuncionesSistema::cantidadProyectosTiempo', ['minimo' => 1, 'maximo' => 10]) ?>
															</span>
														</li>
														<li>
															<label class="cb-container">
																<input class="input-h" type="checkbox" name="tiempo[]" value="2"> <span class="text-small">11 - 30 días</span>
																<span class="checkmark"></span>
															</label>
															<span class="number-item">
																<?= view_cell('App\Libraries\FuncionesSistema::cantidadProyectosTiempo', ['minimo' => 11, 'maximo' => 30]) ?>
															</span>
														</li>
														<li>
															<label class="cb-container">
																<input class="input-h" type="checkbox" name="tiempo[]" value="3"> <span class="text-small">+ 1 mes</span>
																<span class="checkmark"></span>
															</label>
															<span class="number-item">
																<?= view_cell('App\Libraries\FuncionesSistema::cantidadProyectosTiempo', ['minimo' => 31, 'maximo' => 100000]) ?>
															</span>
														</li>
													</ul>
												</div>
											</div>
											<!--NIVEL SOLICITADO-->
											<div class="filter-block mb-30 d-none">
												<h5 class="medium-heading mb-10">Nivel solicitado</h5>
												<div class="form-group">
													<ul class="list-checkbox">
														<li>
															<label class="cb-container">
																<input type="checkbox" name="nivel[]" value="Experto"> <span class="text-small">Experto</span>
																<span class="checkmark"></span>
															</label>
															<span class="number-item">
																<?= view_cell('App\Libraries\FuncionesSistema::cantidadProyectosNivel', ['nivel' => 'Experto']) ?>
															</span>
														</li>
														<li>
															<label class="cb-container">
																<input type="checkbox" name="nivel[]" value="Avanzado"> <span class="text-small">Avanzado</span>
																<span class="checkmark"></span>
															</label>
															<span class="number-item">
																<?= view_cell('App\Libraries\FuncionesSistema::cantidadProyectosNivel', ['nivel' => 'Avanzado']) ?>
															</span>
														</li>
														<li>
															<label class="cb-container">
																<input type="checkbox" name="nivel[]" value="Intermedio"> <span class="text-small">Intermedio</span>
																<span class="checkmark"></span>
															</label>
															<span class="number-item">
																<?= view_cell('App\Libraries\FuncionesSistema::cantidadProyectosNivel', ['nivel' => 'Intermedio']) ?>
															</span>
														</li>
														<li>
															<label class="cb-container">
																<input type="checkbox" name="nivel[]" value="Regular"> <span class="text-small">Regular</span>
																<span class="checkmark"></span>
															</label>
															<span class="number-item">
																<?= view_cell('App\Libraries\FuncionesSistema::cantidadProyectosNivel', ['nivel' => 'Regular']) ?>
															</span>
														</li>
														<li>
															<label class="cb-container">
																<input type="checkbox" name="nivel[]" value="Básico"> <span class="text-small">Básico</span>
																<span class="checkmark"></span>
															</label>
															<span class="number-item">
																<?= view_cell('App\Libraries\FuncionesSistema::cantidadProyectosNivel', ['nivel' => 'Básico']) ?>
															</span>
														</li>
													</ul>
												</div>
											</div>
											<!--SLIDE DE PRECIO-->
											<div class="filter-block mb-40">
												<h5 class="medium-heading mb-25 text-center text-md-start">Presupuesto</h5>
												<div class="">
													<div class="row mb-20">
														<div class="col-sm-12">
															<div id="slider-range-canvas"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-6">
															<label class="lb-slider">Desde</label>
															<div class="form-group minus-input">
																<input type="text" name="min-value-money" class="input-disabled form-control min-value-money" disabled="disabled" value="" />
																<input type="hidden" name="minimo" class="form-control min-value" value="" />
															</div>
														</div>
														<div class="col-lg-6">
															<label class="lb-slider">Hasta</label>
															<div class="form-group">
																<input type="text" name="max-value-money" class="input-disabled form-control max-value-money" disabled="disabled" value="" />
																<input type="hidden" name="maximo" class="form-control max-value" value="" />
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="buttons-filter">
												<div class="row">
													<div class="col-6 d-grid gap-2">
														<a class="btn btn-default" id="filtrar-canvas">Buscar</a>
													</div>
													<div class="col-6 d-grid gap-2">
														<a class="btn btn-default btn-md btn-limpiar" id="limpiar">Limpiar filtro</a>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="sidebar-shadow none-shadow mb-30 d-none d-lg-block">
						<div class="sidebar-filters filtro_abajo">
							<form id="search-proyectos">
								<div class="filter-block mb-30">
									<h5 class="medium-heading mb-15 text-center text-md-start">Categoría</h5>
									<div class="form-group select-style select-style-icon">
										<select class="form-control form-icons select-active select-hidden-accessible" id="habilidad" name="habilidades" data-select-id="1" tabindex="-1" aria-hidden="true">
											<option value="" selected>Seleccione una opción</option>
											<?php if($area == null){ ?>
												<?php foreach($subareas as $s){ ?>
													<option value="<?=$s['area']?>"><?=$s['area']?></option>
												<?php } ?>
											<?php }else{ ?>
												<?php foreach($subareas as $s){ ?>
													<?php if($s['superior'] == $area){ ?>
														<option value="<?=$s['area']?>"><?=$s['area']?></option>
													<?php } ?>
												<?php } ?>
											<?php } ?>
										</select>
										<i class="fi fi-rr-list"></i>
									</div>
								</div>
								<div class="filter-block mb-30">
									<h5 class="medium-heading mb-15 text-center text-md-start">Estado</h5>
									<div class="form-group select-style select-style-icon">
										<select class="form-control form-icons select-active select-hidden-accessible" id="estado" name="estado" data-select3-id="1" tabindex="1" aria-hidden="true">
											<option value="" selected>Seleccione una opción</option>
											<?php foreach($estados as $e){ ?>
												<option value="<?=$e['nombre']?>"><?=$e['nombre']?></option>
											<?php } ?>
										</select>
										<i class="fi-rr-marker"></i>
									</div>
								</div>
								<div class="filter-block mb-30">
									<h5 class="medium-heading mb-15 text-center text-md-start">Duración del proyecto</h5>
									<div class="form-group">
										<ul class="list-checkbox mb-0">
											<li>
												<label class="cb-container">
													<input class="input-h" type="checkbox" name="tiempo[]" value="1"> <span class="text-small">1 - 10 días</span>
													<span class="checkmark"></span>
												</label>
												<span class="number-item">
													<?= view_cell('App\Libraries\FuncionesSistema::cantidadProyectosTiempo', ['minimo' => 1, 'maximo' => 10]) ?>
												</span>
											</li>
											<li>
												<label class="cb-container">
													<input class="input-h" type="checkbox" name="tiempo[]" value="2"> <span class="text-small">11 - 30 días</span>
													<span class="checkmark"></span>
												</label>
												<span class="number-item">
													<?= view_cell('App\Libraries\FuncionesSistema::cantidadProyectosTiempo', ['minimo' => 11, 'maximo' => 30]) ?>
												</span>
											</li>
											<li>
												<label class="cb-container">
													<input class="input-h" type="checkbox" name="tiempo[]" value="3"> <span class="text-small">+ 1 mes</span>
													<span class="checkmark"></span>
												</label>
												<span class="number-item">
													<?= view_cell('App\Libraries\FuncionesSistema::cantidadProyectosTiempo', ['minimo' => 31, 'maximo' => 100000]) ?>
												</span>
											</li>
										</ul>
									</div>
								</div>
								<div class="filter-block mb-30 d-none">
									<h5 class="medium-heading mb-10">Nivel solicitado</h5>
									<div class="form-group">
										<ul class="list-checkbox">
											<li>
												<label class="cb-container">
													<input type="checkbox" name="nivel[]" value="Experto"> <span class="text-small">Experto</span>
													<span class="checkmark"></span>
												</label>
												<span class="number-item">
													<?= view_cell('App\Libraries\FuncionesSistema::cantidadProyectosNivel', ['nivel' => 'Experto']) ?>
												</span>
											</li>
											<li>
												<label class="cb-container">
													<input type="checkbox" name="nivel[]" value="Avanzado"> <span class="text-small">Avanzado</span>
													<span class="checkmark"></span>
												</label>
												<span class="number-item">
													<?= view_cell('App\Libraries\FuncionesSistema::cantidadProyectosNivel', ['nivel' => 'Avanzado']) ?>
												</span>
											</li>
											<li>
												<label class="cb-container">
													<input type="checkbox" name="nivel[]" value="Intermedio"> <span class="text-small">Intermedio</span>
													<span class="checkmark"></span>
												</label>
												<span class="number-item">
													<?= view_cell('App\Libraries\FuncionesSistema::cantidadProyectosNivel', ['nivel' => 'Intermedio']) ?>
												</span>
											</li>
											<li>
												<label class="cb-container">
													<input type="checkbox" name="nivel[]" value="Regular"> <span class="text-small">Regular</span>
													<span class="checkmark"></span>
												</label>
												<span class="number-item">
													<?= view_cell('App\Libraries\FuncionesSistema::cantidadProyectosNivel', ['nivel' => 'Regular']) ?>
												</span>
											</li>
											<li>
												<label class="cb-container">
													<input type="checkbox" name="nivel[]" value="Básico"> <span class="text-small">Básico</span>
													<span class="checkmark"></span>
												</label>
												<span class="number-item">
													<?= view_cell('App\Libraries\FuncionesSistema::cantidadProyectosNivel', ['nivel' => 'Básico']) ?>
												</span>
											</li>
										</ul>
									</div>
								</div>
								<div class="filter-block mb-40">
									<h5 class="medium-heading mb-25 text-center text-md-start">Presupuesto</h5>
									<div class="">
										<div class="row mb-20">
											<div class="col-sm-12">
												<div id="slider-range"></div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<label class="lb-slider">Desde</label>
												<div class="form-group minus-input">
													<input type="text" name="min-value-money" class="input-disabled form-control min-value-money" disabled="disabled" value="" />
													<input type="hidden" name="minimo" class="form-control min-value" value="" />
												</div>
											</div>
											<div class="col-lg-6">
												<label class="lb-slider">Hasta</label>
												<div class="form-group">
													<input type="text" name="max-value-money" class="input-disabled form-control max-value-money" disabled="disabled" value="" />
													<input type="hidden" name="maximo" class="form-control max-value" value="" />
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="buttons-filter">
									<div class="row">
										<div class="col-6 d-grid gap-2">
											<a class="btn btn-default" id="filtrar">Buscar</a>
										</div>
										<div class="col-6 d-grid gap-2">
											<a class="btn btn-default btn-md btn-limpiar" id="limpiar">Limpiar filtro</a>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="sidebar-with-bg background-naranja bg-sidebar pb-80 d-none d-lg-block">
						<h5 class="medium-heading text-white mb-20 mt-20">¿Reclutamiento?</h5>
						<p class="text-body-999 text-white mb-30">Anuncia tus proyectos para llegar a miles de usuarios mensuales, y busqué entre más de 14,000 CV en nuestra base de datos.</p>
						<?php if(autorizacion() == 'contratista'){ ?>
							<a href="<?=base_url('proyecto')?>" class="text-decoration-none btn-border icon-chevron-right btn-white-sm">Publicar proyecto</a>
						<?php }else{ ?>
							<a id="bt-pub" class="text-decoration-none boton-reclutamiento btn-border icon-chevron-right btn-white-sm">Publicar proyecto</a>
							<script>
								$('#bt-pub').click(function(){
									alertify.notify('Para poder publicar un proyecto debes iniciar sesión como contratante.', 'advertencia');
								});
							</script>
						<?php } ?>
					</div>
					<div class="sidebar-azulmt-bg d-none d-lg-block">
						<h5 class="font-semibold mb-10">Suscríbete ahora</h5>
						<p class="text-body-999">Recibe aviso de los últimos proyectos publicados.</p>
						<div class="box-email-reminder">
							<form class="needs-validation form-newsletter" id="boletines_suscripcion" novalidation encytpe="multipart/form-data">
								<div class="form-group mt-15">
									<input type="email" class="form-control input-bg-white form-icons" name="correo" id="correo" placeholder="usuario@gmail.com" required/>
									<i class="fi-rr-envelope"></i>
								</div>
								<div class="form-group mt-25 mb-5">
									<button class="btn btn-default btn-md" type="submit">Suscribirme</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-lg-9 col-md-12 col-sm-12 col-12">
					<div class="content-page">
						<div class="box-filters-job mt-15 mb-10">
							<div class="row">
								<div class="col-lg-7 subtitulos">
									<?php if(view_cell('App\Libraries\FuncionesSistema::cantDisponiblesProyectos') > 50){ ?>
										<span class="text-small">Mostrando <strong id="total-strong"><?=count($proyectos)?></strong> de <strong><?= view_cell('App\Libraries\FuncionesSistema::cantDisponiblesProyectos') ?> </strong>trabajos</span>
									<?php }else{ ?>
										<span class="text-small">Mostrando <strong id="total-strong"><?=count($proyectos)?></strong> proyectos para ti.</span>
									<?php } ?>
								</div>
								<div class="col-lg-5 text-lg-end mt-sm-15 ordenamiento">
									<div class="d-flex justify-content-between justify-content-md-end gap-2">
										<span class="text-sortby">Ordenar por:</span>
										<div class="dropdown dropdown-sort">
											<button class="btn dropdown-toggle" type="button" id="dropdownSort" data-bs-toggle="dropdown" aria-expanded="false" data-bs-display="static"><span>Más nuevos</span> <i class="fi-rr-angle-small-down"></i></button>
											<ul class="dropdown-menu dropdown-menu-light" aria-labelledby="dropdownSort">
												<li><a class="dropdown-item order-cards active" data-ord="news">Más nuevos</a></li>
												<li><a class="dropdown-item order-cards" data-ord="old">Más viejos</a></li>
												<li><a class="dropdown-item order-cards" data-ord="max">Mayor presupuesto</a></li>
												<li><a class="dropdown-item order-cards" data-ord="min">Menor presupuesto</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="my-3 d-flex justify-content-start flex-wrap" id="contenedor_etiquetas_busqueda">
								<?php 
									if($_GET['search']) echo("<span class='badge text-font rounded-pill bg-primary lh-sm me-3 mb-2' style='font-weight:500'>Buscar como: \"". $_GET['search'] ."\"</span>");
									if($_GET['area']) echo("<span class='badge text-font rounded-pill bg-primary lh-sm me-3 mb-2' style='font-weight:500'>Categoría: \"". $_GET['area'] ."\"</span>");
								?>
							</div>
						</div>
						<div class="job-list-list mb-15">
							<div id="sec-proyectos" class="list-recent-jobs">
								<?php if($proyectos != null){ ?>
									<?php foreach($proyectos as $pro){ ?>
										<?=view_cell('App\Libraries\FuncionesSistema::cardProyectoDisponible', ['proyecto' => $pro])?>
									<?php } ?>
									<?php
										$ids = '';
										foreach($proyectos as $key => $project){
											$ids .= $project['id'];
											($key < (count($proyectos) - 1))? $ids .= ',' : $ids .= '' ;
										}
										echo '<input id="ids" type="hidden" value="'.$ids.'">';
										echo '<input id="total" type="hidden" value="'.count($proyectos).'">';
									?>
								<?php }else{ ?>
									<?php vacio();?>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php if(session('logeado')) :?>
		<div class="modal fade" id="postular_a_proyecto" data-bs-backdrop="static" data-bs-keyboard="true" aria-hidden="true" tabindex="-1">
			<div class="modal-dialog modal-dialog-centered modal-lg">
				<div class="modal-content">
					<div class="modal-header justify-content-center border-0">
						<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#perfil" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
						<h5 class="modal-title h4">Postularme al proyecto</h5>
					</div>
					<div class="modal-body py-0">
						<div class="row align-items-center text-center">
							<div class="col-md-5 p-4 listado-talentos">
								<small class="text-font fst-italic">Tu perfil como lo verá el contratante</small>
								<?php $monto = model('ModelPlanes')->selectMax('monto')->where('cuenta', 'freelancer')->get()->getRowArray()['monto'];
									$con_suscripcion_premium = (model('Suscripciones')
									->builder()
									->join('planes', '(id_plan = id_plan_mensual) OR (id_plan = id_plan_anual)')
									->where(['id_usuario' => session('id'), 'estatus' => 'Activo', 'monto' => $monto])
									->get()->getRowArray());
								?>
								<?= view_cell('App\Libraries\FuncionesSistema::cardPostulantes', ['id' => session('id'), 'postu' => 0, 'tipo' => 'talento', 'modal' => 0, 'es_premium' => !empty($con_suscripcion_premium) ? true : false]) ?>
							</div>
							<div class="col-md-7">
								<p class="text-font text-start p-2">
									Estás a punto de postularte al proyecto <strong id="titulo_proyecto_modal"></strong>, recuerda que mientras más completo este tu perfil, mayor es la posibilidad de ser escogido para desarrollar el proyecto, recuerda mantener una actitud cordial y respetuosa con cada miembro de nuestra plataforma. De parte del equipo Mextemps te deseamos la mejor de las suertes.
								</p>
								<a class="btn btn-naranja btn-border my-2" id="enviar_postulacion">Postularse</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif ;?>
</main>
<script>
	$(document).ready(function () {
		$(".noUi-handle").on("click", function () {
			$(this).width(50);
		});
		var rangeSlider = $('#slider-range')[0];
		var rangeSlider_canvas = $('#slider-range-canvas')[0];
		var moneyFormat = wNumb({
			decimals: 0,
			thousand: ",",
			prefix: "$"
		});
		noUiSlider.create(rangeSlider, {
			start: [<?=$minimo?>, <?=$maximo?>],
			step: 1,
			range: {
				min: [<?=$minimo?>],
				max: [<?=$maximo + 100?>]
			},
			format: moneyFormat,
			connect: true
		});
		noUiSlider.create(rangeSlider_canvas, {
			start: [<?=$minimo?>, <?=$maximo?>],
			step: 1,
			range: {
				min: [<?=$minimo?>],
				max: [<?=$maximo + 100?>]
			},
			format: moneyFormat,
			connect: true
		});
		rangeSlider.noUiSlider.on("update", function (values, handle) {
			$(".min-value-money").val(values[0]);
			$(".max-value-money").val(values[1]);
			$(".min-value").val(moneyFormat.from(values[0]));
			$(".max-value").val(moneyFormat.from(values[1]));
		});
		rangeSlider_canvas.noUiSlider.on("update", function (values, handle) {
			$(".min-value-money").val(values[0]);
			$(".max-value-money").val(values[1]);
			$(".min-value").val(moneyFormat.from(values[0]));
			$(".max-value").val(moneyFormat.from(values[1]));
		});
	});
	$('.order-cards').click(function(){
		let tipo = $(this).attr('data-ord');
		let ids = $('#ids').val();
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Proyectos/reordenarProyectos')?>',
			data:{tipo:tipo, ids:ids},
			success: function(data){
				$('#sec-proyectos').html(data);
				$('#total-strong').html($('#total').val());
			}, error: function(data){
				alertify.error('Opps, a surgido un error, intenta de nuevo.', 10);
			}
		});
	});
	/////////////////////////////////////////////////////////////////Funcion para filtrar proyectos
	$('#form_buscador_palabra').submit(function(e){
		e.preventDefault();
		$search = $('#search');
		console.log($search.val());
		$contenedor = $('#contenedor_etiquetas_busqueda');
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/proyectos/filtrarResultadosProyectos')?>',
			data: {search: $search.val(), filtro_palabra: 'true'},
			success: function(data){
				let respuesta = JSON.parse(data);
				$('#sec-proyectos').html(respuesta.html);
				$contenedor.empty();
				if($search.val().length != 0) $contenedor.append('<span class="badge text-font rounded-pill bg-primary lh-sm me-3 mb-2" style="font-weight:500">Buscar como: '+$search.val()+' <a class="p-1 text-light" onclick="elimiBadSearch(this)"><i class="far fa-times-circle"></i><a></span>');
				$('#total-strong').text(respuesta.cantidad_proyectos);
			}, error: function(data){
				alertify.notify('Surgió un error, comuníquese con el área de soporte.', 'falla', 10);
			}
		});
	});
	$('#filtrar').click(function(){
		$area = $('#area');
		$subarea = $('#habilidad');
		$duracion_proyecto = $('input[name="tiempo[]"]');
		$nivel = $('input[name="nivel[]"]');
		$presupuesto_min = $(".min-value-money");
		$presupuesto_max = $(".max-value-money");
		$contenedor = $('#contenedor_etiquetas_busqueda');
		$estado = $('#estado');
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/proyectos/filtrarResultadosProyectos')?>',
			data: $('#search-proyectos').serialize(),
			success: function(data){
				let respuesta = JSON.parse(data);
				$('#contenido_canvas').offcanvas('hide');
				$('#sec-proyectos').html(respuesta.html);
				$contenedor.empty();
				if($area.val().length != 0) $contenedor.append('<span class="badge text-font rounded-pill bg-primary lh-sm me-3 mb-2" style="font-weight:500">Categoría: '+ $area.val() +' <a class="p-1 text-light" onclick="elimiBadArea(this)"><i class="far fa-times-circle"></i><a></span>');
				if($subarea.val().length != 0) $contenedor.append('<span class="badge text-font rounded-pill bg-primary lh-sm me-3 mb-2" style="font-weight:500">Subcategoría: '+ $subarea.val() +' <a class="p-1 text-light" onclick="elimiBadHabilidad(this)"><i class="far fa-times-circle"></i><a></span>');
				if($estado.val().length != 0) $contenedor.append('<span class="badge text-font rounded-pill bg-primary lh-sm me-3 mb-2" style="font-weight:500">Estado: '+ $estado.val() +' <a class="p-1 text-light" onclick="elimiBadEstado(this)"><i class="far fa-times-circle"></i><a></span>');
				document.querySelectorAll('input[name="tiempo[]"]').forEach(function(e){
					if(e.checked) $contenedor.append('<span class="badge text-font rounded-pill bg-primary lh-sm me-3 mb-2" style="font-weight:500">Tiempo: '+e.nextElementSibling.innerText+' <a class="p-1 text-light" onclick="elimiBadTiempo(this, '+e.value+')"><i class="far fa-times-circle"></i><a></span>');
				});
				document.querySelectorAll('input[name="nivel[]"]').forEach(function(evento){
					if(evento.checked) $contenedor.append("<span class='badge text-font rounded-pill bg-primary lh-sm me-3 mb-2' style='font-weight:500'>Experiencia: "+evento.nextElementSibling.innerText+"</span>");
				});
				if($presupuesto_min.val().length != 0 || $presupuesto_max.val().length != 0) $contenedor.append("<span class='badge text-font rounded-pill bg-primary lh-sm me-3 mb-2' style='font-weight:500'>Presupuesto: "+$presupuesto_min.val()+" - "+$presupuesto_max.val());
				$('#total-strong').text(respuesta.cantidad_proyectos);
			}, error: function(data){
				alertify.notify('Surgió un error, comuníquese con el área de soporte.', 'falla', 10);
			}
		});
	});
	$('#filtrar-canvas').click(function(){
		$area = $('#area-canvas');
		$subarea = $('#habilidad-canvas');
		$duracion_proyecto = $('#search-proyectos-canvas input[name="tiempo[]"]');
		$nivel = $('#search-proyectos-canvas input[name="nivel[]"]');
		$presupuesto_min = $("#search-proyectos-canvas .min-value-money");
		$presupuesto_max = $("#search-proyectos-canvas .max-value-money");
		$contenedor = $('#contenedor_etiquetas_busqueda');
		$estado = $('#estado-canvas');
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/proyectos/filtrarResultadosProyectos')?>',
			data: $('#search-proyectos-canvas').serialize(),
			success: function(data){
				let respuesta = JSON.parse(data);
				$('#contenido_canvas').offcanvas('hide');
				$('#sec-proyectos').html(respuesta.html);
				$contenedor.empty();
				if($area.val().length != 0) $contenedor.append('<span class="badge text-font rounded-pill bg-primary lh-sm me-3 mb-2" style="font-weight:500">Categoría: '+ $area.val() +' <a class="p-1 text-light" onclick="elimiBadArea(this)"><i class="far fa-times-circle"></i><a></span>');
				if($subarea.val().length != 0) $contenedor.append('<span class="badge text-font rounded-pill bg-primary lh-sm me-3 mb-2" style="font-weight:500">Subcategoría: '+ $subarea.val() +' <a class="p-1 text-light" onclick="elimiBadHabilidad(this)"><i class="far fa-times-circle"></i><a></span>');
				if($estado.val().length != 0) $contenedor.append('<span class="badge text-font rounded-pill bg-primary lh-sm me-3 mb-2" style="font-weight:500">Estado: '+ $estado.val() +' <a class="p-1 text-light" onclick="elimiBadEstado(this)"><i class="far fa-times-circle"></i><a></span>');
				document.querySelectorAll('input[name="tiempo[]"]').forEach(function(e){
					if(e.checked) $contenedor.append('<span class="badge text-font rounded-pill bg-primary lh-sm me-3 mb-2" style="font-weight:500">Tiempo: '+e.nextElementSibling.innerText+' <a class="p-1 text-light" onclick="elimiBadTiempo(this, '+e.value+')"><i class="far fa-times-circle"></i><a></span>');
				});
				document.querySelectorAll('#search-proyectos-canvas input[name="nivel[]"]').forEach(function(evento){
					if(evento.checked) $contenedor.append("<span class='badge text-font rounded-pill bg-primary lh-sm me-3 mb-2' style='font-weight:500'>Experiencia: "+evento.nextElementSibling.innerText+"</span>");
				});
				if($presupuesto_min.val().length != 0 || $presupuesto_max.val().length != 0) $contenedor.append("<span class='badge text-font rounded-pill bg-primary lh-sm me-3 mb-2' style='font-weight:500'>Presupuesto: "+$presupuesto_min.val()+" - "+$presupuesto_max.val());
				$('#total-strong').text(respuesta.cantidad_proyectos);
			}, error: function(data){
				alertify.notify('Surgio un error, comuníquese con el área de soporte.', 'falla', 10);
			}
		});
	});
	function filtrar_proyectos(){
		$area = $('#area');
		$subarea = $('#habilidad');
		$duracion_proyecto = $('input[name="tiempo[]"]');
		$nivel = $('input[name="nivel[]"]');
		$presupuesto_min = $(".min-value-money");
		$presupuesto_max = $(".max-value-money");
		$contenedor = $('#contenedor_etiquetas_busqueda');
		$estado = $('#estado');
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/proyectos/filtrarResultadosProyectos')?>',
			data: $('#search-proyectos').serialize(),
			success: function(data){
				let respuesta = JSON.parse(data);
				$('#contenido_canvas').offcanvas('hide');
				$('#sec-proyectos').html(respuesta.html);
				$contenedor.empty();
				if($area.val().length != 0) $contenedor.append('<span class="badge text-font rounded-pill bg-primary lh-sm me-3 mb-2" style="font-weight:500">Categoría: '+ $area.val() +' <a class="p-1 text-light" onclick="elimiBadArea(this)"><i class="far fa-times-circle"></i><a></span>');
				if($subarea.val().length != 0) $contenedor.append('<span class="badge text-font rounded-pill bg-primary lh-sm me-3 mb-2" style="font-weight:500">Subcategoría: '+ $subarea.val() +' <a class="p-1 text-light" onclick="elimiBadHabilidad(this)"><i class="far fa-times-circle"></i><a></span>');
				if($estado.val().length != 0) $contenedor.append('<span class="badge text-font rounded-pill bg-primary lh-sm me-3 mb-2" style="font-weight:500">Estado: '+ $estado.val() +' <a class="p-1 text-light" onclick="elimiBadEstado(this)"><i class="far fa-times-circle"></i><a></span>');
				document.querySelectorAll('input[name="tiempo[]"]').forEach(function(e){
					if(e.checked) $contenedor.append('<span class="badge text-font rounded-pill bg-primary lh-sm me-3 mb-2" style="font-weight:500">Tiempo: '+e.nextElementSibling.innerText+' <a class="p-1 text-light" onclick="elimiBadTiempo(this, '+e.value+')"><i class="far fa-times-circle"></i><a></span>');
				});
				document.querySelectorAll('input[name="nivel[]"]').forEach(function(evento){
					if(evento.checked) $contenedor.append("<span class='badge text-font rounded-pill bg-primary lh-sm me-3 mb-2' style='font-weight:500'>Experiencia: "+evento.nextElementSibling.innerText+"</span>");
				});
				if($presupuesto_min.val().length != 0 || $presupuesto_max.val().length != 0) $contenedor.append("<span class='badge text-font rounded-pill bg-primary lh-sm me-3 mb-2' style='font-weight:500'>Presupuesto: "+$presupuesto_min.val()+" - "+$presupuesto_max.val());
				$('#total-strong').text(respuesta.cantidad_proyectos);
			}, error: function(data){
				alertify.notify('Surgió un error, comuníquese con el área de soporte.', 'falla', 10);
			}
		});
	}
	function elimiBadSearch(objeto){
		// alert(tipo);
		$('#search').val('');
		$(objeto).parent().remove();
		filtrar_proyectos();
	}
	function elimiBadArea(objeto){
		$('#area').val(null).trigger('change');
		$('#area-canvas').val(null).trigger('change');
		$(objeto).parent().remove();
		filtrar_proyectos();
	}
	function elimiBadHabilidad(objeto){
		$('#habilidad').val(null).trigger('change');
		$('#habilidad-canvas').val(null).trigger('change');
		$(objeto).parent().remove();
		filtrar_proyectos();
	}
	function elimiBadEstado(objeto){
		$('#estado').val(null).trigger('change');
		$('#estado-canvas').val(null).trigger('change');
		$(objeto).parent().remove();
		filtrar_proyectos();
	}
	function elimiBadTiempo(objeto, valor){
		$('input[value="'+valor+'"]').prop('checked', false);
		$(objeto).parent().remove();
		filtrar_proyectos();
	}
	
	$('.btn-limpiar').click(function(){
		$('#search-proyectos')[0].reset();
		$('#search-proyectos-canvas')[0].reset();
		
		var rangeSlider = $('#slider-range')[0];
		var rangeSlider_canvas = $('#slider-range-canvas')[0];
		
		$('#area').val(null).trigger('change');
		$('#area-canvas').val(null).trigger('change');
		
		$('#habilidad').val(null).trigger('change');
		$('#habilidad-canvas').val(null).trigger('change');
		
		$('#estado').val(null).trigger('change');
		$('#estado-canvas').val(null).trigger('change');
		
		var rangeSlider_canvas = $('#slider-range-canvas')[0];
		rangeSlider_canvas.noUiSlider.on("update", function (values, handle) {
			$(".min-value-money").val('$<?=number_format($minimo, 0, '.', ',')?>');
			$(".max-value-money").val('$<?=number_format($maximo, 0, '.', ',')?>');
			$(".min-value").val('<?=number_format($minimo, 0, '.', ',')?>');
			$(".max-value").val('<?=number_format($maximo, 0, '.', ',')?>');
		});
		var rangeSlider = $('#slider-range')[0];
		rangeSlider_canvas.noUiSlider.on("update", function (values, handle) {
			$(".min-value-money").val('$<?=number_format($minimo, 0, '.', ',')?>');
			$(".max-value-money").val('$<?=number_format($maximo, 0, '.', ',')?>');
			$(".min-value").val('<?=number_format($minimo, 0, '.', ',')?>');
			$(".max-value").val('<?=number_format($maximo, 0, '.', ',')?>');
		});
	});
	$('#area').on('change', function(){
		let area = $(this).val();
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Habilidades/subareas')?>',
			data:{area:area},
			success: function(data){
				$('#habilidad').html(data);
				$('#habilidad-canvas').html(data);
			}, error: function(data){
				console.log('ERROR');
			}
		});
	});
	$('#area-canvas').on('change', function(){
		let area = $(this).val();
		$.ajax({
			type: 'POST',
			url: '<?=base_url('/Habilidades/subareas')?>',
			data:{area:area},
			success: function(data){
				$('#habilidad').html(data);
				$('#habilidad-canvas').html(data);
			}, error: function(data){
				console.log('ERROR');
			}
		});
	});
// 	$('#search-proyectos').clone().prependTo('#contenido_filtro_canvas');
	
// 	$('.buttons-filter, #contenido_filtro_canvas .buttons-filter').delegate('#filtrar, #limpiar', 'click', function(e){
// 	    // Se limpia filtro
//         if(e.target.id == 'limpiar') return limpiar_filtro_proyectos();
//         else return filtrar_proyectos();
// 	});
	
	$('button[data-bs-target="#postular_a_proyecto"]').click(function(){
		$('#titulo_proyecto_modal').text($(this).attr('dato_titulo_proyecto'));
		$('#enviar_postulacion').attr('href', '<?=base_url('/postularme').'/'?>'+$(this).attr('dato_proyecto'));
	});
</script>
<?= $this->endSection() ?>