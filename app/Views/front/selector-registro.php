<!doctype html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
		<!--Google Autenticacion-->
		<meta name="google-signin-scope" content="profile email">
		<meta name="google-signin-client_id" content="198033966029-a75kfv97som76od1s41e6gr26po9ppvh.apps.googleusercontent.com">
		<!--jQuery-->
		<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
		<!--Script para mostrar los iconos-->
		<script defer src="https://kit.fontawesome.com/d47f1b7d62.js" crossorigin="anonymous"></script>
		<!-- Fonts -->
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
		<!-- Bootstrap Scripts-->
		<script defer src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
		<script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
		<!--Hoja de estilos-->
		<link rel="stylesheet" type="text/css" href="<?=base_url('/assets/css/style.css')?>">
		<!--Alertify-->
		<link href="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css" rel="stylesheet"/>
		<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>
		<script defer src="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>
		<!--Estilos del tema-->
		<link rel="stylesheet" href="<?=base_url('/assets/themes/css/plugins/animate.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('/assets/themes/css/main.css')?>?v=1.0">
		<script src="https://cdn.lordicon.com/xdjxvujz.js"></script>
		<title>Registrame como&nbsp;-&nbsp;MEXTEMPS</title>
	</head>
	<body>
		<div id="preloader-active">
			<div class="preloader d-flex align-items-center justify-content-center">
				<div class="preloader-inner position-relative">
					<div class="text-center">
						<img src="<?=base_url('assets/themes/imgs/theme/loading.gif')?>" alt="jobhub" />
					</div>
				</div>
			</div>
		</div>
		<main class="main-no selector-mt">
			<section class="section-box ">
				<div class="container pt-20 login-container">
					<div class="logo" style="text-align: center;">
						<a href="<?=base_url()?>"><img alt="Mextemps" src="<?=base_url('assets/themes/imgs/theme/mextemps.svg')?>" width="245"></a>
						<div style="margin: 30px 0 20px 0; text-align: center;">
							<h4>¿Cómo podemos ayudarte hoy?</h4>
						</div>
					</div>
					<div class="row selector-content">
						<div class="col-xl-12 col-md-12 col-sm-12 mx-auto card-grid-4 mt-20 mb-20" style="padding: 0px; border: 0px;">
							<div class="row">
								<div class=" text-center col-md-6 col-sm-12">
									<div class="p-2">
										<div class="imagen-selector wow animate__ animate__fadeInUp hover-up animated">
											<img src="<?=base_url('assets/themes/imgs/theme/freelancer-cuenta.png')?>" width="280" height="280">
										</div>
										<a class="btn btn-default my-2" href="<?=base_url('/registrarme/freelancer')?>" style="background-color: #003c71; color: #fff; border-radius: 6px; padding: 7px 15px;">
											Freelancer
										</a>
										<p class="text-font text-resgistros mt-10">Encuentra una gran cantidad de proyectos, sé parte de nuestro grupo de élite de freelancers.</p>
									</div>
								</div>
								<div class="col-md-6 col-sm-12 text-center" style="border-radius: 0 10px 10px 0;">
									<div class=" p-2">
										<div class="imagen-selector wow animate__ animate__fadeInUp hover-up animated">
											<img src="<?=base_url('assets/themes/imgs/theme/contratista-cuenta.png')?>" width="280" height="280">
										</div>
										<a class="btn btn-default my-2" href="<?=base_url('/registrarme/contratante')?>" style="background-color: #003c71; color: #fff; border-radius: 6px; padding: 7px 15px;">
											Contratante
										</a>
										<p class="text-font text-resgistros mt-10">Encuentra dentro de nuestra plataforma un ejército de profesionales, al ser un contratante.</p>
									</div>
								</div>
							</div>
						</div>
						<p class="text-center mb-50 mt-20"> ¿Ya tienes una cuenta? <a href="<?=base_url('ingresar')?>">Ingresa aquí</a></p>
					</div>
				</div>
			</section>
		</main>
		<!--Scripts de creados para funciones-->
		<?php if(session('alerta')) :?>
			<script defer>
				$(document).ready(function(){
					alertify.notify('<?= session('alerta.mensaje')?>', '<?= session('alerta.tipo')?>', 10);
				});
			</script>
		<?php endif; ?>
		<script defer src="<?=base_url('/assets/js/front.js')?>"></script>
		<!-- Scripts del tema -->
		<!-- Vendor JS-->
		<script src="<?=base_url('assets/themes/js/plugins/waypoints.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/wow.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/magnific-popup.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/perfect-scrollbar.min.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/select2.min.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/isotope.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/scrollup.js')?>"></script>
		<script src="<?=base_url('assets/themes/js/plugins/swiper-bundle.min.js')?>"></script>
		<!-- Template  JS -->
		<script src="<?=base_url('assets/themes/js/main.js?v=1.0')?>"></script>
	</body>
</html>