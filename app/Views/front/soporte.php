<?= $this->extend('front/main') ?>
<?= $this->section('title') ?>
	Soporte
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<main class="main soporte-page">
	<section class="section-box">
		<div class="container pt-50">
			<div class="w-100 w-md-100 mx-auto text-center">
				<h3 class="section-title-large mb-30 wow animate__animated animate__fadeInUp">Soporte</h3>
				<h4>Temas populares</h4>
				<p class="descripcion mb-30 text-muted wow animate__animated animate__fadeInUp font-md">Te presentamos algunas de las preguntas más frecuentes en nuestro foro</p>
			</div>
			<section class="row justify-content-center text-center py-5 py-lg-0 info-de-soporte">
				<div class="col-12 col-lg-6 mb-3 p-3 pe-lg-5">
					<div class="icono-titulo">
						<!-- <img src="assets/imgs/theme/pagos.png" width="80"> -->
						<lord-icon
							src="https://cdn.lordicon.com/<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'icono-soporte', 'tipo' => '1', 'valor' => 'icono'])?>.json"
							trigger="loop"
							delay="3000"
							colors="primary:#121331,secondary:#FE5000"
							style="width:120px;height:120px">
						</lord-icon>
						<h4 class="card-title h5 text-center fw-bold mb-3">Facturación y pagos</h4>
					</div>
					<div class="card-shadow-sm detalle-info-tabs">
						<div class="card-body d-flex flex-row">
							<!-- TABS 1 -->
							<div class="contenedors" style="width: 100%;">
								<div class="accordion accordion-flush" id="accordionFlushExample">
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingOne">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '1', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body">
												<p class="mb-15"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '1', 'valor' => 'respuesta'])?></p>
											</div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingTwo">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '2', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '2', 'valor' => 'respuesta'])?></div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingThree">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '3', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '3', 'valor' => 'respuesta'])?></div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingFour">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '4', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '4', 'valor' => 'respuesta'])?></div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingFive">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '5', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-1', 'tipo' => '5', 'valor' => 'respuesta'])?></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-3 p-3 pe-lg-5">
					<div class="icono-titulo">
						<!-- <img src="assets/imgs/theme/licencias.png" width="80"> -->
						<lord-icon
							src="https://cdn.lordicon.com/<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'icono-soporte', 'tipo' => '2', 'valor' => 'icono'])?>.json"
							trigger="loop"
							delay="3000"
							colors="primary:#121331,secondary:#FE5000"
							style="width:120px;height:120px">
						</lord-icon>
						<h4 class="card-title h5 text-center fw-bold mb-3">Cómo elegir las licencias adecuadas</h4>
					</div>
					<div class="card-shadow-sm detalle-info-tabs">
						<div class="card-body d-flex flex-row">
							<!-- TABS 2 -->
							<div class="contenedors" style="width: 100%;">
								<div class="accordion accordion-flush" id="accordionFlushExample">
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingOne2">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne2" aria-expanded="false" aria-controls="flush-collapseOne2">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-2', 'tipo' => '1', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseOne2" class="accordion-collapse collapse" aria-labelledby="flush-headingOne2" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body">
												<p class="mb-15">
													<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-2', 'tipo' => '1', 'valor' => 'respuesta'])?>
												</p>
												<a class="btn btn-default" style="color: #ffffff; font-size: 14px; padding: 6px 7px; background-color: #003c71; border-radius: 10px;" href="<?= base_url('tarifas-contratista')?>">Plan contratante</a>
											</div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingTwo2">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo2" aria-expanded="false" aria-controls="flush-collapseTwo2">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-2', 'tipo' => '2', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseTwo2" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo2" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-2', 'tipo' => '2', 'valor' => 'respuesta'])?><br>
											<a class="btn btn-default size2btn" href="<?= base_url('tarifas-freelance')?>">Planes freelancer</a> | <a class="btn btn-primary size2btn" href="<?= base_url('tarifas-contratista')?>">Planes contratante</a> </div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingThree2">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree2" aria-expanded="false" aria-controls="flush-collapseThree2">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-2', 'tipo' => '3', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseThree2" class="accordion-collapse collapse" aria-labelledby="flush-headingThree2" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-2', 'tipo' => '3', 'valor' => 'respuesta'])?> <br>
												<a class="btn btn-default size2btn" href="<?= base_url('tarifas-freelance')?>">Planes freelancer</a> | <a class="btn btn-primary size2btn" href="<?= base_url('tarifas-contratista')?>">Planes contratante</a> </div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingFour2">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour2" aria-expanded="false" aria-controls="flush-collapseFour2">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-2', 'tipo' => '4', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseFour2" class="accordion-collapse collapse" aria-labelledby="flush-headingFour2" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-2', 'tipo' => '4', 'valor' => 'respuesta'])?></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-3 p-3 pe-lg-5">
					<div class="icono-titulo">
						<!-- <img src="assets/imgs/theme/cuenta.png" width="80"> -->
						<lord-icon
							src="https://cdn.lordicon.com/<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'icono-soporte', 'tipo' => '3', 'valor' => 'icono'])?>.json"
							trigger="loop"
							delay="3000"
							colors="primary:#121331,secondary:#FE5000"
							style="width:120px;height:120px">
						</lord-icon>
						<h4 class="card-title h5 text-center fw-bold mb-3">Cuenta</h4>
					</div>
					<div class="card-shadow-sm detalle-info-tabs">
						<div class="card-body d-flex flex-row">
							<!-- TABS 3 -->
							<div class="contenedors" style="width: 100%;">
								<div class="accordion accordion-flush" id="accordionFlushExample">
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingOne3">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne3" aria-expanded="false" aria-controls="flush-collapseOne3">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '1', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseOne3" class="accordion-collapse collapse" aria-labelledby="flush-headingOne3" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body">
												<p class="mb-15"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '1', 'valor' => 'respuesta'])?></p>
											</div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingTwo3">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo3" aria-expanded="false" aria-controls="flush-collapseTwo3">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '2', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseTwo3" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo3" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '2', 'valor' => 'respuesta'])?></div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingThree3">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree3" aria-expanded="false" aria-controls="flush-collapseThree3">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '3', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseThree3" class="accordion-collapse collapse" aria-labelledby="flush-headingThree3" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '3', 'valor' => 'respuesta'])?></div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingThree31">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree31" aria-expanded="false" aria-controls="flush-collapseThree31">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '4', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseThree31" class="accordion-collapse collapse" aria-labelledby="flush-headingThree31" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '4', 'valor' => 'respuesta'])?></div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingFour3">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour3" aria-expanded="false" aria-controls="flush-collapseFour3">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '5', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseFour3" class="accordion-collapse collapse" aria-labelledby="flush-headingFour3" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-3', 'tipo' => '5', 'valor' => 'respuesta'])?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6 mb-3 p-3 pe-lg-5">
					<div class="icono-titulo">
						<!-- <img src="assets/imgs/theme/asistencia.png" width="80"> -->
						<lord-icon
							src="https://cdn.lordicon.com/<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'icono-soporte', 'tipo' => '4', 'valor' => 'icono'])?>.json"
							trigger="loop"
							delay="3000"
							colors="primary:#121331,secondary:#FE5000"
							style="width:120px;height:120px">
						</lord-icon>
						<h2 class="card-title h5 text-center fw-bold mb-3">Asistencia técnica</h2>
					</div>
					<div class="card-shadow-sm detalle-info-tabs">
						<div class="card-body d-flex flex-row">
							<!-- TABS 4 -->
							<div class="contenedors" style="width: 100%;">
								<div class="accordion accordion-flush" id="accordionFlushExample">
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingOne4">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne4" aria-expanded="false" aria-controls="flush-collapseOne4">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-4', 'tipo' => '1', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseOne4" class="accordion-collapse collapse" aria-labelledby="flush-headingOne4" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-4', 'tipo' => '1', 'valor' => 'respuesta'])?></div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingTwo4">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo4" aria-expanded="false" aria-controls="flush-collapseTwo4">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-4', 'tipo' => '2', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseTwo4" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo4" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-4', 'tipo' => '2', 'valor' => 'respuesta'])?></div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingThree4">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree4" aria-expanded="false" aria-controls="flush-collapseThree4">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-4', 'tipo' => '3', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseThree4" class="accordion-collapse collapse" aria-labelledby="flush-headingThree4" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-4', 'tipo' => '3', 'valor' => 'respuesta'])?></div>
										</div>
									</div>
									<div class="accordion-item">
										<h2 class="accordion-header" id="flush-headingFour4">
											<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour4" aria-expanded="false" aria-controls="flush-collapseFour4">
												<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-4', 'tipo' => '4', 'valor' => 'pregunta'])?>
											</button>
										</h2>
										<div id="flush-collapseFour4" class="accordion-collapse collapse" aria-labelledby="flush-headingFour4" data-bs-parent="#accordionFlushExample">
											<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-soporte-4', 'tipo' => '4', 'valor' => 'respuesta'])?></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="row justify-content-center no-encontro-buscar">
				<div class="col-sm-12 col-lg-10 mb-50">
					<h2 class="card-title h5 text-start fw-bold mb-3 titulo-noencontro">¿No encuentró lo que <span class="naranja">buscaba?</span></h2>
					<div class="card p-2">
						<div class="d-flex align-items-center p-3 seleccionar-opciones">
							<div class="flex-shrink-0"><i class="far fa-comment h5 mx-3" aria-hidden="true"></i></div>
							<div class="flex-grow-1 ms-3">
								<label class="d-flex text-font fw-bold num-tarjeta">Chatee con nosotros</label>
								<label class="d-flex text-font text-secundario">Estado: No Disponible</label>
							</div>
							<div class="flex-shrink-0 ms-3">
								<a class="btn btn-secondary disabled" target="_blank" href="https://web.whatsapp.com/send?phone=+528122588288&amp;text=Hola%20tengo%20una%20duda...">Seleccionar</a>
							</div>
						</div>
						<div class="d-flex align-items-center p-3 seleccionar-opciones">
							<div class="flex-shrink-0"><i class="far fa-envelope h5 mx-3" aria-hidden="true"></i></div>
							<div class="flex-grow-1 ms-3">
								<label class="d-flex text-font fw-bold num-tarjeta">Envíenos un correo electrónico</label>
								<label class="d-flex text-font text-secundario">Nos comunicaremos con usted lo más pronto posible</label>
							</div>
							<div class="flex-shrink-0 ms-3">
								<a class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#formularioCoreo_modal">Seleccionar</a>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</section>
</main>
<div class="modal fade" id="formularioCoreo_modal" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="titulo" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header justify-content-center border-bottom-0">
				<h5 class="modal-title fw-bold" id="titulo">Llena el siguiente formulario</h5>
				<a class="btn cerrar-modal px-3 py-2" data-bs-toggle="modal" data-bs-target="#formularioCoreo_modal" aria-label="Close"><i class="fas fa-times" aria-hidden="true"></i></a>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row align-items-center text-center">
						<form class="needs-validation" id="form-soporte" enctype="'multipart/form-data" novalidate>
							<?php if (!session('logeado')){ ?>
								<div class="position-relative">
									<input class="d-block text-font form-control inputs-form my-3" name="nombre" id="nombre" placeholder="Nombre:" required>
									<div class="invalid-tooltip text-font2">Requerido</div>
								</div>
								<div class="position-relative">
									<input class="d-block text-font form-control inputs-form my-3" name="telefono" id="telefono" placeholder="Teléfono:">
									<div class="invalid-tooltip text-font2">Requerido</div>
								</div>
								<div class="position-relative">
									<input class="d-block text-font form-control inputs-form my-3 telefono" type="email" name="correo" id="correo" placeholder="Correo electrónico:" required>
									<div class="invalid-tooltip text-font2">Ingrese un correo válido</div>
								</div>
							<?php }else{ ?>
								<?php $usuario = model('Autenticacion')->where('id', session('id'))->first();?>
								<div class="position-relative">
									<input class="d-block text-font form-control inputs-form my-3" value="<?=$usuario['nombre'] . ' ' . $usuario['apellidos']?>" name="nombre" id="nombre" placeholder="Nombre:" required readonly>
									<div class="invalid-tooltip text-font2">Requerido</div>
								</div>
								<div class="position-relative">
									<input class="d-block text-font form-control inputs-form my-3" name="telefono" id="telefono" placeholder="Teléfono:">
									<div class="invalid-tooltip text-font2">Requerido</div>
								</div>
								<div class="position-relative">
									<input class="d-block text-font form-control inputs-form my-3 telefono" value="<?=$usuario['correo']?>" type="email" name="correo" id="correo" readonly placeholder="Correo electrónico:" required>
									<div class="invalid-tooltip text-font2">Ingrese un correo válido</div>
								</div>
							<?php } ?>
							<div class="position-relative">
								<input class="d-block text-font form-control inputs-form" placeholder="Asunto" name="asunto" id="asunto" required>
								<div class="invalid-tooltip text-font2">Requerido</div>
							</div>
							<div class="position-relative">
								<textarea class="d-block text-font form-control inputs-form my-3" placeholder="Cuentanos. Al describir los hechos más importantes, podemos resolver rápidamente tu solicitud." name="descripcion" id="descripcion" required></textarea>
								<div class="invalid-tooltip text-font2">Requerido</div>
							</div>
							<div class="position-relative">
								<small class="d-block fst-italic text-font2 text-start">Captura del problema</small>
								<input class="text-font form-control adjuntar-file mb-3" type="file" name="archivo_soporte" id="archivo_soporte" multiple accept="image/*">
								<output class="d-none d-flex overflow-auto border border-secondary rounded-3 p-2" id="resultado_imagenes"></output>
								<button type="button" class="btn btn-sm btn-outline-naranja position-absolute top-100 end-0 mt-2" id="limpiar_archivoSoporte">Descartar</button>
							</div>
							<div class="row justify-content-center">
								<div class="col-12 text-center my-3">
									<button class="btn btn-naranja" type="submit" id="enviar">Enviar mensaje</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	window.onload = function() {
		if (!window.File && !window.FileList && !window.FileReader) console.log("Tu navegador no soporta File API");
		$('#archivo_soporte').on("change", function(event) {
			let files = event.target.files;
			let output = document.getElementById("resultado_imagenes");
			for (let i = 0; i < files.length; i++) {
				let file = files[i];
				if (!file.type.match('image.*')) {
					$(this).val("");
					return alertify.notify("OPPS... Lo siento solo puedo permitir que subas imagenes.", 'falla', 10);
				}
				if (this.files[0].size > 4194304) {
					$(this).val("");
					return alertify.notify("OPPS... La imagen es muy pesada, solo puedes enviar como máximo 4MB.", 'advertencia', 10);
				}
				let picReader = new FileReader();
				picReader.addEventListener("load", function(event) {
					let picFile = event.target;
					let div = document.createElement("div");
					div.innerHTML = "<img class='imagen_prevista' src='" + picFile.result + "'" + "title='Prevista imagen'/>";
					output.insertBefore(div, null);
				});
				$('#limpiar_archivoSoporte, #resultado_imagenes').show();
				$('#resultado_imagenes').removeClass('d-none');
				picReader.readAsDataURL(file);
			}
		});
	}
	$('#archivo_soporte').on("click", function() {
		$('.imagen_prevista').parent().remove();
		$('#resultado_imagenes').hide().addClass('d-none');
		$('#limpiar_archivoSoporte').hide();
		$(this).val("");
	});
	$('#limpiar_archivoSoporte').on("click", function() {
		$('.imagen_prevista').parent().remove();
		$('#resultado_imagenes').hide().addClass('d-none');
		$('#archivo_soporte').val("");
		$(this).hide();
	});
	$('#enviar').click(function(event) {
		let formulario = event.target.form;
		if(<?= (session('logeado'))? 1 : 0 ?>){
			if (formulario.asunto.value.length == 0 || formulario.descripcion.value.length == 0) return true;
		}
		else{
			if(formulario.nombre.value.length == 0 || formulario.correo.value.length == 0 || 
			formulario.asunto.value.length == 0 || formulario.descripcion.value.length == 0) return true;
		}
		event.preventDefault();
		$(this).attr('disabled', true);
		$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);

		let form = new FormData($('#form-soporte')[0]);
		let imagenes = $('.imagen_prevista');
		let datos_imagenes = [];
		if(imagenes.length != 0){
			for (let i = 0; i < imagenes.length; i++) datos_imagenes[i] = imagenes[i].attributes.src.value;
		}
		$.ajax({
			url: '<?= base_url('Home/enviarSoporte_datos')?>',
			type: 'POST',
			contentType: false,
			processData: false,
			data: form,
			success: function(id) {
				$.ajax({
					url: '<?= base_url('Home/enviarSoporte_archivos')?>',
					type: 'POST',
					data: {id: id, imagenes: datos_imagenes},
					success: function(data) {
						let respuesta = JSON.parse(data);
						$('#limpiar_archivoSoporte').click();
						$('#form-soporte').removeClass('was-validated');
						$('#form-soporte')[0].reset();
						$('#enviar').removeAttr('disabled').children('span').remove();
						alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					},
					error: function(data) {
						alertify.notify(data.responseText, 'falla', 10);
					}
				});
			},
			error: function(id) {
				alertify.notify(id.responseText, 'falla', 10);
			}
		});
	});
</script>
<?= $this->endSection() ?>