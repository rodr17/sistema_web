<?= $this->extend('front/main') ?>
<?= $this->section('title') ?>
Términos y Condiciones
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="paginas-info">
	<div class="banner-seccion" data-banner="">
		<div class="container">
			<div class="titulo-banner">
				<h1>Términos y condiciones</h1>
			</div>
		</div>
	</div>
	<div class="container pb-5 pt-3">
		<div class="row">
			<div class="col-12">
				<?php if (!isset($terminos['info'])) : ?>
					<h2 class="text-center">Proximamente...</h2>
				<?php else :?>
					<?= $terminos['info'] ?>
				<?php endif ;?>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection() ?>