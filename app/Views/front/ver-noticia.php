<?=$this->extend('front/main')?>
<?=$this->section('title')?>
    <?=$noticia['titulo']?>
<?=$this->endSection()?>

<?=$this->section('content')?>
<main class="main blog-main-detalle">
    <div class="archive-header pt-50 pb-50 text-center">
        <div class="container">
            <h3 class="text-center mx-auto">
                <?=$noticia['titulo']?>
            </h3>
            <div class="post-meta text-muted d-flex align-items-center mx-auto justify-content-center">
                <div class="author d-flex align-items-center mr-30 d-none">
                    <i class="fas fa-user-circle" aria-hidden="true"></i> <span style="padding-left: 6px;"> Carlos Guerrero </span>
                </div>
                <div class="date mr-30">
                    <span><i class="fi-rr-edit mr-5 text-grey-6"></i><?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $noticia['created_at']])?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="post-loop-grid blog-detalle-info">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 mx-auto">
                    <div class="single-body">
                        <figure class="mb-30 text-center">
                            <img src="<?=$noticia['imagen']?>" alt="">
                        </figure>
                        <div class="single-content">
                            <?=$noticia['contenido']?>
                        </div>
                        <div class="author-bio p-30 mt-50 border-radius-15 bg-white" style="display: none;">
                            <div class="author-image mb-15">
                                <!-- <a href="author.html"><img src="assets/imgs/avatar/ava_14.png" alt="" class="avatar"></a> -->
                                <div class="author-infor">
                                    <h5 class="mb-5">Carlos Guerrero</h5>
                                    <p class="mb-0 text-muted font-xs">
                                        <span class="mr-10">56 publicaciónes</span>
                                        <span class="has-dot">Desde 2019</span>
                                    </p>
                                </div>
                            </div>
                            <div class="author-des">
                                <p>Hi, I'm a recruiter with over 25 years of experience. I have worked in many multinational companies and corporations. With my experiences, I hope my articles will bring you knowledge and inspiration.</p>
                            </div>
                        </div>
                        <hr class="mt-50">
                        <div class="related-posts mt-50 botones_carrusel">
                            <h4 class="mb-30">Noticias relacionadas</h4>
                            <?=view_cell('App\Libraries\FuncionesSistema::masNoticias', ['id' => $noticia['id']])?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?=$this->endSection()?>