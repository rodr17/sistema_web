<?php if(!isset($cupon)) :?>
	<div class="d-flex">
		<input class="border border-secondary text-font" onkeyup="mayus(this)" name="cupon" type="text" placeholder="Ingrese su cupón">
		<a class="btn btn-default mx-1 align-self-center validar-cupon-checkout">Validar</a>
	</div>
<?php else :?>
	<div class="d-flex justify-content-between align-items-center contenedor_checks rounded-3 border-azul p-2 ">
		<label class="mb-0"><i class="fas fa-tag fa-rotate-180 text-azul me-2"></i> Cupón: <?=$cupon?></label>
		<input class="hidden cupon_ingresado" type="hidde" value="<?=$cupon?>">
		<a class="cambiar_cupon opciones-ingresar">Cambiar</a>
	</div>
<?php endif;?>