<div class="col-12 col-lg-3">
    <div class="flip-box" style="padding: 10px;">
        <div class="flip-box-inner shadow rounded-circle">
            <div class="flip-box-front">
                <img class="w-100 h-100 rounded-circle" src="<?=imagenPerfil($freelance)?>" class="card-img-top" alt="...">
            </div>
            <div class="flip-box-back card border-0 bg-light overflow-hidden justify-content-center" style="border-radius: 100%;">
                <div style="margin: 0 auto;">
                    <p class="card-text text-naranja fw-bold" style="margin: 1px; color: #ea794f;"><i class="fas fa-check-circle"></i> <?=view_cell('App\Libraries\FuncionesSistema::obtenerHabilidad', ['id' => $freelance['id']])?></p>
                    <span class="card-title fw-bold m-0 text-truncate"><?= word_limiter(isset($nombre)? $nombre : '', 2, '')?></span>
                    <p class="card-text text-secundario small m-0" style="font-size: 15px;"><img src="<?= base_url('assets/images/billete.webp')?>" alt=""></i> $<?=$freelance['pagoHora']?> hora</p>
                    <ul class="p-0 m-0">
                        <li class="nav-link p-0">
                            <i class="fas fa-star text-font rounded-circle icon-top-perfil p-1" style="background-color: #ea794f;"></i> <p class="d-inline text-font text-secundario small" style="font-size: 15px;">
                                <?=view_cell('App\Libraries\FuncionesSistema::cantidadCalificaciones', ['id' => $freelance['id']])?>
                            </p>
                        </li>
                        <?php if(!empty($freelance['direccion'])){ ?>
                            <li class="nav-link p-0 text-truncate">
                                <i class="fas fa-map-marker-alt text-font rounded-circle icon-top-perfil p-1" style="background-color: #ea794f;"></i>
                                <label class="d-inline text-font text-secundario small text-truncate m-0" style="font-size: 15px;"><?=$freelance['direccion']['estado']?></label>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="card-footer bg-transparent border-0 text-center">
                    <a class="btn btn-hover" href="<?= base_url('Usuarios/prevista').'/'.base64_encode($freelance['id'])?>" style="background-color: #ffffff; border-color: #ea794f; color: #ea794f; padding: 5px 7px; font-size: 12px; border-radius: 12px;">VER MÁS</a>
                </div>
            </div>
        </div>
    </div>
</div>