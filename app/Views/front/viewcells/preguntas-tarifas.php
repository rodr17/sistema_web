<div class="row">
	<div class="col-lg-6">
		<div class="accordion accordion-flush" id="accordionFlushExample">
			<div class="accordion-item">
				<h2 class="accordion-header" id="flush-headingOne">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
						<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => 1, 'valor' => 'pregunta'])?>
					</button>
				</h2>
				<div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
					<div class="accordion-body">
						<p class="mb-15">
                            <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => 1, 'valor' => 'respuesta'])?>
						</p>
					</div>
				</div>
			</div>
			<div class="accordion-item">
				<h2 class="accordion-header" id="flush-headingTwo">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
						<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => 2, 'valor' => 'pregunta'])?>
					</button>
				</h2>
				<div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
					<div class="accordion-body">
                        <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => 2, 'valor' => 'respuesta'])?>
					</div>
				</div>
			</div>
			<div class="accordion-item">
				<h2 class="accordion-header" id="flush-headingThree">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
						<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => 3, 'valor' => 'pregunta'])?>
					</button>
				</h2>
				<div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
					<div class="accordion-body">
                        <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => 3, 'valor' => 'respuesta'])?>
					</div>
				</div>
			</div>
			<div class="accordion-item">
				<h2 class="accordion-header" id="flush-headingFour">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
						<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => 4, 'valor' => 'pregunta'])?>
					</button>
				</h2>
				<div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
					<div class="accordion-body">
                        <?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => 4, 'valor' => 'respuesta'])?>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="accordion accordion-flush" id="accordionFlushExample2">
			<div class="accordion-item">
				<h2 class="accordion-header" id="flush-headingOne2">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne2" aria-expanded="false" aria-controls="flush-collapseOne2">
						<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => 5, 'valor' => 'pregunta'])?>
					</button>
				</h2>
				<div id="flush-collapseOne2" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample2">
					<div class="accordion-body">
						<p class="mb-15"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => 5, 'valor' => 'respuesta'])?></p>
					</div>
				</div>
			</div>
			<div class="accordion-item">
				<h2 class="accordion-header" id="flush-headingTwo2">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo2" aria-expanded="false" aria-controls="flush-collapseTwo2">
						<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => 6, 'valor' => 'pregunta'])?>
					</button>
				</h2>
				<div id="flush-collapseTwo2" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo2" data-bs-parent="#accordionFlushExample2">
					<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => 6, 'valor' => 'respuesta'])?></div>
				</div>
			</div>
			<div class="accordion-item">
				<h2 class="accordion-header" id="flush-headingThree2">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree2" aria-expanded="false" aria-controls="flush-collapseThree2">
						<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => 7, 'valor' => 'pregunta'])?>
					</button>
				</h2>
				<div id="flush-collapseThree2" class="accordion-collapse collapse" aria-labelledby="flush-headingThree2" data-bs-parent="#accordionFlushExample2">
					<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => 7, 'valor' => 'respuesta'])?></div>
				</div>
			</div>
			<div class="accordion-item">
				<h2 class="accordion-header" id="flush-headingFour2">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour2" aria-expanded="false" aria-controls="flush-collapseFour2">
						<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => 8, 'valor' => 'pregunta'])?>
					</button>
				</h2>
				<div id="flush-collapseFour2" class="accordion-collapse collapse" aria-labelledby="flush-headingFour2" data-bs-parent="#accordionFlushExample2">
					<div class="accordion-body"><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'pregunta-tarifas', 'tipo' => 8, 'valor' => 'respuesta'])?></div>
				</div>
			</div>
		</div>
	</div>
</div>