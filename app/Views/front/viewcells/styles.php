<style>
    <?php
        $slides = model('ModelSecciones')->where('tipo', 'slide')->find();
    ?>
    /*Estilos para sliders en diferentes medidas de pantallas*/
    /* Dispositivos extra pequeños (teléfonos, de 600px en adelante) */
    @media only screen and (max-width: 600px) {
        <?php foreach($slides as $key => $s){ ?>
            <?php $datos = json_decode($s['info'], true);?>
            #slider .slide-<?=$key?>-home{
                background: url(<?=$datos['movil']?>);
            }
        <?php } ?>
        #sec-empecemos{background-image: url(<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'empezemos', 'valor' => 'fondo-movil'])?>);}
    }
    /* Dispositivos pequeños (tabletas en formato vertical y teléfonos grandes, a partir de 600px) */
    @media only screen and (min-width: 600px) {
        <?php foreach($slides as $key => $s){ ?>
            <?php $datos = json_decode($s['info'], true);?>
            #slider .slide-<?=$key?>-home{
                background: url(<?=$datos['tableta']?>);
            }
        <?php } ?>
        #sec-empecemos{background-image: url(<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'empezemos', 'valor' => 'fondo-tableta'])?>);}
    }
    /* Dispositivos medianos (tabletas apaisadas, a partir de 768px) */
    @media only screen and (min-width: 768px) {
        <?php foreach($slides as $key => $s){ ?>
            <?php $datos = json_decode($s['info'], true);?>
            #slider .slide-<?=$key?>-home{
                background: url(<?=$datos['tableta']?>);
            }
        <?php } ?>
        #sec-empecemos{background-image: url(<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'empezemos', 'valor' => 'fondo-tableta'])?>);}
    }
    /* Dispositivos extra grandes (portátiles y ordenadores de sobremesa grandes, a partir de 1200px) */
    @media only screen and (min-width: 1200px) {
        <?php foreach($slides as $key => $s){ ?>
            <?php $datos = json_decode($s['info'], true);?>
            #slider .slide-<?=$key?>-home{
                background: url(<?=$datos['slide']?>);
            }
        <?php } ?>
        #sec-empecemos{background-image: url(<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'empezemos', 'valor' => 'fondo'])?>);}
    }
</style>