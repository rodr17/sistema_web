<div class="row mt-70 slider-comentarios">
    <div class="box-swiper">
        <div class="swiper-container swiper-group-3">
            <div class="swiper-wrapper pb-70 pt-5">
                
                <?php for($i = 1; $i <= 5; $i++){ ?>
                    <div class="swiper-slide">
                        <div class="card-grid-3 hover-up">
                            <div class="text-center card-grid-3-image card-grid-3-image-circle">
                                <a href="#">
                                    <figure><img alt="Mextemps" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'testimonio', 'tipo' => $i, 'valor' => 'imagen'])?>" /></figure>
                                </a>
                            </div>
                            <div class="card-block-info mt-10">
                                <p class="text-lg text-center textos-coments">
                                    <?= str_replace(['<p>', '</p>'], ['', ''], view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'testimonio', 'tipo' => $i, 'valor' => 'contenido'])) ?>
                                </p>
                                <div class="text-center mt-20 mb-25">
                                    <span><img alt="Mextemps" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                    <span><img alt="Mextemps" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                    <span><img alt="Mextemps" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                    <span><img alt="Mextemps" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                    <span><img alt="Mextemps" src="<?=base_url('assets/themes/imgs/theme/icons/star.svg')?>" /></span>
                                </div>
                                <div class="card-profile text-center">
                                    <strong><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'testimonio', 'tipo' => $i, 'valor' => 'nombre'])?></strong>
                                    <span><?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'testimonio', 'tipo' => $i, 'valor' => 'profesion'])?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="swiper-pagination swiper-pagination3"></div>
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</div>