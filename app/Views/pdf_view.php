<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Resumen de compra</title>
        <link href="<?=base_url('/assets/css/pagos.css')?>" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="whitepaper">
            <div class="Header">
                <div class="Logo_empresa">
                    <img src="<?= base_url('/assets/images/mextemps.png') ?>" alt="Logo Paynet">
                </div>
                <div class="Logo_paynet">
                </div>
            </div>
            <div class="Data" style="display: inline-flex;">
                <div class="Big_Bullet">
                    <span></span>
                </div>
                <div class="col1" style="align-self: center;">
                    <h3>Fecha de compra</h3>
                    <h4><?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $pago['fecha']])?></h4>
                    <div style="align: center;"><span>ID de compra: <?=$pago['id_compra']?></span></div>
                </div>
                <div class="col2" style="background-color: #003b71;">
                    <h2>Total a pagar</h2>
                    <h1>$<?= number_format($pago['monto'], 2, '.', ',') ?><small> MXN</small></h1>
                </div>
            </div>
            <div class="DT-margin"></div>
            <div class="Data">
                <div class="Big_Bullet">
                    <span></span>
                </div>
                <div class="col1">
                    <h3>Detalles de la compra</h3>
                </div>
            </div>
            <div class="Table-Data">
                <div class="table-row color1">
                    <div>Plan</div>
                    <span>Plan <?=$plan['name']?> <?=($tipo == 'anual')? 'Anual' : '' ?></span>
                </div>
                <div class="table-row color2">
                    <div>Fecha de compra</div>
                    <span><?=view_cell('App\Libraries\FuncionesAdmin::humanizarFechaHora', ['fecha' => $pago['fecha']])?></span>
                </div>
                <div class="table-row color1">
                    <div>Pagado con</div>
                    <span>Tarjeta con terminacion <?= substr($tarjeta['card_number'], -4) ?></span>
                </div>
                <div class="table-row color1">
                    <div>Nombre del cliente</div>
                    <span><?=$usuario['nombre']?> <?=$usuario['apellidos']?></span>
                </div>
                <div class="table-row color2">
                    <div>Correo del cliente</div>
                    <span><?=$usuario['correo']?></span>
                </div>
                <div class="table-row color2" style="display:none">
                    <div>&nbsp;</div>
                    <span>&nbsp;</span>
                </div>
            </div>
            <div class="DT-margin"></div>
        </div>
        <div style="height: 40px; width: 100%; float left;"></div>
    </body>
</html>
