<?php if($buscador == 'talentos') :?>
    <div class="col-md-12 col-lg-9 col-xl-7">
        <form class="flex"method="GET" action="<?=base_url('usuarios/talento')?>">
            <?php if($buscar_freelancers) :?>
                <input class="buscar text-font rounded-start border-0 border-end w-50 h-100 bg-white" type="search" name="search" placeholder="¿Qué talentos buscas?" style="margin-right: -4px; border-bottom-right-radius: 0; border-top-right-radius: 0; height: 50px !important; border-left: 1px solid #333 !important; border-top: 1px solid #333 !important; border-bottom: 1px solid #333 !important;"><select class="ciudad text-font rounded-end border-0 w-25 bg-white" name="area" id="select-area" style="border-right: 1px solid #333 !important; border-top: 1px solid #333 !important; border-bottom: 1px solid #333 !important;">
                    <option value="" selected="">Área</option>
                    <?php foreach($areas as $a) : ?>
                        <option value="<?=$a['area']?>"><?=$a['area']?></option>
                    <?php endforeach ?>
                </select>
                <button type="submit" class="btn btn-default shadow p-3 ms-2"> Buscar</button>
            <?php else :?>
                <div class="tooltips w-100">
                    <input disabled class="buscar text-font rounded-start border-0 border-end h-100" type="search" placeholder="¿Qué talentos buscas?" style="margin-right: -4px; border-bottom-right-radius: 0; border-top-right-radius: 0; height: 52px !important; border-left: 1px solid #333 !important; border-top: 1px solid #333 !important; border-bottom: 1px solid #333 !important;">
                    <span class="tooltiptext">Hazte PRÉMIUM para buscar sin limites. Adquierelo <a href="<?=base_url('tarifas-contratista')?>" target="_blank">aquí</a></span>
                </div><select class="ciudad text-font rounded-end border-0 w-25 bg-white" name="area" id="select-area" style="border-right: 1px solid #333 !important; border-top: 1px solid #333 !important; border-bottom: 1px solid #333 !important;height: 52px">
                    <option value="" selected="">Área</option>
                    <?php foreach($areas as $a) : ?>
                        <option value="<?=$a['area']?>"><?=$a['area']?></option>
                    <?php endforeach ?>
                </select>
                <button class="btn btn-default shadow p-3 ms-2"> Buscar</button>
            <?php endif ?>
        </form>
    </div>
<?php else :?>
    <div class="col-md-12 col-lg-9 col-xl-7">
        <form class="flex" id="buscador-slide" method="GET" action="<?=base_url('buscar-proyecto')?>">
            <input class="buscar text-font rounded-start border-0 border-end w-50 h-100 bg-white" type="search" name="search" placeholder="¿Qué proyecto buscas?" style="margin-right: -4px; border-bottom-right-radius: 0; border-top-right-radius: 0; height: 50px !important; border-left: 1px solid #333 !important; border-top: 1px solid #333 !important; border-bottom: 1px solid #333 !important;"><select class="ciudad text-font rounded-end border-0 w-25 bg-white" name="area" id="select-area" style="border-right: 1px solid #333 !important; border-top: 1px solid #333 !important; border-bottom: 1px solid #333 !important;">
                <option value="" selected="">Área</option>
                <?php foreach($areas as $a) : ?>
                    <option value="<?=$a['area']?>"><?=$a['area']?></option>
                <?php endforeach ?>
            </select>
            <button type="submit" class="btn btn-default shadow p-3 ms-2"> Buscar</button>
        </form>
    </div>
<?php endif ?>