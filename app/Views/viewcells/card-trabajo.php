<?php if(empty($pro)) :?>
	<?php vacio();?>
<?php else :?>
	<div class="position-relative">
		<div class="card-job hover-up wow animate__animated <?= $pro['destacado'] == 1 ? 'proyecto_destacado' : ''?> animate__fadeIn" data-id="<?=$pro['id']?>" >
		<?php if($pro['destacado']) :?>
			<span class="position-absolute badge etiq-destacado bg-white" style="top: 10px;right: 30px;color: var(--color-naranja);font-size: .8rem;"><i class="fas fa-ad"></i> Anuncio destacado</span>
		<?php endif ;?>
		<div class="card-job-top">
			<div class="card-job-top--info">
				<div class="d-flex justify-content-between">
					<h6 class="card-job-top--info-heading"><a href="<?= !session('logeado') ? base_url('ingresar') : base_url('/trabajo/'.$pro['id'])?>" class="text-decoration-none"><?=$pro['titulo']?></a></h6>
				</div>
				<div class="row">
					<div class="col-lg-8">
						<span class="card-job-top--company pe-2"><?=view_cell('App\Libraries\FuncionesSistema::nameContratista', ['id' => $pro['id_contratista']])?></span>
						<span class="card-job-top--type-job text-sm d-none d-md-inline-block"><i class="fi-rr-briefcase"></i> <?=view_cell('App\Libraries\FuncionesSistema::diferenciaCantidadTiempo', ['inicio' => $pro['fecha_arranque_esperado'], 'fin' => $pro['fecha_entrega']])?></span>
						<span class="card-job-top--post-time text-sm text-capitalize"><i class="fi-rr-clock"></i> <?=view_cell('App\Libraries\FuncionesSistema::diferenciaFechas', ['fecha' => $pro['created_at']])?></span>
						<?= view_cell('App\Libraries\FuncionesSistema::nivelInsigniaProyecto', ['nivel' => $pro['nivel']]) ?>
					</div>
					<div class="col-lg-4 text-lg-end">
						<span class="card-job-top--price">
							<?php if($pro['presupuesto'] == 0) :?>
								<label class="text-md">Presupuesto confidencial</label>
							<?php else :?>
								$<?= number_format($pro['presupuesto'], 2, '.', ',') ?> <small>MXN</small><span>/Presupuesto</span>
							<?php endif;?>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="card-job-description mt-20 d-none d-lg-block">
			<?=character_limiter($pro['desc_corta'], 146)?>
		</div>
		<div class="card-job-bottom mt-25">
			<div class="d-flex flex-wrap gap-2">
				<div class="d-flex flex-wrap gap-2 cats-subcats">
					<span class="btn background-15 d-none d-md-block"><?=$pro['categoria']?></span>
					<span class="btn background-15 d-none d-md-block"><?=$pro['subcategoria']?></span>
				</div>
				<div class="d-flex flex-wrap gap-2 ms-auto boton-ver-propuesta">
					<?php $postulacion = model('Postulaciones')->where(['id_proyecto' => $pro['id'], 'id_postulante' => session('id')])->first();?>
					<?php if($postulacion != null && (session('logeado'))){?>
						<?= view_cell('App\Libraries\FuncionesSistema::mostrarChatCard', ['idproyecto' => $pro['id'], 'id' => session('id')]) ?>
					<?php } ?>
					<a class="btn btn-default" style="color: #ffffff;" href="<?= !session('logeado') ? base_url('ingresar') : base_url('/trabajo/'.$pro['id'])?>">
						Ver propuesta
					</a>
					<?php if($pro['estatus'] == 'espera' && (session('rol') == 'freelancer')) : ?>
						<?= view_cell('App\Libraries\FuncionesSistema::validarPostulacion', ['idproyecto' => $pro['id'], 'idusuario' => session('id')]) ?>
					<?php endif ;?>
				</div>
			</div>
		</div>
	</div>
	</div>
	<?php if(session('id') == $pro['id_contratista']) :?>
		<script>
			$('#check_destacado_<?=$pro['id']?>').change(function(){
				$btn = $(this);
				$.ajax({
					url: '<?=base_url('Proyectos/destacarProyecto')?>',
					type: 'POST',
					data: {check : $btn.val(), id : <?=$pro['id']?>},
					success: function (data) {
						let respuesta = JSON.parse(data);
						return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					}, error: function (data) {
						let respuesta = JSON.parse(data.responseText);
						alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
					}
				});
			});
		</script>
	<?php endif ;?>
<?php endif ;?>
<style>
	.card-job-bottom .alert{
		padding: 8px 15px !important;
		line-height: 1 !important;
		text-align: center;
	}
</style>