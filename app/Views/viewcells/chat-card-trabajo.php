<?php if(!empty($data)) :?>
    <?php if($data['chat']){ ?>
        <?php
            $mensajes = model('Salas')->where('id', $data['sala'])->first();
            if($mensajes['mensajes'] == null){
                $total = 0;
            }else{
                $d = json_decode($mensajes['mensajes'], TRUE);
                $total = $d[session('rol')];
            }
        ?>
        <a class="btn btn-primary position-relative" href="<?=base_url('/usuarios/chat/'.$data['sala'])?>" data-sala="<?=$data['sala']?>">
            Sala de chat
        </a>
    <?php } ?>
<?php endif ;?>