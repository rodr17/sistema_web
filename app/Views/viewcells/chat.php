<?php if(!empty($data)) :?>
    <?php if($data['chat']){ ?>
        <a class="btn" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Ir a chat" href="<?=base_url('/usuarios/chat/'.$data['sala'])?>">
            <i class="fas fa-comments"></i>
        </a>
    <?php } ?>
<?php endif ;?>