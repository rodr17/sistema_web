<label class="label">Nombre: <?=$freelance['nombre']?></label><br>
<label class="label">Apellidos: <?=$freelance['apellidos']?></label><br>
<label class="label">Correo: <?=$freelance['correo']?></label><br>
<label class="label">Teléfono: <?=!empty($freelance['contacto']['telefono']) ? $freelance['contacto']['telefono'] : ''?></label><br>
<label class="label">Fecha de nacimiento: <?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $freelance['nacimiento']])?></label><br>
<div class="my-3">
    <label class="label">CURP: <?= isset($freelance['direccion']['curp'])? $freelance['direccion']['curp'] : '' ?></label>
    <br><a class="text-danger" href="https://www.gob.mx/curp/" target="_blank">Validar CURP del usuario</a>
</div>
<div class="text-center fw-bold">Dirección</div>
<label class="label">Calle: <?= isset($freelance['direccion']['calle'])? $freelance['direccion']['calle'] : ''?></label><br>
<label class="label">Colonia: <?= isset($freelance['direccion']['colonia'])? $freelance['direccion']['colonia'] : ''?></label><br>
<label class="label">Municipio: <?= isset($freelance['direccion']['municipio'])? $freelance['direccion']['municipio'] : ''?></label><br>
<label class="label">Ciudad: <?= isset($freelance['direccion']['ciudad'])? $freelance['direccion']['ciudad'] : ''?></label><br>
<label class="label">Estado: <?= isset($freelance['direccion']['estado'])? $freelance['direccion']['estado'] : ''?></label><br>
<label class="label">País: <?= isset($freelance['direccion']['pais'])? $freelance['direccion']['pais'] : ''?></label><br>
<label class="label">Codigo Postal: <?= isset($freelance['direccion']['codigo_postal'])? $freelance['direccion']['codigo_postal'] : ''?></label>