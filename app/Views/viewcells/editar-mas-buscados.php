<form id="form-cat-mas-buscada">
    <div class="form-floating mb-3">
        <select class="form-select" id="selectorAreasGeneral" name="categoria" aria-label="Floating label select example">
            <?php foreach($categorias as $c){?>
                <option value="<?=$c['area']?>" <?= (view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-'.$id, 'valor' => 'categoria']) == $c['area'])? 'selected' : '' ?>><?=$c['area']?></option>
            <?php } ?>
        </select>
        <label for="selectorAreasGeneral">Seleccione el area</label>
    </div>
    <input id="id-cat-buscado" name="id" type="hidden" value="<?=$id?>">
    <div class="form-floating mb-3">
        <input type="text" class="form-control" id="titulo-cat-mas-buscado" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-'.$id, 'valor' => 'titulo'])?>" name="titulo" placeholder="...">
        <label for="titulo-cat-mas-buscado">Titulo para busqueda</label>
    </div>
    <div class="my-3">
        <input type="hidden" id="imagen-cat-buscado" name="imagen" value="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-'.$id, 'valor' => 'imagen'])?>">
        <input class="form-control form-control-sm" id="mas-buscados-imagen" type="file" onchange="readUrlImagen(this, 'imagen-cat-buscado', 'preview-cat-buscado');">
        <div class="text-center m-3">
            <img id="preview-cat-buscado" class="w-25" src="<?=view_cell('App\Libraries\FuncionesAdmin::mostrarValorSeccion', ['seccion' => 'inicio', 'tipo' => 'categoria-mas-buscados-'.$id, 'valor' => 'imagen'])?>" alt="your image" />
        </div>
    </div>
    <div class="text-center">
        <a class="btn btn-primary" id="save-cat-buscada">
            Actualizar categoria
        </a>
    </div>
</form>
<script>
    $('#save-cat-buscada').click(function(){
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Secciones/updateCategoriMasBuscados')?>',
            data: $('#form-cat-mas-buscada').serialize(),
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error') return alertify.warning(cont.mensaje, 10);
                return alertify.success(cont.mensaje, 10);
            }, error: function(){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo.', 10);
            }
        });
    });
</script>