<div class="rate-reviews-small text-center">
    <?php for($x = 1; $x <= 5; $x++){ ?>
        <?= ($x <= $promedio)? '<span><img src="'.base_url('assets/themes/imgs/theme/icons/star.svg').'" style="width: 14px; height: 13px;" alt="mextemps" /></span>' : '<span><img style="width: 14px; height: 13px;" src="'.base_url('assets/themes/imgs/theme/icons/star-gray.svg').'" alt="mextemps" /></span>' ;?>
    <?php } ?>
    <span class="text-muted text-small">(<?=$promedio?>)</span>
</div>