<?php $es_mitad = false?>
<div class="rate-reviews-small text-center">
    <?php for($x = 1; $x <= 5; $x++){ ?>
        <?php if($x <= $promedio) :?>
            <span><img src="<?= base_url('assets/themes/imgs/theme/icons/star.svg')?>" alt="mextemps" /></span>
        <?php else :?>
            <?php if(is_double($total) && (abs($total - $promedio) >= .5) && !$es_mitad) :?>
                <?php $es_mitad = true?>
                <span><img src="<?= base_url('assets/themes/imgs/theme/icons/star-middle.svg')?>" alt="calificacion <?=$x?>" /></span>
            <?php else :?>
                <span><img style="width: 14px; height: 13px;" src="<?= base_url('assets/themes/imgs/theme/icons/star-gray.svg')?>" alt="mextemps" /></span>
            <?php endif ;?>
        <?php endif ;?>
    <?php } ?>
    <span class="ml-10 text-muted text-small">(<?=$total?>)</span>
</div>