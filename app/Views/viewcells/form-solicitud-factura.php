<div class="mb3">
    <p class="h4"><strong>ID de referencía</strong></p>
    <input type="text" class="form-control" id="idreferencia" value="<?=$id?>" name="id" readonly placeholder="ID de referencía">
</div>
<p class="h4 mt-3"><strong>Datos de facturación</strong></p>
<div class="row mb-3">
    <div class="col-md-4 align-self-center">
        <label for="nombrefactura" class="mb-0">Nombre</label>
    </div>
    <div class="col-md-8">
        <input type="text" class="form-control" id="nombrefactura" name="nombre" value="<?= $usuario['nombre']?> <?=$usuario['apellidos']?>">
    </div>
</div>
<div class="row mb-3">
    <div class="col-md-4 align-self-center">
        <label for="rfcfactura" class="mb-0">RFC</label>
    </div>
    <div class="col-md-8">
        <input type="text" class="form-control" id="rfcfactura" name="rfc" value="<?= isset($usuario['direccion']['rfc']) ? $usuario['direccion']['rfc'] : '' ?>">
    </div>
</div>
<div class="row mb-3">
    <div class="col-md-4 align-self-center">
        <label form="correofactura" class="mb-0">Correo</label>
    </div>
    <div class="col-md-8">
        <input type="text" class="form-control" id="correofactura" name="correo" value="<?=$usuario['correo']?>">
    </div>
</div>
<div class="row mb-3">
    <div class="col-md-4 align-self-center">
        <label for="telefonofactura" class="mb-0">Teléfono</label>
    </div>
    <div class="col-md-8">
        <input type="text" class="form-control numerico" id="telefonofactura" name="telefono" value="<?= isset($usuario['contacto']['telefono'])? $usuario['contacto']['telefono'] : '' ?>">
    </div>
</div>
<div class="row mb-3">
    <div class="col-md-4 align-self-center">
        <label for="zipfactura" class="mb-0">Código Postal</label>
    </div>
    <div class="col-md-8">
        <input type="text" class="form-control" id="zipfactura" name="zip" value="<?= isset($usuario['direccion']['codigo_postal']) ? $usuario['direccion']['codigo_postal'] : '' ?>">
    </div>
</div>
<div class="row mb-3">
    <div class="col-md-4 align-self-center">
        <label for="callefactura" class="mb-0">Calle y numero</label>
    </div>
    <div class="col-md-8">
        <input type="text" class="form-control" id="callefactura" name="calle" value="<?= isset($usuario['direccion']['calle']) ? $usuario['direccion']['calle'] : '' ?>">
    </div>
</div>
<div class="row mb-3">
    <div class="col-md-4 align-self-center">
        <label for="coloniafactura" class="mb-0">Colonia</label>
    </div>
    <div class="col-md-8">
        <input type="text" class="form-control" id="coloniafactura" name="colonia" value="<?= isset($usuario['direccion']['colonia']) ? $usuario['direccion']['colonia'] : '' ?>">
    </div>
</div>
<div class="row mb-3">
    <div class="col-md-4 align-self-center">
        <label for="ciudadfactura" class="mb-0">Municipio</label>
    </div>
    <div class="col-md-8">
        <input type="text" class="form-control" id="ciudadfactura" name="ciudad" value="<?= isset($usuario['direccion']['municipio']) ? $usuario['direccion']['municipio'] : '' ?>">
    </div>
</div>
<div class="row mb-3">
    <div class="col-md-4 align-self-center">
        <label for="estadofactura" class="mb-0">Estado</label>
    </div>
    <div class="col-md-8">
        <input type="text" class="form-control" id="estadofactura" name="estado" value="<?= isset($usuario['direccion']['estado']) ? $usuario['direccion']['estado'] : '' ?>">
    </div>
</div>
<div class="row mb-3">
    <div class="col-md-4 align-self-center">
        <label for="paisfactura" class="mb-0">Pais</label>
    </div>
    <div class="col-md-8">
        <input type="text" class="form-control" id="paisfactura" name="pais" value="<?= isset($usuario['direccion']['pais']) ? $usuario['direccion']['pais'] : '' ?>">
    </div>
</div>
<div class="text-center mt-3">
    <a class="btn btn-default" id="env-sol-factura">Enviar solicitud</a>
</div>
<script>
    $('#env-sol-factura').click(function(){
        $.ajax({
            type: 'POST',
            url :'<?=base_url('usuarios/crearSolicitudFactura')?>',
            data: $('#form-solicitud-factura').serialize(),
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo != 'correcto') return alertify.notify(cont.mensaje, cont.tipo, 10);
                
                $('#listado-historial-pago').load('<?=base_url('Usuarios/verPagos')?>');
                $('.modal').modal('hide');
                return alertify.notify(cont.mensaje, cont.tipo, 10);
            }, error: function(data){
                alertify.error('Ha surgido un error, comuníquese con el equipo de soporte.', 10);
            }
        });
    });
</script>