<div class="form-group select-style select-style-icon mb-0">
    <select class="form-control form-icons select-active select3-hidden-accessible" name="<?= $atributo ?>" required>
        <option value="default" hidden selected>Idiomas</option>
        <?php foreach($idiomas as $i){ ?>
            <option value="<?=$i['identificador']?>">
            <?= $i['nombre'] ?>
            <small class="small">(<?= $i['identificador'] ?>)</small>
        </option>
        <?php } ?>
    </select>
    <i class="fi-rr-globe"></i>
</div>