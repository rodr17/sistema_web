<?php if(!empty($mensajes)){ ?>
    <?php foreach($mensajes as $m){ ?>
        <?php if($m['iduser'] == session('id')){ ?>
            <div class="col-7 msg-item right-msg offset-4">
                <div class="msg-img">
                    <img class="img-thumbnail rounded-circle" src="<?=imagenPerfil(session('imagen'))?>">
                </div>
                <div class="msg-text">
                    <span class="author">Yo</span> <span class="time"><?=$m['hora']['date']?></span><br>
                    <p><?= $m['mensaje']?></p>
                </div>
            </div>
        <?php }else{ ?>
            <div class="col-7 msg-item left-msg">
                <div class="msg-img">
                    <img class="img-thumbnail rounded-circle" src="<?=imagenPerfil($usuario)?>">
                </div>
                <div class="msg-text">
                    <span class="author"><?=$usuario['nombre']?></span> <span class="time"><?=$m['hora']['date']?></span><br>
                    <p><?= $m['mensaje']?></p>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
<?php } ?>