<?php if($noticias == null){ ?>
    <div class="text-center">
        <h3>Sin más noticias para ti de momento</h3>
    </div>
<?php }else{ ?>
    <div class="owl-carousel noticias-carr">
        <?php foreach($noticias as $n){ ?>
            <div class="item">
                <div class="card-grid-3 hover-up p-15 text-center">
                    <a href="<?=base_url('/ver-noticia?id='.base64_encode($n['id']).'&title='.$n['titulo'])?>">
                        <figure><img alt="mextemps" src="<?=$n['imagen']?>" style="max-height: 200px;"></figure>
                    </a>
                    <h6 class="heading-md mt-15 mb-0" style="min-height: 75px;">
                        <a href="<?=base_url('/ver-noticia?id='.base64_encode($n['id']).'&title='.$n['titulo'])?>">
                            <?=$n['titulo']?>
                        </a>
                    </h6>
                </div>
            </div>
        <?php } ?>
    </div>
    <script>
        $('.owl-carousel.noticias-carr').owlCarousel({
            margin: 20,
            responsiveClass: true,
            nav: false,
            stagePadding: 100,
            loop:true,
            responsive: {
                0: {
                    items: 1
                },
                200: {
                    items: 1
                },
                350: {
                    items: 1
                },
                450: {
                    items: 1
                },
                576: {
                    items: 1
                },
                768: {
                    items: 2
                },
                992: {
                    items: 3
                },
                1200: {
                    items: 3
                }
            }
        });
    </script>
<?php } ?> 