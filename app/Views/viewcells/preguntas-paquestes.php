<div class="text-center d-grid gap-2">
    <?php foreach($preguntas as $p){ ?>
        <?php $info = json_decode($p['info'], true); ?>
        <button class="btn btn-primary btn-pregunta" data-id="<?=$p['id']?>">
            <?=$info['pregunta']?>
        </button>
    <?php } ?>
</div>
<script>
    $('.btn-pregunta').click(function(){
        let id = $(this).attr('data-id');
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Secciones/mostrarPreguntasPaquetes')?>',
            data:{id:id},
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'error'){
                    alertify.warning(cont.mensaje, 10);
                }else{
                    $('#guardar-pregunta').html('Actualizar pregunta')
                    $('#id').val(cont.id);
                    $('#pregunta').val(cont.pregunta);
                    $('#respuesta').val(cont.respuesta);
                }
            }, error: function(){
                alertify.error('A surgido un error, comuniquese con el equipo de desarrollo', 10);
            }
        });
    });
</script>