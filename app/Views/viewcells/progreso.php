<div class="progress" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Progreso del proyecto">
    <div class="progress-bar progress-bar-striped progress-bar-animated <?php if($progreso == '100%') echo 'bg-success'; ?>" id="progreso" role="progressbar" style="width: 100%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"><?=$progreso?>%</div>
</div>