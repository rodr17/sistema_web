<?php if($proyectos == null){ ?>
    <div class="text-center">
        Aun no hay proyectos para tus intereses.
    </div>
<?php }else{ ?>
    <?php foreach($proyectos as $pro){ ?>
        <?=view_cell('App\Libraries\FuncionesSistema::cardProyectoDisponible', ['proyecto' => $pro])?>
    <?php } ?>
<?php } ?>