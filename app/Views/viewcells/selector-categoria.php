<div class="form-floating">
    <select class="form-select" id="selectorAreasGeneral" name="categoria" aria-label="Floating label select example">
        <?php foreach($categorias as $c){?>
            <option value="<?=$c['area']?>"><?=$c['area']?></option>
        <?php } ?>
    </select>
    <label for="selectorAreasGeneral">Seleccione el area</label>
</div>