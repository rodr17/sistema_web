<style>
    .mes-tachado {
		text-decoration: line-through; 
		color: #88929b; 
		font-size: 20px; 
		line-height: 20px;
		display: inline-block;
	}
	.mes-tachado.abajo, .text-month.abajo {
		display: block;
		font-size: 65% !important;
	}
	.box-pricing-item.most-popular .mes-tachado {
		color: #fff;
	}
</style>
<div class="text-center mt-20 switch-pagos">
	<span class="text-lg text-billed">Pago Mensual</span>
	<label class="switch ml-20 mr-20">
		<input type="checkbox" id="cb_billed_type" name="billed_type" onchange="checkBilled()" />
		<span class="slider round"></span>
	</label>
	<span class="text-lg text-billed">Pago Anual</span>
</div>
<div class="block-pricing mt-50 mt-md-50" style="background: #ff510014;">
	<?php $planes = model('ModelPlanes')->where('cuenta', 'contratante')->orderBy('id', 'ASC')->find()?>
	<div class="row">
		<?php foreach($planes as $key => $plan){ ?>
			<div class="col-lg-4 col-sm-12 wow animate__animated animate__fadeInUp <?= ($plan['name'] == 'Intermedio')? 'order-md-1 order-lg-2' : 'order-2'?>" data-wow-delay=".1s">
				<div class="box-pricing-item <?= ($plan['name'] == 'Intermedio')? 'most-popular' : ''?>">
					<?php if($plan['name'] == 'Intermedio') :?>
						<div class="text-end mb-0 destacado">
							<a class="btn btn-white-sm">Más popular</a>
						</div>
					<?php endif ;?>
					<h4 class="mb-15"><?=$plan['name']?></h4>
					<div class="box-info-price">
						<span class="text-price for-month display-month"><?= ($plan['monto'] == '0')? 'Gratis' : '$'.number_format($plan['monto'], 2, '.', ',').' <small>MXN</small>' ?></span>
						<span class="text-price for-year">
							<?php if($plan['monto'] != 0) :?>
								<span class="mes-tachado d-block"><?= '$'.number_format($plan['monto'], 2, '.', ',').' <small>MXN</small>'?></span>
							<?php else :?>
								<small class="diasgratis">365 días</small>
								<span>Gratis</span>
							<?php endif ;?>
							<?= ($plan['monto'] != '0')? '$'.number_format(($plan['monto_anual'] / 12), 2, '.', ',').' <small>MXN</small>' : '' ?>
						</span>
						<span class="text-month for-month display-month" style="margin-top: 1px;"><?= ($plan['monto'] == '0') ? 'Vigencia 1 año' : 'Pago mensual'?></span>
						<span class="text-month for-year" style="margin-top: 1px;"><?= ($plan['monto'] != '0') ? 'Precio mensual' : 'Vigencia 1 año'?></span>
					</div>
					<div>
						<p class="text-desc-package mb-30">
						    <?=$plan['descripcion']?>
						    <br><br class="for-month">
						    <?php if($plan['monto'] != 0) :?>
						        <span class="text-facturados for-year">$<?=number_format($plan['monto_anual'], 2, '.', ',')?> MXM facturados anualmente</span>
                            <?php else :?>
                                <span class="text-facturados for-year">365 días de vigencia</span>
						    <?php endif ;?>
					    </p>
					</div>
					<div class="boton">
						<a class="btn btn-border for-month display-month" plan="<?=$plan['id_plan_mensual']?>" name="<?=$plan['id_plan_mensual']?>"><?=($plan['monto'] != 0)? 'Suscribirse' : 'Probar'?></a>
						<br class="for-month">
						<a class="btn btn-border for-year" plan="<?=$plan['id_plan_anual']?>" name="<?=$plan['id_plan_anual']?>"><?=($plan['monto'] != 0)? 'Suscribirse' : 'Probar'?></a>
						<input class='invisible text-font inputs-form plan_seleccionado' hidden>
					</div>
					<div class="d-block d-lg-none">
                        <div class="accordion accordion-flush" id="accordionFlush<?=$key?>">
                            <div class="accordion-item bg-transparent border-0">
                                <h2 class="accordion-header" id="flush-heading-card-<?=$key?>">
                                    <button class="accordion-button collapsed contenedor-arrow-v" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-card-<?=$key?>" aria-expanded="false" aria-controls="flush-collapse-card-<?=$key?>">
                                        Ver características <i class="arrow-v down-v"></i>
                                    </button>
                                </h2>
                                <div id="flush-collapse-card-<?=$key?>" class="accordion-collapse collapse" aria-labelledby="flush-heading-card-<?=$key?>" data-bs-parent="#accordionFlush<?=$key?>">
                                    <div class="accordion-body p-2 listado-caracteristicas">
                                        <?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'perfilcontratante']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                Crear perfil de Contratante
                                            </div>
                                        <?php } ?>
                                        <?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'paneladministracion']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                Panel de administración
                                            </div>
                                        <?php } ?>
                                        <?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'valoraciontrabajos']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                Valoracion de trabajos
                                            </div>
                                        <?php } ?>
                                        <?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'historialconversaciones']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                Salas de chat
                                            </div>
                                        <?php } ?>
                                        <?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'contactarFreelancer']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                Contactar Freelancer
                                            </div>
                                        <?php } ?>
                                        <div class="text-center my-3 border-bottom caract-list">
                                            Buscador de freelancer
                                        </div>
                                        <?php $solicitudes = intval(view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'enviosolicitudes']))?>
                                        <?php if($solicitudes > 0){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                <?= $solicitudes == 9999 ? 'Envio de solicitudes por proyecto ilimitadas' : $solicitudes.' envio(s) de solicitudes por proyecto' ?>
                                            </div>
                                        <?php } ?>
                                        <?php $proyectos_activos = intval(view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'proyectosActivos']))?>
                                        <?php if($proyectos_activos > 0){?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                <?= $proyectos_activos == 9999 ? 'Proyectos activos ilimitados' : $proyectos_activos.' proyecto(s) activos'?>
                                            </div>
                                        <?php } ?>
                                        <?php $historial_trabajos = intval(view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'historialtrabajos'])) ?>
                                        <?php if($historial_trabajos > 0){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                <?= $historial_trabajos == 9999 ? 'Historial de proyectos ilimitado' : 'Historial de '.$historial_trabajos.' proyectos'?>
                                            </div>
                                        <?php } ?>
                                        <?php $destacados = intval(view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'proyectosDestacados'])); ?>
                                        <?php if($destacados > 0){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                <?= $destacados > 9998 ? 'Proyectos destacados ilimitados' : $destacados.' proyecto(s) destacado(s)'?>
                                            </div>
                                        <?php } ?>
                                        <table class="w-100 tabla-desplegada d-none">
                                            <tbody>
                                                <tr class="border-bottom">
                                                    <td scope="row">Crear perfil de Contratante
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Puedes crear tu perfil con tus datos.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'perfilcontratante'])?>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Panel de administración
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Es donde podrás ver los proyectos y el status de ellos. Así como el historial de compras, métodos de pago entre otros.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'paneladministracion'])?>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Valoracion de trabajos
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Tendrás la oportunidad valorar los trabajos realizados por el freelancer contratado.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'valoraciontrabajos'])?>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Salas de chat
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Acceso donde podrás chatear y consultar todas las conversaciones que hayas tenido previamente en algún proyecto.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <?= view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'historialconversaciones'])?>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Contactar Freelancer
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Acceso donde podrás visualizar, obtener y comunicarte con el freelancer de tu preferencia.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <?= view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'historialconversaciones'])?>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Buscador de freelancer
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Es donde podrás buscar los talentos que necesites de acuerdo a diferentes categorías.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center <?=($key == 0)? '' : 'd-none' ?>">
                                                        Restringido
                                                    </td>
                                                    <td class="text-center <?=($key == 1)? '' : 'd-none' ?>">
                                                        Estándar
                                                    </td>
                                                    <td class="text-center <?=($key == 2)? '' : 'd-none' ?>">
                                                        Ilimitado
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Envio de solicitudes por proyecto
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Te da la oportunidad de contactar al freelancer de tu interés directamente.</span>
                                                        </div>
                                                    </td>
                                                    
                                                    <td class="text-center">
                                                        <?= $solicitudes == 9999 ? 'Ilimitado' : $solicitudes?>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Proyectos activos
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Es la cantidad de proyectos que puedes publicar.</span>
                                                        </div>
                                                    </td>
                                                    
                                                    <td class="text-center">
                                                        
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Historial de proyectos
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Es la cantidad de trabajos finalizados publicados que puedes visualizar.</span>
                                                        </div>
                                                    </td>
                                                    <?php $historial_trabajos = view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'historialtrabajos'])?>
                                                    <td class="text-center">
                                                        <?= $historial_trabajos == 9999 ? 'Ilimitado' : $historial_trabajos?>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Proyectos Destacados
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Cantidad de proyectos publicados que puedes destacar para que sean los primeros que aparezcan en la lista de búsqueda.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <?=view_cell('App\Libraries\FuncionesSistema::valorNumPlan', ['plan' => $plan['id'], 'valor' => 'proyectosDestacados'])?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
<script>
    $(document).ready(function(){
        $("#cb_billed_type").prop('checked', true);
        checkBilled();
    });
</script>