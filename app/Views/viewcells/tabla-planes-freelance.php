<style>
    .mes-tachado {
		text-decoration: line-through; 
		color: #88929b; 
		font-size: 20px; 
		line-height: 20px;
		display: inline-block;
	}
	.mes-tachado.abajo, .text-month.abajo {
		display: block;
		font-size: 65% !important;
	}
	.box-pricing-item.most-popular .mes-tachado {
		color: #fff;
	}
</style>
<div class="text-center mt-20 switch-pagos">
	<span class="text-lg text-billed">Pago Mensual</span>
	<label class="switch ml-20 mr-20">
		<input type="checkbox" id="cb_billed_type" name="billed_type" onchange="checkBilled()" />
		<span class="slider round"></span>
	</label>
	<span class="text-lg text-billed">Pago Anual</span>
</div>
<div class="block-pricing mt-50 mt-md-50" style="background: #ff510014;">
	<?php $planes = model('ModelPlanes')->where('cuenta', 'freelancer')->orderBy('id', 'ASC')->find();
	    $aux = array();
        foreach ($planes as $key => $row) {
            $aux[$key] = $row['monto'];
        }
        array_multisort($aux, SORT_ASC, $planes);
	?>
	<div class="row">
		<?php foreach($planes as $key => $plan){ ?>
			<div class="col-lg-3 col-sm-12 wow animate__animated animate__fadeInUp <?= ($plan['name'] == 'Intermedio')? 'order-md-1 order-lg-2' : 'order-2'?>" data-wow-delay=".1s">
				<div class="box-pricing-item <?= ($plan['name'] == 'Intermedio')? 'most-popular' : ''?>">
                    <div class="text-end mb-0 destacado">
                    <?php if($plan['name'] == 'Intermedio') :?>
                        <a class="btn btn-white-sm">Más popular</a>
                    <?php endif ;?>
                    </div>
                    <h4 class="mb-15"><?=$plan['name']?></h4>
					<div class="box-info-price">
						<span class="text-price for-month display-month"><?= ($plan['monto'] == '0')? 'Gratis' : '$'.number_format($plan['monto'], 2, '.', ',').' <small>MXN</small>' ?></span>
						<span class="text-price for-year">
							<?php if($plan['monto'] != 0) :?>
								<span class="mes-tachado d-block"><?= '$'.number_format($plan['monto'], 2, '.', ',').' <small>MXN</small>'?></span>
							<?php else :?>
							    <small class="diasgratis">365 días</small>
								<span>Gratis</span>
							<?php endif ;?>
							<?= ($plan['monto'] != '0')? '$'.number_format(($plan['monto_anual'] / 12), 2, '.', ',').' <small>MXN</small>' : '' ?>
						</span>
						<span class="text-month for-month display-month" style="margin-top: 1px;"><?= ($plan['monto'] == '0') ? 'Vigencia 1 año' : 'Pago mensual'?></span>
						<span class="text-month for-year" style="margin-top: 1px;"><?= ($plan['monto'] != '0') ? 'Precio mensual <br>' : 'Vigencia 1 año'?></span>
					</div>
					<div>
						<p class="text-desc-package mb-30">
						    <?=$plan['descripcion']?>
						    <br><br class="for-month">
						    <?php if($plan['monto'] != 0) :?>
						        <span class="text-facturados for-year">$<?=number_format($plan['monto_anual'], 2, '.', ',')?> MXM facturados anualmente</span>
                            <?php else :?>
                                <span class="text-facturados for-year">365 días de vigencia</span>
						    <?php endif ;?>
					    </p>
					</div>
					<div class="boton">
						<a class="btn btn-border for-month display-month" plan="<?=($plan['monto'] != 0) ? $plan['id_plan_mensual'] : ''?>" name="<?=($plan['monto'] != 0) ? $plan['id_plan_mensual'] : ''?>"><?=($plan['monto'] != 0)? 'Suscribirse' : 'Probar'?></a>
						<br class="for-month">
						<a class="btn btn-border for-year" plan="<?= ($plan['monto'] != 0) ? $plan['id_plan_anual'] : ''?>" name="<?= ($plan['monto'] != 0) ? $plan['id_plan_anual'] : ''?>"><?=($plan['monto'] != 0)? 'Suscribirse' : 'Probar'?></a>
						<input class='invisible text-font inputs-form plan_seleccionado' hidden>
					</div>
					<div class="linea-separacion">
						<span class="separador-tarjetas"></span>
					</div>
                    <div class="d-block d-lg-none">
                        <div class="accordion accordion-flush" id="accordionFlush<?=$key?>">
                            <div class="accordion-item bg-transparent border-0">
                                <h2 class="accordion-header" id="flush-heading-card-<?=$key?>">
                                    <button class="accordion-button collapsed contenedor-arrow-v" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-card-<?=$key?>" aria-expanded="false" aria-controls="flush-collapse-card-<?=$key?>">
                                        Ver características <i class="arrow-v down-v"></i>
                                    </button>
                                </h2>
                                <div id="flush-collapse-card-<?=$key?>" class="accordion-collapse collapse" aria-labelledby="flush-heading-card-<?=$key?>" data-bs-parent="#accordionFlush<?=$key?>">
                                    <div class="accordion-body p-2 listado-caracteristicas">
                                        <div class="text-center my-3 border-bottom caract-list">
                                            Crear Perfil
                                        </div>
                                        <?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'paneladministracion']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                Panel de administración
                                            </div>
                                        <?php } ?>
                                        <?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'solicitudtrabajo']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                Solicitudes de trabajo
                                            </div>
                                        <?php } ?>
                                        <?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'crearcv']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                Cargar CV
                                            </div>
                                        <?php } ?>
                                        <?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'veroportunidades']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                Visualización de oportunidades
                                            </div>
                                        <?php } ?>
                                        <?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'historialconversaciones']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                Salas de chat
                                            </div>
                                        <?php } ?>
                                        <?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'freelancer_destacado']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                Freelancer destacado
                                            </div>
                                        <?php } ?>
                                        <!-- <php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'ganarinsignias']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                Acceso a insignias
                                            </div>
                                        <php } ?> -->
                                        <!-- <php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'valoraciontrabajos']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                Valoración de trabajos
                                            </div>
                                        <php } ?> -->
                                        <!-- <php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'recomendacionesclientes']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                Testimonios de tus clientes
                                            </div>
                                        <php } ?> -->
                                        <?php if(view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'anadirportafolio']) == '<span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>'){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                Cargar portafolio
                                            </div>
                                        <?php } ?>
                                        <?php $porSemana = view_cell('App\Libraries\FuncionesSistema::postuSemana', ['plan' => $plan['id']])?>
                                        <?php if($porSemana > 0){ ?>
                                            <div class="text-center my-3 border-bottom caract-list">
                                                <?= $porSemana == 9999 ? 'Postulaciones al mes ilimitadas' : $porSemana.' postulaciones al mes'?>
                                            </div>
                                        <?php } ?>
                                        <table class="w-100 tabla-desplegada d-none">
                                            <tbody>
                                                <tr class="border-bottom">
                                                    <td scope="row">Crear Perfil 
                                                        <div class="tooltips d-none"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Descripción del contenido de este apartado pendiente...</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center col-uno">
                                                        <span class="tabla-tarifas-success"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Panel de administración
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Es donde podrás tener el historial de tu perfil, proyectos, pagos y  método de pago.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'paneladministracion'])?>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Solicitudes de trabajo 
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Los contratantes pueden mandarte solicitudes de propuestas, y las podrás ver en esta sección.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'solicitudtrabajo'])?>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Cargar CV 
                                                        <div class="tooltips d-none"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Cargar CV.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'crearcv'])?>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Visualización de oportunidades 
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Podrás ver las diferentes oportunidades de proyectos y la información del contratante para que lo puedas contactar y postularte.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'veroportunidades'])?>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Salas de chat 
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Acceso donde podrás chatear y consultar todas las conversaciones que hayas tenido previamente en algún proyecto.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'historialconversaciones'])?>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Freelancer destacado
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Sé de los primeros freelancers en ser buscados por contratante.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'freelancer_destacado'])?>
                                                    </td>
                                                </tr>
                                                <!-- <tr class="border-bottom">
                                                    <td scope="row">Acceso a insignias
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Tendrás acceso de que tu trabajo sea valorado y convertirte en un freelancer distinguido.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'ganarinsignias'])?>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Valoración de trabajos
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Tendrás la oportunidad de que el contratante  pueda valorarte y que tu trabajo sea reconocido en nuestra red.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'valoraciontrabajos'])?>
                                                    </td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Testimonios de tus clientes 
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Podrás tener comentarios de clientes acerca de los trabajos realizados, esto te ayudará a que tengas recomendaciones de ellos.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'recomendacionesclientes'])?></td>
                                                </tr> -->
                                                <tr class="border-bottom">
                                                    <td scope="row">Cargar portafolio 
                                                        <div class="tooltips d-none"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Cargar portafolio.</span>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><?=view_cell('App\Libraries\FuncionesSistema::verValorTabla', ['plan' => $plan['id'], 'valor' => 'anadirportafolio'])?></td>
                                                </tr>
                                                <tr class="border-bottom">
                                                    <td scope="row">Postulaciones al mes
                                                        <div class="tooltips"><i style="color: #000000;" class="fas fa-info-circle"></i>
                                                            <span class="tooltiptext">Es el número de propuestas a las que te puedes postular por mes.</span>
                                                        </div>
                                                    </td>
                                                    <?php $porSemana = view_cell('App\Libraries\FuncionesSistema::postuSemana', ['plan' => $plan['id']])?>
                                                    <td class="text-center"><?= $porSemana == 9999 ? 'Ilimitado' : $porSemana?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
<script>
    $(document).ready(function(){
        $("#cb_billed_type").prop('checked', true);
        checkBilled();
    });
</script>