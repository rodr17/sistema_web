<div class="row p-2">
    <?php foreach($tareas as $key => $t){ ?>
        <div class="col-12 col-lg-6" id="ta<?=$key?>rea">
            <div class="form-check">
                <label class="form-check-label">
                    <?php if($t['estatus'] == true){ ?>
                        <i class="fas fa-check text-success" data-bs-toggle="tooltip" data-bs-placement="top" title="Tarea realizada"></i>
                    <?php }else{ ?>
                        <i class="fas fa-times text-danger" data-bs-toggle="tooltip" data-bs-placement="top" title="Tarea aún sin realizar"></i>
                    <?php } ?>
                    <?=$t['tarea']?>
                </label>
            </div>
        </div>
    <?php } ?>
</div>