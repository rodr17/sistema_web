<?php foreach($tareas as $key => $t){ ?>
    <div class="col-12" id="ta<?=$key?>rea side-tareas-info">
        <div class="form-check">
            <input class="form-check-input <?= !$proyecto['deleted'] ? 'checkbox-tarea"' : ''?>" <?= !$proyecto['deleted'] ? 'onchange="validar()"' : 'disabled'?>  data-tar="<?=$key?>" type="checkbox" value="true" name="tarea<?=$key?>check" id="tarea<?=$key?>check" <?php if($t['estatus'] == true) echo 'checked'; ?>>
            <label class="form-check-label <?= !$proyecto['deleted'] ? 'badge_eliminar' : ''?>">
                <?php if(!$proyecto['deleted']) :?>
                    <span data-tarea="<?=$key?>"><i class="fas fa-times text-danger" aria-hidden="true"></i></span>
                <?php endif;?>
                &nbsp;&nbsp;&nbsp;&nbsp;<?=$t['tarea']?>
            </label>
        </div>
    </div>
<?php } ?>
<?php if(!$proyecto['deleted']) :?>
    <script>
    $('.checkbox-tarea').change(function(){
        let id = <?=$proyecto['id']?>;
        let tarea = $(this).attr('data-tar');
        $.ajax({
            type: 'POST',
            url: '<?=base_url('/Proyectos/progresoProyecto')?>',
            data:{id:id, tarea:tarea},
            success: function(data){
                let cont = JSON.parse(data);
                if(cont.tipo == 'advertencia'){
                    alertify.notify(cont.mensaje, 'advertencia', 10);
                }else{
                    $('#progreso').css('width', cont.mensaje);
                    $('#progreso').html(cont.mensaje);
                    if(cont.mensaje == '100%'){
                        $('#progreso').addClass('bg-success');
                    }else{
                        $('#progreso').removeClass('bg-success');
                    }
                }
            }, error: function(data){
                alertify.notify(data.responseText, 'falla', 10);
            }
        });
    });
    $('label.badge_eliminar').on('click', function(){
        let tarea = $(this).children('span').attr('data-tarea');
        alertify.confirm('ATENCION', 'Está a punto de eliminar una tarea, ¿estás seguro de esto?', function(){
            $.ajax({
                type: 'POST',
                url: '<?=base_url('/Proyectos/eliminarTarea')?>',
                data:{id:<?=$proyecto['id']?>, tarea:tarea},
                success: function(data){
                    let cont = JSON.parse(data);
                    if(cont.tipo == 'advertencia'){
                        alertify.notify(cont.mensaje, 'advertencia', 10);
                    }else{
                        alertify.notify(cont.mensaje, 'correcto', 10);
                        $('#sec-tareas').html(cont.contenido);
                        $('#progreso').css('width', cont.porcentaje);
                        $('#progreso').html(cont.porcentaje);
                        if(cont.porcentaje == '100%'){
                            $('#progreso').addClass('bg-success');
                        }else{
                            $('#progreso').removeClass('bg-success');
                        }
                    }
                }, error: function(data){
                    alertify.notify(data.responseText, 'falla', 10);
                }
            });
        }, function(){
        });
    });
</script>
<?php endif;?>