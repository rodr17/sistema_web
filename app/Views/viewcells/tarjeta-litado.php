<div class="col-lg-12 my-3" tarjeta="<?= $t['id'] ?>">
	<label class="card p-1 contenedor_checks" for="<?= $t['id'] ?>">
		<input class="invisible check-tarjetas" type="radio" name="tarjetas_usuarios_radio" hidden id="<?= $t['id'] ?>">
		<div class="d-flex align-items-center">
			<div class="flex-shrink-0">
				<img class="ms-3" src="<?= model('autenticacion')->urlImgBanco($t['brand']) ?>" alt="Procesador de pago bancario">
			</div>
			<div class="flex-grow-1 ms-3">
				<p class="d-flex text-font fw-bold">Terminación <?= substr($t['numero'], -4) ?></p>
				<p class="d-flex text-font2"><?= $t['bank_name'] ?></p>
				<p class="d-flex text-font2">Vencimiento: <?= $t['expiration_month'] . '/' . $t['expiration_year'] ?></p>
			</div>
		</div>
	</label>
</div>
<script>
    $('input[name="tarjetas_usuarios_radio"]').change(function(e) {
    	$(this).parent().addClass('active')
    	$('.check-tarjetas').prop('checked', false).parent().removeClass('active');
    	$tarjeta = $(this).siblings().children();
    	console.log($tarjeta);
    	$('#imagen_modal').attr('src', $tarjeta.children('img').attr('src'));
    	$('#terminacion_modal').text($tarjeta.children('p:nth-child(1)').text());
    	$('#banco_modal').text($tarjeta.children('p:nth-child(2)').text());
    	$('#vencimiento_modal').text($tarjeta.children('p:nth-child(3)').text());
    
    	$('#tarj_modal').attr('datos', $(this).parent().attr('for').replace('-mem', ''));
    
    	$('#cambiar_tarjeta').modal('hide');
    	$('#obtener_membresia').modal('show');
    });
</script>