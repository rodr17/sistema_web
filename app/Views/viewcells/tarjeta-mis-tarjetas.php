<div class="col-12 col-sm-6 contenedor-tarjeta mb-3 mb-sm-0" tarjeta="<?= $t['id'] ?>" data="<?= $t['brand'] ?>">
	<div class="card-grid position-relative hover-up wow animate__ animate__fadeInUp animated actual-metodo" style="visibility: visible; animation-name: fadeInUp;">
		<div class="text-center">
			<a>
				<figure><img class="ms-3" src="<?= model('autenticacion')->urlImgBanco($t['brand']) ?>" alt="Procesador de pago bancario" width="60"></figure>
			</a>
		</div>
		<h5 class="text-center mt-0 mb-0 ">
			<a class="num-tarjeta" style="font-size: 20px;">**** <?= $t['numero'] ?> | Expira <?= $t['expiration_month'] . '/' . $t['expiration_year'] ?></a>
		</h5>
		<!-- <p class="text-center" style="margin: 0;"><= $tarjeta->holder_name?></p> -->
		<?php if(!empty($t['type']) && $t['type'] != 'unknown') :?>
			<label class="metodo-agregado">Tarjeta de <?=$t['type']?>o</label>
		<?php endif ;?>
		<div class="text-center mt-10">
			<a class="btn btn-default eliminar-tarjeta" datos="<?= $t['id'] ?>" tarjeta="<?= $t['numero'] ?>">Eliminar</a>
		</div>
	</div>
</div>