<div class="col-lg-12 mb-1" id="contenedor-tarjetaObtenerSubs-<?= $t['id'] ?>">
	<div class="card p-1">
		<div class="d-flex align-items-center">
			<div class="flex-shrink-0">
				<img class="ms-3" id="imagen_modal" src="<?= model('autenticacion')->urlImgBanco($t['brand']) ?>" alt="Procesador de pago bancario">
			</div>
			<div class="flex-grow-1 ms-3">
				<label class="d-flex text-font fw-bold" id="terminacion_modal">Terminación <?= $t['numero'] ?></label>
				<?php if(!empty($t['type']) && $t['type'] != 'unknown') :?>
					<label class="d-flex text-font2">Tarjeta de <?=$t['type']?>o</label>
				<?php endif ;?>
				<label class="d-flex text-font2" id="vencimiento_modal">Vencimiento: <?= $t['expiration_month'] . '/' . $t['expiration_year'] ?></label>
			</div>
			<div class="flex-grow-1 ms-3">
				<a class="text-font justify-content-center user-select-none text-decoration-none text-naranja opciones-ingresar" id="tarj_modal" datos="<?= $t['id'] ?>">Continuar <i class="fas fa-chevron-right"></i></a>
			</div>
		</div>
	</div>
</div>
<div class="d-flex col-12 justify-content-end">
	<a class="text-font2 opciones-ingresar" data-bs-toggle="modal" data-bs-target="#cambiar_tarjeta">Cambiar tarjeta</a>
</div>
<script>
	$('#tarj_modal').click(function(){
		$this = $(this);
		$('#btn_confirm_tarjeta').attr('datos', $(this).attr('datos'))
		$('#obtener_membresia').modal('hide');
			$.ajax({
				url: '/Suscripciones/getPlan',
				type: 'POST',
				data: {id : $('.plan_seleccionado').val(), es_tarjeta: true},
				success: function (data) {
					let respuesta = JSON.parse(data);
					if(respuesta.alerta == 'falla') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					$('#suscripcion_seleccionada_tarjeta').html(respuesta.mensaje);
					
					$('#obtener_membresia').modal('hide');
					return $('#tarjetaResumen_modal').modal('show');
				},
				error: function(data){
					let respuesta = JSON.parse(data.responseText);
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 10);
				}
			});
	});
</script>