<div class="<?= ($conteo == 0 || $conteo ==1 )? 'col-lg-6' : 'col-lg-4' ?> col-md-6 col-sm-12 mb-30">
	<div class="card-blog-1 hover-up wow animate__animated animate__fadeIn" data-wow-delay=".0s">
		<figure class="post-thumb mb-15 text-center">
			<a href="<?=base_url('/ver-noticia?id='.base64_encode($noticia['id']).'&title='.$noticia['titulo'])?>">
				<img alt="mextemps" <?= ($conteo == 0 || $conteo ==1 )? 'style="max-height: 321px;"' : '' ?> src="<?=$noticia['imagen']?>" />
			</a>
		</figure>
		<div class="card-block-info">
			<div class="post-meta text-muted d-flex align-items-center gap-2 mb-15">
				<?php if(!empty($noticia['autor'])) :?>
					<div class="d-flex align-items-center gap-2">
						<i class="fas fa-user-circle" aria-hidden="true"></i> <span><?=$noticia['autor']?></span>
					</div>
				<?php endif ;?>
				<div class="date">
					<span><i class="fi-rr-edit mr-5 text-grey-6"></i><?=view_cell('App\Libraries\FuncionesAdmin::humanizarPieza', ['fecha' => $noticia['created_at']])?></span>
				</div>
			</div>
			<h3 class="<?= ($conteo == 0 || $conteo ==1 )? '' : 'small' ?> post-title"><a href="<?=base_url('/ver-noticia?id='.base64_encode($noticia['id']).'&title='.$noticia['titulo'])?>"><?=$noticia['titulo']?></a></h3>
			<div class="card-2-bottom d-none d-md-block">
				<div class="d-flex align-items-center justify-content-between">
					<div class="keep-reading ">
						<a href="<?=base_url('/ver-noticia?id='.base64_encode($noticia['id']).'&title='.$noticia['titulo'])?>" class="btn btn-border btn-brand-hover">Continuar leyendo</a>
					</div>
				</div>
			</div>
			<div class="card-2-bottom d-block d-md-none ">
				<div class="d-flex align-items-center justify-content-between">
					<div class="keep-reading text-center w-100">
						<a href="<?=base_url('/ver-noticia?id='.base64_encode($noticia['id']).'&title='.$noticia['titulo'])?>" class="btn btn-border btn-brand-hover">Continuar leyendo</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>