<div class="row">
    <div class="col-6 text-center">
        <img class="w-75" src="<?= imagenPerfil($freelance) ?>" class="img-thumbnail">
    </div>
    <div class="col-6 align-self-center text-center">
        <div class="fw-bold">
            Proyecto:<br><?=$proyecto['titulo']?>
        </div><br>
        <a class="btn btn-primary btn-sm" href="<?=base_url('/visualizar-proyecto?id='.$proyecto['id'])?>" target="_blank">
            Ver proyecto
        </a>
    </div>
</div>
<div class="text-center p-2">
    <span class="fw-bold">Comentario</span><br>
    <?=$reporte['comentario']?>
</div>