$(document).ready(function(){
    $("#cb_billed_type").prop('checked', true);
    checkBilled();
	alertify.defaults.theme.ok = "btn btn-naranja btn-border";
	alertify.defaults.theme.cancel = "btn btn-default";
	alertify.defaults.notifier.delay = 15;
	let tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
	let tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
		return new bootstrap.Tooltip(tooltipTriggerEl)
	});
	try{  $('.input-telefono').mask('9999999999'); }
	catch(e){ console.info('%cmask no utilizado', 'color: #FE5000; font-style: italic;'); }
	
	$('.input-telefono').keydown(function (e) {
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 39)) {
			return;
		}
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	$(".validanumericos").keypress(function (e) {
		if (isNaN(this.value + String.fromCharCode(e.charCode))) return false;
	})
	.on("cut copy paste", function (e) {
		e.preventDefault();
	});
	(function () {
		'use strict';
		let forms = document.querySelectorAll('.needs-validation');

		Array.prototype.slice.call(forms).forEach(function (form) {
		  form.addEventListener('submit', function (event) {
			if (!form.checkValidity()) {
				event.preventDefault();
				event.stopPropagation();
			}
			form.classList.add('was-validated')}, false)
		});
	})() 

	$('#text-rango').text(5 * $(this).val()+"%");

	$('#aceptar-validacionPerfil').change(function(event) {
		if(!$(this).is(':checked')) $('#btn-perfil-paso1').addClass('disabled');
		else $('#btn-perfil-paso1').removeClass('disabled');
	});
	
	$('#btn_seleccion_metodo_pago').click(function(){
		if($('#seleccion_metodo_pago').val() == 'TDC/TDD'){
			return $('#agregar-pago').modal('show');
		}
	});
	$('#datalist_area').keyup(function (e) {
		if((e.keyCode == 8) && $('#datalist_area').val().length == 0){
			$('#datalist_subareas').val('');
		} 
	});
});
let iconoAlertify = '<i class="fas fa-question-circle text-azul"></i>';
//Función para validar una CURP
function curpValida(curp) {
	let re = /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/,
		validado = curp.match(re);
	
	//Coincide con el formato general?
	if (!validado) return false;
	
	//Validar que coincida el dígito verificador
	function digitoVerificador(curp17) {
		//Fuente https://consultas.curp.gob.mx/CurpSP/
		let diccionario  = "0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ", lngSuma = 0.0, lngDigito = 0.0;
		for(let i=0; i<17; i++)
			lngSuma = lngSuma + diccionario.indexOf(curp17.charAt(i)) * (18 - i);
		lngDigito = 10 - lngSuma % 10;
		if (lngDigito == 10) return 0;
		return lngDigito;
	}
	if (validado[2] != digitoVerificador(validado[1])) return false;
		
	return true; //Validado
}
//Handler para el evento cuando cambia el input
//Lleva la CURP a mayúsculas para validarlo
function validarCurp(input) {
	input.value.toUpperCase();
	let curp = input.value.toUpperCase(), resultado = input.nextElementSibling;

	if (curpValida(curp)) { // ⬅️ Acá se comprueba
		input.classList.replace('inputs-form', 'form-control');
		input.classList.contains('is-invalid') ? input.classList.replace('is-invalid' ,'is-valid') : input.classList.add('is-valid'); ;
		resultado.innerText = 'Requerido';
	} else{
		input.classList.replace('form-control', 'inputs-form');
		input.classList.contains('is-valid') ? input.classList.replace('is-valid' ,'is-invalid') : input.classList.add('is-invalid'); ;
		resultado.innerText = 'CURP Inválido';
	} 
}
function validarCurp2(input) {
	let curp = input.value.toUpperCase(), resultado = input.nextElementSibling;

	if (curpValida(curp)) { // ⬅️ Acá se comprueba
		input.classList.contains('is-invalid') ? input.classList.replace('is-invalid' ,'is-valid') : input.classList.add('is-valid'); ;
		resultado.innerText = 'Requerido';
	} else{
		input.classList.contains('is-valid') ? input.classList.replace('is-valid' ,'is-invalid') : input.classList.add('is-invalid'); ;
		resultado.innerText = 'CURP Inválido';
	} 
}
// $('#check_whatsapp').change(function() {
// 	if ($(this).is(':checked')) $(this).siblings('i.ojo_whatsapp').replaceWith('<i class="fas ojo_whatsapp visible_whatsapp fa-eye-slash"></i>');
// 	else $(this).siblings('i.ojo_whatsapp').replaceWith('<i class="fas ojo_whatsapp visible_whatsapp fa-eye"></i>');
// });
// ********************************************************* IMAGENES DE PERFIL RECORTADAS POR USUARIO VIEWS(/PERFIL, BACKEND/HOME)  *********************************************************
if($("#upload-demo").length == 1){
	let $uploadCrop, tempFilename, rawImg, imageId;
	function readFile(input) {
		if (input.files && input.files[0]) {
			let reader = new FileReader();
			reader.onload = function (e) {
				rawImg = e.target.result;
				$(".upload-demo").addClass("ready");
				$("#cropImagePop").modal("show");
			};
			reader.readAsDataURL(input.files[0]);
		} else {
			swal("Lo sentimos - Tu navegador no soporte la API FileReader");
		}
	}
	$uploadCrop = $("#upload-demo").croppie({
		enableExif: true,
		viewport: {
			width: 200,
			height: 200,
			type: 'circle'
		},
		boundary: {
			width: 300,
			height: 300
		}
	});
	$("#cropImagePop").on("shown.bs.modal", function () {
		$uploadCrop
		.croppie("bind", {url: rawImg, zoom: true})
		.then(function () {
			console.log("jQuery bind complete");
		});
	});

	$(".item-img").on("change", function () {
		imageId = $(this).data("id");
		tempFilename = $(this).val();
		$("#cancelCropBtn").data("id", imageId);
		readFile(this);
	});
	$("#cropImageBtn").on("click", function (ev) {
		$uploadCrop
		.croppie("result", {
			type: "base64",
			format: "webp",
			size: { width: 200, height: 200 }
		})
		.then(function (resp) {
			$("#imagen-perfil").attr("src", resp);
			$('#img_recortada').val(resp);
			$("#cropImagePop").modal("hide");
			$.ajax({
				type:'POST',
				url: '/usuarios/actuaImgPerfil',
				data:{resp:resp},
				success: function(data){
					let respuesta = JSON.parse(data);
					if(respuesta.alerta == 'advertencia') return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
					
					$('.img-perfil-icon').attr('src', respuesta.imagen);
					return alertify.notify(respuesta.mensaje, respuesta.alerta, 10);
				}, error: function(data){
					let respuesta = JSON.parse(data.responseText);
					alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
				}
			});
		});
	});
}

function mayus(e) {e.value = e.value.toUpperCase()}
// ********************************************************* MENU DE LOS PERFILES FREELANCER Y CONTRATISTAS *********************************************************
const list = document.querySelectorAll('.opciones_vista');
function activarLink() {
	list.forEach((item) => item.classList.remove('active'));
	this.classList.add('active');
}
list.forEach((item) => item.addEventListener('click', activarLink));
// ********************************************************* PAGOS Y MEMBRESIAS CON OPENPAY Y PAYPAL *********************************************************

$('#btn_confirm_efectivo').click(function(evento) {
	$this = $(this);
	evento.preventDefault();
	$(this).addClass('disabled');
	$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);
	if (alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro de realizar tu compra?', function() {
		$.ajax({
			url: '/Suscripcion_efectivo',
			type: 'POST',
			data: {
				id_plan: $('.plan_seleccionado').val(),
				cupon: $('#efectivo_modal .cupon_ingresado').val()
			},
			success: function(data) {
				let respuesta = JSON.parse(data);
				$('#efectivo_modal').modal('hide');
				$($this).removeClass('disabled').children('span').remove();
				$('input[name="otros_pagos"]').prop('checked', false).parent().removeClass('active');
				$('#obtener_membresia .cerrar-modal').click();
				$('.modal').modal('hide');
				$('#listado-historial-pago').load('/usuarios/HistorialPagos');
				if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta);
				$("#referencia").text(respuesta.referencia);
				$('#respuesta_pago').modal('show');
				
				alertify.notify(respuesta.mensaje, respuesta.alerta);
			},
			error: function(data) {
				let respuesta = JSON.parse(data.responseText);
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
			}
		});
	}, function() {
		$($this).removeClass('disabled').children('span').remove()
	}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})) {}
});

$('input[name="nivel"]').change(function (e) {
	e.preventDefault();
	let valor_nivel = $('input[name="nivel"]:checked').val();
	if (valor_nivel != "nativo") {
		$("#text-rango").text(0 + "%");
		return $("#rango").val(0).removeClass("hidden");
	}
	
	$("#text-rango").text(100 + "%");
	return $("#rango").val(20).addClass("hidden");
});
$('.botones').click('a', function(e) {
	e.preventDefault();
	if (e.target.name != undefined) {
		$(e.target).addClass('disabled', true);
		$(e.target).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(e.target);
		$('.plan_seleccionado').val(e.target.attributes.plan.value);
		$('#obtener_membresia').modal('show');
	}
});
$('#obtener_membresia .cerrar-modal').click(function(e) {
	$botones = $('.botones a');
	$($botones).removeClass('disabled').children('span').remove();
	$('#columna_tienda').removeClass('invisible d-none');
});
$('#agregar-pago .cerrar-modal').click(function(e) {
	$botones = $('.botones a');
	$($botones).removeClass('disabled').children('span').remove();
	$('#columna_tienda').removeClass('invisible d-none');
});
$('.actualizar_tarjeta').change(function(e) {
	$(this).parent().addClass('active')
	$('.actualizar_tarjeta').not(':checked').parent().removeClass('active');

	$dato = $(this).parent().attr('for');
	if(alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro de cambiar tu tarjeta?', function(){
		$.ajax({
			url: 'Suscripciones/actualizarTarjeta',
			type: 'POST',
			contentType: false,
			processData: false,
			data: {id : $dato},
			success: function (data) {
				let respuesta = JSON.parse(data);
				if(respuesta.alerta == 'falla') return alertify.notify(respuesta.mensaje, respuesta.alerta);
				$('#actualizar_tarjeta').modal('hide');
				alertify.notify(respuesta.mensaje, respuesta.alerta);
				setTimeout(function(){ location.reload() }, 2000);
			},
			error: function(data){
				let respuesta = JSON.parse(data.responseText);
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
			}
		});
	},
	function(){
		$($this).removeClass('disabled').children('span').remove()
	}.set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'}))){
	}
});

$('.check-tarjetasCambiopago').change(function(e) {
	$('.check-tarjetasCambiopago').prop('checked', false).parent().removeClass('active')
	$tarjeta = $(this).siblings().children();
	$('#imagen_modalCambiopago').attr('src', $tarjeta.children('img').attr('src'));
	$('#terminacion_modalCambiopago').text($tarjeta.children('p:nth-child(1)').text());
	$('#banco_modalCambiopago').text($tarjeta.children('p:nth-child(2)').text());
	$('#vencimiento_modalCambiopago').text($tarjeta.children('p:nth-child(3)').text());
	$('#tarj_modalCambiopago').attr('datos', $(this).parent().attr('for'));

	$('#cambiar_tarjetaCambiopago').modal('hide');
	$('#cambiar_metodo_pago').modal('show');
});
$('input[name="tarjetas_usuarios_radio"]').change(function(e) {
	$(this).parent().addClass('active')
	$('.check-tarjetas').prop('checked', false).parent().removeClass('active');
	$tarjeta = $(this).siblings().children();
	console.log($tarjeta);
	$('#imagen_modal').attr('src', $tarjeta.children('img').attr('src'));
	$('#terminacion_modal').text($tarjeta.children('p:nth-child(1)').text());
	$('#banco_modal').text($tarjeta.children('p:nth-child(2)').text());
	$('#vencimiento_modal').text($tarjeta.children('p:nth-child(3)').text());

	$('#tarj_modal').attr('datos', $(this).parent().attr('for').replace('-mem', ''));

	$('#cambiar_tarjeta').modal('hide');
	$('#obtener_membresia').modal('show');
});
$('#tarj_modal').click(function(){
	$this = $(this);
	$('#btn_confirm_tarjeta').attr('datos', $(this).attr('datos'))
	$('#obtener_membresia').modal('hide');
		$.ajax({
			url: '/Suscripciones/getPlan',
			type: 'POST',
			data: {id : $('.plan_seleccionado').val(), es_tarjeta: true},
			success: function (data) {
				let respuesta = JSON.parse(data);
				if(respuesta.alerta == 'falla') return alertify.notify(respuesta.mensaje, respuesta.mensaje);
				$('#suscripcion_seleccionada_tarjeta').html(respuesta.mensaje);
				$('#obtener_membresia').modal('hide');
				return $('#' + $($this).val() + '_modal').modal('show');
			},
			error: function(data){
				let respuesta = JSON.parse(data.responseText);
				alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
			}
		});
	$('#tarjetaResumen_modal').modal('show');
});
$('#tarj_modalCambiopago').click(function(){
	$this = $(this);
	if(alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro cambiar método de pago con tarjeta? <b>'+$('#terminacion_modalCambiopago').text()+'</b>?', function(){
		$('#form-metodo-pago #nuevo_metodo_pago').val($this.attr('datos'));
		$('#form-metodo-pago').submit();
	}, function(){}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})){
	}
});
// $('.check-otrosPagos').change(function(e) {
//     $(this).parent().addClass('active');
//     $('.check-otrosPagos').not(':checked').parent().removeClass('active');

//     $('#obtener_membresia').modal('hide');
//     $('#' + $(this).val() + '_modal').modal('show');
// });
$('.check-otrosPagosCambiopago').change(function(e) {
	$this = $(this);
	if(alertify.confirm(iconoAlertify+' ¡Atención!', '¿Estás seguro cambiar método de pago a <b>'+$this.val()+'</b>?', function(){
		$('#form-metodo-pago #nuevo_metodo_pago').val($this.val());
		$('#form-metodo-pago').submit();
	}, function(){}).set({transition:'zoom'}).setting('labels', {ok:'Aceptar', cancel:'Cancelar'})){
	}
});
$('#cancelar_suscripcion').click(function(evento){
	$this = $(this);
	evento.preventDefault();
});
$('.pagos .cerrar-modal').click(function(){
	$('h6[id^="suscripcion_seleccionada_"]');
	$('label[id^="costo_seleccionada_"]');
	$('label[id^="descripcion_seleccionada_"]');
});
$('.cerrar-modal[data-bs-target="#obtener_membresia"]').click(() => $('.check-otrosPagos').prop('checked', false).parent().removeClass('active'));
$('.cerrar-modal[data-bs-target="#cambiar_metodo_pago"]').click(() => $('.check-tarjetasCambiopago').prop('checked', false).parent().removeClass('active'));

$('form[id^="boletines_suscripcion_"]').delegate('button', 'click', function(e){
    if(e.target.form.correo.value.length == 0) return true;
    
    e.preventDefault();
    $.ajax({
		url: '/suscribirseBoletines',
		type: 'POST',
		data: { correo: e.target.form.correo.value },
		success: function (data) {
			let respuesta = JSON.parse(data);
			if(respuesta.alerta != 'correcto') return alertify.notify(respuesta.mensaje, respuesta.alerta);
			
			e.target.form.reset();
			return alertify.notify(respuesta.mensaje, respuesta.alerta);
		},
		error: function(data){
			let respuesta = JSON.parse(data.responseText);
			alertify.notify('Error: ' + respuesta.message + '\n' + respuesta.file, 'falla', 0);
		}
	});
});

$('#archivo').on('change', function() {
	let url = $(this)[0].files[0];
	$('#imagen-archivo').attr('src', URL.createObjectURL(url));
});
$('.btn-archivo').click(function() {
	$('#archivo').trigger('click');
});
$('#rango').on('change', function() {
	$('#text-rango').text(5 * $(this).val()+"%");
});
$('#contenido_editar_idiomas').delegate('.edit-rango', 'change', function(evento) {
	let input = evento.target;
	$('#modal-text-rango'+input.attributes.datos.value).text(5 * input.value+"%");
});
$('#lista-areas').change(function(e){
	listar_subareas($(this).val());
});
$('.botones-lista').click('input',function(e){
	if(e.target.classList.contains('invisible')) e.target.click();
});
function listar_subareas(area){
	$.ajax({
		url: 'Subareas',
		type: 'POST',
		data: {area: area},
		success: function (data) {
			let respuesta = JSON.parse(data);
			console.log(respuesta);
			$('#lista-subareas').empty();
			for(let i = 0; i < Object.keys(respuesta).length; i++){
				$('#lista-subareas').append(`<option value="`+respuesta[i]+`">`+respuesta[i]+`</option>`);
			}
		},
		error: function (data) {
			alertify.notify('Network: No se pudo cargar las subareas, comuniquese con soporte', 'falla');
		}
	});
}
$('.desplegar-modal').on('click', function(){
	let show = $(this).attr('data-show');
	let hide = $(this).attr('data-close');
	$('#'+hide).modal('hide');
	$('#'+show).modal('show');
	$('#'+show+' .desplegar-modal').attr('data-show', hide);
});
$('.asignar').on('click', function(){
	let post = $(this).attr('data-postu');
	$.ajax({
		type: 'POST',
		url: '/Proyectos/preAsignarProyecto',
		data:{post:post},
		success: function(data){
			$('#asignar-proyecto').html(data);
		}, error: function(data){
			alertify.notify('Contacte con el area de soporte.', 'falla');
		}
	});
});
function asignar(){
	let proyecto = $('#proyecto-a-asignar').val();
	let freelance = $('#freelance-esc').val();
	$.ajax({
		type: 'POST',
		url: '/Proyectos/asignarProyecto',
		data:{freelance, proyecto},
		success: function(data){
			let contenido = JSON.parse(data);
			if(contenido.tipo != 'correcto'){
				alertify.notify(contenido.mensaje, contenido.tipo);
			}else{
				alertify.notify(contenido.mensaje, contenido.tipo);
				location.reload();
			}
		}, error: function (data) {
			alertify.notify('Contacte con el area de soporte.', 'falla');
		}
	});
}
function asignarProyecto(post, modal){
	$.ajax({
		type: 'POST',
		url: '/Proyectos/preAsignarProyecto',
		data:{post:post},
		success: function(data){
			$('#asignar-proyecto').html(data);
			
		}, error: function(data){
			alertify.notify('Contacte con el area de soporte.', 'falla');
		}
	});
}
function removeFreelance(id, free){
	$('#'+id).remove();
	let frees = $('#free-select').val().split(',').filter((item) => item !== free).toString();
	$('#free-select').val(frees);
	if($('#cont_solicitudes_diponibles').text() == 'Ilimitadas') return false;
	$('#cont_solicitudes_diponibles').text(parseInt($('#cont_solicitudes_diponibles').text())+1);
	let cantidad = parseInt($label.text())+1;
    // console.log(cantidad);
    if(cantidad != 0 && (cantidad > 0)) $('#search-freelance').attr('disabled', false);
}
function noPermitido(){
    alertify.notify('Lo sentimos, para ver más información del proyecto, inicie sesión, por favor.', 'advertencia');
}

$('#listcategoria').change(function(){
    if($(this).val() != 'Seleccione un área'){
        $('#select2-listcategoria-container').addClass('fw-bold');
    }else{
        $('#select2-listcategoria-container').removeClass('fw-bold');
    }
})
$('#listasubcategoria').change(function(){
    if($(this).val() != 'Seleccione una opción' || $(this).val() != ''){
        $('#select2-listasubcategoria-container').addClass('fw-bold');
    }else{
        $('#select2-listasubcategoria-container').removeClass('fw-bold');
    }
});
function imprimir(id){
    let objFra = document.getElementById(id);
    objFra.contentWindow.focus();
    objFra.contentWindow.print();
}

(function (document, window, index) {
	let inputs = document.querySelectorAll(".inputfile");
	Array.prototype.forEach.call(inputs, function (input) {
		let label = input.nextElementSibling,
			labelVal = label.innerHTML;
		
		input.addEventListener("change", function (e) {
			let fileName = "";
			if (this.files && this.files.length > 1)
			fileName = (this.getAttribute("data-multiple-caption") || "").replace("{count}",this.files.length);
			else fileName = e.target.value.split("\\").pop();
		
			if (fileName) {
				label.querySelector("span").innerHTML = this.files.length + " Archivo";
				$("#enviar_archivo").removeClass("disabled");
			} else {
				label.innerHTML = labelVal;
				$("#enviar_archivo").addClass("disabled");
			}
		});
	});
})(document, window, 0);
	
$("#seccion_info_cupon").delegate(".validar-cupon", "click", function (e) {
	$btn = $(this);
	let cupon = $btn.siblings('input[name="cupon"]').val().trim();
	if (cupon == "") return alertify.notify("Ingresa un cupón por favor.", "advertencia");
	
	$.ajax({
		type: "POST",
		url: "/Pagos/utilizarCupon",
		data: { cupon: cupon },
		success: function (data) {
			let respuesta = JSON.parse(data);
			if (respuesta.alerta != "correcto") return alertify.notify(respuesta.mensaje, respuesta.alerta);
		
			alertify.notify("Cupón aplicado", respuesta.alerta);
			$(".listado-planes-registro").remove();
			return $("#seccion_info_cupon").html(respuesta.cuerpo);
		},
		error: function (data) {
			let respuesta = JSON.parse(data.responseText);
			alertify.error("Error: " + respuesta.message + "\n" + respuesta.file);
		},
	});
});

$(".seccion-cupon").delegate('.validar-cupon-checkout', 'click', function (e) {
	$btn = $(this);
	let cupon = $btn.siblings('input[name="cupon"]').val().trim();
	if (cupon == "") return alertify.notify("Ingresa un cupón por favor.", "advertencia");
	let metodo_pago = $('.check-otrosPagos:checked').val() == 'efectivo' ? 'efectivo' : 'tarjeta';
	let es_tarjeta = metodo_pago == 'tarjeta' ? true : false;

	$.ajax({
		type: "POST",
		url: "/Pagos/cuponCheckout",
		data: {cupon: cupon, es_tarjeta : es_tarjeta, id_plan : $('.plan_seleccionado').val()},
		success: function (data) {
			let respuesta = JSON.parse(data);
			if (respuesta.alerta != "correcto") return alertify.notify(respuesta.mensaje, respuesta.alerta);
			
			$(".seccion-cupon").load("/cupon-checkout", { cupon: cupon });
			let datos_suscripcion = {
				id : $('.plan_seleccionado').val(),
				descuento : respuesta.descuento,
				precio : respuesta.precio,
				es_tarjeta: es_tarjeta
			};
			
			$("#suscripcion_seleccionada_" + metodo_pago).load("/Suscripciones/getPlan", datos_suscripcion);
		},
		error: function (data) {
			let respuesta = JSON.parse(data.responseText);
			alertify.error("Error: " + respuesta.message + "\n" + respuesta.file);
		},
	});
});

$(".seccion-cupon").delegate(".cambiar_cupon", "click", function(){
	let metodo_pago = $('.check-otrosPagos:checked').val() == 'efectivo' ? 'efectivo' : 'tarjeta';
	let es_tarjeta = metodo_pago == 'tarjeta' ? true : false;
	let datos_plan = { id : $('.plan_seleccionado').val(), es_tarjeta : es_tarjeta };
	
	$.post("/Suscripciones/getPlan", datos_plan, (json) => $("#suscripcion_seleccionada_" + metodo_pago).html(json.mensaje), "json");
	$(".seccion-cupon").load("/cupon-checkout");
});