let iconoAlertify = '<i class="fas fa-question-circle text-azul"></i>';
alertify.defaults.theme.ok = "btn btn-primary";
alertify.defaults.theme.cancel = "btn btn-danger";
alertify.defaults.notifier.delay = 15;
$(".sidebar-dropdown > a").click(function(){
	$(".sidebar-submenu").slideUp(200);
	if($(this).parent().hasClass("active")){
		$(".sidebar-dropdown").removeClass("active");
		$(this).parent().removeClass("active");
	}else{
		$(".sidebar-dropdown").removeClass("active");
		$(this).next(".sidebar-submenu").slideDown(200);
		$(this).parent().addClass("active");
	}
});
$("#close-sidebar").click(function() {
	$(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function() {
	$(".page-wrapper").addClass("toggled");
});
$('#agregar').click(function(){
	$.ajax({
		type: 'POST',
		url: 'Administrador/newAgreg',
		data: $('#nuevo-admin').serialize(),
		success: function(data){
			var cont = JSON.parse(data);
			if(cont.tipo == 'error') alertify.warning(cont.mensaje);
			
			alertify.success(cont.mensaje);
			$('#inputs_pagos').fadeToggle();
			$('#inputs_usuarios').fadeToggle();
			$('#inputs_contenidos').fadeToggle();
			return $('#nuevo-admin')[0].reset();
		}, error: function(){
			alertify.error('NETWORK');
		}
	});
});
$('#login').on('click', function(e){
	e.preventDefault();
	$.ajax({
		type: 'POST',
		url: 'sig-in-admin',
		data: $('#form-login').serialize(),
		success:function(data){
			var con = JSON.parse(data);
			if(con.tipo == 'error'){
				alertify.set('notifier', 'position', 'top-right');
				alertify.warning(con.mensaje);
			}else{
				alertify.set('notifier','position', 'top-center');
				alertify.success('Bienvenido '+con.mensaje);
				setTimeout(function(){
					location.reload();
				}, 3000);
			}
		}, error:function(data){
			alertify.set('notifier','position', 'top-center');
			alertify.error('NETWORK');
		}
	});
});
$('#save-terminos').click(function(){
	var postTitle = $("#input-titulo").val();
	var editorData = editor.getData();
	var postBody = editorData.replace(/&nbsp;/gi,' ');
	var dataString = 'title='+ postTitle +'&body='+postBody;
	$.ajax({
		type: "POST",
		url: "guardar-seccion-terminos",
		data: dataString,
		cache: false,
		beforeSend: function(){},
		success: function(data){
			console.log(data);
			let cont = JSON.parse(data);
			if(cont.tipo == 'error'){
				alertify.set('notifier','position', 'top-right');
				alertify.warning(cont.mensaje);
				console.log(cont.mensaje);
			}else{
				alertify.set('notifier','position', 'top-right');
				alertify.success(cont.mensaje);
				setTimeout(function(){ location.href = "administrar-terminos-condiciones"},3000);
			}
		},error:function(data){
			alertify.set('notifier','position', 'top-right');
			alertify.error('Error');
		}
	});
	return false;
});
$('#actua-terminos').click(function(){
	var postBody = editor.getData().replace(/&nbsp;/gi, ' ');
	$.ajax({
		type: 'POST',
		url: '/guardar-actua-terminos',
		data: {body : postBody},
		cache: false,
		beforeSend: function(e){
			$('#actua-terminos').addClass('disabled').text('Procesando...');
		},
		success: function(data){
			let cont = JSON.parse(data);
			alertify.set('notifier', 'position', 'top-right');
			$('#actua-terminos').removeClass('disabled').text('Guardar cambios');
			if(cont.tipo == 'error') return alertify.warning(cont.mensaje);
			
			return alertify.success(cont.mensaje);
		}, error: function(data){
			$('#actua-terminos').removeClass('disabled').text('Guardar cambios');
			alertify.set('notifier', 'position', 'top-right');
			alertify.error('Error', data.responseText);
		}
	});
});
$('#save-aviso').click(function(){
	var editorData = editor.getData();
	var postBody = editorData.replace(/&nbsp;/gi, ' ');
	var dataString = 'body='+postBody;
	$.ajax({
		type: 'POST',
		url: 'guardar-aviso-privacidad',
		data: dataString,
		cache: false,
		beforeSend: function(){},
		success: function(data){
			let cont = JSON.parse(data);
			if(cont.tipo == 'error'){
				alertify.set('notifier', 'position', 'top-right');
				alertify.warning(cont.mensaje);
			}else{
				alertify.set('notifier', 'position', 'top-right');
				alertify.success(cont.mensaje);
				setTimeout(function(){ location.reload() }, 3000);
			}
		}
	});
});
$('.delete-secc-terminos').click(function(){
	var id = $(this).attr('ident');
	alertify.confirm('Cuidado', 'Esta a punto de eliminar una sección de los terminos y condiciones, ¿Esta seguro de esto?.', function(){
		$.ajax({
			type: 'POST',
			url: 'eliminar-seccion-terminos',
			data:{id:id},
			success:function(data){
				alertify.success('La sección ha sido eliminada.');
				setTimeout(function(){ location.reload(); }, 3000);
			}, error:function(){
				alertify.error('Error');
			}
		});
	}, function(){
		alertify.error('Operación cancelada');
	});
});
$('#actua-medios-contacto').click(function(){
	$.ajax({
		type: 'POST',
		url: 'update-contacto',
		data:$('#form-cont').serialize(),
		success: function(data){
			let respuesta = JSON.parse(data);
			if(respuesta.tipo == 'success') return alertify.success(respuesta.mensaje);
			alertify.warning(data);
		}, error: function(){
			alertify.error('NETWORK');
		}
	});
});
$('.save-ingreso').click(function(){
	let tipo = $(this).attr('data-tipo');
	$.ajax({
		type: 'POST',
		url: 'actualizar-ingresos',
		data: $('#form'+tipo).serialize(),
		success: function(data){
			let content = JSON.parse(data);
			if(content.tipo == 'error'){
				alertify.warning(content.mensaje);
			}else{
				alertify.success(content.mensaje);
			}
		}, error: function(data){
			alertify.error('NETWORK');
		}
	});
});
$('#save-netpay').click(function(){
	$.ajax({
		type: 'POST',
		url: 'Administrador/updateNetPay',
		data: $('#form-netpay').serialize(),
		success: function(data){
			let respuesta = JSON.parse(data);
			if(respuesta.tipo != 'correcto') return alertify.warning(respuesta.mensaje);
			
			return alertify.success(respuesta.mensaje);
		}, error: function(data){
			alertify.error(data.responseText);
		}
	});
});
$('#nueva-membresia').click(function(){
	$('#new-plan').fadeIn();
});
$('#crear-plan').click(function(e){
	e.preventDefault();
	$(this).addClass('disabled', true);
	$(this).add('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').appendTo(this);

	$.ajax({
		type: 'POST',
		url: 'crear-membresia',
		data:$('#form').serialize(),
		success: function(data){
			let respuesta = JSON.parse(data);

			$('#crear-plan').removeClass('disabled').children('span').remove();
			if(respuesta.alerta != 'correcto') return alertify.warning(respuesta.mensaje);
			
			alertify.success(respuesta.mensaje);
			setTimeout(function(){ location.reload() }, 3000);
		}, error: function(data){
			alertify.notify(data.resposneText);
		}
	});
});
$('.edit-member').click(function(){
	var id = $(this).attr('data-id');
	$.ajax({
		type: 'POST',
		url: 'editar-membresia',
		data:{id:id},
		success: function(data){
			$('#zone-members').html(data);
		}, error: function(data){
			alertify.error(data.responseText)
		}
	});
});
$('.elimi-membership').click(function(){
	let id = $(this).attr('data-id');
	alertify.confirm('ATENCION', 'Esta a punto de eliminar la membresía, ¿está seguro de esto?', function(){
		$.ajax({
			type: 'POST',
			url: 'Administrador/elimiMembership',
			data:{id:id},
			success: function(data){
				alertify.notify('Eliminado', 'correcto');
				setTimeout(function(){ location.reload() },3000);
			}, error: function(data){
				alertify.error(data.responseText);
			}
		});
	}, function(){ });
});
function editarMemb(){
	$.ajax({
		type: 'POST',
		url: 'actualizar-membresia',
		data: $('#edit-form').serialize(),
		success: function(data){
			let respuesta = JSON.parse(data);
			if(respuesta.tipo != 'correcto') return alertify.warning(respuesta.mensaje);
			
			alertify.success(respuesta.mensaje);
			setTimeout(function(){ location.reload() }, 3000);
		}, error: function(data){
			alertify.error(data.responseText);
		}
	});
}
$('.mandar-correo').click(function(){
	var correo = $(this).attr('data-correo');
	var id = $(this).attr('data-id');
	var user = $(this).attr('data-user');
	$('#nom-admin').html(user);
	$('#user-id').val(id);
});
$('#enviar-correo-admin').click(function(){
	var editorData = editor.getData();
	var id = $('#user-id').val();
	var postBody = editorData.replace(/&nbsp;/gi, ' ');
	var dataString = 'id='+id+'&body='+postBody;
	$.ajax({
		type: 'POST',
		url: 'enviar-correo-admins',
		data: dataString,
		cache: false,
		beforeSend: function(){},
		success: function(data){
			let cont = JSON.parse(data);
			if(cont.tipo == 'error'){
				alertify.warning(cont.mensaje);
			}else if(cont.tipo == 'warning'){
				alertify.error(cont.mensaje);
			}else{
				alertify.success(cont.mensaje);
				editor.setData('');
			}
		}, error: function(data){
			alertify.error('NETWORK');
		}
	});
});
$('#actua-perfil').click(function(){
	var formData = new FormData(document.getElementById("form-admin"));
	formData.append('name', $('#name').val());
	formData.append('nombre', $('#nombre').val());
	formData.append('correo', $('#correo').val());
	formData.append('telefono', $('#telefono').val());
	formData.append('correorecuperacion', $('#correorecuperacion').val());
	formData.append('filePicker[]',filePicker);
	$.ajax({
		url: 'Administrador/updatePerfilAdmin',
		data: formData,
		cache: false,
		contentType: false,
		processData: false,
		type: 'POST',
		success: function(data){
			var con = JSON.parse(data);
			if(con.tipo != 'success') return alertify.warning(con.mensaje);
			console.log(con.imagen);
			$('#img_icon_administrador').attr('src', con.imagen);
			return alertify.success(con.mensaje);
		}, error:function(jqXHR, textStatus, errorThrown){
			alertify.error('NTEWORK');
		}
	});
});
$('#actua-administrador').click(function(){
	let form = $('#form-admin')[0];
	let formData = new FormData(form);
	console.log(formData);
	$.ajax({
		type: 'POST',
		method: 'POST',
		url: 'Administrador/updateAdministrador',
		data: formData,
		cache: false,
		contentType: false,
		processData: false,
		success: function(data){
			let respuesta = JSON.parse(data);
			if(respuesta.tipo == 'advertencia'){
				if(respuesta.seccion == 'modal'){
					alertify.notify(respuesta.mensaje, respuesta.tipo);
				}else{
					$('#solicitudmodal').modal('hide');
					$('#'+respuesta.seccion).click();
					alertify.notify(respuesta.mensaje, respuesta.tipo);
				}
			}else{
				$('#solicitudmodal').modal('hide');
				alertify.notify(respuesta.mensaje, respuesta.tipo);
				$('#free-seleccionados').html('');
				$('#free-select').val('');
				$('#proyecto-publicado').modal('show');
				$('#solicitudes-publicar').removeAttr('disabled');
				$('#solicitudes-publicar span').remove();
			}
		}
	});
});
$('#nueva-contra').click(function(){
	alertify.confirm('ATENCIÓN', 'Esta a punto de cambiar su contraseña, ¿Esta seguro de esto?.', function(){
		$.ajax({
			type: 'POST',
			url: '/Administrador/newContraAdmin',
			data:$('#formnewcontra').serialize(),
			success: function(data){
				let cont = JSON.parse(data);
				if(cont.tipo == 'error'){
					alertify.warning(cont.mensaje);
				}else{
					alertify.success(cont.mensaje);
					$('#formnewcontra')[0].reset();
				}
			}, error: function(){
				alertify.error('NETWORK');
			}
		});
	}, function(){
		alertify.error('Operación cancelada');
	});
});
$('.elim-admin').click(function(){
	var id = $(this).attr('data-id');
	alertify.confirm('¡ATENCIÓN!', 'Está a punto de eliminar al usuario administrador, una vez eliminado no se podrá recuperar su información ¿Está seguro de esto?', function(){
		
	}, function(){});
});
$('#input-gratuito').click(function(){
	if($(this).is(':checked')){
		$('.div-precio').hide(); 
		$('#costoPlan').val('0');
		$('#costoPlanAnual').val('0');
		$('#div-gratuito').fadeIn();
	} else {
		$('#div-gratuito').hide();
		$('#costoPlan').val('');
		$('#costoPlanAnual').val('');
		$('.div-precio').fadeIn();
	}
});
$(document).ready(function(){
	$('.input-telefono').mask('999 999 99 99');
	$('.input-telefono').keydown(function (e) {
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 39)) {
			return;
		}
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
	var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
		return new bootstrap.Tooltip(tooltipTriggerEl)
	});

//     var search = document.querySelector('#buscador');
//     var results = document.querySelector('#datalist');
//     var templateContent = document.querySelector('#listtemplate').content;
//     search.addEventListener('keyup', function handler(event) {
//         while (results.children.length) results.removeChild(results.firstChild);
//         var inputVal = new RegExp(search.value.trim(), 'i');
//         var set = Array.prototype.reduce.call(templateContent.cloneNode(true).children, function searchFilter(frag, item, i) {
//             if (inputVal.test(item.value) && frag.children.length < 6) frag.appendChild(item);
//             return frag;
//         }, document.createDocumentFragment());
//         results.appendChild(set);
//     });
//     $('#buscador').change(function() {
// 		var value = $('#buscador').val();
//         var data = $('#datalist [value="' + value + '"]').data('link');
//         location.href = data;
// 	});
});
function verSubAreas(area){
	$.ajax({
		type: 'POST',
		url: '/Habilidades/versubareas',
		data:{area:area},
		success: function(data){
			$('#subareas').html(data);
		}, error: function(data){
			alertify.error('NETWORK');
		}
	});
}
// ***************************************** @RODRIGO VAZQUEZ ORTIZ@ *****************************************