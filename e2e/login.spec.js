const { test, expect } = require('@playwright/test');

// test.beforeEach(async ({ page }) => {
//   await page.goto('https://localhost/sistema_web/');
// });

const USER_TRABAJADOR = "rodriielmejor@gmail.com";
const PASS_TRABAJADOR = "1452";

const USER_CONTRATISTA = "rodrii_290916@outlook.com";
const PASS_CONTRATISTA = "1452";

test.describe('Flujo de Login', () => {

    test('login credenciales incorrectas', async ({ page }) => {
        await page.goto('http://localhost/Mextemps/ingresar');
        
        //datos de acceso
        await page.locator('input[name="correo"]').fill("hola");
        await page.locator('input[name="contraseña"]').fill("pass");
        
        await page.locator('#btn-ingresar').click();
    
        // Check that input is empty.
        await expect(page.locator('text=Correo incorrecto o no se encontró en nuestros registros')).toBeVisible();
      });
    

    test('login trabajador correctamente', async ({ page, context }) => {
        await page.goto('http://localhost/Mextemps/ingresar');
        
        //datos de acceso
        await page.locator('input[name="correo"]').fill(USER_TRABAJADOR);
        await page.locator('input[name="contraseña"]').fill(PASS_TRABAJADOR);
        
        await page.locator('#btn-ingresar').click();
    
        // Check that input is empty.
        await expect(page.locator('h2').nth(0)).toHaveText(/Trabajos/);
        await context.storageState({ path: 'trabajador.json' });
      });

    test('login contratista correctamente', async ({ page, context }) => {
        await page.goto('http://localhost/Mextemps/ingresar');
        
        //datos de acceso
        await page.locator('input[name="correo"]').fill(USER_CONTRATISTA);
        await page.locator('input[name="contraseña"]').fill(PASS_CONTRATISTA);
        
        await page.locator('#btn-ingresar').click();
    
        // Check that input is empty.
        await expect(page.locator('h2').nth(0)).toHaveText(/Trabajos/);
        await context.storageState({ path: 'contratista.json' });
      });
});