const { test, expect } = require('@playwright/test');

test.describe('Flujo de Trabajador', () => {

    test('pagina de inicio', async ({ browser }) => {
        const context = await browser.newContext({ storageState: 'trabajador.json' });
        const page = await context.newPage();
        await page.goto('https://localhost/Mextemps/usuarios/panel');

        await expect(page.locator('h2').nth(0)).toHaveText(/Trabajos/);

    });

    //OTRO TEST ...

});