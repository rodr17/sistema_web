const { initializeApp, applicationDefault, cert } = require('firebase-admin/app');
const { getFirestore, Timestamp, FieldValue, query, onSnapshot, collection, getDocs } = require('firebase-admin/firestore');
const functions = require('firebase-functions');

const firebaseConfig = {
    apiKey: "AIzaSyCmtOCzTL7RjCi2ZkoJExiBmrGTxgnxN6Q",
    authDomain: "mextemps-341000.firebaseapp.com",
    projectId: "mextemps-341000",
    storageBucket: "mextemps-341000.appspot.com",
    messagingSenderId: "198033966029",
    appId: "1:198033966029:web:12659433a00b1e05e1fa84",
    measurementId: "G-SZY88RED1D",
    credential: cert(require('./firebase.json')),
 };

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
console.log(app);
